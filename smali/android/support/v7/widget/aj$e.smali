.class public abstract Landroid/support/v7/widget/aj$e;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v7/widget/aj;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v7/widget/aj$e$b;,
        Landroid/support/v7/widget/aj$e$a;
    }
.end annotation


# instance fields
.field private a:Landroid/support/v7/widget/aj$e$a;


# direct methods
.method static a(Landroid/support/v7/widget/aj$u;)I
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 9936
    invoke-static {p0}, Landroid/support/v7/widget/aj$u;->c(Landroid/support/v7/widget/aj$u;)I

    move-result v0

    and-int/lit8 v0, v0, 0xe

    .line 9937
    invoke-virtual {p0}, Landroid/support/v7/widget/aj$u;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 9938
    const/4 v0, 0x4

    .line 9947
    :cond_0
    :goto_0
    return v0

    .line 9940
    :cond_1
    and-int/lit8 v1, v0, 0x4

    if-nez v1, :cond_0

    .line 9941
    invoke-virtual {p0}, Landroid/support/v7/widget/aj$u;->getOldPosition()I

    move-result v1

    .line 9942
    invoke-virtual {p0}, Landroid/support/v7/widget/aj$u;->getAdapterPosition()I

    move-result v2

    .line 9943
    if-eq v1, v3, :cond_0

    if-eq v2, v3, :cond_0

    if-eq v1, v2, :cond_0

    .line 9944
    or-int/lit16 v0, v0, 0x800

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/support/v7/widget/aj$r;Landroid/support/v7/widget/aj$u;)Landroid/support/v7/widget/aj$e$b;
    .locals 1

    .prologue
    .line 9789
    invoke-virtual {p0}, Landroid/support/v7/widget/aj$e;->c()Landroid/support/v7/widget/aj$e$b;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/support/v7/widget/aj$e$b;->a(Landroid/support/v7/widget/aj$u;)Landroid/support/v7/widget/aj$e$b;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/support/v7/widget/aj$r;Landroid/support/v7/widget/aj$u;ILjava/util/List;)Landroid/support/v7/widget/aj$e$b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v7/widget/aj$r;",
            "Landroid/support/v7/widget/aj$u;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Landroid/support/v7/widget/aj$e$b;"
        }
    .end annotation

    .prologue
    .line 9760
    invoke-virtual {p0}, Landroid/support/v7/widget/aj$e;->c()Landroid/support/v7/widget/aj$e$b;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/support/v7/widget/aj$e$b;->a(Landroid/support/v7/widget/aj$u;)Landroid/support/v7/widget/aj$e$b;

    move-result-object v0

    return-object v0
.end method

.method public abstract a()V
.end method

.method a(Landroid/support/v7/widget/aj$e$a;)V
    .locals 0

    .prologue
    .line 9721
    iput-object p1, p0, Landroid/support/v7/widget/aj$e;->a:Landroid/support/v7/widget/aj$e$a;

    .line 9722
    return-void
.end method

.method public abstract a(Landroid/support/v7/widget/aj$u;Landroid/support/v7/widget/aj$u;Landroid/support/v7/widget/aj$e$b;Landroid/support/v7/widget/aj$e$b;)Z
.end method

.method public abstract b(Landroid/support/v7/widget/aj$u;)V
.end method

.method public abstract b()Z
.end method

.method public c()Landroid/support/v7/widget/aj$e$b;
    .locals 1

    .prologue
    .line 10145
    new-instance v0, Landroid/support/v7/widget/aj$e$b;

    invoke-direct {v0}, Landroid/support/v7/widget/aj$e$b;-><init>()V

    return-object v0
.end method

.method public c(Landroid/support/v7/widget/aj$u;)Z
    .locals 1

    .prologue
    .line 10120
    const/4 v0, 0x1

    return v0
.end method
