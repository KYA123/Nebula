.class public abstract Landroid/support/v7/widget/aj$f;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v7/widget/aj;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "f"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7881
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Canvas;Landroid/support/v7/widget/aj;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 7901
    return-void
.end method

.method public a(Landroid/graphics/Canvas;Landroid/support/v7/widget/aj;Landroid/support/v7/widget/aj$r;)V
    .locals 0

    .prologue
    .line 7892
    invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/aj$f;->a(Landroid/graphics/Canvas;Landroid/support/v7/widget/aj;)V

    .line 7893
    return-void
.end method

.method public a(Landroid/graphics/Rect;ILandroid/support/v7/widget/aj;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 7931
    invoke-virtual {p1, v0, v0, v0, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 7932
    return-void
.end method

.method public a(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/aj;Landroid/support/v7/widget/aj$r;)V
    .locals 1

    .prologue
    .line 7955
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/aj$h;

    invoke-virtual {v0}, Landroid/support/v7/widget/aj$h;->c()I

    move-result v0

    invoke-virtual {p0, p1, v0, p3}, Landroid/support/v7/widget/aj$f;->a(Landroid/graphics/Rect;ILandroid/support/v7/widget/aj;)V

    .line 7957
    return-void
.end method

.method public b(Landroid/graphics/Canvas;Landroid/support/v7/widget/aj;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 7922
    return-void
.end method

.method public b(Landroid/graphics/Canvas;Landroid/support/v7/widget/aj;Landroid/support/v7/widget/aj$r;)V
    .locals 0

    .prologue
    .line 7913
    invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/aj$f;->b(Landroid/graphics/Canvas;Landroid/support/v7/widget/aj;)V

    .line 7914
    return-void
.end method
