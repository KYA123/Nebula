.class Landroid/support/v7/widget/aj$o;
.super Landroid/support/v7/widget/aj$c;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v7/widget/aj;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "o"
.end annotation


# instance fields
.field final synthetic a:Landroid/support/v7/widget/aj;


# virtual methods
.method a()V
    .locals 2

    .prologue
    .line 4050
    iget-object v0, p0, Landroid/support/v7/widget/aj$o;->a:Landroid/support/v7/widget/aj;

    invoke-static {v0}, Landroid/support/v7/widget/aj;->j(Landroid/support/v7/widget/aj;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/aj$o;->a:Landroid/support/v7/widget/aj;

    invoke-static {v0}, Landroid/support/v7/widget/aj;->k(Landroid/support/v7/widget/aj;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/aj$o;->a:Landroid/support/v7/widget/aj;

    invoke-static {v0}, Landroid/support/v7/widget/aj;->l(Landroid/support/v7/widget/aj;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4051
    iget-object v0, p0, Landroid/support/v7/widget/aj$o;->a:Landroid/support/v7/widget/aj;

    iget-object v1, p0, Landroid/support/v7/widget/aj$o;->a:Landroid/support/v7/widget/aj;

    invoke-static {v1}, Landroid/support/v7/widget/aj;->m(Landroid/support/v7/widget/aj;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/support/v4/h/ab;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 4056
    :goto_0
    return-void

    .line 4053
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/aj$o;->a:Landroid/support/v7/widget/aj;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/support/v7/widget/aj;->a(Landroid/support/v7/widget/aj;Z)Z

    .line 4054
    iget-object v0, p0, Landroid/support/v7/widget/aj$o;->a:Landroid/support/v7/widget/aj;

    invoke-virtual {v0}, Landroid/support/v7/widget/aj;->requestLayout()V

    goto :goto_0
.end method

.method public onChanged()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 4001
    iget-object v0, p0, Landroid/support/v7/widget/aj$o;->a:Landroid/support/v7/widget/aj;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/aj;->a(Ljava/lang/String;)V

    .line 4002
    iget-object v0, p0, Landroid/support/v7/widget/aj$o;->a:Landroid/support/v7/widget/aj;

    invoke-static {v0}, Landroid/support/v7/widget/aj;->c(Landroid/support/v7/widget/aj;)Landroid/support/v7/widget/aj$a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/aj$a;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4006
    iget-object v0, p0, Landroid/support/v7/widget/aj$o;->a:Landroid/support/v7/widget/aj;

    iget-object v0, v0, Landroid/support/v7/widget/aj;->f:Landroid/support/v7/widget/aj$r;

    invoke-static {v0, v2}, Landroid/support/v7/widget/aj$r;->a(Landroid/support/v7/widget/aj$r;Z)Z

    .line 4007
    iget-object v0, p0, Landroid/support/v7/widget/aj$o;->a:Landroid/support/v7/widget/aj;

    invoke-static {v0}, Landroid/support/v7/widget/aj;->i(Landroid/support/v7/widget/aj;)V

    .line 4012
    :goto_0
    iget-object v0, p0, Landroid/support/v7/widget/aj$o;->a:Landroid/support/v7/widget/aj;

    iget-object v0, v0, Landroid/support/v7/widget/aj;->b:Landroid/support/v7/widget/f;

    invoke-virtual {v0}, Landroid/support/v7/widget/f;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4013
    iget-object v0, p0, Landroid/support/v7/widget/aj$o;->a:Landroid/support/v7/widget/aj;

    invoke-virtual {v0}, Landroid/support/v7/widget/aj;->requestLayout()V

    .line 4015
    :cond_0
    return-void

    .line 4009
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/aj$o;->a:Landroid/support/v7/widget/aj;

    iget-object v0, v0, Landroid/support/v7/widget/aj;->f:Landroid/support/v7/widget/aj$r;

    invoke-static {v0, v2}, Landroid/support/v7/widget/aj$r;->a(Landroid/support/v7/widget/aj$r;Z)Z

    .line 4010
    iget-object v0, p0, Landroid/support/v7/widget/aj$o;->a:Landroid/support/v7/widget/aj;

    invoke-static {v0}, Landroid/support/v7/widget/aj;->i(Landroid/support/v7/widget/aj;)V

    goto :goto_0
.end method

.method public onItemRangeChanged(IILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 4019
    iget-object v0, p0, Landroid/support/v7/widget/aj$o;->a:Landroid/support/v7/widget/aj;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/aj;->a(Ljava/lang/String;)V

    .line 4020
    iget-object v0, p0, Landroid/support/v7/widget/aj$o;->a:Landroid/support/v7/widget/aj;

    iget-object v0, v0, Landroid/support/v7/widget/aj;->b:Landroid/support/v7/widget/f;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v7/widget/f;->a(IILjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4021
    invoke-virtual {p0}, Landroid/support/v7/widget/aj$o;->a()V

    .line 4023
    :cond_0
    return-void
.end method

.method public onItemRangeInserted(II)V
    .locals 2

    .prologue
    .line 4027
    iget-object v0, p0, Landroid/support/v7/widget/aj$o;->a:Landroid/support/v7/widget/aj;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/aj;->a(Ljava/lang/String;)V

    .line 4028
    iget-object v0, p0, Landroid/support/v7/widget/aj$o;->a:Landroid/support/v7/widget/aj;

    iget-object v0, v0, Landroid/support/v7/widget/aj;->b:Landroid/support/v7/widget/f;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/f;->b(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4029
    invoke-virtual {p0}, Landroid/support/v7/widget/aj$o;->a()V

    .line 4031
    :cond_0
    return-void
.end method

.method public onItemRangeMoved(III)V
    .locals 2

    .prologue
    .line 4043
    iget-object v0, p0, Landroid/support/v7/widget/aj$o;->a:Landroid/support/v7/widget/aj;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/aj;->a(Ljava/lang/String;)V

    .line 4044
    iget-object v0, p0, Landroid/support/v7/widget/aj$o;->a:Landroid/support/v7/widget/aj;

    iget-object v0, v0, Landroid/support/v7/widget/aj;->b:Landroid/support/v7/widget/f;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v7/widget/f;->a(III)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4045
    invoke-virtual {p0}, Landroid/support/v7/widget/aj$o;->a()V

    .line 4047
    :cond_0
    return-void
.end method

.method public onItemRangeRemoved(II)V
    .locals 2

    .prologue
    .line 4035
    iget-object v0, p0, Landroid/support/v7/widget/aj$o;->a:Landroid/support/v7/widget/aj;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/aj;->a(Ljava/lang/String;)V

    .line 4036
    iget-object v0, p0, Landroid/support/v7/widget/aj$o;->a:Landroid/support/v7/widget/aj;

    iget-object v0, v0, Landroid/support/v7/widget/aj;->b:Landroid/support/v7/widget/f;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/f;->c(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4037
    invoke-virtual {p0}, Landroid/support/v7/widget/aj$o;->a()V

    .line 4039
    :cond_0
    return-void
.end method
