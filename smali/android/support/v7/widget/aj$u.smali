.class public abstract Landroid/support/v7/widget/aj$u;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v7/widget/aj;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "u"
.end annotation


# static fields
.field private static final l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field a:I

.field b:I

.field c:J

.field d:I

.field e:I

.field f:Landroid/support/v7/widget/aj$u;

.field g:Landroid/support/v7/widget/aj$u;

.field h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final itemView:Landroid/view/View;

.field j:Landroid/support/v7/widget/aj;

.field private k:I

.field private m:I

.field private n:Landroid/support/v7/widget/aj$m;

.field private o:Z

.field private p:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8232
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    sput-object v0, Landroid/support/v7/widget/aj$u;->l:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 8256
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8128
    iput v3, p0, Landroid/support/v7/widget/aj$u;->a:I

    .line 8129
    iput v3, p0, Landroid/support/v7/widget/aj$u;->b:I

    .line 8130
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroid/support/v7/widget/aj$u;->c:J

    .line 8131
    iput v3, p0, Landroid/support/v7/widget/aj$u;->d:I

    .line 8132
    iput v3, p0, Landroid/support/v7/widget/aj$u;->e:I

    .line 8135
    iput-object v2, p0, Landroid/support/v7/widget/aj$u;->f:Landroid/support/v7/widget/aj$u;

    .line 8137
    iput-object v2, p0, Landroid/support/v7/widget/aj$u;->g:Landroid/support/v7/widget/aj$u;

    .line 8234
    iput-object v2, p0, Landroid/support/v7/widget/aj$u;->h:Ljava/util/List;

    .line 8235
    iput-object v2, p0, Landroid/support/v7/widget/aj$u;->i:Ljava/util/List;

    .line 8237
    iput v4, p0, Landroid/support/v7/widget/aj$u;->m:I

    .line 8241
    iput-object v2, p0, Landroid/support/v7/widget/aj$u;->n:Landroid/support/v7/widget/aj$m;

    .line 8243
    iput-boolean v4, p0, Landroid/support/v7/widget/aj$u;->o:Z

    .line 8247
    iput v4, p0, Landroid/support/v7/widget/aj$u;->p:I

    .line 8257
    if-nez p1, :cond_0

    .line 8258
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "itemView may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 8260
    :cond_0
    iput-object p1, p0, Landroid/support/v7/widget/aj$u;->itemView:Landroid/view/View;

    .line 8261
    return-void
.end method

.method static synthetic a(Landroid/support/v7/widget/aj$u;Landroid/support/v7/widget/aj$m;)Landroid/support/v7/widget/aj$m;
    .locals 0

    .prologue
    .line 8126
    iput-object p1, p0, Landroid/support/v7/widget/aj$u;->n:Landroid/support/v7/widget/aj$m;

    return-object p1
.end method

.method static synthetic a(Landroid/support/v7/widget/aj$u;)Z
    .locals 1

    .prologue
    .line 8126
    invoke-direct {p0}, Landroid/support/v7/widget/aj$u;->t()Z

    move-result v0

    return v0
.end method

.method static synthetic a(Landroid/support/v7/widget/aj$u;Z)Z
    .locals 0

    .prologue
    .line 8126
    iput-boolean p1, p0, Landroid/support/v7/widget/aj$u;->o:Z

    return p1
.end method

.method static synthetic b(Landroid/support/v7/widget/aj$u;)Z
    .locals 1

    .prologue
    .line 8126
    iget-boolean v0, p0, Landroid/support/v7/widget/aj$u;->o:Z

    return v0
.end method

.method static synthetic c(Landroid/support/v7/widget/aj$u;)I
    .locals 1

    .prologue
    .line 8126
    iget v0, p0, Landroid/support/v7/widget/aj$u;->k:I

    return v0
.end method

.method private s()V
    .locals 1

    .prologue
    .line 8475
    iget-object v0, p0, Landroid/support/v7/widget/aj$u;->h:Ljava/util/List;

    if-nez v0, :cond_0

    .line 8476
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/aj$u;->h:Ljava/util/List;

    .line 8477
    iget-object v0, p0, Landroid/support/v7/widget/aj$u;->h:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/aj$u;->i:Ljava/util/List;

    .line 8479
    :cond_0
    return-void
.end method

.method private t()Z
    .locals 1

    .prologue
    .line 8612
    iget v0, p0, Landroid/support/v7/widget/aj$u;->k:I

    and-int/lit8 v0, v0, 0x10

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/aj$u;->itemView:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/h/ab;->c(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 8286
    iput v0, p0, Landroid/support/v7/widget/aj$u;->b:I

    .line 8287
    iput v0, p0, Landroid/support/v7/widget/aj$u;->e:I

    .line 8288
    return-void
.end method

.method a(II)V
    .locals 2

    .prologue
    .line 8458
    iget v0, p0, Landroid/support/v7/widget/aj$u;->k:I

    xor-int/lit8 v1, p2, -0x1

    and-int/2addr v0, v1

    and-int v1, p1, p2

    or-int/2addr v0, v1

    iput v0, p0, Landroid/support/v7/widget/aj$u;->k:I

    .line 8459
    return-void
.end method

.method a(Landroid/support/v7/widget/aj$m;Z)V
    .locals 0

    .prologue
    .line 8425
    iput-object p1, p0, Landroid/support/v7/widget/aj$u;->n:Landroid/support/v7/widget/aj$m;

    .line 8426
    iput-boolean p2, p0, Landroid/support/v7/widget/aj$u;->o:Z

    .line 8427
    return-void
.end method

.method a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 8466
    if-nez p1, :cond_1

    .line 8467
    const/16 v0, 0x400

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/aj$u;->b(I)V

    .line 8472
    :cond_0
    :goto_0
    return-void

    .line 8468
    :cond_1
    iget v0, p0, Landroid/support/v7/widget/aj$u;->k:I

    and-int/lit16 v0, v0, 0x400

    if-nez v0, :cond_0

    .line 8469
    invoke-direct {p0}, Landroid/support/v7/widget/aj$u;->s()V

    .line 8470
    iget-object v0, p0, Landroid/support/v7/widget/aj$u;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method a(I)Z
    .locals 1

    .prologue
    .line 8446
    iget v0, p0, Landroid/support/v7/widget/aj$u;->k:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b()V
    .locals 2

    .prologue
    .line 8291
    iget v0, p0, Landroid/support/v7/widget/aj$u;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 8292
    iget v0, p0, Landroid/support/v7/widget/aj$u;->a:I

    iput v0, p0, Landroid/support/v7/widget/aj$u;->b:I

    .line 8294
    :cond_0
    return-void
.end method

.method b(I)V
    .locals 1

    .prologue
    .line 8462
    iget v0, p0, Landroid/support/v7/widget/aj$u;->k:I

    or-int/2addr v0, p1

    iput v0, p0, Landroid/support/v7/widget/aj$u;->k:I

    .line 8463
    return-void
.end method

.method c()Z
    .locals 1

    .prologue
    .line 8297
    iget v0, p0, Landroid/support/v7/widget/aj$u;->k:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method d()Z
    .locals 1

    .prologue
    .line 8401
    iget-object v0, p0, Landroid/support/v7/widget/aj$u;->n:Landroid/support/v7/widget/aj$m;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method e()V
    .locals 1

    .prologue
    .line 8405
    iget-object v0, p0, Landroid/support/v7/widget/aj$u;->n:Landroid/support/v7/widget/aj$m;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/aj$m;->d(Landroid/support/v7/widget/aj$u;)V

    .line 8406
    return-void
.end method

.method f()Z
    .locals 1

    .prologue
    .line 8409
    iget v0, p0, Landroid/support/v7/widget/aj$u;->k:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method g()V
    .locals 1

    .prologue
    .line 8413
    iget v0, p0, Landroid/support/v7/widget/aj$u;->k:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Landroid/support/v7/widget/aj$u;->k:I

    .line 8414
    return-void
.end method

.method public final getAdapterPosition()I
    .locals 1

    .prologue
    .line 8362
    iget-object v0, p0, Landroid/support/v7/widget/aj$u;->j:Landroid/support/v7/widget/aj;

    if-nez v0, :cond_0

    .line 8363
    const/4 v0, -0x1

    .line 8365
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/aj$u;->j:Landroid/support/v7/widget/aj;

    invoke-static {v0, p0}, Landroid/support/v7/widget/aj;->b(Landroid/support/v7/widget/aj;Landroid/support/v7/widget/aj$u;)I

    move-result v0

    goto :goto_0
.end method

.method public final getItemId()J
    .locals 2

    .prologue
    .line 8390
    iget-wide v0, p0, Landroid/support/v7/widget/aj$u;->c:J

    return-wide v0
.end method

.method public final getItemViewType()I
    .locals 1

    .prologue
    .line 8397
    iget v0, p0, Landroid/support/v7/widget/aj$u;->d:I

    return v0
.end method

.method public final getLayoutPosition()I
    .locals 2

    .prologue
    .line 8336
    iget v0, p0, Landroid/support/v7/widget/aj$u;->e:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Landroid/support/v7/widget/aj$u;->a:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Landroid/support/v7/widget/aj$u;->e:I

    goto :goto_0
.end method

.method public final getOldPosition()I
    .locals 1

    .prologue
    .line 8380
    iget v0, p0, Landroid/support/v7/widget/aj$u;->b:I

    return v0
.end method

.method public final getPosition()I
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 8310
    iget v0, p0, Landroid/support/v7/widget/aj$u;->e:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Landroid/support/v7/widget/aj$u;->a:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Landroid/support/v7/widget/aj$u;->e:I

    goto :goto_0
.end method

.method h()V
    .locals 1

    .prologue
    .line 8417
    iget v0, p0, Landroid/support/v7/widget/aj$u;->k:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Landroid/support/v7/widget/aj$u;->k:I

    .line 8418
    return-void
.end method

.method i()Z
    .locals 1

    .prologue
    .line 8430
    iget v0, p0, Landroid/support/v7/widget/aj$u;->k:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isRecyclable()Z
    .locals 1

    .prologue
    .line 8595
    iget v0, p0, Landroid/support/v7/widget/aj$u;->k:I

    and-int/lit8 v0, v0, 0x10

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/aj$u;->itemView:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/h/ab;->c(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method j()Z
    .locals 1

    .prologue
    .line 8434
    iget v0, p0, Landroid/support/v7/widget/aj$u;->k:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method k()Z
    .locals 1

    .prologue
    .line 8438
    iget v0, p0, Landroid/support/v7/widget/aj$u;->k:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method l()Z
    .locals 1

    .prologue
    .line 8442
    iget v0, p0, Landroid/support/v7/widget/aj$u;->k:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method m()Z
    .locals 1

    .prologue
    .line 8450
    iget v0, p0, Landroid/support/v7/widget/aj$u;->k:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method n()Z
    .locals 1

    .prologue
    .line 8454
    iget v0, p0, Landroid/support/v7/widget/aj$u;->k:I

    and-int/lit16 v0, v0, 0x200

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/aj$u;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method o()V
    .locals 1

    .prologue
    .line 8482
    iget-object v0, p0, Landroid/support/v7/widget/aj$u;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 8483
    iget-object v0, p0, Landroid/support/v7/widget/aj$u;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 8485
    :cond_0
    iget v0, p0, Landroid/support/v7/widget/aj$u;->k:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Landroid/support/v7/widget/aj$u;->k:I

    .line 8486
    return-void
.end method

.method p()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 8489
    iget v0, p0, Landroid/support/v7/widget/aj$u;->k:I

    and-int/lit16 v0, v0, 0x400

    if-nez v0, :cond_2

    .line 8490
    iget-object v0, p0, Landroid/support/v7/widget/aj$u;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/aj$u;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 8492
    :cond_0
    sget-object v0, Landroid/support/v7/widget/aj$u;->l:Ljava/util/List;

    .line 8498
    :goto_0
    return-object v0

    .line 8495
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/aj$u;->i:Ljava/util/List;

    goto :goto_0

    .line 8498
    :cond_2
    sget-object v0, Landroid/support/v7/widget/aj$u;->l:Ljava/util/List;

    goto :goto_0
.end method

.method q()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 8503
    iput v3, p0, Landroid/support/v7/widget/aj$u;->k:I

    .line 8504
    iput v2, p0, Landroid/support/v7/widget/aj$u;->a:I

    .line 8505
    iput v2, p0, Landroid/support/v7/widget/aj$u;->b:I

    .line 8506
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroid/support/v7/widget/aj$u;->c:J

    .line 8507
    iput v2, p0, Landroid/support/v7/widget/aj$u;->e:I

    .line 8508
    iput v3, p0, Landroid/support/v7/widget/aj$u;->m:I

    .line 8509
    iput-object v4, p0, Landroid/support/v7/widget/aj$u;->f:Landroid/support/v7/widget/aj$u;

    .line 8510
    iput-object v4, p0, Landroid/support/v7/widget/aj$u;->g:Landroid/support/v7/widget/aj$u;

    .line 8511
    invoke-virtual {p0}, Landroid/support/v7/widget/aj$u;->o()V

    .line 8512
    iput v3, p0, Landroid/support/v7/widget/aj$u;->p:I

    .line 8513
    return-void
.end method

.method r()Z
    .locals 1

    .prologue
    .line 8616
    iget v0, p0, Landroid/support/v7/widget/aj$u;->k:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setIsRecyclable(Z)V
    .locals 3

    .prologue
    .line 8570
    if-eqz p1, :cond_1

    iget v0, p0, Landroid/support/v7/widget/aj$u;->m:I

    add-int/lit8 v0, v0, -0x1

    :goto_0
    iput v0, p0, Landroid/support/v7/widget/aj$u;->m:I

    .line 8571
    iget v0, p0, Landroid/support/v7/widget/aj$u;->m:I

    if-gez v0, :cond_2

    .line 8572
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/aj$u;->m:I

    .line 8577
    const-string v0, "View"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isRecyclable decremented below 0: unmatched pair of setIsRecyable() calls for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 8587
    :cond_0
    :goto_1
    return-void

    .line 8570
    :cond_1
    iget v0, p0, Landroid/support/v7/widget/aj$u;->m:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8579
    :cond_2
    if-nez p1, :cond_3

    iget v0, p0, Landroid/support/v7/widget/aj$u;->m:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 8580
    iget v0, p0, Landroid/support/v7/widget/aj$u;->k:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Landroid/support/v7/widget/aj$u;->k:I

    goto :goto_1

    .line 8581
    :cond_3
    if-eqz p1, :cond_0

    iget v0, p0, Landroid/support/v7/widget/aj$u;->m:I

    if-nez v0, :cond_0

    .line 8582
    iget v0, p0, Landroid/support/v7/widget/aj$u;->k:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Landroid/support/v7/widget/aj$u;->k:I

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 8537
    new-instance v1, Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ViewHolder{"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " position="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Landroid/support/v7/widget/aj$u;->a:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " id="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Landroid/support/v7/widget/aj$u;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", oldPos="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Landroid/support/v7/widget/aj$u;->b:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", pLpos:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Landroid/support/v7/widget/aj$u;->e:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 8540
    invoke-virtual {p0}, Landroid/support/v7/widget/aj$u;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 8541
    const-string v0, " scrap "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Landroid/support/v7/widget/aj$u;->o:Z

    if-eqz v0, :cond_a

    const-string v0, "[changeScrap]"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8544
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/aj$u;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, " invalid"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8545
    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/widget/aj$u;->k()Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, " unbound"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8546
    :cond_2
    invoke-virtual {p0}, Landroid/support/v7/widget/aj$u;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, " update"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8547
    :cond_3
    invoke-virtual {p0}, Landroid/support/v7/widget/aj$u;->l()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, " removed"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8548
    :cond_4
    invoke-virtual {p0}, Landroid/support/v7/widget/aj$u;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, " ignored"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8549
    :cond_5
    invoke-virtual {p0}, Landroid/support/v7/widget/aj$u;->m()Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, " tmpDetached"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8550
    :cond_6
    invoke-virtual {p0}, Landroid/support/v7/widget/aj$u;->isRecyclable()Z

    move-result v0

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " not recyclable("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Landroid/support/v7/widget/aj$u;->m:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8551
    :cond_7
    invoke-virtual {p0}, Landroid/support/v7/widget/aj$u;->n()Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, " undefined adapter position"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8553
    :cond_8
    iget-object v0, p0, Landroid/support/v7/widget/aj$u;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_9

    const-string v0, " no parent"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8554
    :cond_9
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8555
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 8541
    :cond_a
    const-string v0, "[attachedScrap]"

    goto/16 :goto_0
.end method
