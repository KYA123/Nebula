.class abstract Lcom/b/a/a/a/e;
.super Lcom/b/a/a/a/c;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/b/a/a/a/e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<PlayerOrIMAAd:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/b/a/a/a/c",
        "<TPlayerOrIMAAd;>;"
    }
.end annotation


# instance fields
.field protected j:Lcom/b/a/a/a/e$a;

.field protected k:I

.field protected l:D

.field protected m:I

.field protected n:I

.field private o:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/b/a/a/a/a;Lcom/b/a/a/a/s;)V
    .locals 2

    .prologue
    const/high16 v0, -0x80000000

    .line 37
    invoke-direct {p0, p1, p2, p3}, Lcom/b/a/a/a/c;-><init>(Ljava/lang/String;Lcom/b/a/a/a/a;Lcom/b/a/a/a/s;)V

    .line 38
    iput v0, p0, Lcom/b/a/a/a/e;->m:I

    .line 39
    iput v0, p0, Lcom/b/a/a/a/e;->n:I

    .line 40
    iput v0, p0, Lcom/b/a/a/a/e;->k:I

    .line 41
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    iput-wide v0, p0, Lcom/b/a/a/a/e;->l:D

    .line 42
    const/4 v0, 0x0

    iput v0, p0, Lcom/b/a/a/a/e;->o:I

    .line 43
    sget-object v0, Lcom/b/a/a/a/e$a;->a:Lcom/b/a/a/a/e$a;

    iput-object v0, p0, Lcom/b/a/a/a/e;->j:Lcom/b/a/a/a/e$a;

    .line 44
    return-void
.end method


# virtual methods
.method protected a(Lcom/b/a/a/a/h;)Lorg/json/JSONObject;
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 49
    iget-object v0, p1, Lcom/b/a/a/a/h;->c:Ljava/lang/Integer;

    sget-object v1, Lcom/b/a/a/a/h;->a:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 50
    iget-object v0, p1, Lcom/b/a/a/a/h;->c:Ljava/lang/Integer;

    .line 60
    :goto_0
    iget-object v1, p1, Lcom/b/a/a/a/h;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gez v1, :cond_0

    .line 61
    iget v0, p0, Lcom/b/a/a/a/e;->k:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 62
    iput-object v0, p1, Lcom/b/a/a/a/h;->c:Ljava/lang/Integer;

    .line 65
    :cond_0
    iget-object v1, p1, Lcom/b/a/a/a/h;->e:Lcom/b/a/a/a/i;

    sget-object v2, Lcom/b/a/a/a/i;->d:Lcom/b/a/a/a/i;

    if-ne v1, v2, :cond_2

    .line 69
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, v3, :cond_1

    iget v1, p0, Lcom/b/a/a/a/e;->n:I

    if-eq v1, v3, :cond_1

    iget v1, p0, Lcom/b/a/a/a/e;->n:I

    .line 71
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/b/a/a/a/e;->a(Ljava/lang/Integer;Ljava/lang/Integer;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 72
    :cond_1
    sget-object v0, Lcom/b/a/a/a/e$a;->d:Lcom/b/a/a/a/e$a;

    iput-object v0, p0, Lcom/b/a/a/a/e;->j:Lcom/b/a/a/a/e$a;

    .line 73
    sget-object v0, Lcom/b/a/a/a/i;->h:Lcom/b/a/a/a/i;

    iput-object v0, p1, Lcom/b/a/a/a/h;->e:Lcom/b/a/a/a/i;

    .line 79
    :cond_2
    :goto_1
    invoke-super {p0, p1}, Lcom/b/a/a/a/c;->a(Lcom/b/a/a/a/h;)Lorg/json/JSONObject;

    move-result-object v0

    return-object v0

    .line 53
    :cond_3
    :try_start_0
    invoke-virtual {p0}, Lcom/b/a/a/a/e;->f()Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 57
    :goto_2
    iput-object v0, p1, Lcom/b/a/a/a/h;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 54
    :catch_0
    move-exception v0

    .line 55
    iget v0, p0, Lcom/b/a/a/a/e;->k:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_2

    .line 75
    :cond_4
    sget-object v0, Lcom/b/a/a/a/e$a;->e:Lcom/b/a/a/a/e$a;

    iput-object v0, p0, Lcom/b/a/a/a/e;->j:Lcom/b/a/a/a/e$a;

    goto :goto_1
.end method

.method protected b()V
    .locals 4

    .prologue
    .line 94
    invoke-super {p0}, Lcom/b/a/a/a/c;->b()V

    .line 95
    new-instance v0, Lcom/b/a/a/a/e$1;

    invoke-direct {v0, p0}, Lcom/b/a/a/a/e$1;-><init>(Lcom/b/a/a/a/e;)V

    .line 114
    iget-object v1, p0, Lcom/b/a/a/a/e;->d:Landroid/os/Handler;

    const-wide/16 v2, 0xc8

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 115
    return-void
.end method

.method protected abstract f()Ljava/lang/Integer;
.end method

.method protected abstract g()Z
.end method

.method protected abstract h()Ljava/lang/Integer;
.end method

.method protected i()Z
    .locals 12

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 119
    iget-object v0, p0, Lcom/b/a/a/a/e;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/b/a/a/a/e;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 195
    :cond_0
    :goto_0
    return v3

    .line 124
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/b/a/a/a/e;->f()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 126
    iget v0, p0, Lcom/b/a/a/a/e;->k:I

    if-ltz v0, :cond_2

    if-ltz v4, :cond_0

    .line 130
    :cond_2
    iput v4, p0, Lcom/b/a/a/a/e;->k:I

    .line 133
    if-nez v4, :cond_3

    move v3, v2

    .line 134
    goto :goto_0

    .line 137
    :cond_3
    invoke-virtual {p0}, Lcom/b/a/a/a/e;->h()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 138
    invoke-virtual {p0}, Lcom/b/a/a/a/e;->g()Z

    move-result v5

    .line 139
    int-to-double v6, v0

    const-wide/high16 v8, 0x4010000000000000L    # 4.0

    div-double/2addr v6, v8

    .line 140
    invoke-virtual {p0}, Lcom/b/a/a/a/e;->d()D

    move-result-wide v8

    .line 142
    const/4 v1, 0x0

    .line 144
    iget v10, p0, Lcom/b/a/a/a/e;->m:I

    if-le v4, v10, :cond_4

    .line 145
    iput v4, p0, Lcom/b/a/a/a/e;->m:I

    .line 148
    :cond_4
    iget v10, p0, Lcom/b/a/a/a/e;->n:I

    const/high16 v11, -0x80000000

    if-ne v10, v11, :cond_5

    .line 149
    iput v0, p0, Lcom/b/a/a/a/e;->n:I

    .line 152
    :cond_5
    if-eqz v5, :cond_b

    .line 153
    iget-object v0, p0, Lcom/b/a/a/a/e;->j:Lcom/b/a/a/a/e$a;

    sget-object v5, Lcom/b/a/a/a/e$a;->a:Lcom/b/a/a/a/e$a;

    if-ne v0, v5, :cond_9

    .line 154
    sget-object v1, Lcom/b/a/a/a/i;->g:Lcom/b/a/a/a/i;

    .line 155
    sget-object v0, Lcom/b/a/a/a/e$a;->c:Lcom/b/a/a/a/e$a;

    iput-object v0, p0, Lcom/b/a/a/a/e;->j:Lcom/b/a/a/a/e$a;

    .line 176
    :cond_6
    :goto_1
    if-eqz v1, :cond_c

    move v0, v2

    .line 178
    :goto_2
    if-nez v0, :cond_7

    iget-wide v6, p0, Lcom/b/a/a/a/e;->l:D

    invoke-static {v6, v7}, Ljava/lang/Double;->isNaN(D)Z

    move-result v5

    if-nez v5, :cond_7

    iget-wide v6, p0, Lcom/b/a/a/a/e;->l:D

    sub-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    const-wide v10, 0x3fa999999999999aL    # 0.05

    cmpl-double v5, v6, v10

    if-lez v5, :cond_7

    .line 179
    sget-object v0, Lcom/b/a/a/a/i;->j:Lcom/b/a/a/a/i;

    move-object v1, v0

    move v0, v2

    .line 183
    :cond_7
    if-eqz v0, :cond_8

    .line 184
    new-instance v0, Lcom/b/a/a/a/h;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-direct {v0, v1, v4, v5}, Lcom/b/a/a/a/h;-><init>(Lcom/b/a/a/a/i;Ljava/lang/Integer;Ljava/lang/Double;)V

    invoke-virtual {p0, v0}, Lcom/b/a/a/a/e;->b(Lcom/b/a/a/a/h;)V

    .line 187
    :cond_8
    iput-wide v8, p0, Lcom/b/a/a/a/e;->l:D

    .line 188
    const/4 v0, 0x0

    iput v0, p0, Lcom/b/a/a/a/e;->o:I

    move v3, v2

    .line 190
    goto :goto_0

    .line 156
    :cond_9
    iget-object v0, p0, Lcom/b/a/a/a/e;->j:Lcom/b/a/a/a/e$a;

    sget-object v5, Lcom/b/a/a/a/e$a;->b:Lcom/b/a/a/a/e$a;

    if-ne v0, v5, :cond_a

    .line 157
    sget-object v1, Lcom/b/a/a/a/i;->f:Lcom/b/a/a/a/i;

    .line 158
    sget-object v0, Lcom/b/a/a/a/e$a;->c:Lcom/b/a/a/a/e$a;

    iput-object v0, p0, Lcom/b/a/a/a/e;->j:Lcom/b/a/a/a/e$a;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 191
    :catch_0
    move-exception v0

    .line 195
    iget v0, p0, Lcom/b/a/a/a/e;->o:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/b/a/a/a/e;->o:I

    const/4 v1, 0x5

    if-ge v0, v1, :cond_d

    move v0, v2

    :goto_3
    move v3, v0

    goto/16 :goto_0

    .line 160
    :cond_a
    int-to-double v10, v4

    div-double v6, v10, v6

    :try_start_1
    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    double-to-int v0, v6

    add-int/lit8 v0, v0, -0x1

    .line 161
    const/4 v5, -0x1

    if-le v0, v5, :cond_e

    const/4 v5, 0x3

    if-ge v0, v5, :cond_e

    .line 162
    sget-object v5, Lcom/b/a/a/a/e;->b:[Lcom/b/a/a/a/i;

    aget-object v0, v5, v0

    .line 163
    iget-object v5, p0, Lcom/b/a/a/a/e;->c:Ljava/util/Map;

    invoke-interface {v5, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_e

    .line 165
    iget-object v1, p0, Lcom/b/a/a/a/e;->c:Ljava/util/Map;

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v1, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_4
    move-object v1, v0

    .line 168
    goto :goto_1

    .line 170
    :cond_b
    iget-object v0, p0, Lcom/b/a/a/a/e;->j:Lcom/b/a/a/a/e$a;

    sget-object v5, Lcom/b/a/a/a/e$a;->b:Lcom/b/a/a/a/e$a;

    if-eq v0, v5, :cond_6

    .line 171
    sget-object v1, Lcom/b/a/a/a/i;->e:Lcom/b/a/a/a/i;

    .line 172
    sget-object v0, Lcom/b/a/a/a/e$a;->b:Lcom/b/a/a/a/e$a;

    iput-object v0, p0, Lcom/b/a/a/a/e;->j:Lcom/b/a/a/a/e$a;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    :cond_c
    move v0, v3

    .line 176
    goto/16 :goto_2

    :cond_d
    move v0, v3

    .line 195
    goto :goto_3

    :cond_e
    move-object v0, v1

    goto :goto_4
.end method
