.class public Lcom/facebook/FacebookActivity;
.super Landroid/support/v4/a/p;
.source "SourceFile"


# static fields
.field public static a:Ljava/lang/String;

.field private static b:Ljava/lang/String;

.field private static final c:Ljava/lang/String;


# instance fields
.field private d:Landroid/support/v4/a/o;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-string v0, "PassThrough"

    sput-object v0, Lcom/facebook/FacebookActivity;->a:Ljava/lang/String;

    .line 54
    const-string v0, "SingleFragment"

    sput-object v0, Lcom/facebook/FacebookActivity;->b:Ljava/lang/String;

    .line 55
    const-class v0, Lcom/facebook/FacebookActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/FacebookActivity;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Landroid/support/v4/a/p;-><init>()V

    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/facebook/FacebookActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 132
    invoke-static {v0}, Lcom/facebook/internal/y;->d(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v0

    .line 133
    invoke-static {v0}, Lcom/facebook/internal/y;->a(Landroid/os/Bundle;)Lcom/facebook/l;

    move-result-object v0

    .line 136
    invoke-virtual {p0}, Lcom/facebook/FacebookActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x0

    .line 135
    invoke-static {v1, v2, v0}, Lcom/facebook/internal/y;->a(Landroid/content/Intent;Landroid/os/Bundle;Lcom/facebook/l;)Landroid/content/Intent;

    move-result-object v0

    .line 139
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/FacebookActivity;->setResult(ILandroid/content/Intent;)V

    .line 140
    invoke-virtual {p0}, Lcom/facebook/FacebookActivity;->finish()V

    .line 141
    return-void
.end method


# virtual methods
.method protected a()Landroid/support/v4/a/o;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 87
    invoke-virtual {p0}, Lcom/facebook/FacebookActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 88
    invoke-virtual {p0}, Lcom/facebook/FacebookActivity;->getSupportFragmentManager()Landroid/support/v4/a/t;

    move-result-object v3

    .line 89
    sget-object v0, Lcom/facebook/FacebookActivity;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/support/v4/a/t;->a(Ljava/lang/String;)Landroid/support/v4/a/o;

    move-result-object v0

    .line 91
    if-nez v0, :cond_0

    .line 92
    const-string v0, "FacebookDialogFragment"

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 93
    new-instance v0, Lcom/facebook/internal/k;

    invoke-direct {v0}, Lcom/facebook/internal/k;-><init>()V

    .line 94
    invoke-virtual {v0, v4}, Lcom/facebook/internal/k;->setRetainInstance(Z)V

    .line 95
    sget-object v1, Lcom/facebook/FacebookActivity;->b:Ljava/lang/String;

    invoke-virtual {v0, v3, v1}, Lcom/facebook/internal/k;->show(Landroid/support/v4/a/t;Ljava/lang/String;)V

    .line 112
    :cond_0
    :goto_0
    return-object v0

    .line 98
    :cond_1
    const-string v0, "DeviceShareDialogFragment"

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 99
    new-instance v1, Lcom/facebook/share/a/b;

    invoke-direct {v1}, Lcom/facebook/share/a/b;-><init>()V

    .line 100
    invoke-virtual {v1, v4}, Lcom/facebook/share/a/b;->setRetainInstance(Z)V

    .line 101
    const-string v0, "content"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/share/b/b;

    invoke-virtual {v1, v0}, Lcom/facebook/share/a/b;->a(Lcom/facebook/share/b/b;)V

    .line 102
    sget-object v0, Lcom/facebook/FacebookActivity;->b:Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Lcom/facebook/share/a/b;->show(Landroid/support/v4/a/t;Ljava/lang/String;)V

    move-object v0, v1

    .line 104
    goto :goto_0

    .line 105
    :cond_2
    new-instance v0, Lcom/facebook/login/k;

    invoke-direct {v0}, Lcom/facebook/login/k;-><init>()V

    .line 106
    invoke-virtual {v0, v4}, Landroid/support/v4/a/o;->setRetainInstance(Z)V

    .line 107
    invoke-virtual {v3}, Landroid/support/v4/a/t;->a()Landroid/support/v4/a/y;

    move-result-object v1

    sget v2, Lcom/facebook/ae$d;->com_facebook_fragment_container:I

    sget-object v3, Lcom/facebook/FacebookActivity;->b:Ljava/lang/String;

    .line 108
    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/a/y;->a(ILandroid/support/v4/a/o;Ljava/lang/String;)Landroid/support/v4/a/y;

    move-result-object v1

    .line 109
    invoke-virtual {v1}, Landroid/support/v4/a/y;->a()I

    goto :goto_0
.end method

.method public b()Landroid/support/v4/a/o;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/facebook/FacebookActivity;->d:Landroid/support/v4/a/o;

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 117
    invoke-super {p0, p1}, Landroid/support/v4/a/p;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 119
    iget-object v0, p0, Lcom/facebook/FacebookActivity;->d:Landroid/support/v4/a/o;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/facebook/FacebookActivity;->d:Landroid/support/v4/a/o;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/o;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 122
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 61
    invoke-super {p0, p1}, Landroid/support/v4/a/p;->onCreate(Landroid/os/Bundle;)V

    .line 62
    invoke-virtual {p0}, Lcom/facebook/FacebookActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 68
    invoke-static {}, Lcom/facebook/p;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 69
    sget-object v1, Lcom/facebook/FacebookActivity;->c:Ljava/lang/String;

    const-string v2, "Facebook SDK not initialized. Make sure you call sdkInitialize inside your Application\'s onCreate method."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    invoke-virtual {p0}, Lcom/facebook/FacebookActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/p;->a(Landroid/content/Context;)V

    .line 76
    :cond_0
    sget v1, Lcom/facebook/ae$e;->com_facebook_activity_layout:I

    invoke-virtual {p0, v1}, Lcom/facebook/FacebookActivity;->setContentView(I)V

    .line 78
    sget-object v1, Lcom/facebook/FacebookActivity;->a:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 79
    invoke-direct {p0}, Lcom/facebook/FacebookActivity;->c()V

    .line 84
    :goto_0
    return-void

    .line 83
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/FacebookActivity;->a()Landroid/support/v4/a/o;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/FacebookActivity;->d:Landroid/support/v4/a/o;

    goto :goto_0
.end method
