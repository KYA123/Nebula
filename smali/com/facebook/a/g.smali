.class public Lcom/facebook/a/g;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/facebook/a/g$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static d:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

.field private static e:Lcom/facebook/a/g$a;

.field private static f:Ljava/lang/Object;

.field private static g:Ljava/lang/String;

.field private static h:Z

.field private static i:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Lcom/facebook/a/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 167
    const-class v0, Lcom/facebook/a/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/a/g;->a:Ljava/lang/String;

    .line 189
    sget-object v0, Lcom/facebook/a/g$a;->a:Lcom/facebook/a/g$a;

    sput-object v0, Lcom/facebook/a/g;->e:Lcom/facebook/a/g$a;

    .line 190
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/facebook/a/g;->f:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/a;)V
    .locals 1

    .prologue
    .line 837
    .line 838
    invoke-static {p1}, Lcom/facebook/internal/ad;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 837
    invoke-direct {p0, v0, p2, p3}, Lcom/facebook/a/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/a;)V

    .line 841
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/a;)V
    .locals 2

    .prologue
    .line 846
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 847
    invoke-static {}, Lcom/facebook/internal/ae;->a()V

    .line 848
    iput-object p1, p0, Lcom/facebook/a/g;->b:Ljava/lang/String;

    .line 850
    if-nez p3, :cond_0

    .line 851
    invoke-static {}, Lcom/facebook/a;->a()Lcom/facebook/a;

    move-result-object p3

    .line 855
    :cond_0
    if-eqz p3, :cond_2

    if-eqz p2, :cond_1

    .line 856
    invoke-virtual {p3}, Lcom/facebook/a;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 858
    :cond_1
    new-instance v0, Lcom/facebook/a/a;

    invoke-direct {v0, p3}, Lcom/facebook/a/a;-><init>(Lcom/facebook/a;)V

    iput-object v0, p0, Lcom/facebook/a/g;->c:Lcom/facebook/a/a;

    .line 868
    :goto_0
    invoke-static {}, Lcom/facebook/a/g;->h()V

    .line 869
    return-void

    .line 861
    :cond_2
    if-nez p2, :cond_3

    .line 863
    invoke-static {}, Lcom/facebook/p;->f()Landroid/content/Context;

    move-result-object v0

    .line 862
    invoke-static {v0}, Lcom/facebook/internal/ad;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p2

    .line 865
    :cond_3
    new-instance v0, Lcom/facebook/a/a;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p2}, Lcom/facebook/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/a/g;->c:Lcom/facebook/a/a;

    goto :goto_0
.end method

.method public static a()Lcom/facebook/a/g$a;
    .locals 2

    .prologue
    .line 471
    sget-object v1, Lcom/facebook/a/g;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 472
    :try_start_0
    sget-object v0, Lcom/facebook/a/g;->e:Lcom/facebook/a/g$a;

    monitor-exit v1

    return-object v0

    .line 473
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Landroid/content/Context;)Lcom/facebook/a/g;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 404
    new-instance v0, Lcom/facebook/a/g;

    invoke-direct {v0, p0, v1, v1}, Lcom/facebook/a/g;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/a;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Lcom/facebook/a/g;
    .locals 2

    .prologue
    .line 449
    new-instance v0, Lcom/facebook/a/g;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/facebook/a/g;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/a;)V

    return-object v0
.end method

.method public static a(Landroid/app/Application;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 225
    invoke-static {}, Lcom/facebook/p;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 226
    new-instance v0, Lcom/facebook/l;

    const-string v1, "The Facebook sdk must be initialized before calling activateApp"

    invoke-direct {v0, v1}, Lcom/facebook/l;-><init>(Ljava/lang/String;)V

    throw v0

    .line 230
    :cond_0
    invoke-static {}, Lcom/facebook/a/b;->a()V

    .line 232
    if-nez p1, :cond_1

    .line 233
    invoke-static {}, Lcom/facebook/p;->j()Ljava/lang/String;

    move-result-object p1

    .line 239
    :cond_1
    invoke-static {p0, p1}, Lcom/facebook/p;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 241
    invoke-static {p0, p1}, Lcom/facebook/a/a/a;->a(Landroid/app/Application;Ljava/lang/String;)V

    .line 242
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/facebook/a/c;Lcom/facebook/a/a;)V
    .locals 3

    .prologue
    .line 932
    invoke-static {p2, p1}, Lcom/facebook/a/e;->a(Lcom/facebook/a/a;Lcom/facebook/a/c;)V

    .line 935
    invoke-virtual {p1}, Lcom/facebook/a/c;->b()Z

    move-result v0

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/facebook/a/g;->h:Z

    if-nez v0, :cond_0

    .line 936
    invoke-virtual {p1}, Lcom/facebook/a/c;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "fb_mobile_activate_app"

    if-ne v0, v1, :cond_1

    .line 937
    const/4 v0, 0x1

    sput-boolean v0, Lcom/facebook/a/g;->h:Z

    .line 946
    :cond_0
    :goto_0
    return-void

    .line 939
    :cond_1
    sget-object v0, Lcom/facebook/y;->e:Lcom/facebook/y;

    const-string v1, "AppEvents"

    const-string v2, "Warning: Please call AppEventsLogger.activateApp(...)from the long-lived activity\'s onResume() methodbefore logging other app events."

    invoke-static {v0, v1, v2}, Lcom/facebook/internal/v;->a(Lcom/facebook/y;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/Double;Landroid/os/Bundle;ZLjava/util/UUID;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 908
    :try_start_0
    new-instance v0, Lcom/facebook/a/c;

    iget-object v1, p0, Lcom/facebook/a/g;->b:Ljava/lang/String;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Double;Landroid/os/Bundle;ZLjava/util/UUID;)V

    .line 915
    invoke-static {}, Lcom/facebook/p;->f()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/a/g;->c:Lcom/facebook/a/a;

    invoke-static {v1, v0, v2}, Lcom/facebook/a/g;->a(Landroid/content/Context;Lcom/facebook/a/c;Lcom/facebook/a/a;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/facebook/l; {:try_start_0 .. :try_end_0} :catch_1

    .line 927
    :goto_0
    return-void

    .line 916
    :catch_0
    move-exception v0

    .line 918
    sget-object v1, Lcom/facebook/y;->e:Lcom/facebook/y;

    const-string v2, "AppEvents"

    const-string v3, "JSON encoding for app event failed: \'%s\'"

    new-array v4, v8, [Ljava/lang/Object;

    .line 919
    invoke-virtual {v0}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    .line 918
    invoke-static {v1, v2, v3, v4}, Lcom/facebook/internal/v;->a(Lcom/facebook/y;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 921
    :catch_1
    move-exception v0

    .line 923
    sget-object v1, Lcom/facebook/y;->e:Lcom/facebook/y;

    const-string v2, "AppEvents"

    const-string v3, "Invalid app event: %s"

    new-array v4, v8, [Ljava/lang/Object;

    .line 924
    invoke-virtual {v0}, Lcom/facebook/l;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    .line 923
    invoke-static {v1, v2, v3, v4}, Lcom/facebook/internal/v;->a(Lcom/facebook/y;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1051
    sget-object v0, Lcom/facebook/a/g;->g:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1052
    sget-object v1, Lcom/facebook/a/g;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 1053
    :try_start_0
    sget-object v0, Lcom/facebook/a/g;->g:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1055
    const-string v0, "com.facebook.sdk.appEventPreferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1058
    const-string v2, "anonymousAppDeviceGUID"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/a/g;->g:Ljava/lang/String;

    .line 1059
    sget-object v0, Lcom/facebook/a/g;->g:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1061
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "XZ"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/a/g;->g:Ljava/lang/String;

    .line 1063
    const-string v0, "com.facebook.sdk.appEventPreferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1064
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "anonymousAppDeviceGUID"

    sget-object v3, Lcom/facebook/a/g;->g:Ljava/lang/String;

    .line 1065
    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1066
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1069
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1072
    :cond_1
    sget-object v0, Lcom/facebook/a/g;->g:Ljava/lang/String;

    return-object v0

    .line 1069
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 960
    sget-object v0, Lcom/facebook/y;->f:Lcom/facebook/y;

    const-string v1, "AppEvents"

    invoke-static {v0, v1, p0}, Lcom/facebook/internal/v;->a(Lcom/facebook/y;Ljava/lang/String;Ljava/lang/String;)V

    .line 961
    return-void
.end method

.method public static c()V
    .locals 0

    .prologue
    .line 678
    invoke-static {}, Lcom/facebook/a/e;->a()V

    .line 679
    return-void
.end method

.method static d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 714
    sget-object v1, Lcom/facebook/a/g;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 715
    :try_start_0
    sget-object v0, Lcom/facebook/a/g;->i:Ljava/lang/String;

    monitor-exit v1

    return-object v0

    .line 716
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 735
    invoke-static {}, Lcom/facebook/a/b;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static f()V
    .locals 2

    .prologue
    .line 949
    invoke-static {}, Lcom/facebook/a/g;->a()Lcom/facebook/a/g$a;

    move-result-object v0

    sget-object v1, Lcom/facebook/a/g$a;->b:Lcom/facebook/a/g$a;

    if-eq v0, v1, :cond_0

    .line 950
    sget-object v0, Lcom/facebook/a/h;->f:Lcom/facebook/a/h;

    invoke-static {v0}, Lcom/facebook/a/e;->a(Lcom/facebook/a/h;)V

    .line 952
    :cond_0
    return-void
.end method

.method static g()Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 1036
    sget-object v0, Lcom/facebook/a/g;->d:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    if-nez v0, :cond_0

    .line 1037
    invoke-static {}, Lcom/facebook/a/g;->h()V

    .line 1040
    :cond_0
    sget-object v0, Lcom/facebook/a/g;->d:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    return-object v0
.end method

.method private static h()V
    .locals 7

    .prologue
    .line 872
    sget-object v1, Lcom/facebook/a/g;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 873
    :try_start_0
    sget-object v0, Lcom/facebook/a/g;->d:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    if-eqz v0, :cond_0

    .line 874
    monitor-exit v1

    .line 899
    :goto_0
    return-void

    .line 876
    :cond_0
    new-instance v0, Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(I)V

    sput-object v0, Lcom/facebook/a/g;->d:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    .line 877
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 879
    new-instance v1, Lcom/facebook/a/g$1;

    invoke-direct {v1}, Lcom/facebook/a/g$1;-><init>()V

    .line 893
    sget-object v0, Lcom/facebook/a/g;->d:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    const-wide/16 v2, 0x0

    const-wide/32 v4, 0x15180

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v0 .. v6}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0

    .line 877
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 499
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/a/g;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 500
    return-void
.end method

.method public a(Ljava/lang/String;DLandroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 569
    .line 571
    invoke-static {p2, p3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/4 v4, 0x0

    .line 574
    invoke-static {}, Lcom/facebook/a/a/a;->a()Ljava/util/UUID;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v3, p4

    .line 569
    invoke-direct/range {v0 .. v5}, Lcom/facebook/a/g;->a(Ljava/lang/String;Ljava/lang/Double;Landroid/os/Bundle;ZLjava/util/UUID;)V

    .line 575
    return-void
.end method

.method public a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 539
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 544
    invoke-static {}, Lcom/facebook/a/a/a;->a()Ljava/util/UUID;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    .line 539
    invoke-direct/range {v0 .. v5}, Lcom/facebook/a/g;->a(Ljava/lang/String;Ljava/lang/Double;Landroid/os/Bundle;ZLjava/util/UUID;)V

    .line 545
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/Double;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 812
    const/4 v4, 0x1

    .line 817
    invoke-static {}, Lcom/facebook/a/a/a;->a()Ljava/util/UUID;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    .line 812
    invoke-direct/range {v0 .. v5}, Lcom/facebook/a/g;->a(Ljava/lang/String;Ljava/lang/Double;Landroid/os/Bundle;ZLjava/util/UUID;)V

    .line 818
    return-void
.end method

.method public a(Ljava/math/BigDecimal;Ljava/util/Currency;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 605
    if-nez p1, :cond_0

    .line 606
    const-string v0, "purchaseAmount cannot be null"

    invoke-static {v0}, Lcom/facebook/a/g;->b(Ljava/lang/String;)V

    .line 620
    :goto_0
    return-void

    .line 608
    :cond_0
    if-nez p2, :cond_1

    .line 609
    const-string v0, "currency cannot be null"

    invoke-static {v0}, Lcom/facebook/a/g;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 613
    :cond_1
    if-nez p3, :cond_2

    .line 614
    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    .line 616
    :cond_2
    const-string v0, "fb_currency"

    invoke-virtual {p2}, Ljava/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    const-string v0, "fb_mobile_purchase"

    invoke-virtual {p1}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3, p3}, Lcom/facebook/a/g;->a(Ljava/lang/String;DLandroid/os/Bundle;)V

    .line 619
    invoke-static {}, Lcom/facebook/a/g;->f()V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 666
    sget-object v0, Lcom/facebook/a/h;->a:Lcom/facebook/a/h;

    invoke-static {v0}, Lcom/facebook/a/e;->a(Lcom/facebook/a/h;)V

    .line 667
    return-void
.end method
