.class public final Lcom/facebook/ae$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/facebook/ae;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# static fields
.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f0c00fc

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f0c00fd

.field public static final abc_btn_colored_borderless_text_material:I = 0x7f0c00fe

.field public static final abc_btn_colored_text_material:I = 0x7f0c00ff

.field public static final abc_color_highlight_material:I = 0x7f0c0100

.field public static final abc_hint_foreground_material_dark:I = 0x7f0c0101

.field public static final abc_hint_foreground_material_light:I = 0x7f0c0102

.field public static final abc_input_method_navigation_guard:I = 0x7f0c0095

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f0c0103

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f0c0104

.field public static final abc_primary_text_material_dark:I = 0x7f0c0105

.field public static final abc_primary_text_material_light:I = 0x7f0c0106

.field public static final abc_search_url_text:I = 0x7f0c0107

.field public static final abc_search_url_text_normal:I = 0x7f0c0096

.field public static final abc_search_url_text_pressed:I = 0x7f0c0097

.field public static final abc_search_url_text_selected:I = 0x7f0c0098

.field public static final abc_secondary_text_material_dark:I = 0x7f0c0108

.field public static final abc_secondary_text_material_light:I = 0x7f0c0109

.field public static final abc_tint_btn_checkable:I = 0x7f0c010a

.field public static final abc_tint_default:I = 0x7f0c010b

.field public static final abc_tint_edittext:I = 0x7f0c010c

.field public static final abc_tint_seek_thumb:I = 0x7f0c010d

.field public static final abc_tint_spinner:I = 0x7f0c010e

.field public static final abc_tint_switch_thumb:I = 0x7f0c010f

.field public static final abc_tint_switch_track:I = 0x7f0c0110

.field public static final accent_material_dark:I = 0x7f0c0099

.field public static final accent_material_light:I = 0x7f0c009a

.field public static final background_floating_material_dark:I = 0x7f0c009b

.field public static final background_floating_material_light:I = 0x7f0c009c

.field public static final background_material_dark:I = 0x7f0c009d

.field public static final background_material_light:I = 0x7f0c009e

.field public static final bright_foreground_disabled_material_dark:I = 0x7f0c009f

.field public static final bright_foreground_disabled_material_light:I = 0x7f0c00a0

.field public static final bright_foreground_inverse_material_dark:I = 0x7f0c00a1

.field public static final bright_foreground_inverse_material_light:I = 0x7f0c00a2

.field public static final bright_foreground_material_dark:I = 0x7f0c00a3

.field public static final bright_foreground_material_light:I = 0x7f0c00a4

.field public static final button_material_dark:I = 0x7f0c00a5

.field public static final button_material_light:I = 0x7f0c00a6

.field public static final cardview_dark_background:I = 0x7f0c00a7

.field public static final cardview_light_background:I = 0x7f0c00a8

.field public static final cardview_shadow_end_color:I = 0x7f0c00a9

.field public static final cardview_shadow_start_color:I = 0x7f0c00aa

.field public static final com_facebook_blue:I = 0x7f0c00ab

.field public static final com_facebook_button_background_color:I = 0x7f0c00ac

.field public static final com_facebook_button_background_color_disabled:I = 0x7f0c00ad

.field public static final com_facebook_button_background_color_focused:I = 0x7f0c00ae

.field public static final com_facebook_button_background_color_focused_disabled:I = 0x7f0c00af

.field public static final com_facebook_button_background_color_pressed:I = 0x7f0c00b0

.field public static final com_facebook_button_background_color_selected:I = 0x7f0c00b1

.field public static final com_facebook_button_border_color_focused:I = 0x7f0c00b2

.field public static final com_facebook_button_login_background_color:I = 0x7f0c00b3

.field public static final com_facebook_button_login_silver_background_color:I = 0x7f0c00b4

.field public static final com_facebook_button_login_silver_background_color_pressed:I = 0x7f0c00b5

.field public static final com_facebook_button_send_background_color:I = 0x7f0c00b6

.field public static final com_facebook_button_send_background_color_pressed:I = 0x7f0c00b7

.field public static final com_facebook_button_text_color:I = 0x7f0c0111

.field public static final com_facebook_device_auth_text:I = 0x7f0c00b8

.field public static final com_facebook_likeboxcountview_border_color:I = 0x7f0c00b9

.field public static final com_facebook_likeboxcountview_text_color:I = 0x7f0c00ba

.field public static final com_facebook_likeview_text_color:I = 0x7f0c00bb

.field public static final com_facebook_messenger_blue:I = 0x7f0c00bc

.field public static final com_facebook_send_button_text_color:I = 0x7f0c0112

.field public static final com_facebook_share_button_text_color:I = 0x7f0c00bd

.field public static final com_smart_login_code:I = 0x7f0c00be

.field public static final dim_foreground_disabled_material_dark:I = 0x7f0c00bf

.field public static final dim_foreground_disabled_material_light:I = 0x7f0c00c0

.field public static final dim_foreground_material_dark:I = 0x7f0c00c1

.field public static final dim_foreground_material_light:I = 0x7f0c00c2

.field public static final foreground_material_dark:I = 0x7f0c00c3

.field public static final foreground_material_light:I = 0x7f0c00c4

.field public static final highlighted_text_material_dark:I = 0x7f0c00c5

.field public static final highlighted_text_material_light:I = 0x7f0c00c6

.field public static final material_blue_grey_800:I = 0x7f0c00c7

.field public static final material_blue_grey_900:I = 0x7f0c00c8

.field public static final material_blue_grey_950:I = 0x7f0c00c9

.field public static final material_deep_teal_200:I = 0x7f0c00ca

.field public static final material_deep_teal_500:I = 0x7f0c00cb

.field public static final material_grey_100:I = 0x7f0c00cc

.field public static final material_grey_300:I = 0x7f0c00cd

.field public static final material_grey_50:I = 0x7f0c00ce

.field public static final material_grey_600:I = 0x7f0c00cf

.field public static final material_grey_800:I = 0x7f0c00d0

.field public static final material_grey_850:I = 0x7f0c00d1

.field public static final material_grey_900:I = 0x7f0c00d2

.field public static final notification_action_color_filter:I = 0x7f0c0000

.field public static final notification_icon_bg_color:I = 0x7f0c00e5

.field public static final notification_material_background_media_default_color:I = 0x7f0c00e6

.field public static final primary_dark_material_dark:I = 0x7f0c00e7

.field public static final primary_dark_material_light:I = 0x7f0c00e8

.field public static final primary_material_dark:I = 0x7f0c00e9

.field public static final primary_material_light:I = 0x7f0c00ea

.field public static final primary_text_default_material_dark:I = 0x7f0c00eb

.field public static final primary_text_default_material_light:I = 0x7f0c00ec

.field public static final primary_text_disabled_material_dark:I = 0x7f0c00ed

.field public static final primary_text_disabled_material_light:I = 0x7f0c00ee

.field public static final ripple_material_dark:I = 0x7f0c00ef

.field public static final ripple_material_light:I = 0x7f0c00f0

.field public static final secondary_text_default_material_dark:I = 0x7f0c00f1

.field public static final secondary_text_default_material_light:I = 0x7f0c00f2

.field public static final secondary_text_disabled_material_dark:I = 0x7f0c00f3

.field public static final secondary_text_disabled_material_light:I = 0x7f0c00f4

.field public static final switch_thumb_disabled_material_dark:I = 0x7f0c00f5

.field public static final switch_thumb_disabled_material_light:I = 0x7f0c00f6

.field public static final switch_thumb_material_dark:I = 0x7f0c0116

.field public static final switch_thumb_material_light:I = 0x7f0c0117

.field public static final switch_thumb_normal_material_dark:I = 0x7f0c00f7

.field public static final switch_thumb_normal_material_light:I = 0x7f0c00f8
