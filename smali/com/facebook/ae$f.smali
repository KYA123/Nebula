.class public final Lcom/facebook/ae$f;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/facebook/ae;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "f"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f080341

.field public static final abc_action_bar_home_description_format:I = 0x7f080342

.field public static final abc_action_bar_home_subtitle_description_format:I = 0x7f080343

.field public static final abc_action_bar_up_description:I = 0x7f080344

.field public static final abc_action_menu_overflow_description:I = 0x7f080345

.field public static final abc_action_mode_done:I = 0x7f080346

.field public static final abc_activity_chooser_view_see_all:I = 0x7f080347

.field public static final abc_activitychooserview_choose_application:I = 0x7f080348

.field public static final abc_capital_off:I = 0x7f080349

.field public static final abc_capital_on:I = 0x7f08034a

.field public static final abc_font_family_body_1_material:I = 0x7f0803d2

.field public static final abc_font_family_body_2_material:I = 0x7f0803d3

.field public static final abc_font_family_button_material:I = 0x7f0803d4

.field public static final abc_font_family_caption_material:I = 0x7f0803d5

.field public static final abc_font_family_display_1_material:I = 0x7f0803d6

.field public static final abc_font_family_display_2_material:I = 0x7f0803d7

.field public static final abc_font_family_display_3_material:I = 0x7f0803d8

.field public static final abc_font_family_display_4_material:I = 0x7f0803d9

.field public static final abc_font_family_headline_material:I = 0x7f0803da

.field public static final abc_font_family_menu_material:I = 0x7f0803db

.field public static final abc_font_family_subhead_material:I = 0x7f0803dc

.field public static final abc_font_family_title_material:I = 0x7f0803dd

.field public static final abc_search_hint:I = 0x7f08034b

.field public static final abc_searchview_description_clear:I = 0x7f08034c

.field public static final abc_searchview_description_query:I = 0x7f08034d

.field public static final abc_searchview_description_search:I = 0x7f08034e

.field public static final abc_searchview_description_submit:I = 0x7f08034f

.field public static final abc_searchview_description_voice:I = 0x7f080350

.field public static final abc_shareactionprovider_share_with:I = 0x7f080351

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f080352

.field public static final abc_toolbar_collapse_description:I = 0x7f080353

.field public static final com_facebook_device_auth_instructions:I = 0x7f080358

.field public static final com_facebook_image_download_unknown_error:I = 0x7f080359

.field public static final com_facebook_internet_permission_error_message:I = 0x7f08035a

.field public static final com_facebook_internet_permission_error_title:I = 0x7f08035b

.field public static final com_facebook_like_button_liked:I = 0x7f08035c

.field public static final com_facebook_like_button_not_liked:I = 0x7f08035d

.field public static final com_facebook_loading:I = 0x7f08035e

.field public static final com_facebook_loginview_cancel_action:I = 0x7f08035f

.field public static final com_facebook_loginview_log_in_button:I = 0x7f080360

.field public static final com_facebook_loginview_log_in_button_continue:I = 0x7f080361

.field public static final com_facebook_loginview_log_in_button_continue_f1gender:I = 0x7f0803b6

.field public static final com_facebook_loginview_log_in_button_continue_m2gender:I = 0x7f0803b7

.field public static final com_facebook_loginview_log_in_button_long:I = 0x7f080362

.field public static final com_facebook_loginview_log_out_action:I = 0x7f080363

.field public static final com_facebook_loginview_log_out_action_f1gender:I = 0x7f0803b8

.field public static final com_facebook_loginview_log_out_action_m2gender:I = 0x7f0803b9

.field public static final com_facebook_loginview_log_out_button:I = 0x7f080364

.field public static final com_facebook_loginview_log_out_button_f1gender:I = 0x7f0803ba

.field public static final com_facebook_loginview_log_out_button_m2gender:I = 0x7f0803bb

.field public static final com_facebook_loginview_logged_in_as:I = 0x7f080365

.field public static final com_facebook_loginview_logged_in_as_f1gender:I = 0x7f0803bc

.field public static final com_facebook_loginview_logged_in_as_m2gender:I = 0x7f0803bd

.field public static final com_facebook_loginview_logged_in_using_facebook:I = 0x7f080366

.field public static final com_facebook_loginview_logged_in_using_facebook_f1gender:I = 0x7f0803b4

.field public static final com_facebook_loginview_logged_in_using_facebook_m2gender:I = 0x7f0803b5

.field public static final com_facebook_send_button_text:I = 0x7f080367

.field public static final com_facebook_send_button_text_f1gender:I = 0x7f0803be

.field public static final com_facebook_send_button_text_m2gender:I = 0x7f0803bf

.field public static final com_facebook_share_button_text:I = 0x7f080368

.field public static final com_facebook_share_button_text_f1gender:I = 0x7f0803c0

.field public static final com_facebook_share_button_text_m2gender:I = 0x7f0803c1

.field public static final com_facebook_smart_device_instructions:I = 0x7f080369

.field public static final com_facebook_smart_device_instructions_or:I = 0x7f08036a

.field public static final com_facebook_smart_login_confirmation_cancel:I = 0x7f08036b

.field public static final com_facebook_smart_login_confirmation_cancel_f1gender:I = 0x7f0803c2

.field public static final com_facebook_smart_login_confirmation_cancel_m2gender:I = 0x7f0803c3

.field public static final com_facebook_smart_login_confirmation_continue_as:I = 0x7f08036c

.field public static final com_facebook_smart_login_confirmation_continue_as_f1gender:I = 0x7f0803c4

.field public static final com_facebook_smart_login_confirmation_continue_as_m2gender:I = 0x7f0803c5

.field public static final com_facebook_smart_login_confirmation_title:I = 0x7f08036d

.field public static final com_facebook_smart_login_confirmation_title_f1gender:I = 0x7f0803c6

.field public static final com_facebook_smart_login_confirmation_title_m2gender:I = 0x7f0803c7

.field public static final com_facebook_tooltip_default:I = 0x7f08036e

.field public static final com_facebook_tooltip_default_f1gender:I = 0x7f0803c8

.field public static final com_facebook_tooltip_default_m2gender:I = 0x7f0803c9

.field public static final messenger_send_button_text:I = 0x7f08038f

.field public static final search_menu_title:I = 0x7f08039f

.field public static final status_bar_notification_info_overflow:I = 0x7f0803a2
