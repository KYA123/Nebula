.class public abstract Lcom/facebook/internal/j;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/facebook/internal/j$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<CONTENT:",
        "Ljava/lang/Object;",
        "RESU",
        "LT:Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field protected static final a:Ljava/lang/Object;


# instance fields
.field private final b:Landroid/app/Activity;

.field private final c:Lcom/facebook/internal/r;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/internal/j",
            "<TCONTENT;TRESU",
            "LT;",
            ">.a;>;"
        }
    .end annotation
.end field

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/facebook/internal/j;->a:Ljava/lang/Object;

    return-void
.end method

.method protected constructor <init>(Landroid/app/Activity;I)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const-string v0, "activity"

    invoke-static {p1, v0}, Lcom/facebook/internal/ae;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    iput-object p1, p0, Lcom/facebook/internal/j;->b:Landroid/app/Activity;

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/internal/j;->c:Lcom/facebook/internal/r;

    .line 57
    iput p2, p0, Lcom/facebook/internal/j;->e:I

    .line 58
    return-void
.end method

.method protected constructor <init>(Lcom/facebook/internal/r;I)V
    .locals 2

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    const-string v0, "fragmentWrapper"

    invoke-static {p1, v0}, Lcom/facebook/internal/ae;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    iput-object p1, p0, Lcom/facebook/internal/j;->c:Lcom/facebook/internal/r;

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/internal/j;->b:Landroid/app/Activity;

    .line 64
    iput p2, p0, Lcom/facebook/internal/j;->e:I

    .line 66
    invoke-virtual {p1}, Lcom/facebook/internal/r;->c()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 67
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot use a fragment that is not attached to an activity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/facebook/internal/a;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TCONTENT;",
            "Ljava/lang/Object;",
            ")",
            "Lcom/facebook/internal/a;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 203
    sget-object v0, Lcom/facebook/internal/j;->a:Ljava/lang/Object;

    if-ne p2, v0, :cond_3

    move v1, v2

    .line 205
    :goto_0
    const/4 v3, 0x0

    .line 206
    invoke-direct {p0}, Lcom/facebook/internal/j;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/internal/j$a;

    .line 207
    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/internal/j$a;->a()Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5, p2}, Lcom/facebook/internal/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 210
    :cond_1
    invoke-virtual {v0, p1, v2}, Lcom/facebook/internal/j$a;->a(Ljava/lang/Object;Z)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 215
    :try_start_0
    invoke-virtual {v0, p1}, Lcom/facebook/internal/j$a;->a(Ljava/lang/Object;)Lcom/facebook/internal/a;
    :try_end_0
    .catch Lcom/facebook/l; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 223
    :goto_1
    if-nez v0, :cond_2

    .line 224
    invoke-virtual {p0}, Lcom/facebook/internal/j;->c()Lcom/facebook/internal/a;

    move-result-object v0

    .line 225
    invoke-static {v0}, Lcom/facebook/internal/i;->a(Lcom/facebook/internal/a;)V

    .line 228
    :cond_2
    return-object v0

    .line 203
    :cond_3
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 216
    :catch_0
    move-exception v1

    .line 217
    invoke-virtual {p0}, Lcom/facebook/internal/j;->c()Lcom/facebook/internal/a;

    move-result-object v0

    .line 218
    invoke-static {v0, v1}, Lcom/facebook/internal/i;->a(Lcom/facebook/internal/a;Lcom/facebook/l;)V

    goto :goto_1

    :cond_4
    move-object v0, v3

    goto :goto_1
.end method

.method private d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/internal/j",
            "<TCONTENT;TRESU",
            "LT;",
            ">.a;>;"
        }
    .end annotation

    .prologue
    .line 232
    iget-object v0, p0, Lcom/facebook/internal/j;->d:Ljava/util/List;

    if-nez v0, :cond_0

    .line 233
    invoke-virtual {p0}, Lcom/facebook/internal/j;->b()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/internal/j;->d:Ljava/util/List;

    .line 236
    :cond_0
    iget-object v0, p0, Lcom/facebook/internal/j;->d:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lcom/facebook/internal/j;->e:I

    return v0
.end method

.method public a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TCONTENT;)V"
        }
    .end annotation

    .prologue
    .line 145
    sget-object v0, Lcom/facebook/internal/j;->a:Ljava/lang/Object;

    invoke-virtual {p0, p1, v0}, Lcom/facebook/internal/j;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 146
    return-void
.end method

.method protected a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TCONTENT;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 150
    invoke-direct {p0, p1, p2}, Lcom/facebook/internal/j;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/facebook/internal/a;

    move-result-object v0

    .line 151
    if-eqz v0, :cond_2

    .line 152
    iget-object v1, p0, Lcom/facebook/internal/j;->c:Lcom/facebook/internal/r;

    if-eqz v1, :cond_1

    .line 153
    iget-object v1, p0, Lcom/facebook/internal/j;->c:Lcom/facebook/internal/r;

    invoke-static {v0, v1}, Lcom/facebook/internal/i;->a(Lcom/facebook/internal/a;Lcom/facebook/internal/r;)V

    .line 165
    :cond_0
    :goto_0
    return-void

    .line 155
    :cond_1
    iget-object v1, p0, Lcom/facebook/internal/j;->b:Landroid/app/Activity;

    invoke-static {v0, v1}, Lcom/facebook/internal/i;->a(Lcom/facebook/internal/a;Landroid/app/Activity;)V

    goto :goto_0

    .line 159
    :cond_2
    const-string v0, "No code path should ever result in a null appCall"

    .line 160
    const-string v1, "FacebookDialog"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    invoke-static {}, Lcom/facebook/p;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 162
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected abstract b()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/internal/j",
            "<TCONTENT;TRESU",
            "LT;",
            ">.a;>;"
        }
    .end annotation
.end method

.method protected abstract c()Lcom/facebook/internal/a;
.end method
