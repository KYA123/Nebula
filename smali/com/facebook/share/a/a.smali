.class public final enum Lcom/facebook/share/a/a;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/facebook/internal/h;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/share/a/a;",
        ">;",
        "Lcom/facebook/internal/h;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/facebook/share/a/a;

.field private static final synthetic c:[Lcom/facebook/share/a/a;


# instance fields
.field private b:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 34
    new-instance v0, Lcom/facebook/share/a/a;

    const-string v1, "APP_INVITES_DIALOG"

    const v2, 0x133529d

    invoke-direct {v0, v1, v3, v2}, Lcom/facebook/share/a/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/facebook/share/a/a;->a:Lcom/facebook/share/a/a;

    .line 31
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/facebook/share/a/a;

    sget-object v1, Lcom/facebook/share/a/a;->a:Lcom/facebook/share/a/a;

    aput-object v1, v0, v3

    sput-object v0, Lcom/facebook/share/a/a;->c:[Lcom/facebook/share/a/a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39
    iput p3, p0, Lcom/facebook/share/a/a;->b:I

    .line 40
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/share/a/a;
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/facebook/share/a/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/share/a/a;

    return-object v0
.end method

.method public static values()[Lcom/facebook/share/a/a;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/facebook/share/a/a;->c:[Lcom/facebook/share/a/a;

    invoke-virtual {v0}, [Lcom/facebook/share/a/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/share/a/a;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    const-string v0, "com.facebook.platform.action.request.APPINVITES_DIALOG"

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/facebook/share/a/a;->b:I

    return v0
.end method
