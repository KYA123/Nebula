.class abstract Lcom/facebook/share/a/c$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/facebook/share/a/c$n;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/facebook/share/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "a"
.end annotation


# instance fields
.field protected a:Ljava/lang/String;

.field protected b:Lcom/facebook/share/widget/LikeView$e;

.field protected c:Lcom/facebook/o;

.field final synthetic d:Lcom/facebook/share/a/c;

.field private e:Lcom/facebook/s;


# direct methods
.method protected constructor <init>(Lcom/facebook/share/a/c;Ljava/lang/String;Lcom/facebook/share/widget/LikeView$e;)V
    .locals 0

    .prologue
    .line 1662
    iput-object p1, p0, Lcom/facebook/share/a/c$a;->d:Lcom/facebook/share/a/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1663
    iput-object p2, p0, Lcom/facebook/share/a/c$a;->a:Ljava/lang/String;

    .line 1664
    iput-object p3, p0, Lcom/facebook/share/a/c$a;->b:Lcom/facebook/share/widget/LikeView$e;

    .line 1665
    return-void
.end method


# virtual methods
.method public a()Lcom/facebook/o;
    .locals 1

    .prologue
    .line 1672
    iget-object v0, p0, Lcom/facebook/share/a/c$a;->c:Lcom/facebook/o;

    return-object v0
.end method

.method protected a(Lcom/facebook/o;)V
    .locals 6

    .prologue
    .line 1694
    sget-object v0, Lcom/facebook/y;->a:Lcom/facebook/y;

    .line 1695
    invoke-static {}, Lcom/facebook/share/a/c;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error running request for object \'%s\' with type \'%s\' : %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/share/a/c$a;->a:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/facebook/share/a/c$a;->b:Lcom/facebook/share/widget/LikeView$e;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object p1, v3, v4

    .line 1694
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/internal/v;->a(Lcom/facebook/y;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1700
    return-void
.end method

.method protected a(Lcom/facebook/s;)V
    .locals 1

    .prologue
    .line 1676
    iput-object p1, p0, Lcom/facebook/share/a/c$a;->e:Lcom/facebook/s;

    .line 1679
    invoke-static {}, Lcom/facebook/p;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/s;->a(Ljava/lang/String;)V

    .line 1680
    new-instance v0, Lcom/facebook/share/a/c$a$1;

    invoke-direct {v0, p0}, Lcom/facebook/share/a/c$a$1;-><init>(Lcom/facebook/share/a/c$a;)V

    invoke-virtual {p1, v0}, Lcom/facebook/s;->a(Lcom/facebook/s$b;)V

    .line 1691
    return-void
.end method

.method public a(Lcom/facebook/u;)V
    .locals 1

    .prologue
    .line 1668
    iget-object v0, p0, Lcom/facebook/share/a/c$a;->e:Lcom/facebook/s;

    invoke-virtual {p1, v0}, Lcom/facebook/u;->a(Lcom/facebook/s;)Z

    .line 1669
    return-void
.end method

.method protected abstract a(Lcom/facebook/v;)V
.end method
