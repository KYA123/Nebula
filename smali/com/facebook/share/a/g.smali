.class public Lcom/facebook/share/a/g;
.super Lcom/facebook/internal/j;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/facebook/share/a/g$c;,
        Lcom/facebook/share/a/g$a;,
        Lcom/facebook/share/a/g$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/internal/j",
        "<",
        "Lcom/facebook/share/a/f;",
        "Lcom/facebook/share/a/g$b;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/facebook/internal/f$b;->d:Lcom/facebook/internal/f$b;

    .line 50
    invoke-virtual {v0}, Lcom/facebook/internal/f$b;->a()I

    move-result v0

    sput v0, Lcom/facebook/share/a/g;->b:I

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 85
    sget v0, Lcom/facebook/share/a/g;->b:I

    invoke-direct {p0, p1, v0}, Lcom/facebook/internal/j;-><init>(Landroid/app/Activity;I)V

    .line 86
    return-void
.end method

.method public constructor <init>(Lcom/facebook/internal/r;)V
    .locals 1

    .prologue
    .line 97
    sget v0, Lcom/facebook/share/a/g;->b:I

    invoke-direct {p0, p1, v0}, Lcom/facebook/internal/j;-><init>(Lcom/facebook/internal/r;I)V

    .line 98
    return-void
.end method

.method static synthetic a(Lcom/facebook/share/a/f;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 45
    invoke-static {p0}, Lcom/facebook/share/a/g;->b(Lcom/facebook/share/a/f;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lcom/facebook/share/a/f;)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 199
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 201
    const-string v1, "object_id"

    invoke-virtual {p0}, Lcom/facebook/share/a/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    const-string v1, "object_type"

    invoke-virtual {p0}, Lcom/facebook/share/a/f;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    return-object v0
.end method

.method public static d()Z
    .locals 1

    .prologue
    .line 76
    invoke-static {}, Lcom/facebook/share/a/g;->g()Lcom/facebook/internal/h;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/internal/i;->a(Lcom/facebook/internal/h;)Z

    move-result v0

    return v0
.end method

.method public static e()Z
    .locals 1

    .prologue
    .line 81
    invoke-static {}, Lcom/facebook/share/a/g;->g()Lcom/facebook/internal/h;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/internal/i;->b(Lcom/facebook/internal/h;)Z

    move-result v0

    return v0
.end method

.method static synthetic f()Lcom/facebook/internal/h;
    .locals 1

    .prologue
    .line 45
    invoke-static {}, Lcom/facebook/share/a/g;->g()Lcom/facebook/internal/h;

    move-result-object v0

    return-object v0
.end method

.method private static g()Lcom/facebook/internal/h;
    .locals 1

    .prologue
    .line 195
    sget-object v0, Lcom/facebook/share/a/h;->a:Lcom/facebook/share/a/h;

    return-object v0
.end method


# virtual methods
.method protected b()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/internal/j",
            "<",
            "Lcom/facebook/share/a/f;",
            "Lcom/facebook/share/a/g$b;",
            ">.a;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 107
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 108
    new-instance v1, Lcom/facebook/share/a/g$a;

    invoke-direct {v1, p0, v2}, Lcom/facebook/share/a/g$a;-><init>(Lcom/facebook/share/a/g;Lcom/facebook/share/a/g$1;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 109
    new-instance v1, Lcom/facebook/share/a/g$c;

    invoke-direct {v1, p0, v2}, Lcom/facebook/share/a/g$c;-><init>(Lcom/facebook/share/a/g;Lcom/facebook/share/a/g$1;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    return-object v0
.end method

.method protected c()Lcom/facebook/internal/a;
    .locals 2

    .prologue
    .line 102
    new-instance v0, Lcom/facebook/internal/a;

    invoke-virtual {p0}, Lcom/facebook/share/a/g;->a()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/facebook/internal/a;-><init>(I)V

    return-object v0
.end method
