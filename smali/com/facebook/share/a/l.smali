.class public final Lcom/facebook/share/a/l;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Ljava/lang/String;)Landroid/util/Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 502
    const/4 v0, 0x0

    .line 504
    const/16 v1, 0x3a

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 505
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v3, v1, 0x1

    if-le v2, v3, :cond_0

    .line 506
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 507
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 511
    :cond_0
    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, v0, p0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1
.end method

.method private static a(IILandroid/content/Intent;)Lcom/facebook/internal/a;
    .locals 1

    .prologue
    .line 219
    invoke-static {p2}, Lcom/facebook/internal/y;->b(Landroid/content/Intent;)Ljava/util/UUID;

    move-result-object v0

    .line 220
    if-nez v0, :cond_0

    .line 221
    const/4 v0, 0x0

    .line 224
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0, p0}, Lcom/facebook/internal/a;->a(Ljava/util/UUID;I)Lcom/facebook/internal/a;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/a;Landroid/net/Uri;Lcom/facebook/s$b;)Lcom/facebook/s;
    .locals 6

    .prologue
    .line 681
    invoke-static {p1}, Lcom/facebook/internal/ad;->c(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 682
    new-instance v0, Ljava/io/File;

    .line 684
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 682
    invoke-static {p0, v0, p2}, Lcom/facebook/share/a/l;->a(Lcom/facebook/a;Ljava/io/File;Lcom/facebook/s$b;)Lcom/facebook/s;

    move-result-object v0

    .line 695
    :goto_0
    return-object v0

    .line 686
    :cond_0
    invoke-static {p1}, Lcom/facebook/internal/ad;->b(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 687
    new-instance v0, Lcom/facebook/l;

    const-string v1, "The image Uri must be either a file:// or content:// Uri"

    invoke-direct {v0, v1}, Lcom/facebook/l;-><init>(Ljava/lang/String;)V

    throw v0

    .line 690
    :cond_1
    new-instance v0, Lcom/facebook/s$f;

    const-string v1, "image/png"

    invoke-direct {v0, p1, v1}, Lcom/facebook/s$f;-><init>(Landroid/os/Parcelable;Ljava/lang/String;)V

    .line 692
    new-instance v3, Landroid/os/Bundle;

    const/4 v1, 0x1

    invoke-direct {v3, v1}, Landroid/os/Bundle;-><init>(I)V

    .line 693
    const-string v1, "file"

    invoke-virtual {v3, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 695
    new-instance v0, Lcom/facebook/s;

    const-string v2, "me/staging_resources"

    sget-object v4, Lcom/facebook/w;->b:Lcom/facebook/w;

    move-object v1, p0

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/s;-><init>(Lcom/facebook/a;Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/w;Lcom/facebook/s$b;)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/a;Ljava/io/File;Lcom/facebook/s$b;)Lcom/facebook/s;
    .locals 6

    .prologue
    .line 648
    const/high16 v0, 0x10000000

    .line 649
    invoke-static {p1, v0}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 650
    new-instance v1, Lcom/facebook/s$f;

    const-string v2, "image/png"

    invoke-direct {v1, v0, v2}, Lcom/facebook/s$f;-><init>(Landroid/os/Parcelable;Ljava/lang/String;)V

    .line 652
    new-instance v3, Landroid/os/Bundle;

    const/4 v0, 0x1

    invoke-direct {v3, v0}, Landroid/os/Bundle;-><init>(I)V

    .line 653
    const-string v0, "file"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 655
    new-instance v0, Lcom/facebook/s;

    const-string v2, "me/staging_resources"

    sget-object v4, Lcom/facebook/w;->b:Lcom/facebook/w;

    move-object v1, p0

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/s;-><init>(Lcom/facebook/a;Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/w;Lcom/facebook/s$b;)V

    return-object v0
.end method

.method public static a(Lcom/facebook/share/widget/LikeView$e;Lcom/facebook/share/widget/LikeView$e;)Lcom/facebook/share/widget/LikeView$e;
    .locals 1

    .prologue
    .line 707
    if-ne p0, p1, :cond_1

    .line 717
    :cond_0
    :goto_0
    return-object p0

    .line 711
    :cond_1
    sget-object v0, Lcom/facebook/share/widget/LikeView$e;->a:Lcom/facebook/share/widget/LikeView$e;

    if-ne p0, v0, :cond_2

    move-object p0, p1

    .line 712
    goto :goto_0

    .line 713
    :cond_2
    sget-object v0, Lcom/facebook/share/widget/LikeView$e;->a:Lcom/facebook/share/widget/LikeView$e;

    if-eq p1, v0, :cond_0

    .line 717
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    const-string v0, "completionGesture"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    const-string v0, "completionGesture"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 131
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "com.facebook.platform.extra.COMPLETION_GESTURE"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lorg/json/JSONArray;Z)Lorg/json/JSONArray;
    .locals 4

    .prologue
    .line 436
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 437
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 438
    invoke-virtual {p0, v1}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 439
    instance-of v3, v0, Lorg/json/JSONArray;

    if-eqz v3, :cond_1

    .line 440
    check-cast v0, Lorg/json/JSONArray;

    invoke-static {v0, p1}, Lcom/facebook/share/a/l;->a(Lorg/json/JSONArray;Z)Lorg/json/JSONArray;

    move-result-object v0

    .line 444
    :cond_0
    :goto_1
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 437
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 441
    :cond_1
    instance-of v3, v0, Lorg/json/JSONObject;

    if-eqz v3, :cond_0

    .line 442
    check-cast v0, Lorg/json/JSONObject;

    invoke-static {v0, p1}, Lcom/facebook/share/a/l;->a(Lorg/json/JSONObject;Z)Lorg/json/JSONObject;

    move-result-object v0

    goto :goto_1

    .line 447
    :cond_2
    return-object v2
.end method

.method public static a(Lcom/facebook/share/b/h;)Lorg/json/JSONObject;
    .locals 2

    .prologue
    .line 413
    invoke-virtual {p0}, Lcom/facebook/share/b/h;->c()Lcom/facebook/share/b/g;

    move-result-object v0

    .line 415
    new-instance v1, Lcom/facebook/share/a/l$1;

    invoke-direct {v1}, Lcom/facebook/share/a/l$1;-><init>()V

    invoke-static {v0, v1}, Lcom/facebook/share/a/j;->a(Lcom/facebook/share/b/g;Lcom/facebook/share/a/j$a;)Lorg/json/JSONObject;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lorg/json/JSONObject;Z)Lorg/json/JSONObject;
    .locals 9

    .prologue
    .line 453
    if-nez p0, :cond_0

    .line 454
    const/4 v0, 0x0

    .line 495
    :goto_0
    return-object v0

    .line 458
    :cond_0
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 459
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 460
    invoke-virtual {p0}, Lorg/json/JSONObject;->names()Lorg/json/JSONArray;

    move-result-object v6

    .line 461
    const/4 v0, 0x0

    move v4, v0

    :goto_1
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v4, v0, :cond_7

    .line 462
    invoke-virtual {v6, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 464
    invoke-virtual {p0, v7}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 465
    instance-of v1, v0, Lorg/json/JSONObject;

    if-eqz v1, :cond_1

    .line 466
    check-cast v0, Lorg/json/JSONObject;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/share/a/l;->a(Lorg/json/JSONObject;Z)Lorg/json/JSONObject;

    move-result-object v0

    move-object v3, v0

    .line 471
    :goto_2
    invoke-static {v7}, Lcom/facebook/share/a/l;->a(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v1

    .line 473
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 474
    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    .line 476
    if-eqz p1, :cond_5

    .line 477
    if-eqz v0, :cond_2

    const-string v8, "fbsdk"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 478
    invoke-virtual {v2, v7, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 461
    :goto_3
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 467
    :cond_1
    instance-of v1, v0, Lorg/json/JSONArray;

    if-eqz v1, :cond_9

    .line 468
    check-cast v0, Lorg/json/JSONArray;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/share/a/l;->a(Lorg/json/JSONArray;Z)Lorg/json/JSONArray;

    move-result-object v0

    move-object v3, v0

    goto :goto_2

    .line 479
    :cond_2
    if-eqz v0, :cond_3

    const-string v7, "og"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 480
    :cond_3
    invoke-virtual {v2, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 496
    :catch_0
    move-exception v0

    .line 497
    new-instance v0, Lcom/facebook/l;

    const-string v1, "Failed to create json object from share content"

    invoke-direct {v0, v1}, Lcom/facebook/l;-><init>(Ljava/lang/String;)V

    throw v0

    .line 482
    :cond_4
    :try_start_1
    invoke-virtual {v5, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_3

    .line 484
    :cond_5
    if-eqz v0, :cond_6

    const-string v8, "fb"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 485
    invoke-virtual {v2, v7, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_3

    .line 487
    :cond_6
    invoke-virtual {v2, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_3

    .line 491
    :cond_7
    invoke-virtual {v5}, Lorg/json/JSONObject;->length()I

    move-result v0

    if-lez v0, :cond_8

    .line 492
    const-string v0, "data"

    invoke-virtual {v2, v0, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_8
    move-object v0, v2

    .line 495
    goto/16 :goto_0

    :cond_9
    move-object v3, v0

    goto :goto_2
.end method

.method public static a(IILandroid/content/Intent;Lcom/facebook/share/a/k;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 156
    invoke-static {p0, p1, p2}, Lcom/facebook/share/a/l;->a(IILandroid/content/Intent;)Lcom/facebook/internal/a;

    move-result-object v1

    .line 157
    if-nez v1, :cond_1

    .line 158
    const/4 v0, 0x0

    .line 180
    :cond_0
    :goto_0
    return v0

    .line 161
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/internal/a;->c()Ljava/util/UUID;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/internal/x;->a(Ljava/util/UUID;)V

    .line 162
    if-eqz p3, :cond_0

    .line 167
    invoke-static {p2}, Lcom/facebook/internal/y;->g(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v2

    .line 166
    invoke-static {v2}, Lcom/facebook/internal/y;->a(Landroid/os/Bundle;)Lcom/facebook/l;

    move-result-object v2

    .line 168
    if-eqz v2, :cond_3

    .line 169
    instance-of v3, v2, Lcom/facebook/n;

    if-eqz v3, :cond_2

    .line 170
    invoke-virtual {p3, v1}, Lcom/facebook/share/a/k;->a(Lcom/facebook/internal/a;)V

    goto :goto_0

    .line 172
    :cond_2
    invoke-virtual {p3, v1, v2}, Lcom/facebook/share/a/k;->a(Lcom/facebook/internal/a;Lcom/facebook/l;)V

    goto :goto_0

    .line 176
    :cond_3
    invoke-static {p2}, Lcom/facebook/internal/y;->e(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v2

    .line 177
    invoke-virtual {p3, v1, v2}, Lcom/facebook/share/a/k;->a(Lcom/facebook/internal/a;Landroid/os/Bundle;)V

    goto :goto_0
.end method
