.class public Lcom/flask/colorpicker/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:F

.field private b:F

.field private c:[F

.field private d:[F

.field private e:I


# direct methods
.method public constructor <init>(FF[F)V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/flask/colorpicker/b;->c:[F

    .line 12
    invoke-virtual {p0, p1, p2, p3}, Lcom/flask/colorpicker/b;->a(FF[F)V

    .line 13
    return-void
.end method


# virtual methods
.method public a(FF)D
    .locals 4

    .prologue
    .line 16
    iget v0, p0, Lcom/flask/colorpicker/b;->a:F

    sub-float/2addr v0, p1

    float-to-double v0, v0

    .line 17
    iget v2, p0, Lcom/flask/colorpicker/b;->b:F

    sub-float/2addr v2, p2

    float-to-double v2, v2

    .line 18
    mul-double/2addr v0, v0

    mul-double/2addr v2, v2

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public a()F
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/flask/colorpicker/b;->a:F

    return v0
.end method

.method public a(FF[F)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 43
    iput p1, p0, Lcom/flask/colorpicker/b;->a:F

    .line 44
    iput p2, p0, Lcom/flask/colorpicker/b;->b:F

    .line 45
    iget-object v0, p0, Lcom/flask/colorpicker/b;->c:[F

    aget v1, p3, v2

    aput v1, v0, v2

    .line 46
    iget-object v0, p0, Lcom/flask/colorpicker/b;->c:[F

    aget v1, p3, v3

    aput v1, v0, v3

    .line 47
    iget-object v0, p0, Lcom/flask/colorpicker/b;->c:[F

    aget v1, p3, v4

    aput v1, v0, v4

    .line 48
    iget-object v0, p0, Lcom/flask/colorpicker/b;->c:[F

    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v0

    iput v0, p0, Lcom/flask/colorpicker/b;->e:I

    .line 49
    return-void
.end method

.method public a(F)[F
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 34
    iget-object v0, p0, Lcom/flask/colorpicker/b;->d:[F

    if-nez v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/flask/colorpicker/b;->c:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    iput-object v0, p0, Lcom/flask/colorpicker/b;->d:[F

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/flask/colorpicker/b;->d:[F

    iget-object v1, p0, Lcom/flask/colorpicker/b;->c:[F

    aget v1, v1, v2

    aput v1, v0, v2

    .line 37
    iget-object v0, p0, Lcom/flask/colorpicker/b;->d:[F

    iget-object v1, p0, Lcom/flask/colorpicker/b;->c:[F

    aget v1, v1, v3

    aput v1, v0, v3

    .line 38
    iget-object v0, p0, Lcom/flask/colorpicker/b;->d:[F

    const/4 v1, 0x2

    aput p1, v0, v1

    .line 39
    iget-object v0, p0, Lcom/flask/colorpicker/b;->d:[F

    return-object v0
.end method

.method public b()F
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/flask/colorpicker/b;->b:F

    return v0
.end method

.method public c()[F
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/flask/colorpicker/b;->c:[F

    return-object v0
.end method
