.class public final enum Lcom/fyber/ads/b;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/fyber/ads/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/fyber/ads/b;

.field public static final enum b:Lcom/fyber/ads/b;

.field public static final enum c:Lcom/fyber/ads/b;

.field public static final enum d:Lcom/fyber/ads/b;

.field public static final enum e:Lcom/fyber/ads/b;

.field private static final synthetic f:[Lcom/fyber/ads/b;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    new-instance v0, Lcom/fyber/ads/b;

    const-string v1, "OFFER_WALL"

    invoke-direct {v0, v1, v2}, Lcom/fyber/ads/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/fyber/ads/b;->a:Lcom/fyber/ads/b;

    .line 28
    new-instance v0, Lcom/fyber/ads/b;

    const-string v1, "REWARDED_VIDEO"

    invoke-direct {v0, v1, v3}, Lcom/fyber/ads/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/fyber/ads/b;->b:Lcom/fyber/ads/b;

    .line 32
    new-instance v0, Lcom/fyber/ads/b;

    const-string v1, "INTERSTITIAL"

    invoke-direct {v0, v1, v4}, Lcom/fyber/ads/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/fyber/ads/b;->c:Lcom/fyber/ads/b;

    .line 36
    new-instance v0, Lcom/fyber/ads/b;

    const-string v1, "BANNER"

    invoke-direct {v0, v1, v5}, Lcom/fyber/ads/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/fyber/ads/b;->d:Lcom/fyber/ads/b;

    .line 40
    new-instance v0, Lcom/fyber/ads/b;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6}, Lcom/fyber/ads/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/fyber/ads/b;->e:Lcom/fyber/ads/b;

    .line 19
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/fyber/ads/b;

    sget-object v1, Lcom/fyber/ads/b;->a:Lcom/fyber/ads/b;

    aput-object v1, v0, v2

    sget-object v1, Lcom/fyber/ads/b;->b:Lcom/fyber/ads/b;

    aput-object v1, v0, v3

    sget-object v1, Lcom/fyber/ads/b;->c:Lcom/fyber/ads/b;

    aput-object v1, v0, v4

    sget-object v1, Lcom/fyber/ads/b;->d:Lcom/fyber/ads/b;

    aput-object v1, v0, v5

    sget-object v1, Lcom/fyber/ads/b;->e:Lcom/fyber/ads/b;

    aput-object v1, v0, v6

    sput-object v0, Lcom/fyber/ads/b;->f:[Lcom/fyber/ads/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Landroid/content/Intent;)Lcom/fyber/ads/b;
    .locals 1

    .prologue
    .line 49
    if-eqz p0, :cond_0

    .line 51
    const-string v0, "EXTRA_AD_FORMAT"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    .line 53
    if-eqz v0, :cond_0

    .line 54
    check-cast v0, Lcom/fyber/ads/b;

    .line 57
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/fyber/ads/b;->e:Lcom/fyber/ads/b;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/fyber/ads/b;
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/fyber/ads/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/fyber/ads/b;

    return-object v0
.end method

.method public static values()[Lcom/fyber/ads/b;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/fyber/ads/b;->f:[Lcom/fyber/ads/b;

    invoke-virtual {v0}, [Lcom/fyber/ads/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/fyber/ads/b;

    return-object v0
.end method
