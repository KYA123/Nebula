.class final Lcom/fyber/ads/videos/a/c$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/fyber/ads/videos/a/c;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/fyber/ads/videos/a/c;


# direct methods
.method constructor <init>(Lcom/fyber/ads/videos/a/c;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcom/fyber/ads/videos/a/c$2;->a:Lcom/fyber/ads/videos/a/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)Z
    .locals 3

    .prologue
    .line 194
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 220
    const-string v0, "RewardedVideoClient"

    const-string v1, "Unknown message what field"

    invoke-static {v0, v1}, Lcom/fyber/utils/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 196
    :sswitch_0
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$2;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v0}, Lcom/fyber/ads/videos/a/c;->b(Lcom/fyber/ads/videos/a/c;)Landroid/webkit/WebView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1228
    const-string v1, "http"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    .line 200
    if-eqz v1, :cond_1

    .line 201
    iget-object v1, p0, Lcom/fyber/ads/videos/a/c$2;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v1}, Lcom/fyber/ads/videos/a/c;->b(Lcom/fyber/ads/videos/a/c;)Landroid/webkit/WebView;

    move-result-object v1

    iget-object v2, p0, Lcom/fyber/ads/videos/a/c$2;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v2}, Lcom/fyber/ads/videos/a/c;->c(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/g/a/j;

    move-result-object v2

    invoke-virtual {v2}, Lcom/fyber/g/a/j;->d()Lcom/fyber/g/a/l;

    move-result-object v2

    invoke-virtual {v2}, Lcom/fyber/g/a/l;->d()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;Ljava/util/Map;)V

    .line 205
    :goto_1
    const-string v1, "about:blank"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$2;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v0}, Lcom/fyber/ads/videos/a/c;->d(Lcom/fyber/ads/videos/a/c;)Landroid/webkit/WebView;

    .line 208
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$2;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v0}, Lcom/fyber/ads/videos/a/c;->e(Lcom/fyber/ads/videos/a/c;)Landroid/webkit/WebViewClient;

    .line 209
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$2;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v0}, Lcom/fyber/ads/videos/a/c;->f(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/ads/videos/RewardedVideoActivity;

    .line 210
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$2;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v0}, Lcom/fyber/ads/videos/a/c;->g(Lcom/fyber/ads/videos/a/c;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$2;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v0}, Lcom/fyber/ads/videos/a/c;->h(Lcom/fyber/ads/videos/a/c;)Landroid/content/Context;

    goto :goto_0

    .line 203
    :cond_1
    iget-object v1, p0, Lcom/fyber/ads/videos/a/c$2;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v1}, Lcom/fyber/ads/videos/a/c;->b(Lcom/fyber/ads/videos/a/c;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_1

    .line 217
    :sswitch_1
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$2;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v0}, Lcom/fyber/ads/videos/a/c;->i(Lcom/fyber/ads/videos/a/c;)V

    goto :goto_0

    .line 194
    :sswitch_data_0
    .sparse-switch
        0x7b -> :sswitch_0
        0x20a -> :sswitch_1
    .end sparse-switch
.end method
