.class final Lcom/fyber/ads/videos/a/c$7;
.super Lcom/fyber/utils/e;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/fyber/ads/videos/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/fyber/ads/videos/a/c;


# direct methods
.method constructor <init>(Lcom/fyber/ads/videos/a/c;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 620
    iput-object p1, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    invoke-direct {p0, p2}, Lcom/fyber/utils/e;-><init>(Landroid/app/Activity;)V

    return-void
.end method


# virtual methods
.method protected final a()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 779
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v0}, Lcom/fyber/ads/videos/a/c;->p(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/ads/videos/RewardedVideoActivity;

    move-result-object v0

    return-object v0
.end method

.method protected final a(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 767
    .line 1779
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v0}, Lcom/fyber/ads/videos/a/c;->p(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/ads/videos/RewardedVideoActivity;

    move-result-object v0

    .line 769
    if-nez v0, :cond_0

    .line 775
    :goto_0
    return-void

    .line 773
    :cond_0
    invoke-virtual {v0, p1}, Landroid/app/Activity;->setResult(I)V

    .line 774
    invoke-virtual {p0, p2}, Lcom/fyber/ads/videos/a/c$7;->a(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method protected final a(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 6

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    .line 624
    const-string v0, "requestOffers"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 625
    const-string v0, "n"

    invoke-virtual {p2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 626
    const-string v1, "params"

    invoke-virtual {p2, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fyber/g/a/a/e$a;->a(Ljava/lang/String;)Lcom/fyber/g/a/a/e$a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/fyber/g/a/a/e$a;->a()Lcom/fyber/g/a/a/e;

    move-result-object v1

    .line 627
    iget-object v2, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v2, v0, v1}, Lcom/fyber/ads/videos/a/c;->a(Lcom/fyber/ads/videos/a/c;ILcom/fyber/g/a/a/e;)V

    .line 628
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    const-string v1, "currency_id"

    invoke-virtual {p2, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/fyber/ads/videos/a/c;->b(Lcom/fyber/ads/videos/a/c;Ljava/lang/String;)Ljava/lang/String;

    .line 729
    :cond_0
    :goto_0
    return-void

    .line 629
    :cond_1
    const-string v0, "start"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 630
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    const-string v1, "status"

    invoke-virtual {p2, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/fyber/ads/videos/a/c;->c(Lcom/fyber/ads/videos/a/c;Ljava/lang/String;)V

    goto :goto_0

    .line 631
    :cond_2
    const-string v0, "validate"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 632
    const-string v0, "tpn"

    invoke-virtual {p2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 633
    sget-object v1, Lcom/fyber/mediation/g;->a:Lcom/fyber/mediation/g;

    invoke-virtual {v1, v0}, Lcom/fyber/mediation/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 634
    const-string v2, "id"

    invoke-virtual {p2, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 635
    const-string v3, "RewardedVideoClient"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "RewardedVideo client asks to validate a third party network: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 637
    sget-object v3, Lcom/fyber/mediation/g;->a:Lcom/fyber/mediation/g;

    sget-object v4, Lcom/fyber/ads/b;->b:Lcom/fyber/ads/b;

    invoke-virtual {v3, v0, v4}, Lcom/fyber/mediation/g;->a(Ljava/lang/String;Lcom/fyber/ads/b;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 638
    invoke-static {}, Lcom/fyber/a;->c()Lcom/fyber/a$b;

    move-result-object v3

    new-instance v4, Lcom/fyber/ads/videos/a/c$7$1;

    invoke-direct {v4, p0, v0, v2, v1}, Lcom/fyber/ads/videos/a/c$7$1;-><init>(Lcom/fyber/ads/videos/a/c$7;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/fyber/a$b;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 664
    :cond_3
    iget-object v3, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    sget-object v4, Lcom/fyber/ads/videos/b/c;->a:Lcom/fyber/ads/videos/b/c;

    invoke-static {v3, v0, v1, v4, v2}, Lcom/fyber/ads/videos/a/c;->a(Lcom/fyber/ads/videos/a/c;Ljava/lang/String;Ljava/lang/String;Lcom/fyber/ads/videos/b/c;Ljava/lang/String;)V

    goto :goto_0

    .line 666
    :cond_4
    const-string v0, "play"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 667
    const-string v0, "tpn"

    invoke-virtual {p2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 669
    const-string v1, "local"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 671
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v0}, Lcom/fyber/ads/videos/a/c;->o(Lcom/fyber/ads/videos/a/c;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 672
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v0}, Lcom/fyber/ads/videos/a/c;->p(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/ads/videos/RewardedVideoActivity;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 674
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v0}, Lcom/fyber/ads/videos/a/c;->q(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/ads/videos/a/c$a;

    move-result-object v0

    if-nez v0, :cond_5

    .line 675
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    new-instance v1, Lcom/fyber/ads/videos/a/c$a;

    iget-object v2, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v2}, Lcom/fyber/ads/videos/a/c;->r(Lcom/fyber/ads/videos/a/c;)Landroid/os/Handler;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/fyber/ads/videos/a/c$a;-><init>(Landroid/os/Handler;)V

    invoke-static {v0, v1}, Lcom/fyber/ads/videos/a/c;->a(Lcom/fyber/ads/videos/a/c;Lcom/fyber/ads/videos/a/c$a;)Lcom/fyber/ads/videos/a/c$a;

    .line 678
    :cond_5
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    new-instance v1, Lcom/fyber/c/d/d$a;

    invoke-direct {v1}, Lcom/fyber/c/d/d$a;-><init>()V

    iget-object v2, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    .line 679
    invoke-static {v2}, Lcom/fyber/ads/videos/a/c;->q(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/ads/videos/a/c$a;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/fyber/c/d/d$a;->a(Lcom/fyber/c/d/b;)Lcom/fyber/c/d/d$a;

    move-result-object v1

    const-string v2, "id"

    .line 680
    invoke-virtual {p2, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/fyber/c/d/d$a;->a(Ljava/lang/String;)Lcom/fyber/c/d/d$a;

    move-result-object v1

    const-string v2, "clickThroughUrl"

    .line 681
    invoke-virtual {p2, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/fyber/c/d/d$a;->b(Ljava/lang/String;)Lcom/fyber/c/d/d$a;

    move-result-object v1

    const-string v2, "alertMessage"

    .line 682
    invoke-virtual {p2, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/fyber/c/d/d$a;->d(Ljava/lang/String;)Lcom/fyber/c/d/d$a;

    move-result-object v1

    const-string v2, "showAlert"

    .line 683
    invoke-virtual {p2, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/fyber/c/d/d$a;->e(Ljava/lang/String;)Lcom/fyber/c/d/d$a;

    move-result-object v1

    iget-object v2, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    .line 684
    invoke-virtual {v1, v2}, Lcom/fyber/c/d/d$a;->a(Lcom/fyber/c/d/d$d;)Lcom/fyber/c/d/d$a;

    move-result-object v1

    new-instance v2, Lcom/fyber/ads/videos/a/b;

    invoke-direct {v2}, Lcom/fyber/ads/videos/a/b;-><init>()V

    .line 685
    invoke-virtual {v1, v2}, Lcom/fyber/c/d/d$a;->a(Lcom/fyber/c/d/d$b;)Lcom/fyber/c/d/d$a;

    move-result-object v1

    iget-object v2, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    .line 686
    invoke-static {v2}, Lcom/fyber/ads/videos/a/c;->p(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/ads/videos/RewardedVideoActivity;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/fyber/c/d/d$a;->a(Landroid/app/Activity;)Lcom/fyber/c/d/d;

    move-result-object v1

    .line 678
    invoke-static {v0, v1}, Lcom/fyber/ads/videos/a/c;->a(Lcom/fyber/ads/videos/a/c;Lcom/fyber/c/d/d;)Lcom/fyber/c/d/d;

    .line 688
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v0}, Lcom/fyber/ads/videos/a/c;->p(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/ads/videos/RewardedVideoActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v1}, Lcom/fyber/ads/videos/a/c;->s(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/c/d/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/fyber/ads/videos/RewardedVideoActivity;->a(Lcom/fyber/mediation/d;)V

    .line 690
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v0}, Lcom/fyber/ads/videos/a/c;->s(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/c/d/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fyber/c/d/d;->c()V

    .line 691
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v0}, Lcom/fyber/ads/videos/a/c;->s(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/c/d/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fyber/c/d/d;->d()V

    .line 692
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v0}, Lcom/fyber/ads/videos/a/c;->p(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/ads/videos/RewardedVideoActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v1}, Lcom/fyber/ads/videos/a/c;->s(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/c/d/d;

    move-result-object v1

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Lcom/fyber/ads/videos/RewardedVideoActivity;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 694
    :cond_6
    const-string v0, "RewardedVideoClient"

    const-string v1, "There was an issue - we were unable to attach the video player. "

    invoke-static {v0, v1}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 698
    :cond_7
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, v3}, Ljava/util/HashMap;-><init>(I)V

    .line 699
    const-string v2, "id"

    const-string v3, "id"

    invoke-virtual {p2, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 700
    const-string v2, "RewardedVideoClient"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "RewardedVideo client asks to play an offer from a third party network:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 701
    sget-object v2, Lcom/fyber/mediation/g;->a:Lcom/fyber/mediation/g;

    iget-object v3, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v3}, Lcom/fyber/ads/videos/a/c;->p(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/ads/videos/RewardedVideoActivity;

    move-result-object v3

    new-instance v4, Lcom/fyber/ads/videos/a/c$7$2;

    invoke-direct {v4, p0}, Lcom/fyber/ads/videos/a/c$7$2;-><init>(Lcom/fyber/ads/videos/a/c$7;)V

    invoke-virtual {v2, v3, v0, v1, v4}, Lcom/fyber/mediation/g;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/util/Map;Lcom/fyber/ads/videos/b/d;)V

    goto/16 :goto_0

    .line 715
    :cond_8
    const-string v0, "jud"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 717
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v0}, Lcom/fyber/ads/videos/a/c;->c(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/g/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fyber/g/a/j;->d()Lcom/fyber/g/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fyber/g/a/l;->d()Ljava/util/Map;

    move-result-object v1

    .line 718
    const/4 v0, 0x0

    .line 719
    invoke-static {v1}, Lcom/fyber/utils/q;->b(Ljava/util/Map;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 720
    const-string v0, "X-User-Data"

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 722
    :cond_9
    invoke-static {v0}, Lcom/fyber/utils/c;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 723
    const-string v0, ""

    .line 725
    :cond_a
    const-string v1, "javascript:Sponsorpay.MBE.SDKInterface.trigger(\'jud\', \'%s\')"

    new-array v2, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 726
    const-string v1, "RewardedVideoClient"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "JUD tracking event will be called:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/fyber/utils/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 727
    iget-object v1, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v1, v0}, Lcom/fyber/ads/videos/a/c;->d(Lcom/fyber/ads/videos/a/c;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method protected final b()V
    .locals 2

    .prologue
    .line 756
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    const-string v1, "USER_ENGAGED"

    invoke-static {v0, v1}, Lcom/fyber/ads/videos/a/c;->c(Lcom/fyber/ads/videos/a/c;Ljava/lang/String;)V

    .line 757
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    sget-object v1, Lcom/fyber/ads/videos/a/d$a;->d:Lcom/fyber/ads/videos/a/d$a;

    invoke-static {v0, v1}, Lcom/fyber/ads/videos/a/c;->a(Lcom/fyber/ads/videos/a/c;Lcom/fyber/ads/videos/a/d$a;)V

    .line 758
    return-void
.end method

.method protected final c()V
    .locals 2

    .prologue
    .line 762
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    sget-object v1, Lcom/fyber/a$a$a;->h:Lcom/fyber/a$a$a;

    invoke-static {v1}, Lcom/fyber/utils/x;->a(Lcom/fyber/a$a$a;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/fyber/ads/videos/a/c;->a(Lcom/fyber/ads/videos/a/c;Ljava/lang/String;)V

    .line 763
    return-void
.end method

.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 733
    const-string v0, "RewardedVideoClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onReceivedError url - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 735
    const-string v0, "market://"

    invoke-virtual {p4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 736
    const-string v0, "RewardedVideoClient"

    const-string v1, "discarding error - market:// url"

    invoke-static {v0, v1}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 752
    :goto_0
    return-void

    .line 740
    :cond_0
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v0}, Lcom/fyber/ads/videos/a/c;->t(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/ads/videos/a/e;

    move-result-object v0

    sget-object v1, Lcom/fyber/ads/videos/a/e;->b:Lcom/fyber/ads/videos/a/e;

    if-ne v0, v1, :cond_1

    .line 741
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v0}, Lcom/fyber/ads/videos/a/c;->o(Lcom/fyber/ads/videos/a/c;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 742
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v0}, Lcom/fyber/ads/videos/a/c;->u(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/g/a/e;

    move-result-object v0

    sget-object v1, Lcom/fyber/ads/b;->b:Lcom/fyber/ads/b;

    invoke-virtual {v0, v1}, Lcom/fyber/g/a/e;->d(Ljava/lang/Object;)V

    .line 743
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v0}, Lcom/fyber/ads/videos/a/c;->n(Lcom/fyber/ads/videos/a/c;)V

    .line 751
    :goto_1
    invoke-super {p0, p1, p2, p3, p4}, Lcom/fyber/utils/e;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 745
    :cond_1
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v0}, Lcom/fyber/ads/videos/a/c;->s(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/c/d/d;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 746
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v0}, Lcom/fyber/ads/videos/a/c;->s(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/c/d/d;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v3, v3}, Lcom/fyber/c/d/d;->onError(Landroid/media/MediaPlayer;II)Z

    goto :goto_1

    .line 748
    :cond_2
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$7;->a:Lcom/fyber/ads/videos/a/c;

    sget-object v1, Lcom/fyber/a$a$a;->m:Lcom/fyber/a$a$a;

    invoke-static {v1}, Lcom/fyber/utils/x;->a(Lcom/fyber/a$a$a;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/fyber/ads/videos/a/c;->a(Lcom/fyber/ads/videos/a/c;Ljava/lang/String;)V

    goto :goto_1
.end method
