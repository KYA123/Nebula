.class final Lcom/fyber/ads/videos/a/c$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/fyber/c/d/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/fyber/ads/videos/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/os/Handler;

.field private b:D

.field private c:Ljava/lang/String;

.field private d:Z


# direct methods
.method constructor <init>(Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 925
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 926
    iput-object p1, p0, Lcom/fyber/ads/videos/a/c$a;->a:Landroid/os/Handler;

    .line 927
    return-void
.end method

.method private a(Lcom/fyber/c/d/a;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 983
    invoke-static {p2}, Lcom/fyber/utils/c;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 984
    new-array v0, v6, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/fyber/c/d/a;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    aput-object p2, v0, v5

    invoke-static {v0}, Lcom/fyber/ads/videos/a/c;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 988
    :goto_0
    const-string v1, "%s(\'play\', {tpn:\'%s\', result:\'%s\', id:\'%s\', %s})"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "javascript:Sponsorpay.MBE.SDKInterface.notify"

    aput-object v3, v2, v4

    const-string v3, "local"

    aput-object v3, v2, v5

    aput-object p1, v2, v6

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/fyber/ads/videos/a/c$a;->c:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x4

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 990
    invoke-direct {p0, v0}, Lcom/fyber/ads/videos/a/c$a;->c(Ljava/lang/String;)V

    .line 991
    return-void

    .line 986
    :cond_0
    new-array v0, v4, [Ljava/lang/String;

    invoke-static {v0}, Lcom/fyber/ads/videos/a/c;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/fyber/ads/videos/a/c$a;)Z
    .locals 1

    .prologue
    .line 918
    iget-boolean v0, p0, Lcom/fyber/ads/videos/a/c$a;->d:Z

    return v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 994
    const-string v0, "RewardedVideoClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "javascript client called with URL:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 995
    invoke-static {p1}, Lcom/fyber/utils/c;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 996
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$a;->a:Landroid/os/Handler;

    invoke-static {v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;)Landroid/os/Message;

    move-result-object v0

    .line 997
    const/16 v1, 0x7b

    iput v1, v0, Landroid/os/Message;->what:I

    .line 998
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 999
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1001
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 932
    sget-object v0, Lcom/fyber/c/d/a;->g:Lcom/fyber/c/d/a;

    const-string v1, "video"

    invoke-direct {p0, v0, v1}, Lcom/fyber/ads/videos/a/c$a;->a(Lcom/fyber/c/d/a;Ljava/lang/String;)V

    .line 933
    return-void
.end method

.method public final a(I)V
    .locals 6

    .prologue
    .line 943
    int-to-double v0, p1

    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double/2addr v0, v2

    iput-wide v0, p0, Lcom/fyber/ads/videos/a/c$a;->b:D

    .line 944
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v1, "%s(\'play\', {tpn:\'%s\', result:\'%s\', duration:\'%.2f\', id:\'%s\'})"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "javascript:Sponsorpay.MBE.SDKInterface.notify"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "local"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    sget-object v4, Lcom/fyber/c/d/a;->a:Lcom/fyber/c/d/a;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-wide v4, p0, Lcom/fyber/ads/videos/a/c$a;->b:D

    .line 945
    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget-object v4, p0, Lcom/fyber/ads/videos/a/c$a;->c:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 944
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 946
    invoke-direct {p0, v0}, Lcom/fyber/ads/videos/a/c$a;->c(Ljava/lang/String;)V

    .line 947
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 969
    sget-object v0, Lcom/fyber/c/d/a;->f:Lcom/fyber/c/d/a;

    invoke-direct {p0, v0, p1}, Lcom/fyber/ads/videos/a/c$a;->a(Lcom/fyber/c/d/a;Ljava/lang/String;)V

    .line 970
    return-void
.end method

.method public final a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 937
    iput-object p1, p0, Lcom/fyber/ads/videos/a/c$a;->c:Ljava/lang/String;

    .line 938
    iput-boolean p2, p0, Lcom/fyber/ads/videos/a/c$a;->d:Z

    .line 939
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 959
    sget-object v0, Lcom/fyber/c/d/a;->d:Lcom/fyber/c/d/a;

    .line 1978
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/fyber/ads/videos/a/c$a;->a(Lcom/fyber/c/d/a;Ljava/lang/String;)V

    .line 960
    return-void
.end method

.method public final b(I)V
    .locals 8

    .prologue
    .line 951
    int-to-double v0, p1

    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double/2addr v0, v2

    .line 952
    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v3, "%s(\'play\', {tpn:\'%s\', result:\'%s\', currentTime:\'%.3f\', duration:\'%.2f\', id:\'%s\'})"

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "javascript:Sponsorpay.MBE.SDKInterface.notify"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "local"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    sget-object v6, Lcom/fyber/c/d/a;->c:Lcom/fyber/c/d/a;

    aput-object v6, v4, v5

    const/4 v5, 0x3

    .line 953
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x4

    iget-wide v6, p0, Lcom/fyber/ads/videos/a/c$a;->b:D

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/fyber/ads/videos/a/c$a;->c:Ljava/lang/String;

    aput-object v1, v4, v0

    .line 952
    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 954
    invoke-direct {p0, v0}, Lcom/fyber/ads/videos/a/c$a;->c(Ljava/lang/String;)V

    .line 955
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 974
    sget-object v0, Lcom/fyber/c/d/a;->b:Lcom/fyber/c/d/a;

    invoke-direct {p0, v0, p1}, Lcom/fyber/ads/videos/a/c$a;->a(Lcom/fyber/c/d/a;Ljava/lang/String;)V

    .line 975
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 964
    sget-object v0, Lcom/fyber/c/d/a;->e:Lcom/fyber/c/d/a;

    .line 2978
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/fyber/ads/videos/a/c$a;->a(Lcom/fyber/c/d/a;Ljava/lang/String;)V

    .line 965
    return-void
.end method
