.class public final Lcom/fyber/ads/videos/a/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/fyber/c/d/d$d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/fyber/ads/videos/a/c$a;
    }
.end annotation


# static fields
.field public static final a:Lcom/fyber/ads/videos/a/c;


# instance fields
.field private b:Landroid/os/Handler;

.field private c:Landroid/os/Handler;

.field private d:Lcom/fyber/ads/videos/RewardedVideoActivity;

.field private e:Landroid/content/Context;

.field private f:Landroid/webkit/WebView;

.field private g:Z

.field private h:Ljava/lang/String;

.field private i:Lcom/fyber/ads/videos/a/e;

.field private j:Lcom/fyber/ads/videos/a/d;

.field private k:Landroid/webkit/WebViewClient;

.field private l:Landroid/webkit/WebChromeClient;

.field private m:Lcom/fyber/c/d/d;

.field private n:Lcom/fyber/ads/videos/a/c$a;

.field private o:Lcom/fyber/ads/videos/b/e;

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Lcom/fyber/g/a/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fyber/g/a/e",
            "<",
            "Landroid/content/Intent;",
            "Lcom/fyber/ads/b;",
            ">;"
        }
    .end annotation
.end field

.field private t:Lcom/fyber/g/a/j;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    new-instance v0, Lcom/fyber/ads/videos/a/c;

    invoke-direct {v0}, Lcom/fyber/ads/videos/a/c;-><init>()V

    sput-object v0, Lcom/fyber/ads/videos/a/c;->a:Lcom/fyber/ads/videos/a/c;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    iput-boolean v1, p0, Lcom/fyber/ads/videos/a/c;->g:Z

    .line 147
    sget-object v0, Lcom/fyber/ads/videos/a/e;->a:Lcom/fyber/ads/videos/a/e;

    iput-object v0, p0, Lcom/fyber/ads/videos/a/c;->i:Lcom/fyber/ads/videos/a/e;

    .line 158
    iput-boolean v1, p0, Lcom/fyber/ads/videos/a/c;->p:Z

    .line 162
    iput-boolean v1, p0, Lcom/fyber/ads/videos/a/c;->r:Z

    .line 169
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "RVTimer"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 170
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 171
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    new-instance v2, Lcom/fyber/ads/videos/a/c$1;

    invoke-direct {v2, p0}, Lcom/fyber/ads/videos/a/c$1;-><init>(Lcom/fyber/ads/videos/a/c;)V

    invoke-direct {v1, v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lcom/fyber/ads/videos/a/c;->b:Landroid/os/Handler;

    .line 191
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, Lcom/fyber/ads/videos/a/c$2;

    invoke-direct {v2, p0}, Lcom/fyber/ads/videos/a/c$2;-><init>(Lcom/fyber/ads/videos/a/c;)V

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/fyber/ads/videos/a/c;->c:Landroid/os/Handler;

    .line 231
    new-instance v0, Lcom/fyber/ads/videos/b/e;

    invoke-direct {v0}, Lcom/fyber/ads/videos/b/e;-><init>()V

    iput-object v0, p0, Lcom/fyber/ads/videos/a/c;->o:Lcom/fyber/ads/videos/b/e;

    .line 232
    return-void
.end method

.method static synthetic a(Lcom/fyber/ads/videos/a/c;Lcom/fyber/ads/videos/a/c$a;)Lcom/fyber/ads/videos/a/c$a;
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/fyber/ads/videos/a/c;->n:Lcom/fyber/ads/videos/a/c$a;

    return-object p1
.end method

.method static synthetic a(Lcom/fyber/ads/videos/a/c;Lcom/fyber/c/d/d;)Lcom/fyber/c/d/d;
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/fyber/ads/videos/a/c;->m:Lcom/fyber/c/d/d;

    return-object p1
.end method

.method private static varargs a(Z[Ljava/lang/String;)Ljava/lang/String;
    .locals 13

    .prologue
    const/4 v1, 0x3

    const/4 v2, 0x2

    const/4 v12, 0x1

    const/4 v6, 0x0

    .line 1025
    if-eqz p0, :cond_0

    move v0, v1

    .line 1026
    :goto_0
    array-length v7, p1

    .line 1027
    rem-int v3, v7, v0

    if-eqz v3, :cond_1

    .line 1028
    const-string v0, "tracking_params:{%s}"

    new-array v1, v12, [Ljava/lang/Object;

    const-string v2, ""

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1045
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 1025
    goto :goto_0

    .line 1030
    :cond_1
    div-int v3, v7, v0

    .line 1031
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v3}, Ljava/util/ArrayList;-><init>(I)V

    move v5, v6

    .line 1033
    :goto_2
    if-ge v5, v7, :cond_4

    .line 1034
    aget-object v9, p1, v5

    .line 1035
    add-int/lit8 v3, v5, 0x1

    aget-object v3, p1, v3

    .line 1036
    if-nez p0, :cond_5

    invoke-static {v3}, Lcom/fyber/utils/c;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1037
    const-string v3, ""

    move-object v4, v3

    .line 1039
    :goto_3
    if-eqz p0, :cond_3

    add-int/lit8 v3, v5, 0x2

    aget-object v3, p1, v3

    .line 1040
    :goto_4
    invoke-static {v9}, Lcom/fyber/utils/c;->b(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1041
    const-string v10, "%1$s:%3$s%2$s%3$s"

    new-array v11, v1, [Ljava/lang/Object;

    aput-object v9, v11, v6

    aput-object v4, v11, v12

    aput-object v3, v11, v2

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v8, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1033
    :cond_2
    add-int v3, v5, v0

    move v5, v3

    goto :goto_2

    .line 1039
    :cond_3
    const-string v3, "\'"

    goto :goto_4

    .line 1045
    :cond_4
    const-string v0, "tracking_params:{%s}"

    new-array v1, v12, [Ljava/lang/Object;

    const-string v2, ", "

    invoke-static {v2, v8}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    move-object v4, v3

    goto :goto_3
.end method

.method static synthetic a([Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 12019
    const/4 v0, 0x0

    invoke-static {v0, p0}, Lcom/fyber/ads/videos/a/c;->a(Z[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 88
    return-object v0
.end method

.method static synthetic a(Lcom/fyber/ads/videos/a/c;)V
    .locals 3

    .prologue
    .line 7235
    new-instance v0, Lcom/fyber/b/d/a$a;

    sget-object v1, Lcom/fyber/ads/b/b;->e:Lcom/fyber/ads/b/b;

    invoke-direct {v0, v1}, Lcom/fyber/b/d/a$a;-><init>(Lcom/fyber/ads/b/b;)V

    const-string v1, "global"

    .line 7236
    invoke-virtual {v0, v1}, Lcom/fyber/b/d/a$a;->b(Ljava/lang/String;)Lcom/fyber/b/b$a;

    move-result-object v0

    check-cast v0, Lcom/fyber/b/d/a$a;

    .line 7237
    iget-object v1, p0, Lcom/fyber/ads/videos/a/c;->t:Lcom/fyber/g/a/j;

    invoke-virtual {v1}, Lcom/fyber/g/a/j;->g()Ljava/lang/String;

    move-result-object v1

    .line 7238
    invoke-static {v1}, Lcom/fyber/utils/c;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 7239
    const-string v2, "placement_id"

    invoke-static {v2, v1}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/fyber/b/d/a$a;->a(Ljava/util/Map;)Lcom/fyber/b/b$a;

    .line 7241
    :cond_0
    iget-object v1, p0, Lcom/fyber/ads/videos/a/c;->t:Lcom/fyber/g/a/j;

    invoke-virtual {v1}, Lcom/fyber/g/a/j;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/fyber/b/d/a$a;->a(Ljava/lang/String;)Lcom/fyber/b/b;

    move-result-object v0

    check-cast v0, Lcom/fyber/b/d/a;

    .line 7242
    invoke-virtual {v0}, Lcom/fyber/b/d/a;->b()V

    .line 88
    return-void
.end method

.method static synthetic a(Lcom/fyber/ads/videos/a/c;ILcom/fyber/g/a/a/e;)V
    .locals 3

    .prologue
    .line 88
    .line 7379
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->b:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 7380
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->t:Lcom/fyber/g/a/j;

    const-string v1, "CONTAINER_CACHE_CONFIG"

    invoke-virtual {v0, v1, p2}, Lcom/fyber/g/a/j;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/fyber/g/a/j;

    .line 7381
    if-lez p1, :cond_0

    const/4 v0, 0x1

    .line 7382
    :goto_0
    if-eqz v0, :cond_1

    .line 7383
    sget-object v0, Lcom/fyber/ads/videos/a/e;->c:Lcom/fyber/ads/videos/a/e;

    invoke-direct {p0, v0}, Lcom/fyber/ads/videos/a/c;->a(Lcom/fyber/ads/videos/a/e;)Z

    .line 7384
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/fyber/ads/videos/a/c;->e:Landroid/content/Context;

    const-class v2, Lcom/fyber/ads/videos/RewardedVideoActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 7385
    const-string v1, "EXTRA_AD_FORMAT"

    sget-object v2, Lcom/fyber/ads/b;->b:Lcom/fyber/ads/b;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 7386
    iget-object v1, p0, Lcom/fyber/ads/videos/a/c;->s:Lcom/fyber/g/a/e;

    invoke-virtual {v1, v0}, Lcom/fyber/g/a/e;->c(Ljava/lang/Object;)V

    .line 7387
    :goto_1
    return-void

    .line 7381
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 7388
    :cond_1
    invoke-direct {p0}, Lcom/fyber/ads/videos/a/c;->g()V

    .line 7389
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->s:Lcom/fyber/g/a/e;

    sget-object v1, Lcom/fyber/ads/b;->b:Lcom/fyber/ads/b;

    invoke-virtual {v0, v1}, Lcom/fyber/g/a/e;->d(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/fyber/ads/videos/a/c;Lcom/fyber/ads/videos/a/d$a;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/fyber/ads/videos/a/c;->a(Lcom/fyber/ads/videos/a/d$a;)V

    return-void
.end method

.method static synthetic a(Lcom/fyber/ads/videos/a/c;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/fyber/ads/videos/a/c;->c(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/fyber/ads/videos/a/c;Ljava/lang/String;Ljava/lang/String;Lcom/fyber/ads/videos/b/c;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 88
    .line 10006
    sget-object v0, Lcom/fyber/ads/videos/b/c;->c:Lcom/fyber/ads/videos/b/c;

    if-ne p3, v0, :cond_0

    .line 10007
    new-array v0, v8, [Ljava/lang/String;

    const-string v1, "adapter_version"

    aput-object v1, v0, v4

    aput-object p2, v0, v5

    const-string v1, "timeout"

    aput-object v1, v0, v6

    const-string v1, "network"

    aput-object v1, v0, v7

    .line 10019
    invoke-static {v4, v0}, Lcom/fyber/ads/videos/a/c;->a(Z[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 10011
    :goto_0
    const-string v1, "%s(\'validate\', {tpn:\'%s\', id:%s, result:\'%s\', %s})"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "javascript:Sponsorpay.MBE.SDKInterface.notify"

    aput-object v3, v2, v4

    aput-object p1, v2, v5

    aput-object p4, v2, v6

    aput-object p3, v2, v7

    aput-object v0, v2, v8

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 10012
    const-string v1, "RewardedVideoClient"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Notifying - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 10013
    invoke-direct {p0, v0}, Lcom/fyber/ads/videos/a/c;->b(Ljava/lang/String;)V

    .line 88
    return-void

    .line 10009
    :cond_0
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "adapter_version"

    aput-object v1, v0, v4

    aput-object p2, v0, v5

    .line 11019
    invoke-static {v4, v0}, Lcom/fyber/ads/videos/a/c;->a(Z[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/fyber/ads/videos/a/d$a;)V
    .locals 3

    .prologue
    .line 479
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->j:Lcom/fyber/ads/videos/a/d;

    if-eqz v0, :cond_0

    .line 480
    const-string v0, "RewardedVideoClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RewardedVideoClientStatus -> "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/fyber/utils/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->j:Lcom/fyber/ads/videos/a/d;

    invoke-interface {v0, p1}, Lcom/fyber/ads/videos/a/d;->a(Lcom/fyber/ads/videos/a/d$a;)V

    .line 483
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 394
    .line 4915
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->t:Lcom/fyber/g/a/j;

    if-nez v0, :cond_1

    move v0, v1

    .line 394
    :goto_0
    if-nez v0, :cond_0

    .line 395
    const-string v0, "STARTED"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 396
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->b:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 397
    sget-object v0, Lcom/fyber/ads/videos/a/e;->d:Lcom/fyber/ads/videos/a/e;

    invoke-direct {p0, v0}, Lcom/fyber/ads/videos/a/c;->a(Lcom/fyber/ads/videos/a/e;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 398
    sget-object v0, Lcom/fyber/ads/videos/a/d$a;->a:Lcom/fyber/ads/videos/a/d$a;

    invoke-direct {p0, v0}, Lcom/fyber/ads/videos/a/c;->a(Lcom/fyber/ads/videos/a/d$a;)V

    .line 425
    :cond_0
    :goto_1
    return-void

    .line 4915
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 400
    :cond_2
    const-string v0, "CLOSE_FINISHED"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 403
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->t:Lcom/fyber/g/a/j;

    if-eqz v0, :cond_6

    .line 404
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->t:Lcom/fyber/g/a/j;

    const-string v2, "SHOULD_NOTIFY_ON_USER_ENGAGED"

    invoke-virtual {v0, v2}, Lcom/fyber/g/a/j;->g(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 405
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->e:Landroid/content/Context;

    if-eqz v0, :cond_3

    .line 5584
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->e:Landroid/content/Context;

    sget-object v2, Lcom/fyber/a$a$a;->i:Lcom/fyber/a$a$a;

    .line 5585
    invoke-static {v2}, Lcom/fyber/utils/x;->a(Lcom/fyber/a$a$a;)Ljava/lang/String;

    move-result-object v2

    .line 5584
    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 5586
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 5863
    :cond_3
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->n:Lcom/fyber/ads/videos/a/c$a;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->n:Lcom/fyber/ads/videos/a/c$a;

    invoke-static {v0}, Lcom/fyber/ads/videos/a/c$a;->a(Lcom/fyber/ads/videos/a/c$a;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->d:Lcom/fyber/ads/videos/RewardedVideoActivity;

    if-eqz v0, :cond_4

    .line 5864
    new-instance v0, Landroid/content/Intent;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/fyber/ads/videos/a/c;->d:Lcom/fyber/ads/videos/RewardedVideoActivity;

    invoke-virtual {v3}, Lcom/fyber/ads/videos/RewardedVideoActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".cache.DONE_PRECACHING"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 5865
    const-string v2, "refresh.interval"

    const/4 v3, 0x5

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 5866
    const-string v2, "RewardedVideoClient"

    const-string v3, "Broadcasting intent with refresh interval = 5"

    invoke-static {v2, v3}, Lcom/fyber/utils/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 5867
    iget-object v2, p0, Lcom/fyber/ads/videos/a/c;->d:Lcom/fyber/ads/videos/RewardedVideoActivity;

    invoke-virtual {v2, v0}, Lcom/fyber/ads/videos/RewardedVideoActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 6590
    :cond_4
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->t:Lcom/fyber/g/a/j;

    const-string v2, "CURRENCY_REQUESTER"

    invoke-virtual {v0, v2}, Lcom/fyber/g/a/j;->e(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fyber/g/h;

    .line 6591
    if-eqz v0, :cond_5

    .line 6592
    iput-boolean v1, p0, Lcom/fyber/ads/videos/a/c;->q:Z

    .line 6595
    invoke-static {v0}, Lcom/fyber/g/h;->a(Lcom/fyber/g/e;)Lcom/fyber/g/h;

    move-result-object v0

    iget-object v1, p0, Lcom/fyber/ads/videos/a/c;->t:Lcom/fyber/g/a/j;

    .line 6596
    invoke-virtual {v1}, Lcom/fyber/g/a/j;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/fyber/g/h;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fyber/g/h;

    iget-object v1, p0, Lcom/fyber/ads/videos/a/c;->h:Ljava/lang/String;

    .line 6597
    invoke-virtual {v0, v1}, Lcom/fyber/g/h;->b(Ljava/lang/String;)Lcom/fyber/g/h;

    move-result-object v0

    .line 6600
    iget-object v1, p0, Lcom/fyber/ads/videos/a/c;->b:Landroid/os/Handler;

    new-instance v2, Lcom/fyber/ads/videos/a/c$6;

    invoke-direct {v2, p0, v0}, Lcom/fyber/ads/videos/a/c$6;-><init>(Lcom/fyber/ads/videos/a/c;Lcom/fyber/g/h;)V

    const-wide/16 v4, 0xbb8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 411
    :cond_5
    invoke-direct {p0}, Lcom/fyber/ads/videos/a/c;->g()V

    .line 413
    :cond_6
    sget-object v0, Lcom/fyber/ads/videos/a/d$a;->b:Lcom/fyber/ads/videos/a/d$a;

    invoke-direct {p0, v0}, Lcom/fyber/ads/videos/a/c;->a(Lcom/fyber/ads/videos/a/d$a;)V

    goto/16 :goto_1

    .line 414
    :cond_7
    const-string v0, "CLOSE_ABORTED"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 416
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->b:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 417
    invoke-direct {p0}, Lcom/fyber/ads/videos/a/c;->g()V

    .line 418
    sget-object v0, Lcom/fyber/ads/videos/a/d$a;->c:Lcom/fyber/ads/videos/a/d$a;

    invoke-direct {p0, v0}, Lcom/fyber/ads/videos/a/c;->a(Lcom/fyber/ads/videos/a/d$a;)V

    goto/16 :goto_1

    .line 419
    :cond_8
    const-string v0, "ERROR"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 420
    sget-object v0, Lcom/fyber/a$a$a;->m:Lcom/fyber/a$a$a;

    invoke-static {v0}, Lcom/fyber/utils/x;->a(Lcom/fyber/a$a$a;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/fyber/ads/videos/a/c;->c(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 421
    :cond_9
    const-string v0, "USER_ENGAGED"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 422
    sget-object v0, Lcom/fyber/ads/videos/a/e;->e:Lcom/fyber/ads/videos/a/e;

    invoke-direct {p0, v0}, Lcom/fyber/ads/videos/a/c;->a(Lcom/fyber/ads/videos/a/e;)Z

    goto/16 :goto_1
.end method

.method static synthetic a(Lcom/fyber/ads/videos/a/c;Z)Z
    .locals 0

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/fyber/ads/videos/a/c;->g:Z

    return p1
.end method

.method private a(Lcom/fyber/ads/videos/a/e;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 570
    iget-object v1, p0, Lcom/fyber/ads/videos/a/c;->i:Lcom/fyber/ads/videos/a/e;

    if-eq v1, p1, :cond_0

    .line 571
    invoke-virtual {p1}, Lcom/fyber/ads/videos/a/e;->ordinal()I

    move-result v1

    iget-object v2, p0, Lcom/fyber/ads/videos/a/c;->i:Lcom/fyber/ads/videos/a/e;

    invoke-virtual {v2}, Lcom/fyber/ads/videos/a/e;->ordinal()I

    move-result v2

    sub-int/2addr v1, v2

    .line 574
    if-gt v1, v0, :cond_0

    .line 575
    iput-object p1, p0, Lcom/fyber/ads/videos/a/c;->i:Lcom/fyber/ads/videos/a/e;

    .line 576
    const-string v1, "RewardedVideoClient"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RewardedVideoClient status -> "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/fyber/ads/videos/a/e;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/fyber/ads/videos/a/c;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->f:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic b(Lcom/fyber/ads/videos/a/c;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/fyber/ads/videos/a/c;->h:Ljava/lang/String;

    return-object p1
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 446
    invoke-static {p1}, Lcom/fyber/utils/c;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 447
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->c:Landroid/os/Handler;

    invoke-static {v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;)Landroid/os/Message;

    move-result-object v0

    .line 448
    const/16 v1, 0x7b

    iput v1, v0, Landroid/os/Message;->what:I

    .line 449
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 450
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 452
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/g/a/j;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->t:Lcom/fyber/g/a/j;

    return-object v0
.end method

.method static synthetic c(Lcom/fyber/ads/videos/a/c;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/fyber/ads/videos/a/c;->a(Ljava/lang/String;)V

    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 540
    iget-boolean v0, p0, Lcom/fyber/ads/videos/a/c;->g:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->f:Landroid/webkit/WebView;

    if-eqz v0, :cond_1

    .line 541
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fyber/ads/videos/a/c;->g:Z

    .line 542
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->m:Lcom/fyber/c/d/d;

    if-eqz v0, :cond_0

    .line 544
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->m:Lcom/fyber/c/d/d;

    invoke-virtual {v0}, Lcom/fyber/c/d/d;->f()V

    .line 545
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->m:Lcom/fyber/c/d/d;

    invoke-virtual {v0}, Lcom/fyber/c/d/d;->g()V

    .line 547
    :cond_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->d:Lcom/fyber/ads/videos/RewardedVideoActivity;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->e:Landroid/content/Context;

    :goto_0
    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 548
    sget-object v0, Lcom/fyber/a$a$a;->l:Lcom/fyber/a$a$a;

    invoke-static {v0}, Lcom/fyber/utils/x;->a(Lcom/fyber/a$a$a;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 549
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget-object v2, Lcom/fyber/a$a$a;->o:Lcom/fyber/a$a$a;

    .line 550
    invoke-static {v2}, Lcom/fyber/utils/x;->a(Lcom/fyber/a$a$a;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/fyber/ads/videos/a/c$5;

    invoke-direct {v3, p0}, Lcom/fyber/ads/videos/a/c$5;-><init>(Lcom/fyber/ads/videos/a/c;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 559
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 561
    :try_start_0
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;
    :try_end_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_0

    .line 567
    :cond_1
    :goto_1
    return-void

    .line 547
    :cond_2
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->d:Lcom/fyber/ads/videos/RewardedVideoActivity;

    goto :goto_0

    .line 563
    :catch_0
    move-exception v0

    iput-boolean v4, p0, Lcom/fyber/ads/videos/a/c;->g:Z

    .line 564
    const-string v0, "RewardedVideoClient"

    const-string v1, "Unable to show the dialog window"

    invoke-static {v0, v1}, Lcom/fyber/utils/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic d(Lcom/fyber/ads/videos/a/c;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fyber/ads/videos/a/c;->f:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic d(Lcom/fyber/ads/videos/a/c;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/fyber/ads/videos/a/c;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic e(Lcom/fyber/ads/videos/a/c;)Landroid/webkit/WebViewClient;
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fyber/ads/videos/a/c;->k:Landroid/webkit/WebViewClient;

    return-object v0
.end method

.method static synthetic f(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/ads/videos/RewardedVideoActivity;
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fyber/ads/videos/a/c;->d:Lcom/fyber/ads/videos/RewardedVideoActivity;

    return-object v0
.end method

.method private g()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 428
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->f:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 429
    const-string v0, "about:blank"

    invoke-direct {p0, v0}, Lcom/fyber/ads/videos/a/c;->b(Ljava/lang/String;)V

    .line 432
    :cond_0
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->m:Lcom/fyber/c/d/d;

    if-eqz v0, :cond_1

    .line 434
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->m:Lcom/fyber/c/d/d;

    .line 6649
    const-string v1, "unknown"

    const-string v2, "forceClose"

    invoke-virtual {v0, v1, v2}, Lcom/fyber/c/d/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    :cond_1
    iput-object v3, p0, Lcom/fyber/ads/videos/a/c;->t:Lcom/fyber/g/a/j;

    .line 439
    iput-object v3, p0, Lcom/fyber/ads/videos/a/c;->h:Ljava/lang/String;

    .line 440
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->b:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 441
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->b:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 442
    sget-object v0, Lcom/fyber/ads/videos/a/e;->a:Lcom/fyber/ads/videos/a/e;

    invoke-direct {p0, v0}, Lcom/fyber/ads/videos/a/c;->a(Lcom/fyber/ads/videos/a/e;)Z

    .line 443
    return-void
.end method

.method static synthetic g(Lcom/fyber/ads/videos/a/c;)Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/fyber/ads/videos/a/c;->q:Z

    return v0
.end method

.method static synthetic h(Lcom/fyber/ads/videos/a/c;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fyber/ads/videos/a/c;->e:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic i(Lcom/fyber/ads/videos/a/c;)V
    .locals 3

    .prologue
    .line 88
    .line 7840
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->f:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->m:Lcom/fyber/c/d/d;

    if-nez v0, :cond_0

    .line 7842
    :try_start_0
    const-string v0, "android.webkit.WebView"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v1, "onPause"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iget-object v1, p0, Lcom/fyber/ads/videos/a/c;->f:Landroid/webkit/WebView;

    const/4 v2, 0x0

    .line 7843
    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 7846
    :cond_0
    :goto_0
    return-void

    .line 7844
    :catch_0
    move-exception v0

    .line 7845
    const-string v1, "RewardedVideoClient"

    const-string v2, "onPause error"

    invoke-static {v1, v2, v0}, Lcom/fyber/utils/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method static synthetic j(Lcom/fyber/ads/videos/a/c;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->e:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic k(Lcom/fyber/ads/videos/a/c;)Landroid/webkit/WebChromeClient;
    .locals 1

    .prologue
    .line 88
    .line 8789
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->l:Landroid/webkit/WebChromeClient;

    if-nez v0, :cond_0

    .line 8790
    new-instance v0, Lcom/fyber/ads/videos/a/c$8;

    invoke-direct {v0, p0}, Lcom/fyber/ads/videos/a/c$8;-><init>(Lcom/fyber/ads/videos/a/c;)V

    iput-object v0, p0, Lcom/fyber/ads/videos/a/c;->l:Landroid/webkit/WebChromeClient;

    .line 8828
    :cond_0
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->l:Landroid/webkit/WebChromeClient;

    .line 88
    return-object v0
.end method

.method static synthetic l(Lcom/fyber/ads/videos/a/c;)Landroid/webkit/WebViewClient;
    .locals 2

    .prologue
    .line 88
    .line 9618
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->k:Landroid/webkit/WebViewClient;

    if-nez v0, :cond_0

    .line 9620
    new-instance v0, Lcom/fyber/ads/videos/a/c$7;

    iget-object v1, p0, Lcom/fyber/ads/videos/a/c;->d:Lcom/fyber/ads/videos/RewardedVideoActivity;

    invoke-direct {v0, p0, v1}, Lcom/fyber/ads/videos/a/c$7;-><init>(Lcom/fyber/ads/videos/a/c;Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/fyber/ads/videos/a/c;->k:Landroid/webkit/WebViewClient;

    .line 9784
    :cond_0
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->k:Landroid/webkit/WebViewClient;

    .line 88
    return-object v0
.end method

.method static synthetic m(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/ads/videos/b/e;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->o:Lcom/fyber/ads/videos/b/e;

    return-object v0
.end method

.method static synthetic n(Lcom/fyber/ads/videos/a/c;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/fyber/ads/videos/a/c;->g()V

    return-void
.end method

.method static synthetic o(Lcom/fyber/ads/videos/a/c;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->b:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic p(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/ads/videos/RewardedVideoActivity;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->d:Lcom/fyber/ads/videos/RewardedVideoActivity;

    return-object v0
.end method

.method static synthetic q(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/ads/videos/a/c$a;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->n:Lcom/fyber/ads/videos/a/c$a;

    return-object v0
.end method

.method static synthetic r(Lcom/fyber/ads/videos/a/c;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->c:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic s(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/c/d/d;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->m:Lcom/fyber/c/d/d;

    return-object v0
.end method

.method static synthetic t(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/ads/videos/a/e;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->i:Lcom/fyber/ads/videos/a/e;

    return-object v0
.end method

.method static synthetic u(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/g/a/e;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->s:Lcom/fyber/g/a/e;

    return-object v0
.end method

.method static synthetic v(Lcom/fyber/ads/videos/a/c;)Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/fyber/ads/videos/a/c;->g:Z

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 352
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->i:Lcom/fyber/ads/videos/a/e;

    sget-object v1, Lcom/fyber/ads/videos/a/e;->e:Lcom/fyber/ads/videos/a/e;

    invoke-virtual {v0, v1}, Lcom/fyber/ads/videos/a/e;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->i:Lcom/fyber/ads/videos/a/e;

    sget-object v1, Lcom/fyber/ads/videos/a/e;->d:Lcom/fyber/ads/videos/a/e;

    invoke-virtual {v0, v1}, Lcom/fyber/ads/videos/a/e;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->i:Lcom/fyber/ads/videos/a/e;

    sget-object v1, Lcom/fyber/ads/videos/a/e;->c:Lcom/fyber/ads/videos/a/e;

    invoke-virtual {v0, v1}, Lcom/fyber/ads/videos/a/e;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 354
    :cond_0
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->i:Lcom/fyber/ads/videos/a/e;

    sget-object v1, Lcom/fyber/ads/videos/a/e;->e:Lcom/fyber/ads/videos/a/e;

    if-ne v0, v1, :cond_2

    .line 355
    const-string v0, "CLOSE_FINISHED"

    invoke-direct {p0, v0}, Lcom/fyber/ads/videos/a/c;->a(Ljava/lang/String;)V

    .line 360
    :cond_1
    :goto_0
    return-void

    .line 357
    :cond_2
    const-string v0, "CLOSE_ABORTED"

    invoke-direct {p0, v0}, Lcom/fyber/ads/videos/a/c;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 853
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fyber/ads/videos/a/c;->m:Lcom/fyber/c/d/d;

    .line 855
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fyber/ads/videos/a/c;->p:Z

    .line 856
    return-void
.end method

.method public final a(Landroid/webkit/ValueCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/webkit/ValueCallback",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 486
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->o:Lcom/fyber/ads/videos/b/e;

    iget-object v1, p0, Lcom/fyber/ads/videos/a/c;->f:Landroid/webkit/WebView;

    invoke-virtual {v0, v1, p1}, Lcom/fyber/ads/videos/b/e;->a(Landroid/webkit/WebView;Landroid/webkit/ValueCallback;)V

    .line 487
    return-void
.end method

.method public final a(Lcom/fyber/g/a/e;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fyber/g/a/e",
            "<",
            "Landroid/content/Intent;",
            "Lcom/fyber/ads/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 461
    iput-object p1, p0, Lcom/fyber/ads/videos/a/c;->s:Lcom/fyber/g/a/e;

    .line 462
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 910
    iput-boolean p1, p0, Lcom/fyber/ads/videos/a/c;->r:Z

    .line 912
    return-void
.end method

.method public final a(Lcom/fyber/ads/videos/RewardedVideoActivity;ZLcom/fyber/g/a/a/d;)Z
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 297
    if-eqz p1, :cond_4

    .line 2375
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->i:Lcom/fyber/ads/videos/a/e;

    invoke-virtual {v0}, Lcom/fyber/ads/videos/a/e;->a()Z

    move-result v0

    .line 298
    if-eqz v0, :cond_3

    .line 3326
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Lcom/fyber/g/a/a/d;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p3}, Lcom/fyber/g/a/a/d;->e()I

    move-result v0

    if-lez v0, :cond_2

    .line 3328
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "container_fill_cached"

    aput-object v1, v0, v3

    const-string v1, "true"

    aput-object v1, v0, v2

    const-string v1, ""

    aput-object v1, v0, v8

    const-string v1, "container_fill_cache_age"

    aput-object v1, v0, v9

    .line 3329
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {p3}, Lcom/fyber/g/a/a/d;->b()J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v10

    const/4 v1, 0x5

    const-string v4, ""

    aput-object v4, v0, v1

    .line 3872
    :goto_0
    invoke-static {}, Lcom/fyber/cache/a;->a()Lcom/fyber/cache/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/fyber/cache/a;->b()Lcom/fyber/cache/a/b;

    move-result-object v4

    .line 3873
    const-string v1, ""

    .line 3874
    if-eqz v4, :cond_0

    sget-object v5, Lcom/fyber/cache/a/b;->a:Lcom/fyber/cache/a/b;

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 3875
    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v5, ", cache_config_id:\'%s\'"

    new-array v6, v2, [Ljava/lang/Object;

    invoke-virtual {v4}, Lcom/fyber/cache/a/b;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v3

    invoke-static {v1, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 3877
    :cond_0
    invoke-static {v2, v0}, Lcom/fyber/ads/videos/a/c;->a(Z[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3878
    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v5, "javascript:Sponsorpay.MBE.SDKInterface.do_start({cached_ad_ids:%s, downloaded_videos_count:%d%s, %s})"

    new-array v6, v10, [Ljava/lang/Object;

    invoke-static {}, Lcom/fyber/cache/a;->a()Lcom/fyber/cache/a;

    move-result-object v7

    invoke-virtual {v7}, Lcom/fyber/cache/a;->d()Lcom/fyber/cache/a/f;

    invoke-static {}, Lcom/fyber/cache/a/f;->d()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    .line 3879
    invoke-static {}, Lcom/fyber/cache/a;->a()Lcom/fyber/cache/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/fyber/cache/a;->d()Lcom/fyber/cache/a/f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/fyber/cache/a/f;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v2

    aput-object v1, v6, v8

    aput-object v0, v6, v9

    .line 3878
    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 302
    const-string v1, "RewardedVideoClient"

    invoke-static {v1, v0}, Lcom/fyber/utils/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    invoke-direct {p0, v0}, Lcom/fyber/ads/videos/a/c;->b(Ljava/lang/String;)V

    .line 305
    invoke-static {}, Lcom/fyber/cache/a;->a()Lcom/fyber/cache/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fyber/cache/a;->d()Lcom/fyber/cache/a/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fyber/cache/a/f;->c()V

    .line 307
    iput-object p1, p0, Lcom/fyber/ads/videos/a/c;->d:Lcom/fyber/ads/videos/RewardedVideoActivity;

    .line 308
    if-nez p2, :cond_1

    .line 4336
    invoke-static {}, Lcom/fyber/a;->c()Lcom/fyber/a$b;

    new-instance v0, Lcom/fyber/ads/videos/a/c$3;

    invoke-direct {v0, p0, p1}, Lcom/fyber/ads/videos/a/c$3;-><init>(Lcom/fyber/ads/videos/a/c;Lcom/fyber/ads/videos/RewardedVideoActivity;)V

    invoke-static {v0}, Lcom/fyber/a$b;->b(Ljava/lang/Runnable;)V

    .line 4455
    :cond_1
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->b:Landroid/os/Handler;

    const-wide/16 v4, 0x2710

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    move v0, v2

    .line 322
    :goto_1
    return v0

    .line 3331
    :cond_2
    new-array v0, v3, [Ljava/lang/String;

    goto/16 :goto_0

    .line 315
    :cond_3
    const-string v0, "RewardedVideoClient"

    const-string v1, "RewardedVideoClient is not ready to show offers. Call requestOffers() and wait until your listener is called with the confirmation that offers have been received."

    invoke-static {v0, v1}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    move v0, v3

    .line 322
    goto :goto_1

    .line 320
    :cond_4
    const-string v0, "RewardedVideoClient"

    const-string v1, "The provided activity is null, RewardedVideoClient cannot start the engagement."

    invoke-static {v0, v1}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public final a(Lcom/fyber/ads/videos/a/d;)Z
    .locals 1

    .prologue
    .line 474
    iput-object p1, p0, Lcom/fyber/ads/videos/a/c;->j:Lcom/fyber/ads/videos/a/d;

    .line 475
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Lcom/fyber/g/a/j;Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 259
    invoke-virtual {p0}, Lcom/fyber/ads/videos/a/c;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 261
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->f:Landroid/webkit/WebView;

    if-nez v0, :cond_0

    .line 1491
    iput-object p2, p0, Lcom/fyber/ads/videos/a/c;->e:Landroid/content/Context;

    .line 1492
    iput-boolean v1, p0, Lcom/fyber/ads/videos/a/c;->q:Z

    .line 1510
    new-instance v0, Lcom/fyber/ads/videos/a/c$4;

    invoke-direct {v0, p0}, Lcom/fyber/ads/videos/a/c$4;-><init>(Lcom/fyber/ads/videos/a/c;)V

    .line 1498
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    if-ne v2, v3, :cond_1

    .line 1499
    invoke-interface {v0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/fyber/ads/videos/a/c;->f:Landroid/webkit/WebView;

    .line 1506
    :goto_0
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->f:Landroid/webkit/WebView;

    const-string v2, "videoPlayerWebview"

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 264
    :cond_0
    iput-boolean v1, p0, Lcom/fyber/ads/videos/a/c;->p:Z

    .line 2278
    sget-object v0, Lcom/fyber/ads/videos/a/e;->b:Lcom/fyber/ads/videos/a/e;

    invoke-direct {p0, v0}, Lcom/fyber/ads/videos/a/c;->a(Lcom/fyber/ads/videos/a/e;)Z

    .line 2279
    iput-object p1, p0, Lcom/fyber/ads/videos/a/c;->t:Lcom/fyber/g/a/j;

    .line 2280
    invoke-virtual {p1}, Lcom/fyber/g/a/j;->d()Lcom/fyber/g/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fyber/g/a/l;->a()Ljava/lang/String;

    move-result-object v0

    .line 2282
    const-string v1, "RewardedVideoClient"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Loading URL: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2283
    invoke-direct {p0, v0}, Lcom/fyber/ads/videos/a/c;->b(Ljava/lang/String;)V

    .line 2285
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->b:Landroid/os/Handler;

    const/4 v1, 0x2

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 268
    const/4 v0, 0x1

    .line 273
    :goto_1
    return v0

    .line 1502
    :cond_1
    new-instance v2, Ljava/util/concurrent/FutureTask;

    invoke-direct {v2, v0}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 1503
    invoke-static {}, Lcom/fyber/a;->c()Lcom/fyber/a$b;

    invoke-static {v2}, Lcom/fyber/a$b;->b(Ljava/lang/Runnable;)V

    .line 1504
    invoke-virtual {v2}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/fyber/ads/videos/a/c;->f:Landroid/webkit/WebView;

    goto :goto_0

    .line 270
    :cond_2
    const-string v0, "RewardedVideoClient"

    const-string v2, "RewardedVideoClient cannot request offers at this point. It might be requesting offers right now or an offer might be currently being presented to the user."

    invoke-static {v0, v2}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 273
    goto :goto_1
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 367
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->i:Lcom/fyber/ads/videos/a/e;

    invoke-virtual {v0}, Lcom/fyber/ads/videos/a/e;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/fyber/ads/videos/a/c;->r:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->i:Lcom/fyber/ads/videos/a/e;

    invoke-virtual {v0}, Lcom/fyber/ads/videos/a/e;->a()Z

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 833
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->c:Landroid/os/Handler;

    invoke-static {v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;)Landroid/os/Message;

    move-result-object v0

    .line 834
    const/16 v1, 0x20a

    iput v1, v0, Landroid/os/Message;->what:I

    .line 835
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 836
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 888
    iget-boolean v0, p0, Lcom/fyber/ads/videos/a/c;->p:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->i:Lcom/fyber/ads/videos/a/e;

    sget-object v1, Lcom/fyber/ads/videos/a/e;->a:Lcom/fyber/ads/videos/a/e;

    if-ne v0, v1, :cond_0

    .line 889
    sget-object v0, Lcom/fyber/ads/videos/a/d$a;->c:Lcom/fyber/ads/videos/a/d$a;

    invoke-direct {p0, v0}, Lcom/fyber/ads/videos/a/c;->a(Lcom/fyber/ads/videos/a/d$a;)V

    .line 891
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 894
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->i:Lcom/fyber/ads/videos/a/e;

    sget-object v1, Lcom/fyber/ads/videos/a/e;->d:Lcom/fyber/ads/videos/a/e;

    if-ne v0, v1, :cond_0

    .line 896
    const-string v0, "RewardedVideoClient"

    const-string v1, "Connection has been lost"

    invoke-static {v0, v1}, Lcom/fyber/utils/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 897
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c;->b:Landroid/os/Handler;

    new-instance v1, Lcom/fyber/ads/videos/a/c$9;

    invoke-direct {v1, p0}, Lcom/fyber/ads/videos/a/c$9;-><init>(Lcom/fyber/ads/videos/a/c;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 904
    :cond_0
    return-void
.end method
