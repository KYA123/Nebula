.class public final Lcom/fyber/g/a/a/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/fyber/mediation/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Exception;",
        "I:",
        "Lcom/fyber/ads/b/a",
        "<**>;>",
        "Ljava/lang/Object;",
        "Lcom/fyber/mediation/f",
        "<TR;TE;>;"
    }
.end annotation


# instance fields
.field protected a:Lcom/fyber/utils/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fyber/utils/h",
            "<TR;TE;>;"
        }
    .end annotation
.end field

.field private b:Lcom/fyber/mediation/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fyber/mediation/e",
            "<TR;TE;TI;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/fyber/mediation/e;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fyber/mediation/e",
            "<TR;TE;TI;>;)V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/fyber/g/a/a/j;->b:Lcom/fyber/mediation/e;

    .line 38
    iget-object v0, p0, Lcom/fyber/g/a/a/j;->b:Lcom/fyber/mediation/e;

    invoke-interface {v0, p0}, Lcom/fyber/mediation/e;->a(Lcom/fyber/mediation/f;)V

    .line 39
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/fyber/ads/b/a;)Ljava/util/concurrent/Future;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "TI;)",
            "Ljava/util/concurrent/Future",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 72
    new-instance v0, Lcom/fyber/utils/h;

    invoke-direct {v0}, Lcom/fyber/utils/h;-><init>()V

    iput-object v0, p0, Lcom/fyber/g/a/a/j;->a:Lcom/fyber/utils/h;

    .line 73
    invoke-static {}, Lcom/fyber/a;->c()Lcom/fyber/a$b;

    move-result-object v0

    iget-object v1, p0, Lcom/fyber/g/a/a/j;->a:Lcom/fyber/utils/h;

    invoke-virtual {v0, v1}, Lcom/fyber/a$b;->a(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 75
    iget-object v1, p0, Lcom/fyber/g/a/a/j;->b:Lcom/fyber/mediation/e;

    invoke-interface {v1, p1, p2}, Lcom/fyber/mediation/e;->a(Landroid/content/Context;Ljava/lang/Object;)V

    .line 76
    return-object v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/fyber/g/a/a/j;->a:Lcom/fyber/utils/h;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/fyber/g/a/a/j;->a:Lcom/fyber/utils/h;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/fyber/utils/h;->a(Ljava/lang/Object;)V

    .line 53
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lcom/fyber/g/a/a/j;->a:Lcom/fyber/utils/h;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/fyber/g/a/a/j;->a:Lcom/fyber/utils/h;

    invoke-virtual {v0, p1}, Lcom/fyber/utils/h;->a(Ljava/lang/Object;)V

    .line 46
    :cond_0
    return-void
.end method

.method public final synthetic b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 23
    check-cast p1, Ljava/lang/Exception;

    .line 1057
    iget-object v0, p0, Lcom/fyber/g/a/a/j;->a:Lcom/fyber/utils/h;

    if-eqz v0, :cond_0

    .line 1058
    iget-object v0, p0, Lcom/fyber/g/a/a/j;->a:Lcom/fyber/utils/h;

    invoke-virtual {v0, p1}, Lcom/fyber/utils/h;->a(Ljava/lang/Exception;)V

    .line 23
    :cond_0
    return-void
.end method
