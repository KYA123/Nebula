.class public final Lcom/fyber/g/a/a/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/fyber/g/a/a/c;


# static fields
.field private static final a:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-string v0, "(lat|longt)=[^&]*&?"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/fyber/g/a/a/o;->a:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/fyber/g/a/l;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/fyber/g/a/l;->d()Ljava/util/Map;

    move-result-object v0

    const-string v1, "X-User-Data"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 76
    invoke-static {v0}, Lcom/fyber/utils/c;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fyber/g/a/l;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 77
    const-string v1, "UserDataCacheValidator"

    const-string v2, "Auto location enabled - removing lat/longt values, if any..."

    invoke-static {v1, v2}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    sget-object v1, Lcom/fyber/g/a/a/o;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "&$"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 81
    :cond_0
    return-object v0
.end method

.method private static a(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 93
    invoke-static {p1}, Lcom/fyber/utils/c;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    const/4 v0, 0x0

    .line 101
    :goto_0
    return-object v0

    .line 96
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 97
    invoke-virtual {p0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 98
    :goto_1
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 99
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 101
    :cond_1
    const-string v1, "&"

    invoke-static {v1, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 54
    invoke-static {p0}, Lcom/fyber/utils/c;->a(Ljava/lang/String;)Z

    move-result v0

    .line 55
    invoke-static {p1}, Lcom/fyber/utils/c;->a(Ljava/lang/String;)Z

    move-result v3

    .line 58
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 59
    const-string v0, "UserDataCacheValidator"

    const-string v2, "User data not provided for both requests - valid. Proceeding..."

    invoke-static {v0, v2}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 71
    :goto_0
    return v0

    .line 63
    :cond_0
    if-eq v0, v3, :cond_1

    .line 64
    const-string v0, "UserDataCacheValidator"

    const-string v1, "User data was not provided for one of the requests - invalid"

    invoke-static {v0, v1}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 65
    goto :goto_0

    .line 69
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 70
    const-string v4, "UserDataCacheValidator"

    const-string v5, "User data does %smatch for both requests - %s"

    const/4 v0, 0x2

    new-array v6, v0, [Ljava/lang/Object;

    if-eqz v3, :cond_2

    const-string v0, ""

    :goto_1
    aput-object v0, v6, v2

    if-eqz v3, :cond_3

    const-string v0, "valid. Proceeding..."

    :goto_2
    aput-object v0, v6, v1

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v3

    .line 71
    goto :goto_0

    .line 70
    :cond_2
    const-string v0, "not "

    goto :goto_1

    :cond_3
    const-string v0, "invalid"

    goto :goto_2
.end method


# virtual methods
.method public final a(Lcom/fyber/g/a/a/d;Lcom/fyber/g/a/j;)Z
    .locals 5

    .prologue
    .line 30
    const-string v0, "UserDataCacheValidator"

    const-string v1, "Checking user data..."

    invoke-static {v0, v1}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1085
    invoke-virtual {p1}, Lcom/fyber/g/a/a/d;->c()Lcom/fyber/g/a/j;

    move-result-object v0

    const-string v1, "CONTAINER_CACHE_CONFIG"

    invoke-virtual {v0, v1}, Lcom/fyber/g/a/j;->e(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fyber/g/a/a/e;

    .line 1086
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/fyber/g/a/a/e;->d()[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/fyber/g/a/a/e;->d()[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-lez v1, :cond_0

    .line 1087
    invoke-virtual {v0}, Lcom/fyber/g/a/a/e;->d()[Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 35
    :goto_0
    invoke-virtual {p2}, Lcom/fyber/g/a/j;->d()Lcom/fyber/g/a/l;

    move-result-object v0

    invoke-static {v0}, Lcom/fyber/g/a/a/o;->a(Lcom/fyber/g/a/l;)Ljava/lang/String;

    move-result-object v2

    .line 36
    invoke-virtual {p1}, Lcom/fyber/g/a/a/d;->c()Lcom/fyber/g/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fyber/g/a/j;->d()Lcom/fyber/g/a/l;

    move-result-object v0

    invoke-static {v0}, Lcom/fyber/g/a/a/o;->a(Lcom/fyber/g/a/l;)Ljava/lang/String;

    move-result-object v3

    .line 39
    array-length v0, v1

    if-nez v0, :cond_1

    .line 40
    invoke-static {v2, v3}, Lcom/fyber/g/a/a/o;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 50
    :goto_1
    return v0

    .line 1089
    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    move-object v1, v0

    goto :goto_0

    .line 1113
    :cond_1
    invoke-virtual {p1}, Lcom/fyber/g/a/a/d;->c()Lcom/fyber/g/a/j;

    move-result-object v0

    const-string v4, "PATTERN_KEY"

    invoke-virtual {v0, v4}, Lcom/fyber/g/a/j;->e(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/regex/Pattern;

    .line 1114
    if-nez v0, :cond_2

    .line 1115
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "("

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "|"

    invoke-static {v4, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1117
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=[^&]*"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 1118
    invoke-virtual {p1}, Lcom/fyber/g/a/a/d;->c()Lcom/fyber/g/a/j;

    move-result-object v1

    const-string v4, "PATTERN_KEY"

    invoke-virtual {v1, v4, v0}, Lcom/fyber/g/a/j;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/fyber/g/a/j;

    :cond_2
    move-object v1, v0

    .line 1133
    if-nez p1, :cond_4

    .line 1134
    const/4 v0, 0x0

    .line 48
    :cond_3
    :goto_2
    invoke-static {v1, v2}, Lcom/fyber/g/a/a/o;->a(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 50
    invoke-static {v1, v0}, Lcom/fyber/g/a/a/o;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 1137
    :cond_4
    invoke-virtual {p1}, Lcom/fyber/g/a/a/d;->c()Lcom/fyber/g/a/j;

    move-result-object v0

    const-string v4, "MATCHED_USER_DATA_KEY"

    invoke-virtual {v0, v4}, Lcom/fyber/g/a/j;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1138
    if-nez v0, :cond_3

    .line 1139
    invoke-static {v1, v3}, Lcom/fyber/g/a/a/o;->a(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1140
    invoke-virtual {p1}, Lcom/fyber/g/a/a/d;->c()Lcom/fyber/g/a/j;

    move-result-object v3

    const-string v4, "MATCHED_USER_DATA_KEY"

    invoke-virtual {v3, v4, v0}, Lcom/fyber/g/a/j;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/fyber/g/a/j;

    goto :goto_2
.end method
