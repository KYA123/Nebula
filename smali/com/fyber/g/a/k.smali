.class public final Lcom/fyber/g/a/k;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/fyber/g/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/fyber/g/a/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Lcom/fyber/g/a/b;

    invoke-direct {v0, p1}, Lcom/fyber/g/a/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/fyber/g/a/k;->b:Lcom/fyber/g/a/b;

    .line 22
    new-instance v0, Landroid/util/SparseArray;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Lcom/fyber/g/a/k;->a:Landroid/util/SparseArray;

    .line 23
    iget-object v0, p0, Lcom/fyber/g/a/k;->a:Landroid/util/SparseArray;

    const/4 v1, 0x0

    new-instance v2, Lcom/fyber/g/a/d;

    invoke-direct {v2}, Lcom/fyber/g/a/d;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 24
    iget-object v0, p0, Lcom/fyber/g/a/k;->a:Landroid/util/SparseArray;

    const/4 v1, 0x1

    new-instance v2, Lcom/fyber/g/a/h;

    invoke-direct {v2}, Lcom/fyber/g/a/h;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 25
    iget-object v0, p0, Lcom/fyber/g/a/k;->a:Landroid/util/SparseArray;

    const/4 v1, 0x2

    new-instance v2, Lcom/fyber/g/a/m;

    invoke-direct {v2, p1}, Lcom/fyber/g/a/m;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 26
    iget-object v0, p0, Lcom/fyber/g/a/k;->a:Landroid/util/SparseArray;

    const/4 v1, 0x3

    new-instance v2, Lcom/fyber/g/a/a;

    invoke-direct {v2}, Lcom/fyber/g/a/a;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 27
    iget-object v0, p0, Lcom/fyber/g/a/k;->a:Landroid/util/SparseArray;

    const/4 v1, 0x4

    new-instance v2, Lcom/fyber/g/a/g;

    invoke-direct {v2}, Lcom/fyber/g/a/g;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 28
    iget-object v0, p0, Lcom/fyber/g/a/k;->a:Landroid/util/SparseArray;

    const/4 v1, 0x5

    new-instance v2, Lcom/fyber/g/a/p;

    invoke-direct {v2}, Lcom/fyber/g/a/p;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 29
    iget-object v0, p0, Lcom/fyber/g/a/k;->a:Landroid/util/SparseArray;

    const/4 v1, 0x6

    new-instance v2, Lcom/fyber/g/a/n;

    invoke-direct {v2}, Lcom/fyber/g/a/n;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 30
    iget-object v0, p0, Lcom/fyber/g/a/k;->a:Landroid/util/SparseArray;

    const/4 v1, 0x7

    new-instance v2, Lcom/fyber/g/a/i;

    invoke-direct {v2}, Lcom/fyber/g/a/i;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 31
    iget-object v0, p0, Lcom/fyber/g/a/k;->a:Landroid/util/SparseArray;

    const/16 v1, 0x8

    new-instance v2, Lcom/fyber/g/a/f;

    invoke-direct {v2}, Lcom/fyber/g/a/f;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 32
    iget-object v0, p0, Lcom/fyber/g/a/k;->a:Landroid/util/SparseArray;

    const/16 v1, 0x9

    new-instance v2, Lcom/fyber/g/a/o;

    invoke-direct {v2}, Lcom/fyber/g/a/o;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 33
    return-void
.end method


# virtual methods
.method public final a(Lcom/fyber/g/a/j;Lcom/fyber/g/a/l;)V
    .locals 5

    .prologue
    .line 36
    iget-object v0, p0, Lcom/fyber/g/a/k;->b:Lcom/fyber/g/a/b;

    invoke-virtual {v0, p1, p2}, Lcom/fyber/g/a/b;->a(Lcom/fyber/g/a/j;Lcom/fyber/g/a/l;)V

    .line 37
    iget-object v0, p1, Lcom/fyber/g/a/j;->d:[I

    if-eqz v0, :cond_0

    .line 38
    iget-object v2, p1, Lcom/fyber/g/a/j;->d:[I

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget v0, v2, v1

    .line 39
    iget-object v4, p0, Lcom/fyber/g/a/k;->a:Landroid/util/SparseArray;

    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fyber/g/a/c;

    invoke-interface {v0, p1, p2}, Lcom/fyber/g/a/c;->a(Lcom/fyber/g/a/j;Lcom/fyber/g/a/l;)V

    .line 38
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 42
    :cond_0
    return-void
.end method
