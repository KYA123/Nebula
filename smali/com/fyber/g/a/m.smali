.class public final Lcom/fyber/g/a/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/fyber/g/a/c;


# instance fields
.field private final a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {p1}, Lcom/fyber/g/a/m;->a(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/fyber/g/a/m;->a:Z

    .line 24
    return-void
.end method

.method private static a(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 27
    if-eqz p0, :cond_2

    .line 29
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x80

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 30
    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 35
    const-string v3, "FYBEnableSSLRewardedVideo"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_1

    .line 37
    :goto_0
    if-eqz v0, :cond_0

    .line 38
    const-string v2, "RewardedVideoCustomizer"

    const-string v3, "Manifest metadata - disabling SSL"

    invoke-static {v2, v3}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v0, v1

    .line 35
    goto :goto_0

    .line 42
    :catch_0
    move-exception v0

    .line 43
    :goto_2
    const-string v2, "RewardedVideoCustomizer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to load meta-data from Manifest: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/fyber/utils/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move v0, v1

    .line 46
    goto :goto_1

    .line 42
    :catch_1
    move-exception v0

    goto :goto_2
.end method


# virtual methods
.method public final a(Lcom/fyber/g/a/j;Lcom/fyber/g/a/l;)V
    .locals 3

    .prologue
    .line 51
    invoke-virtual {p2}, Lcom/fyber/g/a/l;->c()Lcom/fyber/utils/y;

    move-result-object v0

    const-string v1, "rewarded"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Lcom/fyber/utils/y;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/fyber/utils/y;

    move-result-object v0

    const-string v1, "ad_format"

    const-string v2, "video"

    .line 52
    invoke-virtual {v0, v1, v2}, Lcom/fyber/utils/y;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/fyber/utils/y;

    .line 54
    iget-boolean v0, p0, Lcom/fyber/g/a/m;->a:Z

    if-eqz v0, :cond_0

    .line 55
    const-string v0, "RewardedVideoCustomizer"

    const-string v1, "Manifest metadata - disabling SSL"

    invoke-static {v0, v1}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    invoke-virtual {p2}, Lcom/fyber/g/a/l;->c()Lcom/fyber/utils/y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fyber/utils/y;->d()Lcom/fyber/utils/y;

    .line 59
    :cond_0
    const-string v0, "TRACKING_URL_KEY"

    const-string v1, "rewarded_video_tracking"

    invoke-virtual {p1, v0, v1}, Lcom/fyber/g/a/j;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/fyber/g/a/j;

    .line 63
    return-void
.end method
