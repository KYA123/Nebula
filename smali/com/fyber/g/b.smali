.class public Lcom/fyber/g/b;
.super Lcom/fyber/g/e;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fyber/g/e",
        "<",
        "Lcom/fyber/g/b;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>(Lcom/fyber/g/c;)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/fyber/g/e;-><init>(Lcom/fyber/g/a;)V

    .line 97
    return-void
.end method

.method public static a(Lcom/fyber/g/c;)Lcom/fyber/g/b;
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/fyber/g/b;

    invoke-direct {v0, p0}, Lcom/fyber/g/b;-><init>(Lcom/fyber/g/c;)V

    return-object v0
.end method


# virtual methods
.method protected final a()Lcom/fyber/g/a/e;
    .locals 4

    .prologue
    .line 75
    new-instance v0, Lcom/fyber/g/b$1;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Lcom/fyber/g/c;

    aput-object v3, v1, v2

    invoke-direct {v0, p0, v1}, Lcom/fyber/g/b$1;-><init>(Lcom/fyber/g/b;[Ljava/lang/Class;)V

    return-object v0
.end method

.method public a(Z)Lcom/fyber/g/b;
    .locals 3

    .prologue
    .line 56
    iget-object v0, p0, Lcom/fyber/g/b;->b:Lcom/fyber/g/a/j;

    const-string v1, "CLOSE_ON_REDIRECT"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/fyber/g/a/j;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/fyber/g/a/j;

    .line 57
    return-object p0
.end method

.method protected final a(Landroid/content/Context;Lcom/fyber/g/a/j;)V
    .locals 4

    .prologue
    .line 63
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/fyber/ads/ofw/OfferWallActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "EXTRA_SHOULD_CLOSE_ON_REDIRECT_KEY"

    const-string v2, "CLOSE_ON_REDIRECT"

    .line 65
    invoke-virtual {p2, v2}, Lcom/fyber/g/a/j;->g(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_URL"

    .line 66
    invoke-virtual {p2}, Lcom/fyber/g/a/j;->d()Lcom/fyber/g/a/l;

    move-result-object v2

    invoke-virtual {v2}, Lcom/fyber/g/a/l;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "EXTRA_USER_SEGMENTS"

    .line 67
    invoke-virtual {p2}, Lcom/fyber/g/a/j;->d()Lcom/fyber/g/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fyber/g/a/l;->d()Ljava/util/Map;

    move-result-object v0

    const-string v3, "X-User-Data"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_AD_FORMAT"

    sget-object v2, Lcom/fyber/ads/b;->a:Lcom/fyber/ads/b;

    .line 68
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    .line 70
    iget-object v1, p0, Lcom/fyber/g/b;->a:Lcom/fyber/g/a/e;

    invoke-virtual {v1, v0}, Lcom/fyber/g/a/e;->c(Ljava/lang/Object;)V

    .line 71
    return-void
.end method

.method protected final b()V
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/fyber/g/b;->b:Lcom/fyber/g/a/j;

    const-string v1, "ofw"

    invoke-virtual {v0, v1}, Lcom/fyber/g/a/j;->a(Ljava/lang/String;)Lcom/fyber/g/a/j;

    move-result-object v0

    const/4 v1, 0x0

    .line 91
    invoke-virtual {v0, v1}, Lcom/fyber/g/a/j;->a(Z)Lcom/fyber/g/a/j;

    move-result-object v0

    const/4 v1, 0x4

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    .line 92
    invoke-virtual {v0, v1}, Lcom/fyber/g/a/j;->a([I)Lcom/fyber/g/a/j;

    .line 93
    return-void

    .line 91
    nop

    :array_0
    .array-data 4
        0x9
        0x8
        0x1
        0x0
    .end array-data
.end method

.method protected final bridge synthetic c()Ljava/lang/Object;
    .locals 0

    .prologue
    .line 25
    return-object p0
.end method
