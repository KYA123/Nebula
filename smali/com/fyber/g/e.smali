.class public abstract Lcom/fyber/g/e;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field protected a:Lcom/fyber/g/a/e;

.field protected b:Lcom/fyber/g/a/j;


# direct methods
.method protected constructor <init>(Lcom/fyber/g/a;)V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    if-nez p1, :cond_0

    .line 56
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "callback cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_0
    invoke-virtual {p0}, Lcom/fyber/g/e;->a()Lcom/fyber/g/a/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/fyber/g/a/e;->a(Lcom/fyber/g/a;)Lcom/fyber/g/a/e;

    move-result-object v0

    iput-object v0, p0, Lcom/fyber/g/e;->a:Lcom/fyber/g/a/e;

    .line 59
    new-instance v0, Lcom/fyber/g/a/j;

    invoke-direct {v0}, Lcom/fyber/g/a/j;-><init>()V

    iput-object v0, p0, Lcom/fyber/g/e;->b:Lcom/fyber/g/a/j;

    .line 60
    invoke-virtual {p0}, Lcom/fyber/g/e;->b()V

    .line 61
    return-void
.end method

.method protected constructor <init>(Lcom/fyber/g/e;)V
    .locals 2

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    if-nez p1, :cond_0

    .line 69
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "requester cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :cond_0
    invoke-virtual {p0}, Lcom/fyber/g/e;->a()Lcom/fyber/g/a/e;

    move-result-object v0

    iget-object v1, p1, Lcom/fyber/g/e;->a:Lcom/fyber/g/a/e;

    invoke-virtual {v0, v1}, Lcom/fyber/g/a/e;->a(Lcom/fyber/g/a/e;)Lcom/fyber/g/a/e;

    move-result-object v0

    iput-object v0, p0, Lcom/fyber/g/e;->a:Lcom/fyber/g/a/e;

    .line 72
    new-instance v0, Lcom/fyber/g/a/j;

    iget-object v1, p1, Lcom/fyber/g/e;->b:Lcom/fyber/g/a/j;

    invoke-direct {v0, v1}, Lcom/fyber/g/a/j;-><init>(Lcom/fyber/g/a/j;)V

    iput-object v0, p0, Lcom/fyber/g/e;->b:Lcom/fyber/g/a/j;

    .line 73
    invoke-virtual {p0}, Lcom/fyber/g/e;->b()V

    .line 74
    return-void
.end method


# virtual methods
.method protected abstract a()Lcom/fyber/g/a/e;
.end method

.method public a(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 131
    iget-object v0, p0, Lcom/fyber/g/e;->b:Lcom/fyber/g/a/j;

    invoke-virtual {v0, p1}, Lcom/fyber/g/a/j;->b(Ljava/lang/String;)Lcom/fyber/g/a/j;

    .line 132
    invoke-virtual {p0}, Lcom/fyber/g/e;->c()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lcom/fyber/g/e;->b:Lcom/fyber/g/a/j;

    invoke-virtual {v0, p1, p2}, Lcom/fyber/g/a/j;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/fyber/g/a/j;

    .line 100
    invoke-virtual {p0}, Lcom/fyber/g/e;->c()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 169
    .line 1192
    if-nez p1, :cond_1

    .line 1193
    iget-object v1, p0, Lcom/fyber/g/e;->a:Lcom/fyber/g/a/e;

    sget-object v2, Lcom/fyber/g/d;->f:Lcom/fyber/g/d;

    invoke-virtual {v1, v2}, Lcom/fyber/g/a/e;->a(Lcom/fyber/g/d;)V

    .line 169
    :goto_0
    if-eqz v0, :cond_0

    .line 172
    new-instance v0, Lcom/fyber/g/e$1;

    invoke-direct {v0, p0, p1}, Lcom/fyber/g/e$1;-><init>(Lcom/fyber/g/e;Landroid/content/Context;)V

    .line 185
    invoke-static {}, Lcom/fyber/a;->c()Lcom/fyber/a$b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/fyber/a$b;->a(Ljava/lang/Runnable;)V

    .line 187
    :cond_0
    return-void

    .line 1196
    :cond_1
    invoke-static {}, Lcom/fyber/utils/m;->f()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1197
    iget-object v1, p0, Lcom/fyber/g/e;->a:Lcom/fyber/g/a/e;

    sget-object v2, Lcom/fyber/g/d;->a:Lcom/fyber/g/d;

    invoke-virtual {v1, v2}, Lcom/fyber/g/a/e;->a(Lcom/fyber/g/d;)V

    goto :goto_0

    .line 1200
    :cond_2
    invoke-static {}, Lcom/fyber/a;->c()Lcom/fyber/a$b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/fyber/a$b;->f()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1201
    iget-object v1, p0, Lcom/fyber/g/e;->a:Lcom/fyber/g/a/e;

    sget-object v2, Lcom/fyber/g/d;->d:Lcom/fyber/g/d;

    invoke-virtual {v1, v2}, Lcom/fyber/g/a/e;->a(Lcom/fyber/g/d;)V

    goto :goto_0

    .line 1204
    :cond_3
    iget-object v1, p0, Lcom/fyber/g/e;->a:Lcom/fyber/g/a/e;

    invoke-virtual {v1}, Lcom/fyber/g/a/e;->a()Z

    move-result v1

    if-nez v1, :cond_4

    .line 1205
    iget-object v1, p0, Lcom/fyber/g/e;->a:Lcom/fyber/g/a/e;

    sget-object v2, Lcom/fyber/g/d;->e:Lcom/fyber/g/d;

    invoke-virtual {v1, v2}, Lcom/fyber/g/a/e;->a(Lcom/fyber/g/d;)V

    goto :goto_0

    .line 1208
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected abstract a(Landroid/content/Context;Lcom/fyber/g/a/j;)V
.end method

.method protected abstract b()V
.end method

.method protected abstract c()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method
