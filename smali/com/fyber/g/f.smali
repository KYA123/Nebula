.class public Lcom/fyber/g/f;
.super Lcom/fyber/g/e;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fyber/g/e",
        "<",
        "Lcom/fyber/g/f;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>(Lcom/fyber/g/a;)V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/fyber/g/e;-><init>(Lcom/fyber/g/a;)V

    .line 90
    return-void
.end method

.method public static a(Lcom/fyber/g/c;)Lcom/fyber/g/f;
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/fyber/g/f;

    invoke-direct {v0, p0}, Lcom/fyber/g/f;-><init>(Lcom/fyber/g/a;)V

    return-object v0
.end method


# virtual methods
.method protected final a()Lcom/fyber/g/a/e;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/fyber/g/a/e",
            "<",
            "Landroid/content/Intent;",
            "Lcom/fyber/ads/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    new-instance v0, Lcom/fyber/g/f$1;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Lcom/fyber/g/c;

    aput-object v3, v1, v2

    invoke-direct {v0, p0, v1}, Lcom/fyber/g/f$1;-><init>(Lcom/fyber/g/f;[Ljava/lang/Class;)V

    return-object v0
.end method

.method public a(Lcom/fyber/g/h;)Lcom/fyber/g/f;
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/fyber/g/f;->b:Lcom/fyber/g/a/j;

    const-string v1, "CURRENCY_REQUESTER"

    invoke-virtual {v0, v1, p1}, Lcom/fyber/g/a/j;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/fyber/g/a/j;

    .line 65
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/fyber/g/h;->b:Lcom/fyber/g/a/j;

    const-string v1, "CURRENCY_ID"

    invoke-virtual {v0, v1}, Lcom/fyber/g/a/j;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 66
    const-string v0, "RewardedVideoRequester"

    const-string v1, "A currency ID was detected in the parameters. It will not be used. The currency related to the ad displayed will be used instead."

    invoke-static {v0, v1}, Lcom/fyber/utils/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    :cond_0
    return-object p0
.end method

.method public a(Z)Lcom/fyber/g/f;
    .locals 3

    .prologue
    .line 50
    iget-object v0, p0, Lcom/fyber/g/f;->b:Lcom/fyber/g/a/j;

    const-string v1, "SHOULD_NOTIFY_ON_USER_ENGAGED"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/fyber/g/a/j;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/fyber/g/a/j;

    .line 51
    return-object p0
.end method

.method protected final a(Landroid/content/Context;Lcom/fyber/g/a/j;)V
    .locals 3

    .prologue
    .line 73
    sget-object v0, Lcom/fyber/ads/videos/a/c;->a:Lcom/fyber/ads/videos/a/c;

    invoke-virtual {v0}, Lcom/fyber/ads/videos/a/c;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    sget-object v0, Lcom/fyber/ads/videos/a/c;->a:Lcom/fyber/ads/videos/a/c;

    iget-object v1, p0, Lcom/fyber/g/f;->a:Lcom/fyber/g/a/e;

    invoke-virtual {v0, v1}, Lcom/fyber/ads/videos/a/c;->a(Lcom/fyber/g/a/e;)V

    .line 77
    :try_start_0
    sget-object v0, Lcom/fyber/ads/videos/a/c;->a:Lcom/fyber/ads/videos/a/c;

    invoke-virtual {v0, p2, p1}, Lcom/fyber/ads/videos/a/c;->a(Lcom/fyber/g/a/j;Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    :goto_0
    return-void

    .line 78
    :catch_0
    move-exception v0

    .line 80
    const-string v1, "RewardedVideoRequester"

    const-string v2, "something went wrong with the video request"

    invoke-static {v1, v2, v0}, Lcom/fyber/utils/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 81
    iget-object v0, p0, Lcom/fyber/g/f;->a:Lcom/fyber/g/a/e;

    sget-object v1, Lcom/fyber/g/d;->c:Lcom/fyber/g/d;

    invoke-virtual {v0, v1}, Lcom/fyber/g/a/e;->a(Lcom/fyber/g/d;)V

    goto :goto_0

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/fyber/g/f;->a:Lcom/fyber/g/a/e;

    sget-object v1, Lcom/fyber/g/d;->i:Lcom/fyber/g/d;

    invoke-virtual {v0, v1}, Lcom/fyber/g/a/e;->a(Lcom/fyber/g/d;)V

    goto :goto_0
.end method

.method protected final b()V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/fyber/g/f;->b:Lcom/fyber/g/a/j;

    const-string v1, "videos"

    invoke-virtual {v0, v1}, Lcom/fyber/g/a/j;->a(Ljava/lang/String;)Lcom/fyber/g/a/j;

    move-result-object v0

    const/4 v1, 0x1

    .line 120
    invoke-virtual {v0, v1}, Lcom/fyber/g/a/j;->a(Z)Lcom/fyber/g/a/j;

    move-result-object v0

    const/4 v1, 0x4

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    .line 121
    invoke-virtual {v0, v1}, Lcom/fyber/g/a/j;->a([I)Lcom/fyber/g/a/j;

    .line 122
    return-void

    .line 120
    nop

    :array_0
    .array-data 4
        0x9
        0x8
        0x2
        0x0
    .end array-data
.end method

.method protected final bridge synthetic c()Ljava/lang/Object;
    .locals 0

    .prologue
    .line 23
    return-object p0
.end method
