.class public final Lcom/fyber/mediation/MediationAdapterStarter;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final ADAPTER_VERSION:Ljava/lang/String; = "ADAPTER_VERSION"

.field public static final FYBER_STARTED:Ljava/lang/String; = "FYBER_STARTED"

.field public static adaptersListener:Lcom/fyber/mediation/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/util/Map;Ljava/lang/String;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 76
    if-nez v0, :cond_0

    .line 77
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 78
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    :cond_0
    return-object v0
.end method

.method private static a(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 99
    if-eqz p0, :cond_2

    invoke-interface {p0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 100
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 101
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 102
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 103
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    .line 104
    if-nez v2, :cond_0

    .line 105
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 107
    :cond_0
    if-eqz v0, :cond_1

    .line 108
    invoke-interface {v2, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 110
    :cond_1
    invoke-interface {p1, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 113
    :cond_2
    const-string v0, "MediationAdapterStarter"

    const-string v1, "There were no configurations to override"

    invoke-static {v0, v1}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    :cond_3
    return-object p1
.end method

.method private static a(Ljava/util/concurrent/Future;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;>;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 84
    invoke-static {}, Lcom/fyber/mediation/MediationConfigProvider;->getConfigs()Ljava/util/Map;

    move-result-object v0

    .line 85
    invoke-static {}, Lcom/fyber/mediation/MediationConfigProvider;->getRuntimeConfigs()Ljava/util/Map;

    move-result-object v1

    .line 86
    invoke-static {v1, v0}, Lcom/fyber/mediation/MediationAdapterStarter;->a(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    .line 88
    if-eqz p0, :cond_0

    .line 89
    :try_start_0
    invoke-interface {p0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 90
    invoke-static {v1, v0}, Lcom/fyber/mediation/MediationAdapterStarter;->a(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    :goto_0
    move-object v1, v0

    .line 95
    :goto_1
    return-object v1

    .line 92
    :catch_0
    move-exception v0

    .line 93
    :goto_2
    const-string v2, "MediationAdapterStarter"

    const-string v3, "Exception occurred"

    invoke-static {v2, v3, v0}, Lcom/fyber/utils/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 92
    :catch_1
    move-exception v0

    goto :goto_2

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method private static a(Landroid/app/Activity;Ljava/util/Map;Ljava/util/Map;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/fyber/mediation/c;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 26
    const/4 v1, 0x0

    .line 28
    :try_start_0
    new-instance v2, Lcom/fyber/mediation/c/a;

    invoke-direct {v2}, Lcom/fyber/mediation/c/a;-><init>()V

    .line 29
    const-string v0, "ADAPTER_VERSION"

    const-string v3, "4.0.2-r2"

    invoke-interface {p1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    const-string v0, "MediationAdapterStarter"

    const-string v3, "Starting adapter Vungle with version 4.0.2-r2"

    invoke-static {v0, v3}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    invoke-virtual {v2, p0, p1}, Lcom/fyber/mediation/c;->a(Landroid/app/Activity;Ljava/util/Map;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 32
    if-eqz v0, :cond_0

    .line 33
    :try_start_1
    const-string v1, "MediationAdapterStarter"

    const-string v3, "Adapter Vungle with version 4.0.2-r2 was started successfully"

    invoke-static {v1, v3}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    const-string v1, "vungle"

    invoke-interface {p2, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    :goto_0
    return v0

    .line 36
    :cond_0
    const-string v1, "MediationAdapterStarter"

    const-string v2, "Adapter Vungle with version 4.0.2-r2 was not started successfully"

    invoke-static {v1, v2}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 38
    :catch_0
    move-exception v1

    .line 39
    :goto_1
    const-string v2, "MediationAdapterStarter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception occurred while loading adapter Vungle with version 4.0.2-r2 - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/fyber/utils/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 38
    :catch_1
    move-exception v0

    move-object v5, v0

    move v0, v1

    move-object v1, v5

    goto :goto_1
.end method

.method private static b(Landroid/app/Activity;Ljava/util/Map;Ljava/util/Map;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/fyber/mediation/c;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 45
    const/4 v1, 0x0

    .line 47
    :try_start_0
    new-instance v2, Lcom/fyber/mediation/b/a;

    invoke-direct {v2}, Lcom/fyber/mediation/b/a;-><init>()V

    .line 48
    const-string v0, "ADAPTER_VERSION"

    const-string v3, "2.1.0-r1"

    invoke-interface {p1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    const-string v0, "MediationAdapterStarter"

    const-string v3, "Starting adapter Applifier with version 2.1.0-r1"

    invoke-static {v0, v3}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    invoke-virtual {v2, p0, p1}, Lcom/fyber/mediation/c;->a(Landroid/app/Activity;Ljava/util/Map;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 51
    if-eqz v0, :cond_0

    .line 52
    :try_start_1
    const-string v1, "MediationAdapterStarter"

    const-string v3, "Adapter Applifier with version 2.1.0-r1 was started successfully"

    invoke-static {v1, v3}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    const-string v1, "applifier"

    invoke-interface {p2, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    :goto_0
    return v0

    .line 55
    :cond_0
    const-string v1, "MediationAdapterStarter"

    const-string v2, "Adapter Applifier with version 2.1.0-r1 was not started successfully"

    invoke-static {v1, v2}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 57
    :catch_0
    move-exception v1

    .line 58
    :goto_1
    const-string v2, "MediationAdapterStarter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception occurred while loading adapter Applifier with version 2.1.0-r1 - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/fyber/utils/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 57
    :catch_1
    move-exception v0

    move-object v5, v0

    move v0, v1

    move-object v1, v5

    goto :goto_1
.end method

.method public static getAdaptersCount()I
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x2

    return v0
.end method

.method public static startAdapters(Landroid/app/Activity;Ljava/util/Map;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/fyber/mediation/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 65
    const-string v1, "Vungle"

    invoke-static {p1, v1}, Lcom/fyber/mediation/MediationAdapterStarter;->a(Ljava/util/Map;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    const-string v2, "FYBER_STARTED"

    const-string v3, "Vungle"

    invoke-static {p1, v3}, Lcom/fyber/mediation/MediationAdapterStarter;->a(Ljava/util/Map;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    invoke-static {p0, v3, v0}, Lcom/fyber/mediation/MediationAdapterStarter;->a(Landroid/app/Activity;Ljava/util/Map;Ljava/util/Map;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    const-string v1, "Applifier"

    invoke-static {p1, v1}, Lcom/fyber/mediation/MediationAdapterStarter;->a(Ljava/util/Map;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    const-string v2, "FYBER_STARTED"

    const-string v3, "Applifier"

    invoke-static {p1, v3}, Lcom/fyber/mediation/MediationAdapterStarter;->a(Ljava/util/Map;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    invoke-static {p0, v3, v0}, Lcom/fyber/mediation/MediationAdapterStarter;->b(Landroid/app/Activity;Ljava/util/Map;Ljava/util/Map;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    return-object v0
.end method

.method public static startAdapters(Landroid/app/Activity;Ljava/util/concurrent/Future;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/concurrent/Future",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;>;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/fyber/mediation/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 119
    invoke-static {p1}, Lcom/fyber/mediation/MediationAdapterStarter;->a(Ljava/util/concurrent/Future;)Ljava/util/Map;

    move-result-object v0

    .line 120
    invoke-static {p0, v0}, Lcom/fyber/mediation/MediationAdapterStarter;->startAdapters(Landroid/app/Activity;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    .line 121
    sget-object v2, Lcom/fyber/mediation/MediationAdapterStarter;->adaptersListener:Lcom/fyber/mediation/a;

    if-eqz v2, :cond_0

    .line 122
    sget-object v2, Lcom/fyber/mediation/MediationAdapterStarter;->adaptersListener:Lcom/fyber/mediation/a;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Lcom/fyber/mediation/a;->a(Ljava/util/Set;Ljava/util/Map;)V

    .line 124
    :cond_0
    invoke-static {v1, v0}, Lcom/fyber/utils/testsuite/b;->a(Ljava/util/Map;Ljava/util/Map;)V

    .line 125
    return-object v1
.end method
