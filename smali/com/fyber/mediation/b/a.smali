.class public Lcom/fyber/mediation/b/a;
.super Lcom/fyber/mediation/c;
.source "SourceFile"

# interfaces
.implements Lcom/unity3d/ads/IUnityAdsListener;


# annotations
.annotation runtime Lcom/fyber/mediation/annotations/AdapterDefinition;
    apiVersion = 0x5
    name = "Applifier"
    sdkFeatures = {
        "banners",
        "blended"
    }
    version = "2.1.0-r1"
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/fyber/mediation/b/b/a;

.field private c:Lcom/fyber/mediation/b/a/a;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/unity3d/ads/IUnityAdsListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/fyber/mediation/b/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/fyber/mediation/b/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/fyber/mediation/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    const-string v0, "Applifier"

    return-object v0
.end method

.method public a(Landroid/app/Activity;Ljava/util/Map;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 48
    sget-object v0, Lcom/fyber/mediation/b/a;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting UnityAds (former Applifier) adapter - SDK version "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 49
    invoke-static {}, Lcom/unity3d/ads/UnityAds;->getVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 48
    invoke-static {v0, v1}, Lcom/fyber/utils/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 53
    sget-object v0, Lcom/fyber/mediation/b/a;->a:Ljava/lang/String;

    const-string v1, "UnityAds requires Android 4.1.x (API 16) or higher.\nThe mediation adapter will not be started"

    invoke-static {v0, v1}, Lcom/fyber/utils/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v3

    .line 94
    :goto_0
    return v0

    .line 58
    :cond_0
    const-string v0, "game.id.key"

    const-class v1, Ljava/lang/String;

    invoke-static {p2, v0, v1}, Lcom/fyber/mediation/b/a;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 59
    const-string v1, "zone.id.rewarded.video"

    const-class v2, Ljava/lang/String;

    invoke-static {p2, v1, v2}, Lcom/fyber/mediation/b/a;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 60
    const-string v2, "debug.mode"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const-class v5, Ljava/lang/Boolean;

    invoke-static {p2, v2, v4, v5}, Lcom/fyber/mediation/b/a;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    .line 62
    invoke-static {v0}, Lcom/fyber/utils/c;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 63
    sget-object v0, Lcom/fyber/mediation/b/a;->a:Ljava/lang/String;

    const-string v1, "Game key is missing. Adapter won\'t start."

    invoke-static {v0, v1}, Lcom/fyber/utils/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v3

    .line 64
    goto :goto_0

    .line 67
    :cond_1
    const-string v4, "[1-9][0-9]*"

    invoke-virtual {v0, v4}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 68
    sget-object v0, Lcom/fyber/mediation/b/a;->a:Ljava/lang/String;

    const-string v1, "Game key value is not valid. Adapter won\'t start."

    invoke-static {v0, v1}, Lcom/fyber/utils/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v3

    .line 69
    goto :goto_0

    .line 72
    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    const/4 v4, 0x2

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v3, p0, Lcom/fyber/mediation/b/a;->d:Ljava/util/List;

    .line 73
    new-instance v3, Lcom/fyber/mediation/b/a/a;

    invoke-direct {v3, p0, p2}, Lcom/fyber/mediation/b/a/a;-><init>(Lcom/fyber/mediation/b/a;Ljava/util/Map;)V

    iput-object v3, p0, Lcom/fyber/mediation/b/a;->c:Lcom/fyber/mediation/b/a/a;

    .line 74
    iget-object v3, p0, Lcom/fyber/mediation/b/a;->d:Ljava/util/List;

    iget-object v4, p0, Lcom/fyber/mediation/b/a;->c:Lcom/fyber/mediation/b/a/a;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    invoke-static {v1}, Lcom/fyber/utils/c;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 77
    new-instance v3, Lcom/fyber/mediation/b/b/a;

    invoke-direct {v3, p0}, Lcom/fyber/mediation/b/b/a;-><init>(Lcom/fyber/mediation/b/a;)V

    iput-object v3, p0, Lcom/fyber/mediation/b/a;->b:Lcom/fyber/mediation/b/b/a;

    .line 78
    iget-object v3, p0, Lcom/fyber/mediation/b/a;->b:Lcom/fyber/mediation/b/b/a;

    invoke-virtual {v3, v1}, Lcom/fyber/mediation/b/b/a;->a(Ljava/lang/String;)V

    .line 79
    iget-object v1, p0, Lcom/fyber/mediation/b/a;->d:Ljava/util/List;

    iget-object v3, p0, Lcom/fyber/mediation/b/a;->b:Lcom/fyber/mediation/b/b/a;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    :goto_1
    new-instance v1, Lcom/unity3d/ads/metadata/MediationMetaData;

    invoke-direct {v1, p1}, Lcom/unity3d/ads/metadata/MediationMetaData;-><init>(Landroid/content/Context;)V

    .line 87
    const-string v3, "Fyber"

    invoke-virtual {v1, v3}, Lcom/unity3d/ads/metadata/MediationMetaData;->setName(Ljava/lang/String;)V

    .line 88
    sget-object v3, Lcom/fyber/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/unity3d/ads/metadata/MediationMetaData;->setVersion(Ljava/lang/String;)V

    .line 89
    invoke-virtual {v1}, Lcom/unity3d/ads/metadata/MediationMetaData;->commit()V

    .line 91
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v1}, Lcom/unity3d/ads/UnityAds;->setDebugMode(Z)V

    .line 92
    invoke-static {p1, v0, p0}, Lcom/unity3d/ads/UnityAds;->initialize(Landroid/app/Activity;Ljava/lang/String;Lcom/unity3d/ads/IUnityAdsListener;)V

    .line 94
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 81
    :cond_3
    sget-object v1, Lcom/fyber/mediation/b/a;->a:Ljava/lang/String;

    const-string v3, "Zone id for rewarded video is empty. Rewarded video format will be unavailable for UnityAds"

    invoke-static {v1, v3}, Lcom/fyber/utils/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    const-string v0, "2.1.0-r1"

    return-object v0
.end method

.method public synthetic c()Lcom/fyber/ads/videos/b/a;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/fyber/mediation/b/a;->g()Lcom/fyber/mediation/b/b/a;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d()Lcom/fyber/ads/interstitials/c/a;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/fyber/mediation/b/a;->h()Lcom/fyber/mediation/b/a/a;

    move-result-object v0

    return-object v0
.end method

.method public e()Lcom/fyber/ads/a/b/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/fyber/ads/a/b/a",
            "<",
            "Lcom/fyber/mediation/b/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 119
    const/4 v0, 0x0

    return-object v0
.end method

.method public g()Lcom/fyber/mediation/b/b/a;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/fyber/mediation/b/a;->b:Lcom/fyber/mediation/b/b/a;

    return-object v0
.end method

.method public h()Lcom/fyber/mediation/b/a/a;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/fyber/mediation/b/a;->c:Lcom/fyber/mediation/b/a/a;

    return-object v0
.end method

.method public onUnityAdsError(Lcom/unity3d/ads/UnityAds$UnityAdsError;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lcom/fyber/mediation/b/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/unity3d/ads/IUnityAdsListener;

    .line 151
    invoke-interface {v0, p1, p2}, Lcom/unity3d/ads/IUnityAdsListener;->onUnityAdsError(Lcom/unity3d/ads/UnityAds$UnityAdsError;Ljava/lang/String;)V

    goto :goto_0

    .line 153
    :cond_0
    return-void
.end method

.method public onUnityAdsFinish(Ljava/lang/String;Lcom/unity3d/ads/UnityAds$FinishState;)V
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/fyber/mediation/b/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/unity3d/ads/IUnityAdsListener;

    .line 144
    invoke-interface {v0, p1, p2}, Lcom/unity3d/ads/IUnityAdsListener;->onUnityAdsFinish(Ljava/lang/String;Lcom/unity3d/ads/UnityAds$FinishState;)V

    goto :goto_0

    .line 146
    :cond_0
    return-void
.end method

.method public onUnityAdsReady(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/fyber/mediation/b/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/unity3d/ads/IUnityAdsListener;

    .line 130
    invoke-interface {v0, p1}, Lcom/unity3d/ads/IUnityAdsListener;->onUnityAdsReady(Ljava/lang/String;)V

    goto :goto_0

    .line 132
    :cond_0
    return-void
.end method

.method public onUnityAdsStart(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/fyber/mediation/b/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/unity3d/ads/IUnityAdsListener;

    .line 137
    invoke-interface {v0, p1}, Lcom/unity3d/ads/IUnityAdsListener;->onUnityAdsStart(Ljava/lang/String;)V

    goto :goto_0

    .line 139
    :cond_0
    return-void
.end method
