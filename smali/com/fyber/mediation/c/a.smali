.class public Lcom/fyber/mediation/c/a;
.super Lcom/fyber/mediation/c;
.source "SourceFile"


# annotations
.annotation runtime Lcom/fyber/mediation/annotations/AdapterDefinition;
    apiVersion = 0x5
    name = "Vungle"
    sdkFeatures = {
        "banners",
        "blended"
    }
    version = "4.0.2-r2"
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/fyber/mediation/c/b/a;

.field private c:Lcom/fyber/mediation/c/a/a;

.field private d:Lcom/fyber/mediation/c/a;

.field private e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Landroid/os/Handler;

.field private g:Landroid/app/Application$ActivityLifecycleCallbacks;

.field private h:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/fyber/mediation/c/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/fyber/mediation/c/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/fyber/mediation/c;-><init>()V

    .line 51
    iput-object p0, p0, Lcom/fyber/mediation/c/a;->d:Lcom/fyber/mediation/c/a;

    .line 53
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/fyber/mediation/c/a;->f:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/fyber/mediation/c/a;Lcom/fyber/mediation/c/a/a;)Lcom/fyber/mediation/c/a/a;
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/fyber/mediation/c/a;->c:Lcom/fyber/mediation/c/a/a;

    return-object p1
.end method

.method static synthetic a(Lcom/fyber/mediation/c/a;)Lcom/fyber/mediation/c/a;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/fyber/mediation/c/a;->d:Lcom/fyber/mediation/c/a;

    return-object v0
.end method

.method static synthetic a(Lcom/fyber/mediation/c/a;Lcom/fyber/mediation/c/b/a;)Lcom/fyber/mediation/c/b/a;
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/fyber/mediation/c/a;->b:Lcom/fyber/mediation/c/b/a;

    return-object p1
.end method

.method private a(Landroid/app/Application;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 97
    new-instance v0, Lcom/fyber/mediation/c/a$2;

    invoke-direct {v0, p0}, Lcom/fyber/mediation/c/a$2;-><init>(Lcom/fyber/mediation/c/a;)V

    iput-object v0, p0, Lcom/fyber/mediation/c/a;->g:Landroid/app/Application$ActivityLifecycleCallbacks;

    .line 133
    iget-object v0, p0, Lcom/fyber/mediation/c/a;->g:Landroid/app/Application$ActivityLifecycleCallbacks;

    invoke-virtual {p1, v0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 134
    return-void
.end method

.method static synthetic a(Lcom/fyber/mediation/c/a;Landroid/app/Application;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/fyber/mediation/c/a;->a(Landroid/app/Application;)V

    return-void
.end method

.method static synthetic a(Lcom/fyber/mediation/c/a;Lcom/vungle/publisher/AdConfig;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/fyber/mediation/c/a;->a(Lcom/vungle/publisher/AdConfig;)V

    return-void
.end method

.method private a(Lcom/vungle/publisher/AdConfig;)V
    .locals 0

    .prologue
    .line 171
    invoke-direct {p0, p1}, Lcom/fyber/mediation/c/a;->b(Lcom/vungle/publisher/AdConfig;)V

    .line 172
    invoke-direct {p0, p1}, Lcom/fyber/mediation/c/a;->c(Lcom/vungle/publisher/AdConfig;)V

    .line 173
    invoke-direct {p0, p1}, Lcom/fyber/mediation/c/a;->d(Lcom/vungle/publisher/AdConfig;)V

    .line 174
    invoke-direct {p0, p1}, Lcom/fyber/mediation/c/a;->e(Lcom/vungle/publisher/AdConfig;)V

    .line 175
    invoke-direct {p0, p1}, Lcom/fyber/mediation/c/a;->f(Lcom/vungle/publisher/AdConfig;)V

    .line 176
    invoke-direct {p0, p1}, Lcom/fyber/mediation/c/a;->g(Lcom/vungle/publisher/AdConfig;)V

    .line 177
    invoke-direct {p0, p1}, Lcom/fyber/mediation/c/a;->h(Lcom/vungle/publisher/AdConfig;)V

    .line 178
    invoke-direct {p0, p1}, Lcom/fyber/mediation/c/a;->i(Lcom/vungle/publisher/AdConfig;)V

    .line 179
    invoke-direct {p0, p1}, Lcom/fyber/mediation/c/a;->j(Lcom/vungle/publisher/AdConfig;)V

    .line 180
    return-void
.end method

.method static synthetic b(Lcom/fyber/mediation/c/a;)Lcom/fyber/mediation/c/b/a;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/fyber/mediation/c/a;->b:Lcom/fyber/mediation/c/b/a;

    return-object v0
.end method

.method private b(Lcom/vungle/publisher/AdConfig;)V
    .locals 3

    .prologue
    .line 183
    iget-object v0, p0, Lcom/fyber/mediation/c/a;->e:Ljava/util/Map;

    const-string v1, "auto.rotation.enabled"

    const-class v2, Ljava/lang/Boolean;

    invoke-static {v0, v1, v2}, Lcom/fyber/mediation/c/a;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 184
    if-eqz v0, :cond_0

    .line 185
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/vungle/publisher/Orientation;->autoRotate:Lcom/vungle/publisher/Orientation;

    :goto_0
    invoke-virtual {p1, v0}, Lcom/vungle/publisher/AdConfig;->setOrientation(Lcom/vungle/publisher/Orientation;)V

    .line 187
    :cond_0
    return-void

    .line 185
    :cond_1
    sget-object v0, Lcom/vungle/publisher/Orientation;->matchVideo:Lcom/vungle/publisher/Orientation;

    goto :goto_0
.end method

.method static synthetic c(Lcom/fyber/mediation/c/a;)Lcom/fyber/mediation/c/a/a;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/fyber/mediation/c/a;->c:Lcom/fyber/mediation/c/a/a;

    return-object v0
.end method

.method private c(Lcom/vungle/publisher/AdConfig;)V
    .locals 3

    .prologue
    .line 190
    iget-object v0, p0, Lcom/fyber/mediation/c/a;->e:Ljava/util/Map;

    const-string v1, "sound.enabled"

    const-class v2, Ljava/lang/Boolean;

    invoke-static {v0, v1, v2}, Lcom/fyber/mediation/c/a;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 191
    if-eqz v0, :cond_0

    .line 192
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/vungle/publisher/AdConfig;->setSoundEnabled(Z)V

    .line 194
    :cond_0
    return-void
.end method

.method private d(Lcom/vungle/publisher/AdConfig;)V
    .locals 3

    .prologue
    .line 197
    iget-object v0, p0, Lcom/fyber/mediation/c/a;->e:Ljava/util/Map;

    const-string v1, "back.button.enabled"

    const-class v2, Ljava/lang/Boolean;

    invoke-static {v0, v1, v2}, Lcom/fyber/mediation/c/a;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 198
    if-eqz v0, :cond_0

    .line 199
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/vungle/publisher/AdConfig;->setBackButtonImmediatelyEnabled(Z)V

    .line 201
    :cond_0
    return-void
.end method

.method private e(Lcom/vungle/publisher/AdConfig;)V
    .locals 3

    .prologue
    .line 204
    iget-object v0, p0, Lcom/fyber/mediation/c/a;->e:Ljava/util/Map;

    const-string v1, "incentivized.mode"

    const-class v2, Ljava/lang/Boolean;

    invoke-static {v0, v1, v2}, Lcom/fyber/mediation/c/a;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 205
    if-eqz v0, :cond_0

    .line 206
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/vungle/publisher/AdConfig;->setIncentivized(Z)V

    .line 210
    :goto_0
    return-void

    .line 208
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/vungle/publisher/AdConfig;->setIncentivized(Z)V

    goto :goto_0
.end method

.method private f(Lcom/vungle/publisher/AdConfig;)V
    .locals 2

    .prologue
    .line 213
    invoke-virtual {p0}, Lcom/fyber/mediation/c/a;->f()Ljava/lang/String;

    move-result-object v0

    .line 214
    invoke-virtual {p0}, Lcom/fyber/mediation/c/a;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fyber/utils/c;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 215
    invoke-virtual {p1, v0}, Lcom/vungle/publisher/AdConfig;->setIncentivizedUserId(Ljava/lang/String;)V

    .line 217
    :cond_0
    return-void
.end method

.method private g(Lcom/vungle/publisher/AdConfig;)V
    .locals 3

    .prologue
    .line 220
    iget-object v0, p0, Lcom/fyber/mediation/c/a;->e:Ljava/util/Map;

    const-string v1, "cancel.dialog.title"

    const-class v2, Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/fyber/mediation/c/a;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 221
    invoke-static {v0}, Lcom/fyber/utils/c;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 222
    invoke-virtual {p1, v0}, Lcom/vungle/publisher/AdConfig;->setIncentivizedCancelDialogTitle(Ljava/lang/String;)V

    .line 224
    :cond_0
    return-void
.end method

.method private h(Lcom/vungle/publisher/AdConfig;)V
    .locals 3

    .prologue
    .line 227
    iget-object v0, p0, Lcom/fyber/mediation/c/a;->e:Ljava/util/Map;

    const-string v1, "cancel.dialog.text"

    const-class v2, Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/fyber/mediation/c/a;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 228
    invoke-static {v0}, Lcom/fyber/utils/c;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 229
    invoke-virtual {p1, v0}, Lcom/vungle/publisher/AdConfig;->setIncentivizedCancelDialogBodyText(Ljava/lang/String;)V

    .line 231
    :cond_0
    return-void
.end method

.method private i(Lcom/vungle/publisher/AdConfig;)V
    .locals 3

    .prologue
    .line 234
    iget-object v0, p0, Lcom/fyber/mediation/c/a;->e:Ljava/util/Map;

    const-string v1, "cancel.dialog.button"

    const-class v2, Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/fyber/mediation/c/a;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 235
    invoke-static {v0}, Lcom/fyber/utils/c;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 236
    invoke-virtual {p1, v0}, Lcom/vungle/publisher/AdConfig;->setIncentivizedCancelDialogCloseButtonText(Ljava/lang/String;)V

    .line 238
    :cond_0
    return-void
.end method

.method private j(Lcom/vungle/publisher/AdConfig;)V
    .locals 3

    .prologue
    .line 241
    iget-object v0, p0, Lcom/fyber/mediation/c/a;->e:Ljava/util/Map;

    const-string v1, "keep.watching.text"

    const-class v2, Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/fyber/mediation/c/a;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 242
    invoke-static {v0}, Lcom/fyber/utils/c;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 243
    invoke-virtual {p1, v0}, Lcom/vungle/publisher/AdConfig;->setIncentivizedCancelDialogKeepWatchingButtonText(Ljava/lang/String;)V

    .line 245
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    const-string v0, "Vungle"

    return-object v0
.end method

.method public a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 250
    iput-object p1, p0, Lcom/fyber/mediation/c/a;->h:Ljava/lang/Object;

    .line 251
    return-void
.end method

.method public a(Landroid/app/Activity;Ljava/util/Map;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 59
    sget-object v0, Lcom/fyber/mediation/c/a;->a:Ljava/lang/String;

    const-string v2, "Starting Vungle adapter"

    invoke-static {v0, v2}, Lcom/fyber/utils/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    iput-object p2, p0, Lcom/fyber/mediation/c/a;->e:Ljava/util/Map;

    .line 62
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-ge v0, v2, :cond_0

    .line 63
    sget-object v0, Lcom/fyber/mediation/c/a;->a:Ljava/lang/String;

    const-string v2, "Vungle Adapter requires Android API level 14+. Adapter won\'t start."

    invoke-static {v0, v2}, Lcom/fyber/utils/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 92
    :goto_0
    return v0

    .line 67
    :cond_0
    const-string v0, "app.id"

    const-class v2, Ljava/lang/String;

    invoke-static {p2, v0, v2}, Lcom/fyber/mediation/c/a;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 68
    invoke-static {v0}, Lcom/fyber/utils/c;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 69
    sget-object v0, Lcom/fyber/mediation/c/a;->a:Ljava/lang/String;

    const-string v2, "\u2018app.id\u2018 is missing. Adapter won\u2019t start"

    invoke-static {v0, v2}, Lcom/fyber/utils/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 70
    goto :goto_0

    .line 72
    :cond_1
    sget-object v1, Lcom/fyber/mediation/c/a;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Using App ID = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/fyber/utils/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    iget-object v1, p0, Lcom/fyber/mediation/c/a;->f:Landroid/os/Handler;

    new-instance v2, Lcom/fyber/mediation/c/a$1;

    invoke-direct {v2, p0, p1, v0}, Lcom/fyber/mediation/c/a$1;-><init>(Lcom/fyber/mediation/c/a;Landroid/app/Activity;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 92
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    const-string v0, "4.0.2-r2"

    return-object v0
.end method

.method public b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/fyber/mediation/c/a;->h:Ljava/lang/Object;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic c()Lcom/fyber/ads/videos/b/a;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/fyber/mediation/c/a;->g()Lcom/fyber/mediation/c/b/a;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/fyber/ads/interstitials/c/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/fyber/ads/interstitials/c/a",
            "<",
            "Lcom/fyber/mediation/c/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 153
    iget-object v0, p0, Lcom/fyber/mediation/c/a;->c:Lcom/fyber/mediation/c/a/a;

    return-object v0
.end method

.method public e()Lcom/fyber/ads/a/b/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/fyber/ads/a/b/a",
            "<",
            "Lcom/fyber/mediation/c/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 158
    const/4 v0, 0x0

    return-object v0
.end method

.method public g()Lcom/fyber/mediation/c/b/a;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/fyber/mediation/c/a;->b:Lcom/fyber/mediation/c/b/a;

    return-object v0
.end method
