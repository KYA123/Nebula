.class public final Lcom/fyber/mediation/g;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/fyber/mediation/g;


# instance fields
.field private b:Z

.field private c:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/fyber/mediation/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lcom/fyber/mediation/g;

    invoke-direct {v0}, Lcom/fyber/mediation/g;-><init>()V

    sput-object v0, Lcom/fyber/mediation/g;->a:Lcom/fyber/mediation/g;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/fyber/mediation/g;->b:Z

    .line 48
    new-instance v0, Lcom/fyber/mediation/g$1;

    invoke-direct {v0, p0}, Lcom/fyber/mediation/g$1;-><init>(Lcom/fyber/mediation/g;)V

    iput-object v0, p0, Lcom/fyber/mediation/g;->d:Ljava/util/Map;

    .line 61
    return-void
.end method

.method static synthetic a(Lcom/fyber/mediation/g;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/fyber/mediation/g;->d:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/fyber/mediation/g;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/fyber/mediation/g;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fyber/mediation/c;

    invoke-virtual {v0}, Lcom/fyber/mediation/c;->b()Ljava/lang/String;

    move-result-object v0

    .line 185
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final a()Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/Future",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 119
    iget-object v0, p0, Lcom/fyber/mediation/g;->c:Ljava/util/concurrent/Future;

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/fyber/ads/videos/a/a;)Ljava/util/concurrent/Future;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/fyber/ads/videos/a/a;",
            ")",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/fyber/ads/videos/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145
    invoke-virtual {p2}, Lcom/fyber/ads/videos/a/a;->c()Ljava/lang/String;

    move-result-object v0

    .line 146
    iget-object v1, p0, Lcom/fyber/mediation/g;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fyber/mediation/c;

    invoke-virtual {v0, p1, p2}, Lcom/fyber/mediation/c;->a(Landroid/content/Context;Lcom/fyber/ads/videos/a/a;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/fyber/mediation/g;->b:Z

    if-eqz v0, :cond_0

    .line 116
    :goto_0
    return-void

    .line 68
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fyber/mediation/g;->b:Z

    .line 72
    iget-object v0, p0, Lcom/fyber/mediation/g;->d:Ljava/util/Map;

    const-string v1, "Fyber"

    new-instance v2, Lcom/fyber/mediation/a/a;

    invoke-direct {v2}, Lcom/fyber/mediation/a/a;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    invoke-static {}, Lcom/fyber/a;->c()Lcom/fyber/a$b;

    move-result-object v0

    new-instance v1, Lcom/fyber/mediation/g$2;

    invoke-direct {v1, p0, p1}, Lcom/fyber/mediation/g$2;-><init>(Lcom/fyber/mediation/g;Landroid/app/Activity;)V

    invoke-virtual {v0, v1}, Lcom/fyber/a$b;->a(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/fyber/mediation/g;->c:Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public final a(Landroid/app/Activity;Ljava/lang/String;Ljava/util/Map;Lcom/fyber/ads/videos/b/d;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/fyber/ads/videos/b/d;",
            ")V"
        }
    .end annotation

    .prologue
    .line 150
    sget-object v0, Lcom/fyber/ads/b;->b:Lcom/fyber/ads/b;

    invoke-virtual {p0, p2, v0}, Lcom/fyber/mediation/g;->a(Ljava/lang/String;Lcom/fyber/ads/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/fyber/mediation/g;->d:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fyber/mediation/c;

    invoke-virtual {v0, p1, p4, p3}, Lcom/fyber/mediation/c;->a(Landroid/app/Activity;Lcom/fyber/ads/videos/b/d;Ljava/util/Map;)V

    .line 155
    :goto_0
    return-void

    .line 153
    :cond_0
    invoke-virtual {p0, p2}, Lcom/fyber/mediation/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/fyber/ads/videos/b/b;->h:Lcom/fyber/ads/videos/b/b;

    invoke-interface {p4, p2, v0, v1, p3}, Lcom/fyber/ads/videos/b/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/fyber/ads/videos/b/b;Ljava/util/Map;)V

    goto :goto_0
.end method

.method public final a(Landroid/app/Activity;Lcom/fyber/ads/interstitials/b/a;)Z
    .locals 2

    .prologue
    .line 164
    invoke-virtual {p2}, Lcom/fyber/ads/interstitials/b/a;->c()Ljava/lang/String;

    move-result-object v0

    .line 165
    sget-object v1, Lcom/fyber/ads/b;->c:Lcom/fyber/ads/b;

    invoke-virtual {p0, v0, v1}, Lcom/fyber/mediation/g;->a(Ljava/lang/String;Lcom/fyber/ads/b;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 166
    iget-object v1, p0, Lcom/fyber/mediation/g;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fyber/mediation/c;

    invoke-virtual {v0, p1}, Lcom/fyber/mediation/c;->a(Landroid/app/Activity;)V

    .line 167
    const/4 v0, 0x1

    .line 169
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/fyber/ads/b;)Z
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/fyber/mediation/g;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fyber/mediation/c;

    .line 137
    if-eqz v0, :cond_0

    .line 138
    invoke-virtual {v0, p2}, Lcom/fyber/mediation/c;->a(Lcom/fyber/ads/b;)Z

    move-result v0

    .line 140
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
