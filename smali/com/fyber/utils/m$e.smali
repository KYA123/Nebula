.class final Lcom/fyber/utils/m$e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/fyber/utils/s;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/fyber/utils/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "e"
.end annotation


# instance fields
.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 311
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 321
    invoke-static {}, Lcom/fyber/utils/m;->i()Lcom/fyber/utils/m;

    move-result-object v0

    if-nez v0, :cond_0

    .line 322
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    .line 330
    :goto_0
    return-object v0

    .line 323
    :cond_0
    iget-object v0, p0, Lcom/fyber/utils/m$e;->a:Ljava/util/Map;

    if-nez v0, :cond_1

    .line 324
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/fyber/utils/m$e;->a:Ljava/util/Map;

    .line 325
    iget-object v0, p0, Lcom/fyber/utils/m$e;->a:Ljava/util/Map;

    const-string v1, "screen_width"

    invoke-static {}, Lcom/fyber/utils/m;->i()Lcom/fyber/utils/m;

    move-result-object v2

    invoke-static {v2}, Lcom/fyber/utils/m;->a(Lcom/fyber/utils/m;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    iget-object v0, p0, Lcom/fyber/utils/m$e;->a:Ljava/util/Map;

    const-string v1, "screen_height"

    invoke-static {}, Lcom/fyber/utils/m;->i()Lcom/fyber/utils/m;

    move-result-object v2

    invoke-static {v2}, Lcom/fyber/utils/m;->b(Lcom/fyber/utils/m;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 327
    iget-object v0, p0, Lcom/fyber/utils/m$e;->a:Ljava/util/Map;

    const-string v1, "screen_density_x"

    invoke-static {}, Lcom/fyber/utils/m;->i()Lcom/fyber/utils/m;

    move-result-object v2

    invoke-static {v2}, Lcom/fyber/utils/m;->c(Lcom/fyber/utils/m;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    iget-object v0, p0, Lcom/fyber/utils/m$e;->a:Ljava/util/Map;

    const-string v1, "screen_density_y"

    invoke-static {}, Lcom/fyber/utils/m;->i()Lcom/fyber/utils/m;

    move-result-object v2

    invoke-static {v2}, Lcom/fyber/utils/m;->d(Lcom/fyber/utils/m;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 330
    :cond_1
    iget-object v0, p0, Lcom/fyber/utils/m$e;->a:Ljava/util/Map;

    goto :goto_0
.end method
