.class public final Lcom/google/android/a/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/a/a/b$c;,
        Lcom/google/android/a/a/b$b;,
        Lcom/google/android/a/a/b$a;,
        Lcom/google/android/a/a/b$e;,
        Lcom/google/android/a/a/b$f;,
        Lcom/google/android/a/a/b$d;
    }
.end annotation


# static fields
.field public static a:Z

.field public static b:Z


# instance fields
.field private A:I

.field private B:J

.field private C:J

.field private D:J

.field private E:F

.field private F:[B

.field private G:I

.field private H:I

.field private final c:Lcom/google/android/a/a/a;

.field private final d:I

.field private final e:Landroid/os/ConditionVariable;

.field private final f:[J

.field private final g:Lcom/google/android/a/a/b$a;

.field private h:Landroid/media/AudioTrack;

.field private i:Landroid/media/AudioTrack;

.field private j:I

.field private k:I

.field private l:I

.field private m:Z

.field private n:I

.field private o:I

.field private p:J

.field private q:I

.field private r:I

.field private s:J

.field private t:J

.field private u:Z

.field private v:J

.field private w:Ljava/lang/reflect/Method;

.field private x:J

.field private y:J

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 179
    sput-boolean v0, Lcom/google/android/a/a/b;->a:Z

    .line 188
    sput-boolean v0, Lcom/google/android/a/a/b;->b:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 235
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-direct {p0, v0, v1}, Lcom/google/android/a/a/b;-><init>(Lcom/google/android/a/a/a;I)V

    .line 236
    return-void
.end method

.method public constructor <init>(Lcom/google/android/a/a/a;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 245
    iput-object p1, p0, Lcom/google/android/a/a/b;->c:Lcom/google/android/a/a/a;

    .line 246
    iput p2, p0, Lcom/google/android/a/a/b;->d:I

    .line 247
    new-instance v0, Landroid/os/ConditionVariable;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/a/a/b;->e:Landroid/os/ConditionVariable;

    .line 248
    sget v0, Lcom/google/android/a/f/n;->a:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 250
    :try_start_0
    const-class v1, Landroid/media/AudioTrack;

    const-string v2, "getLatency"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/a/a/b;->w:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 256
    :cond_0
    :goto_0
    sget v0, Lcom/google/android/a/f/n;->a:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_1

    .line 257
    new-instance v0, Lcom/google/android/a/a/b$c;

    invoke-direct {v0}, Lcom/google/android/a/a/b$c;-><init>()V

    iput-object v0, p0, Lcom/google/android/a/a/b;->g:Lcom/google/android/a/a/b$a;

    .line 263
    :goto_1
    const/16 v0, 0xa

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/google/android/a/a/b;->f:[J

    .line 264
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/a/a/b;->E:F

    .line 265
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/a/a/b;->A:I

    .line 266
    return-void

    .line 258
    :cond_1
    sget v0, Lcom/google/android/a/f/n;->a:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_2

    .line 259
    new-instance v0, Lcom/google/android/a/a/b$b;

    invoke-direct {v0}, Lcom/google/android/a/a/b$b;-><init>()V

    iput-object v0, p0, Lcom/google/android/a/a/b;->g:Lcom/google/android/a/a/b$a;

    goto :goto_1

    .line 261
    :cond_2
    new-instance v0, Lcom/google/android/a/a/b$a;

    invoke-direct {v0, v3}, Lcom/google/android/a/a/b$a;-><init>(Lcom/google/android/a/a/b$1;)V

    iput-object v0, p0, Lcom/google/android/a/a/b;->g:Lcom/google/android/a/a/b$a;

    goto :goto_1

    .line 252
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private static a(ILjava/nio/ByteBuffer;)I
    .locals 3

    .prologue
    .line 968
    const/4 v0, 0x7

    if-eq p0, v0, :cond_0

    const/16 v0, 0x8

    if-ne p0, v0, :cond_1

    .line 969
    :cond_0
    invoke-static {p1}, Lcom/google/android/a/f/d;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 973
    :goto_0
    return v0

    .line 970
    :cond_1
    const/4 v0, 0x5

    if-ne p0, v0, :cond_2

    .line 971
    invoke-static {}, Lcom/google/android/a/f/a;->a()I

    move-result v0

    goto :goto_0

    .line 972
    :cond_2
    const/4 v0, 0x6

    if-ne p0, v0, :cond_3

    .line 973
    invoke-static {p1}, Lcom/google/android/a/f/a;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    goto :goto_0

    .line 975
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected audio encoding: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Landroid/media/AudioTrack;Ljava/nio/ByteBuffer;I)I
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 664
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Landroid/media/AudioTrack;->write(Ljava/nio/ByteBuffer;II)I

    move-result v0

    return v0
.end method

.method private a(J)J
    .locals 3

    .prologue
    .line 908
    iget v0, p0, Lcom/google/android/a/a/b;->n:I

    int-to-long v0, v0

    div-long v0, p1, v0

    return-wide v0
.end method

.method static synthetic a(Lcom/google/android/a/a/b;)Landroid/os/ConditionVariable;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/a/a/b;->e:Landroid/os/ConditionVariable;

    return-object v0
.end method

.method private static a(Landroid/media/AudioTrack;F)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 709
    invoke-virtual {p0, p1}, Landroid/media/AudioTrack;->setVolume(F)I

    .line 710
    return-void
.end method

.method private static b(Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 953
    const/4 v1, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 963
    :goto_1
    return v0

    .line 953
    :sswitch_0
    const-string v2, "audio/ac3"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v2, "audio/eac3"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "audio/vnd.dts"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "audio/vnd.dts.hd"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    .line 955
    :pswitch_0
    const/4 v0, 0x5

    goto :goto_1

    .line 957
    :pswitch_1
    const/4 v0, 0x6

    goto :goto_1

    .line 959
    :pswitch_2
    const/4 v0, 0x7

    goto :goto_1

    .line 961
    :pswitch_3
    const/16 v0, 0x8

    goto :goto_1

    .line 953
    :sswitch_data_0
    .sparse-switch
        -0x41455b98 -> :sswitch_2
        0xb269698 -> :sswitch_0
        0x59ae0c65 -> :sswitch_1
        0x59c2dc42 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private b(J)J
    .locals 5

    .prologue
    .line 912
    const-wide/32 v0, 0xf4240

    mul-long/2addr v0, p1

    iget v2, p0, Lcom/google/android/a/a/b;->j:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    return-wide v0
.end method

.method private static b(Landroid/media/AudioTrack;F)V
    .locals 0

    .prologue
    .line 714
    invoke-virtual {p0, p1, p1}, Landroid/media/AudioTrack;->setStereoVolume(FF)I

    .line 715
    return-void
.end method

.method private c(J)J
    .locals 5

    .prologue
    .line 916
    iget v0, p0, Lcom/google/android/a/a/b;->j:I

    int-to-long v0, v0

    mul-long/2addr v0, p1

    const-wide/32 v2, 0xf4240

    div-long/2addr v0, v2

    return-wide v0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 698
    invoke-virtual {p0}, Lcom/google/android/a/a/b;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 705
    :goto_0
    return-void

    .line 700
    :cond_0
    sget v0, Lcom/google/android/a/f/n;->a:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    .line 701
    iget-object v0, p0, Lcom/google/android/a/a/b;->i:Landroid/media/AudioTrack;

    iget v1, p0, Lcom/google/android/a/a/b;->E:F

    invoke-static {v0, v1}, Lcom/google/android/a/a/b;->a(Landroid/media/AudioTrack;F)V

    goto :goto_0

    .line 703
    :cond_1
    iget-object v0, p0, Lcom/google/android/a/a/b;->i:Landroid/media/AudioTrack;

    iget v1, p0, Lcom/google/android/a/a/b;->E:F

    invoke-static {v0, v1}, Lcom/google/android/a/a/b;->b(Landroid/media/AudioTrack;F)V

    goto :goto_0
.end method

.method private m()V
    .locals 2

    .prologue
    .line 776
    iget-object v0, p0, Lcom/google/android/a/a/b;->h:Landroid/media/AudioTrack;

    if-nez v0, :cond_0

    .line 789
    :goto_0
    return-void

    .line 781
    :cond_0
    iget-object v0, p0, Lcom/google/android/a/a/b;->h:Landroid/media/AudioTrack;

    .line 782
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/a/a/b;->h:Landroid/media/AudioTrack;

    .line 783
    new-instance v1, Lcom/google/android/a/a/b$2;

    invoke-direct {v1, p0, v0}, Lcom/google/android/a/a/b$2;-><init>(Lcom/google/android/a/a/b;Landroid/media/AudioTrack;)V

    invoke-virtual {v1}, Lcom/google/android/a/a/b$2;->start()V

    goto :goto_0
.end method

.method private n()Z
    .locals 1

    .prologue
    .line 795
    invoke-virtual {p0}, Lcom/google/android/a/a/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/a/a/b;->A:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private o()V
    .locals 12

    .prologue
    .line 802
    iget-object v0, p0, Lcom/google/android/a/a/b;->g:Lcom/google/android/a/a/b$a;

    invoke-virtual {v0}, Lcom/google/android/a/a/b$a;->c()J

    move-result-wide v2

    .line 803
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-nez v0, :cond_1

    .line 880
    :cond_0
    :goto_0
    return-void

    .line 807
    :cond_1
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    const-wide/16 v4, 0x3e8

    div-long v4, v0, v4

    .line 808
    iget-wide v0, p0, Lcom/google/android/a/a/b;->t:J

    sub-long v0, v4, v0

    const-wide/16 v6, 0x7530

    cmp-long v0, v0, v6

    if-ltz v0, :cond_3

    .line 810
    iget-object v0, p0, Lcom/google/android/a/a/b;->f:[J

    iget v1, p0, Lcom/google/android/a/a/b;->q:I

    sub-long v6, v2, v4

    aput-wide v6, v0, v1

    .line 811
    iget v0, p0, Lcom/google/android/a/a/b;->q:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit8 v0, v0, 0xa

    iput v0, p0, Lcom/google/android/a/a/b;->q:I

    .line 812
    iget v0, p0, Lcom/google/android/a/a/b;->r:I

    const/16 v1, 0xa

    if-ge v0, v1, :cond_2

    .line 813
    iget v0, p0, Lcom/google/android/a/a/b;->r:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/a/a/b;->r:I

    .line 815
    :cond_2
    iput-wide v4, p0, Lcom/google/android/a/a/b;->t:J

    .line 816
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/a/a/b;->s:J

    .line 817
    const/4 v0, 0x0

    :goto_1
    iget v1, p0, Lcom/google/android/a/a/b;->r:I

    if-ge v0, v1, :cond_3

    .line 818
    iget-wide v6, p0, Lcom/google/android/a/a/b;->s:J

    iget-object v1, p0, Lcom/google/android/a/a/b;->f:[J

    aget-wide v8, v1, v0

    iget v1, p0, Lcom/google/android/a/a/b;->r:I

    int-to-long v10, v1

    div-long/2addr v8, v10

    add-long/2addr v6, v8

    iput-wide v6, p0, Lcom/google/android/a/a/b;->s:J

    .line 817
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 822
    :cond_3
    invoke-direct {p0}, Lcom/google/android/a/a/b;->s()Z

    move-result v0

    if-nez v0, :cond_0

    .line 828
    iget-wide v0, p0, Lcom/google/android/a/a/b;->v:J

    sub-long v0, v4, v0

    const-wide/32 v6, 0x7a120

    cmp-long v0, v0, v6

    if-ltz v0, :cond_0

    .line 829
    iget-object v0, p0, Lcom/google/android/a/a/b;->g:Lcom/google/android/a/a/b$a;

    invoke-virtual {v0}, Lcom/google/android/a/a/b$a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/a/a/b;->u:Z

    .line 830
    iget-boolean v0, p0, Lcom/google/android/a/a/b;->u:Z

    if-eqz v0, :cond_4

    .line 832
    iget-object v0, p0, Lcom/google/android/a/a/b;->g:Lcom/google/android/a/a/b$a;

    invoke-virtual {v0}, Lcom/google/android/a/a/b$a;->e()J

    move-result-wide v0

    const-wide/16 v6, 0x3e8

    div-long/2addr v0, v6

    .line 833
    iget-object v6, p0, Lcom/google/android/a/a/b;->g:Lcom/google/android/a/a/b$a;

    invoke-virtual {v6}, Lcom/google/android/a/a/b$a;->f()J

    move-result-wide v6

    .line 834
    iget-wide v8, p0, Lcom/google/android/a/a/b;->C:J

    cmp-long v8, v0, v8

    if-gez v8, :cond_6

    .line 836
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/a/a/b;->u:Z

    .line 860
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/google/android/a/a/b;->w:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/google/android/a/a/b;->m:Z

    if-nez v0, :cond_5

    .line 864
    :try_start_0
    iget-object v1, p0, Lcom/google/android/a/a/b;->w:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/google/android/a/a/b;->i:Landroid/media/AudioTrack;

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/a/a/b;->p:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/a/a/b;->D:J

    .line 867
    iget-wide v0, p0, Lcom/google/android/a/a/b;->D:J

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/a/a/b;->D:J

    .line 869
    iget-wide v0, p0, Lcom/google/android/a/a/b;->D:J

    const-wide/32 v2, 0x4c4b40

    cmp-long v0, v0, v2

    if-lez v0, :cond_5

    .line 870
    const-string v0, "AudioTrack"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Ignoring impossibly large audio latency: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/a/a/b;->D:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 871
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/a/a/b;->D:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 878
    :cond_5
    :goto_3
    iput-wide v4, p0, Lcom/google/android/a/a/b;->v:J

    goto/16 :goto_0

    .line 837
    :cond_6
    sub-long v8, v0, v4

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(J)J

    move-result-wide v8

    const-wide/32 v10, 0x4c4b40

    cmp-long v8, v8, v10

    if-lez v8, :cond_8

    .line 839
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Spurious audio timestamp (system clock mismatch): "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 842
    sget-boolean v1, Lcom/google/android/a/a/b;->b:Z

    if-eqz v1, :cond_7

    .line 843
    new-instance v1, Lcom/google/android/a/a/b$e;

    invoke-direct {v1, v0}, Lcom/google/android/a/a/b$e;-><init>(Ljava/lang/String;)V

    throw v1

    .line 845
    :cond_7
    const-string v1, "AudioTrack"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 846
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/a/a/b;->u:Z

    goto/16 :goto_2

    .line 847
    :cond_8
    invoke-direct {p0, v6, v7}, Lcom/google/android/a/a/b;->b(J)J

    move-result-wide v8

    sub-long/2addr v8, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(J)J

    move-result-wide v8

    const-wide/32 v10, 0x4c4b40

    cmp-long v8, v8, v10

    if-lez v8, :cond_4

    .line 850
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Spurious audio timestamp (frame position mismatch): "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 853
    sget-boolean v1, Lcom/google/android/a/a/b;->b:Z

    if-eqz v1, :cond_9

    .line 854
    new-instance v1, Lcom/google/android/a/a/b$e;

    invoke-direct {v1, v0}, Lcom/google/android/a/a/b$e;-><init>(Ljava/lang/String;)V

    throw v1

    .line 856
    :cond_9
    const-string v1, "AudioTrack"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 857
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/a/a/b;->u:Z

    goto/16 :goto_2

    .line 873
    :catch_0
    move-exception v0

    .line 875
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/a/a/b;->w:Ljava/lang/reflect/Method;

    goto/16 :goto_3
.end method

.method private p()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 890
    iget-object v0, p0, Lcom/google/android/a/a/b;->i:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getState()I

    move-result v0

    .line 891
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 892
    return-void

    .line 896
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/a/a/b;->i:Landroid/media/AudioTrack;

    invoke-virtual {v1}, Landroid/media/AudioTrack;->release()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 901
    iput-object v2, p0, Lcom/google/android/a/a/b;->i:Landroid/media/AudioTrack;

    .line 904
    :goto_0
    new-instance v1, Lcom/google/android/a/a/b$d;

    iget v2, p0, Lcom/google/android/a/a/b;->j:I

    iget v3, p0, Lcom/google/android/a/a/b;->k:I

    iget v4, p0, Lcom/google/android/a/a/b;->o:I

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/google/android/a/a/b$d;-><init>(IIII)V

    throw v1

    .line 897
    :catch_0
    move-exception v1

    .line 901
    iput-object v2, p0, Lcom/google/android/a/a/b;->i:Landroid/media/AudioTrack;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v2, p0, Lcom/google/android/a/a/b;->i:Landroid/media/AudioTrack;

    throw v0
.end method

.method private q()J
    .locals 2

    .prologue
    .line 920
    iget-boolean v0, p0, Lcom/google/android/a/a/b;->m:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/a/a/b;->y:J

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/google/android/a/a/b;->x:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/a/a/b;->a(J)J

    move-result-wide v0

    goto :goto_0
.end method

.method private r()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 924
    iput-wide v2, p0, Lcom/google/android/a/a/b;->s:J

    .line 925
    iput v0, p0, Lcom/google/android/a/a/b;->r:I

    .line 926
    iput v0, p0, Lcom/google/android/a/a/b;->q:I

    .line 927
    iput-wide v2, p0, Lcom/google/android/a/a/b;->t:J

    .line 928
    iput-boolean v0, p0, Lcom/google/android/a/a/b;->u:Z

    .line 929
    iput-wide v2, p0, Lcom/google/android/a/a/b;->v:J

    .line 930
    return-void
.end method

.method private s()Z
    .locals 2

    .prologue
    .line 937
    sget v0, Lcom/google/android/a/f/n;->a:I

    const/16 v1, 0x17

    if-ge v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/a/a/b;->l:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/a/a/b;->l:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private t()Z
    .locals 2

    .prologue
    .line 947
    invoke-direct {p0}, Lcom/google/android/a/a/b;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/a/a/b;->i:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/a/a/b;->i:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlaybackHeadPosition()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(I)I
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v6, 0x1

    .line 452
    iget-object v0, p0, Lcom/google/android/a/a/b;->e:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 454
    if-nez p1, :cond_2

    .line 455
    new-instance v0, Landroid/media/AudioTrack;

    iget v1, p0, Lcom/google/android/a/a/b;->d:I

    iget v2, p0, Lcom/google/android/a/a/b;->j:I

    iget v3, p0, Lcom/google/android/a/a/b;->k:I

    iget v4, p0, Lcom/google/android/a/a/b;->l:I

    iget v5, p0, Lcom/google/android/a/a/b;->o:I

    invoke-direct/range {v0 .. v6}, Landroid/media/AudioTrack;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/google/android/a/a/b;->i:Landroid/media/AudioTrack;

    .line 462
    :goto_0
    invoke-direct {p0}, Lcom/google/android/a/a/b;->p()V

    .line 464
    iget-object v0, p0, Lcom/google/android/a/a/b;->i:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getAudioSessionId()I

    move-result v7

    .line 465
    sget-boolean v0, Lcom/google/android/a/a/b;->a:Z

    if-eqz v0, :cond_1

    .line 466
    sget v0, Lcom/google/android/a/f/n;->a:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_1

    .line 469
    iget-object v0, p0, Lcom/google/android/a/a/b;->h:Landroid/media/AudioTrack;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/a/a/b;->h:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getAudioSessionId()I

    move-result v0

    if-eq v7, v0, :cond_0

    .line 471
    invoke-direct {p0}, Lcom/google/android/a/a/b;->m()V

    .line 473
    :cond_0
    iget-object v0, p0, Lcom/google/android/a/a/b;->h:Landroid/media/AudioTrack;

    if-nez v0, :cond_1

    .line 474
    const/16 v2, 0xfa0

    .line 475
    const/4 v3, 0x4

    .line 478
    new-instance v0, Landroid/media/AudioTrack;

    iget v1, p0, Lcom/google/android/a/a/b;->d:I

    const/4 v6, 0x0

    move v4, v8

    move v5, v8

    invoke-direct/range {v0 .. v7}, Landroid/media/AudioTrack;-><init>(IIIIIII)V

    iput-object v0, p0, Lcom/google/android/a/a/b;->h:Landroid/media/AudioTrack;

    .line 484
    :cond_1
    iget-object v0, p0, Lcom/google/android/a/a/b;->g:Lcom/google/android/a/a/b$a;

    iget-object v1, p0, Lcom/google/android/a/a/b;->i:Landroid/media/AudioTrack;

    invoke-direct {p0}, Lcom/google/android/a/a/b;->s()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/a/a/b$a;->a(Landroid/media/AudioTrack;Z)V

    .line 485
    invoke-direct {p0}, Lcom/google/android/a/a/b;->l()V

    .line 487
    return v7

    .line 459
    :cond_2
    new-instance v0, Landroid/media/AudioTrack;

    iget v1, p0, Lcom/google/android/a/a/b;->d:I

    iget v2, p0, Lcom/google/android/a/a/b;->j:I

    iget v3, p0, Lcom/google/android/a/a/b;->k:I

    iget v4, p0, Lcom/google/android/a/a/b;->l:I

    iget v5, p0, Lcom/google/android/a/a/b;->o:I

    move v7, p1

    invoke-direct/range {v0 .. v7}, Landroid/media/AudioTrack;-><init>(IIIIIII)V

    iput-object v0, p0, Lcom/google/android/a/a/b;->i:Landroid/media/AudioTrack;

    goto :goto_0
.end method

.method public a(Ljava/nio/ByteBuffer;IIJ)I
    .locals 10

    .prologue
    .line 553
    if-nez p3, :cond_1

    .line 554
    const/4 v0, 0x2

    .line 648
    :cond_0
    :goto_0
    return v0

    .line 557
    :cond_1
    invoke-direct {p0}, Lcom/google/android/a/a/b;->s()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 560
    iget-object v0, p0, Lcom/google/android/a/a/b;->i:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 561
    const/4 v0, 0x0

    goto :goto_0

    .line 567
    :cond_2
    iget-object v0, p0, Lcom/google/android/a/a/b;->i:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/a/a/b;->g:Lcom/google/android/a/a/b$a;

    invoke-virtual {v0}, Lcom/google/android/a/a/b$a;->b()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    .line 569
    const/4 v0, 0x0

    goto :goto_0

    .line 573
    :cond_3
    const/4 v2, 0x0

    .line 574
    iget v0, p0, Lcom/google/android/a/a/b;->H:I

    if-nez v0, :cond_11

    .line 577
    iput p3, p0, Lcom/google/android/a/a/b;->H:I

    .line 578
    invoke-virtual {p1, p2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 579
    iget-boolean v0, p0, Lcom/google/android/a/a/b;->m:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/google/android/a/a/b;->z:I

    if-nez v0, :cond_4

    .line 581
    iget v0, p0, Lcom/google/android/a/a/b;->l:I

    invoke-static {v0, p1}, Lcom/google/android/a/a/b;->a(ILjava/nio/ByteBuffer;)I

    move-result v0

    iput v0, p0, Lcom/google/android/a/a/b;->z:I

    .line 583
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/a/a/b;->m:Z

    if-eqz v0, :cond_9

    iget v0, p0, Lcom/google/android/a/a/b;->z:I

    int-to-long v0, v0

    .line 584
    :goto_1
    invoke-direct {p0, v0, v1}, Lcom/google/android/a/a/b;->b(J)J

    move-result-wide v0

    .line 586
    sub-long v0, p4, v0

    .line 587
    iget v3, p0, Lcom/google/android/a/a/b;->A:I

    if-nez v3, :cond_a

    .line 588
    const-wide/16 v4, 0x0

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/a/a/b;->B:J

    .line 589
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/a/a/b;->A:I

    move v0, v2

    .line 607
    :goto_2
    sget v1, Lcom/google/android/a/f/n;->a:I

    const/16 v2, 0x15

    if-ge v1, v2, :cond_7

    .line 609
    iget-object v1, p0, Lcom/google/android/a/a/b;->F:[B

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/a/a/b;->F:[B

    array-length v1, v1

    if-ge v1, p3, :cond_6

    .line 610
    :cond_5
    new-array v1, p3, [B

    iput-object v1, p0, Lcom/google/android/a/a/b;->F:[B

    .line 612
    :cond_6
    iget-object v1, p0, Lcom/google/android/a/a/b;->F:[B

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2, p3}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 613
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/a/a/b;->G:I

    .line 617
    :cond_7
    :goto_3
    const/4 v1, 0x0

    .line 618
    sget v2, Lcom/google/android/a/f/n;->a:I

    const/16 v3, 0x15

    if-ge v2, v3, :cond_c

    .line 620
    iget-wide v2, p0, Lcom/google/android/a/a/b;->x:J

    iget-object v4, p0, Lcom/google/android/a/a/b;->g:Lcom/google/android/a/a/b$a;

    invoke-virtual {v4}, Lcom/google/android/a/a/b$a;->b()J

    move-result-wide v4

    iget v6, p0, Lcom/google/android/a/a/b;->n:I

    int-to-long v6, v6

    mul-long/2addr v4, v6

    sub-long/2addr v2, v4

    long-to-int v2, v2

    .line 622
    iget v3, p0, Lcom/google/android/a/a/b;->o:I

    sub-int v2, v3, v2

    .line 623
    if-lez v2, :cond_8

    .line 624
    iget v1, p0, Lcom/google/android/a/a/b;->H:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 625
    iget-object v2, p0, Lcom/google/android/a/a/b;->i:Landroid/media/AudioTrack;

    iget-object v3, p0, Lcom/google/android/a/a/b;->F:[B

    iget v4, p0, Lcom/google/android/a/a/b;->G:I

    invoke-virtual {v2, v3, v4, v1}, Landroid/media/AudioTrack;->write([BII)I

    move-result v1

    .line 626
    if-ltz v1, :cond_8

    .line 627
    iget v2, p0, Lcom/google/android/a/a/b;->G:I

    add-int/2addr v2, v1

    iput v2, p0, Lcom/google/android/a/a/b;->G:I

    .line 634
    :cond_8
    :goto_4
    if-gez v1, :cond_d

    .line 635
    new-instance v0, Lcom/google/android/a/a/b$f;

    invoke-direct {v0, v1}, Lcom/google/android/a/a/b$f;-><init>(I)V

    throw v0

    .line 583
    :cond_9
    int-to-long v0, p3

    invoke-direct {p0, v0, v1}, Lcom/google/android/a/a/b;->a(J)J

    move-result-wide v0

    goto :goto_1

    .line 592
    :cond_a
    iget-wide v4, p0, Lcom/google/android/a/a/b;->B:J

    invoke-direct {p0}, Lcom/google/android/a/a/b;->q()J

    move-result-wide v6

    invoke-direct {p0, v6, v7}, Lcom/google/android/a/a/b;->b(J)J

    move-result-wide v6

    add-long/2addr v4, v6

    .line 593
    iget v3, p0, Lcom/google/android/a/a/b;->A:I

    const/4 v6, 0x1

    if-ne v3, v6, :cond_b

    sub-long v6, v4, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(J)J

    move-result-wide v6

    const-wide/32 v8, 0x30d40

    cmp-long v3, v6, v8

    if-lez v3, :cond_b

    .line 595
    const-string v3, "AudioTrack"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Discontinuity detected [expected "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", got "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 597
    const/4 v3, 0x2

    iput v3, p0, Lcom/google/android/a/a/b;->A:I

    .line 599
    :cond_b
    iget v3, p0, Lcom/google/android/a/a/b;->A:I

    const/4 v6, 0x2

    if-ne v3, v6, :cond_10

    .line 602
    iget-wide v2, p0, Lcom/google/android/a/a/b;->B:J

    sub-long/2addr v0, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/a/a/b;->B:J

    .line 603
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/a/a/b;->A:I

    .line 604
    const/4 v0, 0x1

    goto/16 :goto_2

    .line 631
    :cond_c
    iget-object v1, p0, Lcom/google/android/a/a/b;->i:Landroid/media/AudioTrack;

    iget v2, p0, Lcom/google/android/a/a/b;->H:I

    invoke-static {v1, p1, v2}, Lcom/google/android/a/a/b;->a(Landroid/media/AudioTrack;Ljava/nio/ByteBuffer;I)I

    move-result v1

    goto :goto_4

    .line 638
    :cond_d
    iget v2, p0, Lcom/google/android/a/a/b;->H:I

    sub-int/2addr v2, v1

    iput v2, p0, Lcom/google/android/a/a/b;->H:I

    .line 639
    iget-boolean v2, p0, Lcom/google/android/a/a/b;->m:Z

    if-nez v2, :cond_e

    .line 640
    iget-wide v2, p0, Lcom/google/android/a/a/b;->x:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/a/a/b;->x:J

    .line 642
    :cond_e
    iget v1, p0, Lcom/google/android/a/a/b;->H:I

    if-nez v1, :cond_0

    .line 643
    iget-boolean v1, p0, Lcom/google/android/a/a/b;->m:Z

    if-eqz v1, :cond_f

    .line 644
    iget-wide v2, p0, Lcom/google/android/a/a/b;->y:J

    iget v1, p0, Lcom/google/android/a/a/b;->z:I

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/a/a/b;->y:J

    .line 646
    :cond_f
    or-int/lit8 v0, v0, 0x2

    goto/16 :goto_0

    :cond_10
    move v0, v2

    goto/16 :goto_2

    :cond_11
    move v0, v2

    goto/16 :goto_3
.end method

.method public a(Z)J
    .locals 6

    .prologue
    const-wide/16 v4, 0x3e8

    .line 297
    invoke-direct {p0}, Lcom/google/android/a/a/b;->n()Z

    move-result v0

    if-nez v0, :cond_1

    .line 298
    const-wide/high16 v0, -0x8000000000000000L

    .line 332
    :cond_0
    :goto_0
    return-wide v0

    .line 301
    :cond_1
    iget-object v0, p0, Lcom/google/android/a/a/b;->i:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 302
    invoke-direct {p0}, Lcom/google/android/a/a/b;->o()V

    .line 305
    :cond_2
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    div-long/2addr v0, v4

    .line 307
    iget-boolean v2, p0, Lcom/google/android/a/a/b;->u:Z

    if-eqz v2, :cond_3

    .line 309
    iget-object v2, p0, Lcom/google/android/a/a/b;->g:Lcom/google/android/a/a/b$a;

    invoke-virtual {v2}, Lcom/google/android/a/a/b$a;->e()J

    move-result-wide v2

    div-long/2addr v2, v4

    sub-long/2addr v0, v2

    .line 311
    long-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/a/a/b;->g:Lcom/google/android/a/a/b$a;

    invoke-virtual {v1}, Lcom/google/android/a/a/b$a;->g()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-long v0, v0

    .line 313
    invoke-direct {p0, v0, v1}, Lcom/google/android/a/a/b;->c(J)J

    move-result-wide v0

    .line 315
    iget-object v2, p0, Lcom/google/android/a/a/b;->g:Lcom/google/android/a/a/b$a;

    invoke-virtual {v2}, Lcom/google/android/a/a/b$a;->f()J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 316
    invoke-direct {p0, v0, v1}, Lcom/google/android/a/a/b;->b(J)J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/a/a/b;->B:J

    add-long/2addr v0, v2

    .line 317
    goto :goto_0

    .line 318
    :cond_3
    iget v2, p0, Lcom/google/android/a/a/b;->r:I

    if-nez v2, :cond_4

    .line 320
    iget-object v0, p0, Lcom/google/android/a/a/b;->g:Lcom/google/android/a/a/b$a;

    invoke-virtual {v0}, Lcom/google/android/a/a/b$a;->c()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/a/a/b;->B:J

    add-long/2addr v0, v2

    .line 327
    :goto_1
    if-nez p1, :cond_0

    .line 328
    iget-wide v2, p0, Lcom/google/android/a/a/b;->D:J

    sub-long/2addr v0, v2

    goto :goto_0

    .line 325
    :cond_4
    iget-wide v2, p0, Lcom/google/android/a/a/b;->s:J

    add-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/a/a/b;->B:J

    add-long/2addr v0, v2

    goto :goto_1
.end method

.method public a(F)V
    .locals 1

    .prologue
    .line 691
    iget v0, p0, Lcom/google/android/a/a/b;->E:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 692
    iput p1, p0, Lcom/google/android/a/a/b;->E:F

    .line 693
    invoke-direct {p0}, Lcom/google/android/a/a/b;->l()V

    .line 695
    :cond_0
    return-void
.end method

.method public a(Landroid/media/MediaFormat;Z)V
    .locals 1

    .prologue
    .line 343
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/a/a/b;->a(Landroid/media/MediaFormat;ZI)V

    .line 344
    return-void
.end method

.method public a(Landroid/media/MediaFormat;ZI)V
    .locals 10

    .prologue
    .line 355
    const-string v0, "channel-count"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v2

    .line 357
    packed-switch v2, :pswitch_data_0

    .line 383
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported channel count: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 359
    :pswitch_0
    const/4 v0, 0x4

    .line 385
    :goto_0
    const-string v1, "sample-rate"

    invoke-virtual {p1, v1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v3

    .line 386
    const-string v1, "mime"

    invoke-virtual {p1, v1}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 387
    if-eqz p2, :cond_0

    invoke-static {v1}, Lcom/google/android/a/a/b;->b(Ljava/lang/String;)I

    move-result v1

    .line 388
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/a/a/b;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/google/android/a/a/b;->j:I

    if-ne v4, v3, :cond_1

    iget v4, p0, Lcom/google/android/a/a/b;->k:I

    if-ne v4, v0, :cond_1

    iget v4, p0, Lcom/google/android/a/a/b;->l:I

    if-ne v4, v1, :cond_1

    .line 428
    :goto_2
    return-void

    .line 362
    :pswitch_1
    const/16 v0, 0xc

    .line 363
    goto :goto_0

    .line 365
    :pswitch_2
    const/16 v0, 0x1c

    .line 366
    goto :goto_0

    .line 368
    :pswitch_3
    const/16 v0, 0xcc

    .line 369
    goto :goto_0

    .line 371
    :pswitch_4
    const/16 v0, 0xdc

    .line 372
    goto :goto_0

    .line 374
    :pswitch_5
    const/16 v0, 0xfc

    .line 375
    goto :goto_0

    .line 377
    :pswitch_6
    const/16 v0, 0x4fc

    .line 378
    goto :goto_0

    .line 380
    :pswitch_7
    sget v0, Lcom/google/android/a/a;->a:I

    goto :goto_0

    .line 387
    :cond_0
    const/4 v1, 0x2

    goto :goto_1

    .line 394
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/a/a/b;->j()V

    .line 396
    iput v1, p0, Lcom/google/android/a/a/b;->l:I

    .line 397
    iput-boolean p2, p0, Lcom/google/android/a/a/b;->m:Z

    .line 398
    iput v3, p0, Lcom/google/android/a/a/b;->j:I

    .line 399
    iput v0, p0, Lcom/google/android/a/a/b;->k:I

    .line 400
    mul-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/android/a/a/b;->n:I

    .line 402
    if-eqz p3, :cond_2

    .line 403
    iput p3, p0, Lcom/google/android/a/a/b;->o:I

    .line 426
    :goto_3
    if-eqz p2, :cond_9

    const-wide/16 v0, -0x1

    :goto_4
    iput-wide v0, p0, Lcom/google/android/a/a/b;->p:J

    goto :goto_2

    .line 404
    :cond_2
    if-eqz p2, :cond_5

    .line 407
    const/4 v0, 0x5

    if-eq v1, v0, :cond_3

    const/4 v0, 0x6

    if-ne v1, v0, :cond_4

    .line 409
    :cond_3
    const/16 v0, 0x5000

    iput v0, p0, Lcom/google/android/a/a/b;->o:I

    goto :goto_3

    .line 412
    :cond_4
    const v0, 0xc000

    iput v0, p0, Lcom/google/android/a/a/b;->o:I

    goto :goto_3

    .line 415
    :cond_5
    invoke-static {v3, v0, v1}, Landroid/media/AudioTrack;->getMinBufferSize(III)I

    move-result v1

    .line 417
    const/4 v0, -0x2

    if-eq v1, v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, Lcom/google/android/a/f/b;->b(Z)V

    .line 418
    mul-int/lit8 v2, v1, 0x4

    .line 419
    const-wide/32 v4, 0x3d090

    invoke-direct {p0, v4, v5}, Lcom/google/android/a/a/b;->c(J)J

    move-result-wide v4

    long-to-int v0, v4

    iget v3, p0, Lcom/google/android/a/a/b;->n:I

    mul-int/2addr v0, v3

    .line 420
    int-to-long v4, v1

    const-wide/32 v6, 0xb71b0

    invoke-direct {p0, v6, v7}, Lcom/google/android/a/a/b;->c(J)J

    move-result-wide v6

    iget v1, p0, Lcom/google/android/a/a/b;->n:I

    int-to-long v8, v1

    mul-long/2addr v6, v8

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    long-to-int v1, v4

    .line 422
    if-ge v2, v0, :cond_7

    :goto_6
    iput v0, p0, Lcom/google/android/a/a/b;->o:I

    goto :goto_3

    .line 417
    :cond_6
    const/4 v0, 0x0

    goto :goto_5

    .line 422
    :cond_7
    if-le v2, v1, :cond_8

    move v0, v1

    goto :goto_6

    :cond_8
    move v0, v2

    goto :goto_6

    .line 426
    :cond_9
    iget v0, p0, Lcom/google/android/a/a/b;->o:I

    int-to-long v0, v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/a/a/b;->a(J)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/a/a/b;->b(J)J

    move-result-wide v0

    goto :goto_4

    .line 357
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public a(Landroid/media/PlaybackParams;)V
    .locals 1

    .prologue
    .line 683
    iget-object v0, p0, Lcom/google/android/a/a/b;->g:Lcom/google/android/a/a/b$a;

    invoke-virtual {v0, p1}, Lcom/google/android/a/a/b$a;->a(Landroid/media/PlaybackParams;)V

    .line 684
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lcom/google/android/a/a/b;->i:Landroid/media/AudioTrack;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/a/a/b;->c:Lcom/google/android/a/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/a/a/b;->c:Lcom/google/android/a/a/a;

    invoke-static {p1}, Lcom/google/android/a/a/b;->b(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/a/a/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 436
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/a/a/b;->a(I)I

    move-result v0

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 499
    iget v0, p0, Lcom/google/android/a/a/b;->o:I

    return v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 513
    iget-wide v0, p0, Lcom/google/android/a/a/b;->p:J

    return-wide v0
.end method

.method public e()V
    .locals 4

    .prologue
    .line 520
    invoke-virtual {p0}, Lcom/google/android/a/a/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 521
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/a/a/b;->C:J

    .line 522
    iget-object v0, p0, Lcom/google/android/a/a/b;->i:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->play()V

    .line 524
    :cond_0
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 531
    iget v0, p0, Lcom/google/android/a/a/b;->A:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 532
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/a/a/b;->A:I

    .line 534
    :cond_0
    return-void
.end method

.method public g()V
    .locals 4

    .prologue
    .line 656
    invoke-virtual {p0}, Lcom/google/android/a/a/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 657
    iget-object v0, p0, Lcom/google/android/a/a/b;->g:Lcom/google/android/a/a/b$a;

    invoke-direct {p0}, Lcom/google/android/a/a/b;->q()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/a/a/b$a;->a(J)V

    .line 659
    :cond_0
    return-void
.end method

.method public h()Z
    .locals 4

    .prologue
    .line 671
    invoke-virtual {p0}, Lcom/google/android/a/a/b;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/a/a/b;->q()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/a/a/b;->g:Lcom/google/android/a/a/b$a;

    invoke-virtual {v2}, Lcom/google/android/a/a/b$a;->b()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/a/a/b;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()V
    .locals 1

    .prologue
    .line 721
    invoke-virtual {p0}, Lcom/google/android/a/a/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 722
    invoke-direct {p0}, Lcom/google/android/a/a/b;->r()V

    .line 723
    iget-object v0, p0, Lcom/google/android/a/a/b;->g:Lcom/google/android/a/a/b$a;

    invoke-virtual {v0}, Lcom/google/android/a/a/b$a;->a()V

    .line 725
    :cond_0
    return-void
.end method

.method public j()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 733
    invoke-virtual {p0}, Lcom/google/android/a/a/b;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 734
    iput-wide v4, p0, Lcom/google/android/a/a/b;->x:J

    .line 735
    iput-wide v4, p0, Lcom/google/android/a/a/b;->y:J

    .line 736
    iput v2, p0, Lcom/google/android/a/a/b;->z:I

    .line 737
    iput v2, p0, Lcom/google/android/a/a/b;->H:I

    .line 738
    iput v2, p0, Lcom/google/android/a/a/b;->A:I

    .line 739
    iput-wide v4, p0, Lcom/google/android/a/a/b;->D:J

    .line 740
    invoke-direct {p0}, Lcom/google/android/a/a/b;->r()V

    .line 741
    iget-object v0, p0, Lcom/google/android/a/a/b;->i:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v0

    .line 742
    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 743
    iget-object v0, p0, Lcom/google/android/a/a/b;->i:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->pause()V

    .line 746
    :cond_0
    iget-object v0, p0, Lcom/google/android/a/a/b;->i:Landroid/media/AudioTrack;

    .line 747
    iput-object v3, p0, Lcom/google/android/a/a/b;->i:Landroid/media/AudioTrack;

    .line 748
    iget-object v1, p0, Lcom/google/android/a/a/b;->g:Lcom/google/android/a/a/b$a;

    invoke-virtual {v1, v3, v2}, Lcom/google/android/a/a/b$a;->a(Landroid/media/AudioTrack;Z)V

    .line 749
    iget-object v1, p0, Lcom/google/android/a/a/b;->e:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->close()V

    .line 750
    new-instance v1, Lcom/google/android/a/a/b$1;

    invoke-direct {v1, p0, v0}, Lcom/google/android/a/a/b$1;-><init>(Lcom/google/android/a/a/b;Landroid/media/AudioTrack;)V

    invoke-virtual {v1}, Lcom/google/android/a/a/b$1;->start()V

    .line 762
    :cond_1
    return-void
.end method

.method public k()V
    .locals 0

    .prologue
    .line 768
    invoke-virtual {p0}, Lcom/google/android/a/a/b;->j()V

    .line 769
    invoke-direct {p0}, Lcom/google/android/a/a/b;->m()V

    .line 770
    return-void
.end method
