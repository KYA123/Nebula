.class public Lcom/google/android/a/c/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/a/c/m;


# instance fields
.field private final a:Lcom/google/android/a/c/k;

.field private final b:Lcom/google/android/a/r;

.field private c:Z

.field private d:J

.field private e:J

.field private volatile f:J

.field private volatile g:Lcom/google/android/a/o;


# direct methods
.method public constructor <init>(Lcom/google/android/a/e/b;)V
    .locals 4

    .prologue
    const-wide/high16 v2, -0x8000000000000000L

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v0, Lcom/google/android/a/c/k;

    invoke-direct {v0, p1}, Lcom/google/android/a/c/k;-><init>(Lcom/google/android/a/e/b;)V

    iput-object v0, p0, Lcom/google/android/a/c/c;->a:Lcom/google/android/a/c/k;

    .line 51
    new-instance v0, Lcom/google/android/a/r;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/a/r;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/a/c/c;->b:Lcom/google/android/a/r;

    .line 52
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/a/c/c;->c:Z

    .line 53
    iput-wide v2, p0, Lcom/google/android/a/c/c;->d:J

    .line 54
    iput-wide v2, p0, Lcom/google/android/a/c/c;->e:J

    .line 55
    iput-wide v2, p0, Lcom/google/android/a/c/c;->f:J

    .line 56
    return-void
.end method

.method private f()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 212
    iget-object v1, p0, Lcom/google/android/a/c/c;->a:Lcom/google/android/a/c/k;

    iget-object v2, p0, Lcom/google/android/a/c/c;->b:Lcom/google/android/a/r;

    invoke-virtual {v1, v2}, Lcom/google/android/a/c/k;->a(Lcom/google/android/a/r;)Z

    move-result v1

    .line 213
    iget-boolean v2, p0, Lcom/google/android/a/c/c;->c:Z

    if-eqz v2, :cond_0

    .line 214
    :goto_0
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/a/c/c;->b:Lcom/google/android/a/r;

    invoke-virtual {v2}, Lcom/google/android/a/r;->c()Z

    move-result v2

    if-nez v2, :cond_0

    .line 215
    iget-object v1, p0, Lcom/google/android/a/c/c;->a:Lcom/google/android/a/c/k;

    invoke-virtual {v1}, Lcom/google/android/a/c/k;->b()V

    .line 216
    iget-object v1, p0, Lcom/google/android/a/c/c;->a:Lcom/google/android/a/c/k;

    iget-object v2, p0, Lcom/google/android/a/c/c;->b:Lcom/google/android/a/r;

    invoke-virtual {v1, v2}, Lcom/google/android/a/c/k;->a(Lcom/google/android/a/r;)Z

    move-result v1

    goto :goto_0

    .line 219
    :cond_0
    if-nez v1, :cond_2

    .line 225
    :cond_1
    :goto_1
    return v0

    .line 222
    :cond_2
    iget-wide v2, p0, Lcom/google/android/a/c/c;->e:J

    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/a/c/c;->b:Lcom/google/android/a/r;

    iget-wide v2, v1, Lcom/google/android/a/r;->e:J

    iget-wide v4, p0, Lcom/google/android/a/c/c;->e:J

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    .line 225
    :cond_3
    const/4 v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public a(Lcom/google/android/a/c/f;IZ)I
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/google/android/a/c/c;->a:Lcom/google/android/a/c/k;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/a/c/k;->a(Lcom/google/android/a/c/f;IZ)I

    move-result v0

    return v0
.end method

.method public a()V
    .locals 4

    .prologue
    const-wide/high16 v2, -0x8000000000000000L

    .line 64
    iget-object v0, p0, Lcom/google/android/a/c/c;->a:Lcom/google/android/a/c/k;

    invoke-virtual {v0}, Lcom/google/android/a/c/k;->a()V

    .line 65
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/a/c/c;->c:Z

    .line 66
    iput-wide v2, p0, Lcom/google/android/a/c/c;->d:J

    .line 67
    iput-wide v2, p0, Lcom/google/android/a/c/c;->e:J

    .line 68
    iput-wide v2, p0, Lcom/google/android/a/c/c;->f:J

    .line 69
    return-void
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 154
    :goto_0
    iget-object v0, p0, Lcom/google/android/a/c/c;->a:Lcom/google/android/a/c/k;

    iget-object v1, p0, Lcom/google/android/a/c/c;->b:Lcom/google/android/a/r;

    invoke-virtual {v0, v1}, Lcom/google/android/a/c/k;->a(Lcom/google/android/a/r;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/a/c/c;->b:Lcom/google/android/a/r;

    iget-wide v0, v0, Lcom/google/android/a/r;->e:J

    cmp-long v0, v0, p1

    if-gez v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/google/android/a/c/c;->a:Lcom/google/android/a/c/k;

    invoke-virtual {v0}, Lcom/google/android/a/c/k;->b()V

    .line 157
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/a/c/c;->c:Z

    goto :goto_0

    .line 159
    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/google/android/a/c/c;->d:J

    .line 160
    return-void
.end method

.method public a(JIII[B)V
    .locals 9

    .prologue
    .line 266
    iget-wide v0, p0, Lcom/google/android/a/c/c;->f:J

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/a/c/c;->f:J

    .line 267
    iget-object v0, p0, Lcom/google/android/a/c/c;->a:Lcom/google/android/a/c/k;

    iget-object v1, p0, Lcom/google/android/a/c/c;->a:Lcom/google/android/a/c/k;

    invoke-virtual {v1}, Lcom/google/android/a/c/k;->c()J

    move-result-wide v2

    int-to-long v4, p4

    sub-long/2addr v2, v4

    int-to-long v4, p5

    sub-long v4, v2, v4

    move-wide v1, p1

    move v3, p3

    move v6, p4

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/a/c/k;->a(JIJI[B)V

    .line 269
    return-void
.end method

.method public a(Lcom/google/android/a/f/j;I)V
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/a/c/c;->a:Lcom/google/android/a/c/k;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/a/c/k;->a(Lcom/google/android/a/f/j;I)V

    .line 262
    return-void
.end method

.method public a(Lcom/google/android/a/o;)V
    .locals 0

    .prologue
    .line 250
    iput-object p1, p0, Lcom/google/android/a/c/c;->g:Lcom/google/android/a/o;

    .line 251
    return-void
.end method

.method public a(Lcom/google/android/a/r;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 137
    invoke-direct {p0}, Lcom/google/android/a/c/c;->f()Z

    move-result v1

    .line 138
    if-nez v1, :cond_0

    .line 145
    :goto_0
    return v0

    .line 142
    :cond_0
    iget-object v1, p0, Lcom/google/android/a/c/c;->a:Lcom/google/android/a/c/k;

    invoke-virtual {v1, p1}, Lcom/google/android/a/c/k;->b(Lcom/google/android/a/r;)Z

    .line 143
    iput-boolean v0, p0, Lcom/google/android/a/c/c;->c:Z

    .line 144
    iget-wide v0, p1, Lcom/google/android/a/r;->e:J

    iput-wide v0, p0, Lcom/google/android/a/c/c;->d:J

    .line 145
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/a/c/c;->g:Lcom/google/android/a/o;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(J)Z
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/a/c/c;->a:Lcom/google/android/a/c/k;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/a/c/k;->a(J)Z

    move-result v0

    return v0
.end method

.method public c()Lcom/google/android/a/o;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/a/c/c;->g:Lcom/google/android/a/o;

    return-object v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 117
    iget-wide v0, p0, Lcom/google/android/a/c/c;->f:J

    return-wide v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 124
    invoke-direct {p0}, Lcom/google/android/a/c/c;->f()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
