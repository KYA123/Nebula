.class final Lcom/google/android/a/c/d/g;
.super Lcom/google/android/a/c/d/e;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/a/c/d/g$a;
    }
.end annotation


# instance fields
.field private b:Z

.field private final c:Lcom/google/android/a/c/d/n;

.field private final d:[Z

.field private final e:Lcom/google/android/a/c/d/g$a;

.field private final f:Lcom/google/android/a/c/d/k;

.field private final g:Lcom/google/android/a/c/d/k;

.field private final h:Lcom/google/android/a/c/d/k;

.field private i:Z

.field private j:J

.field private k:J

.field private l:Z

.field private m:J

.field private n:J

.field private final o:Lcom/google/android/a/f/j;


# direct methods
.method public constructor <init>(Lcom/google/android/a/c/m;Lcom/google/android/a/c/d/n;Z)V
    .locals 3

    .prologue
    const/16 v2, 0x80

    .line 72
    invoke-direct {p0, p1}, Lcom/google/android/a/c/d/e;-><init>(Lcom/google/android/a/c/m;)V

    .line 73
    iput-object p2, p0, Lcom/google/android/a/c/d/g;->c:Lcom/google/android/a/c/d/n;

    .line 74
    const/4 v0, 0x3

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/google/android/a/c/d/g;->d:[Z

    .line 75
    if-eqz p3, :cond_0

    new-instance v0, Lcom/google/android/a/c/d/g$a;

    invoke-direct {v0}, Lcom/google/android/a/c/d/g$a;-><init>()V

    :goto_0
    iput-object v0, p0, Lcom/google/android/a/c/d/g;->e:Lcom/google/android/a/c/d/g$a;

    .line 76
    new-instance v0, Lcom/google/android/a/c/d/k;

    const/4 v1, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/a/c/d/k;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/a/c/d/g;->f:Lcom/google/android/a/c/d/k;

    .line 77
    new-instance v0, Lcom/google/android/a/c/d/k;

    const/16 v1, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/a/c/d/k;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/a/c/d/g;->g:Lcom/google/android/a/c/d/k;

    .line 78
    new-instance v0, Lcom/google/android/a/c/d/k;

    const/4 v1, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/a/c/d/k;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/a/c/d/g;->h:Lcom/google/android/a/c/d/k;

    .line 79
    new-instance v0, Lcom/google/android/a/f/j;

    invoke-direct {v0}, Lcom/google/android/a/f/j;-><init>()V

    iput-object v0, p0, Lcom/google/android/a/c/d/g;->o:Lcom/google/android/a/f/j;

    .line 80
    return-void

    .line 75
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/google/android/a/c/d/k;Lcom/google/android/a/c/d/k;)Lcom/google/android/a/o;
    .locals 11

    .prologue
    const/4 v2, -0x1

    .line 209
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 210
    iget-object v0, p0, Lcom/google/android/a/c/d/k;->a:[B

    iget v1, p0, Lcom/google/android/a/c/d/k;->b:I

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    iget-object v0, p1, Lcom/google/android/a/c/d/k;->a:[B

    iget v1, p1, Lcom/google/android/a/c/d/k;->b:I

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 214
    iget-object v0, p0, Lcom/google/android/a/c/d/k;->a:[B

    iget v1, p0, Lcom/google/android/a/c/d/k;->b:I

    invoke-static {v0, v1}, Lcom/google/android/a/f/h;->a([BI)I

    .line 215
    new-instance v0, Lcom/google/android/a/f/i;

    iget-object v1, p0, Lcom/google/android/a/c/d/k;->a:[B

    invoke-direct {v0, v1}, Lcom/google/android/a/f/i;-><init>([B)V

    .line 216
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Lcom/google/android/a/f/i;->b(I)V

    .line 217
    invoke-static {v0}, Lcom/google/android/a/f/c;->a(Lcom/google/android/a/f/i;)Lcom/google/android/a/f/c$a;

    move-result-object v3

    .line 219
    const/4 v0, 0x0

    const-string v1, "video/avc"

    const-wide/16 v4, -0x1

    iget v6, v3, Lcom/google/android/a/f/c$a;->a:I

    iget v7, v3, Lcom/google/android/a/f/c$a;->b:I

    iget v10, v3, Lcom/google/android/a/f/c$a;->c:F

    move v3, v2

    move v9, v2

    invoke-static/range {v0 .. v10}, Lcom/google/android/a/o;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;IF)Lcom/google/android/a/o;

    move-result-object v0

    return-object v0
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/a/c/d/g;->e:Lcom/google/android/a/c/d/g$a;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/google/android/a/c/d/g;->e:Lcom/google/android/a/c/d/g$a;

    invoke-virtual {v0, p1}, Lcom/google/android/a/c/d/g$a;->a(I)V

    .line 179
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/a/c/d/g;->b:Z

    if-nez v0, :cond_1

    .line 180
    iget-object v0, p0, Lcom/google/android/a/c/d/g;->f:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0, p1}, Lcom/google/android/a/c/d/k;->a(I)V

    .line 181
    iget-object v0, p0, Lcom/google/android/a/c/d/g;->g:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0, p1}, Lcom/google/android/a/c/d/k;->a(I)V

    .line 183
    :cond_1
    iget-object v0, p0, Lcom/google/android/a/c/d/g;->h:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0, p1}, Lcom/google/android/a/c/d/k;->a(I)V

    .line 184
    return-void
.end method

.method private a(JI)V
    .locals 3

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/a/c/d/g;->f:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0, p3}, Lcom/google/android/a/c/d/k;->b(I)Z

    .line 199
    iget-object v0, p0, Lcom/google/android/a/c/d/g;->g:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0, p3}, Lcom/google/android/a/c/d/k;->b(I)Z

    .line 200
    iget-object v0, p0, Lcom/google/android/a/c/d/g;->h:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0, p3}, Lcom/google/android/a/c/d/k;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/google/android/a/c/d/g;->h:Lcom/google/android/a/c/d/k;

    iget-object v0, v0, Lcom/google/android/a/c/d/k;->a:[B

    iget-object v1, p0, Lcom/google/android/a/c/d/g;->h:Lcom/google/android/a/c/d/k;

    iget v1, v1, Lcom/google/android/a/c/d/k;->b:I

    invoke-static {v0, v1}, Lcom/google/android/a/f/h;->a([BI)I

    move-result v0

    .line 202
    iget-object v1, p0, Lcom/google/android/a/c/d/g;->o:Lcom/google/android/a/f/j;

    iget-object v2, p0, Lcom/google/android/a/c/d/g;->h:Lcom/google/android/a/c/d/k;

    iget-object v2, v2, Lcom/google/android/a/c/d/k;->a:[B

    invoke-virtual {v1, v2, v0}, Lcom/google/android/a/f/j;->a([BI)V

    .line 203
    iget-object v0, p0, Lcom/google/android/a/c/d/g;->o:Lcom/google/android/a/f/j;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/a/f/j;->b(I)V

    .line 204
    iget-object v0, p0, Lcom/google/android/a/c/d/g;->c:Lcom/google/android/a/c/d/n;

    iget-object v1, p0, Lcom/google/android/a/c/d/g;->o:Lcom/google/android/a/f/j;

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/a/c/d/n;->a(JLcom/google/android/a/f/j;)V

    .line 206
    :cond_0
    return-void
.end method

.method private a([BII)V
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/a/c/d/g;->e:Lcom/google/android/a/c/d/g$a;

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/google/android/a/c/d/g;->e:Lcom/google/android/a/c/d/g$a;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/a/c/d/g$a;->a([BII)V

    .line 190
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/a/c/d/g;->b:Z

    if-nez v0, :cond_1

    .line 191
    iget-object v0, p0, Lcom/google/android/a/c/d/g;->f:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/a/c/d/k;->a([BII)V

    .line 192
    iget-object v0, p0, Lcom/google/android/a/c/d/g;->g:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/a/c/d/k;->a([BII)V

    .line 194
    :cond_1
    iget-object v0, p0, Lcom/google/android/a/c/d/g;->h:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/a/c/d/k;->a([BII)V

    .line 195
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/a/c/d/g;->d:[Z

    invoke-static {v0}, Lcom/google/android/a/f/h;->a([Z)V

    .line 85
    iget-object v0, p0, Lcom/google/android/a/c/d/g;->f:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0}, Lcom/google/android/a/c/d/k;->a()V

    .line 86
    iget-object v0, p0, Lcom/google/android/a/c/d/g;->g:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0}, Lcom/google/android/a/c/d/k;->a()V

    .line 87
    iget-object v0, p0, Lcom/google/android/a/c/d/g;->h:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0}, Lcom/google/android/a/c/d/k;->a()V

    .line 88
    iget-object v0, p0, Lcom/google/android/a/c/d/g;->e:Lcom/google/android/a/c/d/g$a;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/google/android/a/c/d/g;->e:Lcom/google/android/a/c/d/g$a;

    invoke-virtual {v0}, Lcom/google/android/a/c/d/g$a;->a()V

    .line 91
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/a/c/d/g;->i:Z

    .line 92
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/a/c/d/g;->j:J

    .line 93
    return-void
.end method

.method public a(JZ)V
    .locals 1

    .prologue
    .line 97
    iput-wide p1, p0, Lcom/google/android/a/c/d/g;->k:J

    .line 98
    return-void
.end method

.method public a(Lcom/google/android/a/f/j;)V
    .locals 13

    .prologue
    .line 102
    invoke-virtual {p1}, Lcom/google/android/a/f/j;->b()I

    move-result v0

    if-lez v0, :cond_0

    .line 103
    invoke-virtual {p1}, Lcom/google/android/a/f/j;->d()I

    move-result v0

    .line 104
    invoke-virtual {p1}, Lcom/google/android/a/f/j;->c()I

    move-result v8

    .line 105
    iget-object v9, p1, Lcom/google/android/a/f/j;->a:[B

    .line 108
    iget-wide v2, p0, Lcom/google/android/a/c/d/g;->j:J

    invoke-virtual {p1}, Lcom/google/android/a/f/j;->b()I

    move-result v1

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/a/c/d/g;->j:J

    .line 109
    iget-object v1, p0, Lcom/google/android/a/c/d/g;->a:Lcom/google/android/a/c/m;

    invoke-virtual {p1}, Lcom/google/android/a/f/j;->b()I

    move-result v2

    invoke-interface {v1, p1, v2}, Lcom/google/android/a/c/m;->a(Lcom/google/android/a/f/j;I)V

    .line 113
    :goto_0
    iget-object v1, p0, Lcom/google/android/a/c/d/g;->d:[Z

    invoke-static {v9, v0, v8, v1}, Lcom/google/android/a/f/h;->a([BII[Z)I

    move-result v10

    .line 115
    if-ne v10, v8, :cond_1

    .line 117
    invoke-direct {p0, v9, v0, v8}, Lcom/google/android/a/c/d/g;->a([BII)V

    .line 168
    :cond_0
    return-void

    .line 122
    :cond_1
    invoke-static {v9, v10}, Lcom/google/android/a/f/h;->b([BI)I

    move-result v11

    .line 126
    sub-int v12, v10, v0

    .line 127
    if-lez v12, :cond_2

    .line 128
    invoke-direct {p0, v9, v0, v10}, Lcom/google/android/a/c/d/g;->a([BII)V

    .line 131
    :cond_2
    sparse-switch v11, :sswitch_data_0

    .line 161
    :goto_1
    iget-wide v2, p0, Lcom/google/android/a/c/d/g;->k:J

    if-gez v12, :cond_9

    neg-int v0, v12

    :goto_2
    invoke-direct {p0, v2, v3, v0}, Lcom/google/android/a/c/d/g;->a(JI)V

    .line 163
    invoke-direct {p0, v11}, Lcom/google/android/a/c/d/g;->a(I)V

    .line 165
    add-int/lit8 v0, v10, 0x3

    .line 166
    goto :goto_0

    .line 133
    :sswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/a/c/d/g;->l:Z

    goto :goto_1

    .line 136
    :sswitch_1
    sub-int v6, v8, v10

    .line 137
    iget-boolean v0, p0, Lcom/google/android/a/c/d/g;->i:Z

    if-eqz v0, :cond_6

    .line 138
    iget-object v0, p0, Lcom/google/android/a/c/d/g;->e:Lcom/google/android/a/c/d/g$a;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/a/c/d/g;->e:Lcom/google/android/a/c/d/g$a;

    invoke-virtual {v0}, Lcom/google/android/a/c/d/g$a;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 139
    iget-object v0, p0, Lcom/google/android/a/c/d/g;->e:Lcom/google/android/a/c/d/g$a;

    invoke-virtual {v0}, Lcom/google/android/a/c/d/g$a;->c()I

    move-result v0

    .line 140
    iget-boolean v1, p0, Lcom/google/android/a/c/d/g;->l:Z

    const/4 v2, 0x2

    if-eq v0, v2, :cond_3

    const/4 v2, 0x7

    if-ne v0, v2, :cond_7

    :cond_3
    const/4 v0, 0x1

    :goto_3
    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/google/android/a/c/d/g;->l:Z

    .line 141
    iget-object v0, p0, Lcom/google/android/a/c/d/g;->e:Lcom/google/android/a/c/d/g$a;

    invoke-virtual {v0}, Lcom/google/android/a/c/d/g$a;->a()V

    .line 143
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/a/c/d/g;->l:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/google/android/a/c/d/g;->b:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/a/c/d/g;->f:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0}, Lcom/google/android/a/c/d/k;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/a/c/d/g;->g:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0}, Lcom/google/android/a/c/d/k;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 144
    iget-object v0, p0, Lcom/google/android/a/c/d/g;->a:Lcom/google/android/a/c/m;

    iget-object v1, p0, Lcom/google/android/a/c/d/g;->f:Lcom/google/android/a/c/d/k;

    iget-object v2, p0, Lcom/google/android/a/c/d/g;->g:Lcom/google/android/a/c/d/k;

    invoke-static {v1, v2}, Lcom/google/android/a/c/d/g;->a(Lcom/google/android/a/c/d/k;Lcom/google/android/a/c/d/k;)Lcom/google/android/a/o;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/a/c/m;->a(Lcom/google/android/a/o;)V

    .line 145
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/a/c/d/g;->b:Z

    .line 147
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/a/c/d/g;->l:Z

    if-eqz v0, :cond_8

    const/4 v4, 0x1

    .line 148
    :goto_4
    iget-wide v0, p0, Lcom/google/android/a/c/d/g;->j:J

    iget-wide v2, p0, Lcom/google/android/a/c/d/g;->m:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    sub-int v5, v0, v6

    .line 149
    iget-object v1, p0, Lcom/google/android/a/c/d/g;->a:Lcom/google/android/a/c/m;

    iget-wide v2, p0, Lcom/google/android/a/c/d/g;->n:J

    const/4 v7, 0x0

    invoke-interface/range {v1 .. v7}, Lcom/google/android/a/c/m;->a(JIII[B)V

    .line 151
    :cond_6
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/a/c/d/g;->i:Z

    .line 152
    iget-wide v0, p0, Lcom/google/android/a/c/d/g;->j:J

    int-to-long v2, v6

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/a/c/d/g;->m:J

    .line 153
    iget-wide v0, p0, Lcom/google/android/a/c/d/g;->k:J

    iput-wide v0, p0, Lcom/google/android/a/c/d/g;->n:J

    .line 154
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/a/c/d/g;->l:Z

    goto/16 :goto_1

    .line 140
    :cond_7
    const/4 v0, 0x0

    goto :goto_3

    .line 147
    :cond_8
    const/4 v4, 0x0

    goto :goto_4

    .line 161
    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 131
    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0x9 -> :sswitch_1
    .end sparse-switch
.end method

.method public b()V
    .locals 0

    .prologue
    .line 173
    return-void
.end method
