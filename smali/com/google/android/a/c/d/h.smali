.class final Lcom/google/android/a/c/d/h;
.super Lcom/google/android/a/c/d/e;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/a/c/d/h$a;
    }
.end annotation


# instance fields
.field private b:Z

.field private final c:Lcom/google/android/a/c/d/n;

.field private final d:[Z

.field private final e:Lcom/google/android/a/c/d/k;

.field private final f:Lcom/google/android/a/c/d/k;

.field private final g:Lcom/google/android/a/c/d/k;

.field private final h:Lcom/google/android/a/c/d/k;

.field private final i:Lcom/google/android/a/c/d/k;

.field private final j:Lcom/google/android/a/c/d/h$a;

.field private k:J

.field private l:J

.field private final m:Lcom/google/android/a/f/j;


# direct methods
.method public constructor <init>(Lcom/google/android/a/c/m;Lcom/google/android/a/c/d/n;)V
    .locals 3

    .prologue
    const/16 v2, 0x80

    .line 68
    invoke-direct {p0, p1}, Lcom/google/android/a/c/d/e;-><init>(Lcom/google/android/a/c/m;)V

    .line 69
    iput-object p2, p0, Lcom/google/android/a/c/d/h;->c:Lcom/google/android/a/c/d/n;

    .line 70
    const/4 v0, 0x3

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/google/android/a/c/d/h;->d:[Z

    .line 71
    new-instance v0, Lcom/google/android/a/c/d/k;

    const/16 v1, 0x20

    invoke-direct {v0, v1, v2}, Lcom/google/android/a/c/d/k;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/a/c/d/h;->e:Lcom/google/android/a/c/d/k;

    .line 72
    new-instance v0, Lcom/google/android/a/c/d/k;

    const/16 v1, 0x21

    invoke-direct {v0, v1, v2}, Lcom/google/android/a/c/d/k;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/a/c/d/h;->f:Lcom/google/android/a/c/d/k;

    .line 73
    new-instance v0, Lcom/google/android/a/c/d/k;

    const/16 v1, 0x22

    invoke-direct {v0, v1, v2}, Lcom/google/android/a/c/d/k;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/a/c/d/h;->g:Lcom/google/android/a/c/d/k;

    .line 74
    new-instance v0, Lcom/google/android/a/c/d/k;

    const/16 v1, 0x27

    invoke-direct {v0, v1, v2}, Lcom/google/android/a/c/d/k;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/a/c/d/h;->h:Lcom/google/android/a/c/d/k;

    .line 75
    new-instance v0, Lcom/google/android/a/c/d/k;

    const/16 v1, 0x28

    invoke-direct {v0, v1, v2}, Lcom/google/android/a/c/d/k;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/a/c/d/h;->i:Lcom/google/android/a/c/d/k;

    .line 76
    new-instance v0, Lcom/google/android/a/c/d/h$a;

    invoke-direct {v0, p1}, Lcom/google/android/a/c/d/h$a;-><init>(Lcom/google/android/a/c/m;)V

    iput-object v0, p0, Lcom/google/android/a/c/d/h;->j:Lcom/google/android/a/c/d/h$a;

    .line 77
    new-instance v0, Lcom/google/android/a/f/j;

    invoke-direct {v0}, Lcom/google/android/a/f/j;-><init>()V

    iput-object v0, p0, Lcom/google/android/a/c/d/h;->m:Lcom/google/android/a/f/j;

    .line 78
    return-void
.end method

.method private static a(Lcom/google/android/a/c/d/k;Lcom/google/android/a/c/d/k;Lcom/google/android/a/c/d/k;)Lcom/google/android/a/o;
    .locals 13

    .prologue
    .line 204
    iget v0, p0, Lcom/google/android/a/c/d/k;->b:I

    iget v1, p1, Lcom/google/android/a/c/d/k;->b:I

    add-int/2addr v0, v1

    iget v1, p2, Lcom/google/android/a/c/d/k;->b:I

    add-int/2addr v0, v1

    new-array v8, v0, [B

    .line 205
    iget-object v0, p0, Lcom/google/android/a/c/d/k;->a:[B

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/a/c/d/k;->b:I

    invoke-static {v0, v1, v8, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 206
    iget-object v0, p1, Lcom/google/android/a/c/d/k;->a:[B

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/a/c/d/k;->b:I

    iget v3, p1, Lcom/google/android/a/c/d/k;->b:I

    invoke-static {v0, v1, v8, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 207
    iget-object v0, p2, Lcom/google/android/a/c/d/k;->a:[B

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/a/c/d/k;->b:I

    iget v3, p1, Lcom/google/android/a/c/d/k;->b:I

    add-int/2addr v2, v3

    iget v3, p2, Lcom/google/android/a/c/d/k;->b:I

    invoke-static {v0, v1, v8, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 210
    iget-object v0, p1, Lcom/google/android/a/c/d/k;->a:[B

    iget v1, p1, Lcom/google/android/a/c/d/k;->b:I

    invoke-static {v0, v1}, Lcom/google/android/a/f/h;->a([BI)I

    .line 211
    new-instance v3, Lcom/google/android/a/f/i;

    iget-object v0, p1, Lcom/google/android/a/c/d/k;->a:[B

    invoke-direct {v3, v0}, Lcom/google/android/a/f/i;-><init>([B)V

    .line 212
    const/16 v0, 0x2c

    invoke-virtual {v3, v0}, Lcom/google/android/a/f/i;->b(I)V

    .line 213
    const/4 v0, 0x3

    invoke-virtual {v3, v0}, Lcom/google/android/a/f/i;->c(I)I

    move-result v1

    .line 214
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Lcom/google/android/a/f/i;->b(I)V

    .line 217
    const/16 v0, 0x58

    invoke-virtual {v3, v0}, Lcom/google/android/a/f/i;->b(I)V

    .line 218
    const/16 v0, 0x8

    invoke-virtual {v3, v0}, Lcom/google/android/a/f/i;->b(I)V

    .line 219
    const/4 v2, 0x0

    .line 220
    const/4 v0, 0x0

    move v12, v0

    move v0, v2

    move v2, v12

    :goto_0
    if-ge v2, v1, :cond_2

    .line 221
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/android/a/f/i;->c(I)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 222
    add-int/lit8 v0, v0, 0x59

    .line 224
    :cond_0
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/android/a/f/i;->c(I)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 225
    add-int/lit8 v0, v0, 0x8

    .line 220
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 228
    :cond_2
    invoke-virtual {v3, v0}, Lcom/google/android/a/f/i;->b(I)V

    .line 229
    if-lez v1, :cond_3

    .line 230
    rsub-int/lit8 v0, v1, 0x8

    mul-int/lit8 v0, v0, 0x2

    invoke-virtual {v3, v0}, Lcom/google/android/a/f/i;->b(I)V

    .line 233
    :cond_3
    invoke-virtual {v3}, Lcom/google/android/a/f/i;->d()I

    .line 234
    invoke-virtual {v3}, Lcom/google/android/a/f/i;->d()I

    move-result v4

    .line 235
    const/4 v0, 0x3

    if-ne v4, v0, :cond_4

    .line 236
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Lcom/google/android/a/f/i;->b(I)V

    .line 238
    :cond_4
    invoke-virtual {v3}, Lcom/google/android/a/f/i;->d()I

    move-result v6

    .line 239
    invoke-virtual {v3}, Lcom/google/android/a/f/i;->d()I

    move-result v7

    .line 240
    invoke-virtual {v3}, Lcom/google/android/a/f/i;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 241
    invoke-virtual {v3}, Lcom/google/android/a/f/i;->d()I

    move-result v5

    .line 242
    invoke-virtual {v3}, Lcom/google/android/a/f/i;->d()I

    move-result v9

    .line 243
    invoke-virtual {v3}, Lcom/google/android/a/f/i;->d()I

    move-result v10

    .line 244
    invoke-virtual {v3}, Lcom/google/android/a/f/i;->d()I

    move-result v11

    .line 246
    const/4 v0, 0x1

    if-eq v4, v0, :cond_5

    const/4 v0, 0x2

    if-ne v4, v0, :cond_7

    :cond_5
    const/4 v0, 0x2

    move v2, v0

    .line 247
    :goto_1
    const/4 v0, 0x1

    if-ne v4, v0, :cond_8

    const/4 v0, 0x2

    .line 248
    :goto_2
    add-int v4, v5, v9

    mul-int/2addr v2, v4

    sub-int/2addr v6, v2

    .line 249
    add-int v2, v10, v11

    mul-int/2addr v0, v2

    sub-int/2addr v7, v0

    .line 251
    :cond_6
    invoke-virtual {v3}, Lcom/google/android/a/f/i;->d()I

    .line 252
    invoke-virtual {v3}, Lcom/google/android/a/f/i;->d()I

    .line 253
    invoke-virtual {v3}, Lcom/google/android/a/f/i;->d()I

    move-result v2

    .line 255
    invoke-virtual {v3}, Lcom/google/android/a/f/i;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    const/4 v0, 0x0

    :goto_3
    if-gt v0, v1, :cond_a

    .line 256
    invoke-virtual {v3}, Lcom/google/android/a/f/i;->d()I

    .line 257
    invoke-virtual {v3}, Lcom/google/android/a/f/i;->d()I

    .line 258
    invoke-virtual {v3}, Lcom/google/android/a/f/i;->d()I

    .line 255
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 246
    :cond_7
    const/4 v0, 0x1

    move v2, v0

    goto :goto_1

    .line 247
    :cond_8
    const/4 v0, 0x1

    goto :goto_2

    :cond_9
    move v0, v1

    .line 255
    goto :goto_3

    .line 260
    :cond_a
    invoke-virtual {v3}, Lcom/google/android/a/f/i;->d()I

    .line 261
    invoke-virtual {v3}, Lcom/google/android/a/f/i;->d()I

    .line 262
    invoke-virtual {v3}, Lcom/google/android/a/f/i;->d()I

    .line 263
    invoke-virtual {v3}, Lcom/google/android/a/f/i;->d()I

    .line 264
    invoke-virtual {v3}, Lcom/google/android/a/f/i;->d()I

    .line 265
    invoke-virtual {v3}, Lcom/google/android/a/f/i;->d()I

    .line 267
    invoke-virtual {v3}, Lcom/google/android/a/f/i;->b()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {v3}, Lcom/google/android/a/f/i;->b()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 268
    invoke-static {v3}, Lcom/google/android/a/c/d/h;->a(Lcom/google/android/a/f/i;)V

    .line 270
    :cond_b
    const/4 v0, 0x2

    invoke-virtual {v3, v0}, Lcom/google/android/a/f/i;->b(I)V

    .line 271
    invoke-virtual {v3}, Lcom/google/android/a/f/i;->b()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 273
    const/16 v0, 0x8

    invoke-virtual {v3, v0}, Lcom/google/android/a/f/i;->b(I)V

    .line 274
    invoke-virtual {v3}, Lcom/google/android/a/f/i;->d()I

    .line 275
    invoke-virtual {v3}, Lcom/google/android/a/f/i;->d()I

    .line 276
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Lcom/google/android/a/f/i;->b(I)V

    .line 279
    :cond_c
    invoke-static {v3}, Lcom/google/android/a/c/d/h;->b(Lcom/google/android/a/f/i;)V

    .line 280
    invoke-virtual {v3}, Lcom/google/android/a/f/i;->b()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 282
    const/4 v0, 0x0

    :goto_4
    invoke-virtual {v3}, Lcom/google/android/a/f/i;->d()I

    move-result v1

    if-ge v0, v1, :cond_d

    .line 283
    add-int/lit8 v1, v2, 0x4

    .line 285
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v3, v1}, Lcom/google/android/a/f/i;->b(I)V

    .line 282
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 288
    :cond_d
    const/4 v0, 0x2

    invoke-virtual {v3, v0}, Lcom/google/android/a/f/i;->b(I)V

    .line 289
    const/high16 v0, 0x3f800000    # 1.0f

    .line 290
    invoke-virtual {v3}, Lcom/google/android/a/f/i;->b()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 291
    invoke-virtual {v3}, Lcom/google/android/a/f/i;->b()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 292
    const/16 v1, 0x8

    invoke-virtual {v3, v1}, Lcom/google/android/a/f/i;->c(I)I

    move-result v1

    .line 293
    const/16 v2, 0xff

    if-ne v1, v2, :cond_f

    .line 294
    const/16 v1, 0x10

    invoke-virtual {v3, v1}, Lcom/google/android/a/f/i;->c(I)I

    move-result v1

    .line 295
    const/16 v2, 0x10

    invoke-virtual {v3, v2}, Lcom/google/android/a/f/i;->c(I)I

    move-result v2

    .line 296
    if-eqz v1, :cond_e

    if-eqz v2, :cond_e

    .line 297
    int-to-float v0, v1

    int-to-float v1, v2

    div-float/2addr v0, v1

    :cond_e
    move v10, v0

    .line 307
    :goto_5
    const/4 v0, 0x0

    const-string v1, "video/hevc"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-wide/16 v4, -0x1

    invoke-static {v8}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    const/4 v9, -0x1

    invoke-static/range {v0 .. v10}, Lcom/google/android/a/o;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;IF)Lcom/google/android/a/o;

    move-result-object v0

    return-object v0

    .line 299
    :cond_f
    sget-object v2, Lcom/google/android/a/f/h;->b:[F

    array-length v2, v2

    if-ge v1, v2, :cond_10

    .line 300
    sget-object v0, Lcom/google/android/a/f/h;->b:[F

    aget v0, v0, v1

    move v10, v0

    goto :goto_5

    .line 302
    :cond_10
    const-string v2, "H265Reader"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unexpected aspect_ratio_idc value: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_11
    move v10, v0

    goto :goto_5
.end method

.method private a(JIIJ)V
    .locals 9

    .prologue
    .line 149
    iget-boolean v0, p0, Lcom/google/android/a/c/d/h;->b:Z

    if-nez v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->e:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0, p4}, Lcom/google/android/a/c/d/k;->a(I)V

    .line 151
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->f:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0, p4}, Lcom/google/android/a/c/d/k;->a(I)V

    .line 152
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->g:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0, p4}, Lcom/google/android/a/c/d/k;->a(I)V

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->h:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0, p4}, Lcom/google/android/a/c/d/k;->a(I)V

    .line 155
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->i:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0, p4}, Lcom/google/android/a/c/d/k;->a(I)V

    .line 156
    iget-object v1, p0, Lcom/google/android/a/c/d/h;->j:Lcom/google/android/a/c/d/h$a;

    move-wide v2, p1

    move v4, p3

    move v5, p4

    move-wide v6, p5

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/a/c/d/h$a;->a(JIIJ)V

    .line 157
    return-void
.end method

.method private static a(Lcom/google/android/a/f/i;)V
    .locals 7

    .prologue
    const/4 v1, 0x3

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 314
    move v5, v3

    :goto_0
    const/4 v0, 0x4

    if-ge v5, v0, :cond_5

    move v4, v3

    .line 315
    :goto_1
    const/4 v0, 0x6

    if-ge v4, v0, :cond_4

    .line 316
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 318
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->d()I

    .line 315
    :cond_0
    if-ne v5, v1, :cond_3

    move v0, v1

    :goto_2
    add-int/2addr v0, v4

    move v4, v0

    goto :goto_1

    .line 320
    :cond_1
    const/16 v0, 0x40

    shl-int/lit8 v6, v5, 0x1

    add-int/lit8 v6, v6, 0x4

    shl-int v6, v2, v6

    invoke-static {v0, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 321
    if-le v5, v2, :cond_2

    .line 323
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->e()I

    :cond_2
    move v0, v3

    .line 325
    :goto_3
    if-ge v0, v6, :cond_0

    .line 326
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->e()I

    .line 325
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    move v0, v2

    .line 315
    goto :goto_2

    .line 314
    :cond_4
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_0

    .line 331
    :cond_5
    return-void
.end method

.method private a([BII)V
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/google/android/a/c/d/h;->b:Z

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->j:Lcom/google/android/a/c/d/h$a;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/a/c/d/h$a;->a([BII)V

    .line 167
    :goto_0
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->h:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/a/c/d/k;->a([BII)V

    .line 168
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->i:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/a/c/d/k;->a([BII)V

    .line 169
    return-void

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->e:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/a/c/d/k;->a([BII)V

    .line 164
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->f:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/a/c/d/k;->a([BII)V

    .line 165
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->g:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/a/c/d/k;->a([BII)V

    goto :goto_0
.end method

.method private b(JIIJ)V
    .locals 5

    .prologue
    const/4 v4, 0x5

    .line 172
    iget-boolean v0, p0, Lcom/google/android/a/c/d/h;->b:Z

    if-eqz v0, :cond_3

    .line 173
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->j:Lcom/google/android/a/c/d/h$a;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/a/c/d/h$a;->a(JI)V

    .line 183
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->h:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0, p4}, Lcom/google/android/a/c/d/k;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 184
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->h:Lcom/google/android/a/c/d/k;

    iget-object v0, v0, Lcom/google/android/a/c/d/k;->a:[B

    iget-object v1, p0, Lcom/google/android/a/c/d/h;->h:Lcom/google/android/a/c/d/k;

    iget v1, v1, Lcom/google/android/a/c/d/k;->b:I

    invoke-static {v0, v1}, Lcom/google/android/a/f/h;->a([BI)I

    move-result v0

    .line 185
    iget-object v1, p0, Lcom/google/android/a/c/d/h;->m:Lcom/google/android/a/f/j;

    iget-object v2, p0, Lcom/google/android/a/c/d/h;->h:Lcom/google/android/a/c/d/k;

    iget-object v2, v2, Lcom/google/android/a/c/d/k;->a:[B

    invoke-virtual {v1, v2, v0}, Lcom/google/android/a/f/j;->a([BI)V

    .line 188
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->m:Lcom/google/android/a/f/j;

    invoke-virtual {v0, v4}, Lcom/google/android/a/f/j;->c(I)V

    .line 189
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->c:Lcom/google/android/a/c/d/n;

    iget-object v1, p0, Lcom/google/android/a/c/d/h;->m:Lcom/google/android/a/f/j;

    invoke-virtual {v0, p5, p6, v1}, Lcom/google/android/a/c/d/n;->a(JLcom/google/android/a/f/j;)V

    .line 191
    :cond_1
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->i:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0, p4}, Lcom/google/android/a/c/d/k;->b(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 192
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->i:Lcom/google/android/a/c/d/k;

    iget-object v0, v0, Lcom/google/android/a/c/d/k;->a:[B

    iget-object v1, p0, Lcom/google/android/a/c/d/h;->i:Lcom/google/android/a/c/d/k;

    iget v1, v1, Lcom/google/android/a/c/d/k;->b:I

    invoke-static {v0, v1}, Lcom/google/android/a/f/h;->a([BI)I

    move-result v0

    .line 193
    iget-object v1, p0, Lcom/google/android/a/c/d/h;->m:Lcom/google/android/a/f/j;

    iget-object v2, p0, Lcom/google/android/a/c/d/h;->i:Lcom/google/android/a/c/d/k;

    iget-object v2, v2, Lcom/google/android/a/c/d/k;->a:[B

    invoke-virtual {v1, v2, v0}, Lcom/google/android/a/f/j;->a([BI)V

    .line 196
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->m:Lcom/google/android/a/f/j;

    invoke-virtual {v0, v4}, Lcom/google/android/a/f/j;->c(I)V

    .line 197
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->c:Lcom/google/android/a/c/d/n;

    iget-object v1, p0, Lcom/google/android/a/c/d/h;->m:Lcom/google/android/a/f/j;

    invoke-virtual {v0, p5, p6, v1}, Lcom/google/android/a/c/d/n;->a(JLcom/google/android/a/f/j;)V

    .line 199
    :cond_2
    return-void

    .line 175
    :cond_3
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->e:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0, p4}, Lcom/google/android/a/c/d/k;->b(I)Z

    .line 176
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->f:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0, p4}, Lcom/google/android/a/c/d/k;->b(I)Z

    .line 177
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->g:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0, p4}, Lcom/google/android/a/c/d/k;->b(I)Z

    .line 178
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->e:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0}, Lcom/google/android/a/c/d/k;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/a/c/d/h;->f:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0}, Lcom/google/android/a/c/d/k;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/a/c/d/h;->g:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0}, Lcom/google/android/a/c/d/k;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->a:Lcom/google/android/a/c/m;

    iget-object v1, p0, Lcom/google/android/a/c/d/h;->e:Lcom/google/android/a/c/d/k;

    iget-object v2, p0, Lcom/google/android/a/c/d/h;->f:Lcom/google/android/a/c/d/k;

    iget-object v3, p0, Lcom/google/android/a/c/d/h;->g:Lcom/google/android/a/c/d/k;

    invoke-static {v1, v2, v3}, Lcom/google/android/a/c/d/h;->a(Lcom/google/android/a/c/d/k;Lcom/google/android/a/c/d/k;Lcom/google/android/a/c/d/k;)Lcom/google/android/a/o;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/a/c/m;->a(Lcom/google/android/a/o;)V

    .line 180
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/a/c/d/h;->b:Z

    goto/16 :goto_0
.end method

.method private static b(Lcom/google/android/a/f/i;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 338
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->d()I

    move-result v5

    move v4, v2

    move v0, v2

    move v1, v2

    .line 346
    :goto_0
    if-ge v4, v5, :cond_4

    .line 347
    if-eqz v4, :cond_5

    .line 348
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->b()Z

    move-result v1

    move v3, v1

    .line 350
    :goto_1
    if-eqz v3, :cond_1

    .line 351
    invoke-virtual {p0, v8}, Lcom/google/android/a/f/i;->b(I)V

    .line 352
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->d()I

    move v1, v2

    .line 353
    :goto_2
    if-gt v1, v0, :cond_3

    .line 354
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->b()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 355
    invoke-virtual {p0, v8}, Lcom/google/android/a/f/i;->b(I)V

    .line 353
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 359
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->d()I

    move-result v6

    .line 360
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->d()I

    move-result v7

    .line 361
    add-int v0, v6, v7

    move v1, v2

    .line 362
    :goto_3
    if-ge v1, v6, :cond_2

    .line 363
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->d()I

    .line 364
    invoke-virtual {p0, v8}, Lcom/google/android/a/f/i;->b(I)V

    .line 362
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_2
    move v1, v2

    .line 366
    :goto_4
    if-ge v1, v7, :cond_3

    .line 367
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->d()I

    .line 368
    invoke-virtual {p0, v8}, Lcom/google/android/a/f/i;->b(I)V

    .line 366
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 346
    :cond_3
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v1, v3

    goto :goto_0

    .line 372
    :cond_4
    return-void

    :cond_5
    move v3, v1

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->d:[Z

    invoke-static {v0}, Lcom/google/android/a/f/h;->a([Z)V

    .line 83
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->e:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0}, Lcom/google/android/a/c/d/k;->a()V

    .line 84
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->f:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0}, Lcom/google/android/a/c/d/k;->a()V

    .line 85
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->g:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0}, Lcom/google/android/a/c/d/k;->a()V

    .line 86
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->h:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0}, Lcom/google/android/a/c/d/k;->a()V

    .line 87
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->i:Lcom/google/android/a/c/d/k;

    invoke-virtual {v0}, Lcom/google/android/a/c/d/k;->a()V

    .line 88
    iget-object v0, p0, Lcom/google/android/a/c/d/h;->j:Lcom/google/android/a/c/d/h$a;

    invoke-virtual {v0}, Lcom/google/android/a/c/d/h$a;->a()V

    .line 89
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/a/c/d/h;->k:J

    .line 90
    return-void
.end method

.method public a(JZ)V
    .locals 1

    .prologue
    .line 94
    iput-wide p1, p0, Lcom/google/android/a/c/d/h;->l:J

    .line 95
    return-void
.end method

.method public a(Lcom/google/android/a/f/j;)V
    .locals 12

    .prologue
    .line 99
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/a/f/j;->b()I

    move-result v0

    if-lez v0, :cond_1

    .line 100
    invoke-virtual {p1}, Lcom/google/android/a/f/j;->d()I

    move-result v0

    .line 101
    invoke-virtual {p1}, Lcom/google/android/a/f/j;->c()I

    move-result v8

    .line 102
    iget-object v9, p1, Lcom/google/android/a/f/j;->a:[B

    .line 105
    iget-wide v2, p0, Lcom/google/android/a/c/d/h;->k:J

    invoke-virtual {p1}, Lcom/google/android/a/f/j;->b()I

    move-result v1

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/a/c/d/h;->k:J

    .line 106
    iget-object v1, p0, Lcom/google/android/a/c/d/h;->a:Lcom/google/android/a/c/m;

    invoke-virtual {p1}, Lcom/google/android/a/f/j;->b()I

    move-result v2

    invoke-interface {v1, p1, v2}, Lcom/google/android/a/c/m;->a(Lcom/google/android/a/f/j;I)V

    .line 109
    :goto_0
    if-ge v0, v8, :cond_0

    .line 110
    iget-object v1, p0, Lcom/google/android/a/c/d/h;->d:[Z

    invoke-static {v9, v0, v8, v1}, Lcom/google/android/a/f/h;->a([BII[Z)I

    move-result v10

    .line 112
    if-ne v10, v8, :cond_2

    .line 114
    invoke-direct {p0, v9, v0, v8}, Lcom/google/android/a/c/d/h;->a([BII)V

    .line 141
    :cond_1
    return-void

    .line 119
    :cond_2
    invoke-static {v9, v10}, Lcom/google/android/a/f/h;->c([BI)I

    move-result v11

    .line 123
    sub-int v1, v10, v0

    .line 124
    if-lez v1, :cond_3

    .line 125
    invoke-direct {p0, v9, v0, v10}, Lcom/google/android/a/c/d/h;->a([BII)V

    .line 128
    :cond_3
    sub-int v4, v8, v10

    .line 129
    iget-wide v2, p0, Lcom/google/android/a/c/d/h;->k:J

    int-to-long v6, v4

    sub-long/2addr v2, v6

    .line 133
    if-gez v1, :cond_4

    neg-int v5, v1

    :goto_1
    iget-wide v6, p0, Lcom/google/android/a/c/d/h;->l:J

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/google/android/a/c/d/h;->b(JIIJ)V

    .line 136
    iget-wide v6, p0, Lcom/google/android/a/c/d/h;->l:J

    move-object v1, p0

    move v5, v11

    invoke-direct/range {v1 .. v7}, Lcom/google/android/a/c/d/h;->a(JIIJ)V

    .line 138
    add-int/lit8 v0, v10, 0x3

    .line 139
    goto :goto_0

    .line 133
    :cond_4
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public b()V
    .locals 0

    .prologue
    .line 146
    return-void
.end method
