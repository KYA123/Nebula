.class final Lcom/google/android/a/c/d/o$b;
.super Lcom/google/android/a/c/d/o$d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/a/c/d/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "b"
.end annotation


# instance fields
.field private final a:Lcom/google/android/a/c/d/e;

.field private final b:Lcom/google/android/a/c/d/m;

.field private final c:Lcom/google/android/a/f/i;

.field private d:I

.field private e:I

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:I

.field private j:I

.field private k:Z

.field private l:J


# direct methods
.method public constructor <init>(Lcom/google/android/a/c/d/e;Lcom/google/android/a/c/d/m;)V
    .locals 2

    .prologue
    .line 459
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/a/c/d/o$d;-><init>(Lcom/google/android/a/c/d/o$1;)V

    .line 460
    iput-object p1, p0, Lcom/google/android/a/c/d/o$b;->a:Lcom/google/android/a/c/d/e;

    .line 461
    iput-object p2, p0, Lcom/google/android/a/c/d/o$b;->b:Lcom/google/android/a/c/d/m;

    .line 462
    new-instance v0, Lcom/google/android/a/f/i;

    const/16 v1, 0xa

    new-array v1, v1, [B

    invoke-direct {v0, v1}, Lcom/google/android/a/f/i;-><init>([B)V

    iput-object v0, p0, Lcom/google/android/a/c/d/o$b;->c:Lcom/google/android/a/f/i;

    .line 463
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/a/c/d/o$b;->d:I

    .line 464
    return-void
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 542
    iput p1, p0, Lcom/google/android/a/c/d/o$b;->d:I

    .line 543
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/a/c/d/o$b;->e:I

    .line 544
    return-void
.end method

.method private a(Lcom/google/android/a/f/j;[BI)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 556
    invoke-virtual {p1}, Lcom/google/android/a/f/j;->b()I

    move-result v1

    iget v2, p0, Lcom/google/android/a/c/d/o$b;->e:I

    sub-int v2, p3, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 557
    if-gtz v1, :cond_1

    .line 565
    :cond_0
    :goto_0
    return v0

    .line 559
    :cond_1
    if-nez p2, :cond_2

    .line 560
    invoke-virtual {p1, v1}, Lcom/google/android/a/f/j;->c(I)V

    .line 564
    :goto_1
    iget v2, p0, Lcom/google/android/a/c/d/o$b;->e:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/a/c/d/o$b;->e:I

    .line 565
    iget v1, p0, Lcom/google/android/a/c/d/o$b;->e:I

    if-eq v1, p3, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 562
    :cond_2
    iget v2, p0, Lcom/google/android/a/c/d/o$b;->e:I

    invoke-virtual {p1, p2, v2, v1}, Lcom/google/android/a/f/j;->a([BII)V

    goto :goto_1
.end method

.method private b()Z
    .locals 6

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v5, -0x1

    .line 571
    iget-object v2, p0, Lcom/google/android/a/c/d/o$b;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v2, v0}, Lcom/google/android/a/f/i;->a(I)V

    .line 572
    iget-object v2, p0, Lcom/google/android/a/c/d/o$b;->c:Lcom/google/android/a/f/i;

    const/16 v3, 0x18

    invoke-virtual {v2, v3}, Lcom/google/android/a/f/i;->c(I)I

    move-result v2

    .line 573
    if-eq v2, v1, :cond_0

    .line 574
    const-string v1, "TsExtractor"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unexpected start code prefix: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 575
    iput v5, p0, Lcom/google/android/a/c/d/o$b;->j:I

    .line 597
    :goto_0
    return v0

    .line 579
    :cond_0
    iget-object v0, p0, Lcom/google/android/a/c/d/o$b;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v0, v4}, Lcom/google/android/a/f/i;->b(I)V

    .line 580
    iget-object v0, p0, Lcom/google/android/a/c/d/o$b;->c:Lcom/google/android/a/f/i;

    const/16 v2, 0x10

    invoke-virtual {v0, v2}, Lcom/google/android/a/f/i;->c(I)I

    move-result v0

    .line 581
    iget-object v2, p0, Lcom/google/android/a/c/d/o$b;->c:Lcom/google/android/a/f/i;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/google/android/a/f/i;->b(I)V

    .line 582
    iget-object v2, p0, Lcom/google/android/a/c/d/o$b;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v2}, Lcom/google/android/a/f/i;->b()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/a/c/d/o$b;->k:Z

    .line 583
    iget-object v2, p0, Lcom/google/android/a/c/d/o$b;->c:Lcom/google/android/a/f/i;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/google/android/a/f/i;->b(I)V

    .line 584
    iget-object v2, p0, Lcom/google/android/a/c/d/o$b;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v2}, Lcom/google/android/a/f/i;->b()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/a/c/d/o$b;->f:Z

    .line 585
    iget-object v2, p0, Lcom/google/android/a/c/d/o$b;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v2}, Lcom/google/android/a/f/i;->b()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/a/c/d/o$b;->g:Z

    .line 588
    iget-object v2, p0, Lcom/google/android/a/c/d/o$b;->c:Lcom/google/android/a/f/i;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Lcom/google/android/a/f/i;->b(I)V

    .line 589
    iget-object v2, p0, Lcom/google/android/a/c/d/o$b;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v2, v4}, Lcom/google/android/a/f/i;->c(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/a/c/d/o$b;->i:I

    .line 591
    if-nez v0, :cond_1

    .line 592
    iput v5, p0, Lcom/google/android/a/c/d/o$b;->j:I

    :goto_1
    move v0, v1

    .line 597
    goto :goto_0

    .line 594
    :cond_1
    add-int/lit8 v0, v0, 0x6

    add-int/lit8 v0, v0, -0x9

    iget v2, p0, Lcom/google/android/a/c/d/o$b;->i:I

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/google/android/a/c/d/o$b;->j:I

    goto :goto_1
.end method

.method private c()V
    .locals 9

    .prologue
    const/16 v8, 0x1e

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/16 v7, 0xf

    const/4 v6, 0x1

    .line 601
    iget-object v0, p0, Lcom/google/android/a/c/d/o$b;->c:Lcom/google/android/a/f/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/a/f/i;->a(I)V

    .line 602
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/a/c/d/o$b;->l:J

    .line 603
    iget-boolean v0, p0, Lcom/google/android/a/c/d/o$b;->f:Z

    if-eqz v0, :cond_1

    .line 604
    iget-object v0, p0, Lcom/google/android/a/c/d/o$b;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v0, v5}, Lcom/google/android/a/f/i;->b(I)V

    .line 605
    iget-object v0, p0, Lcom/google/android/a/c/d/o$b;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v0, v4}, Lcom/google/android/a/f/i;->c(I)I

    move-result v0

    int-to-long v0, v0

    shl-long/2addr v0, v8

    .line 606
    iget-object v2, p0, Lcom/google/android/a/c/d/o$b;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v2, v6}, Lcom/google/android/a/f/i;->b(I)V

    .line 607
    iget-object v2, p0, Lcom/google/android/a/c/d/o$b;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v2, v7}, Lcom/google/android/a/f/i;->c(I)I

    move-result v2

    shl-int/lit8 v2, v2, 0xf

    int-to-long v2, v2

    or-long/2addr v0, v2

    .line 608
    iget-object v2, p0, Lcom/google/android/a/c/d/o$b;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v2, v6}, Lcom/google/android/a/f/i;->b(I)V

    .line 609
    iget-object v2, p0, Lcom/google/android/a/c/d/o$b;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v2, v7}, Lcom/google/android/a/f/i;->c(I)I

    move-result v2

    int-to-long v2, v2

    or-long/2addr v0, v2

    .line 610
    iget-object v2, p0, Lcom/google/android/a/c/d/o$b;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v2, v6}, Lcom/google/android/a/f/i;->b(I)V

    .line 611
    iget-boolean v2, p0, Lcom/google/android/a/c/d/o$b;->h:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/a/c/d/o$b;->g:Z

    if-eqz v2, :cond_0

    .line 612
    iget-object v2, p0, Lcom/google/android/a/c/d/o$b;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v2, v5}, Lcom/google/android/a/f/i;->b(I)V

    .line 613
    iget-object v2, p0, Lcom/google/android/a/c/d/o$b;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v2, v4}, Lcom/google/android/a/f/i;->c(I)I

    move-result v2

    int-to-long v2, v2

    shl-long/2addr v2, v8

    .line 614
    iget-object v4, p0, Lcom/google/android/a/c/d/o$b;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v4, v6}, Lcom/google/android/a/f/i;->b(I)V

    .line 615
    iget-object v4, p0, Lcom/google/android/a/c/d/o$b;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v4, v7}, Lcom/google/android/a/f/i;->c(I)I

    move-result v4

    shl-int/lit8 v4, v4, 0xf

    int-to-long v4, v4

    or-long/2addr v2, v4

    .line 616
    iget-object v4, p0, Lcom/google/android/a/c/d/o$b;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v4, v6}, Lcom/google/android/a/f/i;->b(I)V

    .line 617
    iget-object v4, p0, Lcom/google/android/a/c/d/o$b;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v4, v7}, Lcom/google/android/a/f/i;->c(I)I

    move-result v4

    int-to-long v4, v4

    or-long/2addr v2, v4

    .line 618
    iget-object v4, p0, Lcom/google/android/a/c/d/o$b;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v4, v6}, Lcom/google/android/a/f/i;->b(I)V

    .line 624
    iget-object v4, p0, Lcom/google/android/a/c/d/o$b;->b:Lcom/google/android/a/c/d/m;

    invoke-virtual {v4, v2, v3}, Lcom/google/android/a/c/d/m;->a(J)J

    .line 625
    iput-boolean v6, p0, Lcom/google/android/a/c/d/o$b;->h:Z

    .line 627
    :cond_0
    iget-object v2, p0, Lcom/google/android/a/c/d/o$b;->b:Lcom/google/android/a/c/d/m;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/a/c/d/m;->a(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/a/c/d/o$b;->l:J

    .line 629
    :cond_1
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 468
    iput v0, p0, Lcom/google/android/a/c/d/o$b;->d:I

    .line 469
    iput v0, p0, Lcom/google/android/a/c/d/o$b;->e:I

    .line 470
    iput-boolean v0, p0, Lcom/google/android/a/c/d/o$b;->h:Z

    .line 471
    iget-object v0, p0, Lcom/google/android/a/c/d/o$b;->a:Lcom/google/android/a/c/d/e;

    invoke-virtual {v0}, Lcom/google/android/a/c/d/e;->a()V

    .line 472
    return-void
.end method

.method public a(Lcom/google/android/a/f/j;ZLcom/google/android/a/c/g;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    const/4 v5, -0x1

    .line 477
    if-eqz p2, :cond_0

    .line 478
    iget v0, p0, Lcom/google/android/a/c/d/o$b;->d:I

    packed-switch v0, :pswitch_data_0

    .line 498
    :goto_0
    :pswitch_0
    invoke-direct {p0, v6}, Lcom/google/android/a/c/d/o$b;->a(I)V

    .line 501
    :cond_0
    :goto_1
    invoke-virtual {p1}, Lcom/google/android/a/f/j;->b()I

    move-result v0

    if-lez v0, :cond_5

    .line 502
    iget v0, p0, Lcom/google/android/a/c/d/o$b;->d:I

    packed-switch v0, :pswitch_data_1

    goto :goto_1

    .line 504
    :pswitch_1
    invoke-virtual {p1}, Lcom/google/android/a/f/j;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/a/f/j;->c(I)V

    goto :goto_1

    .line 484
    :pswitch_2
    const-string v0, "TsExtractor"

    const-string v2, "Unexpected start indicator reading extended header"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 491
    :pswitch_3
    iget v0, p0, Lcom/google/android/a/c/d/o$b;->j:I

    if-eq v0, v5, :cond_1

    .line 492
    const-string v0, "TsExtractor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected start indicator: expected "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/a/c/d/o$b;->j:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " more bytes"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 495
    :cond_1
    iget-object v0, p0, Lcom/google/android/a/c/d/o$b;->a:Lcom/google/android/a/c/d/e;

    invoke-virtual {v0}, Lcom/google/android/a/c/d/e;->b()V

    goto :goto_0

    .line 507
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/a/c/d/o$b;->c:Lcom/google/android/a/f/i;

    iget-object v0, v0, Lcom/google/android/a/f/i;->a:[B

    const/16 v2, 0x9

    invoke-direct {p0, p1, v0, v2}, Lcom/google/android/a/c/d/o$b;->a(Lcom/google/android/a/f/j;[BI)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 508
    invoke-direct {p0}, Lcom/google/android/a/c/d/o$b;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    :goto_2
    invoke-direct {p0, v0}, Lcom/google/android/a/c/d/o$b;->a(I)V

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2

    .line 512
    :pswitch_5
    const/16 v0, 0xa

    iget v2, p0, Lcom/google/android/a/c/d/o$b;->i:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 514
    iget-object v2, p0, Lcom/google/android/a/c/d/o$b;->c:Lcom/google/android/a/f/i;

    iget-object v2, v2, Lcom/google/android/a/f/i;->a:[B

    invoke-direct {p0, p1, v2, v0}, Lcom/google/android/a/c/d/o$b;->a(Lcom/google/android/a/f/j;[BI)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iget v2, p0, Lcom/google/android/a/c/d/o$b;->i:I

    invoke-direct {p0, p1, v0, v2}, Lcom/google/android/a/c/d/o$b;->a(Lcom/google/android/a/f/j;[BI)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 516
    invoke-direct {p0}, Lcom/google/android/a/c/d/o$b;->c()V

    .line 517
    iget-object v0, p0, Lcom/google/android/a/c/d/o$b;->a:Lcom/google/android/a/c/d/e;

    iget-wide v2, p0, Lcom/google/android/a/c/d/o$b;->l:J

    iget-boolean v4, p0, Lcom/google/android/a/c/d/o$b;->k:Z

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/a/c/d/e;->a(JZ)V

    .line 518
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/a/c/d/o$b;->a(I)V

    goto/16 :goto_1

    .line 522
    :pswitch_6
    invoke-virtual {p1}, Lcom/google/android/a/f/j;->b()I

    move-result v0

    .line 523
    iget v2, p0, Lcom/google/android/a/c/d/o$b;->j:I

    if-ne v2, v5, :cond_4

    move v2, v1

    .line 524
    :goto_3
    if-lez v2, :cond_3

    .line 525
    sub-int/2addr v0, v2

    .line 526
    invoke-virtual {p1}, Lcom/google/android/a/f/j;->d()I

    move-result v2

    add-int/2addr v2, v0

    invoke-virtual {p1, v2}, Lcom/google/android/a/f/j;->a(I)V

    .line 528
    :cond_3
    iget-object v2, p0, Lcom/google/android/a/c/d/o$b;->a:Lcom/google/android/a/c/d/e;

    invoke-virtual {v2, p1}, Lcom/google/android/a/c/d/e;->a(Lcom/google/android/a/f/j;)V

    .line 529
    iget v2, p0, Lcom/google/android/a/c/d/o$b;->j:I

    if-eq v2, v5, :cond_0

    .line 530
    iget v2, p0, Lcom/google/android/a/c/d/o$b;->j:I

    sub-int v0, v2, v0

    iput v0, p0, Lcom/google/android/a/c/d/o$b;->j:I

    .line 531
    iget v0, p0, Lcom/google/android/a/c/d/o$b;->j:I

    if-nez v0, :cond_0

    .line 532
    iget-object v0, p0, Lcom/google/android/a/c/d/o$b;->a:Lcom/google/android/a/c/d/e;

    invoke-virtual {v0}, Lcom/google/android/a/c/d/e;->b()V

    .line 533
    invoke-direct {p0, v6}, Lcom/google/android/a/c/d/o$b;->a(I)V

    goto/16 :goto_1

    .line 523
    :cond_4
    iget v2, p0, Lcom/google/android/a/c/d/o$b;->j:I

    sub-int v2, v0, v2

    goto :goto_3

    .line 539
    :cond_5
    return-void

    .line 478
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 502
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
