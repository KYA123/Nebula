.class public final Lcom/google/android/a/c/e/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/a/c/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/a/c/e/f$1;,
        Lcom/google/android/a/c/e/f$b;,
        Lcom/google/android/a/c/e/f$a;
    }
.end annotation


# static fields
.field private static final a:[B

.field private static final b:[B


# instance fields
.field private A:Lcom/google/android/a/f/e;

.field private B:Lcom/google/android/a/f/e;

.field private C:Z

.field private D:I

.field private E:J

.field private F:J

.field private G:I

.field private H:I

.field private I:[I

.field private J:I

.field private K:I

.field private L:I

.field private M:I

.field private N:Z

.field private O:I

.field private P:I

.field private Q:Z

.field private R:Z

.field private S:Lcom/google/android/a/c/g;

.field private final c:Lcom/google/android/a/c/e/b;

.field private final d:Lcom/google/android/a/c/e/e;

.field private final e:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/a/c/e/f$b;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/google/android/a/f/j;

.field private final g:Lcom/google/android/a/f/j;

.field private final h:Lcom/google/android/a/f/j;

.field private final i:Lcom/google/android/a/f/j;

.field private final j:Lcom/google/android/a/f/j;

.field private final k:Lcom/google/android/a/f/j;

.field private final l:Lcom/google/android/a/f/j;

.field private m:J

.field private n:J

.field private o:J

.field private p:J

.field private q:J

.field private r:Lcom/google/android/a/c/e/f$b;

.field private s:Z

.field private t:Z

.field private u:I

.field private v:J

.field private w:Z

.field private x:J

.field private y:J

.field private z:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 158
    const/16 v0, 0x20

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/a/c/e/f;->a:[B

    .line 166
    const/16 v0, 0xc

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/a/c/e/f;->b:[B

    return-void

    .line 158
    nop

    :array_0
    .array-data 1
        0x31t
        0xat
        0x30t
        0x30t
        0x3at
        0x30t
        0x30t
        0x3at
        0x30t
        0x30t
        0x2ct
        0x30t
        0x30t
        0x30t
        0x20t
        0x2dt
        0x2dt
        0x3et
        0x20t
        0x30t
        0x30t
        0x3at
        0x30t
        0x30t
        0x3at
        0x30t
        0x30t
        0x2ct
        0x30t
        0x30t
        0x30t
        0xat
    .end array-data

    .line 166
    :array_1
    .array-data 1
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 239
    new-instance v0, Lcom/google/android/a/c/e/a;

    invoke-direct {v0}, Lcom/google/android/a/c/e/a;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/a/c/e/f;-><init>(Lcom/google/android/a/c/e/b;)V

    .line 240
    return-void
.end method

.method constructor <init>(Lcom/google/android/a/c/e/b;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const-wide/16 v0, -0x1

    .line 242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 190
    iput-wide v0, p0, Lcom/google/android/a/c/e/f;->m:J

    .line 191
    iput-wide v0, p0, Lcom/google/android/a/c/e/f;->n:J

    .line 192
    iput-wide v0, p0, Lcom/google/android/a/c/e/f;->o:J

    .line 193
    iput-wide v0, p0, Lcom/google/android/a/c/e/f;->p:J

    .line 194
    iput-wide v0, p0, Lcom/google/android/a/c/e/f;->q:J

    .line 209
    iput-wide v0, p0, Lcom/google/android/a/c/e/f;->x:J

    .line 210
    iput-wide v0, p0, Lcom/google/android/a/c/e/f;->y:J

    .line 211
    iput-wide v0, p0, Lcom/google/android/a/c/e/f;->z:J

    .line 243
    iput-object p1, p0, Lcom/google/android/a/c/e/f;->c:Lcom/google/android/a/c/e/b;

    .line 244
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->c:Lcom/google/android/a/c/e/b;

    new-instance v1, Lcom/google/android/a/c/e/f$a;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/a/c/e/f$a;-><init>(Lcom/google/android/a/c/e/f;Lcom/google/android/a/c/e/f$1;)V

    invoke-interface {v0, v1}, Lcom/google/android/a/c/e/b;->a(Lcom/google/android/a/c/e/c;)V

    .line 245
    new-instance v0, Lcom/google/android/a/c/e/e;

    invoke-direct {v0}, Lcom/google/android/a/c/e/e;-><init>()V

    iput-object v0, p0, Lcom/google/android/a/c/e/f;->d:Lcom/google/android/a/c/e/e;

    .line 246
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/a/c/e/f;->e:Landroid/util/SparseArray;

    .line 247
    new-instance v0, Lcom/google/android/a/f/j;

    invoke-direct {v0, v3}, Lcom/google/android/a/f/j;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/a/c/e/f;->h:Lcom/google/android/a/f/j;

    .line 248
    new-instance v0, Lcom/google/android/a/f/j;

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/a/f/j;-><init>([B)V

    iput-object v0, p0, Lcom/google/android/a/c/e/f;->i:Lcom/google/android/a/f/j;

    .line 249
    new-instance v0, Lcom/google/android/a/f/j;

    invoke-direct {v0, v3}, Lcom/google/android/a/f/j;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/a/c/e/f;->j:Lcom/google/android/a/f/j;

    .line 250
    new-instance v0, Lcom/google/android/a/f/j;

    sget-object v1, Lcom/google/android/a/f/h;->a:[B

    invoke-direct {v0, v1}, Lcom/google/android/a/f/j;-><init>([B)V

    iput-object v0, p0, Lcom/google/android/a/c/e/f;->f:Lcom/google/android/a/f/j;

    .line 251
    new-instance v0, Lcom/google/android/a/f/j;

    invoke-direct {v0, v3}, Lcom/google/android/a/f/j;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/a/c/e/f;->g:Lcom/google/android/a/f/j;

    .line 252
    new-instance v0, Lcom/google/android/a/f/j;

    invoke-direct {v0}, Lcom/google/android/a/f/j;-><init>()V

    iput-object v0, p0, Lcom/google/android/a/c/e/f;->k:Lcom/google/android/a/f/j;

    .line 253
    new-instance v0, Lcom/google/android/a/f/j;

    invoke-direct {v0}, Lcom/google/android/a/f/j;-><init>()V

    iput-object v0, p0, Lcom/google/android/a/c/e/f;->l:Lcom/google/android/a/f/j;

    .line 254
    return-void
.end method

.method private a(Lcom/google/android/a/c/f;Lcom/google/android/a/c/m;I)I
    .locals 2

    .prologue
    .line 954
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->k:Lcom/google/android/a/f/j;

    invoke-virtual {v0}, Lcom/google/android/a/f/j;->b()I

    move-result v0

    .line 955
    if-lez v0, :cond_0

    .line 956
    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 957
    iget-object v1, p0, Lcom/google/android/a/c/e/f;->k:Lcom/google/android/a/f/j;

    invoke-interface {p2, v1, v0}, Lcom/google/android/a/c/m;->a(Lcom/google/android/a/f/j;I)V

    .line 961
    :goto_0
    iget v1, p0, Lcom/google/android/a/c/e/f;->M:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/google/android/a/c/e/f;->M:I

    .line 962
    iget v1, p0, Lcom/google/android/a/c/e/f;->P:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/google/android/a/c/e/f;->P:I

    .line 963
    return v0

    .line 959
    :cond_0
    const/4 v0, 0x0

    invoke-interface {p2, p1, p3, v0}, Lcom/google/android/a/c/m;->a(Lcom/google/android/a/c/f;IZ)I

    move-result v0

    goto :goto_0
.end method

.method private a(J)J
    .locals 7

    .prologue
    .line 1029
    iget-wide v0, p0, Lcom/google/android/a/c/e/f;->o:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1030
    new-instance v0, Lcom/google/android/a/q;

    const-string v1, "Can\'t scale timecode prior to timecodeScale being set."

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1032
    :cond_0
    iget-wide v2, p0, Lcom/google/android/a/c/e/f;->o:J

    const-wide/16 v4, 0x3e8

    move-wide v0, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/a/f/n;->a(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method private a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 789
    iput v0, p0, Lcom/google/android/a/c/e/f;->M:I

    .line 790
    iput v0, p0, Lcom/google/android/a/c/e/f;->P:I

    .line 791
    iput v0, p0, Lcom/google/android/a/c/e/f;->O:I

    .line 792
    iput-boolean v0, p0, Lcom/google/android/a/c/e/f;->N:Z

    .line 793
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->k:Lcom/google/android/a/f/j;

    invoke-virtual {v0}, Lcom/google/android/a/f/j;->a()V

    .line 794
    return-void
.end method

.method private a(Lcom/google/android/a/c/e/f$b;)V
    .locals 4

    .prologue
    .line 907
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->l:Lcom/google/android/a/f/j;

    iget-object v0, v0, Lcom/google/android/a/f/j;->a:[B

    iget-wide v2, p0, Lcom/google/android/a/c/e/f;->F:J

    invoke-static {v0, v2, v3}, Lcom/google/android/a/c/e/f;->a([BJ)V

    .line 910
    iget-object v0, p1, Lcom/google/android/a/c/e/f$b;->o:Lcom/google/android/a/c/m;

    iget-object v1, p0, Lcom/google/android/a/c/e/f;->l:Lcom/google/android/a/f/j;

    iget-object v2, p0, Lcom/google/android/a/c/e/f;->l:Lcom/google/android/a/f/j;

    invoke-virtual {v2}, Lcom/google/android/a/f/j;->c()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/a/c/m;->a(Lcom/google/android/a/f/j;I)V

    .line 911
    iget v0, p0, Lcom/google/android/a/c/e/f;->P:I

    iget-object v1, p0, Lcom/google/android/a/c/e/f;->l:Lcom/google/android/a/f/j;

    invoke-virtual {v1}, Lcom/google/android/a/f/j;->c()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/a/c/e/f;->P:I

    .line 912
    return-void
.end method

.method private a(Lcom/google/android/a/c/e/f$b;J)V
    .locals 8

    .prologue
    .line 780
    const-string v0, "S_TEXT/UTF8"

    iget-object v1, p1, Lcom/google/android/a/c/e/f$b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 781
    invoke-direct {p0, p1}, Lcom/google/android/a/c/e/f;->a(Lcom/google/android/a/c/e/f$b;)V

    .line 783
    :cond_0
    iget-object v1, p1, Lcom/google/android/a/c/e/f$b;->o:Lcom/google/android/a/c/m;

    iget v4, p0, Lcom/google/android/a/c/e/f;->L:I

    iget v5, p0, Lcom/google/android/a/c/e/f;->P:I

    const/4 v6, 0x0

    iget-object v7, p1, Lcom/google/android/a/c/e/f$b;->g:[B

    move-wide v2, p2

    invoke-interface/range {v1 .. v7}, Lcom/google/android/a/c/m;->a(JIII[B)V

    .line 784
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/a/c/e/f;->Q:Z

    .line 785
    invoke-direct {p0}, Lcom/google/android/a/c/e/f;->a()V

    .line 786
    return-void
.end method

.method private a(Lcom/google/android/a/c/f;I)V
    .locals 3

    .prologue
    .line 802
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->h:Lcom/google/android/a/f/j;

    invoke-virtual {v0}, Lcom/google/android/a/f/j;->c()I

    move-result v0

    if-lt v0, p2, :cond_0

    .line 811
    :goto_0
    return-void

    .line 805
    :cond_0
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->h:Lcom/google/android/a/f/j;

    invoke-virtual {v0}, Lcom/google/android/a/f/j;->e()I

    move-result v0

    if-ge v0, p2, :cond_1

    .line 806
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->h:Lcom/google/android/a/f/j;

    iget-object v1, p0, Lcom/google/android/a/c/e/f;->h:Lcom/google/android/a/f/j;

    iget-object v1, v1, Lcom/google/android/a/f/j;->a:[B

    iget-object v2, p0, Lcom/google/android/a/c/e/f;->h:Lcom/google/android/a/f/j;

    iget-object v2, v2, Lcom/google/android/a/f/j;->a:[B

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v2, p2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/a/c/e/f;->h:Lcom/google/android/a/f/j;

    invoke-virtual {v2}, Lcom/google/android/a/f/j;->c()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/a/f/j;->a([BI)V

    .line 809
    :cond_1
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->h:Lcom/google/android/a/f/j;

    iget-object v0, v0, Lcom/google/android/a/f/j;->a:[B

    iget-object v1, p0, Lcom/google/android/a/c/e/f;->h:Lcom/google/android/a/f/j;

    invoke-virtual {v1}, Lcom/google/android/a/f/j;->c()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/a/c/e/f;->h:Lcom/google/android/a/f/j;

    invoke-virtual {v2}, Lcom/google/android/a/f/j;->c()I

    move-result v2

    sub-int v2, p2, v2

    invoke-interface {p1, v0, v1, v2}, Lcom/google/android/a/c/f;->b([BII)V

    .line 810
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->h:Lcom/google/android/a/f/j;

    invoke-virtual {v0, p2}, Lcom/google/android/a/f/j;->a(I)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/a/c/f;Lcom/google/android/a/c/e/f$b;I)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v4, 0x1

    const/4 v7, 0x0

    .line 815
    const-string v0, "S_TEXT/UTF8"

    iget-object v1, p2, Lcom/google/android/a/c/e/f$b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 816
    sget-object v0, Lcom/google/android/a/c/e/f;->a:[B

    array-length v0, v0

    add-int/2addr v0, p3

    .line 817
    iget-object v1, p0, Lcom/google/android/a/c/e/f;->l:Lcom/google/android/a/f/j;

    invoke-virtual {v1}, Lcom/google/android/a/f/j;->e()I

    move-result v1

    if-ge v1, v0, :cond_0

    .line 820
    iget-object v1, p0, Lcom/google/android/a/c/e/f;->l:Lcom/google/android/a/f/j;

    sget-object v2, Lcom/google/android/a/c/e/f;->a:[B

    add-int v3, v0, p3

    invoke-static {v2, v3}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/a/f/j;->a:[B

    .line 822
    :cond_0
    iget-object v1, p0, Lcom/google/android/a/c/e/f;->l:Lcom/google/android/a/f/j;

    iget-object v1, v1, Lcom/google/android/a/f/j;->a:[B

    sget-object v2, Lcom/google/android/a/c/e/f;->a:[B

    array-length v2, v2

    invoke-interface {p1, v1, v2, p3}, Lcom/google/android/a/c/f;->b([BII)V

    .line 823
    iget-object v1, p0, Lcom/google/android/a/c/e/f;->l:Lcom/google/android/a/f/j;

    invoke-virtual {v1, v7}, Lcom/google/android/a/f/j;->b(I)V

    .line 824
    iget-object v1, p0, Lcom/google/android/a/c/e/f;->l:Lcom/google/android/a/f/j;

    invoke-virtual {v1, v0}, Lcom/google/android/a/f/j;->a(I)V

    .line 904
    :cond_1
    :goto_0
    return-void

    .line 830
    :cond_2
    iget-object v0, p2, Lcom/google/android/a/c/e/f$b;->o:Lcom/google/android/a/c/m;

    .line 831
    iget-boolean v1, p0, Lcom/google/android/a/c/e/f;->N:Z

    if-nez v1, :cond_5

    .line 832
    iget-boolean v1, p2, Lcom/google/android/a/c/e/f$b;->e:Z

    if-eqz v1, :cond_7

    .line 835
    iget v1, p0, Lcom/google/android/a/c/e/f;->L:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/android/a/c/e/f;->L:I

    .line 836
    iget-object v1, p0, Lcom/google/android/a/c/e/f;->h:Lcom/google/android/a/f/j;

    iget-object v1, v1, Lcom/google/android/a/f/j;->a:[B

    invoke-interface {p1, v1, v7, v4}, Lcom/google/android/a/c/f;->b([BII)V

    .line 837
    iget v1, p0, Lcom/google/android/a/c/e/f;->M:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/a/c/e/f;->M:I

    .line 838
    iget-object v1, p0, Lcom/google/android/a/c/e/f;->h:Lcom/google/android/a/f/j;

    iget-object v1, v1, Lcom/google/android/a/f/j;->a:[B

    aget-byte v1, v1, v7

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_3

    .line 839
    new-instance v0, Lcom/google/android/a/q;

    const-string v1, "Extension bit is set in signal byte"

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 841
    :cond_3
    iget-object v1, p0, Lcom/google/android/a/c/e/f;->h:Lcom/google/android/a/f/j;

    iget-object v1, v1, Lcom/google/android/a/f/j;->a:[B

    aget-byte v1, v1, v7

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v4, :cond_4

    .line 842
    iget-object v1, p0, Lcom/google/android/a/c/e/f;->h:Lcom/google/android/a/f/j;

    iget-object v1, v1, Lcom/google/android/a/f/j;->a:[B

    const/16 v2, 0x8

    aput-byte v2, v1, v7

    .line 843
    iget-object v1, p0, Lcom/google/android/a/c/e/f;->h:Lcom/google/android/a/f/j;

    invoke-virtual {v1, v7}, Lcom/google/android/a/f/j;->b(I)V

    .line 844
    iget-object v1, p0, Lcom/google/android/a/c/e/f;->h:Lcom/google/android/a/f/j;

    invoke-interface {v0, v1, v4}, Lcom/google/android/a/c/m;->a(Lcom/google/android/a/f/j;I)V

    .line 845
    iget v1, p0, Lcom/google/android/a/c/e/f;->P:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/a/c/e/f;->P:I

    .line 846
    iget v1, p0, Lcom/google/android/a/c/e/f;->L:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/android/a/c/e/f;->L:I

    .line 852
    :cond_4
    :goto_1
    iput-boolean v4, p0, Lcom/google/android/a/c/e/f;->N:Z

    .line 854
    :cond_5
    iget-object v1, p0, Lcom/google/android/a/c/e/f;->k:Lcom/google/android/a/f/j;

    invoke-virtual {v1}, Lcom/google/android/a/f/j;->c()I

    move-result v1

    add-int/2addr v1, p3

    .line 856
    const-string v2, "V_MPEG4/ISO/AVC"

    iget-object v3, p2, Lcom/google/android/a/c/e/f$b;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    const-string v2, "V_MPEGH/ISO/HEVC"

    iget-object v3, p2, Lcom/google/android/a/c/e/f$b;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 861
    :cond_6
    iget-object v2, p0, Lcom/google/android/a/c/e/f;->g:Lcom/google/android/a/f/j;

    iget-object v2, v2, Lcom/google/android/a/f/j;->a:[B

    .line 862
    aput-byte v7, v2, v7

    .line 863
    aput-byte v7, v2, v4

    .line 864
    const/4 v3, 0x2

    aput-byte v7, v2, v3

    .line 865
    iget v3, p2, Lcom/google/android/a/c/e/f$b;->p:I

    .line 866
    iget v4, p2, Lcom/google/android/a/c/e/f$b;->p:I

    rsub-int/lit8 v4, v4, 0x4

    .line 870
    :goto_2
    iget v5, p0, Lcom/google/android/a/c/e/f;->M:I

    if-ge v5, v1, :cond_a

    .line 871
    iget v5, p0, Lcom/google/android/a/c/e/f;->O:I

    if-nez v5, :cond_8

    .line 873
    invoke-direct {p0, p1, v2, v4, v3}, Lcom/google/android/a/c/e/f;->a(Lcom/google/android/a/c/f;[BII)V

    .line 875
    iget-object v5, p0, Lcom/google/android/a/c/e/f;->g:Lcom/google/android/a/f/j;

    invoke-virtual {v5, v7}, Lcom/google/android/a/f/j;->b(I)V

    .line 876
    iget-object v5, p0, Lcom/google/android/a/c/e/f;->g:Lcom/google/android/a/f/j;

    invoke-virtual {v5}, Lcom/google/android/a/f/j;->o()I

    move-result v5

    iput v5, p0, Lcom/google/android/a/c/e/f;->O:I

    .line 878
    iget-object v5, p0, Lcom/google/android/a/c/e/f;->f:Lcom/google/android/a/f/j;

    invoke-virtual {v5, v7}, Lcom/google/android/a/f/j;->b(I)V

    .line 879
    iget-object v5, p0, Lcom/google/android/a/c/e/f;->f:Lcom/google/android/a/f/j;

    invoke-interface {v0, v5, v8}, Lcom/google/android/a/c/m;->a(Lcom/google/android/a/f/j;I)V

    .line 880
    iget v5, p0, Lcom/google/android/a/c/e/f;->P:I

    add-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/android/a/c/e/f;->P:I

    goto :goto_2

    .line 848
    :cond_7
    iget-object v1, p2, Lcom/google/android/a/c/e/f$b;->f:[B

    if-eqz v1, :cond_4

    .line 850
    iget-object v1, p0, Lcom/google/android/a/c/e/f;->k:Lcom/google/android/a/f/j;

    iget-object v2, p2, Lcom/google/android/a/c/e/f$b;->f:[B

    iget-object v3, p2, Lcom/google/android/a/c/e/f$b;->f:[B

    array-length v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/a/f/j;->a([BI)V

    goto :goto_1

    .line 883
    :cond_8
    iget v5, p0, Lcom/google/android/a/c/e/f;->O:I

    iget v6, p0, Lcom/google/android/a/c/e/f;->O:I

    invoke-direct {p0, p1, v0, v6}, Lcom/google/android/a/c/e/f;->a(Lcom/google/android/a/c/f;Lcom/google/android/a/c/m;I)I

    move-result v6

    sub-int/2addr v5, v6

    iput v5, p0, Lcom/google/android/a/c/e/f;->O:I

    goto :goto_2

    .line 888
    :cond_9
    :goto_3
    iget v2, p0, Lcom/google/android/a/c/e/f;->M:I

    if-ge v2, v1, :cond_a

    .line 889
    iget v2, p0, Lcom/google/android/a/c/e/f;->M:I

    sub-int v2, v1, v2

    invoke-direct {p0, p1, v0, v2}, Lcom/google/android/a/c/e/f;->a(Lcom/google/android/a/c/f;Lcom/google/android/a/c/m;I)I

    goto :goto_3

    .line 893
    :cond_a
    const-string v1, "A_VORBIS"

    iget-object v2, p2, Lcom/google/android/a/c/e/f$b;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 900
    iget-object v1, p0, Lcom/google/android/a/c/e/f;->i:Lcom/google/android/a/f/j;

    invoke-virtual {v1, v7}, Lcom/google/android/a/f/j;->b(I)V

    .line 901
    iget-object v1, p0, Lcom/google/android/a/c/e/f;->i:Lcom/google/android/a/f/j;

    invoke-interface {v0, v1, v8}, Lcom/google/android/a/c/m;->a(Lcom/google/android/a/f/j;I)V

    .line 902
    iget v0, p0, Lcom/google/android/a/c/e/f;->P:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/a/c/e/f;->P:I

    goto/16 :goto_0
.end method

.method private a(Lcom/google/android/a/c/f;[BII)V
    .locals 3

    .prologue
    .line 939
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->k:Lcom/google/android/a/f/j;

    invoke-virtual {v0}, Lcom/google/android/a/f/j;->b()I

    move-result v0

    invoke-static {p4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 940
    add-int v1, p3, v0

    sub-int v2, p4, v0

    invoke-interface {p1, p2, v1, v2}, Lcom/google/android/a/c/f;->b([BII)V

    .line 941
    if-lez v0, :cond_0

    .line 942
    iget-object v1, p0, Lcom/google/android/a/c/e/f;->k:Lcom/google/android/a/f/j;

    invoke-virtual {v1, p2, p3, v0}, Lcom/google/android/a/f/j;->a([BII)V

    .line 944
    :cond_0
    iget v0, p0, Lcom/google/android/a/c/e/f;->M:I

    add-int/2addr v0, p4

    iput v0, p0, Lcom/google/android/a/c/e/f;->M:I

    .line 945
    return-void
.end method

.method private static a([BJ)V
    .locals 9

    .prologue
    const-wide v4, 0xd693a400L

    const/4 v8, 0x0

    .line 916
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 917
    sget-object v0, Lcom/google/android/a/c/e/f;->b:[B

    .line 929
    :goto_0
    const/16 v1, 0x13

    const/16 v2, 0xc

    invoke-static {v0, v8, p0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 931
    return-void

    .line 919
    :cond_0
    div-long v0, p1, v4

    long-to-int v0, v0

    .line 920
    int-to-long v2, v0

    mul-long/2addr v2, v4

    sub-long v2, p1, v2

    .line 921
    const-wide/32 v4, 0x3938700

    div-long v4, v2, v4

    long-to-int v1, v4

    .line 922
    const v4, 0x3938700

    mul-int/2addr v4, v1

    int-to-long v4, v4

    sub-long/2addr v2, v4

    .line 923
    const-wide/32 v4, 0xf4240

    div-long v4, v2, v4

    long-to-int v4, v4

    .line 924
    const v5, 0xf4240

    mul-int/2addr v5, v4

    int-to-long v6, v5

    sub-long/2addr v2, v6

    .line 925
    const-wide/16 v6, 0x3e8

    div-long/2addr v2, v6

    long-to-int v2, v2

    .line 926
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "%02d:%02d:%02d,%03d"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v8

    const/4 v0, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v0

    const/4 v0, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v0

    const/4 v0, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v0

    invoke-static {v3, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/google/android/a/c/j;J)Z
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1012
    iget-boolean v2, p0, Lcom/google/android/a/c/e/f;->w:Z

    if-eqz v2, :cond_0

    .line 1013
    iput-wide p2, p0, Lcom/google/android/a/c/e/f;->y:J

    .line 1014
    iget-wide v2, p0, Lcom/google/android/a/c/e/f;->x:J

    iput-wide v2, p1, Lcom/google/android/a/c/j;->a:J

    .line 1015
    iput-boolean v1, p0, Lcom/google/android/a/c/e/f;->w:Z

    .line 1025
    :goto_0
    return v0

    .line 1020
    :cond_0
    iget-boolean v2, p0, Lcom/google/android/a/c/e/f;->t:Z

    if-eqz v2, :cond_1

    iget-wide v2, p0, Lcom/google/android/a/c/e/f;->y:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 1021
    iget-wide v2, p0, Lcom/google/android/a/c/e/f;->y:J

    iput-wide v2, p1, Lcom/google/android/a/c/j;->a:J

    .line 1022
    iput-wide v4, p0, Lcom/google/android/a/c/e/f;->y:J

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1025
    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1036
    const-string v0, "V_VP8"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_VP9"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_MPEG2"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_MPEG4/ISO/SP"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_MPEG4/ISO/ASP"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_MPEG4/ISO/AP"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_MPEG4/ISO/AVC"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_MPEGH/ISO/HEVC"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_OPUS"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_VORBIS"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_AAC"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_MPEG/L3"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_AC3"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_EAC3"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_TRUEHD"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_DTS"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_DTS/EXPRESS"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_DTS/LOSSLESS"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "S_TEXT/UTF8"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a([II)[I
    .locals 1

    .prologue
    .line 1062
    if-nez p0, :cond_1

    .line 1063
    new-array p0, p1, [I

    .line 1068
    :cond_0
    :goto_0
    return-object p0

    .line 1064
    :cond_1
    array-length v0, p0

    if-ge v0, p1, :cond_0

    .line 1068
    array-length v0, p0

    mul-int/lit8 v0, v0, 0x2

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    new-array p0, v0, [I

    goto :goto_0
.end method

.method private c()Lcom/google/android/a/c/l;
    .locals 13

    .prologue
    const-wide/16 v4, -0x1

    const/4 v0, 0x0

    const/4 v12, 0x0

    .line 973
    iget-wide v2, p0, Lcom/google/android/a/c/e/f;->m:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/a/c/e/f;->q:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/a/c/e/f;->A:Lcom/google/android/a/f/e;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/a/c/e/f;->A:Lcom/google/android/a/f/e;

    invoke-virtual {v1}, Lcom/google/android/a/f/e;->a()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/a/c/e/f;->B:Lcom/google/android/a/f/e;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/a/c/e/f;->B:Lcom/google/android/a/f/e;

    invoke-virtual {v1}, Lcom/google/android/a/f/e;->a()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/a/c/e/f;->A:Lcom/google/android/a/f/e;

    invoke-virtual {v2}, Lcom/google/android/a/f/e;->a()I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 977
    :cond_0
    iput-object v12, p0, Lcom/google/android/a/c/e/f;->A:Lcom/google/android/a/f/e;

    .line 978
    iput-object v12, p0, Lcom/google/android/a/c/e/f;->B:Lcom/google/android/a/f/e;

    .line 979
    sget-object v0, Lcom/google/android/a/c/l;->f:Lcom/google/android/a/c/l;

    .line 999
    :goto_0
    return-object v0

    .line 981
    :cond_1
    iget-object v1, p0, Lcom/google/android/a/c/e/f;->A:Lcom/google/android/a/f/e;

    invoke-virtual {v1}, Lcom/google/android/a/f/e;->a()I

    move-result v2

    .line 982
    new-array v3, v2, [I

    .line 983
    new-array v4, v2, [J

    .line 984
    new-array v5, v2, [J

    .line 985
    new-array v6, v2, [J

    move v1, v0

    .line 986
    :goto_1
    if-ge v1, v2, :cond_2

    .line 987
    iget-object v7, p0, Lcom/google/android/a/c/e/f;->A:Lcom/google/android/a/f/e;

    invoke-virtual {v7, v1}, Lcom/google/android/a/f/e;->a(I)J

    move-result-wide v8

    aput-wide v8, v6, v1

    .line 988
    iget-wide v8, p0, Lcom/google/android/a/c/e/f;->m:J

    iget-object v7, p0, Lcom/google/android/a/c/e/f;->B:Lcom/google/android/a/f/e;

    invoke-virtual {v7, v1}, Lcom/google/android/a/f/e;->a(I)J

    move-result-wide v10

    add-long/2addr v8, v10

    aput-wide v8, v4, v1

    .line 986
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 990
    :cond_2
    :goto_2
    add-int/lit8 v1, v2, -0x1

    if-ge v0, v1, :cond_3

    .line 991
    add-int/lit8 v1, v0, 0x1

    aget-wide v8, v4, v1

    aget-wide v10, v4, v0

    sub-long/2addr v8, v10

    long-to-int v1, v8

    aput v1, v3, v0

    .line 992
    add-int/lit8 v1, v0, 0x1

    aget-wide v8, v6, v1

    aget-wide v10, v6, v0

    sub-long/2addr v8, v10

    aput-wide v8, v5, v0

    .line 990
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 994
    :cond_3
    add-int/lit8 v0, v2, -0x1

    iget-wide v8, p0, Lcom/google/android/a/c/e/f;->m:J

    iget-wide v10, p0, Lcom/google/android/a/c/e/f;->n:J

    add-long/2addr v8, v10

    add-int/lit8 v1, v2, -0x1

    aget-wide v10, v4, v1

    sub-long/2addr v8, v10

    long-to-int v1, v8

    aput v1, v3, v0

    .line 996
    add-int/lit8 v0, v2, -0x1

    iget-wide v8, p0, Lcom/google/android/a/c/e/f;->q:J

    add-int/lit8 v1, v2, -0x1

    aget-wide v10, v6, v1

    sub-long/2addr v8, v10

    aput-wide v8, v5, v0

    .line 997
    iput-object v12, p0, Lcom/google/android/a/c/e/f;->A:Lcom/google/android/a/f/e;

    .line 998
    iput-object v12, p0, Lcom/google/android/a/c/e/f;->B:Lcom/google/android/a/f/e;

    .line 999
    new-instance v0, Lcom/google/android/a/c/a;

    invoke-direct {v0, v3, v4, v5, v6}, Lcom/google/android/a/c/a;-><init>([I[J[J[J)V

    goto :goto_0
.end method


# virtual methods
.method a(I)I
    .locals 1

    .prologue
    .line 290
    sparse-switch p1, :sswitch_data_0

    .line 349
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 310
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 333
    :sswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 337
    :sswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 344
    :sswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 347
    :sswitch_4
    const/4 v0, 0x5

    goto :goto_0

    .line 290
    nop

    :sswitch_data_0
    .sparse-switch
        0x83 -> :sswitch_1
        0x86 -> :sswitch_2
        0x9b -> :sswitch_1
        0x9f -> :sswitch_1
        0xa0 -> :sswitch_0
        0xa1 -> :sswitch_3
        0xa3 -> :sswitch_3
        0xae -> :sswitch_0
        0xb0 -> :sswitch_1
        0xb3 -> :sswitch_1
        0xb5 -> :sswitch_4
        0xb7 -> :sswitch_0
        0xba -> :sswitch_1
        0xbb -> :sswitch_0
        0xd7 -> :sswitch_1
        0xe0 -> :sswitch_0
        0xe1 -> :sswitch_0
        0xe7 -> :sswitch_1
        0xf1 -> :sswitch_1
        0xfb -> :sswitch_1
        0x4254 -> :sswitch_1
        0x4255 -> :sswitch_3
        0x4282 -> :sswitch_2
        0x4285 -> :sswitch_1
        0x42f7 -> :sswitch_1
        0x4489 -> :sswitch_4
        0x47e1 -> :sswitch_1
        0x47e2 -> :sswitch_3
        0x47e7 -> :sswitch_0
        0x47e8 -> :sswitch_1
        0x4dbb -> :sswitch_0
        0x5031 -> :sswitch_1
        0x5032 -> :sswitch_1
        0x5034 -> :sswitch_0
        0x5035 -> :sswitch_0
        0x53ab -> :sswitch_3
        0x53ac -> :sswitch_1
        0x56aa -> :sswitch_1
        0x56bb -> :sswitch_1
        0x6240 -> :sswitch_0
        0x63a2 -> :sswitch_3
        0x6d80 -> :sswitch_0
        0x22b59c -> :sswitch_2
        0x23e383 -> :sswitch_1
        0x2ad7b1 -> :sswitch_1
        0x114d9b74 -> :sswitch_0
        0x1549a966 -> :sswitch_0
        0x1654ae6b -> :sswitch_0
        0x18538067 -> :sswitch_0
        0x1a45dfa3 -> :sswitch_0
        0x1c53bb6b -> :sswitch_0
        0x1f43b675 -> :sswitch_0
    .end sparse-switch
.end method

.method public a(Lcom/google/android/a/c/f;Lcom/google/android/a/c/j;)I
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 278
    iput-boolean v0, p0, Lcom/google/android/a/c/e/f;->Q:Z

    move v2, v1

    .line 280
    :cond_0
    if-eqz v2, :cond_2

    iget-boolean v3, p0, Lcom/google/android/a/c/e/f;->Q:Z

    if-nez v3, :cond_2

    .line 281
    iget-object v2, p0, Lcom/google/android/a/c/e/f;->c:Lcom/google/android/a/c/e/b;

    invoke-interface {v2, p1}, Lcom/google/android/a/c/e/b;->a(Lcom/google/android/a/c/f;)Z

    move-result v2

    .line 282
    if-eqz v2, :cond_0

    invoke-interface {p1}, Lcom/google/android/a/c/f;->c()J

    move-result-wide v4

    invoke-direct {p0, p2, v4, v5}, Lcom/google/android/a/c/e/f;->a(Lcom/google/android/a/c/j;J)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    .line 286
    :cond_1
    :goto_0
    return v0

    :cond_2
    if-nez v2, :cond_1

    const/4 v0, -0x1

    goto :goto_0
.end method

.method a(ID)V
    .locals 2

    .prologue
    .line 588
    sparse-switch p1, :sswitch_data_0

    .line 596
    :goto_0
    return-void

    .line 590
    :sswitch_0
    double-to-long v0, p2

    iput-wide v0, p0, Lcom/google/android/a/c/e/f;->p:J

    goto :goto_0

    .line 593
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    double-to-int v1, p2

    iput v1, v0, Lcom/google/android/a/c/e/f$b;->l:I

    goto :goto_0

    .line 588
    :sswitch_data_0
    .sparse-switch
        0xb5 -> :sswitch_1
        0x4489 -> :sswitch_0
    .end sparse-switch
.end method

.method a(IILcom/google/android/a/c/f;)V
    .locals 15

    .prologue
    .line 621
    sparse-switch p1, :sswitch_data_0

    .line 775
    new-instance v2, Lcom/google/android/a/q;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unexpected id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v2

    .line 623
    :sswitch_0
    iget-object v2, p0, Lcom/google/android/a/c/e/f;->j:Lcom/google/android/a/f/j;

    iget-object v2, v2, Lcom/google/android/a/f/j;->a:[B

    const/4 v3, 0x0

    invoke-static {v2, v3}, Ljava/util/Arrays;->fill([BB)V

    .line 624
    iget-object v2, p0, Lcom/google/android/a/c/e/f;->j:Lcom/google/android/a/f/j;

    iget-object v2, v2, Lcom/google/android/a/f/j;->a:[B

    rsub-int/lit8 v3, p2, 0x4

    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-interface {v0, v2, v3, v1}, Lcom/google/android/a/c/f;->b([BII)V

    .line 625
    iget-object v2, p0, Lcom/google/android/a/c/e/f;->j:Lcom/google/android/a/f/j;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/a/f/j;->b(I)V

    .line 626
    iget-object v2, p0, Lcom/google/android/a/c/e/f;->j:Lcom/google/android/a/f/j;

    invoke-virtual {v2}, Lcom/google/android/a/f/j;->j()J

    move-result-wide v2

    long-to-int v2, v2

    iput v2, p0, Lcom/google/android/a/c/e/f;->u:I

    .line 773
    :goto_0
    return-void

    .line 629
    :sswitch_1
    iget-object v2, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    move/from16 v0, p2

    new-array v3, v0, [B

    iput-object v3, v2, Lcom/google/android/a/c/e/f$b;->h:[B

    .line 630
    iget-object v2, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    iget-object v2, v2, Lcom/google/android/a/c/e/f$b;->h:[B

    const/4 v3, 0x0

    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-interface {v0, v2, v3, v1}, Lcom/google/android/a/c/f;->b([BII)V

    goto :goto_0

    .line 634
    :sswitch_2
    iget-object v2, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    move/from16 v0, p2

    new-array v3, v0, [B

    iput-object v3, v2, Lcom/google/android/a/c/e/f$b;->f:[B

    .line 635
    iget-object v2, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    iget-object v2, v2, Lcom/google/android/a/c/e/f$b;->f:[B

    const/4 v3, 0x0

    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-interface {v0, v2, v3, v1}, Lcom/google/android/a/c/f;->b([BII)V

    goto :goto_0

    .line 638
    :sswitch_3
    iget-object v2, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    move/from16 v0, p2

    new-array v3, v0, [B

    iput-object v3, v2, Lcom/google/android/a/c/e/f$b;->g:[B

    .line 639
    iget-object v2, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    iget-object v2, v2, Lcom/google/android/a/c/e/f$b;->g:[B

    const/4 v3, 0x0

    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-interface {v0, v2, v3, v1}, Lcom/google/android/a/c/f;->b([BII)V

    goto :goto_0

    .line 648
    :sswitch_4
    iget v2, p0, Lcom/google/android/a/c/e/f;->D:I

    if-nez v2, :cond_0

    .line 649
    iget-object v2, p0, Lcom/google/android/a/c/e/f;->d:Lcom/google/android/a/c/e/e;

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/16 v5, 0x8

    move-object/from16 v0, p3

    invoke-virtual {v2, v0, v3, v4, v5}, Lcom/google/android/a/c/e/e;->a(Lcom/google/android/a/c/f;ZZI)J

    move-result-wide v2

    long-to-int v2, v2

    iput v2, p0, Lcom/google/android/a/c/e/f;->J:I

    .line 650
    iget-object v2, p0, Lcom/google/android/a/c/e/f;->d:Lcom/google/android/a/c/e/e;

    invoke-virtual {v2}, Lcom/google/android/a/c/e/e;->b()I

    move-result v2

    iput v2, p0, Lcom/google/android/a/c/e/f;->K:I

    .line 651
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/a/c/e/f;->F:J

    .line 652
    const/4 v2, 0x1

    iput v2, p0, Lcom/google/android/a/c/e/f;->D:I

    .line 653
    iget-object v2, p0, Lcom/google/android/a/c/e/f;->h:Lcom/google/android/a/f/j;

    invoke-virtual {v2}, Lcom/google/android/a/f/j;->a()V

    .line 656
    :cond_0
    iget-object v2, p0, Lcom/google/android/a/c/e/f;->e:Landroid/util/SparseArray;

    iget v3, p0, Lcom/google/android/a/c/e/f;->J:I

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/a/c/e/f$b;

    .line 659
    if-nez v2, :cond_1

    .line 660
    iget v2, p0, Lcom/google/android/a/c/e/f;->K:I

    sub-int v2, p2, v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Lcom/google/android/a/c/f;->b(I)V

    .line 661
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/a/c/e/f;->D:I

    goto :goto_0

    .line 665
    :cond_1
    iget v3, p0, Lcom/google/android/a/c/e/f;->D:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    .line 667
    const/4 v3, 0x3

    move-object/from16 v0, p3

    invoke-direct {p0, v0, v3}, Lcom/google/android/a/c/e/f;->a(Lcom/google/android/a/c/f;I)V

    .line 668
    iget-object v3, p0, Lcom/google/android/a/c/e/f;->h:Lcom/google/android/a/f/j;

    iget-object v3, v3, Lcom/google/android/a/f/j;->a:[B

    const/4 v4, 0x2

    aget-byte v3, v3, v4

    and-int/lit8 v3, v3, 0x6

    shr-int/lit8 v3, v3, 0x1

    .line 669
    if-nez v3, :cond_4

    .line 670
    const/4 v3, 0x1

    iput v3, p0, Lcom/google/android/a/c/e/f;->H:I

    .line 671
    iget-object v3, p0, Lcom/google/android/a/c/e/f;->I:[I

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/google/android/a/c/e/f;->a([II)[I

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/a/c/e/f;->I:[I

    .line 672
    iget-object v3, p0, Lcom/google/android/a/c/e/f;->I:[I

    const/4 v4, 0x0

    iget v5, p0, Lcom/google/android/a/c/e/f;->K:I

    sub-int v5, p2, v5

    add-int/lit8 v5, v5, -0x3

    aput v5, v3, v4

    .line 746
    :goto_1
    iget-object v3, p0, Lcom/google/android/a/c/e/f;->h:Lcom/google/android/a/f/j;

    iget-object v3, v3, Lcom/google/android/a/f/j;->a:[B

    const/4 v4, 0x0

    aget-byte v3, v3, v4

    shl-int/lit8 v3, v3, 0x8

    iget-object v4, p0, Lcom/google/android/a/c/e/f;->h:Lcom/google/android/a/f/j;

    iget-object v4, v4, Lcom/google/android/a/f/j;->a:[B

    const/4 v5, 0x1

    aget-byte v4, v4, v5

    and-int/lit16 v4, v4, 0xff

    or-int/2addr v3, v4

    .line 747
    iget-wide v4, p0, Lcom/google/android/a/c/e/f;->z:J

    int-to-long v6, v3

    invoke-direct {p0, v6, v7}, Lcom/google/android/a/c/e/f;->a(J)J

    move-result-wide v6

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/android/a/c/e/f;->E:J

    .line 748
    iget-object v3, p0, Lcom/google/android/a/c/e/f;->h:Lcom/google/android/a/f/j;

    iget-object v3, v3, Lcom/google/android/a/f/j;->a:[B

    const/4 v4, 0x2

    aget-byte v3, v3, v4

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_13

    const/4 v3, 0x1

    .line 749
    :goto_2
    iget v4, v2, Lcom/google/android/a/c/e/f$b;->c:I

    const/4 v5, 0x2

    if-eq v4, v5, :cond_2

    const/16 v4, 0xa3

    move/from16 v0, p1

    if-ne v0, v4, :cond_14

    iget-object v4, p0, Lcom/google/android/a/c/e/f;->h:Lcom/google/android/a/f/j;

    iget-object v4, v4, Lcom/google/android/a/f/j;->a:[B

    const/4 v5, 0x2

    aget-byte v4, v4, v5

    and-int/lit16 v4, v4, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_14

    :cond_2
    const/4 v4, 0x1

    .line 751
    :goto_3
    if-eqz v4, :cond_15

    const/4 v4, 0x1

    :goto_4
    if-eqz v3, :cond_16

    const/high16 v3, 0x8000000

    :goto_5
    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/android/a/c/e/f;->L:I

    .line 753
    const/4 v3, 0x2

    iput v3, p0, Lcom/google/android/a/c/e/f;->D:I

    .line 754
    const/4 v3, 0x0

    iput v3, p0, Lcom/google/android/a/c/e/f;->G:I

    .line 757
    :cond_3
    const/16 v3, 0xa3

    move/from16 v0, p1

    if-ne v0, v3, :cond_18

    .line 759
    :goto_6
    iget v3, p0, Lcom/google/android/a/c/e/f;->G:I

    iget v4, p0, Lcom/google/android/a/c/e/f;->H:I

    if-ge v3, v4, :cond_17

    .line 760
    iget-object v3, p0, Lcom/google/android/a/c/e/f;->I:[I

    iget v4, p0, Lcom/google/android/a/c/e/f;->G:I

    aget v3, v3, v4

    move-object/from16 v0, p3

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/a/c/e/f;->a(Lcom/google/android/a/c/f;Lcom/google/android/a/c/e/f$b;I)V

    .line 761
    iget-wide v4, p0, Lcom/google/android/a/c/e/f;->E:J

    iget v3, p0, Lcom/google/android/a/c/e/f;->G:I

    iget v6, v2, Lcom/google/android/a/c/e/f$b;->d:I

    mul-int/2addr v3, v6

    div-int/lit16 v3, v3, 0x3e8

    int-to-long v6, v3

    add-long/2addr v4, v6

    .line 763
    invoke-direct {p0, v2, v4, v5}, Lcom/google/android/a/c/e/f;->a(Lcom/google/android/a/c/e/f$b;J)V

    .line 764
    iget v3, p0, Lcom/google/android/a/c/e/f;->G:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/a/c/e/f;->G:I

    goto :goto_6

    .line 674
    :cond_4
    const/16 v4, 0xa3

    move/from16 v0, p1

    if-eq v0, v4, :cond_5

    .line 675
    new-instance v2, Lcom/google/android/a/q;

    const-string v3, "Lacing only supported in SimpleBlocks."

    invoke-direct {v2, v3}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v2

    .line 679
    :cond_5
    const/4 v4, 0x4

    move-object/from16 v0, p3

    invoke-direct {p0, v0, v4}, Lcom/google/android/a/c/e/f;->a(Lcom/google/android/a/c/f;I)V

    .line 680
    iget-object v4, p0, Lcom/google/android/a/c/e/f;->h:Lcom/google/android/a/f/j;

    iget-object v4, v4, Lcom/google/android/a/f/j;->a:[B

    const/4 v5, 0x3

    aget-byte v4, v4, v5

    and-int/lit16 v4, v4, 0xff

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/android/a/c/e/f;->H:I

    .line 681
    iget-object v4, p0, Lcom/google/android/a/c/e/f;->I:[I

    iget v5, p0, Lcom/google/android/a/c/e/f;->H:I

    invoke-static {v4, v5}, Lcom/google/android/a/c/e/f;->a([II)[I

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/a/c/e/f;->I:[I

    .line 683
    const/4 v4, 0x2

    if-ne v3, v4, :cond_6

    .line 684
    iget v3, p0, Lcom/google/android/a/c/e/f;->K:I

    sub-int v3, p2, v3

    add-int/lit8 v3, v3, -0x4

    iget v4, p0, Lcom/google/android/a/c/e/f;->H:I

    div-int/2addr v3, v4

    .line 686
    iget-object v4, p0, Lcom/google/android/a/c/e/f;->I:[I

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/a/c/e/f;->H:I

    invoke-static {v4, v5, v6, v3}, Ljava/util/Arrays;->fill([IIII)V

    goto/16 :goto_1

    .line 687
    :cond_6
    const/4 v4, 0x1

    if-ne v3, v4, :cond_9

    .line 688
    const/4 v5, 0x0

    .line 689
    const/4 v4, 0x4

    .line 690
    const/4 v3, 0x0

    :goto_7
    iget v6, p0, Lcom/google/android/a/c/e/f;->H:I

    add-int/lit8 v6, v6, -0x1

    if-ge v3, v6, :cond_8

    .line 691
    iget-object v6, p0, Lcom/google/android/a/c/e/f;->I:[I

    const/4 v7, 0x0

    aput v7, v6, v3

    .line 694
    :cond_7
    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p3

    invoke-direct {p0, v0, v4}, Lcom/google/android/a/c/e/f;->a(Lcom/google/android/a/c/f;I)V

    .line 695
    iget-object v6, p0, Lcom/google/android/a/c/e/f;->h:Lcom/google/android/a/f/j;

    iget-object v6, v6, Lcom/google/android/a/f/j;->a:[B

    add-int/lit8 v7, v4, -0x1

    aget-byte v6, v6, v7

    and-int/lit16 v6, v6, 0xff

    .line 696
    iget-object v7, p0, Lcom/google/android/a/c/e/f;->I:[I

    aget v8, v7, v3

    add-int/2addr v8, v6

    aput v8, v7, v3

    .line 697
    const/16 v7, 0xff

    if-eq v6, v7, :cond_7

    .line 698
    iget-object v6, p0, Lcom/google/android/a/c/e/f;->I:[I

    aget v6, v6, v3

    add-int/2addr v5, v6

    .line 690
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 700
    :cond_8
    iget-object v3, p0, Lcom/google/android/a/c/e/f;->I:[I

    iget v6, p0, Lcom/google/android/a/c/e/f;->H:I

    add-int/lit8 v6, v6, -0x1

    iget v7, p0, Lcom/google/android/a/c/e/f;->K:I

    sub-int v7, p2, v7

    sub-int v4, v7, v4

    sub-int/2addr v4, v5

    aput v4, v3, v6

    goto/16 :goto_1

    .line 702
    :cond_9
    const/4 v4, 0x3

    if-ne v3, v4, :cond_12

    .line 703
    const/4 v5, 0x0

    .line 704
    const/4 v4, 0x4

    .line 705
    const/4 v3, 0x0

    :goto_8
    iget v6, p0, Lcom/google/android/a/c/e/f;->H:I

    add-int/lit8 v6, v6, -0x1

    if-ge v3, v6, :cond_11

    .line 706
    iget-object v6, p0, Lcom/google/android/a/c/e/f;->I:[I

    const/4 v7, 0x0

    aput v7, v6, v3

    .line 707
    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p3

    invoke-direct {p0, v0, v4}, Lcom/google/android/a/c/e/f;->a(Lcom/google/android/a/c/f;I)V

    .line 708
    iget-object v6, p0, Lcom/google/android/a/c/e/f;->h:Lcom/google/android/a/f/j;

    iget-object v6, v6, Lcom/google/android/a/f/j;->a:[B

    add-int/lit8 v7, v4, -0x1

    aget-byte v6, v6, v7

    if-nez v6, :cond_a

    .line 709
    new-instance v2, Lcom/google/android/a/q;

    const-string v3, "No valid varint length mask found"

    invoke-direct {v2, v3}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v2

    .line 711
    :cond_a
    const-wide/16 v6, 0x0

    .line 712
    const/4 v8, 0x0

    move v10, v8

    :goto_9
    const/16 v8, 0x8

    if-ge v10, v8, :cond_c

    .line 713
    const/4 v8, 0x1

    rsub-int/lit8 v9, v10, 0x7

    shl-int/2addr v8, v9

    .line 714
    iget-object v9, p0, Lcom/google/android/a/c/e/f;->h:Lcom/google/android/a/f/j;

    iget-object v9, v9, Lcom/google/android/a/f/j;->a:[B

    add-int/lit8 v11, v4, -0x1

    aget-byte v9, v9, v11

    and-int/2addr v9, v8

    if-eqz v9, :cond_e

    .line 715
    add-int/lit8 v7, v4, -0x1

    .line 716
    add-int/2addr v4, v10

    .line 717
    move-object/from16 v0, p3

    invoke-direct {p0, v0, v4}, Lcom/google/android/a/c/e/f;->a(Lcom/google/android/a/c/f;I)V

    .line 718
    iget-object v6, p0, Lcom/google/android/a/c/e/f;->h:Lcom/google/android/a/f/j;

    iget-object v9, v6, Lcom/google/android/a/f/j;->a:[B

    add-int/lit8 v6, v7, 0x1

    aget-byte v7, v9, v7

    and-int/lit16 v7, v7, 0xff

    xor-int/lit8 v8, v8, -0x1

    and-int/2addr v7, v8

    int-to-long v8, v7

    move v14, v6

    move-wide v6, v8

    move v8, v14

    .line 719
    :goto_a
    if-ge v8, v4, :cond_b

    .line 720
    const/16 v9, 0x8

    shl-long v12, v6, v9

    .line 721
    iget-object v6, p0, Lcom/google/android/a/c/e/f;->h:Lcom/google/android/a/f/j;

    iget-object v7, v6, Lcom/google/android/a/f/j;->a:[B

    add-int/lit8 v6, v8, 0x1

    aget-byte v7, v7, v8

    and-int/lit16 v7, v7, 0xff

    int-to-long v8, v7

    or-long/2addr v8, v12

    move v14, v6

    move-wide v6, v8

    move v8, v14

    goto :goto_a

    .line 724
    :cond_b
    if-lez v3, :cond_c

    .line 725
    const-wide/16 v8, 0x1

    mul-int/lit8 v10, v10, 0x7

    add-int/lit8 v10, v10, 0x6

    shl-long/2addr v8, v10

    const-wide/16 v10, 0x1

    sub-long/2addr v8, v10

    sub-long/2addr v6, v8

    .line 730
    :cond_c
    const-wide/32 v8, -0x80000000

    cmp-long v8, v6, v8

    if-ltz v8, :cond_d

    const-wide/32 v8, 0x7fffffff

    cmp-long v8, v6, v8

    if-lez v8, :cond_f

    .line 731
    :cond_d
    new-instance v2, Lcom/google/android/a/q;

    const-string v3, "EBML lacing sample size out of range."

    invoke-direct {v2, v3}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v2

    .line 712
    :cond_e
    add-int/lit8 v8, v10, 0x1

    move v10, v8

    goto :goto_9

    .line 733
    :cond_f
    long-to-int v6, v6

    .line 734
    iget-object v7, p0, Lcom/google/android/a/c/e/f;->I:[I

    if-nez v3, :cond_10

    :goto_b
    aput v6, v7, v3

    .line 736
    iget-object v6, p0, Lcom/google/android/a/c/e/f;->I:[I

    aget v6, v6, v3

    add-int/2addr v5, v6

    .line 705
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_8

    .line 734
    :cond_10
    iget-object v8, p0, Lcom/google/android/a/c/e/f;->I:[I

    add-int/lit8 v9, v3, -0x1

    aget v8, v8, v9

    add-int/2addr v6, v8

    goto :goto_b

    .line 738
    :cond_11
    iget-object v3, p0, Lcom/google/android/a/c/e/f;->I:[I

    iget v6, p0, Lcom/google/android/a/c/e/f;->H:I

    add-int/lit8 v6, v6, -0x1

    iget v7, p0, Lcom/google/android/a/c/e/f;->K:I

    sub-int v7, p2, v7

    sub-int v4, v7, v4

    sub-int/2addr v4, v5

    aput v4, v3, v6

    goto/16 :goto_1

    .line 742
    :cond_12
    new-instance v2, Lcom/google/android/a/q;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unexpected lacing value: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v2

    .line 748
    :cond_13
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 749
    :cond_14
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 751
    :cond_15
    const/4 v4, 0x0

    goto/16 :goto_4

    :cond_16
    const/4 v3, 0x0

    goto/16 :goto_5

    .line 766
    :cond_17
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/a/c/e/f;->D:I

    goto/16 :goto_0

    .line 770
    :cond_18
    iget-object v3, p0, Lcom/google/android/a/c/e/f;->I:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p3

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/a/c/e/f;->a(Lcom/google/android/a/c/f;Lcom/google/android/a/c/e/f$b;I)V

    goto/16 :goto_0

    .line 621
    :sswitch_data_0
    .sparse-switch
        0xa1 -> :sswitch_4
        0xa3 -> :sswitch_4
        0x4255 -> :sswitch_2
        0x47e2 -> :sswitch_3
        0x53ab -> :sswitch_0
        0x63a2 -> :sswitch_1
    .end sparse-switch
.end method

.method a(IJ)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const-wide/16 v0, 0x1

    .line 486
    sparse-switch p1, :sswitch_data_0

    .line 585
    :cond_0
    :goto_0
    return-void

    .line 489
    :sswitch_0
    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    .line 490
    new-instance v0, Lcom/google/android/a/q;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EBMLReadVersion "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 495
    :sswitch_1
    cmp-long v0, p2, v0

    if-ltz v0, :cond_1

    const-wide/16 v0, 0x2

    cmp-long v0, p2, v0

    if-lez v0, :cond_0

    .line 496
    :cond_1
    new-instance v0, Lcom/google/android/a/q;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DocTypeReadVersion "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 502
    :sswitch_2
    iget-wide v0, p0, Lcom/google/android/a/c/e/f;->m:J

    add-long/2addr v0, p2

    iput-wide v0, p0, Lcom/google/android/a/c/e/f;->v:J

    goto :goto_0

    .line 505
    :sswitch_3
    iput-wide p2, p0, Lcom/google/android/a/c/e/f;->o:J

    goto :goto_0

    .line 508
    :sswitch_4
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    long-to-int v1, p2

    iput v1, v0, Lcom/google/android/a/c/e/f$b;->i:I

    goto :goto_0

    .line 511
    :sswitch_5
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    long-to-int v1, p2

    iput v1, v0, Lcom/google/android/a/c/e/f$b;->j:I

    goto :goto_0

    .line 514
    :sswitch_6
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    long-to-int v1, p2

    iput v1, v0, Lcom/google/android/a/c/e/f$b;->b:I

    goto :goto_0

    .line 517
    :sswitch_7
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    long-to-int v1, p2

    iput v1, v0, Lcom/google/android/a/c/e/f$b;->c:I

    goto :goto_0

    .line 520
    :sswitch_8
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    long-to-int v1, p2

    iput v1, v0, Lcom/google/android/a/c/e/f$b;->d:I

    goto :goto_0

    .line 523
    :sswitch_9
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    iput-wide p2, v0, Lcom/google/android/a/c/e/f$b;->m:J

    goto :goto_0

    .line 526
    :sswitch_a
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    iput-wide p2, v0, Lcom/google/android/a/c/e/f$b;->n:J

    goto :goto_0

    .line 529
    :sswitch_b
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    long-to-int v1, p2

    iput v1, v0, Lcom/google/android/a/c/e/f$b;->k:I

    goto/16 :goto_0

    .line 532
    :sswitch_c
    iput-boolean v2, p0, Lcom/google/android/a/c/e/f;->R:Z

    goto/16 :goto_0

    .line 536
    :sswitch_d
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    .line 537
    new-instance v0, Lcom/google/android/a/q;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ContentEncodingOrder "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 542
    :sswitch_e
    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    .line 543
    new-instance v0, Lcom/google/android/a/q;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ContentEncodingScope "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 548
    :sswitch_f
    const-wide/16 v0, 0x3

    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    .line 549
    new-instance v0, Lcom/google/android/a/q;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ContentCompAlgo "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 554
    :sswitch_10
    const-wide/16 v0, 0x5

    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    .line 555
    new-instance v0, Lcom/google/android/a/q;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ContentEncAlgo "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 560
    :sswitch_11
    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    .line 561
    new-instance v0, Lcom/google/android/a/q;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AESSettingsCipherMode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 565
    :sswitch_12
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->A:Lcom/google/android/a/f/e;

    invoke-direct {p0, p2, p3}, Lcom/google/android/a/c/e/f;->a(J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/a/f/e;->a(J)V

    goto/16 :goto_0

    .line 568
    :sswitch_13
    iget-boolean v0, p0, Lcom/google/android/a/c/e/f;->C:Z

    if-nez v0, :cond_0

    .line 572
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->B:Lcom/google/android/a/f/e;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/a/f/e;->a(J)V

    .line 573
    iput-boolean v2, p0, Lcom/google/android/a/c/e/f;->C:Z

    goto/16 :goto_0

    .line 577
    :sswitch_14
    invoke-direct {p0, p2, p3}, Lcom/google/android/a/c/e/f;->a(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/a/c/e/f;->z:J

    goto/16 :goto_0

    .line 580
    :sswitch_15
    invoke-direct {p0, p2, p3}, Lcom/google/android/a/c/e/f;->a(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/a/c/e/f;->F:J

    goto/16 :goto_0

    .line 486
    :sswitch_data_0
    .sparse-switch
        0x83 -> :sswitch_7
        0x9b -> :sswitch_15
        0x9f -> :sswitch_b
        0xb0 -> :sswitch_4
        0xb3 -> :sswitch_12
        0xba -> :sswitch_5
        0xd7 -> :sswitch_6
        0xe7 -> :sswitch_14
        0xf1 -> :sswitch_13
        0xfb -> :sswitch_c
        0x4254 -> :sswitch_f
        0x4285 -> :sswitch_1
        0x42f7 -> :sswitch_0
        0x47e1 -> :sswitch_10
        0x47e8 -> :sswitch_11
        0x5031 -> :sswitch_d
        0x5032 -> :sswitch_e
        0x53ac -> :sswitch_2
        0x56aa -> :sswitch_9
        0x56bb -> :sswitch_a
        0x23e383 -> :sswitch_8
        0x2ad7b1 -> :sswitch_3
    .end sparse-switch
.end method

.method a(IJJ)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const-wide/16 v4, -0x1

    const/4 v2, 0x1

    .line 359
    sparse-switch p1, :sswitch_data_0

    .line 405
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 361
    :sswitch_1
    iget-wide v0, p0, Lcom/google/android/a/c/e/f;->m:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/google/android/a/c/e/f;->m:J

    cmp-long v0, v0, p2

    if-eqz v0, :cond_1

    .line 362
    new-instance v0, Lcom/google/android/a/q;

    const-string v1, "Multiple Segment elements not supported"

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 364
    :cond_1
    iput-wide p2, p0, Lcom/google/android/a/c/e/f;->m:J

    .line 365
    iput-wide p4, p0, Lcom/google/android/a/c/e/f;->n:J

    goto :goto_0

    .line 368
    :sswitch_2
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/a/c/e/f;->u:I

    .line 369
    iput-wide v4, p0, Lcom/google/android/a/c/e/f;->v:J

    goto :goto_0

    .line 372
    :sswitch_3
    new-instance v0, Lcom/google/android/a/f/e;

    invoke-direct {v0}, Lcom/google/android/a/f/e;-><init>()V

    iput-object v0, p0, Lcom/google/android/a/c/e/f;->A:Lcom/google/android/a/f/e;

    .line 373
    new-instance v0, Lcom/google/android/a/f/e;

    invoke-direct {v0}, Lcom/google/android/a/f/e;-><init>()V

    iput-object v0, p0, Lcom/google/android/a/c/e/f;->B:Lcom/google/android/a/f/e;

    goto :goto_0

    .line 376
    :sswitch_4
    iput-boolean v0, p0, Lcom/google/android/a/c/e/f;->C:Z

    goto :goto_0

    .line 379
    :sswitch_5
    iget-boolean v0, p0, Lcom/google/android/a/c/e/f;->t:Z

    if-nez v0, :cond_0

    .line 381
    iget-wide v0, p0, Lcom/google/android/a/c/e/f;->x:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 383
    iput-boolean v2, p0, Lcom/google/android/a/c/e/f;->w:Z

    goto :goto_0

    .line 387
    :cond_2
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->S:Lcom/google/android/a/c/g;

    sget-object v1, Lcom/google/android/a/c/l;->f:Lcom/google/android/a/c/l;

    invoke-interface {v0, v1}, Lcom/google/android/a/c/g;->a(Lcom/google/android/a/c/l;)V

    .line 388
    iput-boolean v2, p0, Lcom/google/android/a/c/e/f;->t:Z

    goto :goto_0

    .line 393
    :sswitch_6
    iput-boolean v0, p0, Lcom/google/android/a/c/e/f;->R:Z

    goto :goto_0

    .line 399
    :sswitch_7
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    iput-boolean v2, v0, Lcom/google/android/a/c/e/f$b;->e:Z

    goto :goto_0

    .line 402
    :sswitch_8
    new-instance v0, Lcom/google/android/a/c/e/f$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/a/c/e/f$b;-><init>(Lcom/google/android/a/c/e/f$1;)V

    iput-object v0, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    goto :goto_0

    .line 359
    nop

    :sswitch_data_0
    .sparse-switch
        0xa0 -> :sswitch_6
        0xae -> :sswitch_8
        0xbb -> :sswitch_4
        0x4dbb -> :sswitch_2
        0x5035 -> :sswitch_7
        0x6240 -> :sswitch_0
        0x18538067 -> :sswitch_1
        0x1c53bb6b -> :sswitch_3
        0x1f43b675 -> :sswitch_5
    .end sparse-switch
.end method

.method a(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 601
    sparse-switch p1, :sswitch_data_0

    .line 615
    :cond_0
    :goto_0
    return-void

    .line 604
    :sswitch_0
    const-string v0, "webm"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "matroska"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 605
    new-instance v0, Lcom/google/android/a/q;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DocType "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 609
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    iput-object p2, v0, Lcom/google/android/a/c/e/f$b;->a:Ljava/lang/String;

    goto :goto_0

    .line 612
    :sswitch_2
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    invoke-static {v0, p2}, Lcom/google/android/a/c/e/f$b;->a(Lcom/google/android/a/c/e/f$b;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 601
    :sswitch_data_0
    .sparse-switch
        0x86 -> :sswitch_1
        0x4282 -> :sswitch_0
        0x22b59c -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Lcom/google/android/a/c/g;)V
    .locals 0

    .prologue
    .line 263
    iput-object p1, p0, Lcom/google/android/a/c/e/f;->S:Lcom/google/android/a/c/g;

    .line 264
    return-void
.end method

.method public a(Lcom/google/android/a/c/f;)Z
    .locals 1

    .prologue
    .line 258
    new-instance v0, Lcom/google/android/a/c/e/d;

    invoke-direct {v0}, Lcom/google/android/a/c/e/d;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/android/a/c/e/d;->a(Lcom/google/android/a/c/f;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 268
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/a/c/e/f;->z:J

    .line 269
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/a/c/e/f;->D:I

    .line 270
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->c:Lcom/google/android/a/c/e/b;

    invoke-interface {v0}, Lcom/google/android/a/c/e/b;->a()V

    .line 271
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->d:Lcom/google/android/a/c/e/e;

    invoke-virtual {v0}, Lcom/google/android/a/c/e/e;->a()V

    .line 272
    invoke-direct {p0}, Lcom/google/android/a/c/e/f;->a()V

    .line 273
    return-void
.end method

.method b(I)Z
    .locals 1

    .prologue
    .line 354
    const v0, 0x1549a966

    if-eq p1, v0, :cond_0

    const v0, 0x1f43b675

    if-eq p1, v0, :cond_0

    const v0, 0x1c53bb6b

    if-eq p1, v0, :cond_0

    const v0, 0x1654ae6b

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method c(I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const-wide/16 v2, -0x1

    .line 410
    sparse-switch p1, :sswitch_data_0

    .line 481
    :cond_0
    :goto_0
    return-void

    .line 412
    :sswitch_0
    iget-wide v0, p0, Lcom/google/android/a/c/e/f;->o:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 414
    const-wide/32 v0, 0xf4240

    iput-wide v0, p0, Lcom/google/android/a/c/e/f;->o:J

    .line 416
    :cond_1
    iget-wide v0, p0, Lcom/google/android/a/c/e/f;->p:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 417
    iget-wide v0, p0, Lcom/google/android/a/c/e/f;->p:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/a/c/e/f;->a(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/a/c/e/f;->q:J

    goto :goto_0

    .line 421
    :sswitch_1
    iget v0, p0, Lcom/google/android/a/c/e/f;->u:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    iget-wide v0, p0, Lcom/google/android/a/c/e/f;->v:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    .line 422
    :cond_2
    new-instance v0, Lcom/google/android/a/q;

    const-string v1, "Mandatory element SeekID or SeekPosition not found"

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 424
    :cond_3
    iget v0, p0, Lcom/google/android/a/c/e/f;->u:I

    const v1, 0x1c53bb6b

    if-ne v0, v1, :cond_0

    .line 425
    iget-wide v0, p0, Lcom/google/android/a/c/e/f;->v:J

    iput-wide v0, p0, Lcom/google/android/a/c/e/f;->x:J

    goto :goto_0

    .line 429
    :sswitch_2
    iget-boolean v0, p0, Lcom/google/android/a/c/e/f;->t:Z

    if-nez v0, :cond_0

    .line 430
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->S:Lcom/google/android/a/c/g;

    invoke-direct {p0}, Lcom/google/android/a/c/e/f;->c()Lcom/google/android/a/c/l;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/a/c/g;->a(Lcom/google/android/a/c/l;)V

    .line 431
    iput-boolean v5, p0, Lcom/google/android/a/c/e/f;->t:Z

    goto :goto_0

    .line 437
    :sswitch_3
    iget v0, p0, Lcom/google/android/a/c/e/f;->D:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 442
    iget-boolean v0, p0, Lcom/google/android/a/c/e/f;->R:Z

    if-nez v0, :cond_4

    .line 443
    iget v0, p0, Lcom/google/android/a/c/e/f;->L:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/a/c/e/f;->L:I

    .line 445
    :cond_4
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->e:Landroid/util/SparseArray;

    iget v1, p0, Lcom/google/android/a/c/e/f;->J:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/a/c/e/f$b;

    iget-wide v2, p0, Lcom/google/android/a/c/e/f;->E:J

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/a/c/e/f;->a(Lcom/google/android/a/c/e/f$b;J)V

    .line 446
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/a/c/e/f;->D:I

    goto :goto_0

    .line 449
    :sswitch_4
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    iget-boolean v0, v0, Lcom/google/android/a/c/e/f$b;->e:Z

    if-eqz v0, :cond_0

    .line 450
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    iget-object v0, v0, Lcom/google/android/a/c/e/f$b;->g:[B

    if-nez v0, :cond_5

    .line 451
    new-instance v0, Lcom/google/android/a/q;

    const-string v1, "Encrypted Track found but ContentEncKeyID was not found"

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 453
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/a/c/e/f;->s:Z

    if-nez v0, :cond_0

    .line 454
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->S:Lcom/google/android/a/c/g;

    new-instance v1, Lcom/google/android/a/b/a$c;

    new-instance v2, Lcom/google/android/a/b/a$b;

    const-string v3, "video/webm"

    iget-object v4, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    iget-object v4, v4, Lcom/google/android/a/c/e/f$b;->g:[B

    invoke-direct {v2, v3, v4}, Lcom/google/android/a/b/a$b;-><init>(Ljava/lang/String;[B)V

    invoke-direct {v1, v2}, Lcom/google/android/a/b/a$c;-><init>(Lcom/google/android/a/b/a$b;)V

    invoke-interface {v0, v1}, Lcom/google/android/a/c/g;->a(Lcom/google/android/a/b/a;)V

    .line 456
    iput-boolean v5, p0, Lcom/google/android/a/c/e/f;->s:Z

    goto/16 :goto_0

    .line 461
    :sswitch_5
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    iget-boolean v0, v0, Lcom/google/android/a/c/e/f$b;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    iget-object v0, v0, Lcom/google/android/a/c/e/f$b;->f:[B

    if-eqz v0, :cond_0

    .line 462
    new-instance v0, Lcom/google/android/a/q;

    const-string v1, "Combining encryption and compression is not supported"

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 466
    :sswitch_6
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->e:Landroid/util/SparseArray;

    iget-object v1, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    iget v1, v1, Lcom/google/android/a/c/e/f$b;->b:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    iget-object v0, v0, Lcom/google/android/a/c/e/f$b;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/a/c/e/f;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 467
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    iget-object v1, p0, Lcom/google/android/a/c/e/f;->S:Lcom/google/android/a/c/g;

    iget-object v2, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    iget v2, v2, Lcom/google/android/a/c/e/f$b;->b:I

    iget-wide v4, p0, Lcom/google/android/a/c/e/f;->q:J

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/google/android/a/c/e/f$b;->a(Lcom/google/android/a/c/g;IJ)V

    .line 468
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->e:Landroid/util/SparseArray;

    iget-object v1, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    iget v1, v1, Lcom/google/android/a/c/e/f$b;->b:I

    iget-object v2, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 472
    :cond_6
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/a/c/e/f;->r:Lcom/google/android/a/c/e/f$b;

    goto/16 :goto_0

    .line 475
    :sswitch_7
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_7

    .line 476
    new-instance v0, Lcom/google/android/a/q;

    const-string v1, "No valid tracks were found"

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 478
    :cond_7
    iget-object v0, p0, Lcom/google/android/a/c/e/f;->S:Lcom/google/android/a/c/g;

    invoke-interface {v0}, Lcom/google/android/a/c/g;->f()V

    goto/16 :goto_0

    .line 410
    nop

    :sswitch_data_0
    .sparse-switch
        0xa0 -> :sswitch_3
        0xae -> :sswitch_6
        0x4dbb -> :sswitch_1
        0x6240 -> :sswitch_4
        0x6d80 -> :sswitch_5
        0x1549a966 -> :sswitch_0
        0x1654ae6b -> :sswitch_7
        0x1c53bb6b -> :sswitch_2
    .end sparse-switch
.end method
