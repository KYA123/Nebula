.class final Lcom/google/android/a/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Landroid/os/HandlerThread;

.field private final c:Landroid/os/Handler;

.field private final d:Lcom/google/android/a/u;

.field private final e:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/a/v;",
            ">;"
        }
    .end annotation
.end field

.field private final g:[[Lcom/google/android/a/o;

.field private final h:[I

.field private final i:J

.field private final j:J

.field private k:[Lcom/google/android/a/v;

.field private l:Lcom/google/android/a/v;

.field private m:Lcom/google/android/a/i;

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:I

.field private r:I

.field private s:I

.field private t:J

.field private u:J

.field private volatile v:J

.field private volatile w:J

.field private volatile x:J


# direct methods
.method public constructor <init>(Landroid/os/Handler;Z[III)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x3e8

    const-wide/16 v2, -0x1

    const/4 v0, 0x0

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput v0, p0, Lcom/google/android/a/h;->r:I

    .line 86
    iput v0, p0, Lcom/google/android/a/h;->s:I

    .line 96
    iput-object p1, p0, Lcom/google/android/a/h;->c:Landroid/os/Handler;

    .line 97
    iput-boolean p2, p0, Lcom/google/android/a/h;->o:Z

    .line 98
    int-to-long v0, p4

    mul-long/2addr v0, v4

    iput-wide v0, p0, Lcom/google/android/a/h;->i:J

    .line 99
    int-to-long v0, p5

    mul-long/2addr v0, v4

    iput-wide v0, p0, Lcom/google/android/a/h;->j:J

    .line 100
    array-length v0, p3

    invoke-static {p3, v0}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/a/h;->h:[I

    .line 101
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/a/h;->q:I

    .line 102
    iput-wide v2, p0, Lcom/google/android/a/h;->v:J

    .line 103
    iput-wide v2, p0, Lcom/google/android/a/h;->x:J

    .line 105
    new-instance v0, Lcom/google/android/a/u;

    invoke-direct {v0}, Lcom/google/android/a/u;-><init>()V

    iput-object v0, p0, Lcom/google/android/a/h;->d:Lcom/google/android/a/u;

    .line 106
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/google/android/a/h;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 107
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/a/h;->f:Ljava/util/List;

    .line 108
    array-length v0, p3

    new-array v0, v0, [[Lcom/google/android/a/o;

    iput-object v0, p0, Lcom/google/android/a/h;->g:[[Lcom/google/android/a/o;

    .line 111
    new-instance v0, Lcom/google/android/a/f/l;

    const-string v1, "ExoPlayerImplInternal:Handler"

    const/16 v2, -0x10

    invoke-direct {v0, v1, v2}, Lcom/google/android/a/f/l;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/a/h;->b:Landroid/os/HandlerThread;

    .line 113
    iget-object v0, p0, Lcom/google/android/a/h;->b:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 114
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/a/h;->b:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/a/h;->a:Landroid/os/Handler;

    .line 115
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 253
    iget v0, p0, Lcom/google/android/a/h;->q:I

    if-eq v0, p1, :cond_0

    .line 254
    iput p1, p0, Lcom/google/android/a/h;->q:I

    .line 255
    iget-object v0, p0, Lcom/google/android/a/h;->c:Landroid/os/Handler;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 257
    :cond_0
    return-void
.end method

.method private a(II)V
    .locals 8

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 604
    iget-object v2, p0, Lcom/google/android/a/h;->h:[I

    aget v2, v2, p1

    if-ne v2, p2, :cond_1

    .line 652
    :cond_0
    :goto_0
    return-void

    .line 608
    :cond_1
    iget-object v2, p0, Lcom/google/android/a/h;->h:[I

    aput p2, v2, p1

    .line 609
    iget v2, p0, Lcom/google/android/a/h;->q:I

    if-eq v2, v0, :cond_0

    iget v2, p0, Lcom/google/android/a/h;->q:I

    if-eq v2, v5, :cond_0

    .line 613
    iget-object v2, p0, Lcom/google/android/a/h;->k:[Lcom/google/android/a/v;

    aget-object v4, v2, p1

    .line 614
    invoke-virtual {v4}, Lcom/google/android/a/v;->u()I

    move-result v2

    .line 615
    if-eqz v2, :cond_0

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    invoke-virtual {v4}, Lcom/google/android/a/v;->t()I

    move-result v3

    if-eqz v3, :cond_0

    .line 621
    if-eq v2, v5, :cond_2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_6

    :cond_2
    move v3, v0

    .line 623
    :goto_1
    if-ltz p2, :cond_7

    iget-object v2, p0, Lcom/google/android/a/h;->g:[[Lcom/google/android/a/o;

    aget-object v2, v2, p1

    array-length v2, v2

    if-ge p2, v2, :cond_7

    move v2, v0

    .line 625
    :goto_2
    if-eqz v3, :cond_4

    .line 629
    if-nez v2, :cond_3

    iget-object v5, p0, Lcom/google/android/a/h;->l:Lcom/google/android/a/v;

    if-ne v4, v5, :cond_3

    .line 633
    iget-object v5, p0, Lcom/google/android/a/h;->d:Lcom/google/android/a/u;

    iget-object v6, p0, Lcom/google/android/a/h;->m:Lcom/google/android/a/i;

    invoke-interface {v6}, Lcom/google/android/a/i;->a()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/google/android/a/u;->a(J)V

    .line 635
    :cond_3
    invoke-direct {p0, v4}, Lcom/google/android/a/h;->d(Lcom/google/android/a/v;)V

    .line 636
    iget-object v5, p0, Lcom/google/android/a/h;->f:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 637
    invoke-virtual {v4}, Lcom/google/android/a/v;->x()V

    .line 640
    :cond_4
    if-eqz v2, :cond_0

    .line 642
    iget-boolean v2, p0, Lcom/google/android/a/h;->o:Z

    if-eqz v2, :cond_8

    iget v2, p0, Lcom/google/android/a/h;->q:I

    const/4 v5, 0x4

    if-ne v2, v5, :cond_8

    move v2, v0

    .line 644
    :goto_3
    if-nez v3, :cond_9

    if-eqz v2, :cond_9

    .line 645
    :goto_4
    iget-wide v6, p0, Lcom/google/android/a/h;->w:J

    invoke-virtual {v4, p2, v6, v7, v0}, Lcom/google/android/a/v;->b(IJZ)V

    .line 646
    iget-object v0, p0, Lcom/google/android/a/h;->f:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 647
    if-eqz v2, :cond_5

    .line 648
    invoke-virtual {v4}, Lcom/google/android/a/v;->v()V

    .line 650
    :cond_5
    iget-object v0, p0, Lcom/google/android/a/h;->a:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_6
    move v3, v1

    .line 621
    goto :goto_1

    :cond_7
    move v2, v1

    .line 623
    goto :goto_2

    :cond_8
    move v2, v1

    .line 642
    goto :goto_3

    :cond_9
    move v0, v1

    .line 644
    goto :goto_4
.end method

.method private a(IJJ)V
    .locals 4

    .prologue
    .line 489
    add-long v0, p2, p4

    .line 490
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 491
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    .line 492
    iget-object v0, p0, Lcom/google/android/a/h;->a:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 496
    :goto_0
    return-void

    .line 494
    :cond_0
    iget-object v2, p0, Lcom/google/android/a/h;->a:Landroid/os/Handler;

    invoke-virtual {v2, p1, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method private a(ILjava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 588
    :try_start_0
    check-cast p2, Landroid/util/Pair;

    .line 589
    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/a/f$a;

    iget-object v1, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-interface {v0, p1, v1}, Lcom/google/android/a/f$a;->a(ILjava/lang/Object;)V

    .line 590
    iget v0, p0, Lcom/google/android/a/h;->q:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/a/h;->q:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 592
    iget-object v0, p0, Lcom/google/android/a/h;->a:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 595
    :cond_0
    monitor-enter p0

    .line 596
    :try_start_1
    iget v0, p0, Lcom/google/android/a/h;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/a/h;->s:I

    .line 597
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 598
    monitor-exit p0

    .line 600
    return-void

    .line 598
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 595
    :catchall_1
    move-exception v0

    monitor-enter p0

    .line 596
    :try_start_2
    iget v1, p0, Lcom/google/android/a/h;->s:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/a/h;->s:I

    .line 597
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 598
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    throw v0

    :catchall_2
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    throw v0
.end method

.method private a(Lcom/google/android/a/v;)Z
    .locals 12

    .prologue
    const-wide/16 v10, -0x1

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 351
    invoke-virtual {p1}, Lcom/google/android/a/v;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 363
    :cond_0
    :goto_0
    return v1

    .line 354
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/a/v;->f()Z

    move-result v2

    if-nez v2, :cond_2

    move v1, v0

    .line 355
    goto :goto_0

    .line 357
    :cond_2
    iget v2, p0, Lcom/google/android/a/h;->q:I

    const/4 v3, 0x4

    if-eq v2, v3, :cond_0

    .line 360
    invoke-virtual {p1}, Lcom/google/android/a/v;->q()J

    move-result-wide v4

    .line 361
    invoke-virtual {p1}, Lcom/google/android/a/v;->p()J

    move-result-wide v6

    .line 362
    iget-boolean v2, p0, Lcom/google/android/a/h;->p:Z

    if-eqz v2, :cond_5

    iget-wide v2, p0, Lcom/google/android/a/h;->j:J

    .line 363
    :goto_1
    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-lez v8, :cond_3

    cmp-long v8, v6, v10

    if-eqz v8, :cond_3

    const-wide/16 v8, -0x3

    cmp-long v8, v6, v8

    if-eqz v8, :cond_3

    iget-wide v8, p0, Lcom/google/android/a/h;->w:J

    add-long/2addr v2, v8

    cmp-long v2, v6, v2

    if-gez v2, :cond_3

    cmp-long v2, v4, v10

    if-eqz v2, :cond_4

    const-wide/16 v2, -0x2

    cmp-long v2, v4, v2

    if-eqz v2, :cond_4

    cmp-long v2, v6, v4

    if-ltz v2, :cond_4

    :cond_3
    move v0, v1

    :cond_4
    move v1, v0

    goto :goto_0

    .line 362
    :cond_5
    iget-wide v2, p0, Lcom/google/android/a/h;->i:J

    goto :goto_1
.end method

.method private b(J)V
    .locals 9

    .prologue
    const-wide/16 v6, 0x3e8

    const/4 v0, 0x0

    .line 500
    :try_start_0
    iget-wide v2, p0, Lcom/google/android/a/h;->w:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v1, p1, v2

    if-nez v1, :cond_0

    .line 520
    iget-object v0, p0, Lcom/google/android/a/h;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 522
    :goto_0
    return-void

    .line 505
    :cond_0
    const/4 v1, 0x0

    :try_start_1
    iput-boolean v1, p0, Lcom/google/android/a/h;->p:Z

    .line 506
    mul-long v2, p1, v6

    iput-wide v2, p0, Lcom/google/android/a/h;->w:J

    .line 507
    iget-object v1, p0, Lcom/google/android/a/h;->d:Lcom/google/android/a/u;

    invoke-virtual {v1}, Lcom/google/android/a/u;->c()V

    .line 508
    iget-object v1, p0, Lcom/google/android/a/h;->d:Lcom/google/android/a/u;

    iget-wide v2, p0, Lcom/google/android/a/h;->w:J

    invoke-virtual {v1, v2, v3}, Lcom/google/android/a/u;->a(J)V

    .line 509
    iget v1, p0, Lcom/google/android/a/h;->q:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/google/android/a/h;->q:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 520
    :cond_1
    iget-object v0, p0, Lcom/google/android/a/h;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    goto :goto_0

    :cond_2
    move v1, v0

    .line 512
    :goto_1
    :try_start_2
    iget-object v0, p0, Lcom/google/android/a/h;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 513
    iget-object v0, p0, Lcom/google/android/a/h;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/a/v;

    .line 514
    invoke-direct {p0, v0}, Lcom/google/android/a/h;->d(Lcom/google/android/a/v;)V

    .line 515
    iget-wide v2, p0, Lcom/google/android/a/h;->w:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/a/v;->e(J)V

    .line 512
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 517
    :cond_3
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/a/h;->a(I)V

    .line 518
    iget-object v0, p0, Lcom/google/android/a/h;->a:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 520
    iget-object v0, p0, Lcom/google/android/a/h;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/a/h;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    throw v0
.end method

.method private b(Lcom/google/android/a/v;)V
    .locals 3

    .prologue
    .line 559
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/a/h;->d(Lcom/google/android/a/v;)V

    .line 560
    invoke-virtual {p1}, Lcom/google/android/a/v;->u()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 561
    invoke-virtual {p1}, Lcom/google/android/a/v;->x()V
    :try_end_0
    .catch Lcom/google/android/a/e; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 570
    :cond_0
    :goto_0
    return-void

    .line 563
    :catch_0
    move-exception v0

    .line 565
    const-string v1, "ExoPlayerImplInternal"

    const-string v2, "Stop failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 566
    :catch_1
    move-exception v0

    .line 568
    const-string v1, "ExoPlayerImplInternal"

    const-string v2, "Stop failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private b(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 374
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/a/h;->p:Z

    .line 375
    iput-boolean p1, p0, Lcom/google/android/a/h;->o:Z

    .line 376
    if-nez p1, :cond_1

    .line 377
    invoke-direct {p0}, Lcom/google/android/a/h;->g()V

    .line 378
    invoke-direct {p0}, Lcom/google/android/a/h;->h()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 388
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/a/h;->c:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 390
    return-void

    .line 380
    :cond_1
    :try_start_1
    iget v0, p0, Lcom/google/android/a/h;->q:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 381
    invoke-direct {p0}, Lcom/google/android/a/h;->f()V

    .line 382
    iget-object v0, p0, Lcom/google/android/a/h;->a:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 388
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/a/h;->c:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    throw v0

    .line 383
    :cond_2
    :try_start_2
    iget v0, p0, Lcom/google/android/a/h;->q:I

    if-ne v0, v2, :cond_0

    .line 384
    iget-object v0, p0, Lcom/google/android/a/h;->a:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private b([Lcom/google/android/a/v;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 260
    invoke-direct {p0}, Lcom/google/android/a/h;->l()V

    .line 261
    iput-object p1, p0, Lcom/google/android/a/h;->k:[Lcom/google/android/a/v;

    .line 262
    iget-object v0, p0, Lcom/google/android/a/h;->g:[[Lcom/google/android/a/o;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    move v0, v1

    .line 263
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_2

    .line 264
    aget-object v2, p1, v0

    invoke-virtual {v2}, Lcom/google/android/a/v;->b()Lcom/google/android/a/i;

    move-result-object v3

    .line 265
    if-eqz v3, :cond_0

    .line 266
    iget-object v2, p0, Lcom/google/android/a/h;->m:Lcom/google/android/a/i;

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, Lcom/google/android/a/f/b;->b(Z)V

    .line 267
    iput-object v3, p0, Lcom/google/android/a/h;->m:Lcom/google/android/a/i;

    .line 268
    aget-object v2, p1, v0

    iput-object v2, p0, Lcom/google/android/a/h;->l:Lcom/google/android/a/v;

    .line 263
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v2, v1

    .line 266
    goto :goto_1

    .line 271
    :cond_2
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/a/h;->a(I)V

    .line 272
    invoke-direct {p0}, Lcom/google/android/a/h;->e()V

    .line 273
    return-void
.end method

.method private c(Lcom/google/android/a/v;)V
    .locals 3

    .prologue
    .line 574
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/a/v;->y()V
    :try_end_0
    .catch Lcom/google/android/a/e; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 582
    :goto_0
    return-void

    .line 575
    :catch_0
    move-exception v0

    .line 577
    const-string v1, "ExoPlayerImplInternal"

    const-string v2, "Release failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 578
    :catch_1
    move-exception v0

    .line 580
    const-string v1, "ExoPlayerImplInternal"

    const-string v2, "Release failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private d(Lcom/google/android/a/v;)V
    .locals 2

    .prologue
    .line 655
    invoke-virtual {p1}, Lcom/google/android/a/v;->u()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 656
    invoke-virtual {p1}, Lcom/google/android/a/v;->w()V

    .line 658
    :cond_0
    return-void
.end method

.method private e()V
    .locals 14

    .prologue
    .line 276
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 277
    const/4 v1, 0x1

    .line 278
    const/4 v0, 0x0

    :goto_0
    iget-object v4, p0, Lcom/google/android/a/h;->k:[Lcom/google/android/a/v;

    array-length v4, v4

    if-ge v0, v4, :cond_1

    .line 279
    iget-object v4, p0, Lcom/google/android/a/h;->k:[Lcom/google/android/a/v;

    aget-object v4, v4, v0

    .line 280
    invoke-virtual {v4}, Lcom/google/android/a/v;->u()I

    move-result v5

    if-nez v5, :cond_0

    .line 281
    iget-wide v6, p0, Lcom/google/android/a/h;->w:J

    invoke-virtual {v4, v6, v7}, Lcom/google/android/a/v;->g(J)I

    move-result v5

    .line 282
    if-nez v5, :cond_0

    .line 283
    invoke-virtual {v4}, Lcom/google/android/a/v;->r()V

    .line 284
    const/4 v1, 0x0

    .line 278
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 289
    :cond_1
    if-nez v1, :cond_2

    .line 291
    const/4 v1, 0x2

    const-wide/16 v4, 0xa

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/a/h;->a(IJJ)V

    .line 348
    :goto_1
    return-void

    .line 295
    :cond_2
    const-wide/16 v4, 0x0

    .line 296
    const/4 v2, 0x1

    .line 297
    const/4 v1, 0x1

    .line 298
    const/4 v0, 0x0

    :goto_2
    iget-object v3, p0, Lcom/google/android/a/h;->k:[Lcom/google/android/a/v;

    array-length v3, v3

    if-ge v0, v3, :cond_a

    .line 299
    iget-object v3, p0, Lcom/google/android/a/h;->k:[Lcom/google/android/a/v;

    aget-object v6, v3, v0

    .line 300
    invoke-virtual {v6}, Lcom/google/android/a/v;->t()I

    move-result v7

    .line 301
    new-array v8, v7, [Lcom/google/android/a/o;

    .line 302
    const/4 v3, 0x0

    :goto_3
    if-ge v3, v7, :cond_3

    .line 303
    invoke-virtual {v6, v3}, Lcom/google/android/a/v;->b(I)Lcom/google/android/a/o;

    move-result-object v9

    aput-object v9, v8, v3

    .line 302
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 305
    :cond_3
    iget-object v3, p0, Lcom/google/android/a/h;->g:[[Lcom/google/android/a/o;

    aput-object v8, v3, v0

    .line 306
    if-lez v7, :cond_5

    .line 307
    const-wide/16 v10, -0x1

    cmp-long v3, v4, v10

    if-nez v3, :cond_6

    .line 320
    :cond_4
    :goto_4
    iget-object v3, p0, Lcom/google/android/a/h;->h:[I

    aget v3, v3, v0

    .line 321
    if-ltz v3, :cond_5

    array-length v7, v8

    if-ge v3, v7, :cond_5

    .line 322
    iget-wide v8, p0, Lcom/google/android/a/h;->w:J

    const/4 v7, 0x0

    invoke-virtual {v6, v3, v8, v9, v7}, Lcom/google/android/a/v;->b(IJZ)V

    .line 323
    iget-object v3, p0, Lcom/google/android/a/h;->f:Ljava/util/List;

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 324
    if-eqz v2, :cond_8

    invoke-virtual {v6}, Lcom/google/android/a/v;->e()Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    .line 325
    :goto_5
    if-eqz v1, :cond_9

    invoke-direct {p0, v6}, Lcom/google/android/a/h;->a(Lcom/google/android/a/v;)Z

    move-result v1

    if-eqz v1, :cond_9

    const/4 v1, 0x1

    .line 298
    :cond_5
    :goto_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 311
    :cond_6
    invoke-virtual {v6}, Lcom/google/android/a/v;->q()J

    move-result-wide v10

    .line 312
    const-wide/16 v12, -0x1

    cmp-long v3, v10, v12

    if-nez v3, :cond_7

    .line 313
    const-wide/16 v4, -0x1

    goto :goto_4

    .line 314
    :cond_7
    const-wide/16 v12, -0x2

    cmp-long v3, v10, v12

    if-eqz v3, :cond_4

    .line 317
    invoke-static {v4, v5, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    goto :goto_4

    .line 324
    :cond_8
    const/4 v2, 0x0

    goto :goto_5

    .line 325
    :cond_9
    const/4 v1, 0x0

    goto :goto_6

    .line 329
    :cond_a
    iput-wide v4, p0, Lcom/google/android/a/h;->v:J

    .line 331
    if-eqz v2, :cond_d

    const-wide/16 v2, -0x1

    cmp-long v0, v4, v2

    if-eqz v0, :cond_b

    iget-wide v2, p0, Lcom/google/android/a/h;->w:J

    cmp-long v0, v4, v2

    if-gtz v0, :cond_d

    .line 334
    :cond_b
    const/4 v0, 0x5

    iput v0, p0, Lcom/google/android/a/h;->q:I

    .line 341
    :goto_7
    iget-object v0, p0, Lcom/google/android/a/h;->c:Landroid/os/Handler;

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/a/h;->q:I

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/a/h;->g:[[Lcom/google/android/a/o;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 344
    iget-boolean v0, p0, Lcom/google/android/a/h;->o:Z

    if-eqz v0, :cond_c

    iget v0, p0, Lcom/google/android/a/h;->q:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_c

    .line 345
    invoke-direct {p0}, Lcom/google/android/a/h;->f()V

    .line 347
    :cond_c
    iget-object v0, p0, Lcom/google/android/a/h;->a:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_1

    .line 336
    :cond_d
    if-eqz v1, :cond_e

    const/4 v0, 0x4

    :goto_8
    iput v0, p0, Lcom/google/android/a/h;->q:I

    goto :goto_7

    :cond_e
    const/4 v0, 0x3

    goto :goto_8
.end method

.method private f()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 393
    iput-boolean v0, p0, Lcom/google/android/a/h;->p:Z

    .line 394
    iget-object v1, p0, Lcom/google/android/a/h;->d:Lcom/google/android/a/u;

    invoke-virtual {v1}, Lcom/google/android/a/u;->b()V

    move v1, v0

    .line 395
    :goto_0
    iget-object v0, p0, Lcom/google/android/a/h;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 396
    iget-object v0, p0, Lcom/google/android/a/h;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/a/v;

    invoke-virtual {v0}, Lcom/google/android/a/v;->v()V

    .line 395
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 398
    :cond_0
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 401
    iget-object v0, p0, Lcom/google/android/a/h;->d:Lcom/google/android/a/u;

    invoke-virtual {v0}, Lcom/google/android/a/u;->c()V

    .line 402
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/a/h;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 403
    iget-object v0, p0, Lcom/google/android/a/h;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/a/v;

    invoke-direct {p0, v0}, Lcom/google/android/a/h;->d(Lcom/google/android/a/v;)V

    .line 402
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 405
    :cond_0
    return-void
.end method

.method private h()V
    .locals 4

    .prologue
    .line 408
    iget-object v0, p0, Lcom/google/android/a/h;->m:Lcom/google/android/a/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/a/h;->f:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/a/h;->l:Lcom/google/android/a/v;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/a/h;->l:Lcom/google/android/a/v;

    invoke-virtual {v0}, Lcom/google/android/a/v;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 410
    iget-object v0, p0, Lcom/google/android/a/h;->m:Lcom/google/android/a/i;

    invoke-interface {v0}, Lcom/google/android/a/i;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/a/h;->w:J

    .line 411
    iget-object v0, p0, Lcom/google/android/a/h;->d:Lcom/google/android/a/u;

    iget-wide v2, p0, Lcom/google/android/a/h;->w:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/a/u;->a(J)V

    .line 415
    :goto_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/a/h;->u:J

    .line 416
    return-void

    .line 413
    :cond_0
    iget-object v0, p0, Lcom/google/android/a/h;->d:Lcom/google/android/a/u;

    invoke-virtual {v0}, Lcom/google/android/a/u;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/a/h;->w:J

    goto :goto_0
.end method

.method private i()V
    .locals 15

    .prologue
    .line 419
    const-string v0, "doSomeWork"

    invoke-static {v0}, Lcom/google/android/a/f/m;->a(Ljava/lang/String;)V

    .line 420
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 421
    iget-wide v0, p0, Lcom/google/android/a/h;->v:J

    const-wide/16 v4, -0x1

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    iget-wide v0, p0, Lcom/google/android/a/h;->v:J

    .line 423
    :goto_0
    const/4 v6, 0x1

    .line 424
    const/4 v5, 0x1

    .line 425
    invoke-direct {p0}, Lcom/google/android/a/h;->h()V

    .line 426
    const/4 v4, 0x0

    move v14, v4

    move v4, v5

    move v5, v6

    move-wide v6, v0

    move v1, v14

    :goto_1
    iget-object v0, p0, Lcom/google/android/a/h;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 427
    iget-object v0, p0, Lcom/google/android/a/h;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/a/v;

    .line 431
    iget-wide v8, p0, Lcom/google/android/a/h;->w:J

    iget-wide v10, p0, Lcom/google/android/a/h;->u:J

    invoke-virtual {v0, v8, v9, v10, v11}, Lcom/google/android/a/v;->a(JJ)V

    .line 432
    if-eqz v5, :cond_3

    invoke-virtual {v0}, Lcom/google/android/a/v;->e()Z

    move-result v5

    if-eqz v5, :cond_3

    const/4 v5, 0x1

    .line 436
    :goto_2
    invoke-direct {p0, v0}, Lcom/google/android/a/h;->a(Lcom/google/android/a/v;)Z

    move-result v8

    .line 437
    if-nez v8, :cond_0

    .line 438
    invoke-virtual {v0}, Lcom/google/android/a/v;->r()V

    .line 440
    :cond_0
    if-eqz v4, :cond_4

    if-eqz v8, :cond_4

    const/4 v4, 0x1

    .line 442
    :goto_3
    const-wide/16 v8, -0x1

    cmp-long v8, v6, v8

    if-nez v8, :cond_5

    .line 426
    :cond_1
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 421
    :cond_2
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0

    .line 432
    :cond_3
    const/4 v5, 0x0

    goto :goto_2

    .line 440
    :cond_4
    const/4 v4, 0x0

    goto :goto_3

    .line 446
    :cond_5
    invoke-virtual {v0}, Lcom/google/android/a/v;->q()J

    move-result-wide v8

    .line 447
    invoke-virtual {v0}, Lcom/google/android/a/v;->p()J

    move-result-wide v10

    .line 448
    const-wide/16 v12, -0x1

    cmp-long v0, v10, v12

    if-nez v0, :cond_6

    .line 449
    const-wide/16 v6, -0x1

    goto :goto_4

    .line 450
    :cond_6
    const-wide/16 v12, -0x3

    cmp-long v0, v10, v12

    if-eqz v0, :cond_1

    const-wide/16 v12, -0x1

    cmp-long v0, v8, v12

    if-eqz v0, :cond_7

    const-wide/16 v12, -0x2

    cmp-long v0, v8, v12

    if-eqz v0, :cond_7

    cmp-long v0, v10, v8

    if-gez v0, :cond_1

    .line 456
    :cond_7
    invoke-static {v6, v7, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    goto :goto_4

    .line 460
    :cond_8
    iput-wide v6, p0, Lcom/google/android/a/h;->x:J

    .line 462
    if-eqz v5, :cond_e

    iget-wide v0, p0, Lcom/google/android/a/h;->v:J

    const-wide/16 v6, -0x1

    cmp-long v0, v0, v6

    if-eqz v0, :cond_9

    iget-wide v0, p0, Lcom/google/android/a/h;->v:J

    iget-wide v6, p0, Lcom/google/android/a/h;->w:J

    cmp-long v0, v0, v6

    if-gtz v0, :cond_e

    .line 464
    :cond_9
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/google/android/a/h;->a(I)V

    .line 465
    invoke-direct {p0}, Lcom/google/android/a/h;->g()V

    .line 477
    :cond_a
    :goto_5
    iget-object v0, p0, Lcom/google/android/a/h;->a:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 478
    iget-boolean v0, p0, Lcom/google/android/a/h;->o:Z

    if-eqz v0, :cond_b

    iget v0, p0, Lcom/google/android/a/h;->q:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_c

    :cond_b
    iget v0, p0, Lcom/google/android/a/h;->q:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_10

    .line 479
    :cond_c
    const/4 v1, 0x7

    const-wide/16 v4, 0xa

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/a/h;->a(IJJ)V

    .line 484
    :cond_d
    :goto_6
    invoke-static {}, Lcom/google/android/a/f/m;->a()V

    .line 485
    return-void

    .line 466
    :cond_e
    iget v0, p0, Lcom/google/android/a/h;->q:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_f

    if-eqz v4, :cond_f

    .line 467
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/a/h;->a(I)V

    .line 468
    iget-boolean v0, p0, Lcom/google/android/a/h;->o:Z

    if-eqz v0, :cond_a

    .line 469
    invoke-direct {p0}, Lcom/google/android/a/h;->f()V

    goto :goto_5

    .line 471
    :cond_f
    iget v0, p0, Lcom/google/android/a/h;->q:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_a

    if-nez v4, :cond_a

    .line 472
    iget-boolean v0, p0, Lcom/google/android/a/h;->o:Z

    iput-boolean v0, p0, Lcom/google/android/a/h;->p:Z

    .line 473
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/a/h;->a(I)V

    .line 474
    invoke-direct {p0}, Lcom/google/android/a/h;->g()V

    goto :goto_5

    .line 480
    :cond_10
    iget-object v0, p0, Lcom/google/android/a/h;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    .line 481
    const/4 v1, 0x7

    const-wide/16 v4, 0x3e8

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/a/h;->a(IJJ)V

    goto :goto_6
.end method

.method private j()V
    .locals 1

    .prologue
    .line 525
    invoke-direct {p0}, Lcom/google/android/a/h;->l()V

    .line 526
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/a/h;->a(I)V

    .line 527
    return-void
.end method

.method private k()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 530
    invoke-direct {p0}, Lcom/google/android/a/h;->l()V

    .line 531
    invoke-direct {p0, v0}, Lcom/google/android/a/h;->a(I)V

    .line 532
    monitor-enter p0

    .line 533
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/a/h;->n:Z

    .line 534
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 535
    monitor-exit p0

    .line 536
    return-void

    .line 535
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private l()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 539
    iget-object v1, p0, Lcom/google/android/a/h;->a:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 540
    iget-object v1, p0, Lcom/google/android/a/h;->a:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 541
    iput-boolean v0, p0, Lcom/google/android/a/h;->p:Z

    .line 542
    iget-object v1, p0, Lcom/google/android/a/h;->d:Lcom/google/android/a/u;

    invoke-virtual {v1}, Lcom/google/android/a/u;->c()V

    .line 543
    iget-object v1, p0, Lcom/google/android/a/h;->k:[Lcom/google/android/a/v;

    if-nez v1, :cond_0

    .line 555
    :goto_0
    return-void

    .line 546
    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/google/android/a/h;->k:[Lcom/google/android/a/v;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 547
    iget-object v1, p0, Lcom/google/android/a/h;->k:[Lcom/google/android/a/v;

    aget-object v1, v1, v0

    .line 548
    invoke-direct {p0, v1}, Lcom/google/android/a/h;->b(Lcom/google/android/a/v;)V

    .line 549
    invoke-direct {p0, v1}, Lcom/google/android/a/h;->c(Lcom/google/android/a/v;)V

    .line 546
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 551
    :cond_1
    iput-object v3, p0, Lcom/google/android/a/h;->k:[Lcom/google/android/a/v;

    .line 552
    iput-object v3, p0, Lcom/google/android/a/h;->m:Lcom/google/android/a/i;

    .line 553
    iput-object v3, p0, Lcom/google/android/a/h;->l:Lcom/google/android/a/v;

    .line 554
    iget-object v0, p0, Lcom/google/android/a/h;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 4

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/a/h;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-lez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/a/h;->t:J

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/google/android/a/h;->w:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    goto :goto_0
.end method

.method public a(J)V
    .locals 5

    .prologue
    .line 144
    iput-wide p1, p0, Lcom/google/android/a/h;->t:J

    .line 145
    iget-object v0, p0, Lcom/google/android/a/h;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 146
    iget-object v0, p0, Lcom/google/android/a/h;->a:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-static {p1, p2}, Lcom/google/android/a/f/n;->a(J)I

    move-result v2

    invoke-static {p1, p2}, Lcom/google/android/a/f/n;->b(J)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 148
    return-void
.end method

.method public a(Lcom/google/android/a/f$a;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 160
    iget v0, p0, Lcom/google/android/a/h;->r:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/a/h;->r:I

    .line 161
    iget-object v0, p0, Lcom/google/android/a/h;->a:Landroid/os/Handler;

    const/16 v1, 0x9

    const/4 v2, 0x0

    invoke-static {p1, p3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    invoke-virtual {v0, v1, p2, v2, v3}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 162
    return-void
.end method

.method public a(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 140
    iget-object v2, p0, Lcom/google/android/a/h;->a:Landroid/os/Handler;

    const/4 v3, 0x3

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 141
    return-void

    :cond_0
    move v0, v1

    .line 140
    goto :goto_0
.end method

.method public varargs a([Lcom/google/android/a/v;)V
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/a/h;->a:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 137
    return-void
.end method

.method public b()J
    .locals 4

    .prologue
    const-wide/16 v0, -0x1

    .line 131
    iget-wide v2, p0, Lcom/google/android/a/h;->v:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/google/android/a/h;->v:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/a/h;->a:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 152
    return-void
.end method

.method public declared-synchronized d()V
    .locals 2

    .prologue
    .line 182
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/a/h;->n:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 194
    :goto_0
    monitor-exit p0

    return-void

    .line 185
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/a/h;->a:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 186
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/a/h;->n:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_1

    .line 188
    :try_start_2
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 189
    :catch_0
    move-exception v0

    .line 190
    :try_start_3
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 182
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 193
    :cond_1
    :try_start_4
    iget-object v0, p0, Lcom/google/android/a/h;->b:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 199
    :try_start_0
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 248
    :goto_0
    return v0

    .line 201
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [Lcom/google/android/a/v;

    check-cast v0, [Lcom/google/android/a/v;

    invoke-direct {p0, v0}, Lcom/google/android/a/h;->b([Lcom/google/android/a/v;)V

    move v0, v1

    .line 202
    goto :goto_0

    .line 205
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/a/h;->e()V

    move v0, v1

    .line 206
    goto :goto_0

    .line 209
    :pswitch_2
    iget v2, p1, Landroid/os/Message;->arg1:I

    if-eqz v2, :cond_0

    move v0, v1

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/a/h;->b(Z)V

    move v0, v1

    .line 210
    goto :goto_0

    .line 213
    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/a/h;->i()V

    move v0, v1

    .line 214
    goto :goto_0

    .line 217
    :pswitch_4
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-static {v0, v2}, Lcom/google/android/a/f/n;->b(II)J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/google/android/a/h;->b(J)V

    move v0, v1

    .line 218
    goto :goto_0

    .line 221
    :pswitch_5
    invoke-direct {p0}, Lcom/google/android/a/h;->j()V

    move v0, v1

    .line 222
    goto :goto_0

    .line 225
    :pswitch_6
    invoke-direct {p0}, Lcom/google/android/a/h;->k()V

    move v0, v1

    .line 226
    goto :goto_0

    .line 229
    :pswitch_7
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-direct {p0, v0, v2}, Lcom/google/android/a/h;->a(ILjava/lang/Object;)V

    move v0, v1

    .line 230
    goto :goto_0

    .line 233
    :pswitch_8
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-direct {p0, v0, v2}, Lcom/google/android/a/h;->a(II)V
    :try_end_0
    .catch Lcom/google/android/a/e; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move v0, v1

    .line 234
    goto :goto_0

    .line 239
    :catch_0
    move-exception v0

    .line 240
    const-string v2, "ExoPlayerImplInternal"

    const-string v3, "Internal track renderer error."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 241
    iget-object v2, p0, Lcom/google/android/a/h;->c:Landroid/os/Handler;

    invoke-virtual {v2, v4, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 242
    invoke-direct {p0}, Lcom/google/android/a/h;->j()V

    move v0, v1

    .line 243
    goto :goto_0

    .line 244
    :catch_1
    move-exception v0

    .line 245
    const-string v2, "ExoPlayerImplInternal"

    const-string v3, "Internal runtime error."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 246
    iget-object v2, p0, Lcom/google/android/a/h;->c:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/a/e;

    invoke-direct {v3, v0, v1}, Lcom/google/android/a/e;-><init>(Ljava/lang/Throwable;Z)V

    invoke-virtual {v2, v4, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 247
    invoke-direct {p0}, Lcom/google/android/a/h;->j()V

    move v0, v1

    .line 248
    goto :goto_0

    .line 199
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_6
        :pswitch_4
        :pswitch_3
        :pswitch_8
        :pswitch_7
    .end packed-switch
.end method
