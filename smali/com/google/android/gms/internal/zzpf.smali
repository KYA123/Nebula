.class public interface abstract Lcom/google/android/gms/internal/zzpf;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract destroy()V
.end method

.method public abstract getAdvertiser()Ljava/lang/String;
.end method

.method public abstract getBody()Ljava/lang/String;
.end method

.method public abstract getCallToAction()Ljava/lang/String;
.end method

.method public abstract getExtras()Landroid/os/Bundle;
.end method

.method public abstract getHeadline()Ljava/lang/String;
.end method

.method public abstract getImages()Ljava/util/List;
.end method

.method public abstract getVideoController()Lcom/google/android/gms/internal/zzks;
.end method

.method public abstract zzc(Landroid/os/Bundle;)V
.end method

.method public abstract zzd(Landroid/os/Bundle;)Z
.end method

.method public abstract zze(Landroid/os/Bundle;)V
.end method

.method public abstract zzei()Lcom/google/android/gms/dynamic/IObjectWrapper;
.end method

.method public abstract zzem()Lcom/google/android/gms/internal/zzos;
.end method
