.class public interface abstract Lcom/google/android/gms/internal/zzvf;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract getAdvertiser()Ljava/lang/String;
.end method

.method public abstract getBody()Ljava/lang/String;
.end method

.method public abstract getCallToAction()Ljava/lang/String;
.end method

.method public abstract getExtras()Landroid/os/Bundle;
.end method

.method public abstract getHeadline()Ljava/lang/String;
.end method

.method public abstract getImages()Ljava/util/List;
.end method

.method public abstract getOverrideClickHandling()Z
.end method

.method public abstract getOverrideImpressionRecording()Z
.end method

.method public abstract getVideoController()Lcom/google/android/gms/internal/zzks;
.end method

.method public abstract recordImpression()V
.end method

.method public abstract zzem()Lcom/google/android/gms/internal/zzos;
.end method

.method public abstract zzfw()Lcom/google/android/gms/dynamic/IObjectWrapper;
.end method

.method public abstract zzl(Lcom/google/android/gms/dynamic/IObjectWrapper;)V
.end method

.method public abstract zzm(Lcom/google/android/gms/dynamic/IObjectWrapper;)V
.end method

.method public abstract zzn(Lcom/google/android/gms/dynamic/IObjectWrapper;)V
.end method
