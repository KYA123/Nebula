.class public Lcom/google/webp/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 78
    const/4 v0, 0x1

    new-array v0, v0, [I

    aput v1, v0, v1

    sput-object v0, Lcom/google/webp/a;->a:[I

    return-void
.end method

.method public static a([BJ[I[I)I
    .locals 1

    .prologue
    .line 17
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/webp/libwebpJNI;->WebPGetInfo([BJ[I[I)I

    move-result v0

    return v0
.end method

.method public static a([BIIIF)[B
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 90
    sget-object v3, Lcom/google/webp/a;->a:[I

    move-object v0, p0

    move v2, v1

    move v4, p1

    move v5, p2

    move v6, p3

    move v7, p4

    invoke-static/range {v0 .. v7}, Lcom/google/webp/a;->a([BII[IIIIF)[B

    move-result-object v0

    return-object v0
.end method

.method private static a([BII[IIIIF)[B
    .locals 1

    .prologue
    .line 53
    invoke-static/range {p0 .. p7}, Lcom/google/webp/libwebpJNI;->wrap_WebPEncodeRGBA([BII[IIIIF)[B

    move-result-object v0

    return-object v0
.end method

.method public static b([BJ[I[I)[B
    .locals 1

    .prologue
    .line 29
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/webp/libwebpJNI;->WebPDecodeARGB([BJ[I[I)[B

    move-result-object v0

    return-object v0
.end method
