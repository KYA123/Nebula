.class public Lcom/millennialmedia/InlineAd$InlineAdMetadata;
.super Lcom/millennialmedia/internal/AdPlacementMetadata;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/millennialmedia/InlineAd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "InlineAdMetadata"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/millennialmedia/internal/AdPlacementMetadata",
        "<",
        "Lcom/millennialmedia/InlineAd$InlineAdMetadata;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/millennialmedia/InlineAd$AdSize;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 197
    const-string v0, "inline"

    invoke-direct {p0, v0}, Lcom/millennialmedia/internal/AdPlacementMetadata;-><init>(Ljava/lang/String;)V

    .line 198
    return-void
.end method


# virtual methods
.method a(Lcom/millennialmedia/InlineAd;)I
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/millennialmedia/InlineAd$InlineAdMetadata;->a:Lcom/millennialmedia/InlineAd$AdSize;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/InlineAd$InlineAdMetadata;->a:Lcom/millennialmedia/InlineAd$AdSize;

    iget v0, v0, Lcom/millennialmedia/InlineAd$AdSize;->width:I

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/millennialmedia/InlineAd$InlineAdMetadata;->a:Lcom/millennialmedia/InlineAd$AdSize;

    iget v0, v0, Lcom/millennialmedia/InlineAd$AdSize;->width:I

    .line 240
    :goto_0
    return v0

    .line 235
    :cond_0
    invoke-static {p1}, Lcom/millennialmedia/InlineAd;->a(Lcom/millennialmedia/InlineAd;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 236
    if-nez v0, :cond_1

    .line 237
    const/4 v0, 0x0

    goto :goto_0

    .line 240
    :cond_1
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v0

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ViewUtils;->convertPixelsToDips(I)I

    move-result v0

    goto :goto_0
.end method

.method b(Lcom/millennialmedia/InlineAd;)I
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/millennialmedia/InlineAd$InlineAdMetadata;->a:Lcom/millennialmedia/InlineAd$AdSize;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/InlineAd$InlineAdMetadata;->a:Lcom/millennialmedia/InlineAd$AdSize;

    iget v0, v0, Lcom/millennialmedia/InlineAd$AdSize;->height:I

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/millennialmedia/InlineAd$InlineAdMetadata;->a:Lcom/millennialmedia/InlineAd$AdSize;

    iget v0, v0, Lcom/millennialmedia/InlineAd$AdSize;->height:I

    .line 255
    :goto_0
    return v0

    .line 250
    :cond_0
    invoke-static {p1}, Lcom/millennialmedia/InlineAd;->a(Lcom/millennialmedia/InlineAd;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 251
    if-nez v0, :cond_1

    .line 252
    const/4 v0, 0x0

    goto :goto_0

    .line 255
    :cond_1
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ViewUtils;->convertPixelsToDips(I)I

    move-result v0

    goto :goto_0
.end method

.method c(Lcom/millennialmedia/InlineAd;)I
    .locals 3

    .prologue
    .line 262
    iget-object v0, p0, Lcom/millennialmedia/InlineAd$InlineAdMetadata;->a:Lcom/millennialmedia/InlineAd$AdSize;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/InlineAd$InlineAdMetadata;->a:Lcom/millennialmedia/InlineAd$AdSize;

    iget v0, v0, Lcom/millennialmedia/InlineAd$AdSize;->width:I

    if-eqz v0, :cond_0

    .line 263
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/millennialmedia/InlineAd$InlineAdMetadata;->a:Lcom/millennialmedia/InlineAd$AdSize;

    iget v1, v1, Lcom/millennialmedia/InlineAd$AdSize;->width:I

    int-to-float v1, v1

    .line 264
    invoke-static {}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 263
    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    .line 272
    :goto_0
    return v0

    .line 267
    :cond_0
    invoke-static {p1}, Lcom/millennialmedia/InlineAd;->a(Lcom/millennialmedia/InlineAd;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 268
    if-nez v0, :cond_1

    .line 269
    const/4 v0, 0x0

    goto :goto_0

    .line 272
    :cond_1
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v0

    goto :goto_0
.end method

.method d(Lcom/millennialmedia/InlineAd;)I
    .locals 3

    .prologue
    .line 279
    iget-object v0, p0, Lcom/millennialmedia/InlineAd$InlineAdMetadata;->a:Lcom/millennialmedia/InlineAd$AdSize;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/InlineAd$InlineAdMetadata;->a:Lcom/millennialmedia/InlineAd$AdSize;

    iget v0, v0, Lcom/millennialmedia/InlineAd$AdSize;->height:I

    if-eqz v0, :cond_0

    .line 280
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/millennialmedia/InlineAd$InlineAdMetadata;->a:Lcom/millennialmedia/InlineAd$AdSize;

    iget v1, v1, Lcom/millennialmedia/InlineAd$AdSize;->height:I

    int-to-float v1, v1

    .line 281
    invoke-static {}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 280
    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    .line 289
    :goto_0
    return v0

    .line 284
    :cond_0
    invoke-static {p1}, Lcom/millennialmedia/InlineAd;->a(Lcom/millennialmedia/InlineAd;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 285
    if-nez v0, :cond_1

    .line 286
    const/4 v0, 0x0

    goto :goto_0

    .line 289
    :cond_1
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    goto :goto_0
.end method

.method e(Lcom/millennialmedia/InlineAd;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/millennialmedia/InlineAd;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 296
    invoke-super {p0, p1}, Lcom/millennialmedia/internal/AdPlacementMetadata;->toMap(Lcom/millennialmedia/internal/AdPlacement;)Ljava/util/Map;

    move-result-object v0

    .line 298
    const-string v1, "width"

    invoke-virtual {p0, p1}, Lcom/millennialmedia/InlineAd$InlineAdMetadata;->a(Lcom/millennialmedia/InlineAd;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/millennialmedia/internal/utils/Utils;->injectIfNotNull(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Object;)V

    .line 299
    const-string v1, "height"

    invoke-virtual {p0, p1}, Lcom/millennialmedia/InlineAd$InlineAdMetadata;->b(Lcom/millennialmedia/InlineAd;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/millennialmedia/internal/utils/Utils;->injectIfNotNull(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Object;)V

    .line 300
    const-string v1, "refreshRate"

    invoke-virtual {p1}, Lcom/millennialmedia/InlineAd;->a()Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/millennialmedia/internal/utils/Utils;->injectIfNotNull(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Object;)V

    .line 302
    return-object v0
.end method

.method public getAdSize()Lcom/millennialmedia/InlineAd$AdSize;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/millennialmedia/InlineAd$InlineAdMetadata;->a:Lcom/millennialmedia/InlineAd$AdSize;

    return-object v0
.end method

.method public setAdSize(Lcom/millennialmedia/InlineAd$AdSize;)Lcom/millennialmedia/InlineAd$InlineAdMetadata;
    .locals 2

    .prologue
    .line 209
    if-nez p1, :cond_0

    .line 210
    invoke-static {}, Lcom/millennialmedia/InlineAd;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Provided AdSize cannot be null"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    :goto_0
    return-object p0

    .line 212
    :cond_0
    iput-object p1, p0, Lcom/millennialmedia/InlineAd$InlineAdMetadata;->a:Lcom/millennialmedia/InlineAd$AdSize;

    goto :goto_0
.end method
