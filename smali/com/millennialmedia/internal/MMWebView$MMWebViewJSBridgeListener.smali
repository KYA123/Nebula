.class Lcom/millennialmedia/internal/MMWebView$MMWebViewJSBridgeListener;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/millennialmedia/internal/JSBridge$JSBridgeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/millennialmedia/internal/MMWebView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MMWebViewJSBridgeListener"
.end annotation


# instance fields
.field a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/millennialmedia/internal/MMWebView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/millennialmedia/internal/MMWebView;)V
    .locals 1

    .prologue
    .line 229
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 231
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/millennialmedia/internal/MMWebView$MMWebViewJSBridgeListener;->a:Ljava/lang/ref/WeakReference;

    .line 232
    return-void
.end method

.method private a()Lcom/millennialmedia/internal/MMWebView;
    .locals 2

    .prologue
    .line 237
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView$MMWebViewJSBridgeListener;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/MMWebView;

    .line 238
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/millennialmedia/internal/MMWebView;->isDestroyed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 242
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView$MMWebViewJSBridgeListener;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/MMWebView;

    .line 282
    if-eqz v0, :cond_0

    .line 283
    iget-object v0, v0, Lcom/millennialmedia/internal/MMWebView;->a:Lcom/millennialmedia/internal/MMWebView$MMWebViewListener;

    invoke-interface {v0}, Lcom/millennialmedia/internal/MMWebView$MMWebViewListener;->close()V

    .line 285
    :cond_0
    return-void
.end method

.method public expand(Lcom/millennialmedia/internal/SizableStateManager$ExpandParams;)Z
    .locals 1

    .prologue
    .line 291
    invoke-direct {p0}, Lcom/millennialmedia/internal/MMWebView$MMWebViewJSBridgeListener;->a()Lcom/millennialmedia/internal/MMWebView;

    move-result-object v0

    .line 292
    if-nez v0, :cond_0

    .line 293
    const/4 v0, 0x0

    .line 296
    :goto_0
    return v0

    :cond_0
    iget-object v0, v0, Lcom/millennialmedia/internal/MMWebView;->a:Lcom/millennialmedia/internal/MMWebView$MMWebViewListener;

    invoke-interface {v0, p1}, Lcom/millennialmedia/internal/MMWebView$MMWebViewListener;->expand(Lcom/millennialmedia/internal/SizableStateManager$ExpandParams;)Z

    move-result v0

    goto :goto_0
.end method

.method public onAdLeftApplication()V
    .locals 1

    .prologue
    .line 325
    invoke-direct {p0}, Lcom/millennialmedia/internal/MMWebView$MMWebViewJSBridgeListener;->a()Lcom/millennialmedia/internal/MMWebView;

    move-result-object v0

    .line 326
    if-eqz v0, :cond_0

    .line 327
    iget-object v0, v0, Lcom/millennialmedia/internal/MMWebView;->a:Lcom/millennialmedia/internal/MMWebView$MMWebViewListener;

    invoke-interface {v0}, Lcom/millennialmedia/internal/MMWebView$MMWebViewListener;->onAdLeftApplication()V

    .line 329
    :cond_0
    return-void
.end method

.method public onInjectedScriptsLoaded()V
    .locals 3

    .prologue
    .line 249
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250
    invoke-static {}, Lcom/millennialmedia/internal/MMWebView;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Injected scripts have been loaded"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    :cond_0
    invoke-direct {p0}, Lcom/millennialmedia/internal/MMWebView$MMWebViewJSBridgeListener;->a()Lcom/millennialmedia/internal/MMWebView;

    move-result-object v0

    .line 254
    if-eqz v0, :cond_1

    .line 255
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/millennialmedia/internal/MMWebView;->a(Lcom/millennialmedia/internal/MMWebView;Z)Z

    .line 256
    iget-object v1, v0, Lcom/millennialmedia/internal/MMWebView;->b:Lcom/millennialmedia/internal/JSBridge;

    sget v2, Lcom/millennialmedia/MMLog;->logLevel:I

    invoke-virtual {v1, v2}, Lcom/millennialmedia/internal/JSBridge;->setLogLevel(I)V

    .line 257
    invoke-static {v0}, Lcom/millennialmedia/internal/MMWebView;->a(Lcom/millennialmedia/internal/MMWebView;)V

    .line 261
    :goto_0
    return-void

    .line 259
    :cond_1
    invoke-static {}, Lcom/millennialmedia/internal/MMWebView;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MMWebView reference no longer points to a valid object"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onJSBridgeReady()V
    .locals 2

    .prologue
    .line 267
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268
    invoke-static {}, Lcom/millennialmedia/internal/MMWebView;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JSBridge is ready"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    :cond_0
    invoke-direct {p0}, Lcom/millennialmedia/internal/MMWebView$MMWebViewJSBridgeListener;->a()Lcom/millennialmedia/internal/MMWebView;

    move-result-object v0

    .line 272
    if-eqz v0, :cond_1

    .line 273
    iget-object v0, v0, Lcom/millennialmedia/internal/MMWebView;->a:Lcom/millennialmedia/internal/MMWebView$MMWebViewListener;

    invoke-interface {v0}, Lcom/millennialmedia/internal/MMWebView$MMWebViewListener;->onReady()V

    .line 275
    :cond_1
    return-void
.end method

.method public resize(Lcom/millennialmedia/internal/SizableStateManager$ResizeParams;)Z
    .locals 1

    .prologue
    .line 303
    invoke-direct {p0}, Lcom/millennialmedia/internal/MMWebView$MMWebViewJSBridgeListener;->a()Lcom/millennialmedia/internal/MMWebView;

    move-result-object v0

    .line 304
    if-nez v0, :cond_0

    .line 305
    const/4 v0, 0x0

    .line 308
    :goto_0
    return v0

    :cond_0
    iget-object v0, v0, Lcom/millennialmedia/internal/MMWebView;->a:Lcom/millennialmedia/internal/MMWebView$MMWebViewListener;

    invoke-interface {v0, p1}, Lcom/millennialmedia/internal/MMWebView$MMWebViewListener;->resize(Lcom/millennialmedia/internal/SizableStateManager$ResizeParams;)Z

    move-result v0

    goto :goto_0
.end method

.method public setOrientation(I)V
    .locals 1

    .prologue
    .line 335
    invoke-direct {p0}, Lcom/millennialmedia/internal/MMWebView$MMWebViewJSBridgeListener;->a()Lcom/millennialmedia/internal/MMWebView;

    move-result-object v0

    .line 336
    if-eqz v0, :cond_0

    .line 337
    iget-object v0, v0, Lcom/millennialmedia/internal/MMWebView;->a:Lcom/millennialmedia/internal/MMWebView$MMWebViewListener;

    invoke-interface {v0, p1}, Lcom/millennialmedia/internal/MMWebView$MMWebViewListener;->setOrientation(I)V

    .line 339
    :cond_0
    return-void
.end method

.method public showCloseIndicator(Z)V
    .locals 1

    .prologue
    .line 315
    invoke-direct {p0}, Lcom/millennialmedia/internal/MMWebView$MMWebViewJSBridgeListener;->a()Lcom/millennialmedia/internal/MMWebView;

    move-result-object v0

    .line 316
    if-eqz v0, :cond_0

    .line 317
    iget-object v0, v0, Lcom/millennialmedia/internal/MMWebView;->a:Lcom/millennialmedia/internal/MMWebView$MMWebViewListener;

    invoke-interface {v0, p1}, Lcom/millennialmedia/internal/MMWebView$MMWebViewListener;->showCloseIndicator(Z)V

    .line 319
    :cond_0
    return-void
.end method
