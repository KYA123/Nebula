.class Lcom/millennialmedia/internal/SizableStateManager$2$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/millennialmedia/internal/SizableStateManager$2;->onLayoutChange(Landroid/view/View;IIIIIIII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:I

.field final synthetic c:Lcom/millennialmedia/internal/SizableStateManager$2;


# direct methods
.method constructor <init>(Lcom/millennialmedia/internal/SizableStateManager$2;II)V
    .locals 0

    .prologue
    .line 628
    iput-object p1, p0, Lcom/millennialmedia/internal/SizableStateManager$2$1;->c:Lcom/millennialmedia/internal/SizableStateManager$2;

    iput p2, p0, Lcom/millennialmedia/internal/SizableStateManager$2$1;->a:I

    iput p3, p0, Lcom/millennialmedia/internal/SizableStateManager$2$1;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 632
    iget-object v0, p0, Lcom/millennialmedia/internal/SizableStateManager$2$1;->c:Lcom/millennialmedia/internal/SizableStateManager$2;

    iget-object v0, v0, Lcom/millennialmedia/internal/SizableStateManager$2;->b:Lcom/millennialmedia/internal/SizableStateManager$SizableState;

    sget-object v1, Lcom/millennialmedia/internal/SizableStateManager$SizableState;->STATE_RESIZED:Lcom/millennialmedia/internal/SizableStateManager$SizableState;

    if-ne v0, v1, :cond_1

    .line 633
    iget-object v0, p0, Lcom/millennialmedia/internal/SizableStateManager$2$1;->c:Lcom/millennialmedia/internal/SizableStateManager$2;

    iget-object v0, v0, Lcom/millennialmedia/internal/SizableStateManager$2;->c:Lcom/millennialmedia/internal/SizableStateManager;

    invoke-static {v0}, Lcom/millennialmedia/internal/SizableStateManager;->b(Lcom/millennialmedia/internal/SizableStateManager;)Lcom/millennialmedia/internal/SizableStateManager$SizableListener;

    move-result-object v0

    iget v1, p0, Lcom/millennialmedia/internal/SizableStateManager$2$1;->a:I

    iget v2, p0, Lcom/millennialmedia/internal/SizableStateManager$2$1;->b:I

    invoke-interface {v0, v1, v2}, Lcom/millennialmedia/internal/SizableStateManager$SizableListener;->onResized(II)V

    .line 641
    :cond_0
    :goto_0
    return-void

    .line 634
    :cond_1
    iget-object v0, p0, Lcom/millennialmedia/internal/SizableStateManager$2$1;->c:Lcom/millennialmedia/internal/SizableStateManager$2;

    iget-object v0, v0, Lcom/millennialmedia/internal/SizableStateManager$2;->b:Lcom/millennialmedia/internal/SizableStateManager$SizableState;

    sget-object v1, Lcom/millennialmedia/internal/SizableStateManager$SizableState;->STATE_EXPANDED:Lcom/millennialmedia/internal/SizableStateManager$SizableState;

    if-ne v0, v1, :cond_2

    .line 635
    iget-object v0, p0, Lcom/millennialmedia/internal/SizableStateManager$2$1;->c:Lcom/millennialmedia/internal/SizableStateManager$2;

    iget-object v0, v0, Lcom/millennialmedia/internal/SizableStateManager$2;->c:Lcom/millennialmedia/internal/SizableStateManager;

    invoke-static {v0}, Lcom/millennialmedia/internal/SizableStateManager;->b(Lcom/millennialmedia/internal/SizableStateManager;)Lcom/millennialmedia/internal/SizableStateManager$SizableListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/millennialmedia/internal/SizableStateManager$SizableListener;->onExpanded()V

    goto :goto_0

    .line 636
    :cond_2
    iget-object v0, p0, Lcom/millennialmedia/internal/SizableStateManager$2$1;->c:Lcom/millennialmedia/internal/SizableStateManager$2;

    iget-object v0, v0, Lcom/millennialmedia/internal/SizableStateManager$2;->b:Lcom/millennialmedia/internal/SizableStateManager$SizableState;

    sget-object v1, Lcom/millennialmedia/internal/SizableStateManager$SizableState;->STATE_UNRESIZED:Lcom/millennialmedia/internal/SizableStateManager$SizableState;

    if-ne v0, v1, :cond_3

    .line 637
    iget-object v0, p0, Lcom/millennialmedia/internal/SizableStateManager$2$1;->c:Lcom/millennialmedia/internal/SizableStateManager$2;

    iget-object v0, v0, Lcom/millennialmedia/internal/SizableStateManager$2;->c:Lcom/millennialmedia/internal/SizableStateManager;

    invoke-static {v0}, Lcom/millennialmedia/internal/SizableStateManager;->b(Lcom/millennialmedia/internal/SizableStateManager;)Lcom/millennialmedia/internal/SizableStateManager$SizableListener;

    move-result-object v0

    iget v1, p0, Lcom/millennialmedia/internal/SizableStateManager$2$1;->a:I

    iget v2, p0, Lcom/millennialmedia/internal/SizableStateManager$2$1;->b:I

    invoke-interface {v0, v1, v2}, Lcom/millennialmedia/internal/SizableStateManager$SizableListener;->onUnresized(II)V

    goto :goto_0

    .line 638
    :cond_3
    iget-object v0, p0, Lcom/millennialmedia/internal/SizableStateManager$2$1;->c:Lcom/millennialmedia/internal/SizableStateManager$2;

    iget-object v0, v0, Lcom/millennialmedia/internal/SizableStateManager$2;->b:Lcom/millennialmedia/internal/SizableStateManager$SizableState;

    sget-object v1, Lcom/millennialmedia/internal/SizableStateManager$SizableState;->STATE_COLLAPSED:Lcom/millennialmedia/internal/SizableStateManager$SizableState;

    if-ne v0, v1, :cond_0

    .line 639
    iget-object v0, p0, Lcom/millennialmedia/internal/SizableStateManager$2$1;->c:Lcom/millennialmedia/internal/SizableStateManager$2;

    iget-object v0, v0, Lcom/millennialmedia/internal/SizableStateManager$2;->c:Lcom/millennialmedia/internal/SizableStateManager;

    invoke-static {v0}, Lcom/millennialmedia/internal/SizableStateManager;->b(Lcom/millennialmedia/internal/SizableStateManager;)Lcom/millennialmedia/internal/SizableStateManager$SizableListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/millennialmedia/internal/SizableStateManager$SizableListener;->onCollapsed()V

    goto :goto_0
.end method
