.class Lcom/millennialmedia/internal/SizableStateManager$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/millennialmedia/internal/SizableStateManager;->a(Landroid/view/View;Lcom/millennialmedia/internal/SizableStateManager$SizableState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lcom/millennialmedia/internal/SizableStateManager$SizableState;

.field final synthetic c:Lcom/millennialmedia/internal/SizableStateManager;


# direct methods
.method constructor <init>(Lcom/millennialmedia/internal/SizableStateManager;Landroid/view/View;Lcom/millennialmedia/internal/SizableStateManager$SizableState;)V
    .locals 0

    .prologue
    .line 615
    iput-object p1, p0, Lcom/millennialmedia/internal/SizableStateManager$2;->c:Lcom/millennialmedia/internal/SizableStateManager;

    iput-object p2, p0, Lcom/millennialmedia/internal/SizableStateManager$2;->a:Landroid/view/View;

    iput-object p3, p0, Lcom/millennialmedia/internal/SizableStateManager$2;->b:Lcom/millennialmedia/internal/SizableStateManager$SizableState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 3

    .prologue
    .line 620
    sub-int v0, p4, p2

    .line 621
    sub-int v1, p5, p3

    .line 622
    if-lez v0, :cond_0

    if-lez v1, :cond_0

    .line 623
    iget-object v2, p0, Lcom/millennialmedia/internal/SizableStateManager$2;->a:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 628
    new-instance v2, Lcom/millennialmedia/internal/SizableStateManager$2$1;

    invoke-direct {v2, p0, v0, v1}, Lcom/millennialmedia/internal/SizableStateManager$2$1;-><init>(Lcom/millennialmedia/internal/SizableStateManager$2;II)V

    invoke-static {v2}, Lcom/millennialmedia/internal/utils/ThreadUtils;->postOnUiThread(Ljava/lang/Runnable;)V

    .line 644
    :cond_0
    return-void
.end method
