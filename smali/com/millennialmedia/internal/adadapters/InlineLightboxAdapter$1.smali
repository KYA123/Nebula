.class Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/millennialmedia/internal/adcontrollers/LightboxController$LightboxControllerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;


# direct methods
.method constructor <init>(Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;)V
    .locals 0

    .prologue
    .line 20
    iput-object p1, p0, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter$1;->a:Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public attachFailed()V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter$1;->a:Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;

    invoke-static {v0}, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;->a(Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;)Lcom/millennialmedia/internal/adadapters/InlineAdapter$InlineAdapterListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/millennialmedia/internal/adadapters/InlineAdapter$InlineAdapterListener;->displayFailed()V

    .line 47
    return-void
.end method

.method public attachSucceeded()V
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter$1;->a:Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;

    invoke-static {v0}, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;->a(Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;)Lcom/millennialmedia/internal/adadapters/InlineAdapter$InlineAdapterListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/millennialmedia/internal/adadapters/InlineAdapter$InlineAdapterListener;->displaySucceeded()V

    .line 40
    return-void
.end method

.method public initFailed()V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter$1;->a:Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;

    invoke-static {v0}, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;->a(Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;)Lcom/millennialmedia/internal/adadapters/InlineAdapter$InlineAdapterListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/millennialmedia/internal/adadapters/InlineAdapter$InlineAdapterListener;->initFailed()V

    .line 33
    return-void
.end method

.method public initSucceeded()V
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter$1;->a:Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;

    invoke-static {v0}, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;->a(Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;)Lcom/millennialmedia/internal/adadapters/InlineAdapter$InlineAdapterListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/millennialmedia/internal/adadapters/InlineAdapter$InlineAdapterListener;->initSucceeded()V

    .line 26
    return-void
.end method

.method public onAdLeftApplication()V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter$1;->a:Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;

    invoke-static {v0}, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;->a(Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;)Lcom/millennialmedia/internal/adadapters/InlineAdapter$InlineAdapterListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/millennialmedia/internal/adadapters/InlineAdapter$InlineAdapterListener;->onAdLeftApplication()V

    .line 61
    return-void
.end method

.method public onClicked()V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter$1;->a:Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;

    invoke-static {v0}, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;->a(Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;)Lcom/millennialmedia/internal/adadapters/InlineAdapter$InlineAdapterListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/millennialmedia/internal/adadapters/InlineAdapter$InlineAdapterListener;->onClicked()V

    .line 54
    return-void
.end method

.method public onCollapsed()V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter$1;->a:Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;

    invoke-static {v0}, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;->a(Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;)Lcom/millennialmedia/internal/adadapters/InlineAdapter$InlineAdapterListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/millennialmedia/internal/adadapters/InlineAdapter$InlineAdapterListener;->onCollapsed()V

    .line 75
    return-void
.end method

.method public onExpanded()V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter$1;->a:Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;

    invoke-static {v0}, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;->a(Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;)Lcom/millennialmedia/internal/adadapters/InlineAdapter$InlineAdapterListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/millennialmedia/internal/adadapters/InlineAdapter$InlineAdapterListener;->onExpanded()V

    .line 68
    return-void
.end method
