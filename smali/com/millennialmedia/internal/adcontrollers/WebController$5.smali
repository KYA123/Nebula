.class Lcom/millennialmedia/internal/adcontrollers/WebController$5;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/millennialmedia/internal/SizableStateManager$SizableListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/millennialmedia/internal/adcontrollers/WebController;->a()Lcom/millennialmedia/internal/SizableStateManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/millennialmedia/internal/adcontrollers/WebController;


# direct methods
.method constructor <init>(Lcom/millennialmedia/internal/adcontrollers/WebController;)V
    .locals 0

    .prologue
    .line 232
    iput-object p1, p0, Lcom/millennialmedia/internal/adcontrollers/WebController$5;->a:Lcom/millennialmedia/internal/adcontrollers/WebController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCollapsed()V
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/millennialmedia/internal/adcontrollers/WebController$5;->a:Lcom/millennialmedia/internal/adcontrollers/WebController;

    invoke-static {v0}, Lcom/millennialmedia/internal/adcontrollers/WebController;->c(Lcom/millennialmedia/internal/adcontrollers/WebController;)Lcom/millennialmedia/internal/MMWebView;

    move-result-object v0

    .line 312
    if-eqz v0, :cond_0

    .line 313
    invoke-virtual {v0}, Lcom/millennialmedia/internal/MMWebView;->setStateCollapsed()V

    .line 314
    iget-object v0, p0, Lcom/millennialmedia/internal/adcontrollers/WebController$5;->a:Lcom/millennialmedia/internal/adcontrollers/WebController;

    invoke-static {v0}, Lcom/millennialmedia/internal/adcontrollers/WebController;->a(Lcom/millennialmedia/internal/adcontrollers/WebController;)Lcom/millennialmedia/internal/adcontrollers/WebController$WebControllerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/millennialmedia/internal/adcontrollers/WebController$WebControllerListener;->onCollapsed()V

    .line 315
    iget-object v0, p0, Lcom/millennialmedia/internal/adcontrollers/WebController$5;->a:Lcom/millennialmedia/internal/adcontrollers/WebController;

    invoke-static {v0}, Lcom/millennialmedia/internal/adcontrollers/WebController;->d(Lcom/millennialmedia/internal/adcontrollers/WebController;)V

    .line 317
    :cond_0
    return-void
.end method

.method public onCollapsing()V
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/millennialmedia/internal/adcontrollers/WebController$5;->a:Lcom/millennialmedia/internal/adcontrollers/WebController;

    invoke-static {v0}, Lcom/millennialmedia/internal/adcontrollers/WebController;->c(Lcom/millennialmedia/internal/adcontrollers/WebController;)Lcom/millennialmedia/internal/MMWebView;

    move-result-object v0

    .line 302
    if-eqz v0, :cond_0

    .line 303
    invoke-virtual {v0}, Lcom/millennialmedia/internal/MMWebView;->setStateResizing()V

    .line 305
    :cond_0
    return-void
.end method

.method public onExpandFailed()V
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcom/millennialmedia/internal/adcontrollers/WebController$5;->a:Lcom/millennialmedia/internal/adcontrollers/WebController;

    invoke-static {v0}, Lcom/millennialmedia/internal/adcontrollers/WebController;->a(Lcom/millennialmedia/internal/adcontrollers/WebController;)Lcom/millennialmedia/internal/adcontrollers/WebController$WebControllerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/millennialmedia/internal/adcontrollers/WebController$WebControllerListener;->attachFailed()V

    .line 324
    iget-object v0, p0, Lcom/millennialmedia/internal/adcontrollers/WebController$5;->a:Lcom/millennialmedia/internal/adcontrollers/WebController;

    invoke-static {v0}, Lcom/millennialmedia/internal/adcontrollers/WebController;->d(Lcom/millennialmedia/internal/adcontrollers/WebController;)V

    .line 325
    return-void
.end method

.method public onExpanded()V
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lcom/millennialmedia/internal/adcontrollers/WebController$5;->a:Lcom/millennialmedia/internal/adcontrollers/WebController;

    invoke-static {v0}, Lcom/millennialmedia/internal/adcontrollers/WebController;->c(Lcom/millennialmedia/internal/adcontrollers/WebController;)Lcom/millennialmedia/internal/MMWebView;

    move-result-object v0

    .line 291
    if-eqz v0, :cond_0

    .line 292
    invoke-virtual {v0}, Lcom/millennialmedia/internal/MMWebView;->setStateExpanded()V

    .line 293
    iget-object v0, p0, Lcom/millennialmedia/internal/adcontrollers/WebController$5;->a:Lcom/millennialmedia/internal/adcontrollers/WebController;

    invoke-static {v0}, Lcom/millennialmedia/internal/adcontrollers/WebController;->a(Lcom/millennialmedia/internal/adcontrollers/WebController;)Lcom/millennialmedia/internal/adcontrollers/WebController$WebControllerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/millennialmedia/internal/adcontrollers/WebController$WebControllerListener;->onExpanded()V

    .line 295
    :cond_0
    return-void
.end method

.method public onExpanding()V
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/millennialmedia/internal/adcontrollers/WebController$5;->a:Lcom/millennialmedia/internal/adcontrollers/WebController;

    invoke-static {v0}, Lcom/millennialmedia/internal/adcontrollers/WebController;->c(Lcom/millennialmedia/internal/adcontrollers/WebController;)Lcom/millennialmedia/internal/MMWebView;

    move-result-object v0

    .line 281
    if-eqz v0, :cond_0

    .line 282
    invoke-virtual {v0}, Lcom/millennialmedia/internal/MMWebView;->setStateResizing()V

    .line 284
    :cond_0
    return-void
.end method

.method public onResized(II)V
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Lcom/millennialmedia/internal/adcontrollers/WebController$5;->a:Lcom/millennialmedia/internal/adcontrollers/WebController;

    invoke-static {v0}, Lcom/millennialmedia/internal/adcontrollers/WebController;->c(Lcom/millennialmedia/internal/adcontrollers/WebController;)Lcom/millennialmedia/internal/MMWebView;

    move-result-object v0

    .line 248
    if-eqz v0, :cond_0

    .line 249
    invoke-virtual {v0}, Lcom/millennialmedia/internal/MMWebView;->setStateResized()V

    .line 250
    iget-object v0, p0, Lcom/millennialmedia/internal/adcontrollers/WebController$5;->a:Lcom/millennialmedia/internal/adcontrollers/WebController;

    invoke-static {v0}, Lcom/millennialmedia/internal/adcontrollers/WebController;->a(Lcom/millennialmedia/internal/adcontrollers/WebController;)Lcom/millennialmedia/internal/adcontrollers/WebController$WebControllerListener;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p1, p2, v1}, Lcom/millennialmedia/internal/adcontrollers/WebController$WebControllerListener;->onResized(IIZ)V

    .line 252
    :cond_0
    return-void
.end method

.method public onResizing(II)V
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/millennialmedia/internal/adcontrollers/WebController$5;->a:Lcom/millennialmedia/internal/adcontrollers/WebController;

    invoke-static {v0}, Lcom/millennialmedia/internal/adcontrollers/WebController;->c(Lcom/millennialmedia/internal/adcontrollers/WebController;)Lcom/millennialmedia/internal/MMWebView;

    move-result-object v0

    .line 237
    if-eqz v0, :cond_0

    .line 238
    invoke-virtual {v0}, Lcom/millennialmedia/internal/MMWebView;->setStateResizing()V

    .line 239
    iget-object v0, p0, Lcom/millennialmedia/internal/adcontrollers/WebController$5;->a:Lcom/millennialmedia/internal/adcontrollers/WebController;

    invoke-static {v0}, Lcom/millennialmedia/internal/adcontrollers/WebController;->a(Lcom/millennialmedia/internal/adcontrollers/WebController;)Lcom/millennialmedia/internal/adcontrollers/WebController$WebControllerListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/millennialmedia/internal/adcontrollers/WebController$WebControllerListener;->onResize(II)V

    .line 241
    :cond_0
    return-void
.end method

.method public onUnresized(II)V
    .locals 2

    .prologue
    .line 268
    iget-object v0, p0, Lcom/millennialmedia/internal/adcontrollers/WebController$5;->a:Lcom/millennialmedia/internal/adcontrollers/WebController;

    invoke-static {v0}, Lcom/millennialmedia/internal/adcontrollers/WebController;->c(Lcom/millennialmedia/internal/adcontrollers/WebController;)Lcom/millennialmedia/internal/MMWebView;

    move-result-object v0

    .line 269
    if-eqz v0, :cond_0

    .line 270
    invoke-virtual {v0}, Lcom/millennialmedia/internal/MMWebView;->setStateUnresized()V

    .line 271
    iget-object v0, p0, Lcom/millennialmedia/internal/adcontrollers/WebController$5;->a:Lcom/millennialmedia/internal/adcontrollers/WebController;

    invoke-static {v0}, Lcom/millennialmedia/internal/adcontrollers/WebController;->a(Lcom/millennialmedia/internal/adcontrollers/WebController;)Lcom/millennialmedia/internal/adcontrollers/WebController$WebControllerListener;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, p1, p2, v1}, Lcom/millennialmedia/internal/adcontrollers/WebController$WebControllerListener;->onResized(IIZ)V

    .line 272
    iget-object v0, p0, Lcom/millennialmedia/internal/adcontrollers/WebController$5;->a:Lcom/millennialmedia/internal/adcontrollers/WebController;

    invoke-static {v0}, Lcom/millennialmedia/internal/adcontrollers/WebController;->d(Lcom/millennialmedia/internal/adcontrollers/WebController;)V

    .line 274
    :cond_0
    return-void
.end method

.method public onUnresizing(II)V
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/millennialmedia/internal/adcontrollers/WebController$5;->a:Lcom/millennialmedia/internal/adcontrollers/WebController;

    invoke-static {v0}, Lcom/millennialmedia/internal/adcontrollers/WebController;->c(Lcom/millennialmedia/internal/adcontrollers/WebController;)Lcom/millennialmedia/internal/MMWebView;

    move-result-object v0

    .line 259
    if-eqz v0, :cond_0

    .line 260
    invoke-virtual {v0}, Lcom/millennialmedia/internal/MMWebView;->setStateResizing()V

    .line 262
    :cond_0
    return-void
.end method
