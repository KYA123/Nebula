.class public Lcom/millennialmedia/internal/utils/EnvironmentUtils;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/millennialmedia/internal/utils/EnvironmentUtils$AvailableCameras;
    }
.end annotation


# static fields
.field public static final ORIENTATION_LANDSCAPE:Ljava/lang/String; = "landscape"

.field public static final ORIENTATION_PORTRAIT:Ljava/lang/String; = "portrait"

.field private static final a:Ljava/lang/String;

.field private static b:Landroid/app/Application;

.field private static c:Landroid/content/Context;

.field private static d:Z

.field private static e:Z

.field private static f:Z

.field private static g:Z

.field private static h:Z

.field private static i:Z

.field private static j:Z

.field private static k:Z

.field private static l:Z

.field private static m:Ljava/lang/Integer;

.field private static n:Lcom/millennialmedia/internal/utils/EnvironmentUtils$AvailableCameras;

.field private static volatile o:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    const-class v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0

    .prologue
    .line 72
    sput-object p0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->m:Ljava/lang/Integer;

    return-object p0
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 72
    sput-object p0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->o:Ljava/lang/String;

    return-object p0
.end method

.method public static areHeadphonesPluggedIn()Z
    .locals 2

    .prologue
    .line 1141
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1143
    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v0

    return v0
.end method

.method private static b(Ljava/lang/String;)J
    .locals 4

    .prologue
    .line 718
    new-instance v0, Landroid/os/StatFs;

    invoke-direct {v0, p0}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 719
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v1

    int-to-long v2, v1

    .line 721
    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method static synthetic b()Z
    .locals 1

    .prologue
    .line 72
    sget-boolean v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->d:Z

    return v0
.end method

.method static synthetic c()Z
    .locals 1

    .prologue
    .line 72
    sget-boolean v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->e:Z

    return v0
.end method

.method public static checkPermissionGranted(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 728
    :try_start_0
    sget-object v1, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    invoke-static {v1, p0}, Landroid/support/v4/b/b;->a(Landroid/content/Context;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 735
    :cond_0
    :goto_0
    return v0

    .line 731
    :catch_0
    move-exception v1

    .line 732
    sget-object v2, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to check permission "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic d()Z
    .locals 1

    .prologue
    .line 72
    sget-boolean v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->f:Z

    return v0
.end method

.method static synthetic e()Z
    .locals 1

    .prologue
    .line 72
    sget-boolean v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->h:Z

    return v0
.end method

.method static synthetic f()Z
    .locals 1

    .prologue
    .line 72
    sget-boolean v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->g:Z

    return v0
.end method

.method static synthetic g()Z
    .locals 1

    .prologue
    .line 72
    sget-boolean v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->i:Z

    return v0
.end method

.method public static getAdInfo()Lcom/millennialmedia/internal/utils/AdvertisingIdInfo;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 346
    invoke-static {}, Lcom/millennialmedia/internal/utils/ThreadUtils;->isUiThread()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 347
    sget-object v1, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->a:Ljava/lang/String;

    const-string v2, "Unable to get AdInfo instance on UI thread!"

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    :cond_0
    :goto_0
    return-object v0

    .line 356
    :cond_1
    invoke-static {}, Lcom/google/android/gms/common/GoogleApiAvailability;->getInstance()Lcom/google/android/gms/common/GoogleApiAvailability;

    move-result-object v1

    sget-object v2, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    .line 357
    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/GoogleApiAvailability;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v1

    if-nez v1, :cond_2

    .line 359
    const-string v1, "Unable to get google play services advertising info, "

    .line 362
    :try_start_0
    sget-object v1, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    .line 363
    invoke-static {v1}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;->getAdvertisingIdInfo(Landroid/content/Context;)Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

    move-result-object v2

    .line 365
    new-instance v1, Lcom/millennialmedia/internal/utils/GoogleAdvertisingIdInfo;

    invoke-direct {v1, v2}, Lcom/millennialmedia/internal/utils/GoogleAdvertisingIdInfo;-><init>(Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/gms/common/GooglePlayServicesRepairableException; {:try_start_0 .. :try_end_0} :catch_3

    .line 382
    :goto_1
    if-nez v1, :cond_3

    .line 385
    :try_start_1
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 388
    const-string v2, "limit_ad_tracking"

    invoke-static {v0, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v2

    .line 391
    const-string v3, "advertising_id"

    invoke-static {v0, v3}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 393
    new-instance v0, Lcom/millennialmedia/internal/utils/AmazonAdvertisingIdInfo;

    invoke-direct {v0, v3, v2}, Lcom/millennialmedia/internal/utils/AmazonAdvertisingIdInfo;-><init>(Ljava/lang/String;I)V
    :try_end_1
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_1} :catch_4

    .line 402
    :goto_2
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 403
    sget-object v1, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 367
    :catch_0
    move-exception v1

    .line 368
    sget-object v2, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->a:Ljava/lang/String;

    const-string v3, "Unable to get google play services advertising info, google play services (e.g., the old version of the service doesn\'t support getting advertising ID)"

    invoke-static {v2, v3, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v0

    .line 377
    goto :goto_1

    .line 371
    :catch_1
    move-exception v1

    .line 372
    sget-object v2, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->a:Ljava/lang/String;

    const-string v3, "Unable to get google play services advertising info, google play services is not available"

    invoke-static {v2, v3, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v0

    .line 377
    goto :goto_1

    .line 373
    :catch_2
    move-exception v1

    .line 374
    sget-object v2, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->a:Ljava/lang/String;

    const-string v3, "Unable to get google play services advertising info, illegal state"

    invoke-static {v2, v3, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v0

    .line 377
    goto :goto_1

    .line 375
    :catch_3
    move-exception v1

    .line 376
    sget-object v2, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->a:Ljava/lang/String;

    const-string v3, "Unable to get google play services advertising info, google play services is not installed, up-to-date, or enabled"

    invoke-static {v2, v3, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_2
    move-object v1, v0

    goto :goto_1

    .line 394
    :catch_4
    move-exception v0

    .line 396
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 397
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->a:Ljava/lang/String;

    const-string v2, "Amazon advertiser info not available."

    invoke-static {v0, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method

.method public static getAdvertisingId(Lcom/millennialmedia/internal/utils/AdvertisingIdInfo;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 412
    if-nez p0, :cond_0

    .line 413
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->a:Ljava/lang/String;

    const-string v1, "Unable to get advertisering id value"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    const/4 v0, 0x0

    .line 418
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, Lcom/millennialmedia/internal/utils/AdvertisingIdInfo;->getId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 587
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getApplication()Landroid/app/Application;
    .locals 1

    .prologue
    .line 286
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->b:Landroid/app/Application;

    return-object v0
.end method

.method public static getApplicationContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 292
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    return-object v0
.end method

.method public static getApplicationName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 572
    :try_start_0
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 573
    sget-object v1, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 575
    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 580
    :goto_0
    return-object v0

    .line 577
    :catch_0
    move-exception v0

    .line 578
    sget-object v1, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->a:Ljava/lang/String;

    const-string v2, "Unable to determine package name"

    invoke-static {v1, v2, v0}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 580
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getAvailableCameras()Lcom/millennialmedia/internal/utils/EnvironmentUtils$AvailableCameras;
    .locals 1

    .prologue
    .line 1082
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->n:Lcom/millennialmedia/internal/utils/EnvironmentUtils$AvailableCameras;

    return-object v0
.end method

.method public static getAvailableExternalStorageSize()J
    .locals 2

    .prologue
    .line 697
    invoke-static {}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->isExternalStorageReadable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 698
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->b(Ljava/lang/String;)J

    move-result-wide v0

    .line 701
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public static getAvailableInternalStorageSize()J
    .locals 2

    .prologue
    .line 691
    invoke-static {}, Landroid/os/Environment;->getRootDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->b(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getAvailableStorageSize()J
    .locals 4

    .prologue
    .line 707
    const-wide/16 v0, 0x0

    .line 709
    invoke-static {}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->getAvailableInternalStorageSize()J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 710
    invoke-static {}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->getAvailableExternalStorageSize()J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 712
    return-wide v0
.end method

.method public static getBatteryLevel()Ljava/lang/Integer;
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, -0x1

    .line 649
    invoke-static {}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->u()Landroid/content/Intent;

    move-result-object v1

    .line 650
    if-nez v1, :cond_1

    .line 663
    :cond_0
    :goto_0
    return-object v0

    .line 654
    :cond_1
    const-string v2, "scale"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 655
    const-string v3, "level"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 657
    if-eq v2, v4, :cond_0

    if-eq v1, v4, :cond_0

    .line 661
    int-to-float v0, v1

    int-to-float v1, v2

    div-float/2addr v0, v1

    .line 663
    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public static getCellSignalDbm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 898
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->m:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 899
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->m:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 902
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getConfigOrientationFromRequestedOrientation(I)I
    .locals 1

    .prologue
    .line 1005
    packed-switch p0, :pswitch_data_0

    .line 1025
    :pswitch_0
    invoke-static {}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->getCurrentConfigOrientation()I

    move-result v0

    :goto_0
    return v0

    .line 1011
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 1018
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    .line 1022
    :pswitch_3
    const/4 v0, 0x0

    goto :goto_0

    .line 1005
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static getCurrentConfigOrientation()I
    .locals 1

    .prologue
    .line 957
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    return v0
.end method

.method public static getCurrentConfigOrientationString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 963
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    packed-switch v0, :pswitch_data_0

    .line 970
    invoke-static {}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->getNaturalConfigOrientationString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 965
    :pswitch_0
    const-string v0, "portrait"

    goto :goto_0

    .line 967
    :pswitch_1
    const-string v0, "landscape"

    goto :goto_0

    .line 963
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getDisplayDensity()F
    .locals 1

    .prologue
    .line 298
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    return v0
.end method

.method public static getDisplayDensityDpi()I
    .locals 1

    .prologue
    .line 304
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    return v0
.end method

.method public static getDisplayHeight()I
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 311
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 313
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 315
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 316
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 317
    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 319
    iget v0, v1, Landroid/graphics/Point;->y:I

    .line 322
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    goto :goto_0
.end method

.method public static getDisplayWidth()I
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 329
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 331
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 333
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 334
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 335
    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 337
    iget v0, v1, Landroid/graphics/Point;->x:I

    .line 340
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    goto :goto_0
.end method

.method public static declared-synchronized getHashedDeviceId(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 436
    const-class v2, Lcom/millennialmedia/internal/utils/EnvironmentUtils;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    .line 437
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v3, "android_id"

    invoke-static {v1, v3}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 439
    if-nez v3, :cond_0

    .line 453
    :goto_0
    monitor-exit v2

    return-object v0

    .line 444
    :cond_0
    :try_start_1
    invoke-static {p0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 445
    const-string v4, "UTF-8"

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v1

    .line 447
    invoke-static {v1}, Lcom/millennialmedia/internal/utils/Utils;->a([B)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 449
    :catch_0
    move-exception v1

    .line 450
    :try_start_2
    sget-object v4, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception calculating <"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "> hashed device id with ANDROID_ID <"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ">"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 436
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public static getIpAddress()Ljava/lang/String;
    .locals 4

    .prologue
    .line 909
    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->list(Ljava/util/Enumeration;)Ljava/util/ArrayList;

    move-result-object v0

    .line 910
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/NetworkInterface;

    .line 911
    invoke-virtual {v0}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->list(Ljava/util/Enumeration;)Ljava/util/ArrayList;

    move-result-object v0

    .line 912
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    .line 913
    invoke-virtual {v0}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v3

    if-nez v3, :cond_1

    .line 917
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 919
    instance-of v0, v0, Ljava/net/Inet4Address;

    if-eqz v0, :cond_2

    .line 936
    :goto_0
    return-object v1

    .line 922
    :cond_2
    const/16 v0, 0x25

    invoke-virtual {v1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 924
    if-gez v0, :cond_3

    move-object v0, v1

    :goto_1
    move-object v1, v0

    .line 927
    goto :goto_0

    .line 924
    :cond_3
    const/4 v2, 0x0

    .line 925
    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_1

    .line 932
    :catch_0
    move-exception v0

    .line 933
    sget-object v1, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->a:Ljava/lang/String;

    const-string v2, "Unable to determine IP address for device"

    invoke-static {v1, v2, v0}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 936
    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getLocaleCountry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 565
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getLocaleLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 559
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getLocation()Landroid/location/Location;
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingPermission"
        }
    .end annotation

    .prologue
    .line 1033
    sget-boolean v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->g:Z

    if-eqz v0, :cond_0

    .line 1034
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    const-string v1, "location"

    .line 1035
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 1037
    if-eqz v0, :cond_0

    .line 1038
    const-string v1, "passive"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    .line 1042
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getMacAddress()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingPermission"
        }
    .end annotation

    .prologue
    .line 943
    sget-boolean v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->f:Z

    if-eqz v0, :cond_0

    .line 944
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 946
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    .line 948
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v0

    .line 951
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getMcc()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 499
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 500
    iget v1, v0, Landroid/content/res/Configuration;->mcc:I

    if-nez v1, :cond_1

    .line 501
    invoke-static {}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->getNetworkOperator()Ljava/lang/String;

    move-result-object v0

    .line 502
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x6

    if-lt v1, v2, :cond_0

    .line 504
    const/4 v1, 0x0

    const/4 v2, 0x3

    :try_start_0
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 515
    :goto_0
    return-object v0

    .line 505
    :catch_0
    move-exception v0

    .line 506
    sget-object v1, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->a:Ljava/lang/String;

    const-string v2, "Unable to parse mcc from network operator"

    invoke-static {v1, v2, v0}, Lcom/millennialmedia/MMLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 510
    :cond_0
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->a:Ljava/lang/String;

    const-string v1, "Unable to retrieve mcc"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    const/4 v0, 0x0

    goto :goto_0

    .line 515
    :cond_1
    iget v0, v0, Landroid/content/res/Configuration;->mcc:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public static getMillennialDir()Ljava/io/File;
    .locals 2

    .prologue
    .line 1149
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/.com.millennialmedia/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1150
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1151
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 1153
    return-object v1
.end method

.method public static getMnc()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 522
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 523
    iget v1, v0, Landroid/content/res/Configuration;->mnc:I

    if-nez v1, :cond_1

    .line 524
    invoke-static {}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->getNetworkOperator()Ljava/lang/String;

    move-result-object v0

    .line 525
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x6

    if-lt v1, v2, :cond_0

    .line 527
    const/4 v1, 0x3

    :try_start_0
    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 538
    :goto_0
    return-object v0

    .line 528
    :catch_0
    move-exception v0

    .line 529
    sget-object v1, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->a:Ljava/lang/String;

    const-string v2, "Unable to parse mnc from network operator"

    invoke-static {v1, v2, v0}, Lcom/millennialmedia/MMLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 533
    :cond_0
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->a:Ljava/lang/String;

    const-string v1, "Unable to retrieve mnc"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    const/4 v0, 0x0

    goto :goto_0

    .line 538
    :cond_1
    iget v0, v0, Landroid/content/res/Configuration;->mnc:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public static getNaturalConfigOrientation()I
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x2

    .line 976
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    const-string v3, "window"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 978
    sget-object v3, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    .line 979
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 981
    iget v4, v3, Landroid/content/res/Configuration;->orientation:I

    if-ne v4, v1, :cond_1

    if-eqz v0, :cond_0

    if-ne v0, v1, :cond_1

    :cond_0
    move v0, v1

    .line 988
    :goto_0
    return v0

    .line 984
    :cond_1
    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v2, :cond_3

    if-eq v0, v2, :cond_2

    const/4 v3, 0x3

    if-ne v0, v3, :cond_3

    :cond_2
    move v0, v1

    .line 986
    goto :goto_0

    :cond_3
    move v0, v2

    .line 988
    goto :goto_0
.end method

.method public static getNaturalConfigOrientationString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 995
    invoke-static {}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->getNaturalConfigOrientation()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 996
    const-string v0, "landscape"

    .line 999
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "portrait"

    goto :goto_0
.end method

.method public static getNetworkConnectionType()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 788
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    const-string v1, "connectivity"

    .line 789
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 791
    if-nez v0, :cond_0

    .line 792
    const-string v0, "unknown"

    .line 892
    :goto_0
    return-object v0

    .line 795
    :cond_0
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 796
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-ne v1, v2, :cond_3

    .line 797
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    .line 799
    if-ne v1, v2, :cond_1

    .line 800
    const-string v0, "wifi"

    goto :goto_0

    .line 801
    :cond_1
    if-nez v1, :cond_2

    .line 802
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 879
    const-string v0, "unknown"

    goto :goto_0

    .line 804
    :pswitch_0
    const-string v0, "1xrtt"

    goto :goto_0

    .line 809
    :pswitch_1
    const-string v0, "cdma"

    goto :goto_0

    .line 814
    :pswitch_2
    const-string v0, "edge"

    goto :goto_0

    .line 819
    :pswitch_3
    const-string v0, "ehrpd"

    goto :goto_0

    .line 824
    :pswitch_4
    const-string v0, "evdo_0"

    goto :goto_0

    .line 829
    :pswitch_5
    const-string v0, "evdo_a"

    goto :goto_0

    .line 834
    :pswitch_6
    const-string v0, "evdo_b"

    goto :goto_0

    .line 839
    :pswitch_7
    const-string v0, "gprs"

    goto :goto_0

    .line 844
    :pswitch_8
    const-string v0, "hsdpa"

    goto :goto_0

    .line 849
    :pswitch_9
    const-string v0, "hspa"

    goto :goto_0

    .line 854
    :pswitch_a
    const-string v0, "hspap"

    goto :goto_0

    .line 859
    :pswitch_b
    const-string v0, "hsupa"

    goto :goto_0

    .line 864
    :pswitch_c
    const-string v0, "iden"

    goto :goto_0

    .line 869
    :pswitch_d
    const-string v0, "lte"

    goto :goto_0

    .line 874
    :pswitch_e
    const-string v0, "umts"

    goto :goto_0

    .line 885
    :cond_2
    const-string v0, "unknown"

    goto :goto_0

    .line 889
    :cond_3
    const-string v0, "offline"

    goto :goto_0

    .line 802
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_2
        :pswitch_e
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_8
        :pswitch_b
        :pswitch_9
        :pswitch_c
        :pswitch_6
        :pswitch_d
        :pswitch_3
        :pswitch_a
    .end packed-switch
.end method

.method public static getNetworkOperator()Ljava/lang/String;
    .locals 2

    .prologue
    .line 545
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 546
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v0

    .line 545
    return-object v0
.end method

.method public static getNetworkOperatorName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 552
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 553
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v0

    .line 552
    return-object v0
.end method

.method public static getPicturesDirectory()Ljava/io/File;
    .locals 3

    .prologue
    .line 1159
    const/4 v0, 0x0

    .line 1161
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v1, v2, :cond_1

    .line 1162
    sget-object v1, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getExternalMediaDirs()[Ljava/io/File;

    move-result-object v1

    .line 1164
    array-length v2, v1

    if-lez v2, :cond_0

    .line 1165
    const/4 v0, 0x0

    aget-object v0, v1, v0

    :cond_0
    move-object v1, v0

    .line 1171
    :goto_0
    if-eqz v1, :cond_2

    .line 1172
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "AOL"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1173
    invoke-static {}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->getApplicationName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1175
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 1178
    :goto_1
    return-object v0

    .line 1168
    :cond_1
    sget-object v0, Landroid/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    invoke-static {v0}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public static getUserAgent()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 594
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->o:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 595
    invoke-static {}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->s()V

    .line 597
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->o:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 598
    invoke-static {}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->t()V

    .line 602
    :cond_0
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->o:Ljava/lang/String;

    return-object v0
.end method

.method public static getVolume(I)I
    .locals 3

    .prologue
    .line 1124
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1126
    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    .line 1127
    const/4 v2, 0x1

    if-ge v1, v2, :cond_0

    .line 1128
    const/4 v0, 0x0

    .line 1133
    :goto_0
    return v0

    .line 1131
    :cond_0
    const/high16 v2, 0x42c80000    # 100.0f

    int-to-float v1, v1

    div-float v1, v2, v1

    .line 1133
    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_0
.end method

.method static synthetic h()Z
    .locals 1

    .prologue
    .line 72
    sget-boolean v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->j:Z

    return v0
.end method

.method public static hasBluetooth()Z
    .locals 2

    .prologue
    .line 1100
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.bluetooth"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static hasBluetoothPermission()Z
    .locals 1

    .prologue
    .line 1106
    sget-boolean v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->j:Z

    return v0
.end method

.method public static hasCalendarPermission()Z
    .locals 1

    .prologue
    .line 1088
    sget-boolean v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->h:Z

    return v0
.end method

.method public static hasCamera()Z
    .locals 1

    .prologue
    .line 1072
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->n:Lcom/millennialmedia/internal/utils/EnvironmentUtils$AvailableCameras;

    iget-boolean v0, v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils$AvailableCameras;->backCamera:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->n:Lcom/millennialmedia/internal/utils/EnvironmentUtils$AvailableCameras;

    iget-boolean v0, v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils$AvailableCameras;->frontCamera:Z

    if-eqz v0, :cond_1

    .line 1073
    :cond_0
    const/4 v0, 0x1

    .line 1076
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static hasFineLocationPermission()Z
    .locals 1

    .prologue
    .line 1054
    sget-boolean v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->g:Z

    return v0
.end method

.method public static hasGps()Z
    .locals 2

    .prologue
    .line 1048
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.location.gps"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static hasMicrophone()Z
    .locals 2

    .prologue
    .line 1060
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.microphone"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static hasMicrophonePermission()Z
    .locals 1

    .prologue
    .line 1066
    sget-boolean v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->l:Z

    return v0
.end method

.method public static hasNfc()Z
    .locals 2

    .prologue
    .line 1112
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.nfc"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static hasNfcPermission()Z
    .locals 1

    .prologue
    .line 1118
    sget-boolean v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->k:Z

    return v0
.end method

.method public static hasVibratePermission()Z
    .locals 1

    .prologue
    .line 1094
    sget-boolean v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->i:Z

    return v0
.end method

.method static synthetic i()Z
    .locals 1

    .prologue
    .line 72
    sget-boolean v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->k:Z

    return v0
.end method

.method public static init(Landroid/app/Application;)V
    .locals 2

    .prologue
    .line 107
    sput-object p0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->b:Landroid/app/Application;

    .line 108
    invoke-virtual {p0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    .line 111
    invoke-static {}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->o()V

    .line 116
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-ge v0, v1, :cond_0

    .line 117
    invoke-static {}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->t()V

    .line 120
    :cond_0
    invoke-static {}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->q()V

    .line 122
    invoke-static {}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->p()V

    .line 124
    invoke-static {}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->r()Lcom/millennialmedia/internal/utils/EnvironmentUtils$AvailableCameras;

    move-result-object v0

    sput-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->n:Lcom/millennialmedia/internal/utils/EnvironmentUtils$AvailableCameras;

    .line 126
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/MediaContentProvider;->verifyMediaContentProvider(Landroid/content/Context;)V

    .line 128
    new-instance v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils$1;

    invoke-direct {v0}, Lcom/millennialmedia/internal/utils/EnvironmentUtils$1;-><init>()V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThread(Ljava/lang/Runnable;)V

    .line 163
    return-void
.end method

.method public static isCalendarSupported()Z
    .locals 2

    .prologue
    .line 1192
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isDeviceIdle()Z
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .prologue
    .line 1207
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    .line 1208
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->b:Landroid/app/Application;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 1210
    invoke-virtual {v0}, Landroid/os/PowerManager;->isDeviceIdleMode()Z

    move-result v0

    .line 1213
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isDevicePlugged()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 669
    invoke-static {}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->u()Landroid/content/Intent;

    move-result-object v1

    .line 670
    if-nez v1, :cond_1

    .line 679
    :cond_0
    :goto_0
    return v0

    .line 674
    :cond_1
    const-string v2, "plugged"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 675
    if-eqz v1, :cond_0

    .line 679
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isExternalStorageReadable()Z
    .locals 2

    .prologue
    .line 741
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    .line 743
    sget-boolean v1, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->d:Z

    if-eqz v1, :cond_1

    const-string v1, "mounted"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "mounted_ro"

    .line 744
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 743
    :goto_0
    return v0

    .line 744
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isExternalStorageSupported()Z
    .locals 1

    .prologue
    .line 758
    sget-boolean v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->e:Z

    return v0
.end method

.method public static isExternalStorageWritable()Z
    .locals 2

    .prologue
    .line 750
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    .line 752
    sget-boolean v1, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->e:Z

    if-eqz v1, :cond_0

    const-string v1, "mounted"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isLimitAdTrackingEnabled(Lcom/millennialmedia/internal/utils/AdvertisingIdInfo;)Z
    .locals 2

    .prologue
    .line 424
    if-nez p0, :cond_0

    .line 425
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->a:Ljava/lang/String;

    const-string v1, "Unable to get limit ad tracking value, ad info is null"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    const/4 v0, 0x0

    .line 430
    :goto_0
    return v0

    :cond_0
    invoke-interface {p0}, Lcom/millennialmedia/internal/utils/AdvertisingIdInfo;->isLimitAdTrackingEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public static isNetworkAvailable()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 764
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    const-string v2, "connectivity"

    .line 765
    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 767
    if-nez v0, :cond_1

    .line 780
    :cond_0
    :goto_0
    return v1

    .line 770
    :cond_1
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getAllNetworkInfo()[Landroid/net/NetworkInfo;

    move-result-object v2

    .line 771
    if-eqz v2, :cond_0

    move v0, v1

    .line 772
    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 773
    aget-object v3, v2, v0

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v3

    sget-object v4, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v3, v4, :cond_2

    .line 774
    const/4 v1, 0x1

    goto :goto_0

    .line 772
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static isSmsSupported()Z
    .locals 2

    .prologue
    .line 1184
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 1186
    const-string v1, "android.hardware.telephony"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isTelSupported()Z
    .locals 2

    .prologue
    .line 1198
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 1200
    const-string v1, "android.hardware.telephony"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic j()Z
    .locals 1

    .prologue
    .line 72
    sget-boolean v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->l:Z

    return v0
.end method

.method static synthetic k()Lcom/millennialmedia/internal/utils/EnvironmentUtils$AvailableCameras;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->n:Lcom/millennialmedia/internal/utils/EnvironmentUtils$AvailableCameras;

    return-object v0
.end method

.method static synthetic l()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->m:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic m()Landroid/content/Context;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->o:Ljava/lang/String;

    return-object v0
.end method

.method private static o()V
    .locals 3

    .prologue
    .line 179
    :try_start_0
    const-string v0, "android.webkit.WebView"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 188
    return-void

    .line 181
    :catch_0
    move-exception v0

    .line 182
    sget-object v1, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->a:Ljava/lang/String;

    const-string v2, "Unable to find system webview class"

    invoke-static {v1, v2, v0}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 184
    new-instance v0, Lcom/millennialmedia/MMInitializationException;

    const-string v1, "Unable to initialize SDK. The system webview class is currently unavailable, most likely due to a system update in progress. Reinitialize the SDK after the system webview update has finished."

    invoke-direct {v0, v1}, Lcom/millennialmedia/MMInitializationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static p()V
    .locals 3

    .prologue
    .line 194
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->b:Landroid/app/Application;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 195
    new-instance v1, Lcom/millennialmedia/internal/utils/EnvironmentUtils$2;

    invoke-direct {v1}, Lcom/millennialmedia/internal/utils/EnvironmentUtils$2;-><init>()V

    const/16 v2, 0x100

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 213
    return-void
.end method

.method private static q()V
    .locals 2

    .prologue
    .line 222
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x16

    if-gt v0, v1, :cond_0

    const-string v0, "android.permission.WRITE_EXTERNAL_STORAGE"

    .line 223
    invoke-static {v0}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->checkPermissionGranted(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->e:Z

    .line 225
    const-string v0, "android.permission.READ_EXTERNAL_STORAGE"

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->checkPermissionGranted(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->d:Z

    .line 226
    const-string v0, "android.permission.ACCESS_WIFI_STATE"

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->checkPermissionGranted(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->f:Z

    .line 227
    const-string v0, "android.permission.WRITE_CALENDAR"

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->checkPermissionGranted(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->h:Z

    .line 228
    const-string v0, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->checkPermissionGranted(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->g:Z

    .line 229
    const-string v0, "android.permission.VIBRATE"

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->checkPermissionGranted(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->i:Z

    .line 230
    const-string v0, "android.permission.BLUETOOTH"

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->checkPermissionGranted(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->j:Z

    .line 231
    const-string v0, "android.permission.NFC"

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->checkPermissionGranted(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->k:Z

    .line 232
    const-string v0, "android.permission.RECORD_AUDIO"

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->checkPermissionGranted(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->l:Z

    .line 233
    return-void

    .line 223
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static r()Lcom/millennialmedia/internal/utils/EnvironmentUtils$AvailableCameras;
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x1

    .line 240
    new-instance v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils$AvailableCameras;

    invoke-direct {v0}, Lcom/millennialmedia/internal/utils/EnvironmentUtils$AvailableCameras;-><init>()V

    sput-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->n:Lcom/millennialmedia/internal/utils/EnvironmentUtils$AvailableCameras;

    .line 241
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->n:Lcom/millennialmedia/internal/utils/EnvironmentUtils$AvailableCameras;

    iput-boolean v1, v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils$AvailableCameras;->frontCamera:Z

    .line 242
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->n:Lcom/millennialmedia/internal/utils/EnvironmentUtils$AvailableCameras;

    iput-boolean v1, v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils$AvailableCameras;->backCamera:Z

    .line 245
    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v0, v2, :cond_3

    .line 246
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    const-string v2, "camera"

    .line 247
    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/camera2/CameraManager;

    .line 249
    invoke-virtual {v0}, Landroid/hardware/camera2/CameraManager;->getCameraIdList()[Ljava/lang/String;

    move-result-object v3

    .line 250
    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v1, v3, v2

    .line 251
    invoke-virtual {v0, v1}, Landroid/hardware/camera2/CameraManager;->getCameraCharacteristics(Ljava/lang/String;)Landroid/hardware/camera2/CameraCharacteristics;

    move-result-object v1

    .line 252
    sget-object v5, Landroid/hardware/camera2/CameraCharacteristics;->LENS_FACING:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {v1, v5}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 253
    if-nez v1, :cond_1

    .line 254
    sget-object v1, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->n:Lcom/millennialmedia/internal/utils/EnvironmentUtils$AvailableCameras;

    const/4 v5, 0x1

    iput-boolean v5, v1, Lcom/millennialmedia/internal/utils/EnvironmentUtils$AvailableCameras;->frontCamera:Z

    .line 250
    :cond_0
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 255
    :cond_1
    if-ne v1, v6, :cond_0

    .line 256
    sget-object v1, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->n:Lcom/millennialmedia/internal/utils/EnvironmentUtils$AvailableCameras;

    const/4 v5, 0x1

    iput-boolean v5, v1, Lcom/millennialmedia/internal/utils/EnvironmentUtils$AvailableCameras;->backCamera:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/AssertionError; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 272
    :catch_0
    move-exception v0

    .line 273
    sget-object v1, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->a:Ljava/lang/String;

    const-string v2, "Error retrieving camera information for device"

    invoke-static {v1, v2, v0}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 280
    :cond_2
    :goto_2
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->n:Lcom/millennialmedia/internal/utils/EnvironmentUtils$AvailableCameras;

    return-object v0

    .line 261
    :cond_3
    :try_start_1
    new-instance v2, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v2}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 262
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v3

    move v0, v1

    .line 263
    :goto_3
    if-ge v0, v3, :cond_2

    .line 264
    invoke-static {v0, v2}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 265
    iget v1, v2, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v1, v6, :cond_5

    .line 266
    sget-object v1, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->n:Lcom/millennialmedia/internal/utils/EnvironmentUtils$AvailableCameras;

    const/4 v4, 0x1

    iput-boolean v4, v1, Lcom/millennialmedia/internal/utils/EnvironmentUtils$AvailableCameras;->frontCamera:Z

    .line 263
    :cond_4
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 267
    :cond_5
    iget v1, v2, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-nez v1, :cond_4

    .line 268
    sget-object v1, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->n:Lcom/millennialmedia/internal/utils/EnvironmentUtils$AvailableCameras;

    const/4 v4, 0x1

    iput-boolean v4, v1, Lcom/millennialmedia/internal/utils/EnvironmentUtils$AvailableCameras;->backCamera:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/AssertionError; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_4

    .line 274
    :catch_1
    move-exception v0

    .line 278
    sget-object v1, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AssertionError while retrieving camera info <"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/AssertionError;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "> -- ignored"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private static s()V
    .locals 3

    .prologue
    .line 608
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 613
    :try_start_0
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/webkit/WebSettings;->getDefaultUserAgent(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->o:Ljava/lang/String;

    .line 614
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "User agent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->o:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 620
    :cond_0
    :goto_0
    return-void

    .line 616
    :catch_0
    move-exception v0

    .line 617
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->a:Ljava/lang/String;

    const-string v1, "Unable to get user agent from call to getDefaultUserAgent"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static t()V
    .locals 2

    .prologue
    .line 629
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Android "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->o:Ljava/lang/String;

    .line 631
    new-instance v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils$3;

    invoke-direct {v0}, Lcom/millennialmedia/internal/utils/EnvironmentUtils$3;-><init>()V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 644
    return-void
.end method

.method private static u()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 685
    sget-object v0, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->c:Landroid/content/Context;

    const/4 v1, 0x0

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
