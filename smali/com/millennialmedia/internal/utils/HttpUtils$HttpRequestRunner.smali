.class public Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/millennialmedia/internal/utils/HttpUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HttpRequestRunner"
.end annotation


# instance fields
.field private a:Ljava/util/concurrent/CountDownLatch;

.field private b:J

.field private c:Ljava/lang/String;

.field private d:Lcom/millennialmedia/internal/utils/HttpUtils$ResponseStreamer;

.field public headers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public postData:Ljava/lang/String;

.field public response:Lcom/millennialmedia/internal/utils/HttpUtils$Response;

.field public timeout:I

.field public url:Ljava/lang/String;


# direct methods
.method constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/millennialmedia/internal/utils/HttpUtils$ResponseStreamer;)V
    .locals 3

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->a:Ljava/util/concurrent/CountDownLatch;

    .line 126
    iput-wide p1, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->b:J

    .line 127
    iput-object p3, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->url:Ljava/lang/String;

    .line 128
    iput-object p4, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->postData:Ljava/lang/String;

    .line 129
    iput-object p5, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->c:Ljava/lang/String;

    .line 130
    iput p6, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->timeout:I

    .line 131
    iput-object p7, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->d:Lcom/millennialmedia/internal/utils/HttpUtils$ResponseStreamer;

    .line 132
    return-void
.end method


# virtual methods
.method a(J)Lcom/millennialmedia/internal/utils/HttpUtils$Response;
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 167
    :try_start_0
    iget-object v0, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->a:Ljava/util/concurrent/CountDownLatch;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p1, p2, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->response:Lcom/millennialmedia/internal/utils/HttpUtils$Response;

    .line 184
    :goto_0
    return-object v0

    .line 171
    :cond_0
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 172
    invoke-static {}, Lcom/millennialmedia/internal/utils/HttpUtils;->a()Ljava/lang/String;

    move-result-object v0

    .line 173
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    const-string v2, "HTTP request timed out.\n\trequestId: %d\n\twait time: %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-wide v6, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->b:J

    .line 174
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    .line 173
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 172
    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    :cond_1
    new-instance v0, Lcom/millennialmedia/internal/utils/HttpUtils$Response;

    const/16 v1, 0x198

    invoke-direct {v0, v1}, Lcom/millennialmedia/internal/utils/HttpUtils$Response;-><init>(I)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 180
    :catch_0
    move-exception v0

    .line 181
    invoke-static {}, Lcom/millennialmedia/internal/utils/HttpUtils;->a()Ljava/lang/String;

    move-result-object v0

    .line 182
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    const-string v2, "Http request was interrupted.\n\trequestId: %d"

    new-array v3, v9, [Ljava/lang/Object;

    iget-wide v4, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 181
    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    new-instance v0, Lcom/millennialmedia/internal/utils/HttpUtils$Response;

    const/16 v1, 0x190

    invoke-direct {v0, v1}, Lcom/millennialmedia/internal/utils/HttpUtils$Response;-><init>(I)V

    goto :goto_0
.end method

.method public run()V
    .locals 15

    .prologue
    const/16 v9, 0x190

    const/4 v5, 0x0

    .line 192
    new-instance v2, Lcom/millennialmedia/internal/utils/HttpUtils$Response;

    invoke-direct {v2}, Lcom/millennialmedia/internal/utils/HttpUtils$Response;-><init>()V

    iput-object v2, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->response:Lcom/millennialmedia/internal/utils/HttpUtils$Response;

    .line 199
    :try_start_0
    new-instance v2, Ljava/net/URL;

    iget-object v3, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->url:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 200
    invoke-virtual {v2}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    check-cast v2, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 201
    :try_start_1
    iget v3, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->timeout:I

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 202
    iget v3, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->timeout:I

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 203
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 204
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 205
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 206
    const-string v3, "User-Agent"

    invoke-static {}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->getUserAgent()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    iget-object v3, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->headers:Ljava/util/Map;

    if-eqz v3, :cond_2

    .line 209
    iget-object v3, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->headers:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 210
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v4, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 289
    :catch_0
    move-exception v3

    move-object v3, v5

    move-object v4, v5

    .line 290
    :goto_1
    :try_start_2
    iget-object v6, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->response:Lcom/millennialmedia/internal/utils/HttpUtils$Response;

    const/16 v7, 0x198

    iput v7, v6, Lcom/millennialmedia/internal/utils/HttpUtils$Response;->code:I

    .line 292
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 293
    invoke-static {}, Lcom/millennialmedia/internal/utils/HttpUtils;->a()Ljava/lang/String;

    move-result-object v6

    .line 294
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    const-string v8, "HTTP request socket timed out.\n\trequestId: %d"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-wide v12, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->b:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 293
    invoke-static {v6, v7}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_5

    .line 309
    :cond_0
    invoke-static {v4}, Lcom/millennialmedia/internal/utils/IOUtils;->closeStream(Ljava/io/Closeable;)Z

    .line 310
    invoke-static {v5}, Lcom/millennialmedia/internal/utils/IOUtils;->closeStream(Ljava/io/Closeable;)Z

    .line 311
    invoke-static {v3}, Lcom/millennialmedia/internal/utils/IOUtils;->closeStream(Ljava/io/Closeable;)Z

    .line 313
    if-eqz v2, :cond_1

    .line 314
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 318
    :cond_1
    iget-object v2, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 320
    :goto_2
    return-void

    .line 214
    :cond_2
    :try_start_3
    instance-of v3, v2, Ljavax/net/ssl/HttpsURLConnection;

    if-eqz v3, :cond_4

    .line 215
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 216
    invoke-static {}, Lcom/millennialmedia/internal/utils/HttpUtils;->a()Ljava/lang/String;

    move-result-object v3

    const-string v4, "HttpsURLConnection created"

    invoke-static {v3, v4}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    :cond_3
    invoke-static {}, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->getInstance()Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;

    move-result-object v4

    .line 219
    if-eqz v4, :cond_4

    .line 220
    move-object v0, v2

    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;

    move-object v3, v0

    invoke-virtual {v3, v4}, Ljavax/net/ssl/HttpsURLConnection;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 225
    :cond_4
    iget-object v3, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->postData:Ljava/lang/String;

    if-nez v3, :cond_9

    .line 226
    const-string v3, "GET"

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 227
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->connect()V

    .line 244
    :goto_3
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v3

    if-eqz v3, :cond_5

    instance-of v3, v2, Ljavax/net/ssl/HttpsURLConnection;

    if-eqz v3, :cond_5

    .line 246
    move-object v0, v2

    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;

    move-object v3, v0

    .line 248
    invoke-static {}, Lcom/millennialmedia/internal/utils/HttpUtils;->a()Ljava/lang/String;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Negotiated Cipher Suite: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljavax/net/ssl/HttpsURLConnection;->getCipherSuite()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    :cond_5
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v3

    .line 255
    if-eqz v3, :cond_b

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_b

    .line 256
    new-instance v6, Lcom/millennialmedia/internal/AdMetadata;

    invoke-direct {v6}, Lcom/millennialmedia/internal/AdMetadata;-><init>()V

    .line 257
    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_6
    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 258
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 261
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_10

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_10

    .line 262
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    const/4 v8, 0x0

    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 265
    :goto_5
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v6, v3, v4}, Lcom/millennialmedia/internal/AdMetadata;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/net/SocketTimeoutException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_4

    .line 297
    :catch_1
    move-exception v3

    move-object v4, v5

    move-object v6, v5

    move-object v14, v5

    move-object v5, v2

    move-object v2, v3

    move-object v3, v14

    .line 298
    :goto_6
    :try_start_4
    iget-object v7, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->response:Lcom/millennialmedia/internal/utils/HttpUtils$Response;

    const/16 v8, 0x190

    iput v8, v7, Lcom/millennialmedia/internal/utils/HttpUtils$Response;->code:I

    .line 300
    invoke-static {}, Lcom/millennialmedia/internal/utils/HttpUtils;->a()Ljava/lang/String;

    move-result-object v7

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    const-string v9, "Error occurred when trying to get response content.\n\trequestId: %d\n\texception: %s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-wide v12, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->b:J

    .line 301
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    .line 302
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    .line 300
    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 305
    invoke-static {}, Lcom/millennialmedia/internal/utils/HttpUtils;->a()Ljava/lang/String;

    move-result-object v7

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    const-string v9, "Debug info for requestId: %d"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-wide v12, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->b:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_6

    .line 309
    :cond_7
    invoke-static {v6}, Lcom/millennialmedia/internal/utils/IOUtils;->closeStream(Ljava/io/Closeable;)Z

    .line 310
    invoke-static {v3}, Lcom/millennialmedia/internal/utils/IOUtils;->closeStream(Ljava/io/Closeable;)Z

    .line 311
    invoke-static {v4}, Lcom/millennialmedia/internal/utils/IOUtils;->closeStream(Ljava/io/Closeable;)Z

    .line 313
    if-eqz v5, :cond_8

    .line 314
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 318
    :cond_8
    iget-object v2, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_2

    .line 231
    :cond_9
    const/4 v3, 0x1

    :try_start_5
    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 232
    const-string v3, "POST"

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 233
    const-string v3, "Content-Type"

    iget-object v4, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->c:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    const-string v3, "Content-Length"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->postData:Ljava/lang/String;

    const-string v7, "UTF-8"

    invoke-virtual {v6, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v6

    array-length v6, v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;
    :try_end_5
    .catch Ljava/net/SocketTimeoutException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v3

    .line 237
    :try_start_6
    iget-object v4, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->postData:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/millennialmedia/internal/utils/IOUtils;->write(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 239
    invoke-virtual {v3}, Ljava/io/OutputStream;->flush()V

    .line 240
    invoke-static {v3}, Lcom/millennialmedia/internal/utils/IOUtils;->closeStream(Ljava/io/Closeable;)Z
    :try_end_6
    .catch Ljava/net/SocketTimeoutException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    goto/16 :goto_3

    .line 289
    :catch_2
    move-exception v4

    move-object v4, v5

    move-object v14, v5

    move-object v5, v3

    move-object v3, v14

    goto/16 :goto_1

    .line 269
    :cond_a
    :try_start_7
    iget-object v3, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->response:Lcom/millennialmedia/internal/utils/HttpUtils$Response;

    iput-object v6, v3, Lcom/millennialmedia/internal/utils/HttpUtils$Response;->adMetadata:Lcom/millennialmedia/internal/AdMetadata;

    .line 272
    :cond_b
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v4

    .line 273
    iget-object v3, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->response:Lcom/millennialmedia/internal/utils/HttpUtils$Response;

    iput v4, v3, Lcom/millennialmedia/internal/utils/HttpUtils$Response;->code:I

    .line 275
    const/16 v3, 0xc8

    if-ne v4, v3, :cond_d

    .line 276
    iget-object v3, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->response:Lcom/millennialmedia/internal/utils/HttpUtils$Response;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/millennialmedia/internal/utils/HttpUtils$Response;->contentType:Ljava/lang/String;

    .line 277
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_7
    .catch Ljava/net/SocketTimeoutException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-result-object v3

    .line 278
    :try_start_8
    iget-object v4, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->d:Lcom/millennialmedia/internal/utils/HttpUtils$ResponseStreamer;

    iget-object v6, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->response:Lcom/millennialmedia/internal/utils/HttpUtils$Response;

    invoke-interface {v4, v3, v6}, Lcom/millennialmedia/internal/utils/HttpUtils$ResponseStreamer;->streamContent(Ljava/io/InputStream;Lcom/millennialmedia/internal/utils/HttpUtils$Response;)V
    :try_end_8
    .catch Ljava/net/SocketTimeoutException; {:try_start_8 .. :try_end_8} :catch_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    move-object v4, v3

    move-object v3, v5

    .line 309
    :goto_7
    invoke-static {v4}, Lcom/millennialmedia/internal/utils/IOUtils;->closeStream(Ljava/io/Closeable;)Z

    .line 310
    invoke-static {v5}, Lcom/millennialmedia/internal/utils/IOUtils;->closeStream(Ljava/io/Closeable;)Z

    .line 311
    invoke-static {v3}, Lcom/millennialmedia/internal/utils/IOUtils;->closeStream(Ljava/io/Closeable;)Z

    .line 313
    if-eqz v2, :cond_c

    .line 314
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 318
    :cond_c
    iget-object v2, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_2

    .line 280
    :cond_d
    if-lt v4, v9, :cond_f

    .line 281
    :try_start_9
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;
    :try_end_9
    .catch Ljava/net/SocketTimeoutException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move-result-object v3

    .line 282
    :try_start_a
    iget-object v6, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->response:Lcom/millennialmedia/internal/utils/HttpUtils$Response;

    invoke-static {v3}, Lcom/millennialmedia/internal/utils/IOUtils;->convertStreamToString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/millennialmedia/internal/utils/HttpUtils$Response;->content:Ljava/lang/String;

    .line 284
    invoke-static {}, Lcom/millennialmedia/internal/utils/HttpUtils;->a()Ljava/lang/String;

    move-result-object v6

    .line 285
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    const-string v8, "HTTP ERROR.\n\trequestId: %d\n\tcode: %d\n\tmessage: %s"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-wide v12, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->b:J

    .line 286
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v9, v10

    const/4 v4, 0x2

    iget-object v10, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->response:Lcom/millennialmedia/internal/utils/HttpUtils$Response;

    iget-object v10, v10, Lcom/millennialmedia/internal/utils/HttpUtils$Response;->content:Ljava/lang/String;

    aput-object v10, v9, v4

    .line 285
    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 284
    invoke-static {v6, v4}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/net/SocketTimeoutException; {:try_start_a .. :try_end_a} :catch_9
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    move-object v4, v5

    goto :goto_7

    .line 309
    :catchall_0
    move-exception v2

    move-object v3, v5

    move-object v4, v5

    move-object v6, v5

    :goto_8
    invoke-static {v6}, Lcom/millennialmedia/internal/utils/IOUtils;->closeStream(Ljava/io/Closeable;)Z

    .line 310
    invoke-static {v3}, Lcom/millennialmedia/internal/utils/IOUtils;->closeStream(Ljava/io/Closeable;)Z

    .line 311
    invoke-static {v4}, Lcom/millennialmedia/internal/utils/IOUtils;->closeStream(Ljava/io/Closeable;)Z

    .line 313
    if-eqz v5, :cond_e

    .line 314
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 318
    :cond_e
    iget-object v3, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v2

    .line 309
    :catchall_1
    move-exception v3

    move-object v4, v5

    move-object v6, v5

    move-object v14, v5

    move-object v5, v2

    move-object v2, v3

    move-object v3, v14

    goto :goto_8

    :catchall_2
    move-exception v4

    move-object v6, v5

    move-object v14, v2

    move-object v2, v4

    move-object v4, v5

    move-object v5, v14

    goto :goto_8

    :catchall_3
    move-exception v4

    move-object v6, v3

    move-object v3, v5

    move-object v14, v2

    move-object v2, v4

    move-object v4, v5

    move-object v5, v14

    goto :goto_8

    :catchall_4
    move-exception v4

    move-object v6, v5

    move-object v14, v2

    move-object v2, v4

    move-object v4, v3

    move-object v3, v5

    move-object v5, v14

    goto :goto_8

    :catchall_5
    move-exception v6

    move-object v14, v6

    move-object v6, v4

    move-object v4, v3

    move-object v3, v5

    move-object v5, v2

    move-object v2, v14

    goto :goto_8

    :catchall_6
    move-exception v2

    goto :goto_8

    .line 297
    :catch_3
    move-exception v2

    move-object v3, v5

    move-object v4, v5

    move-object v6, v5

    goto/16 :goto_6

    :catch_4
    move-exception v4

    move-object v6, v5

    move-object v14, v2

    move-object v2, v4

    move-object v4, v5

    move-object v5, v14

    goto/16 :goto_6

    :catch_5
    move-exception v4

    move-object v6, v3

    move-object v3, v5

    move-object v14, v2

    move-object v2, v4

    move-object v4, v5

    move-object v5, v14

    goto/16 :goto_6

    :catch_6
    move-exception v4

    move-object v6, v5

    move-object v14, v2

    move-object v2, v4

    move-object v4, v3

    move-object v3, v5

    move-object v5, v14

    goto/16 :goto_6

    .line 289
    :catch_7
    move-exception v2

    move-object v2, v5

    move-object v3, v5

    move-object v4, v5

    goto/16 :goto_1

    :catch_8
    move-exception v4

    move-object v4, v3

    move-object v3, v5

    goto/16 :goto_1

    :catch_9
    move-exception v4

    move-object v4, v5

    goto/16 :goto_1

    :cond_f
    move-object v3, v5

    move-object v4, v5

    goto/16 :goto_7

    :cond_10
    move-object v4, v5

    goto/16 :goto_5
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 138
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 141
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    const-string v2, "requestId: %d\n\turl: %s\n\ttimeout: %d"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    iget-wide v4, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v6

    iget-object v4, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->url:Ljava/lang/String;

    aput-object v4, v3, v7

    const/4 v4, 0x2

    iget v5, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->timeout:I

    .line 142
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 141
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 140
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    iget-object v1, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 145
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    const-string v2, "\n\tcontent type: %s"

    new-array v3, v7, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->c:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    :cond_0
    iget-object v1, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->postData:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 149
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    const-string v2, "\n\tpost data: %s"

    new-array v3, v7, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->postData:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
