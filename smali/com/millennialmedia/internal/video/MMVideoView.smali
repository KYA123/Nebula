.class public Lcom/millennialmedia/internal/video/MMVideoView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/media/MediaPlayer$OnBufferingUpdateListener;
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnInfoListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Landroid/media/MediaPlayer$OnSeekCompleteListener;
.implements Landroid/media/MediaPlayer$OnVideoSizeChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/millennialmedia/internal/video/MMVideoView$ProgressRunnable;,
        Lcom/millennialmedia/internal/video/MMVideoView$VideoSurfaceView;,
        Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewInfo;,
        Lcom/millennialmedia/internal/video/MMVideoView$MediaController;,
        Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewListener;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/net/Uri;

.field private c:I

.field private d:I

.field private e:Landroid/media/MediaPlayer;

.field private f:Lcom/millennialmedia/internal/video/MMVideoView$VideoSurfaceView;

.field private g:Landroid/view/SurfaceHolder;

.field private h:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

.field private i:Z

.field private j:I

.field private k:Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewListener;

.field private l:Lcom/millennialmedia/internal/video/MMVideoView$MediaController;

.field private m:Lcom/b/a/a/a/k;

.field private n:Lcom/b/a/a/a/p;

.field private o:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private volatile p:I

.field private volatile q:I

.field private r:Landroid/view/SurfaceHolder$Callback;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/millennialmedia/internal/video/MMVideoView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/millennialmedia/internal/video/MMVideoView;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZZLjava/util/Map;Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewListener;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "ZZ",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewListener;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 378
    new-instance v0, Landroid/content/MutableContextWrapper;

    invoke-direct {v0, p1}, Landroid/content/MutableContextWrapper;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 70
    iput v1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->j:I

    .line 80
    iput v1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->q:I

    .line 253
    new-instance v0, Lcom/millennialmedia/internal/video/MMVideoView$1;

    invoke-direct {v0, p0}, Lcom/millennialmedia/internal/video/MMVideoView$1;-><init>(Lcom/millennialmedia/internal/video/MMVideoView;)V

    iput-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->r:Landroid/view/SurfaceHolder$Callback;

    .line 380
    if-nez p4, :cond_1

    .line 381
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->o:Ljava/util/Map;

    .line 387
    :goto_0
    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/MMVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/content/MutableContextWrapper;

    .line 389
    iput-boolean p3, p0, Lcom/millennialmedia/internal/video/MMVideoView;->i:Z

    .line 390
    iput-object p5, p0, Lcom/millennialmedia/internal/video/MMVideoView;->k:Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewListener;

    .line 392
    if-eqz p2, :cond_0

    .line 393
    const/4 v1, 0x4

    iput v1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->p:I

    .line 397
    :cond_0
    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/MMVideoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x106000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/millennialmedia/internal/video/MMVideoView;->setBackgroundColor(I)V

    .line 399
    new-instance v1, Lcom/millennialmedia/internal/video/MMVideoView$VideoSurfaceView;

    invoke-direct {v1, p0, v0}, Lcom/millennialmedia/internal/video/MMVideoView$VideoSurfaceView;-><init>(Lcom/millennialmedia/internal/video/MMVideoView;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->f:Lcom/millennialmedia/internal/video/MMVideoView$VideoSurfaceView;

    .line 400
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->f:Lcom/millennialmedia/internal/video/MMVideoView$VideoSurfaceView;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/video/MMVideoView$VideoSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->r:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 401
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->f:Lcom/millennialmedia/internal/video/MMVideoView$VideoSurfaceView;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/video/MMVideoView$VideoSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 403
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 406
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 408
    iget-object v1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->f:Lcom/millennialmedia/internal/video/MMVideoView$VideoSurfaceView;

    invoke-virtual {p0, v1, v0}, Lcom/millennialmedia/internal/video/MMVideoView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 409
    return-void

    .line 383
    :cond_1
    iput-object p4, p0, Lcom/millennialmedia/internal/video/MMVideoView;->o:Ljava/util/Map;

    goto :goto_0
.end method

.method static synthetic a(Lcom/millennialmedia/internal/video/MMVideoView;)I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->c:I

    return v0
.end method

.method static synthetic a(Lcom/millennialmedia/internal/video/MMVideoView;I)I
    .locals 0

    .prologue
    .line 40
    iput p1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->q:I

    return p1
.end method

.method static synthetic a(Lcom/millennialmedia/internal/video/MMVideoView;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->g:Landroid/view/SurfaceHolder;

    return-object p1
.end method

.method static synthetic a(Lcom/millennialmedia/internal/video/MMVideoView;Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;)Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->h:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    return-object p1
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x3

    .line 526
    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/MMVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 527
    iget-boolean v1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->i:Z

    if-nez v1, :cond_0

    .line 530
    invoke-virtual {v0, v3, v2, v2}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 534
    :goto_0
    return-void

    .line 532
    :cond_0
    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    goto :goto_0
.end method

.method static synthetic b(Lcom/millennialmedia/internal/video/MMVideoView;)I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->d:I

    return v0
.end method

.method static synthetic b(Lcom/millennialmedia/internal/video/MMVideoView;I)I
    .locals 0

    .prologue
    .line 40
    iput p1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->p:I

    return p1
.end method

.method private b()V
    .locals 2

    .prologue
    .line 539
    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/MMVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 540
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 541
    return-void
.end method

.method static synthetic c(Lcom/millennialmedia/internal/video/MMVideoView;)Landroid/view/SurfaceHolder;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->g:Landroid/view/SurfaceHolder;

    return-object v0
.end method

.method private c()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 999
    iget v1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->q:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->q:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->q:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->q:I

    const/4 v2, 0x7

    if-eq v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/millennialmedia/internal/video/MMVideoView;)Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewListener;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->k:Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewListener;

    return-object v0
.end method

.method static synthetic e(Lcom/millennialmedia/internal/video/MMVideoView;)Landroid/media/MediaPlayer;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic f(Lcom/millennialmedia/internal/video/MMVideoView;)I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->q:I

    return v0
.end method

.method static synthetic g(Lcom/millennialmedia/internal/video/MMVideoView;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/millennialmedia/internal/video/MMVideoView;->a()V

    return-void
.end method

.method static synthetic h(Lcom/millennialmedia/internal/video/MMVideoView;)I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->p:I

    return v0
.end method

.method static synthetic i(Lcom/millennialmedia/internal/video/MMVideoView;)Lcom/millennialmedia/internal/video/MMVideoView$MediaController;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->l:Lcom/millennialmedia/internal/video/MMVideoView$MediaController;

    return-object v0
.end method


# virtual methods
.method public getCurrentPosition()I
    .locals 1

    .prologue
    .line 761
    invoke-direct {p0}, Lcom/millennialmedia/internal/video/MMVideoView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 762
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    .line 765
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getDuration()I
    .locals 2

    .prologue
    .line 771
    invoke-direct {p0}, Lcom/millennialmedia/internal/video/MMVideoView;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->q:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 772
    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    .line 775
    :goto_0
    return v0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 787
    invoke-direct {p0}, Lcom/millennialmedia/internal/video/MMVideoView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public mute()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 699
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->i:Z

    .line 700
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 701
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1, v1}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 704
    :cond_0
    invoke-direct {p0}, Lcom/millennialmedia/internal/video/MMVideoView;->a()V

    .line 706
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->k:Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewListener;

    if-eqz v0, :cond_1

    .line 707
    new-instance v0, Lcom/millennialmedia/internal/video/MMVideoView$8;

    invoke-direct {v0, p0}, Lcom/millennialmedia/internal/video/MMVideoView$8;-><init>(Lcom/millennialmedia/internal/video/MMVideoView;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThread(Ljava/lang/Runnable;)V

    .line 716
    :cond_1
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->l:Lcom/millennialmedia/internal/video/MMVideoView$MediaController;

    if-eqz v0, :cond_2

    .line 717
    new-instance v0, Lcom/millennialmedia/internal/video/MMVideoView$9;

    invoke-direct {v0, p0}, Lcom/millennialmedia/internal/video/MMVideoView$9;-><init>(Lcom/millennialmedia/internal/video/MMVideoView;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThread(Ljava/lang/Runnable;)V

    .line 725
    :cond_2
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 1007
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 1009
    invoke-direct {p0}, Lcom/millennialmedia/internal/video/MMVideoView;->a()V

    .line 1012
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->o:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1013
    invoke-static {p0}, Lcom/millennialmedia/internal/utils/ViewUtils;->getActivityForView(Landroid/view/View;)Landroid/app/Activity;

    move-result-object v0

    .line 1015
    if-eqz v0, :cond_1

    .line 1016
    invoke-static {v0}, Lcom/b/a/a/a/k;->a(Landroid/app/Activity;)Lcom/b/a/a/a/k;

    move-result-object v0

    iput-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->m:Lcom/b/a/a/a/k;

    .line 1023
    :cond_0
    :goto_0
    return-void

    .line 1018
    :cond_1
    sget-object v0, Lcom/millennialmedia/internal/video/MMVideoView;->a:Ljava/lang/String;

    const-string v1, "Cannot determine the activity context. Moat video tracking disabled."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1020
    :cond_2
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1021
    sget-object v0, Lcom/millennialmedia/internal/video/MMVideoView;->a:Ljava/lang/String;

    const-string v1, "Moat ad identifiers were not provided. Moat video tracking disabled."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onBufferingUpdate(Landroid/media/MediaPlayer;I)V
    .locals 1

    .prologue
    .line 922
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->k:Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewListener;

    if-eqz v0, :cond_0

    .line 923
    new-instance v0, Lcom/millennialmedia/internal/video/MMVideoView$19;

    invoke-direct {v0, p0, p2}, Lcom/millennialmedia/internal/video/MMVideoView$19;-><init>(Lcom/millennialmedia/internal/video/MMVideoView;I)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThread(Ljava/lang/Runnable;)V

    .line 931
    :cond_0
    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 3

    .prologue
    const/4 v0, 0x6

    .line 804
    iput v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->q:I

    .line 805
    iput v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->p:I

    .line 809
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->n:Lcom/b/a/a/a/p;

    if-eqz v0, :cond_0

    .line 810
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 811
    const-string v1, "type"

    const-string v2, "AdVideoComplete"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 812
    iget-object v1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->n:Lcom/b/a/a/a/p;

    invoke-interface {v1, v0}, Lcom/b/a/a/a/p;->a(Ljava/util/Map;)V

    .line 815
    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->h:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    if-eqz v0, :cond_1

    .line 816
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->h:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    invoke-interface {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;->cancel()V

    .line 817
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->h:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    .line 820
    :cond_1
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->k:Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewListener;

    if-eqz v0, :cond_2

    .line 821
    new-instance v0, Lcom/millennialmedia/internal/video/MMVideoView$12;

    invoke-direct {v0, p0}, Lcom/millennialmedia/internal/video/MMVideoView$12;-><init>(Lcom/millennialmedia/internal/video/MMVideoView;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThread(Ljava/lang/Runnable;)V

    .line 831
    :cond_2
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->l:Lcom/millennialmedia/internal/video/MMVideoView$MediaController;

    if-eqz v0, :cond_3

    .line 832
    new-instance v0, Lcom/millennialmedia/internal/video/MMVideoView$13;

    invoke-direct {v0, p0}, Lcom/millennialmedia/internal/video/MMVideoView$13;-><init>(Lcom/millennialmedia/internal/video/MMVideoView;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThread(Ljava/lang/Runnable;)V

    .line 840
    :cond_3
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 1029
    invoke-direct {p0}, Lcom/millennialmedia/internal/video/MMVideoView;->b()V

    .line 1031
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 1032
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 1

    .prologue
    .line 846
    const/4 v0, 0x7

    iput v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->q:I

    .line 848
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->k:Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewListener;

    if-eqz v0, :cond_0

    .line 849
    new-instance v0, Lcom/millennialmedia/internal/video/MMVideoView$14;

    invoke-direct {v0, p0}, Lcom/millennialmedia/internal/video/MMVideoView$14;-><init>(Lcom/millennialmedia/internal/video/MMVideoView;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThread(Ljava/lang/Runnable;)V

    .line 858
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onInfo(Landroid/media/MediaPlayer;II)Z
    .locals 1

    .prologue
    .line 937
    const/4 v0, 0x0

    return v0
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 2

    .prologue
    .line 865
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->g:Landroid/view/SurfaceHolder;

    if-eqz v0, :cond_4

    .line 866
    invoke-direct {p0}, Lcom/millennialmedia/internal/video/MMVideoView;->a()V

    .line 867
    const/4 v0, 0x3

    iput v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->q:I

    .line 868
    iget v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->p:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 869
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->k:Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewListener;

    if-eqz v0, :cond_0

    .line 870
    new-instance v0, Lcom/millennialmedia/internal/video/MMVideoView$15;

    invoke-direct {v0, p0}, Lcom/millennialmedia/internal/video/MMVideoView$15;-><init>(Lcom/millennialmedia/internal/video/MMVideoView;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThread(Ljava/lang/Runnable;)V

    .line 879
    :cond_0
    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/MMVideoView;->start()V

    .line 907
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->l:Lcom/millennialmedia/internal/video/MMVideoView$MediaController;

    if-eqz v0, :cond_2

    .line 908
    new-instance v0, Lcom/millennialmedia/internal/video/MMVideoView$18;

    invoke-direct {v0, p0}, Lcom/millennialmedia/internal/video/MMVideoView$18;-><init>(Lcom/millennialmedia/internal/video/MMVideoView;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThread(Ljava/lang/Runnable;)V

    .line 916
    :cond_2
    return-void

    .line 881
    :cond_3
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->k:Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewListener;

    if-eqz v0, :cond_1

    .line 882
    new-instance v0, Lcom/millennialmedia/internal/video/MMVideoView$16;

    invoke-direct {v0, p0}, Lcom/millennialmedia/internal/video/MMVideoView$16;-><init>(Lcom/millennialmedia/internal/video/MMVideoView;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 893
    :cond_4
    const/4 v0, 0x2

    iput v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->q:I

    .line 895
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->k:Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewListener;

    if-eqz v0, :cond_1

    .line 896
    new-instance v0, Lcom/millennialmedia/internal/video/MMVideoView$17;

    invoke-direct {v0, p0}, Lcom/millennialmedia/internal/video/MMVideoView$17;-><init>(Lcom/millennialmedia/internal/video/MMVideoView;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 415
    check-cast p1, Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewInfo;

    .line 417
    invoke-virtual {p1}, Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewInfo;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 419
    iget v0, p1, Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewInfo;->b:I

    iput v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->p:I

    .line 420
    iget v0, p1, Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewInfo;->c:I

    iput v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->j:I

    .line 421
    iget-boolean v0, p1, Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewInfo;->d:Z

    iput-boolean v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->i:Z

    .line 423
    iget v0, p1, Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewInfo;->a:I

    if-eq v0, v1, :cond_0

    iget v0, p1, Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewInfo;->b:I

    if-ne v0, v1, :cond_1

    .line 424
    :cond_0
    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/MMVideoView;->start()V

    .line 426
    :cond_1
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 432
    new-instance v0, Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewInfo;

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewInfo;-><init>(Landroid/os/Parcelable;)V

    .line 433
    iget v1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->q:I

    iput v1, v0, Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewInfo;->a:I

    .line 434
    iget v1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->p:I

    iput v1, v0, Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewInfo;->b:I

    .line 435
    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/MMVideoView;->getCurrentPosition()I

    move-result v1

    iput v1, v0, Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewInfo;->c:I

    .line 436
    iget-boolean v1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->i:Z

    iput-boolean v1, v0, Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewInfo;->d:Z

    .line 437
    iget-object v1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->b:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewInfo;->e:Ljava/lang/String;

    .line 439
    return-object v0
.end method

.method public onSeekComplete(Landroid/media/MediaPlayer;)V
    .locals 1

    .prologue
    .line 944
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->k:Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewListener;

    if-eqz v0, :cond_0

    .line 945
    new-instance v0, Lcom/millennialmedia/internal/video/MMVideoView$20;

    invoke-direct {v0, p0}, Lcom/millennialmedia/internal/video/MMVideoView$20;-><init>(Lcom/millennialmedia/internal/video/MMVideoView;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThread(Ljava/lang/Runnable;)V

    .line 954
    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->l:Lcom/millennialmedia/internal/video/MMVideoView$MediaController;

    if-eqz v0, :cond_1

    .line 955
    new-instance v0, Lcom/millennialmedia/internal/video/MMVideoView$21;

    invoke-direct {v0, p0}, Lcom/millennialmedia/internal/video/MMVideoView$21;-><init>(Lcom/millennialmedia/internal/video/MMVideoView;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThread(Ljava/lang/Runnable;)V

    .line 963
    :cond_1
    return-void
.end method

.method public onVideoSizeChanged(Landroid/media/MediaPlayer;II)V
    .locals 3

    .prologue
    .line 980
    if-eqz p3, :cond_0

    if-eqz p2, :cond_0

    .line 981
    iput p2, p0, Lcom/millennialmedia/internal/video/MMVideoView;->c:I

    .line 982
    iput p3, p0, Lcom/millennialmedia/internal/video/MMVideoView;->d:I

    .line 984
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->g:Landroid/view/SurfaceHolder;

    if-eqz v0, :cond_0

    .line 985
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->g:Landroid/view/SurfaceHolder;

    iget v1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->c:I

    iget v2, p0, Lcom/millennialmedia/internal/video/MMVideoView;->d:I

    invoke-interface {v0, v1, v2}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    .line 986
    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/MMVideoView;->requestLayout()V

    .line 989
    :cond_0
    return-void
.end method

.method public pause()V
    .locals 2

    .prologue
    const/4 v1, 0x5

    .line 656
    invoke-direct {p0}, Lcom/millennialmedia/internal/video/MMVideoView;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 657
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 659
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->k:Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewListener;

    if-eqz v0, :cond_0

    .line 660
    new-instance v0, Lcom/millennialmedia/internal/video/MMVideoView$6;

    invoke-direct {v0, p0}, Lcom/millennialmedia/internal/video/MMVideoView$6;-><init>(Lcom/millennialmedia/internal/video/MMVideoView;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThread(Ljava/lang/Runnable;)V

    .line 669
    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->l:Lcom/millennialmedia/internal/video/MMVideoView$MediaController;

    if-eqz v0, :cond_1

    .line 670
    new-instance v0, Lcom/millennialmedia/internal/video/MMVideoView$7;

    invoke-direct {v0, p0}, Lcom/millennialmedia/internal/video/MMVideoView$7;-><init>(Lcom/millennialmedia/internal/video/MMVideoView;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThread(Ljava/lang/Runnable;)V

    .line 679
    :cond_1
    iput v1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->q:I

    .line 680
    iput v1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->p:I

    .line 682
    :cond_2
    return-void
.end method

.method public release()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 447
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->h:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    if-eqz v0, :cond_0

    .line 448
    iget-object v1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->h:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    monitor-enter v1

    .line 449
    :try_start_0
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->h:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    invoke-interface {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;->cancel()V

    .line 450
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->h:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    .line 454
    const/4 v0, 0x0

    iput v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->q:I

    .line 455
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 458
    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 460
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 461
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 462
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 463
    iput-object v2, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    .line 465
    iput v3, p0, Lcom/millennialmedia/internal/video/MMVideoView;->q:I

    .line 467
    :cond_1
    return-void

    .line 455
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public restart()V
    .locals 2

    .prologue
    .line 558
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-gt v0, v1, :cond_1

    .line 559
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->b:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 569
    :goto_0
    return-void

    .line 563
    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->b:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/millennialmedia/internal/video/MMVideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 568
    :goto_1
    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/MMVideoView;->start()V

    goto :goto_0

    .line 565
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/millennialmedia/internal/video/MMVideoView;->seekTo(I)V

    goto :goto_1
.end method

.method public seekTo(I)V
    .locals 1

    .prologue
    .line 687
    invoke-direct {p0}, Lcom/millennialmedia/internal/video/MMVideoView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 688
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 689
    const/4 v0, 0x0

    iput v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->j:I

    .line 694
    :goto_0
    return-void

    .line 692
    :cond_0
    iput p1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->j:I

    goto :goto_0
.end method

.method public setMediaController(Lcom/millennialmedia/internal/video/MMVideoView$MediaController;)V
    .locals 0

    .prologue
    .line 552
    iput-object p1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->l:Lcom/millennialmedia/internal/video/MMVideoView$MediaController;

    .line 553
    return-void
.end method

.method public setVideoPath(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 546
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/millennialmedia/internal/video/MMVideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 547
    return-void
.end method

.method public setVideoURI(Landroid/net/Uri;)V
    .locals 4

    .prologue
    const/4 v3, 0x7

    .line 472
    iput-object p1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->b:Landroid/net/Uri;

    .line 475
    if-nez p1, :cond_1

    .line 521
    :cond_0
    :goto_0
    return-void

    .line 479
    :cond_1
    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/MMVideoView;->release()V

    .line 481
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    .line 484
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->g:Landroid/view/SurfaceHolder;

    if-eqz v0, :cond_2

    .line 485
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->g:Landroid/view/SurfaceHolder;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 488
    :cond_2
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 489
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 490
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 491
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    .line 492
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    .line 493
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 494
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    .line 497
    :try_start_0
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/MMVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    .line 499
    const/4 v0, 0x1

    iput v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->q:I

    .line 503
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 505
    :catch_0
    move-exception v0

    .line 506
    sget-object v1, Lcom/millennialmedia/internal/video/MMVideoView;->a:Ljava/lang/String;

    const-string v2, "An error occurred preparing the VideoPlayer."

    invoke-static {v1, v2, v0}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 508
    iput v3, p0, Lcom/millennialmedia/internal/video/MMVideoView;->q:I

    .line 509
    iput v3, p0, Lcom/millennialmedia/internal/video/MMVideoView;->p:I

    .line 511
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->k:Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewListener;

    if-eqz v0, :cond_0

    .line 512
    new-instance v0, Lcom/millennialmedia/internal/video/MMVideoView$2;

    invoke-direct {v0, p0}, Lcom/millennialmedia/internal/video/MMVideoView$2;-><init>(Lcom/millennialmedia/internal/video/MMVideoView;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public setVideoViewListener(Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewListener;)V
    .locals 0

    .prologue
    .line 781
    iput-object p1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->k:Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewListener;

    .line 782
    return-void
.end method

.method public start()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 574
    invoke-direct {p0}, Lcom/millennialmedia/internal/video/MMVideoView;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    iget v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->q:I

    if-eq v0, v3, :cond_6

    .line 575
    iget-boolean v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->i:Z

    if-eqz v0, :cond_0

    .line 576
    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/MMVideoView;->mute()V

    .line 579
    :cond_0
    iget v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->j:I

    if-eqz v0, :cond_1

    .line 580
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    iget v1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->j:I

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 581
    const/4 v0, 0x0

    iput v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->j:I

    .line 585
    :cond_1
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->o:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->m:Lcom/b/a/a/a/k;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->n:Lcom/b/a/a/a/p;

    if-nez v0, :cond_2

    .line 588
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->m:Lcom/b/a/a/a/k;

    const-string v1, "millennialmedianativeapp775281030677"

    invoke-virtual {v0, v1}, Lcom/b/a/a/a/k;->a(Ljava/lang/String;)Lcom/b/a/a/a/p;

    move-result-object v0

    iput-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->n:Lcom/b/a/a/a/p;

    .line 589
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->n:Lcom/b/a/a/a/p;

    iget-object v1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->o:Ljava/util/Map;

    iget-object v2, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    invoke-interface {v0, v1, v2, p0}, Lcom/b/a/a/a/p;->a(Ljava/util/Map;Landroid/media/MediaPlayer;Landroid/view/View;)Z

    .line 591
    sget-object v0, Lcom/millennialmedia/internal/video/MMVideoView;->a:Ljava/lang/String;

    const-string v1, "Moat video tracking enabled."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 594
    :cond_2
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 596
    iput v3, p0, Lcom/millennialmedia/internal/video/MMVideoView;->q:I

    .line 597
    iput v3, p0, Lcom/millennialmedia/internal/video/MMVideoView;->p:I

    .line 599
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->k:Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewListener;

    if-eqz v0, :cond_3

    .line 600
    new-instance v0, Lcom/millennialmedia/internal/video/MMVideoView$3;

    invoke-direct {v0, p0}, Lcom/millennialmedia/internal/video/MMVideoView$3;-><init>(Lcom/millennialmedia/internal/video/MMVideoView;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThread(Ljava/lang/Runnable;)V

    .line 609
    :cond_3
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->l:Lcom/millennialmedia/internal/video/MMVideoView$MediaController;

    if-eqz v0, :cond_4

    .line 610
    new-instance v0, Lcom/millennialmedia/internal/video/MMVideoView$4;

    invoke-direct {v0, p0}, Lcom/millennialmedia/internal/video/MMVideoView$4;-><init>(Lcom/millennialmedia/internal/video/MMVideoView;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThread(Ljava/lang/Runnable;)V

    .line 619
    :cond_4
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->h:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    if-eqz v0, :cond_5

    .line 620
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->h:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    invoke-interface {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;->cancel()V

    .line 623
    :cond_5
    new-instance v0, Lcom/millennialmedia/internal/video/MMVideoView$ProgressRunnable;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/millennialmedia/internal/video/MMVideoView$ProgressRunnable;-><init>(Lcom/millennialmedia/internal/video/MMVideoView;Lcom/millennialmedia/internal/video/MMVideoView$1;)V

    const-wide/16 v2, 0x64

    invoke-static {v0, v2, v3}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThreadDelayed(Ljava/lang/Runnable;J)Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    move-result-object v0

    iput-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->h:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    .line 628
    :goto_0
    return-void

    .line 626
    :cond_6
    iput v3, p0, Lcom/millennialmedia/internal/video/MMVideoView;->p:I

    goto :goto_0
.end method

.method public stop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 633
    invoke-direct {p0}, Lcom/millennialmedia/internal/video/MMVideoView;->b()V

    .line 635
    invoke-direct {p0}, Lcom/millennialmedia/internal/video/MMVideoView;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->q:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    .line 636
    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 638
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->k:Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewListener;

    if-eqz v0, :cond_1

    .line 639
    new-instance v0, Lcom/millennialmedia/internal/video/MMVideoView$5;

    invoke-direct {v0, p0}, Lcom/millennialmedia/internal/video/MMVideoView$5;-><init>(Lcom/millennialmedia/internal/video/MMVideoView;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThread(Ljava/lang/Runnable;)V

    .line 648
    :cond_1
    iput v2, p0, Lcom/millennialmedia/internal/video/MMVideoView;->q:I

    .line 649
    iput v2, p0, Lcom/millennialmedia/internal/video/MMVideoView;->p:I

    .line 651
    :cond_2
    return-void
.end method

.method public unmute()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 730
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->i:Z

    .line 731
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 732
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1, v1}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 735
    :cond_0
    invoke-direct {p0}, Lcom/millennialmedia/internal/video/MMVideoView;->a()V

    .line 737
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->k:Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewListener;

    if-eqz v0, :cond_1

    .line 738
    new-instance v0, Lcom/millennialmedia/internal/video/MMVideoView$10;

    invoke-direct {v0, p0}, Lcom/millennialmedia/internal/video/MMVideoView$10;-><init>(Lcom/millennialmedia/internal/video/MMVideoView;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThread(Ljava/lang/Runnable;)V

    .line 747
    :cond_1
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->l:Lcom/millennialmedia/internal/video/MMVideoView$MediaController;

    if-eqz v0, :cond_2

    .line 748
    new-instance v0, Lcom/millennialmedia/internal/video/MMVideoView$11;

    invoke-direct {v0, p0}, Lcom/millennialmedia/internal/video/MMVideoView$11;-><init>(Lcom/millennialmedia/internal/video/MMVideoView;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThread(Ljava/lang/Runnable;)V

    .line 756
    :cond_2
    return-void
.end method

.method public videoSkipped()V
    .locals 3

    .prologue
    .line 793
    iget-object v0, p0, Lcom/millennialmedia/internal/video/MMVideoView;->n:Lcom/b/a/a/a/p;

    if-eqz v0, :cond_0

    .line 794
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 795
    const-string v1, "type"

    const-string v2, "AdSkipped"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 796
    iget-object v1, p0, Lcom/millennialmedia/internal/video/MMVideoView;->n:Lcom/b/a/a/a/p;

    invoke-interface {v1, v0}, Lcom/b/a/a/a/p;->a(Ljava/util/Map;)V

    .line 798
    :cond_0
    return-void
.end method
