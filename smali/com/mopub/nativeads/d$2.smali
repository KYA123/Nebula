.class Lcom/mopub/nativeads/d$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mopub/nativeads/MoPubNative$MoPubNativeNetworkListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mopub/nativeads/d;-><init>(Ljava/util/List;Landroid/os/Handler;Lcom/mopub/nativeads/AdRendererRegistry;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/mopub/nativeads/d;


# direct methods
.method constructor <init>(Lcom/mopub/nativeads/d;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/mopub/nativeads/d$2;->a:Lcom/mopub/nativeads/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNativeFail(Lcom/mopub/nativeads/NativeErrorCode;)V
    .locals 4

    .prologue
    .line 117
    iget-object v0, p0, Lcom/mopub/nativeads/d$2;->a:Lcom/mopub/nativeads/d;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/mopub/nativeads/d;->b:Z

    .line 121
    iget-object v0, p0, Lcom/mopub/nativeads/d$2;->a:Lcom/mopub/nativeads/d;

    iget v0, v0, Lcom/mopub/nativeads/d;->e:I

    sget-object v1, Lcom/mopub/nativeads/d;->a:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_0

    .line 122
    iget-object v0, p0, Lcom/mopub/nativeads/d$2;->a:Lcom/mopub/nativeads/d;

    invoke-virtual {v0}, Lcom/mopub/nativeads/d;->e()V

    .line 129
    :goto_0
    return-void

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/mopub/nativeads/d$2;->a:Lcom/mopub/nativeads/d;

    invoke-virtual {v0}, Lcom/mopub/nativeads/d;->d()V

    .line 127
    iget-object v0, p0, Lcom/mopub/nativeads/d$2;->a:Lcom/mopub/nativeads/d;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/mopub/nativeads/d;->c:Z

    .line 128
    iget-object v0, p0, Lcom/mopub/nativeads/d$2;->a:Lcom/mopub/nativeads/d;

    invoke-static {v0}, Lcom/mopub/nativeads/d;->e(Lcom/mopub/nativeads/d;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/mopub/nativeads/d$2;->a:Lcom/mopub/nativeads/d;

    invoke-static {v1}, Lcom/mopub/nativeads/d;->d(Lcom/mopub/nativeads/d;)Ljava/lang/Runnable;

    move-result-object v1

    iget-object v2, p0, Lcom/mopub/nativeads/d$2;->a:Lcom/mopub/nativeads/d;

    invoke-virtual {v2}, Lcom/mopub/nativeads/d;->f()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public onNativeLoad(Lcom/mopub/nativeads/NativeAd;)V
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/mopub/nativeads/d$2;->a:Lcom/mopub/nativeads/d;

    invoke-static {v0}, Lcom/mopub/nativeads/d;->a(Lcom/mopub/nativeads/d;)Lcom/mopub/nativeads/MoPubNative;

    move-result-object v0

    if-nez v0, :cond_0

    .line 112
    :goto_0
    return-void

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/mopub/nativeads/d$2;->a:Lcom/mopub/nativeads/d;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/mopub/nativeads/d;->b:Z

    .line 103
    iget-object v0, p0, Lcom/mopub/nativeads/d$2;->a:Lcom/mopub/nativeads/d;

    iget v1, v0, Lcom/mopub/nativeads/d;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/mopub/nativeads/d;->d:I

    .line 104
    iget-object v0, p0, Lcom/mopub/nativeads/d$2;->a:Lcom/mopub/nativeads/d;

    invoke-virtual {v0}, Lcom/mopub/nativeads/d;->e()V

    .line 106
    iget-object v0, p0, Lcom/mopub/nativeads/d$2;->a:Lcom/mopub/nativeads/d;

    invoke-static {v0}, Lcom/mopub/nativeads/d;->b(Lcom/mopub/nativeads/d;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/mopub/nativeads/l;

    invoke-direct {v1, p1}, Lcom/mopub/nativeads/l;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    iget-object v0, p0, Lcom/mopub/nativeads/d$2;->a:Lcom/mopub/nativeads/d;

    invoke-static {v0}, Lcom/mopub/nativeads/d;->b(Lcom/mopub/nativeads/d;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/mopub/nativeads/d$2;->a:Lcom/mopub/nativeads/d;

    invoke-static {v0}, Lcom/mopub/nativeads/d;->c(Lcom/mopub/nativeads/d;)Lcom/mopub/nativeads/d$a;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 108
    iget-object v0, p0, Lcom/mopub/nativeads/d$2;->a:Lcom/mopub/nativeads/d;

    invoke-static {v0}, Lcom/mopub/nativeads/d;->c(Lcom/mopub/nativeads/d;)Lcom/mopub/nativeads/d$a;

    move-result-object v0

    invoke-interface {v0}, Lcom/mopub/nativeads/d$a;->onAdsAvailable()V

    .line 111
    :cond_1
    iget-object v0, p0, Lcom/mopub/nativeads/d$2;->a:Lcom/mopub/nativeads/d;

    invoke-virtual {v0}, Lcom/mopub/nativeads/d;->g()V

    goto :goto_0
.end method
