.class public Lsoftware/simplicial/a/a/e;
.super Lsoftware/simplicial/a/a/y;
.source "SourceFile"


# instance fields
.field public a:B

.field public b:B

.field public c:S


# direct methods
.method public constructor <init>(III)V
    .locals 2

    .prologue
    .line 18
    sget-object v0, Lsoftware/simplicial/a/a/y$a;->F:Lsoftware/simplicial/a/a/y$a;

    const/4 v1, 0x5

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/a/a/y;-><init>(Lsoftware/simplicial/a/a/y$a;I)V

    .line 19
    int-to-byte v0, p1

    iput-byte v0, p0, Lsoftware/simplicial/a/a/e;->a:B

    .line 20
    int-to-byte v0, p2

    iput-byte v0, p0, Lsoftware/simplicial/a/a/e;->b:B

    .line 21
    int-to-short v0, p3

    iput-short v0, p0, Lsoftware/simplicial/a/a/e;->c:S

    .line 22
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    iput-short v0, p0, Lsoftware/simplicial/a/a/e;->c:S

    .line 49
    return-void
.end method

.method public a(Lsoftware/simplicial/a/f/bn;)V
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lsoftware/simplicial/a/a/e;->e:Lsoftware/simplicial/a/a/y$a;

    invoke-virtual {v0}, Lsoftware/simplicial/a/a/y$a;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 29
    iget-byte v0, p0, Lsoftware/simplicial/a/a/e;->a:B

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 30
    iget-byte v0, p0, Lsoftware/simplicial/a/a/e;->b:B

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 31
    iget-short v0, p0, Lsoftware/simplicial/a/a/e;->c:S

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    .line 32
    return-void
.end method

.method public a(S)V
    .locals 1

    .prologue
    .line 53
    iget-short v0, p0, Lsoftware/simplicial/a/a/e;->c:S

    or-int/2addr v0, p1

    int-to-short v0, v0

    iput-short v0, p0, Lsoftware/simplicial/a/a/e;->c:S

    .line 54
    return-void
.end method

.method public a(II)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 37
    iget-byte v1, p0, Lsoftware/simplicial/a/a/e;->a:B

    add-int/lit8 v2, p1, -0x1

    if-le v1, v2, :cond_1

    .line 43
    :cond_0
    :goto_0
    return v0

    .line 38
    :cond_1
    iget-byte v1, p0, Lsoftware/simplicial/a/a/e;->a:B

    if-ltz v1, :cond_0

    .line 39
    iget-byte v1, p0, Lsoftware/simplicial/a/a/e;->b:B

    add-int/lit8 v2, p2, -0x1

    if-gt v1, v2, :cond_0

    .line 40
    iget-byte v1, p0, Lsoftware/simplicial/a/a/e;->b:B

    if-ltz v1, :cond_0

    .line 43
    const/4 v0, 0x1

    goto :goto_0
.end method
