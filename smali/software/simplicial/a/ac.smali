.class public final enum Lsoftware/simplicial/a/ac;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/ac;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/ac;

.field public static final enum b:Lsoftware/simplicial/a/ac;

.field public static final enum c:Lsoftware/simplicial/a/ac;

.field public static final d:[Lsoftware/simplicial/a/ac;

.field private static final synthetic e:[Lsoftware/simplicial/a/ac;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 8
    new-instance v0, Lsoftware/simplicial/a/ac;

    const-string v1, "EASY"

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/ac;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/ac;->a:Lsoftware/simplicial/a/ac;

    new-instance v0, Lsoftware/simplicial/a/ac;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/ac;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/ac;->b:Lsoftware/simplicial/a/ac;

    new-instance v0, Lsoftware/simplicial/a/ac;

    const-string v1, "HARD"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/ac;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/ac;->c:Lsoftware/simplicial/a/ac;

    .line 6
    const/4 v0, 0x3

    new-array v0, v0, [Lsoftware/simplicial/a/ac;

    sget-object v1, Lsoftware/simplicial/a/ac;->a:Lsoftware/simplicial/a/ac;

    aput-object v1, v0, v2

    sget-object v1, Lsoftware/simplicial/a/ac;->b:Lsoftware/simplicial/a/ac;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/ac;->c:Lsoftware/simplicial/a/ac;

    aput-object v1, v0, v4

    sput-object v0, Lsoftware/simplicial/a/ac;->e:[Lsoftware/simplicial/a/ac;

    .line 10
    invoke-static {}, Lsoftware/simplicial/a/ac;->values()[Lsoftware/simplicial/a/ac;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/a/ac;->d:[Lsoftware/simplicial/a/ac;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/ac;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lsoftware/simplicial/a/ac;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/ac;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/ac;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lsoftware/simplicial/a/ac;->e:[Lsoftware/simplicial/a/ac;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/ac;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/ac;

    return-object v0
.end method
