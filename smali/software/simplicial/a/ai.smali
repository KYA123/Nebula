.class public abstract Lsoftware/simplicial/a/ai;
.super Lsoftware/simplicial/a/aj;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field public static final Q:F

.field public static final R:F

.field public static final S:F

.field public static final T:F

.field public static final U:Lsoftware/simplicial/a/bf;

.field public static final V:[Ljava/lang/Integer;

.field public static final W:[Ljava/lang/String;

.field public static final X:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final Y:Lsoftware/simplicial/a/ak;

.field public static Z:Lsoftware/simplicial/a/bp;

.field private static final d:Ljava/text/SimpleDateFormat;

.field private static final e:Ljava/util/Calendar;

.field private static final f:Ljava/util/Calendar;


# instance fields
.field private final a:I

.field public final aA:I

.field protected final aB:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lsoftware/simplicial/a/f/be;",
            ">;"
        }
    .end annotation
.end field

.field protected final aC:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lsoftware/simplicial/a/f/bi;",
            ">;"
        }
    .end annotation
.end field

.field public aD:Lsoftware/simplicial/a/bv;

.field public aE:F

.field public aF:F

.field public aG:I

.field public aH:S

.field protected aI:Lsoftware/simplicial/a/f/bn;

.field protected aJ:Lsoftware/simplicial/a/al;

.field protected aK:Ljava/lang/Thread;

.field protected volatile aL:Z

.field protected volatile aM:Z

.field protected aN:I

.field public aO:J

.field final aa:Lsoftware/simplicial/a/bj;

.field protected final ab:F

.field protected ac:Lsoftware/simplicial/a/bf;

.field public final ad:Ljava/util/Random;

.field public final ae:[Lsoftware/simplicial/a/bf;

.field public final af:[Lsoftware/simplicial/a/bq;

.field public final ag:[Lsoftware/simplicial/a/bt;

.field public final ah:[Lsoftware/simplicial/a/ae;

.field public final ai:[Lsoftware/simplicial/a/g;

.field public final aj:I

.field public final ak:Lsoftware/simplicial/a/bx;

.field public final al:[Lsoftware/simplicial/a/bx;

.field public final am:I

.field public final an:Lsoftware/simplicial/a/ag;

.field public final ao:[Lsoftware/simplicial/a/ag;

.field public final ap:I

.field public final aq:[Lsoftware/simplicial/a/ca;

.field public final ar:I

.field public final as:[[Lsoftware/simplicial/a/bz;

.field public at:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lsoftware/simplicial/a/bz;",
            ">;"
        }
    .end annotation
.end field

.field public final au:I

.field public final av:[Lsoftware/simplicial/a/bl;

.field public final aw:I

.field public final ax:[Lsoftware/simplicial/a/z;

.field public final ay:I

.field public final az:I

.field private volatile b:Z

.field private volatile c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .prologue
    const/4 v4, 0x1

    const/high16 v5, 0x40000000    # 2.0f

    const/16 v15, 0x10

    const/4 v2, -0x1

    const/4 v10, 0x0

    .line 47
    sget v0, Lsoftware/simplicial/a/bh;->e:F

    const v1, 0x439d4a3d

    mul-float/2addr v0, v1

    sput v0, Lsoftware/simplicial/a/ai;->Q:F

    .line 48
    sget v0, Lsoftware/simplicial/a/ai;->Q:F

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    sput v0, Lsoftware/simplicial/a/ai;->R:F

    .line 49
    sget v0, Lsoftware/simplicial/a/ai;->Q:F

    const v1, 0x3f34fdf4    # 0.707f

    mul-float/2addr v0, v1

    sput v0, Lsoftware/simplicial/a/ai;->S:F

    .line 50
    sget v0, Lsoftware/simplicial/a/ai;->Q:F

    const v1, 0x3f99999a    # 1.2f

    mul-float/2addr v0, v1

    sput v0, Lsoftware/simplicial/a/ai;->T:F

    .line 82
    new-instance v0, Lsoftware/simplicial/a/bf;

    invoke-direct {v0}, Lsoftware/simplicial/a/bf;-><init>()V

    sput-object v0, Lsoftware/simplicial/a/ai;->U:Lsoftware/simplicial/a/bf;

    .line 91
    const/16 v0, 0x1c

    new-array v0, v0, [Ljava/lang/Integer;

    .line 93
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v10

    const/4 v1, -0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const/4 v3, -0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x3

    const/4 v3, -0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x4

    const/4 v3, -0x5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x5

    const/4 v3, -0x6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x6

    const/4 v3, -0x7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x7

    const/4 v3, -0x8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/16 v1, 0x8

    const/16 v3, -0x9

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/16 v1, 0x9

    const/16 v3, -0xa

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/16 v1, 0xa

    const/16 v3, -0xb

    .line 94
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/16 v1, 0xb

    const/16 v3, -0xc

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/16 v1, 0xc

    const/16 v3, -0xd

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/16 v1, 0xd

    const/16 v3, -0xe

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/16 v1, 0xe

    const/16 v3, -0xf

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/16 v1, 0xf

    const/16 v3, -0x10

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/16 v1, -0x11

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v15

    const/16 v1, 0x11

    const/16 v3, -0x12

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/16 v1, 0x12

    const/16 v3, -0x13

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/16 v1, 0x13

    const/16 v3, -0x14

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/16 v1, 0x14

    const/16 v3, -0x15

    .line 95
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/16 v1, 0x15

    const/16 v3, -0x16

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/16 v1, 0x16

    const/16 v3, -0x17

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/16 v1, 0x17

    const/16 v3, -0x18

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/16 v1, 0x18

    const/16 v3, -0x19

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/16 v1, 0x19

    const/16 v3, -0x1a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/16 v1, 0x1a

    const/16 v3, -0x1b

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/16 v1, 0x1b

    const/16 v3, -0x1c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    sput-object v0, Lsoftware/simplicial/a/ai;->V:[Ljava/lang/Integer;

    .line 97
    const/16 v0, 0x1c

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Vector"

    aput-object v1, v0, v10

    const-string v1, "Matrix"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v3, "Cathode"

    aput-object v3, v0, v1

    const/4 v1, 0x3

    const-string v3, "Silicon"

    aput-object v3, v0, v1

    const/4 v1, 0x4

    const-string v3, "Divisor"

    aput-object v3, v0, v1

    const/4 v1, 0x5

    const-string v3, "Tensor"

    aput-object v3, v0, v1

    const/4 v1, 0x6

    const-string v3, "Enigma"

    aput-object v3, v0, v1

    const/4 v1, 0x7

    const-string v3, "Function"

    aput-object v3, v0, v1

    const/16 v1, 0x8

    const-string v3, "Xan"

    aput-object v3, v0, v1

    const/16 v1, 0x9

    const-string v3, "Nebula"

    aput-object v3, v0, v1

    const/16 v1, 0xa

    const-string v3, "Andromeda"

    aput-object v3, v0, v1

    const/16 v1, 0xb

    const-string v3, "Aquarius"

    aput-object v3, v0, v1

    const/16 v1, 0xc

    const-string v3, "Ara"

    aput-object v3, v0, v1

    const/16 v1, 0xd

    const-string v3, "Zeta"

    aput-object v3, v0, v1

    const/16 v1, 0xe

    const-string v3, "Tau"

    aput-object v3, v0, v1

    const/16 v1, 0xf

    const-string v3, "Cephus"

    aput-object v3, v0, v1

    const-string v1, "Canis"

    aput-object v1, v0, v15

    const/16 v1, 0x11

    const-string v3, "Corona"

    aput-object v3, v0, v1

    const/16 v1, 0x12

    const-string v3, "Coma"

    aput-object v3, v0, v1

    const/16 v1, 0x13

    const-string v3, "Gamma"

    aput-object v3, v0, v1

    const/16 v1, 0x14

    const-string v3, "Crater"

    aput-object v3, v0, v1

    const/16 v1, 0x15

    const-string v3, "Milky Way"

    aput-object v3, v0, v1

    const/16 v1, 0x16

    const-string v3, "Pulsar"

    aput-object v3, v0, v1

    const/16 v1, 0x17

    const-string v3, "Quasar"

    aput-object v3, v0, v1

    const/16 v1, 0x18

    const-string v3, "Bot"

    aput-object v3, v0, v1

    const/16 v1, 0x19

    const-string v3, "Bot1"

    aput-object v3, v0, v1

    const/16 v1, 0x1a

    const-string v3, "Bot2"

    aput-object v3, v0, v1

    const/16 v1, 0x1b

    const-string v3, "Bot3"

    aput-object v3, v0, v1

    sput-object v0, Lsoftware/simplicial/a/ai;->W:[Ljava/lang/String;

    .line 102
    sget-object v0, Lsoftware/simplicial/a/ak;->b:Lsoftware/simplicial/a/ak;

    sput-object v0, Lsoftware/simplicial/a/ai;->Y:Lsoftware/simplicial/a/ak;

    .line 112
    sget-object v0, Lsoftware/simplicial/a/bp;->a:Lsoftware/simplicial/a/bp;

    sput-object v0, Lsoftware/simplicial/a/ai;->Z:Lsoftware/simplicial/a/bp;

    .line 119
    sget-object v0, Lsoftware/simplicial/a/ai;->U:Lsoftware/simplicial/a/bf;

    new-instance v1, Lsoftware/simplicial/a/f/bl;

    invoke-direct {v1}, Lsoftware/simplicial/a/f/bl;-><init>()V

    sget v3, Lsoftware/simplicial/a/ai;->Q:F

    div-float v4, v3, v5

    sget v3, Lsoftware/simplicial/a/ai;->Q:F

    div-float v5, v3, v5

    sget-object v6, Lsoftware/simplicial/a/am;->a:Lsoftware/simplicial/a/am;

    sget-object v7, Lsoftware/simplicial/a/aq;->a:Lsoftware/simplicial/a/aq;

    const-string v8, ""

    new-array v9, v10, [B

    sget-object v11, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    const-wide/16 v12, 0x0

    move v3, v2

    move v14, v10

    invoke-virtual/range {v0 .. v15}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/f/bl;IIFFLsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;Ljava/lang/String;[BZLsoftware/simplicial/a/q;JII)V

    .line 122
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lsoftware/simplicial/a/ai;->X:Ljava/util/Set;

    .line 123
    sget-object v0, Lsoftware/simplicial/a/ai;->W:[Ljava/lang/String;

    array-length v1, v0

    :goto_0
    if-ge v10, v1, :cond_0

    aget-object v2, v0, v10

    .line 124
    sget-object v3, Lsoftware/simplicial/a/ai;->X:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 123
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 175
    :cond_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd\'T\'HH:mm:ss"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lsoftware/simplicial/a/ai;->d:Ljava/text/SimpleDateFormat;

    .line 176
    sget-object v0, Lsoftware/simplicial/a/ai;->d:Ljava/text/SimpleDateFormat;

    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 181
    :try_start_0
    sget-object v0, Lsoftware/simplicial/a/ai;->d:Ljava/text/SimpleDateFormat;

    const-string v1, "2015-10-01T00:00:00"

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 188
    :goto_1
    new-instance v1, Ljava/util/GregorianCalendar;

    const-string v2, "UTC"

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    sput-object v1, Lsoftware/simplicial/a/ai;->e:Ljava/util/Calendar;

    .line 189
    sget-object v1, Lsoftware/simplicial/a/ai;->e:Ljava/util/Calendar;

    invoke-virtual {v1, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 194
    :try_start_1
    sget-object v0, Lsoftware/simplicial/a/ai;->d:Ljava/text/SimpleDateFormat;

    const-string v1, "2017-08-11T00:00:00"

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 201
    :goto_2
    new-instance v1, Ljava/util/GregorianCalendar;

    const-string v2, "UTC"

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    sput-object v1, Lsoftware/simplicial/a/ai;->f:Ljava/util/Calendar;

    .line 202
    sget-object v1, Lsoftware/simplicial/a/ai;->f:Ljava/util/Calendar;

    invoke-virtual {v1, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 203
    return-void

    .line 183
    :catch_0
    move-exception v0

    .line 185
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    goto :goto_1

    .line 196
    :catch_1
    move-exception v0

    .line 198
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    goto :goto_2
.end method

.method public constructor <init>(Lsoftware/simplicial/a/al;ILsoftware/simplicial/a/am;ILsoftware/simplicial/a/aq;Lsoftware/simplicial/a/ap;JLjava/lang/String;[BSLsoftware/simplicial/a/bj;Z[ZZIISLsoftware/simplicial/a/ac;Lsoftware/simplicial/a/i/a;)V
    .locals 21

    .prologue
    .line 210
    move-object/from16 v4, p0

    move/from16 v5, p4

    move/from16 v6, p17

    move/from16 v7, p16

    move-object/from16 v8, p6

    move/from16 v9, p13

    move-object/from16 v10, p14

    move/from16 v11, p15

    move-object/from16 v12, p3

    move-object/from16 v13, p5

    move-object/from16 v14, p9

    move-object/from16 v15, p10

    move/from16 v16, p18

    move-object/from16 v17, p19

    invoke-direct/range {v4 .. v17}, Lsoftware/simplicial/a/aj;-><init>(IIILsoftware/simplicial/a/ap;Z[ZZLsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;Ljava/lang/String;[BSLsoftware/simplicial/a/ac;)V

    .line 115
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lsoftware/simplicial/a/ai;->ac:Lsoftware/simplicial/a/bf;

    .line 143
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lsoftware/simplicial/a/ai;->at:Ljava/util/Collection;

    .line 154
    invoke-static {}, Lsoftware/simplicial/a/ai;->j()Lsoftware/simplicial/a/bv;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lsoftware/simplicial/a/ai;->aD:Lsoftware/simplicial/a/bv;

    .line 159
    new-instance v4, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v4}, Lsoftware/simplicial/a/f/bn;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lsoftware/simplicial/a/ai;->aI:Lsoftware/simplicial/a/f/bn;

    .line 212
    const/high16 v4, 0x3f800000    # 1.0f

    move/from16 v0, p2

    int-to-float v5, v0

    div-float/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/a/ai;->ab:F

    .line 213
    move-object/from16 v0, p12

    move-object/from16 v1, p0

    iput-object v0, v1, Lsoftware/simplicial/a/ai;->aa:Lsoftware/simplicial/a/bj;

    .line 214
    new-instance v4, Ljava/util/Random;

    move-wide/from16 v0, p7

    invoke-direct {v4, v0, v1}, Ljava/util/Random;-><init>(J)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lsoftware/simplicial/a/ai;->ad:Ljava/util/Random;

    .line 215
    sget-object v4, Lsoftware/simplicial/a/ai$1;->a:[I

    invoke-virtual/range {p6 .. p6}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 230
    sget v4, Lsoftware/simplicial/a/ai;->Q:F

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/a/ai;->aE:F

    .line 234
    :goto_0
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/ai;->aY:I

    new-array v4, v4, [Lsoftware/simplicial/a/bf;

    move-object/from16 v0, p0

    iput-object v4, v0, Lsoftware/simplicial/a/ai;->ae:[Lsoftware/simplicial/a/bf;

    .line 235
    const/4 v7, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/ai;->ae:[Lsoftware/simplicial/a/bf;

    array-length v4, v4

    if-ge v7, v4, :cond_0

    .line 237
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/ai;->ae:[Lsoftware/simplicial/a/bf;

    new-instance v5, Lsoftware/simplicial/a/bf;

    invoke-direct {v5}, Lsoftware/simplicial/a/bf;-><init>()V

    aput-object v5, v4, v7

    .line 238
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/ai;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v4, v4, v7

    new-instance v5, Lsoftware/simplicial/a/f/bl;

    invoke-direct {v5}, Lsoftware/simplicial/a/f/bl;-><init>()V

    const/4 v6, -0x1

    move-object/from16 v0, p0

    iget v8, v0, Lsoftware/simplicial/a/ai;->aE:F

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    move-object/from16 v0, p0

    iget v9, v0, Lsoftware/simplicial/a/ai;->aE:F

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    const-string v12, ""

    const/4 v10, 0x0

    new-array v13, v10, [B

    const/4 v14, 0x0

    sget-object v15, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    const-wide/16 v16, 0x0

    .line 239
    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/a/ai;->o()I

    move-result v18

    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/a/ai;->p()I

    move-result v19

    move-object/from16 v10, p3

    move-object/from16 v11, p5

    .line 238
    invoke-virtual/range {v4 .. v19}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/f/bl;IIFFLsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;Ljava/lang/String;[BZLsoftware/simplicial/a/q;JII)V

    .line 235
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 218
    :pswitch_0
    sget v4, Lsoftware/simplicial/a/ai;->T:F

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/a/ai;->aE:F

    goto :goto_0

    .line 221
    :pswitch_1
    sget v4, Lsoftware/simplicial/a/ai;->Q:F

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/a/ai;->aE:F

    goto :goto_0

    .line 224
    :pswitch_2
    sget v4, Lsoftware/simplicial/a/ai;->S:F

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/a/ai;->aE:F

    goto :goto_0

    .line 227
    :pswitch_3
    sget v4, Lsoftware/simplicial/a/ai;->R:F

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/a/ai;->aE:F

    goto :goto_0

    .line 242
    :cond_0
    const/16 v4, 0x5c

    new-array v4, v4, [Lsoftware/simplicial/a/bq;

    move-object/from16 v0, p0

    iput-object v4, v0, Lsoftware/simplicial/a/ai;->af:[Lsoftware/simplicial/a/bq;

    .line 243
    const/4 v5, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/ai;->af:[Lsoftware/simplicial/a/bq;

    array-length v4, v4

    if-ge v5, v4, :cond_1

    .line 245
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/ai;->af:[Lsoftware/simplicial/a/bq;

    new-instance v6, Lsoftware/simplicial/a/bq;

    invoke-direct {v6}, Lsoftware/simplicial/a/bq;-><init>()V

    aput-object v6, v4, v5

    .line 246
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/ai;->af:[Lsoftware/simplicial/a/bq;

    aget-object v4, v4, v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v4 .. v11}, Lsoftware/simplicial/a/bq;->a(IFFFFFF)V

    .line 243
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 249
    :cond_1
    move-object/from16 v0, p6

    move-object/from16 v1, p12

    invoke-static {v0, v1}, Lsoftware/simplicial/a/ai;->a(Lsoftware/simplicial/a/ap;Lsoftware/simplicial/a/bj;)I

    move-result v4

    new-array v4, v4, [Lsoftware/simplicial/a/bt;

    move-object/from16 v0, p0

    iput-object v4, v0, Lsoftware/simplicial/a/ai;->ag:[Lsoftware/simplicial/a/bt;

    .line 250
    const/4 v4, 0x0

    move v10, v4

    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/ai;->ag:[Lsoftware/simplicial/a/bt;

    array-length v4, v4

    if-ge v10, v4, :cond_3

    .line 252
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/ai;->ag:[Lsoftware/simplicial/a/bt;

    new-instance v5, Lsoftware/simplicial/a/bt;

    invoke-direct {v5}, Lsoftware/simplicial/a/bt;-><init>()V

    aput-object v5, v4, v10

    .line 253
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/ai;->ag:[Lsoftware/simplicial/a/bt;

    aget-object v4, v4, v10

    invoke-static/range {p6 .. p6}, Lsoftware/simplicial/a/ai;->e(Lsoftware/simplicial/a/ap;)I

    move-result v5

    if-ge v10, v5, :cond_2

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/ai;->aD:Lsoftware/simplicial/a/bv;

    :goto_4
    const/high16 v6, 0x40600000    # 3.5f

    .line 254
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lsoftware/simplicial/a/ai;->a(F)F

    move-result v6

    const/high16 v7, 0x40600000    # 3.5f

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lsoftware/simplicial/a/ai;->a(F)F

    move-result v7

    int-to-byte v8, v10

    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/a/ai;->ad:Ljava/util/Random;

    .line 253
    invoke-virtual/range {v4 .. v9}, Lsoftware/simplicial/a/bt;->a(Lsoftware/simplicial/a/bv;FFBLjava/util/Random;)V

    .line 250
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    goto :goto_3

    .line 253
    :cond_2
    sget-object v5, Lsoftware/simplicial/a/bv;->f:Lsoftware/simplicial/a/bv;

    goto :goto_4

    .line 257
    :cond_3
    invoke-static/range {p6 .. p6}, Lsoftware/simplicial/a/ai;->b(Lsoftware/simplicial/a/ap;)S

    move-result v4

    new-array v4, v4, [Lsoftware/simplicial/a/ae;

    move-object/from16 v0, p0

    iput-object v4, v0, Lsoftware/simplicial/a/ai;->ah:[Lsoftware/simplicial/a/ae;

    .line 258
    const/4 v4, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/ai;->ah:[Lsoftware/simplicial/a/ae;

    array-length v5, v5

    if-ge v4, v5, :cond_4

    .line 260
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/ai;->ah:[Lsoftware/simplicial/a/ae;

    new-instance v6, Lsoftware/simplicial/a/ae;

    invoke-direct {v6}, Lsoftware/simplicial/a/ae;-><init>()V

    aput-object v6, v5, v4

    .line 261
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/ai;->ah:[Lsoftware/simplicial/a/ae;

    aget-object v5, v5, v4

    sget v6, Lsoftware/simplicial/a/ae;->a:F

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lsoftware/simplicial/a/ai;->a(F)F

    move-result v6

    sget v7, Lsoftware/simplicial/a/ae;->a:F

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lsoftware/simplicial/a/ai;->a(F)F

    move-result v7

    int-to-short v8, v4

    invoke-virtual {v5, v6, v7, v8}, Lsoftware/simplicial/a/ae;->a(FFS)V

    .line 258
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 264
    :cond_4
    if-eqz p20, :cond_6

    const/4 v4, 0x1

    :goto_6
    move-object/from16 v0, p3

    move-object/from16 v1, p6

    move/from16 v2, p13

    invoke-static {v0, v1, v2, v4}, Lsoftware/simplicial/a/ai;->a(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/ap;ZZ)I

    move-result v12

    .line 265
    if-eqz p20, :cond_7

    const/4 v4, 0x1

    :goto_7
    move-object/from16 v0, p6

    move/from16 v1, p13

    invoke-static {v0, v1, v4}, Lsoftware/simplicial/a/ai;->a(Lsoftware/simplicial/a/ap;ZZ)I

    move-result v13

    .line 266
    invoke-static/range {p6 .. p6}, Lsoftware/simplicial/a/ai;->a(Lsoftware/simplicial/a/ap;)I

    move-result v4

    new-array v4, v4, [Lsoftware/simplicial/a/g;

    move-object/from16 v0, p0

    iput-object v4, v0, Lsoftware/simplicial/a/ai;->ai:[Lsoftware/simplicial/a/g;

    .line 267
    const/4 v5, 0x0

    :goto_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/ai;->ai:[Lsoftware/simplicial/a/g;

    array-length v4, v4

    if-ge v5, v4, :cond_9

    .line 269
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/ai;->ai:[Lsoftware/simplicial/a/g;

    new-instance v6, Lsoftware/simplicial/a/g;

    invoke-direct {v6}, Lsoftware/simplicial/a/g;-><init>()V

    aput-object v6, v4, v5

    .line 271
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/ai;->ad:Ljava/util/Random;

    invoke-virtual {v4}, Ljava/util/Random;->nextFloat()F

    move-result v4

    float-to-double v6, v4

    const-wide v8, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v6, v8

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    mul-double/2addr v6, v8

    double-to-float v9, v6

    .line 272
    const v11, 0x40555555

    .line 274
    sget-object v10, Lsoftware/simplicial/a/g$a;->a:Lsoftware/simplicial/a/g$a;

    .line 275
    if-ge v5, v12, :cond_8

    .line 276
    sget-object v10, Lsoftware/simplicial/a/g$a;->b:Lsoftware/simplicial/a/g$a;

    .line 280
    :cond_5
    :goto_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/ai;->ai:[Lsoftware/simplicial/a/g;

    aget-object v4, v4, v5

    const v6, 0x417a6666    # 15.65f

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lsoftware/simplicial/a/ai;->a(F)F

    move-result v6

    const v7, 0x417a6666    # 15.65f

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lsoftware/simplicial/a/ai;->a(F)F

    move-result v7

    float-to-double v14, v11

    float-to-double v0, v9

    move-wide/from16 v16, v0

    .line 281
    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->cos(D)D

    move-result-wide v16

    mul-double v14, v14, v16

    double-to-float v8, v14

    float-to-double v14, v11

    float-to-double v0, v9

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sin(D)D

    move-result-wide v16

    mul-double v14, v14, v16

    double-to-float v9, v14

    const v11, 0x38d1b717    # 1.0E-4f

    .line 280
    invoke-virtual/range {v4 .. v11}, Lsoftware/simplicial/a/g;->a(IFFFFLsoftware/simplicial/a/g$a;F)V

    .line 267
    add-int/lit8 v5, v5, 0x1

    goto :goto_8

    .line 264
    :cond_6
    const/4 v4, 0x0

    goto/16 :goto_6

    .line 265
    :cond_7
    const/4 v4, 0x0

    goto :goto_7

    .line 277
    :cond_8
    add-int v4, v12, v13

    if-ge v5, v4, :cond_5

    .line 278
    sget-object v10, Lsoftware/simplicial/a/g$a;->c:Lsoftware/simplicial/a/g$a;

    goto :goto_9

    .line 284
    :cond_9
    move-object/from16 v0, p3

    move-object/from16 v1, p6

    invoke-static {v0, v1}, Lsoftware/simplicial/a/ai;->a(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/ap;)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/a/ai;->aj:I

    .line 285
    move-object/from16 v0, p3

    move-object/from16 v1, p6

    invoke-static {v0, v1}, Lsoftware/simplicial/a/ai;->a(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/ap;)I

    move-result v4

    new-array v4, v4, [Lsoftware/simplicial/a/bx;

    move-object/from16 v0, p0

    iput-object v4, v0, Lsoftware/simplicial/a/ai;->al:[Lsoftware/simplicial/a/bx;

    .line 286
    new-instance v4, Lsoftware/simplicial/a/bx;

    const-string v5, "Neutral"

    const/4 v6, -0x1

    move-object/from16 v0, p0

    iget v7, v0, Lsoftware/simplicial/a/ai;->aE:F

    move-object/from16 v0, p3

    invoke-direct {v4, v5, v6, v7, v0}, Lsoftware/simplicial/a/bx;-><init>(Ljava/lang/String;IFLsoftware/simplicial/a/am;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lsoftware/simplicial/a/ai;->ak:Lsoftware/simplicial/a/bx;

    .line 287
    const/4 v4, 0x0

    :goto_a
    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/ai;->aj:I

    if-ge v4, v5, :cond_a

    .line 288
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/ai;->al:[Lsoftware/simplicial/a/bx;

    new-instance v6, Lsoftware/simplicial/a/bx;

    invoke-direct {v6}, Lsoftware/simplicial/a/bx;-><init>()V

    aput-object v6, v5, v4

    .line 287
    add-int/lit8 v4, v4, 0x1

    goto :goto_a

    .line 289
    :cond_a
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/ai;->aj:I

    if-lez v4, :cond_b

    .line 291
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/ai;->al:[Lsoftware/simplicial/a/bx;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    const-string v5, "Red Team"

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v7, v0, Lsoftware/simplicial/a/ai;->aE:F

    move-object/from16 v0, p3

    invoke-virtual {v4, v5, v6, v7, v0}, Lsoftware/simplicial/a/bx;->a(Ljava/lang/String;IFLsoftware/simplicial/a/am;)V

    .line 292
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/ai;->al:[Lsoftware/simplicial/a/bx;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    const-string v5, "Green Team"

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget v7, v0, Lsoftware/simplicial/a/ai;->aE:F

    move-object/from16 v0, p3

    invoke-virtual {v4, v5, v6, v7, v0}, Lsoftware/simplicial/a/bx;->a(Ljava/lang/String;IFLsoftware/simplicial/a/am;)V

    .line 294
    :cond_b
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/ai;->aj:I

    const/4 v5, 0x2

    if-le v4, v5, :cond_c

    .line 296
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/ai;->al:[Lsoftware/simplicial/a/bx;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    const-string v5, "Blue Team"

    const/4 v6, 0x2

    move-object/from16 v0, p0

    iget v7, v0, Lsoftware/simplicial/a/ai;->aE:F

    move-object/from16 v0, p3

    invoke-virtual {v4, v5, v6, v7, v0}, Lsoftware/simplicial/a/bx;->a(Ljava/lang/String;IFLsoftware/simplicial/a/am;)V

    .line 298
    :cond_c
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/ai;->aj:I

    const/4 v5, 0x3

    if-le v4, v5, :cond_d

    .line 300
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/ai;->al:[Lsoftware/simplicial/a/bx;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    const-string v5, "Yellow Team"

    const/4 v6, 0x3

    move-object/from16 v0, p0

    iget v7, v0, Lsoftware/simplicial/a/ai;->aE:F

    move-object/from16 v0, p3

    invoke-virtual {v4, v5, v6, v7, v0}, Lsoftware/simplicial/a/bx;->a(Ljava/lang/String;IFLsoftware/simplicial/a/am;)V

    .line 303
    :cond_d
    sget-object v4, Lsoftware/simplicial/a/am;->l:Lsoftware/simplicial/a/am;

    move-object/from16 v0, p3

    if-ne v0, v4, :cond_e

    invoke-static/range {p6 .. p6}, Lsoftware/simplicial/a/bz;->a(Lsoftware/simplicial/a/ap;)I

    move-result v4

    :goto_b
    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/a/ai;->ar:I

    .line 304
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/ai;->ar:I

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/ai;->ar:I

    filled-new-array {v4, v5}, [I

    move-result-object v4

    const-class v5, Lsoftware/simplicial/a/bz;

    invoke-static {v5, v4}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [[Lsoftware/simplicial/a/bz;

    move-object/from16 v0, p0

    iput-object v4, v0, Lsoftware/simplicial/a/ai;->as:[[Lsoftware/simplicial/a/bz;

    .line 305
    const/4 v4, 0x0

    :goto_c
    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/ai;->ar:I

    if-ge v4, v5, :cond_10

    .line 307
    const/4 v5, 0x0

    :goto_d
    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/a/ai;->ar:I

    if-ge v5, v6, :cond_f

    .line 309
    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/ai;->as:[[Lsoftware/simplicial/a/bz;

    aget-object v6, v6, v5

    new-instance v7, Lsoftware/simplicial/a/bz;

    move-object/from16 v0, p0

    iget v8, v0, Lsoftware/simplicial/a/ai;->aE:F

    move-object/from16 v0, p6

    invoke-direct {v7, v5, v4, v0, v8}, Lsoftware/simplicial/a/bz;-><init>(IILsoftware/simplicial/a/ap;F)V

    aput-object v7, v6, v4

    .line 307
    add-int/lit8 v5, v5, 0x1

    goto :goto_d

    .line 303
    :cond_e
    const/4 v4, 0x0

    goto :goto_b

    .line 305
    :cond_f
    add-int/lit8 v4, v4, 0x1

    goto :goto_c

    .line 313
    :cond_10
    move-object/from16 v0, p3

    move-object/from16 v1, p6

    invoke-static {v0, v1}, Lsoftware/simplicial/a/ai;->c(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/ap;)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/a/ai;->am:I

    .line 314
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/ai;->am:I

    new-array v4, v4, [Lsoftware/simplicial/a/ag;

    move-object/from16 v0, p0

    iput-object v4, v0, Lsoftware/simplicial/a/ai;->ao:[Lsoftware/simplicial/a/ag;

    .line 315
    move-object/from16 v0, p3

    move-object/from16 v1, p6

    invoke-static {v0, v1}, Lsoftware/simplicial/a/ai;->d(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/ap;)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/a/ai;->aw:I

    .line 316
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/ai;->aU:Lsoftware/simplicial/a/am;

    sget-object v5, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-ne v4, v5, :cond_12

    .line 317
    const/4 v4, 0x1

    new-array v4, v4, [Lsoftware/simplicial/a/z;

    move-object/from16 v0, p0

    iput-object v4, v0, Lsoftware/simplicial/a/ai;->ax:[Lsoftware/simplicial/a/z;

    .line 320
    :goto_e
    sget-object v4, Lsoftware/simplicial/a/ai$1;->b:[I

    invoke-virtual/range {p3 .. p3}, Lsoftware/simplicial/a/am;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_1

    .line 346
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lsoftware/simplicial/a/ai;->an:Lsoftware/simplicial/a/ag;

    .line 350
    :cond_11
    :goto_f
    move-object/from16 v0, p6

    move/from16 v1, p13

    invoke-static {v0, v1}, Lsoftware/simplicial/a/ai;->a(Lsoftware/simplicial/a/ap;Z)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/a/ai;->au:I

    .line 351
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/ai;->au:I

    new-array v4, v4, [Lsoftware/simplicial/a/bl;

    move-object/from16 v0, p0

    iput-object v4, v0, Lsoftware/simplicial/a/ai;->av:[Lsoftware/simplicial/a/bl;

    .line 352
    const/4 v5, 0x0

    :goto_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/ai;->av:[Lsoftware/simplicial/a/bl;

    array-length v4, v4

    if-ge v5, v4, :cond_16

    .line 354
    new-instance v4, Lsoftware/simplicial/a/bl;

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/ai;->ak:Lsoftware/simplicial/a/bx;

    move-object/from16 v0, p0

    iget-object v8, v0, Lsoftware/simplicial/a/ai;->ad:Ljava/util/Random;

    move-object/from16 v0, p0

    iget v9, v0, Lsoftware/simplicial/a/ai;->aE:F

    move-object/from16 v7, p3

    invoke-direct/range {v4 .. v9}, Lsoftware/simplicial/a/bl;-><init>(ILsoftware/simplicial/a/bx;Lsoftware/simplicial/a/am;Ljava/util/Random;F)V

    .line 355
    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/ai;->ad:Ljava/util/Random;

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/ai;->aU:Lsoftware/simplicial/a/am;

    sget-object v8, Lsoftware/simplicial/a/am;->j:Lsoftware/simplicial/a/am;

    if-eq v6, v8, :cond_15

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/ai;->aU:Lsoftware/simplicial/a/am;

    sget-object v8, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-eq v6, v8, :cond_15

    if-nez p20, :cond_15

    const/4 v6, 0x1

    :goto_11
    move-object/from16 v0, p14

    invoke-virtual {v4, v7, v6, v0}, Lsoftware/simplicial/a/bl;->a(Ljava/util/Random;Z[Z)V

    .line 356
    sget v6, Lsoftware/simplicial/a/bl;->a:F

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lsoftware/simplicial/a/ai;->a(F)F

    move-result v6

    sget v7, Lsoftware/simplicial/a/bl;->a:F

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lsoftware/simplicial/a/ai;->a(F)F

    move-result v7

    sget v8, Lsoftware/simplicial/a/bl;->a:F

    invoke-virtual {v4, v6, v7, v8}, Lsoftware/simplicial/a/bl;->a(FFF)V

    .line 358
    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/ai;->av:[Lsoftware/simplicial/a/bl;

    aput-object v4, v6, v5

    .line 352
    add-int/lit8 v5, v5, 0x1

    goto :goto_10

    .line 319
    :cond_12
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/ai;->aw:I

    new-array v4, v4, [Lsoftware/simplicial/a/z;

    move-object/from16 v0, p0

    iput-object v4, v0, Lsoftware/simplicial/a/ai;->ax:[Lsoftware/simplicial/a/z;

    goto/16 :goto_e

    .line 323
    :pswitch_4
    new-instance v4, Lsoftware/simplicial/a/ag;

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/ai;->ak:Lsoftware/simplicial/a/bx;

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/ai;->ad:Ljava/util/Random;

    move-object/from16 v0, p0

    iget v7, v0, Lsoftware/simplicial/a/ai;->aE:F

    move-object/from16 v0, p3

    invoke-direct {v4, v5, v0, v6, v7}, Lsoftware/simplicial/a/ag;-><init>(Lsoftware/simplicial/a/bx;Lsoftware/simplicial/a/am;Ljava/util/Random;F)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lsoftware/simplicial/a/ai;->an:Lsoftware/simplicial/a/ag;

    .line 324
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/ai;->ao:[Lsoftware/simplicial/a/ag;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/ai;->an:Lsoftware/simplicial/a/ag;

    aput-object v6, v4, v5

    goto/16 :goto_f

    .line 327
    :pswitch_5
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lsoftware/simplicial/a/ai;->an:Lsoftware/simplicial/a/ag;

    .line 328
    const/4 v4, 0x0

    :goto_12
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/ai;->ao:[Lsoftware/simplicial/a/ag;

    array-length v5, v5

    if-ge v4, v5, :cond_11

    .line 329
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/ai;->ao:[Lsoftware/simplicial/a/ag;

    new-instance v6, Lsoftware/simplicial/a/ag;

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/ai;->al:[Lsoftware/simplicial/a/bx;

    aget-object v7, v7, v4

    move-object/from16 v0, p0

    iget-object v8, v0, Lsoftware/simplicial/a/ai;->ad:Ljava/util/Random;

    move-object/from16 v0, p0

    iget v9, v0, Lsoftware/simplicial/a/ai;->aE:F

    move-object/from16 v0, p3

    invoke-direct {v6, v7, v0, v8, v9}, Lsoftware/simplicial/a/ag;-><init>(Lsoftware/simplicial/a/bx;Lsoftware/simplicial/a/am;Ljava/util/Random;F)V

    aput-object v6, v5, v4

    .line 328
    add-int/lit8 v4, v4, 0x1

    goto :goto_12

    .line 332
    :pswitch_6
    const/4 v4, 0x0

    move v10, v4

    :goto_13
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/ai;->ax:[Lsoftware/simplicial/a/z;

    array-length v4, v4

    if-ge v10, v4, :cond_13

    .line 333
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/ai;->ax:[Lsoftware/simplicial/a/z;

    new-instance v4, Lsoftware/simplicial/a/z;

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/ai;->ad:Ljava/util/Random;

    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/a/ai;->aE:F

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/ai;->ax:[Lsoftware/simplicial/a/z;

    move-object/from16 v0, p0

    iget-object v8, v0, Lsoftware/simplicial/a/ai;->aU:Lsoftware/simplicial/a/am;

    const/4 v9, 0x0

    invoke-direct/range {v4 .. v9}, Lsoftware/simplicial/a/z;-><init>(Ljava/util/Random;F[Lsoftware/simplicial/a/z;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/bx;)V

    aput-object v4, v11, v10

    .line 332
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    goto :goto_13

    .line 334
    :cond_13
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lsoftware/simplicial/a/ai;->an:Lsoftware/simplicial/a/ag;

    goto/16 :goto_f

    .line 337
    :pswitch_7
    const/4 v4, 0x0

    move v10, v4

    :goto_14
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/ai;->ax:[Lsoftware/simplicial/a/z;

    array-length v4, v4

    if-ge v10, v4, :cond_14

    .line 338
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/ai;->ax:[Lsoftware/simplicial/a/z;

    new-instance v4, Lsoftware/simplicial/a/z;

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/ai;->ad:Ljava/util/Random;

    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/a/ai;->aE:F

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/ai;->ax:[Lsoftware/simplicial/a/z;

    move-object/from16 v0, p0

    iget-object v8, v0, Lsoftware/simplicial/a/ai;->aU:Lsoftware/simplicial/a/am;

    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/a/ai;->al:[Lsoftware/simplicial/a/bx;

    aget-object v9, v9, v10

    invoke-direct/range {v4 .. v9}, Lsoftware/simplicial/a/z;-><init>(Ljava/util/Random;F[Lsoftware/simplicial/a/z;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/bx;)V

    aput-object v4, v11, v10

    .line 337
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    goto :goto_14

    .line 339
    :cond_14
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lsoftware/simplicial/a/ai;->an:Lsoftware/simplicial/a/ag;

    goto/16 :goto_f

    .line 342
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/ai;->ax:[Lsoftware/simplicial/a/z;

    const/4 v11, 0x0

    new-instance v4, Lsoftware/simplicial/a/z;

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/ai;->ad:Ljava/util/Random;

    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/a/ai;->aE:F

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/ai;->ax:[Lsoftware/simplicial/a/z;

    move-object/from16 v0, p0

    iget-object v8, v0, Lsoftware/simplicial/a/ai;->aU:Lsoftware/simplicial/a/am;

    const/4 v9, 0x0

    invoke-direct/range {v4 .. v9}, Lsoftware/simplicial/a/z;-><init>(Ljava/util/Random;F[Lsoftware/simplicial/a/z;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/bx;)V

    aput-object v4, v10, v11

    .line 343
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lsoftware/simplicial/a/ai;->an:Lsoftware/simplicial/a/ag;

    goto/16 :goto_f

    .line 355
    :cond_15
    const/4 v6, 0x0

    goto/16 :goto_11

    .line 361
    :cond_16
    move-object/from16 v0, p6

    move/from16 v1, p13

    invoke-static {v0, v1}, Lsoftware/simplicial/a/ai;->b(Lsoftware/simplicial/a/ap;Z)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/a/ai;->ap:I

    .line 362
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/ai;->ap:I

    new-array v4, v4, [Lsoftware/simplicial/a/ca;

    move-object/from16 v0, p0

    iput-object v4, v0, Lsoftware/simplicial/a/ai;->aq:[Lsoftware/simplicial/a/ca;

    .line 363
    const/4 v4, 0x0

    move v11, v4

    :goto_15
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/ai;->aq:[Lsoftware/simplicial/a/ca;

    array-length v4, v4

    if-ge v11, v4, :cond_17

    .line 365
    new-instance v4, Lsoftware/simplicial/a/ca;

    invoke-direct {v4, v11}, Lsoftware/simplicial/a/ca;-><init>(I)V

    .line 367
    const v5, 0x429c8000    # 78.25f

    .line 368
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lsoftware/simplicial/a/ai;->a(F)F

    move-result v5

    const v6, 0x429c8000    # 78.25f

    .line 369
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lsoftware/simplicial/a/ai;->a(F)F

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/ai;->ad:Ljava/util/Random;

    .line 370
    invoke-virtual {v7}, Ljava/util/Random;->nextFloat()F

    move-result v7

    const v8, 0x40490fdb    # (float)Math.PI

    mul-float/2addr v7, v8

    const/high16 v8, 0x40000000    # 2.0f

    mul-float/2addr v7, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lsoftware/simplicial/a/ai;->ad:Ljava/util/Random;

    .line 371
    invoke-virtual {v8}, Ljava/util/Random;->nextFloat()F

    move-result v8

    const/high16 v9, 0x3f800000    # 1.0f

    mul-float/2addr v8, v9

    const/high16 v9, 0x40000000    # 2.0f

    mul-float/2addr v8, v9

    const/high16 v9, 0x3f800000    # 1.0f

    sub-float/2addr v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/a/ai;->ad:Ljava/util/Random;

    .line 372
    invoke-virtual {v9}, Ljava/util/Random;->nextFloat()F

    move-result v9

    const/high16 v10, 0x3f800000    # 1.0f

    mul-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    mul-float/2addr v9, v10

    const/high16 v10, 0x3f800000    # 1.0f

    sub-float/2addr v9, v10

    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/ai;->ad:Ljava/util/Random;

    .line 373
    invoke-virtual {v10}, Ljava/util/Random;->nextFloat()F

    move-result v10

    const v12, 0x3e00adfd

    mul-float/2addr v10, v12

    const/high16 v12, 0x40000000    # 2.0f

    mul-float/2addr v10, v12

    const v12, 0x3e00adfd

    sub-float/2addr v10, v12

    .line 367
    invoke-virtual/range {v4 .. v10}, Lsoftware/simplicial/a/ca;->a(FFFFFF)V

    .line 374
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/ai;->aq:[Lsoftware/simplicial/a/ca;

    aput-object v4, v5, v11

    .line 363
    add-int/lit8 v4, v11, 0x1

    move v11, v4

    goto :goto_15

    .line 377
    :cond_17
    invoke-static/range {p6 .. p6}, Lsoftware/simplicial/a/ai;->b(Lsoftware/simplicial/a/ap;)S

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/a/ai;->ay:I

    .line 378
    invoke-static/range {p6 .. p6}, Lsoftware/simplicial/a/ai;->a(Lsoftware/simplicial/a/ap;)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/a/ai;->az:I

    .line 379
    move-object/from16 v0, p6

    move-object/from16 v1, p12

    invoke-static {v0, v1}, Lsoftware/simplicial/a/ai;->a(Lsoftware/simplicial/a/ap;Lsoftware/simplicial/a/bj;)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/a/ai;->aA:I

    .line 381
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/a/ai;->aG:I

    .line 383
    new-instance v4, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v4}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lsoftware/simplicial/a/ai;->aB:Ljava/util/Queue;

    .line 384
    new-instance v4, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v4}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lsoftware/simplicial/a/ai;->aC:Ljava/util/Queue;

    .line 385
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lsoftware/simplicial/a/ai;->aK:Ljava/lang/Thread;

    .line 386
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lsoftware/simplicial/a/ai;->aJ:Lsoftware/simplicial/a/al;

    .line 387
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/a/ai;->aN:I

    .line 388
    const-wide/16 v4, 0x0

    move-object/from16 v0, p0

    iput-wide v4, v0, Lsoftware/simplicial/a/ai;->aO:J

    .line 389
    move/from16 v0, p11

    move-object/from16 v1, p0

    iput-short v0, v1, Lsoftware/simplicial/a/ai;->aH:S

    .line 390
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lsoftware/simplicial/a/ai;->aL:Z

    .line 392
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lsoftware/simplicial/a/ai;->a:I

    .line 393
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lsoftware/simplicial/a/ai;->b:Z

    .line 394
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lsoftware/simplicial/a/ai;->c:Z

    .line 395
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lsoftware/simplicial/a/ai;->aM:Z

    .line 396
    return-void

    .line 215
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 320
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method protected static a(FFFF)F
    .locals 2

    .prologue
    .line 851
    sub-float v0, p0, p2

    .line 852
    sub-float v1, p1, p3

    .line 853
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public static a(Lsoftware/simplicial/a/am;)I
    .locals 2

    .prologue
    .line 592
    sget-object v0, Lsoftware/simplicial/a/ai$1;->b:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/am;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 609
    :pswitch_0
    const/4 v0, 0x7

    :goto_0
    return v0

    .line 602
    :pswitch_1
    const/4 v0, 0x5

    goto :goto_0

    .line 607
    :pswitch_2
    const/4 v0, 0x4

    goto :goto_0

    .line 592
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/ap;)I
    .locals 5

    .prologue
    const/4 v2, 0x3

    const/4 v0, 0x4

    const/4 v1, 0x2

    .line 442
    sget-object v3, Lsoftware/simplicial/a/ai$1;->b:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/am;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 488
    :pswitch_0
    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    .line 445
    :pswitch_1
    sget-object v2, Lsoftware/simplicial/a/ap;->b:Lsoftware/simplicial/a/ap;

    if-eq p1, v2, :cond_1

    sget-object v2, Lsoftware/simplicial/a/ap;->a:Lsoftware/simplicial/a/ap;

    if-ne p1, v2, :cond_0

    :cond_1
    move v0, v1

    .line 446
    goto :goto_0

    .line 450
    :pswitch_2
    sget-object v0, Lsoftware/simplicial/a/ap;->b:Lsoftware/simplicial/a/ap;

    if-eq p1, v0, :cond_2

    sget-object v0, Lsoftware/simplicial/a/ap;->a:Lsoftware/simplicial/a/ap;

    if-ne p1, v0, :cond_3

    :cond_2
    move v0, v1

    .line 451
    goto :goto_0

    :cond_3
    move v0, v2

    .line 453
    goto :goto_0

    .line 455
    :pswitch_3
    sget-object v2, Lsoftware/simplicial/a/ap;->b:Lsoftware/simplicial/a/ap;

    if-eq p1, v2, :cond_4

    sget-object v2, Lsoftware/simplicial/a/ap;->a:Lsoftware/simplicial/a/ap;

    if-eq p1, v2, :cond_4

    sget-object v2, Lsoftware/simplicial/a/ap;->c:Lsoftware/simplicial/a/ap;

    if-ne p1, v2, :cond_0

    :cond_4
    move v0, v1

    .line 456
    goto :goto_0

    .line 460
    :pswitch_4
    sget-object v2, Lsoftware/simplicial/a/ap;->b:Lsoftware/simplicial/a/ap;

    if-eq p1, v2, :cond_5

    sget-object v2, Lsoftware/simplicial/a/ap;->a:Lsoftware/simplicial/a/ap;

    if-ne p1, v2, :cond_0

    :cond_5
    move v0, v1

    .line 461
    goto :goto_0

    .line 465
    :pswitch_5
    sget-object v3, Lsoftware/simplicial/a/ap;->d:Lsoftware/simplicial/a/ap;

    if-eq p1, v3, :cond_0

    .line 467
    sget-object v0, Lsoftware/simplicial/a/ap;->c:Lsoftware/simplicial/a/ap;

    if-ne p1, v0, :cond_6

    move v0, v2

    .line 468
    goto :goto_0

    :cond_6
    move v0, v1

    .line 470
    goto :goto_0

    :pswitch_6
    move v0, v1

    .line 472
    goto :goto_0

    :pswitch_7
    move v0, v1

    .line 474
    goto :goto_0

    .line 476
    :pswitch_8
    sget-object v3, Lsoftware/simplicial/a/ap;->b:Lsoftware/simplicial/a/ap;

    if-eq p1, v3, :cond_7

    sget-object v3, Lsoftware/simplicial/a/ap;->a:Lsoftware/simplicial/a/ap;

    if-ne p1, v3, :cond_8

    :cond_7
    move v0, v1

    .line 477
    goto :goto_0

    .line 478
    :cond_8
    sget-object v1, Lsoftware/simplicial/a/ap;->c:Lsoftware/simplicial/a/ap;

    if-ne p1, v1, :cond_0

    move v0, v2

    .line 479
    goto :goto_0

    .line 483
    :pswitch_9
    sget-object v1, Lsoftware/simplicial/a/ap;->b:Lsoftware/simplicial/a/ap;

    if-eq p1, v1, :cond_9

    sget-object v1, Lsoftware/simplicial/a/ap;->a:Lsoftware/simplicial/a/ap;

    if-eq p1, v1, :cond_9

    sget-object v1, Lsoftware/simplicial/a/ap;->c:Lsoftware/simplicial/a/ap;

    if-ne p1, v1, :cond_0

    :cond_9
    move v0, v2

    .line 484
    goto :goto_0

    .line 442
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_1
        :pswitch_8
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_9
    .end packed-switch
.end method

.method public static a(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/ap;ZZ)I
    .locals 5

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x3

    const/4 v0, 0x2

    .line 615
    sget-object v3, Lsoftware/simplicial/a/am;->j:Lsoftware/simplicial/a/am;

    if-eq p0, v3, :cond_0

    sget-object v3, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-ne p0, v3, :cond_1

    .line 616
    :cond_0
    invoke-static {p1}, Lsoftware/simplicial/a/ai;->a(Lsoftware/simplicial/a/ap;)I

    move-result v0

    invoke-static {p1, p2, p3}, Lsoftware/simplicial/a/ai;->a(Lsoftware/simplicial/a/ap;ZZ)I

    move-result v1

    sub-int/2addr v0, v1

    .line 679
    :goto_0
    :pswitch_0
    return v0

    .line 618
    :cond_1
    if-eqz p2, :cond_2

    if-nez p3, :cond_2

    .line 620
    sget-object v3, Lsoftware/simplicial/a/ai$1;->a:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 633
    :cond_2
    sget-object v3, Lsoftware/simplicial/a/ai$1;->b:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/am;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    .line 679
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 623
    :pswitch_2
    invoke-static {p1}, Lsoftware/simplicial/a/ai;->a(Lsoftware/simplicial/a/ap;)I

    move-result v0

    invoke-static {p1, p2, p3}, Lsoftware/simplicial/a/ai;->a(Lsoftware/simplicial/a/ap;ZZ)I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 625
    :pswitch_3
    invoke-static {p1}, Lsoftware/simplicial/a/ai;->a(Lsoftware/simplicial/a/ap;)I

    move-result v0

    invoke-static {p1, p2, p3}, Lsoftware/simplicial/a/ai;->a(Lsoftware/simplicial/a/ap;ZZ)I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x2

    goto :goto_0

    .line 627
    :pswitch_4
    invoke-static {p1}, Lsoftware/simplicial/a/ai;->a(Lsoftware/simplicial/a/ap;)I

    move-result v0

    invoke-static {p1, p2, p3}, Lsoftware/simplicial/a/ai;->a(Lsoftware/simplicial/a/ap;ZZ)I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x2

    goto :goto_0

    .line 629
    :pswitch_5
    invoke-static {p1}, Lsoftware/simplicial/a/ai;->a(Lsoftware/simplicial/a/ap;)I

    move-result v0

    invoke-static {p1, p2, p3}, Lsoftware/simplicial/a/ai;->a(Lsoftware/simplicial/a/ap;ZZ)I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x3

    goto :goto_0

    .line 639
    :pswitch_6
    sget-object v1, Lsoftware/simplicial/a/ai$1;->a:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_2

    goto :goto_0

    .line 642
    :pswitch_7
    const/4 v0, 0x1

    goto :goto_0

    .line 656
    :pswitch_8
    sget-object v0, Lsoftware/simplicial/a/ai$1;->a:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_3

    .line 663
    const/4 v0, 0x5

    goto :goto_0

    :pswitch_9
    move v0, v1

    .line 659
    goto :goto_0

    :pswitch_a
    move v0, v2

    .line 661
    goto :goto_0

    .line 669
    :pswitch_b
    sget-object v3, Lsoftware/simplicial/a/ai$1;->a:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_4

    move v0, v2

    .line 676
    goto :goto_0

    :pswitch_c
    move v0, v1

    .line 674
    goto :goto_0

    .line 620
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch

    .line 633
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_1
        :pswitch_8
        :pswitch_6
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_b
        :pswitch_6
        :pswitch_1
        :pswitch_6
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_6
    .end packed-switch

    .line 639
    :pswitch_data_2
    .packed-switch 0x3
        :pswitch_0
        :pswitch_7
    .end packed-switch

    .line 656
    :pswitch_data_3
    .packed-switch 0x3
        :pswitch_a
        :pswitch_9
    .end packed-switch

    .line 669
    :pswitch_data_4
    .packed-switch 0x3
        :pswitch_c
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Lsoftware/simplicial/a/ap;)I
    .locals 2

    .prologue
    .line 418
    sget-object v0, Lsoftware/simplicial/a/ai$1;->a:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 427
    const/16 v0, 0xc

    :goto_0
    return v0

    .line 421
    :pswitch_0
    const/4 v0, 0x6

    goto :goto_0

    .line 423
    :pswitch_1
    const/16 v0, 0x8

    goto :goto_0

    .line 425
    :pswitch_2
    const/16 v0, 0xa

    goto :goto_0

    .line 418
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Lsoftware/simplicial/a/ap;Lsoftware/simplicial/a/bj;)I
    .locals 2

    .prologue
    .line 753
    sget-object v0, Lsoftware/simplicial/a/ai$1;->c:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/bj;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 760
    invoke-static {p0}, Lsoftware/simplicial/a/ai;->d(Lsoftware/simplicial/a/ap;)I

    move-result v0

    invoke-static {p0}, Lsoftware/simplicial/a/ai;->e(Lsoftware/simplicial/a/ap;)I

    move-result v1

    add-int/2addr v0, v1

    :goto_0
    return v0

    .line 756
    :pswitch_0
    invoke-static {p0}, Lsoftware/simplicial/a/ai;->e(Lsoftware/simplicial/a/ap;)I

    move-result v0

    goto :goto_0

    .line 758
    :pswitch_1
    invoke-static {p0}, Lsoftware/simplicial/a/ai;->d(Lsoftware/simplicial/a/ap;)I

    move-result v0

    invoke-static {p0}, Lsoftware/simplicial/a/ai;->e(Lsoftware/simplicial/a/ap;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0

    .line 753
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Lsoftware/simplicial/a/ap;Z)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 909
    if-nez p1, :cond_0

    .line 923
    :goto_0
    return v0

    .line 912
    :cond_0
    sget-object v1, Lsoftware/simplicial/a/ai$1;->a:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 921
    :pswitch_0
    const/16 v0, 0xd

    goto :goto_0

    .line 915
    :pswitch_1
    const/4 v0, 0x3

    goto :goto_0

    .line 917
    :pswitch_2
    const/4 v0, 0x7

    goto :goto_0

    .line 919
    :pswitch_3
    const/16 v0, 0xa

    goto :goto_0

    .line 912
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lsoftware/simplicial/a/ap;ZZ)I
    .locals 1

    .prologue
    .line 685
    if-eqz p1, :cond_1

    if-nez p2, :cond_1

    .line 687
    sget-object v0, Lsoftware/simplicial/a/ap;->d:Lsoftware/simplicial/a/ap;

    if-ne p0, v0, :cond_0

    .line 688
    const/4 v0, 0x3

    .line 693
    :goto_0
    return v0

    .line 690
    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    .line 693
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a([Lsoftware/simplicial/a/bf;)Lsoftware/simplicial/a/bf;
    .locals 5

    .prologue
    .line 433
    const/4 v1, 0x0

    .line 434
    const/4 v0, 0x0

    move v4, v0

    move-object v0, v1

    move v1, v4

    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_2

    .line 435
    aget-object v2, p0, v1

    iget v2, v2, Lsoftware/simplicial/a/bf;->S:I

    if-lez v2, :cond_1

    if-eqz v0, :cond_0

    aget-object v2, p0, v1

    invoke-virtual {v2}, Lsoftware/simplicial/a/bf;->d()I

    move-result v2

    invoke-virtual {v0}, Lsoftware/simplicial/a/bf;->d()I

    move-result v3

    if-le v2, v3, :cond_1

    .line 436
    :cond_0
    aget-object v0, p0, v1

    .line 434
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 437
    :cond_2
    return-object v0
.end method

.method public static a(Lsoftware/simplicial/a/am;S)S
    .locals 5

    .prologue
    const/16 v2, 0x78

    const/16 v0, 0x3c

    const/16 v1, 0x708

    .line 800
    sget-object v3, Lsoftware/simplicial/a/ai$1;->b:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/am;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 830
    invoke-static {p0}, Lsoftware/simplicial/a/ai;->b(Lsoftware/simplicial/a/am;)S

    move-result p1

    :cond_0
    :goto_0
    return p1

    .line 810
    :pswitch_0
    const/16 p1, 0x7fff

    goto :goto_0

    .line 818
    :pswitch_1
    if-ge p1, v0, :cond_1

    move p1, v0

    .line 819
    goto :goto_0

    .line 820
    :cond_1
    if-le p1, v1, :cond_0

    move p1, v1

    .line 821
    goto :goto_0

    .line 824
    :pswitch_2
    if-ge p1, v2, :cond_2

    move p1, v2

    .line 825
    goto :goto_0

    .line 826
    :cond_2
    if-le p1, v1, :cond_0

    move p1, v1

    .line 827
    goto :goto_0

    .line 800
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1003
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/a/ai;->aB:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    .line 1004
    if-nez v0, :cond_0

    .line 1010
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/a/ai;->aC:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    .line 1011
    if-nez v0, :cond_1

    .line 1014
    return-void
.end method

.method protected static b(FFFF)F
    .locals 2

    .prologue
    .line 858
    sub-float v0, p0, p2

    .line 859
    sub-float v1, p1, p3

    .line 860
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    return v0
.end method

.method public static b(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/ap;)I
    .locals 7

    .prologue
    const/16 v3, 0x14

    const/16 v4, 0x12

    const/16 v0, 0x15

    const/16 v1, 0xe

    const/4 v2, 0x6

    .line 494
    sget-object v5, Lsoftware/simplicial/a/ai$1;->b:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/am;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 587
    :goto_0
    const/4 v0, 0x0

    :goto_1
    :pswitch_0
    return v0

    .line 505
    :pswitch_1
    sget-object v3, Lsoftware/simplicial/a/ai$1;->a:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    goto :goto_0

    .line 508
    :pswitch_2
    const/16 v0, 0x1b

    goto :goto_1

    :pswitch_3
    move v0, v1

    .line 512
    goto :goto_1

    :pswitch_4
    move v0, v2

    .line 514
    goto :goto_1

    .line 518
    :pswitch_5
    sget-object v0, Lsoftware/simplicial/a/ai$1;->a:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    :pswitch_6
    move v0, v3

    .line 521
    goto :goto_1

    :pswitch_7
    move v0, v4

    .line 523
    goto :goto_1

    .line 525
    :pswitch_8
    const/16 v0, 0xc

    goto :goto_1

    :pswitch_9
    move v0, v2

    .line 527
    goto :goto_1

    .line 532
    :pswitch_a
    sget-object v3, Lsoftware/simplicial/a/ai$1;->a:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_3

    goto :goto_0

    :pswitch_b
    move v0, v1

    .line 539
    goto :goto_1

    :pswitch_c
    move v0, v2

    .line 541
    goto :goto_1

    .line 545
    :pswitch_d
    sget-object v3, Lsoftware/simplicial/a/ai$1;->a:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_4

    goto :goto_0

    .line 548
    :pswitch_e
    const/16 v0, 0x1b

    goto :goto_1

    :pswitch_f
    move v0, v1

    .line 552
    goto :goto_1

    :pswitch_10
    move v0, v2

    .line 554
    goto :goto_1

    .line 561
    :pswitch_11
    sget-object v0, Lsoftware/simplicial/a/ai$1;->a:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_5

    goto :goto_0

    .line 564
    :pswitch_12
    const/16 v0, 0x18

    goto :goto_1

    :pswitch_13
    move v0, v3

    .line 566
    goto :goto_1

    :pswitch_14
    move v0, v1

    .line 568
    goto :goto_1

    :pswitch_15
    move v0, v2

    .line 570
    goto :goto_1

    .line 574
    :pswitch_16
    sget-object v0, Lsoftware/simplicial/a/ai$1;->a:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_6

    goto :goto_0

    :pswitch_17
    move v0, v4

    .line 577
    goto :goto_1

    :pswitch_18
    move v0, v1

    .line 579
    goto :goto_1

    .line 581
    :pswitch_19
    const/16 v0, 0xa

    goto :goto_1

    :pswitch_1a
    move v0, v2

    .line 583
    goto :goto_1

    .line 494
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_16
        :pswitch_11
        :pswitch_a
        :pswitch_d
        :pswitch_1
        :pswitch_a
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_5
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 505
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 518
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 532
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_c
    .end packed-switch

    .line 545
    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_e
        :pswitch_0
        :pswitch_f
        :pswitch_10
    .end packed-switch

    .line 561
    :pswitch_data_5
    .packed-switch 0x1
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
    .end packed-switch

    .line 574
    :pswitch_data_6
    .packed-switch 0x1
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
    .end packed-switch
.end method

.method private static b(Lsoftware/simplicial/a/ap;Z)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 928
    if-nez p1, :cond_0

    .line 942
    :goto_0
    return v0

    .line 931
    :cond_0
    sget-object v1, Lsoftware/simplicial/a/ai$1;->a:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 940
    :pswitch_0
    const/4 v0, 0x4

    goto :goto_0

    .line 934
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 936
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 938
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 931
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static b(Lsoftware/simplicial/a/am;)S
    .locals 3

    .prologue
    const/16 v0, 0x12c

    .line 732
    sget-object v1, Lsoftware/simplicial/a/ai$1;->b:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/am;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 747
    :pswitch_0
    const/16 v0, 0x7fff

    :goto_0
    :pswitch_1
    return v0

    .line 738
    :pswitch_2
    const/16 v0, 0x168

    goto :goto_0

    .line 745
    :pswitch_3
    const/16 v0, 0xb4

    goto :goto_0

    .line 732
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static b(Lsoftware/simplicial/a/ap;)S
    .locals 3

    .prologue
    const/16 v0, 0x14a

    .line 698
    sget-object v1, Lsoftware/simplicial/a/ai$1;->a:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 709
    :goto_0
    :pswitch_0
    return v0

    .line 701
    :pswitch_1
    const/16 v0, 0xa8

    goto :goto_0

    .line 703
    :pswitch_2
    const/16 v0, 0xfc

    goto :goto_0

    .line 707
    :pswitch_3
    const/16 v0, 0x1f8

    goto :goto_0

    .line 698
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private static c(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/ap;)I
    .locals 2

    .prologue
    .line 865
    sget-object v0, Lsoftware/simplicial/a/ai$1;->b:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/am;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 872
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 868
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 870
    :pswitch_1
    invoke-static {p0, p1}, Lsoftware/simplicial/a/ai;->a(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/ap;)I

    move-result v0

    goto :goto_0

    .line 865
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static c(Lsoftware/simplicial/a/am;)Lsoftware/simplicial/a/ap;
    .locals 2

    .prologue
    .line 835
    sget-object v0, Lsoftware/simplicial/a/ai$1;->b:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/am;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    .line 845
    sget-object v0, Lsoftware/simplicial/a/ap;->c:Lsoftware/simplicial/a/ap;

    :goto_0
    return-object v0

    .line 843
    :sswitch_0
    sget-object v0, Lsoftware/simplicial/a/ap;->b:Lsoftware/simplicial/a/ap;

    goto :goto_0

    .line 835
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x5 -> :sswitch_0
        0x8 -> :sswitch_0
        0x9 -> :sswitch_0
        0xf -> :sswitch_0
        0x11 -> :sswitch_0
    .end sparse-switch
.end method

.method public static c(Lsoftware/simplicial/a/ap;)S
    .locals 3

    .prologue
    const/16 v0, 0x14a

    .line 715
    sget-object v1, Lsoftware/simplicial/a/ai$1;->a:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 726
    :goto_0
    :pswitch_0
    return v0

    .line 718
    :pswitch_1
    const/16 v0, 0xa8

    goto :goto_0

    .line 720
    :pswitch_2
    const/16 v0, 0xfc

    goto :goto_0

    .line 724
    :pswitch_3
    const/16 v0, 0x1f8

    goto :goto_0

    .line 715
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private static d(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/ap;)I
    .locals 4

    .prologue
    const/4 v1, 0x4

    const/4 v0, 0x2

    .line 878
    sget-object v2, Lsoftware/simplicial/a/ai$1;->b:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/am;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 904
    :goto_0
    const/4 v0, 0x0

    :goto_1
    :pswitch_0
    return v0

    .line 881
    :pswitch_1
    sget-object v2, Lsoftware/simplicial/a/ai$1;->a:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    :pswitch_2
    move v0, v1

    .line 889
    goto :goto_1

    .line 887
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_1

    .line 893
    :pswitch_4
    sget-object v2, Lsoftware/simplicial/a/ai$1;->a:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_2

    goto :goto_0

    :pswitch_5
    move v0, v1

    .line 900
    goto :goto_1

    .line 878
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_4
    .end packed-switch

    .line 881
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 893
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static d(Lsoftware/simplicial/a/ap;)I
    .locals 2

    .prologue
    .line 766
    sget-object v0, Lsoftware/simplicial/a/ai$1;->a:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 777
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 769
    :pswitch_0
    const/16 v0, 0x20

    goto :goto_0

    .line 771
    :pswitch_1
    const/16 v0, 0x10

    goto :goto_0

    .line 773
    :pswitch_2
    const/16 v0, 0x8

    goto :goto_0

    .line 775
    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 766
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static e(Lsoftware/simplicial/a/ap;)I
    .locals 2

    .prologue
    .line 783
    sget-object v0, Lsoftware/simplicial/a/ai$1;->a:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 794
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 786
    :pswitch_0
    const/16 v0, 0x20

    goto :goto_0

    .line 788
    :pswitch_1
    const/16 v0, 0x10

    goto :goto_0

    .line 790
    :pswitch_2
    const/16 v0, 0x8

    goto :goto_0

    .line 792
    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 783
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static j()Lsoftware/simplicial/a/bv;
    .locals 4

    .prologue
    .line 402
    :try_start_0
    new-instance v0, Ljava/util/GregorianCalendar;

    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 403
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 405
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    sget-object v2, Lsoftware/simplicial/a/ai;->e:Ljava/util/Calendar;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    sub-int/2addr v1, v2

    .line 406
    mul-int/lit8 v1, v1, 0xc

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    sget-object v2, Lsoftware/simplicial/a/ai;->e:Ljava/util/Calendar;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    sub-int/2addr v0, v2

    add-int/2addr v0, v1

    rem-int/lit8 v0, v0, 0xc

    .line 408
    sget-object v1, Lsoftware/simplicial/a/bv;->q:[Lsoftware/simplicial/a/bv;

    aget-object v0, v1, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 412
    :goto_0
    return-object v0

    .line 410
    :catch_0
    move-exception v0

    .line 412
    sget-object v0, Lsoftware/simplicial/a/bv;->e:Lsoftware/simplicial/a/bv;

    goto :goto_0
.end method

.method public static n()Lsoftware/simplicial/a/am;
    .locals 5

    .prologue
    .line 1240
    :try_start_0
    new-instance v0, Ljava/util/GregorianCalendar;

    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 1241
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1243
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    sget-object v2, Lsoftware/simplicial/a/ai;->f:Ljava/util/Calendar;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    sub-int/2addr v1, v2

    .line 1244
    mul-int/lit16 v2, v1, 0x16d

    const/4 v3, 0x6

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v0

    sget-object v3, Lsoftware/simplicial/a/ai;->f:Ljava/util/Calendar;

    const/4 v4, 0x6

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    sub-int/2addr v0, v3

    add-int/2addr v0, v2

    .line 1246
    if-nez v1, :cond_0

    const/4 v1, 0x7

    if-gt v0, v1, :cond_0

    .line 1247
    sget-object v0, Lsoftware/simplicial/a/am;->r:Lsoftware/simplicial/a/am;

    .line 1268
    :goto_0
    return-object v0

    .line 1249
    :cond_0
    rem-int/lit8 v0, v0, 0x4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    packed-switch v0, :pswitch_data_0

    .line 1268
    :goto_1
    sget-object v0, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    goto :goto_0

    .line 1252
    :pswitch_0
    :try_start_1
    sget-object v0, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    goto :goto_0

    .line 1254
    :pswitch_1
    sget-object v0, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    goto :goto_0

    .line 1256
    :pswitch_2
    sget-object v0, Lsoftware/simplicial/a/am;->p:Lsoftware/simplicial/a/am;

    goto :goto_0

    .line 1258
    :pswitch_3
    sget-object v0, Lsoftware/simplicial/a/am;->q:Lsoftware/simplicial/a/am;

    goto :goto_0

    .line 1260
    :pswitch_4
    sget-object v0, Lsoftware/simplicial/a/am;->r:Lsoftware/simplicial/a/am;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1263
    :catch_0
    move-exception v0

    .line 1265
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 1249
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method protected a(F)F
    .locals 3

    .prologue
    .line 947
    iget-object v0, p0, Lsoftware/simplicial/a/ai;->ad:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    move-result v0

    iget v1, p0, Lsoftware/simplicial/a/ai;->aE:F

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v2, p1

    sub-float/2addr v1, v2

    mul-float/2addr v0, v1

    add-float/2addr v0, p1

    return v0
.end method

.method protected a(FFF)F
    .locals 4

    .prologue
    .line 952
    add-float v0, p2, p1

    iget-object v1, p0, Lsoftware/simplicial/a/ai;->ad:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextFloat()F

    move-result v1

    sub-float v2, p3, p2

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v3, p1

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bh;Ljava/util/Collection;)Ljava/util/Collection;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsoftware/simplicial/a/bf;",
            "Lsoftware/simplicial/a/bh;",
            "Ljava/util/Collection",
            "<",
            "Lsoftware/simplicial/a/bz;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Lsoftware/simplicial/a/bz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1098
    new-instance v11, Ljava/util/LinkedHashSet;

    invoke-direct {v11}, Ljava/util/LinkedHashSet;-><init>()V

    .line 1099
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/ai;->aE:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/ai;->aQ:Lsoftware/simplicial/a/ap;

    invoke-static {v3}, Lsoftware/simplicial/a/bz;->a(Lsoftware/simplicial/a/ap;)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 1100
    move-object/from16 v0, p2

    iget v3, v0, Lsoftware/simplicial/a/bh;->l:F

    move-object/from16 v0, p2

    iget v4, v0, Lsoftware/simplicial/a/bh;->n:F

    sub-float v12, v3, v4

    .line 1101
    move-object/from16 v0, p2

    iget v3, v0, Lsoftware/simplicial/a/bh;->l:F

    move-object/from16 v0, p2

    iget v4, v0, Lsoftware/simplicial/a/bh;->n:F

    add-float v13, v3, v4

    .line 1102
    move-object/from16 v0, p2

    iget v3, v0, Lsoftware/simplicial/a/bh;->m:F

    move-object/from16 v0, p2

    iget v4, v0, Lsoftware/simplicial/a/bh;->n:F

    add-float v14, v3, v4

    .line 1103
    move-object/from16 v0, p2

    iget v3, v0, Lsoftware/simplicial/a/bh;->m:F

    move-object/from16 v0, p2

    iget v4, v0, Lsoftware/simplicial/a/bh;->n:F

    sub-float v15, v3, v4

    .line 1104
    div-float v3, v12, v2

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    const-wide/16 v6, 0x0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    double-to-int v3, v4

    .line 1105
    div-float v4, v13, v2

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/a/ai;->ar:I

    add-int/lit8 v6, v6, -0x1

    int-to-double v6, v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    double-to-int v4, v4

    .line 1106
    div-float v5, v15, v2

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    const-wide/16 v8, 0x0

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(DD)D

    move-result-wide v6

    double-to-int v0, v6

    move/from16 v16, v0

    .line 1107
    div-float v2, v14, v2

    float-to-double v6, v2

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/ai;->ar:I

    add-int/lit8 v2, v2, -0x1

    int-to-double v8, v2

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->min(DD)D

    move-result-wide v6

    double-to-int v0, v6

    move/from16 v17, v0

    .line 1110
    add-int v2, v17, v16

    int-to-double v6, v2

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v10, v6

    move v9, v10

    move v6, v4

    move v5, v3

    .line 1111
    :goto_0
    move/from16 v0, v17

    if-gt v9, v0, :cond_13

    .line 1113
    :goto_1
    if-gt v5, v6, :cond_29

    .line 1115
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/ai;->as:[[Lsoftware/simplicial/a/bz;

    aget-object v2, v2, v5

    aget-object v18, v2, v9

    .line 1117
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/ai;->aq:[Lsoftware/simplicial/a/ca;

    move-object/from16 v0, p0

    iget v7, v0, Lsoftware/simplicial/a/ai;->aN:I

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-static {v0, v1, v2, v7}, Lsoftware/simplicial/a/ao;->a(Lsoftware/simplicial/a/ao;Lsoftware/simplicial/a/ao;[Lsoftware/simplicial/a/ca;I)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1119
    move-object/from16 v0, v18

    iget v2, v0, Lsoftware/simplicial/a/bz;->c:F

    cmpl-float v2, v12, v2

    if-ltz v2, :cond_2

    move-object/from16 v0, v18

    iget v2, v0, Lsoftware/simplicial/a/bz;->d:F

    cmpg-float v2, v13, v2

    if-gtz v2, :cond_2

    const/4 v2, 0x1

    .line 1120
    :goto_2
    move-object/from16 v0, v18

    iget v7, v0, Lsoftware/simplicial/a/bz;->f:F

    cmpl-float v7, v15, v7

    if-ltz v7, :cond_3

    move-object/from16 v0, v18

    iget v7, v0, Lsoftware/simplicial/a/bz;->e:F

    cmpg-float v7, v14, v7

    if-gtz v7, :cond_3

    const/4 v7, 0x1

    move v8, v7

    .line 1121
    :goto_3
    if-eqz v2, :cond_4

    if-eqz v8, :cond_4

    move v8, v6

    .line 1131
    :goto_4
    if-gt v5, v8, :cond_0

    .line 1133
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/ai;->as:[[Lsoftware/simplicial/a/bz;

    aget-object v2, v2, v8

    aget-object v18, v2, v9

    .line 1134
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/ai;->aq:[Lsoftware/simplicial/a/ca;

    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/a/ai;->aN:I

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-static {v0, v1, v2, v6}, Lsoftware/simplicial/a/ao;->a(Lsoftware/simplicial/a/ao;Lsoftware/simplicial/a/ao;[Lsoftware/simplicial/a/ca;I)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1136
    move-object/from16 v0, v18

    iget v2, v0, Lsoftware/simplicial/a/bz;->c:F

    cmpl-float v2, v12, v2

    if-ltz v2, :cond_8

    move-object/from16 v0, v18

    iget v2, v0, Lsoftware/simplicial/a/bz;->d:F

    cmpg-float v2, v13, v2

    if-gtz v2, :cond_8

    const/4 v2, 0x1

    .line 1137
    :goto_5
    move-object/from16 v0, v18

    iget v6, v0, Lsoftware/simplicial/a/bz;->f:F

    cmpl-float v6, v15, v6

    if-ltz v6, :cond_9

    move-object/from16 v0, v18

    iget v6, v0, Lsoftware/simplicial/a/bz;->e:F

    cmpg-float v6, v14, v6

    if-gtz v6, :cond_9

    const/4 v6, 0x1

    move v7, v6

    .line 1138
    :goto_6
    if-eqz v2, :cond_a

    if-eqz v7, :cond_a

    :cond_0
    move v2, v5

    .line 1148
    :goto_7
    if-gt v2, v8, :cond_12

    .line 1150
    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/ai;->as:[[Lsoftware/simplicial/a/bz;

    aget-object v6, v6, v2

    aget-object v6, v6, v9

    .line 1152
    invoke-virtual/range {p2 .. p2}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v7

    iget v0, v6, Lsoftware/simplicial/a/bz;->h:F

    move/from16 v18, v0

    cmpg-float v7, v7, v18

    if-gtz v7, :cond_e

    .line 1148
    :cond_1
    :goto_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 1119
    :cond_2
    const/4 v2, 0x0

    goto :goto_2

    .line 1120
    :cond_3
    const/4 v7, 0x0

    move v8, v7

    goto :goto_3

    .line 1123
    :cond_4
    if-eqz v2, :cond_5

    move-object/from16 v0, p2

    iget v2, v0, Lsoftware/simplicial/a/bh;->l:F

    move v7, v2

    .line 1124
    :goto_9
    if-eqz v8, :cond_6

    move-object/from16 v0, p2

    iget v2, v0, Lsoftware/simplicial/a/bh;->m:F

    .line 1125
    :goto_a
    move-object/from16 v0, p2

    iget v8, v0, Lsoftware/simplicial/a/bh;->l:F

    sub-float/2addr v8, v7

    move-object/from16 v0, p2

    iget v0, v0, Lsoftware/simplicial/a/bh;->l:F

    move/from16 v18, v0

    sub-float v7, v18, v7

    mul-float/2addr v7, v8

    move-object/from16 v0, p2

    iget v8, v0, Lsoftware/simplicial/a/bh;->m:F

    sub-float/2addr v8, v2

    move-object/from16 v0, p2

    iget v0, v0, Lsoftware/simplicial/a/bh;->m:F

    move/from16 v18, v0

    sub-float v2, v18, v2

    mul-float/2addr v2, v8

    add-float/2addr v2, v7

    .line 1126
    invoke-virtual/range {p2 .. p2}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v7

    invoke-virtual/range {p2 .. p2}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v8

    mul-float/2addr v7, v8

    cmpg-float v2, v2, v7

    if-gtz v2, :cond_7

    move v8, v6

    .line 1127
    goto/16 :goto_4

    .line 1123
    :cond_5
    move-object/from16 v0, v18

    iget v2, v0, Lsoftware/simplicial/a/bz;->d:F

    move v7, v2

    goto :goto_9

    .line 1124
    :cond_6
    move-object/from16 v0, v18

    iget v2, v0, Lsoftware/simplicial/a/bz;->f:F

    goto :goto_a

    .line 1129
    :cond_7
    add-int/lit8 v5, v5, 0x1

    .line 1130
    goto/16 :goto_1

    .line 1136
    :cond_8
    const/4 v2, 0x0

    goto :goto_5

    .line 1137
    :cond_9
    const/4 v6, 0x0

    move v7, v6

    goto :goto_6

    .line 1140
    :cond_a
    if-eqz v2, :cond_c

    move-object/from16 v0, p2

    iget v2, v0, Lsoftware/simplicial/a/bh;->l:F

    move v6, v2

    .line 1141
    :goto_b
    if-eqz v7, :cond_d

    move-object/from16 v0, p2

    iget v2, v0, Lsoftware/simplicial/a/bh;->m:F

    .line 1142
    :goto_c
    move-object/from16 v0, p2

    iget v7, v0, Lsoftware/simplicial/a/bh;->l:F

    sub-float/2addr v7, v6

    move-object/from16 v0, p2

    iget v0, v0, Lsoftware/simplicial/a/bh;->l:F

    move/from16 v18, v0

    sub-float v6, v18, v6

    mul-float/2addr v6, v7

    move-object/from16 v0, p2

    iget v7, v0, Lsoftware/simplicial/a/bh;->m:F

    sub-float/2addr v7, v2

    move-object/from16 v0, p2

    iget v0, v0, Lsoftware/simplicial/a/bh;->m:F

    move/from16 v18, v0

    sub-float v2, v18, v2

    mul-float/2addr v2, v7

    add-float/2addr v2, v6

    .line 1143
    invoke-virtual/range {p2 .. p2}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v6

    invoke-virtual/range {p2 .. p2}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v7

    mul-float/2addr v6, v7

    cmpg-float v2, v2, v6

    if-lez v2, :cond_0

    .line 1146
    :cond_b
    add-int/lit8 v2, v8, -0x1

    move v8, v2

    .line 1147
    goto/16 :goto_4

    .line 1140
    :cond_c
    move-object/from16 v0, v18

    iget v2, v0, Lsoftware/simplicial/a/bz;->c:F

    move v6, v2

    goto :goto_b

    .line 1141
    :cond_d
    move-object/from16 v0, v18

    iget v2, v0, Lsoftware/simplicial/a/bz;->f:F

    goto :goto_c

    .line 1154
    :cond_e
    invoke-virtual/range {p2 .. p2}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v7

    iput v7, v6, Lsoftware/simplicial/a/bz;->h:F

    .line 1155
    if-eqz p3, :cond_f

    .line 1156
    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 1158
    :cond_f
    iget-object v7, v6, Lsoftware/simplicial/a/bz;->g:Lsoftware/simplicial/a/bx;

    .line 1159
    move-object/from16 v0, p1

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    move-object/from16 v18, v0

    .line 1160
    move-object/from16 v0, v18

    if-eq v7, v0, :cond_1

    .line 1162
    if-eqz v7, :cond_10

    .line 1163
    iget v0, v7, Lsoftware/simplicial/a/bx;->e:I

    move/from16 v19, v0

    add-int/lit8 v19, v19, -0x1

    move/from16 v0, v19

    iput v0, v7, Lsoftware/simplicial/a/bx;->e:I

    .line 1164
    :cond_10
    move-object/from16 v0, v18

    iput-object v0, v6, Lsoftware/simplicial/a/bz;->g:Lsoftware/simplicial/a/bx;

    .line 1165
    if-eqz v18, :cond_11

    .line 1166
    move-object/from16 v0, v18

    iget v7, v0, Lsoftware/simplicial/a/bx;->e:I

    add-int/lit8 v7, v7, 0x1

    move-object/from16 v0, v18

    iput v7, v0, Lsoftware/simplicial/a/bx;->e:I

    .line 1167
    :cond_11
    invoke-interface {v11, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_8

    .line 1111
    :cond_12
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    move v6, v8

    goto/16 :goto_0

    .line 1173
    :cond_13
    add-int/lit8 v2, v10, -0x1

    move v7, v2

    :goto_d
    move/from16 v0, v16

    if-lt v7, v0, :cond_27

    .line 1175
    :goto_e
    if-gt v3, v4, :cond_28

    .line 1177
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/ai;->as:[[Lsoftware/simplicial/a/bz;

    aget-object v2, v2, v3

    aget-object v8, v2, v7

    .line 1178
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/ai;->aq:[Lsoftware/simplicial/a/ca;

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/ai;->aN:I

    move-object/from16 v0, p2

    invoke-static {v8, v0, v2, v5}, Lsoftware/simplicial/a/ao;->a(Lsoftware/simplicial/a/ao;Lsoftware/simplicial/a/ao;[Lsoftware/simplicial/a/ca;I)Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 1180
    iget v2, v8, Lsoftware/simplicial/a/bz;->c:F

    cmpl-float v2, v12, v2

    if-ltz v2, :cond_16

    iget v2, v8, Lsoftware/simplicial/a/bz;->d:F

    cmpg-float v2, v13, v2

    if-gtz v2, :cond_16

    const/4 v2, 0x1

    .line 1181
    :goto_f
    iget v5, v8, Lsoftware/simplicial/a/bz;->f:F

    cmpl-float v5, v15, v5

    if-ltz v5, :cond_17

    iget v5, v8, Lsoftware/simplicial/a/bz;->e:F

    cmpg-float v5, v14, v5

    if-gtz v5, :cond_17

    const/4 v5, 0x1

    move v6, v5

    .line 1182
    :goto_10
    if-eqz v2, :cond_18

    if-eqz v6, :cond_18

    move v6, v4

    .line 1192
    :goto_11
    if-gt v3, v6, :cond_14

    .line 1194
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/ai;->as:[[Lsoftware/simplicial/a/bz;

    aget-object v2, v2, v6

    aget-object v8, v2, v7

    .line 1195
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/ai;->aq:[Lsoftware/simplicial/a/ca;

    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/ai;->aN:I

    move-object/from16 v0, p2

    invoke-static {v8, v0, v2, v4}, Lsoftware/simplicial/a/ao;->a(Lsoftware/simplicial/a/ao;Lsoftware/simplicial/a/ao;[Lsoftware/simplicial/a/ca;I)Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 1197
    iget v2, v8, Lsoftware/simplicial/a/bz;->c:F

    cmpl-float v2, v12, v2

    if-ltz v2, :cond_1c

    iget v2, v8, Lsoftware/simplicial/a/bz;->d:F

    cmpg-float v2, v13, v2

    if-gtz v2, :cond_1c

    const/4 v2, 0x1

    .line 1198
    :goto_12
    iget v4, v8, Lsoftware/simplicial/a/bz;->f:F

    cmpl-float v4, v15, v4

    if-ltz v4, :cond_1d

    iget v4, v8, Lsoftware/simplicial/a/bz;->e:F

    cmpg-float v4, v14, v4

    if-gtz v4, :cond_1d

    const/4 v4, 0x1

    move v5, v4

    .line 1199
    :goto_13
    if-eqz v2, :cond_1e

    if-eqz v5, :cond_1e

    :cond_14
    move v2, v3

    .line 1209
    :goto_14
    if-gt v2, v6, :cond_26

    .line 1211
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/ai;->as:[[Lsoftware/simplicial/a/bz;

    aget-object v4, v4, v2

    aget-object v4, v4, v7

    .line 1213
    invoke-virtual/range {p2 .. p2}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v5

    iget v8, v4, Lsoftware/simplicial/a/bz;->h:F

    cmpg-float v5, v5, v8

    if-gtz v5, :cond_22

    .line 1209
    :cond_15
    :goto_15
    add-int/lit8 v2, v2, 0x1

    goto :goto_14

    .line 1180
    :cond_16
    const/4 v2, 0x0

    goto :goto_f

    .line 1181
    :cond_17
    const/4 v5, 0x0

    move v6, v5

    goto :goto_10

    .line 1184
    :cond_18
    if-eqz v2, :cond_19

    move-object/from16 v0, p2

    iget v2, v0, Lsoftware/simplicial/a/bh;->l:F

    move v5, v2

    .line 1185
    :goto_16
    if-eqz v6, :cond_1a

    move-object/from16 v0, p2

    iget v2, v0, Lsoftware/simplicial/a/bh;->m:F

    .line 1186
    :goto_17
    move-object/from16 v0, p2

    iget v6, v0, Lsoftware/simplicial/a/bh;->l:F

    sub-float/2addr v6, v5

    move-object/from16 v0, p2

    iget v8, v0, Lsoftware/simplicial/a/bh;->l:F

    sub-float v5, v8, v5

    mul-float/2addr v5, v6

    move-object/from16 v0, p2

    iget v6, v0, Lsoftware/simplicial/a/bh;->m:F

    sub-float/2addr v6, v2

    move-object/from16 v0, p2

    iget v8, v0, Lsoftware/simplicial/a/bh;->m:F

    sub-float v2, v8, v2

    mul-float/2addr v2, v6

    add-float/2addr v2, v5

    .line 1187
    invoke-virtual/range {p2 .. p2}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v5

    invoke-virtual/range {p2 .. p2}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v6

    mul-float/2addr v5, v6

    cmpg-float v2, v2, v5

    if-gtz v2, :cond_1b

    move v6, v4

    .line 1188
    goto/16 :goto_11

    .line 1184
    :cond_19
    iget v2, v8, Lsoftware/simplicial/a/bz;->d:F

    move v5, v2

    goto :goto_16

    .line 1185
    :cond_1a
    iget v2, v8, Lsoftware/simplicial/a/bz;->e:F

    goto :goto_17

    .line 1190
    :cond_1b
    add-int/lit8 v3, v3, 0x1

    .line 1191
    goto/16 :goto_e

    .line 1197
    :cond_1c
    const/4 v2, 0x0

    goto :goto_12

    .line 1198
    :cond_1d
    const/4 v4, 0x0

    move v5, v4

    goto :goto_13

    .line 1201
    :cond_1e
    if-eqz v2, :cond_20

    move-object/from16 v0, p2

    iget v2, v0, Lsoftware/simplicial/a/bh;->l:F

    move v4, v2

    .line 1202
    :goto_18
    if-eqz v5, :cond_21

    move-object/from16 v0, p2

    iget v2, v0, Lsoftware/simplicial/a/bh;->m:F

    .line 1203
    :goto_19
    move-object/from16 v0, p2

    iget v5, v0, Lsoftware/simplicial/a/bh;->l:F

    sub-float/2addr v5, v4

    move-object/from16 v0, p2

    iget v8, v0, Lsoftware/simplicial/a/bh;->l:F

    sub-float v4, v8, v4

    mul-float/2addr v4, v5

    move-object/from16 v0, p2

    iget v5, v0, Lsoftware/simplicial/a/bh;->m:F

    sub-float/2addr v5, v2

    move-object/from16 v0, p2

    iget v8, v0, Lsoftware/simplicial/a/bh;->m:F

    sub-float v2, v8, v2

    mul-float/2addr v2, v5

    add-float/2addr v2, v4

    .line 1204
    invoke-virtual/range {p2 .. p2}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v4

    invoke-virtual/range {p2 .. p2}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v5

    mul-float/2addr v4, v5

    cmpg-float v2, v2, v4

    if-lez v2, :cond_14

    .line 1207
    :cond_1f
    add-int/lit8 v2, v6, -0x1

    move v6, v2

    .line 1208
    goto/16 :goto_11

    .line 1201
    :cond_20
    iget v2, v8, Lsoftware/simplicial/a/bz;->c:F

    move v4, v2

    goto :goto_18

    .line 1202
    :cond_21
    iget v2, v8, Lsoftware/simplicial/a/bz;->e:F

    goto :goto_19

    .line 1215
    :cond_22
    invoke-virtual/range {p2 .. p2}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v5

    iput v5, v4, Lsoftware/simplicial/a/bz;->h:F

    .line 1216
    if-eqz p3, :cond_23

    .line 1217
    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 1219
    :cond_23
    iget-object v5, v4, Lsoftware/simplicial/a/bz;->g:Lsoftware/simplicial/a/bx;

    .line 1220
    move-object/from16 v0, p1

    iget-object v8, v0, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    .line 1221
    if-eq v5, v8, :cond_15

    .line 1223
    if-eqz v5, :cond_24

    .line 1224
    iget v9, v5, Lsoftware/simplicial/a/bx;->e:I

    add-int/lit8 v9, v9, -0x1

    iput v9, v5, Lsoftware/simplicial/a/bx;->e:I

    .line 1225
    :cond_24
    iput-object v8, v4, Lsoftware/simplicial/a/bz;->g:Lsoftware/simplicial/a/bx;

    .line 1226
    if-eqz v8, :cond_25

    .line 1227
    iget v5, v8, Lsoftware/simplicial/a/bx;->e:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v8, Lsoftware/simplicial/a/bx;->e:I

    .line 1228
    :cond_25
    invoke-interface {v11, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_15

    .line 1173
    :cond_26
    add-int/lit8 v2, v7, -0x1

    move v7, v2

    move v4, v6

    goto/16 :goto_d

    .line 1233
    :cond_27
    return-object v11

    :cond_28
    move v6, v4

    goto/16 :goto_11

    :cond_29
    move v8, v6

    goto/16 :goto_4
.end method

.method protected a(Lsoftware/simplicial/a/bf;)V
    .locals 2

    .prologue
    .line 1293
    iget-object v0, p0, Lsoftware/simplicial/a/ai;->ac:Lsoftware/simplicial/a/bf;

    if-eqz v0, :cond_0

    .line 1294
    iget-object v0, p0, Lsoftware/simplicial/a/ai;->ac:Lsoftware/simplicial/a/bf;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/bf;->a(Z)V

    .line 1295
    :cond_0
    iput-object p1, p0, Lsoftware/simplicial/a/ai;->ac:Lsoftware/simplicial/a/bf;

    .line 1296
    iget-object v0, p0, Lsoftware/simplicial/a/ai;->ac:Lsoftware/simplicial/a/bf;

    if-eqz v0, :cond_1

    .line 1297
    iget-object v0, p0, Lsoftware/simplicial/a/ai;->ac:Lsoftware/simplicial/a/bf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/bf;->a(Z)V

    .line 1299
    :cond_1
    return-void
.end method

.method public a(Lsoftware/simplicial/a/f/be;)V
    .locals 1

    .prologue
    .line 1079
    iget-boolean v0, p0, Lsoftware/simplicial/a/ai;->aL:Z

    if-eqz v0, :cond_0

    .line 1084
    :goto_0
    return-void

    .line 1083
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/a/ai;->aB:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public declared-synchronized a(ZZ)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 973
    monitor-enter p0

    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, p0, Lsoftware/simplicial/a/ai;->aL:Z

    .line 975
    if-eqz p2, :cond_0

    .line 976
    invoke-direct {p0}, Lsoftware/simplicial/a/ai;->a()V

    .line 978
    :cond_0
    iget-boolean v2, p0, Lsoftware/simplicial/a/ai;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    .line 996
    :goto_0
    monitor-exit p0

    return v0

    .line 981
    :cond_1
    const/4 v2, 0x1

    :try_start_1
    iput-boolean v2, p0, Lsoftware/simplicial/a/ai;->c:Z

    .line 982
    iget-object v2, p0, Lsoftware/simplicial/a/ai;->aK:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 983
    if-eqz p1, :cond_2

    .line 987
    :try_start_2
    iget-object v2, p0, Lsoftware/simplicial/a/ai;->aK:Ljava/lang/Thread;

    const/16 v3, 0x2710

    iget v4, p0, Lsoftware/simplicial/a/ai;->a:I

    div-int/2addr v3, v4

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Ljava/lang/Thread;->join(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_2
    move v0, v1

    .line 996
    goto :goto_0

    .line 989
    :catch_0
    move-exception v1

    .line 991
    :try_start_3
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 973
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1066
    iget-boolean v0, p0, Lsoftware/simplicial/a/ai;->aM:Z

    if-eqz v0, :cond_0

    .line 1071
    :goto_0
    return v2

    .line 1069
    :cond_0
    iget v0, p0, Lsoftware/simplicial/a/ai;->aN:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsoftware/simplicial/a/ai;->aN:I

    .line 1070
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lsoftware/simplicial/a/ai;->aO:J

    goto :goto_0
.end method

.method public declared-synchronized k()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 957
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lsoftware/simplicial/a/ai;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_0

    .line 967
    :goto_0
    monitor-exit p0

    return v0

    .line 960
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lsoftware/simplicial/a/ai;->c:Z

    .line 961
    new-instance v2, Ljava/lang/Thread;

    instance-of v0, p0, Lsoftware/simplicial/a/bs;

    if-eqz v0, :cond_1

    const-string v0, "SG"

    :goto_1
    invoke-direct {v2, p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v2, p0, Lsoftware/simplicial/a/ai;->aK:Ljava/lang/Thread;

    .line 962
    iget-object v0, p0, Lsoftware/simplicial/a/ai;->aK:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 964
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/a/ai;->b:Z

    .line 965
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/a/ai;->aL:Z

    move v0, v1

    .line 967
    goto :goto_0

    .line 961
    :cond_1
    const-string v0, "CG"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 957
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public l()V
    .locals 1

    .prologue
    .line 1088
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/a/ai;->aM:Z

    .line 1089
    return-void
.end method

.method public m()V
    .locals 1

    .prologue
    .line 1093
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/a/ai;->aM:Z

    .line 1094
    return-void
.end method

.method protected o()I
    .locals 4

    .prologue
    const/16 v0, 0x96

    const/16 v3, 0x7e

    .line 1276
    iget-object v1, p0, Lsoftware/simplicial/a/ai;->aU:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    if-ne v1, v2, :cond_0

    move v1, v0

    move v2, v0

    .line 1288
    :goto_0
    const/high16 v3, -0x1000000

    shl-int/lit8 v0, v0, 0x10

    or-int/2addr v0, v3

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    or-int/2addr v0, v2

    return v0

    .line 1284
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/a/ai;->ad:Ljava/util/Random;

    invoke-virtual {v0, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    add-int/lit8 v2, v0, 0x64

    .line 1285
    iget-object v0, p0, Lsoftware/simplicial/a/ai;->ad:Ljava/util/Random;

    invoke-virtual {v0, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    add-int/lit8 v1, v0, 0x64

    .line 1286
    iget-object v0, p0, Lsoftware/simplicial/a/ai;->ad:Ljava/util/Random;

    invoke-virtual {v0, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x64

    goto :goto_0
.end method

.method public p()I
    .locals 2

    .prologue
    .line 1303
    iget-object v0, p0, Lsoftware/simplicial/a/ai;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->r:Lsoftware/simplicial/a/am;

    if-ne v0, v1, :cond_0

    .line 1304
    const/16 v0, 0x10

    .line 1305
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1021
    :goto_0
    :try_start_0
    iget-boolean v0, p0, Lsoftware/simplicial/a/ai;->c:Z

    if-nez v0, :cond_0

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    if-nez v0, :cond_0

    .line 1025
    :try_start_1
    iget-boolean v0, p0, Lsoftware/simplicial/a/ai;->aM:Z

    if-nez v0, :cond_3

    .line 1027
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 1029
    invoke-virtual {p0}, Lsoftware/simplicial/a/ai;->c()Z

    .line 1031
    const v2, 0x4e6e6b28    # 1.0E9f

    iget v3, p0, Lsoftware/simplicial/a/ai;->ab:F

    mul-float/2addr v2, v3

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    sub-long v0, v4, v0

    long-to-float v0, v0

    sub-float v0, v2, v0

    const v1, 0x49742400    # 1000000.0f

    div-float/2addr v0, v1

    float-to-long v2, v0

    .line 1032
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-lez v0, :cond_1

    .line 1033
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 1045
    :catch_0
    move-exception v0

    .line 1060
    :cond_0
    :goto_1
    iput-boolean v7, p0, Lsoftware/simplicial/a/ai;->b:Z

    .line 1061
    return-void

    .line 1035
    :cond_1
    :try_start_2
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v4, "%s frame blown: %d"

    const/4 v0, 0x2

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v6, 0x0

    instance-of v0, p0, Lsoftware/simplicial/a/bs;

    if-eqz v0, :cond_2

    const-string v0, "SG"

    :goto_2
    aput-object v0, v5, v6

    const/4 v0, 0x1

    neg-long v2, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v5, v0

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 1051
    :catch_1
    move-exception v0

    .line 1053
    iput-boolean v8, p0, Lsoftware/simplicial/a/ai;->aL:Z

    .line 1054
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 1055
    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 1056
    invoke-virtual {v0, v2}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 1057
    iget-object v1, p0, Lsoftware/simplicial/a/ai;->aJ:Lsoftware/simplicial/a/al;

    invoke-interface {v1, p0, v0}, Lsoftware/simplicial/a/al;->a(Lsoftware/simplicial/a/ai;Ljava/lang/Exception;)V

    goto :goto_1

    .line 1035
    :cond_2
    :try_start_3
    const-string v0, "CG"

    goto :goto_2

    .line 1039
    :cond_3
    const-wide/16 v0, 0xa

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0
.end method
