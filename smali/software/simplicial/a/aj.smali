.class public abstract Lsoftware/simplicial/a/aj;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final aP:I

.field public final aQ:Lsoftware/simplicial/a/ap;

.field public final aR:Z

.field public final aS:[Z

.field public final aT:Z

.field public final aU:Lsoftware/simplicial/a/am;

.field public final aV:Lsoftware/simplicial/a/aq;

.field public final aW:Lsoftware/simplicial/a/ac;

.field public final aX:S

.field public final aY:I

.field public aZ:I

.field public ba:Ljava/lang/String;

.field public bb:[B


# direct methods
.method protected constructor <init>(IIILsoftware/simplicial/a/ap;Z[ZZLsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;Ljava/lang/String;[BSLsoftware/simplicial/a/ac;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {p8, p4}, Lsoftware/simplicial/a/ai;->b(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/ap;)I

    move-result v0

    if-le p1, v0, :cond_1

    .line 30
    invoke-static {p8, p4}, Lsoftware/simplicial/a/ai;->b(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/ap;)I

    move-result v0

    .line 31
    :goto_0
    if-le p3, v0, :cond_0

    move p3, v0

    .line 34
    :cond_0
    iput p2, p0, Lsoftware/simplicial/a/aj;->aZ:I

    .line 35
    iput-object p10, p0, Lsoftware/simplicial/a/aj;->ba:Ljava/lang/String;

    .line 36
    iput-object p11, p0, Lsoftware/simplicial/a/aj;->bb:[B

    .line 37
    iput p3, p0, Lsoftware/simplicial/a/aj;->aP:I

    .line 38
    iput-object p4, p0, Lsoftware/simplicial/a/aj;->aQ:Lsoftware/simplicial/a/ap;

    .line 39
    iput-boolean p5, p0, Lsoftware/simplicial/a/aj;->aR:Z

    .line 40
    iput-object p6, p0, Lsoftware/simplicial/a/aj;->aS:[Z

    .line 41
    iput-boolean p7, p0, Lsoftware/simplicial/a/aj;->aT:Z

    .line 42
    iput-object p8, p0, Lsoftware/simplicial/a/aj;->aU:Lsoftware/simplicial/a/am;

    .line 43
    iput-object p9, p0, Lsoftware/simplicial/a/aj;->aV:Lsoftware/simplicial/a/aq;

    .line 44
    iput v0, p0, Lsoftware/simplicial/a/aj;->aY:I

    .line 45
    invoke-static {p8, p12}, Lsoftware/simplicial/a/ai;->a(Lsoftware/simplicial/a/am;S)S

    move-result v0

    iput-short v0, p0, Lsoftware/simplicial/a/aj;->aX:S

    .line 46
    iput-object p13, p0, Lsoftware/simplicial/a/aj;->aW:Lsoftware/simplicial/a/ac;

    .line 47
    return-void

    :cond_1
    move v0, p1

    goto :goto_0
.end method
