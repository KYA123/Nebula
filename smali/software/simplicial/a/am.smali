.class public final enum Lsoftware/simplicial/a/am;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/am;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/am;

.field public static final enum b:Lsoftware/simplicial/a/am;

.field public static final enum c:Lsoftware/simplicial/a/am;

.field public static final enum d:Lsoftware/simplicial/a/am;

.field public static final enum e:Lsoftware/simplicial/a/am;

.field public static final enum f:Lsoftware/simplicial/a/am;

.field public static final enum g:Lsoftware/simplicial/a/am;

.field public static final enum h:Lsoftware/simplicial/a/am;

.field public static final enum i:Lsoftware/simplicial/a/am;

.field public static final enum j:Lsoftware/simplicial/a/am;

.field public static final enum k:Lsoftware/simplicial/a/am;

.field public static final enum l:Lsoftware/simplicial/a/am;

.field public static final enum m:Lsoftware/simplicial/a/am;

.field public static final enum n:Lsoftware/simplicial/a/am;

.field public static final enum o:Lsoftware/simplicial/a/am;

.field public static final enum p:Lsoftware/simplicial/a/am;

.field public static final enum q:Lsoftware/simplicial/a/am;

.field public static final enum r:Lsoftware/simplicial/a/am;

.field public static final s:[Lsoftware/simplicial/a/am;

.field private static final synthetic t:[Lsoftware/simplicial/a/am;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 8
    new-instance v0, Lsoftware/simplicial/a/am;

    const-string v1, "FFA"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/am;->a:Lsoftware/simplicial/a/am;

    new-instance v0, Lsoftware/simplicial/a/am;

    const-string v1, "FFA_TIME"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/am;->b:Lsoftware/simplicial/a/am;

    new-instance v0, Lsoftware/simplicial/a/am;

    const-string v1, "TEAMS"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/a/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/am;->c:Lsoftware/simplicial/a/am;

    new-instance v0, Lsoftware/simplicial/a/am;

    const-string v1, "TEAMS_TIME"

    invoke-direct {v0, v1, v6}, Lsoftware/simplicial/a/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/am;->d:Lsoftware/simplicial/a/am;

    new-instance v0, Lsoftware/simplicial/a/am;

    const-string v1, "CTF"

    invoke-direct {v0, v1, v7}, Lsoftware/simplicial/a/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/am;->e:Lsoftware/simplicial/a/am;

    new-instance v0, Lsoftware/simplicial/a/am;

    const-string v1, "SURVIVAL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/am;->f:Lsoftware/simplicial/a/am;

    new-instance v0, Lsoftware/simplicial/a/am;

    const-string v1, "SOCCER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    new-instance v0, Lsoftware/simplicial/a/am;

    const-string v1, "FFA_CLASSIC"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    new-instance v0, Lsoftware/simplicial/a/am;

    const-string v1, "DOMINATION"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/am;->i:Lsoftware/simplicial/a/am;

    new-instance v0, Lsoftware/simplicial/a/am;

    const-string v1, "FFA_ULTRA"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/am;->j:Lsoftware/simplicial/a/am;

    new-instance v0, Lsoftware/simplicial/a/am;

    const-string v1, "ZA"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    new-instance v0, Lsoftware/simplicial/a/am;

    const-string v1, "PAINT"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/am;->l:Lsoftware/simplicial/a/am;

    new-instance v0, Lsoftware/simplicial/a/am;

    const-string v1, "TEAM_DEATHMATCH"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    new-instance v0, Lsoftware/simplicial/a/am;

    const-string v1, "X"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    new-instance v0, Lsoftware/simplicial/a/am;

    const-string v1, "X2"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    new-instance v0, Lsoftware/simplicial/a/am;

    const-string v1, "X3"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/am;->p:Lsoftware/simplicial/a/am;

    new-instance v0, Lsoftware/simplicial/a/am;

    const-string v1, "X4"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/am;->q:Lsoftware/simplicial/a/am;

    new-instance v0, Lsoftware/simplicial/a/am;

    const-string v1, "X5"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/am;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/am;->r:Lsoftware/simplicial/a/am;

    .line 6
    const/16 v0, 0x12

    new-array v0, v0, [Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->a:Lsoftware/simplicial/a/am;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/am;->b:Lsoftware/simplicial/a/am;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/a/am;->c:Lsoftware/simplicial/a/am;

    aput-object v1, v0, v5

    sget-object v1, Lsoftware/simplicial/a/am;->d:Lsoftware/simplicial/a/am;

    aput-object v1, v0, v6

    sget-object v1, Lsoftware/simplicial/a/am;->e:Lsoftware/simplicial/a/am;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lsoftware/simplicial/a/am;->f:Lsoftware/simplicial/a/am;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lsoftware/simplicial/a/am;->i:Lsoftware/simplicial/a/am;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lsoftware/simplicial/a/am;->j:Lsoftware/simplicial/a/am;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lsoftware/simplicial/a/am;->l:Lsoftware/simplicial/a/am;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lsoftware/simplicial/a/am;->p:Lsoftware/simplicial/a/am;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lsoftware/simplicial/a/am;->q:Lsoftware/simplicial/a/am;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lsoftware/simplicial/a/am;->r:Lsoftware/simplicial/a/am;

    aput-object v2, v0, v1

    sput-object v0, Lsoftware/simplicial/a/am;->t:[Lsoftware/simplicial/a/am;

    .line 10
    invoke-static {}, Lsoftware/simplicial/a/am;->values()[Lsoftware/simplicial/a/am;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/a/am;->s:[Lsoftware/simplicial/a/am;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(B)Lsoftware/simplicial/a/am;
    .locals 1

    .prologue
    .line 14
    if-ltz p0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/am;->s:[Lsoftware/simplicial/a/am;

    array-length v0, v0

    if-lt p0, v0, :cond_1

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lsoftware/simplicial/a/am;->s:[Lsoftware/simplicial/a/am;

    aget-object v0, v0, p0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/am;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lsoftware/simplicial/a/am;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/am;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/am;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lsoftware/simplicial/a/am;->t:[Lsoftware/simplicial/a/am;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/am;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/am;

    return-object v0
.end method
