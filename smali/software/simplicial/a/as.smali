.class public Lsoftware/simplicial/a/as;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lsoftware/simplicial/a/as;

.field public static final b:[Lsoftware/simplicial/a/as;


# instance fields
.field public final c:B

.field public d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v1, -0x1

    .line 10
    new-instance v0, Lsoftware/simplicial/a/as;

    invoke-direct {v0, v1, v1}, Lsoftware/simplicial/a/as;-><init>(BI)V

    sput-object v0, Lsoftware/simplicial/a/as;->a:Lsoftware/simplicial/a/as;

    .line 12
    const/4 v0, 0x5

    new-array v0, v0, [Lsoftware/simplicial/a/as;

    sput-object v0, Lsoftware/simplicial/a/as;->b:[Lsoftware/simplicial/a/as;

    .line 16
    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lsoftware/simplicial/a/as;->b:[Lsoftware/simplicial/a/as;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 17
    sget-object v1, Lsoftware/simplicial/a/as;->b:[Lsoftware/simplicial/a/as;

    new-instance v2, Lsoftware/simplicial/a/as;

    int-to-byte v3, v0

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Lsoftware/simplicial/a/as;-><init>(BI)V

    aput-object v2, v1, v0

    .line 16
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 18
    :cond_0
    return-void
.end method

.method public constructor <init>(BI)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-byte p1, p0, Lsoftware/simplicial/a/as;->c:B

    .line 26
    iput p2, p0, Lsoftware/simplicial/a/as;->d:I

    .line 27
    return-void
.end method

.method public static a(I)Lsoftware/simplicial/a/as;
    .locals 1

    .prologue
    .line 42
    if-ltz p0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/as;->b:[Lsoftware/simplicial/a/as;

    array-length v0, v0

    if-lt p0, v0, :cond_1

    .line 43
    :cond_0
    sget-object v0, Lsoftware/simplicial/a/as;->a:Lsoftware/simplicial/a/as;

    .line 44
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lsoftware/simplicial/a/as;->b:[Lsoftware/simplicial/a/as;

    aget-object v0, v0, p0

    goto :goto_0
.end method

.method public static a(Lsoftware/simplicial/a/as;Ljava/util/Set;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsoftware/simplicial/a/as;",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/as;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 32
    sget-object v1, Lsoftware/simplicial/a/as;->a:Lsoftware/simplicial/a/as;

    if-ne p0, v1, :cond_1

    .line 37
    :cond_0
    :goto_0
    return v0

    .line 34
    :cond_1
    invoke-interface {p1, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 37
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 50
    sget-object v0, Lsoftware/simplicial/a/as;->a:Lsoftware/simplicial/a/as;

    if-ne p0, v0, :cond_0

    .line 51
    const-string v0, "misc_none"

    .line 52
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "hat_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-byte v1, p0, Lsoftware/simplicial/a/as;->c:B

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
