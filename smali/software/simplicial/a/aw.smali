.class public Lsoftware/simplicial/a/aw;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(I)I
    .locals 1

    .prologue
    .line 13
    rem-int/lit8 v0, p0, 0xa

    if-nez v0, :cond_0

    .line 15
    const/16 v0, 0x64

    .line 17
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x19

    goto :goto_0
.end method

.method public static a(J)I
    .locals 4

    .prologue
    .line 22
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    .line 23
    const/4 v0, 0x1

    .line 24
    :goto_0
    return v0

    :cond_0
    long-to-double v0, p0

    const-wide v2, 0x407f400000000000L    # 500.0

    div-double/2addr v0, v2

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    invoke-static {v0, v1, v2, v3}, Lsoftware/simplicial/a/bb;->a(DD)D

    move-result-wide v0

    double-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static b(J)I
    .locals 4

    .prologue
    .line 38
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    .line 39
    const/4 v0, 0x1

    .line 40
    :goto_0
    return v0

    :cond_0
    long-to-double v0, p0

    const-wide v2, 0x407f400000000000L    # 500.0

    div-double/2addr v0, v2

    const-wide/high16 v2, 0x3fd0000000000000L    # 0.25

    invoke-static {v0, v1, v2, v3}, Lsoftware/simplicial/a/bb;->a(DD)D

    move-result-wide v0

    double-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static b(I)J
    .locals 4

    .prologue
    const-wide/16 v0, 0x0

    .line 29
    if-gtz p0, :cond_1

    .line 33
    :cond_0
    :goto_0
    return-wide v0

    .line 31
    :cond_1
    const/4 v2, 0x1

    if-le p0, v2, :cond_0

    .line 33
    int-to-double v0, p0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1, v2, v3}, Lsoftware/simplicial/a/bb;->a(DD)D

    move-result-wide v0

    double-to-long v0, v0

    const-wide/16 v2, 0x1f4

    mul-long/2addr v0, v2

    goto :goto_0
.end method

.method public static c(I)J
    .locals 4

    .prologue
    const-wide/16 v0, 0x0

    .line 45
    if-gtz p0, :cond_1

    .line 49
    :cond_0
    :goto_0
    return-wide v0

    .line 47
    :cond_1
    const/4 v2, 0x1

    if-le p0, v2, :cond_0

    .line 49
    int-to-double v0, p0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v0, v2

    const-wide/high16 v2, 0x4010000000000000L    # 4.0

    invoke-static {v0, v1, v2, v3}, Lsoftware/simplicial/a/bb;->a(DD)D

    move-result-wide v0

    double-to-long v0, v0

    const-wide/16 v2, 0x1f4

    mul-long/2addr v0, v2

    goto :goto_0
.end method
