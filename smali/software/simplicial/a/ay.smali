.class public Lsoftware/simplicial/a/ay;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public A:I

.field public B:J

.field public C:J

.field public D:I

.field public E:I

.field public F:I

.field public G:I

.field public H:I

.field public I:I

.field public J:I

.field public K:I

.field public L:B

.field public M:Z

.field public N:Z

.field public O:Z

.field public P:Z

.field public Q:Z

.field public R:Z

.field public S:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/d;",
            ">;"
        }
    .end annotation
.end field

.field public T:J

.field public U:I

.field public V:J

.field public W:J

.field public X:I

.field public Y:I

.field public a:I

.field public b:Lsoftware/simplicial/a/am;

.field public c:Z

.field public d:Lsoftware/simplicial/a/aq;

.field public e:Ljava/lang/String;

.field public f:[B

.field public g:J

.field public h:I

.field public i:I

.field public j:I

.field public k:I

.field public l:I

.field public m:I

.field public n:I

.field public o:I

.field public p:I

.field public q:I

.field public r:I

.field public s:I

.field public t:I

.field public u:I

.field public v:I

.field public w:I

.field public x:I

.field public y:I

.field public z:I


# direct methods
.method public constructor <init>(ILsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;Ljava/lang/String;[BZ)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-wide v2, p0, Lsoftware/simplicial/a/ay;->g:J

    .line 44
    iput-wide v2, p0, Lsoftware/simplicial/a/ay;->B:J

    .line 45
    iput-wide v2, p0, Lsoftware/simplicial/a/ay;->C:J

    .line 55
    iput-boolean v1, p0, Lsoftware/simplicial/a/ay;->M:Z

    .line 56
    iput-boolean v1, p0, Lsoftware/simplicial/a/ay;->N:Z

    .line 57
    iput-boolean v1, p0, Lsoftware/simplicial/a/ay;->O:Z

    .line 58
    iput-boolean v1, p0, Lsoftware/simplicial/a/ay;->P:Z

    .line 59
    iput-boolean v1, p0, Lsoftware/simplicial/a/ay;->Q:Z

    .line 62
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/ay;->S:Ljava/util/Set;

    .line 65
    iput-wide v2, p0, Lsoftware/simplicial/a/ay;->T:J

    .line 66
    iput v1, p0, Lsoftware/simplicial/a/ay;->U:I

    .line 67
    iput-wide v2, p0, Lsoftware/simplicial/a/ay;->V:J

    .line 68
    iput-wide v2, p0, Lsoftware/simplicial/a/ay;->W:J

    .line 69
    iput v1, p0, Lsoftware/simplicial/a/ay;->X:I

    .line 70
    iput v1, p0, Lsoftware/simplicial/a/ay;->Y:I

    .line 74
    iput p1, p0, Lsoftware/simplicial/a/ay;->a:I

    .line 75
    iput-object p2, p0, Lsoftware/simplicial/a/ay;->b:Lsoftware/simplicial/a/am;

    .line 76
    iput-boolean p6, p0, Lsoftware/simplicial/a/ay;->c:Z

    .line 77
    iput-object p3, p0, Lsoftware/simplicial/a/ay;->d:Lsoftware/simplicial/a/aq;

    .line 78
    iput-object p4, p0, Lsoftware/simplicial/a/ay;->e:Ljava/lang/String;

    .line 79
    array-length v0, p5

    invoke-static {p5, v0}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/a/ay;->f:[B

    .line 80
    return-void
.end method

.method public constructor <init>(Lsoftware/simplicial/a/ay;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-wide v2, p0, Lsoftware/simplicial/a/ay;->g:J

    .line 44
    iput-wide v2, p0, Lsoftware/simplicial/a/ay;->B:J

    .line 45
    iput-wide v2, p0, Lsoftware/simplicial/a/ay;->C:J

    .line 55
    iput-boolean v1, p0, Lsoftware/simplicial/a/ay;->M:Z

    .line 56
    iput-boolean v1, p0, Lsoftware/simplicial/a/ay;->N:Z

    .line 57
    iput-boolean v1, p0, Lsoftware/simplicial/a/ay;->O:Z

    .line 58
    iput-boolean v1, p0, Lsoftware/simplicial/a/ay;->P:Z

    .line 59
    iput-boolean v1, p0, Lsoftware/simplicial/a/ay;->Q:Z

    .line 62
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/ay;->S:Ljava/util/Set;

    .line 65
    iput-wide v2, p0, Lsoftware/simplicial/a/ay;->T:J

    .line 66
    iput v1, p0, Lsoftware/simplicial/a/ay;->U:I

    .line 67
    iput-wide v2, p0, Lsoftware/simplicial/a/ay;->V:J

    .line 68
    iput-wide v2, p0, Lsoftware/simplicial/a/ay;->W:J

    .line 69
    iput v1, p0, Lsoftware/simplicial/a/ay;->X:I

    .line 70
    iput v1, p0, Lsoftware/simplicial/a/ay;->Y:I

    .line 138
    iget v0, p1, Lsoftware/simplicial/a/ay;->a:I

    iput v0, p0, Lsoftware/simplicial/a/ay;->a:I

    .line 139
    iget-object v0, p1, Lsoftware/simplicial/a/ay;->b:Lsoftware/simplicial/a/am;

    iput-object v0, p0, Lsoftware/simplicial/a/ay;->b:Lsoftware/simplicial/a/am;

    .line 140
    iget-boolean v0, p1, Lsoftware/simplicial/a/ay;->c:Z

    iput-boolean v0, p0, Lsoftware/simplicial/a/ay;->c:Z

    .line 141
    iget-object v0, p1, Lsoftware/simplicial/a/ay;->d:Lsoftware/simplicial/a/aq;

    iput-object v0, p0, Lsoftware/simplicial/a/ay;->d:Lsoftware/simplicial/a/aq;

    .line 142
    iget-object v0, p1, Lsoftware/simplicial/a/ay;->e:Ljava/lang/String;

    iput-object v0, p0, Lsoftware/simplicial/a/ay;->e:Ljava/lang/String;

    .line 143
    iget-object v0, p1, Lsoftware/simplicial/a/ay;->f:[B

    iget-object v1, p1, Lsoftware/simplicial/a/ay;->f:[B

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/a/ay;->f:[B

    .line 145
    iget-wide v0, p1, Lsoftware/simplicial/a/ay;->g:J

    iput-wide v0, p0, Lsoftware/simplicial/a/ay;->g:J

    .line 146
    iget v0, p1, Lsoftware/simplicial/a/ay;->h:I

    iput v0, p0, Lsoftware/simplicial/a/ay;->h:I

    .line 147
    iget v0, p1, Lsoftware/simplicial/a/ay;->i:I

    iput v0, p0, Lsoftware/simplicial/a/ay;->i:I

    .line 148
    iget v0, p1, Lsoftware/simplicial/a/ay;->j:I

    iput v0, p0, Lsoftware/simplicial/a/ay;->j:I

    .line 149
    iget v0, p1, Lsoftware/simplicial/a/ay;->k:I

    iput v0, p0, Lsoftware/simplicial/a/ay;->k:I

    .line 150
    iget v0, p1, Lsoftware/simplicial/a/ay;->l:I

    iput v0, p0, Lsoftware/simplicial/a/ay;->l:I

    .line 151
    iget v0, p1, Lsoftware/simplicial/a/ay;->m:I

    iput v0, p0, Lsoftware/simplicial/a/ay;->m:I

    .line 152
    iget v0, p1, Lsoftware/simplicial/a/ay;->n:I

    iput v0, p0, Lsoftware/simplicial/a/ay;->n:I

    .line 153
    iget v0, p1, Lsoftware/simplicial/a/ay;->o:I

    iput v0, p0, Lsoftware/simplicial/a/ay;->o:I

    .line 154
    iget v0, p1, Lsoftware/simplicial/a/ay;->p:I

    iput v0, p0, Lsoftware/simplicial/a/ay;->p:I

    .line 155
    iget v0, p1, Lsoftware/simplicial/a/ay;->q:I

    iput v0, p0, Lsoftware/simplicial/a/ay;->q:I

    .line 156
    iget v0, p1, Lsoftware/simplicial/a/ay;->r:I

    iput v0, p0, Lsoftware/simplicial/a/ay;->r:I

    .line 157
    iget v0, p1, Lsoftware/simplicial/a/ay;->s:I

    iput v0, p0, Lsoftware/simplicial/a/ay;->s:I

    .line 158
    iget v0, p1, Lsoftware/simplicial/a/ay;->t:I

    iput v0, p0, Lsoftware/simplicial/a/ay;->t:I

    .line 159
    iget v0, p1, Lsoftware/simplicial/a/ay;->u:I

    iput v0, p0, Lsoftware/simplicial/a/ay;->u:I

    .line 160
    iget v0, p1, Lsoftware/simplicial/a/ay;->v:I

    iput v0, p0, Lsoftware/simplicial/a/ay;->v:I

    .line 161
    iget v0, p1, Lsoftware/simplicial/a/ay;->w:I

    iput v0, p0, Lsoftware/simplicial/a/ay;->w:I

    .line 162
    iget v0, p1, Lsoftware/simplicial/a/ay;->x:I

    iput v0, p0, Lsoftware/simplicial/a/ay;->x:I

    .line 163
    iget v0, p1, Lsoftware/simplicial/a/ay;->y:I

    iput v0, p0, Lsoftware/simplicial/a/ay;->y:I

    .line 164
    iget v0, p1, Lsoftware/simplicial/a/ay;->z:I

    iput v0, p0, Lsoftware/simplicial/a/ay;->z:I

    .line 165
    iget v0, p1, Lsoftware/simplicial/a/ay;->A:I

    iput v0, p0, Lsoftware/simplicial/a/ay;->A:I

    .line 166
    iget-wide v0, p1, Lsoftware/simplicial/a/ay;->B:J

    iput-wide v0, p0, Lsoftware/simplicial/a/ay;->B:J

    .line 167
    iget-wide v0, p1, Lsoftware/simplicial/a/ay;->C:J

    iput-wide v0, p0, Lsoftware/simplicial/a/ay;->C:J

    .line 168
    iget-boolean v0, p1, Lsoftware/simplicial/a/ay;->M:Z

    iput-boolean v0, p0, Lsoftware/simplicial/a/ay;->M:Z

    .line 169
    iget-boolean v0, p1, Lsoftware/simplicial/a/ay;->N:Z

    iput-boolean v0, p0, Lsoftware/simplicial/a/ay;->N:Z

    .line 170
    iget-boolean v0, p1, Lsoftware/simplicial/a/ay;->O:Z

    iput-boolean v0, p0, Lsoftware/simplicial/a/ay;->O:Z

    .line 171
    iget-boolean v0, p1, Lsoftware/simplicial/a/ay;->P:Z

    iput-boolean v0, p0, Lsoftware/simplicial/a/ay;->P:Z

    .line 172
    iget-boolean v0, p1, Lsoftware/simplicial/a/ay;->Q:Z

    iput-boolean v0, p0, Lsoftware/simplicial/a/ay;->Q:Z

    .line 173
    iget v0, p1, Lsoftware/simplicial/a/ay;->D:I

    iput v0, p0, Lsoftware/simplicial/a/ay;->D:I

    .line 174
    iget v0, p1, Lsoftware/simplicial/a/ay;->E:I

    iput v0, p0, Lsoftware/simplicial/a/ay;->E:I

    .line 175
    iget v0, p1, Lsoftware/simplicial/a/ay;->F:I

    iput v0, p0, Lsoftware/simplicial/a/ay;->F:I

    .line 176
    iget v0, p1, Lsoftware/simplicial/a/ay;->H:I

    iput v0, p0, Lsoftware/simplicial/a/ay;->H:I

    .line 177
    iget v0, p1, Lsoftware/simplicial/a/ay;->G:I

    iput v0, p0, Lsoftware/simplicial/a/ay;->G:I

    .line 178
    iget v0, p1, Lsoftware/simplicial/a/ay;->I:I

    iput v0, p0, Lsoftware/simplicial/a/ay;->I:I

    .line 179
    iget v0, p1, Lsoftware/simplicial/a/ay;->J:I

    iput v0, p0, Lsoftware/simplicial/a/ay;->J:I

    .line 180
    iget v0, p1, Lsoftware/simplicial/a/ay;->K:I

    iput v0, p0, Lsoftware/simplicial/a/ay;->K:I

    .line 181
    iget-byte v0, p1, Lsoftware/simplicial/a/ay;->L:B

    iput-byte v0, p0, Lsoftware/simplicial/a/ay;->L:B

    .line 183
    iget-boolean v0, p1, Lsoftware/simplicial/a/ay;->R:Z

    iput-boolean v0, p0, Lsoftware/simplicial/a/ay;->R:Z

    .line 184
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p1, Lsoftware/simplicial/a/ay;->S:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lsoftware/simplicial/a/ay;->S:Ljava/util/Set;

    .line 185
    return-void
.end method

.method public constructor <init>(Lsoftware/simplicial/a/f/bm;)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-wide v2, p0, Lsoftware/simplicial/a/ay;->g:J

    .line 44
    iput-wide v2, p0, Lsoftware/simplicial/a/ay;->B:J

    .line 45
    iput-wide v2, p0, Lsoftware/simplicial/a/ay;->C:J

    .line 55
    iput-boolean v0, p0, Lsoftware/simplicial/a/ay;->M:Z

    .line 56
    iput-boolean v0, p0, Lsoftware/simplicial/a/ay;->N:Z

    .line 57
    iput-boolean v0, p0, Lsoftware/simplicial/a/ay;->O:Z

    .line 58
    iput-boolean v0, p0, Lsoftware/simplicial/a/ay;->P:Z

    .line 59
    iput-boolean v0, p0, Lsoftware/simplicial/a/ay;->Q:Z

    .line 62
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lsoftware/simplicial/a/ay;->S:Ljava/util/Set;

    .line 65
    iput-wide v2, p0, Lsoftware/simplicial/a/ay;->T:J

    .line 66
    iput v0, p0, Lsoftware/simplicial/a/ay;->U:I

    .line 67
    iput-wide v2, p0, Lsoftware/simplicial/a/ay;->V:J

    .line 68
    iput-wide v2, p0, Lsoftware/simplicial/a/ay;->W:J

    .line 69
    iput v0, p0, Lsoftware/simplicial/a/ay;->X:I

    .line 70
    iput v0, p0, Lsoftware/simplicial/a/ay;->Y:I

    .line 84
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/ay;->a:I

    .line 85
    sget-object v1, Lsoftware/simplicial/a/am;->s:[Lsoftware/simplicial/a/am;

    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lsoftware/simplicial/a/ay;->b:Lsoftware/simplicial/a/am;

    .line 86
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v1

    iput-boolean v1, p0, Lsoftware/simplicial/a/ay;->c:Z

    .line 87
    sget-object v1, Lsoftware/simplicial/a/aq;->c:[Lsoftware/simplicial/a/aq;

    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lsoftware/simplicial/a/ay;->d:Lsoftware/simplicial/a/aq;

    .line 88
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lsoftware/simplicial/a/ay;->e:Ljava/lang/String;

    .line 89
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    new-array v1, v1, [B

    iput-object v1, p0, Lsoftware/simplicial/a/ay;->f:[B

    .line 90
    iget-object v1, p0, Lsoftware/simplicial/a/ay;->f:[B

    iget-object v2, p0, Lsoftware/simplicial/a/ay;->f:[B

    array-length v2, v2

    invoke-virtual {p1, v1, v0, v2}, Lsoftware/simplicial/a/f/bm;->readFully([BII)V

    .line 92
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lsoftware/simplicial/a/ay;->g:J

    .line 93
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/ay;->h:I

    .line 94
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/ay;->i:I

    .line 95
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/ay;->j:I

    .line 96
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/ay;->k:I

    .line 97
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/ay;->l:I

    .line 98
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/ay;->m:I

    .line 99
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/ay;->n:I

    .line 100
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/ay;->o:I

    .line 101
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/ay;->p:I

    .line 102
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/ay;->q:I

    .line 103
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/ay;->r:I

    .line 104
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/ay;->s:I

    .line 105
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/ay;->t:I

    .line 106
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/ay;->u:I

    .line 107
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/ay;->v:I

    .line 108
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/ay;->w:I

    .line 109
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/ay;->x:I

    .line 110
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/ay;->y:I

    .line 111
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/ay;->z:I

    .line 112
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/ay;->A:I

    .line 113
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lsoftware/simplicial/a/ay;->B:J

    .line 114
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lsoftware/simplicial/a/ay;->C:J

    .line 115
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/ay;->D:I

    .line 116
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/ay;->E:I

    .line 117
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/ay;->F:I

    .line 118
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/ay;->G:I

    .line 119
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/ay;->H:I

    .line 120
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/ay;->I:I

    .line 121
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/ay;->J:I

    .line 122
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/ay;->K:I

    .line 123
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v1

    iput-boolean v1, p0, Lsoftware/simplicial/a/ay;->M:Z

    .line 124
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v1

    iput-boolean v1, p0, Lsoftware/simplicial/a/ay;->N:Z

    .line 125
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v1

    iput-boolean v1, p0, Lsoftware/simplicial/a/ay;->O:Z

    .line 126
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v1

    iput-boolean v1, p0, Lsoftware/simplicial/a/ay;->P:Z

    .line 127
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v1

    iput-boolean v1, p0, Lsoftware/simplicial/a/ay;->Q:Z

    .line 128
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    iput-byte v1, p0, Lsoftware/simplicial/a/ay;->L:B

    .line 130
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v1

    iput-boolean v1, p0, Lsoftware/simplicial/a/ay;->R:Z

    .line 131
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v1

    .line 132
    :goto_0
    if-ge v0, v1, :cond_0

    .line 133
    iget-object v2, p0, Lsoftware/simplicial/a/ay;->S:Ljava/util/Set;

    sget-object v3, Lsoftware/simplicial/a/d;->bv:Ljava/util/Map;

    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v4

    invoke-static {v4}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 132
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 134
    :cond_0
    return-void
.end method


# virtual methods
.method public a(ILsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;Ljava/lang/String;[BZ)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 189
    iput p1, p0, Lsoftware/simplicial/a/ay;->a:I

    .line 190
    iput-object p2, p0, Lsoftware/simplicial/a/ay;->b:Lsoftware/simplicial/a/am;

    .line 191
    iput-boolean p6, p0, Lsoftware/simplicial/a/ay;->c:Z

    .line 192
    iput-object p3, p0, Lsoftware/simplicial/a/ay;->d:Lsoftware/simplicial/a/aq;

    .line 193
    iput-object p4, p0, Lsoftware/simplicial/a/ay;->e:Ljava/lang/String;

    .line 194
    array-length v0, p5

    invoke-static {p5, v0}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/a/ay;->f:[B

    .line 196
    iput-wide v2, p0, Lsoftware/simplicial/a/ay;->g:J

    .line 197
    iput v1, p0, Lsoftware/simplicial/a/ay;->h:I

    .line 198
    iput v1, p0, Lsoftware/simplicial/a/ay;->i:I

    .line 199
    iput v1, p0, Lsoftware/simplicial/a/ay;->j:I

    .line 200
    iput v1, p0, Lsoftware/simplicial/a/ay;->k:I

    .line 201
    iput v1, p0, Lsoftware/simplicial/a/ay;->l:I

    .line 202
    iput v1, p0, Lsoftware/simplicial/a/ay;->m:I

    .line 203
    iput v1, p0, Lsoftware/simplicial/a/ay;->n:I

    .line 204
    iput v1, p0, Lsoftware/simplicial/a/ay;->o:I

    .line 205
    iput v1, p0, Lsoftware/simplicial/a/ay;->p:I

    .line 206
    iput v1, p0, Lsoftware/simplicial/a/ay;->q:I

    .line 207
    iput v1, p0, Lsoftware/simplicial/a/ay;->r:I

    .line 208
    iput v1, p0, Lsoftware/simplicial/a/ay;->s:I

    .line 209
    iput v1, p0, Lsoftware/simplicial/a/ay;->t:I

    .line 210
    iput v1, p0, Lsoftware/simplicial/a/ay;->u:I

    .line 211
    iput v1, p0, Lsoftware/simplicial/a/ay;->v:I

    .line 212
    iput v1, p0, Lsoftware/simplicial/a/ay;->w:I

    .line 213
    iput v1, p0, Lsoftware/simplicial/a/ay;->x:I

    .line 214
    iput v1, p0, Lsoftware/simplicial/a/ay;->y:I

    .line 215
    iput v1, p0, Lsoftware/simplicial/a/ay;->z:I

    .line 216
    iput v1, p0, Lsoftware/simplicial/a/ay;->A:I

    .line 217
    iput-wide v2, p0, Lsoftware/simplicial/a/ay;->B:J

    .line 218
    iput-wide v2, p0, Lsoftware/simplicial/a/ay;->C:J

    .line 219
    iput v1, p0, Lsoftware/simplicial/a/ay;->D:I

    .line 220
    iput v1, p0, Lsoftware/simplicial/a/ay;->E:I

    .line 221
    iput v1, p0, Lsoftware/simplicial/a/ay;->F:I

    .line 222
    iput v1, p0, Lsoftware/simplicial/a/ay;->G:I

    .line 223
    iput v1, p0, Lsoftware/simplicial/a/ay;->H:I

    .line 224
    iput v1, p0, Lsoftware/simplicial/a/ay;->I:I

    .line 225
    iput v1, p0, Lsoftware/simplicial/a/ay;->J:I

    .line 226
    iput v1, p0, Lsoftware/simplicial/a/ay;->K:I

    .line 227
    iput-boolean v1, p0, Lsoftware/simplicial/a/ay;->M:Z

    .line 228
    iput-boolean v1, p0, Lsoftware/simplicial/a/ay;->N:Z

    .line 229
    iput-boolean v1, p0, Lsoftware/simplicial/a/ay;->O:Z

    .line 230
    iput-boolean v1, p0, Lsoftware/simplicial/a/ay;->P:Z

    .line 231
    iput-boolean v1, p0, Lsoftware/simplicial/a/ay;->Q:Z

    .line 232
    sget-object v0, Lsoftware/simplicial/a/bd;->a:Lsoftware/simplicial/a/bd;

    iget-byte v0, v0, Lsoftware/simplicial/a/bd;->c:B

    iput-byte v0, p0, Lsoftware/simplicial/a/ay;->L:B

    .line 234
    iput-boolean v1, p0, Lsoftware/simplicial/a/ay;->R:Z

    .line 235
    iget-object v0, p0, Lsoftware/simplicial/a/ay;->S:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 238
    iput-wide v2, p0, Lsoftware/simplicial/a/ay;->T:J

    .line 239
    iput v1, p0, Lsoftware/simplicial/a/ay;->U:I

    .line 240
    iput-wide v2, p0, Lsoftware/simplicial/a/ay;->V:J

    .line 241
    iput-wide v2, p0, Lsoftware/simplicial/a/ay;->W:J

    .line 242
    iput v1, p0, Lsoftware/simplicial/a/ay;->X:I

    .line 243
    iput v1, p0, Lsoftware/simplicial/a/ay;->Y:I

    .line 244
    return-void
.end method

.method public a(Lsoftware/simplicial/a/f/bn;)V
    .locals 3

    .prologue
    .line 248
    iget v0, p0, Lsoftware/simplicial/a/ay;->a:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 249
    iget-object v0, p0, Lsoftware/simplicial/a/ay;->b:Lsoftware/simplicial/a/am;

    invoke-virtual {v0}, Lsoftware/simplicial/a/am;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 250
    iget-boolean v0, p0, Lsoftware/simplicial/a/ay;->c:Z

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 251
    iget-object v0, p0, Lsoftware/simplicial/a/ay;->d:Lsoftware/simplicial/a/aq;

    invoke-virtual {v0}, Lsoftware/simplicial/a/aq;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 252
    iget-object v0, p0, Lsoftware/simplicial/a/ay;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 253
    iget-object v0, p0, Lsoftware/simplicial/a/ay;->f:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 254
    iget-object v0, p0, Lsoftware/simplicial/a/ay;->f:[B

    const/4 v1, 0x0

    iget-object v2, p0, Lsoftware/simplicial/a/ay;->f:[B

    array-length v2, v2

    invoke-virtual {p1, v0, v1, v2}, Lsoftware/simplicial/a/f/bn;->write([BII)V

    .line 256
    iget-wide v0, p0, Lsoftware/simplicial/a/ay;->g:J

    invoke-virtual {p1, v0, v1}, Lsoftware/simplicial/a/f/bn;->writeLong(J)V

    .line 257
    iget v0, p0, Lsoftware/simplicial/a/ay;->h:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 258
    iget v0, p0, Lsoftware/simplicial/a/ay;->i:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 259
    iget v0, p0, Lsoftware/simplicial/a/ay;->j:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 260
    iget v0, p0, Lsoftware/simplicial/a/ay;->k:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 261
    iget v0, p0, Lsoftware/simplicial/a/ay;->l:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 262
    iget v0, p0, Lsoftware/simplicial/a/ay;->m:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 263
    iget v0, p0, Lsoftware/simplicial/a/ay;->n:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 264
    iget v0, p0, Lsoftware/simplicial/a/ay;->o:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 265
    iget v0, p0, Lsoftware/simplicial/a/ay;->p:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 266
    iget v0, p0, Lsoftware/simplicial/a/ay;->q:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 267
    iget v0, p0, Lsoftware/simplicial/a/ay;->r:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 268
    iget v0, p0, Lsoftware/simplicial/a/ay;->s:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 269
    iget v0, p0, Lsoftware/simplicial/a/ay;->t:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 270
    iget v0, p0, Lsoftware/simplicial/a/ay;->u:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 271
    iget v0, p0, Lsoftware/simplicial/a/ay;->v:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 272
    iget v0, p0, Lsoftware/simplicial/a/ay;->w:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 273
    iget v0, p0, Lsoftware/simplicial/a/ay;->x:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 274
    iget v0, p0, Lsoftware/simplicial/a/ay;->y:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 275
    iget v0, p0, Lsoftware/simplicial/a/ay;->z:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 276
    iget v0, p0, Lsoftware/simplicial/a/ay;->A:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 277
    iget-wide v0, p0, Lsoftware/simplicial/a/ay;->B:J

    invoke-virtual {p1, v0, v1}, Lsoftware/simplicial/a/f/bn;->writeLong(J)V

    .line 278
    iget-wide v0, p0, Lsoftware/simplicial/a/ay;->C:J

    invoke-virtual {p1, v0, v1}, Lsoftware/simplicial/a/f/bn;->writeLong(J)V

    .line 279
    iget v0, p0, Lsoftware/simplicial/a/ay;->D:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 280
    iget v0, p0, Lsoftware/simplicial/a/ay;->E:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 281
    iget v0, p0, Lsoftware/simplicial/a/ay;->F:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 282
    iget v0, p0, Lsoftware/simplicial/a/ay;->G:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 283
    iget v0, p0, Lsoftware/simplicial/a/ay;->H:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 284
    iget v0, p0, Lsoftware/simplicial/a/ay;->I:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 285
    iget v0, p0, Lsoftware/simplicial/a/ay;->J:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 286
    iget v0, p0, Lsoftware/simplicial/a/ay;->K:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 287
    iget-boolean v0, p0, Lsoftware/simplicial/a/ay;->M:Z

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 288
    iget-boolean v0, p0, Lsoftware/simplicial/a/ay;->N:Z

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 289
    iget-boolean v0, p0, Lsoftware/simplicial/a/ay;->O:Z

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 290
    iget-boolean v0, p0, Lsoftware/simplicial/a/ay;->P:Z

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 291
    iget-boolean v0, p0, Lsoftware/simplicial/a/ay;->Q:Z

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 292
    iget-byte v0, p0, Lsoftware/simplicial/a/ay;->L:B

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 294
    iget-boolean v0, p0, Lsoftware/simplicial/a/ay;->R:Z

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 295
    iget-object v0, p0, Lsoftware/simplicial/a/ay;->S:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    .line 296
    iget-object v0, p0, Lsoftware/simplicial/a/ay;->S:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/d;

    .line 297
    iget-short v0, v0, Lsoftware/simplicial/a/d;->bw:S

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    goto :goto_0

    .line 298
    :cond_0
    return-void
.end method
