.class public final enum Lsoftware/simplicial/a/b/b;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/b/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/b/b;

.field public static final enum b:Lsoftware/simplicial/a/b/b;

.field public static final enum c:Lsoftware/simplicial/a/b/b;

.field public static final enum d:Lsoftware/simplicial/a/b/b;

.field public static final enum e:Lsoftware/simplicial/a/b/b;

.field public static final f:[Lsoftware/simplicial/a/b/b;

.field private static final synthetic g:[Lsoftware/simplicial/a/b/b;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 8
    new-instance v0, Lsoftware/simplicial/a/b/b;

    const-string v1, "INVALID"

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/b/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/b/b;->a:Lsoftware/simplicial/a/b/b;

    new-instance v0, Lsoftware/simplicial/a/b/b;

    const-string v1, "SEARCHING"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/b/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/b/b;->b:Lsoftware/simplicial/a/b/b;

    new-instance v0, Lsoftware/simplicial/a/b/b;

    const-string v1, "COMPETEING"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/b/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/b/b;->c:Lsoftware/simplicial/a/b/b;

    new-instance v0, Lsoftware/simplicial/a/b/b;

    const-string v1, "DONE"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/a/b/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/b/b;->d:Lsoftware/simplicial/a/b/b;

    new-instance v0, Lsoftware/simplicial/a/b/b;

    const-string v1, "VALIDATING"

    invoke-direct {v0, v1, v6}, Lsoftware/simplicial/a/b/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/b/b;->e:Lsoftware/simplicial/a/b/b;

    .line 6
    const/4 v0, 0x5

    new-array v0, v0, [Lsoftware/simplicial/a/b/b;

    sget-object v1, Lsoftware/simplicial/a/b/b;->a:Lsoftware/simplicial/a/b/b;

    aput-object v1, v0, v2

    sget-object v1, Lsoftware/simplicial/a/b/b;->b:Lsoftware/simplicial/a/b/b;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/b/b;->c:Lsoftware/simplicial/a/b/b;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/a/b/b;->d:Lsoftware/simplicial/a/b/b;

    aput-object v1, v0, v5

    sget-object v1, Lsoftware/simplicial/a/b/b;->e:Lsoftware/simplicial/a/b/b;

    aput-object v1, v0, v6

    sput-object v0, Lsoftware/simplicial/a/b/b;->g:[Lsoftware/simplicial/a/b/b;

    .line 9
    invoke-static {}, Lsoftware/simplicial/a/b/b;->values()[Lsoftware/simplicial/a/b/b;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/a/b/b;->f:[Lsoftware/simplicial/a/b/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/b/b;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lsoftware/simplicial/a/b/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/b/b;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/b/b;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lsoftware/simplicial/a/b/b;->g:[Lsoftware/simplicial/a/b/b;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/b/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/b/b;

    return-object v0
.end method
