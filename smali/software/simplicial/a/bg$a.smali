.class public final enum Lsoftware/simplicial/a/bg$a;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsoftware/simplicial/a/bg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/bg$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/bg$a;

.field public static final enum b:Lsoftware/simplicial/a/bg$a;

.field public static final enum c:Lsoftware/simplicial/a/bg$a;

.field public static final enum d:Lsoftware/simplicial/a/bg$a;

.field private static final synthetic e:[Lsoftware/simplicial/a/bg$a;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 45
    new-instance v0, Lsoftware/simplicial/a/bg$a;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bg$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$a;->a:Lsoftware/simplicial/a/bg$a;

    new-instance v0, Lsoftware/simplicial/a/bg$a;

    const-string v1, "REQUESTED"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/bg$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$a;->b:Lsoftware/simplicial/a/bg$a;

    new-instance v0, Lsoftware/simplicial/a/bg$a;

    const-string v1, "PENDING"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/bg$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$a;->c:Lsoftware/simplicial/a/bg$a;

    new-instance v0, Lsoftware/simplicial/a/bg$a;

    const-string v1, "MUTUAL"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/a/bg$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$a;->d:Lsoftware/simplicial/a/bg$a;

    .line 43
    const/4 v0, 0x4

    new-array v0, v0, [Lsoftware/simplicial/a/bg$a;

    sget-object v1, Lsoftware/simplicial/a/bg$a;->a:Lsoftware/simplicial/a/bg$a;

    aput-object v1, v0, v2

    sget-object v1, Lsoftware/simplicial/a/bg$a;->b:Lsoftware/simplicial/a/bg$a;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/bg$a;->c:Lsoftware/simplicial/a/bg$a;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/a/bg$a;->d:Lsoftware/simplicial/a/bg$a;

    aput-object v1, v0, v5

    sput-object v0, Lsoftware/simplicial/a/bg$a;->e:[Lsoftware/simplicial/a/bg$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/bg$a;
    .locals 1

    .prologue
    .line 43
    const-class v0, Lsoftware/simplicial/a/bg$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bg$a;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/bg$a;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lsoftware/simplicial/a/bg$a;->e:[Lsoftware/simplicial/a/bg$a;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/bg$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/bg$a;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 49
    sget-object v1, Lsoftware/simplicial/a/bg$1;->a:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/bg$a;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 60
    :goto_0
    :pswitch_0
    return v0

    .line 52
    :pswitch_1
    const/4 v0, 0x3

    goto :goto_0

    .line 54
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 56
    :pswitch_3
    const/4 v0, 0x1

    goto :goto_0

    .line 49
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method
