.class public final enum Lsoftware/simplicial/a/bl$a;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsoftware/simplicial/a/bl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/bl$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/bl$a;

.field public static final enum b:Lsoftware/simplicial/a/bl$a;

.field public static final enum c:Lsoftware/simplicial/a/bl$a;

.field public static final d:[Lsoftware/simplicial/a/bl$a;

.field private static final synthetic e:[Lsoftware/simplicial/a/bl$a;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 140
    new-instance v0, Lsoftware/simplicial/a/bl$a;

    const-string v1, "DEAD"

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bl$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bl$a;->a:Lsoftware/simplicial/a/bl$a;

    new-instance v0, Lsoftware/simplicial/a/bl$a;

    const-string v1, "INACTIVE"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/bl$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bl$a;->b:Lsoftware/simplicial/a/bl$a;

    new-instance v0, Lsoftware/simplicial/a/bl$a;

    const-string v1, "ACTIVE"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/bl$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bl$a;->c:Lsoftware/simplicial/a/bl$a;

    .line 138
    const/4 v0, 0x3

    new-array v0, v0, [Lsoftware/simplicial/a/bl$a;

    sget-object v1, Lsoftware/simplicial/a/bl$a;->a:Lsoftware/simplicial/a/bl$a;

    aput-object v1, v0, v2

    sget-object v1, Lsoftware/simplicial/a/bl$a;->b:Lsoftware/simplicial/a/bl$a;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/bl$a;->c:Lsoftware/simplicial/a/bl$a;

    aput-object v1, v0, v4

    sput-object v0, Lsoftware/simplicial/a/bl$a;->e:[Lsoftware/simplicial/a/bl$a;

    .line 142
    invoke-static {}, Lsoftware/simplicial/a/bl$a;->values()[Lsoftware/simplicial/a/bl$a;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/a/bl$a;->d:[Lsoftware/simplicial/a/bl$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 138
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/bl$a;
    .locals 1

    .prologue
    .line 138
    const-class v0, Lsoftware/simplicial/a/bl$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bl$a;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/bl$a;
    .locals 1

    .prologue
    .line 138
    sget-object v0, Lsoftware/simplicial/a/bl$a;->e:[Lsoftware/simplicial/a/bl$a;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/bl$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/bl$a;

    return-object v0
.end method
