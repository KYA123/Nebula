.class public Lsoftware/simplicial/a/bq;
.super Lsoftware/simplicial/a/h;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/a/bq$a;
    }
.end annotation


# static fields
.field public static final e:F

.field public static final f:F

.field public static final g:F


# instance fields
.field public A:I

.field public B:I

.field public C:I

.field public D:Z

.field public E:Z

.field public F:Z

.field public G:Z

.field public H:F

.field public I:I

.field public J:I

.field public K:Lsoftware/simplicial/a/af;

.field public L:Lsoftware/simplicial/a/as;

.field public M:Lsoftware/simplicial/a/bd;

.field public N:Lsoftware/simplicial/a/s;

.field public h:F

.field public i:F

.field public j:F

.field public k:F

.field public z:Lsoftware/simplicial/a/bq$a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 8
    const-wide/high16 v0, 0x4014000000000000L    # 5.0

    invoke-static {v0, v1}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lsoftware/simplicial/a/bq;->e:F

    .line 9
    sget v0, Lsoftware/simplicial/a/bh;->e:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lsoftware/simplicial/a/bq;->f:F

    .line 11
    sget v0, Lsoftware/simplicial/a/bh;->h:F

    sput v0, Lsoftware/simplicial/a/bq;->g:F

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Lsoftware/simplicial/a/h;-><init>()V

    .line 17
    iput v0, p0, Lsoftware/simplicial/a/bq;->j:F

    .line 19
    iput v0, p0, Lsoftware/simplicial/a/bq;->k:F

    .line 21
    sget-object v0, Lsoftware/simplicial/a/bq$a;->a:Lsoftware/simplicial/a/bq$a;

    iput-object v0, p0, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    .line 44
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 88
    iget v0, p0, Lsoftware/simplicial/a/bq;->s:F

    iget v1, p0, Lsoftware/simplicial/a/bq;->l:F

    iget v2, p0, Lsoftware/simplicial/a/bq;->s:F

    sub-float/2addr v1, v2

    div-float/2addr v1, v3

    add-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/bq;->s:F

    .line 89
    iget v0, p0, Lsoftware/simplicial/a/bq;->t:F

    iget v1, p0, Lsoftware/simplicial/a/bq;->m:F

    iget v2, p0, Lsoftware/simplicial/a/bq;->t:F

    sub-float/2addr v1, v2

    div-float/2addr v1, v3

    add-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/bq;->t:F

    .line 90
    iget v0, p0, Lsoftware/simplicial/a/bq;->n:F

    iput v0, p0, Lsoftware/simplicial/a/bq;->w:F

    .line 91
    return-void
.end method

.method public a(IFFFFFF)V
    .locals 10

    .prologue
    .line 53
    sget v6, Lsoftware/simplicial/a/bq;->e:F

    sget v7, Lsoftware/simplicial/a/bq;->e:F

    sget v8, Lsoftware/simplicial/a/bq;->f:F

    const v9, 0x48f42400    # 500000.0f

    move-object v0, p0

    move v1, p1

    move v2, p4

    move v3, p5

    move/from16 v4, p6

    move/from16 v5, p7

    invoke-super/range {v0 .. v9}, Lsoftware/simplicial/a/h;->a(IFFFFFFFF)V

    .line 55
    iput p2, p0, Lsoftware/simplicial/a/bq;->h:F

    .line 56
    iput p3, p0, Lsoftware/simplicial/a/bq;->i:F

    .line 57
    iput p2, p0, Lsoftware/simplicial/a/bq;->s:F

    .line 58
    iput p3, p0, Lsoftware/simplicial/a/bq;->t:F

    .line 60
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/a/bq;->j:F

    .line 62
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/a/bq;->k:F

    .line 64
    sget-object v0, Lsoftware/simplicial/a/bq$a;->a:Lsoftware/simplicial/a/bq$a;

    iput-object v0, p0, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    .line 66
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/a/bq;->A:I

    .line 67
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/a/bq;->B:I

    .line 68
    const/4 v0, -0x1

    iput v0, p0, Lsoftware/simplicial/a/bq;->C:I

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/a/bq;->D:Z

    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/a/bq;->F:Z

    .line 74
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/a/bq;->H:F

    .line 76
    sget-object v0, Lsoftware/simplicial/a/af;->a:Lsoftware/simplicial/a/af;

    iput-object v0, p0, Lsoftware/simplicial/a/bq;->K:Lsoftware/simplicial/a/af;

    .line 77
    sget-object v0, Lsoftware/simplicial/a/bd;->a:Lsoftware/simplicial/a/bd;

    iput-object v0, p0, Lsoftware/simplicial/a/bq;->M:Lsoftware/simplicial/a/bd;

    .line 78
    sget-object v0, Lsoftware/simplicial/a/as;->a:Lsoftware/simplicial/a/as;

    iput-object v0, p0, Lsoftware/simplicial/a/bq;->L:Lsoftware/simplicial/a/as;

    .line 79
    sget-object v0, Lsoftware/simplicial/a/s;->a:Lsoftware/simplicial/a/s;

    iput-object v0, p0, Lsoftware/simplicial/a/bq;->N:Lsoftware/simplicial/a/s;

    .line 80
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/a/bq;->I:I

    .line 81
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/a/bq;->J:I

    .line 82
    return-void
.end method
