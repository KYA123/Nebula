.class public Lsoftware/simplicial/a/bt;
.super Lsoftware/simplicial/a/ao;
.source "SourceFile"


# instance fields
.field public a:B

.field public b:Lsoftware/simplicial/a/bv;

.field public c:I

.field public d:Lsoftware/simplicial/a/bv;

.field public e:I

.field public f:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 29
    invoke-direct {p0}, Lsoftware/simplicial/a/ao;-><init>()V

    .line 18
    iput-byte v1, p0, Lsoftware/simplicial/a/bt;->a:B

    .line 20
    sget-object v0, Lsoftware/simplicial/a/bv;->e:Lsoftware/simplicial/a/bv;

    iput-object v0, p0, Lsoftware/simplicial/a/bt;->b:Lsoftware/simplicial/a/bv;

    .line 21
    iput v1, p0, Lsoftware/simplicial/a/bt;->c:I

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/a/bt;->f:F

    .line 31
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lsoftware/simplicial/a/bt;->l:F

    iput v0, p0, Lsoftware/simplicial/a/bt;->s:F

    .line 37
    iget v0, p0, Lsoftware/simplicial/a/bt;->m:F

    iput v0, p0, Lsoftware/simplicial/a/bt;->t:F

    .line 38
    iget v0, p0, Lsoftware/simplicial/a/bt;->n:F

    iput v0, p0, Lsoftware/simplicial/a/bt;->w:F

    .line 39
    return-void
.end method

.method public a(F)V
    .locals 2

    .prologue
    const v1, 0x3f19999a    # 0.6f

    .line 43
    iget v0, p0, Lsoftware/simplicial/a/bt;->f:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 45
    iget v0, p0, Lsoftware/simplicial/a/bt;->f:F

    add-float/2addr v0, p1

    iput v0, p0, Lsoftware/simplicial/a/bt;->f:F

    .line 46
    iget v0, p0, Lsoftware/simplicial/a/bt;->f:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 47
    iput v1, p0, Lsoftware/simplicial/a/bt;->f:F

    .line 49
    :cond_0
    return-void
.end method

.method public a(Lsoftware/simplicial/a/bv;FFBLjava/util/Random;)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 53
    const/high16 v0, 0x40600000    # 3.5f

    invoke-super {p0, p2, p3, v0}, Lsoftware/simplicial/a/ao;->a(FFF)V

    .line 55
    iput-object p1, p0, Lsoftware/simplicial/a/bt;->b:Lsoftware/simplicial/a/bv;

    .line 56
    iput-byte p4, p0, Lsoftware/simplicial/a/bt;->a:B

    .line 57
    sget-object v0, Lsoftware/simplicial/a/bt$1;->a:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/bv;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 75
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/a/bt;->c:I

    .line 78
    :goto_0
    return-void

    .line 60
    :pswitch_0
    invoke-virtual {p5, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/a/bt;->c:I

    goto :goto_0

    .line 63
    :pswitch_1
    invoke-virtual {p5, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/a/bt;->c:I

    goto :goto_0

    .line 66
    :pswitch_2
    invoke-virtual {p5, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/a/bt;->c:I

    goto :goto_0

    .line 69
    :pswitch_3
    const/4 v0, 0x6

    invoke-virtual {p5, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/a/bt;->c:I

    goto :goto_0

    .line 72
    :pswitch_4
    invoke-virtual {p5, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/a/bt;->c:I

    goto :goto_0

    .line 57
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
