.class public Lsoftware/simplicial/a/bx;
.super Lsoftware/simplicial/a/ao;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lsoftware/simplicial/a/ao;",
        "Ljava/lang/Comparable",
        "<",
        "Lsoftware/simplicial/a/bx;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:F


# instance fields
.field public b:Ljava/lang/String;

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 8
    const-wide/high16 v0, 0x403e000000000000L    # 30.0

    invoke-static {v0, v1}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lsoftware/simplicial/a/bx;->a:F

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lsoftware/simplicial/a/ao;-><init>()V

    .line 28
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IFLsoftware/simplicial/a/am;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lsoftware/simplicial/a/ao;-><init>()V

    .line 22
    invoke-virtual {p0, p1, p2, p3, p4}, Lsoftware/simplicial/a/bx;->a(Ljava/lang/String;IFLsoftware/simplicial/a/am;)V

    .line 23
    return-void
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/bx;)I
    .locals 2

    .prologue
    .line 129
    iget v0, p1, Lsoftware/simplicial/a/bx;->d:I

    iget v1, p0, Lsoftware/simplicial/a/bx;->d:I

    if-eq v0, v1, :cond_0

    .line 130
    iget v0, p1, Lsoftware/simplicial/a/bx;->d:I

    iget v1, p0, Lsoftware/simplicial/a/bx;->d:I

    sub-int/2addr v0, v1

    .line 131
    :goto_0
    return v0

    :cond_0
    iget v0, p1, Lsoftware/simplicial/a/bx;->e:I

    iget v1, p0, Lsoftware/simplicial/a/bx;->e:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 137
    iget v0, p0, Lsoftware/simplicial/a/bx;->l:F

    iput v0, p0, Lsoftware/simplicial/a/bx;->s:F

    .line 138
    iget v0, p0, Lsoftware/simplicial/a/bx;->m:F

    iput v0, p0, Lsoftware/simplicial/a/bx;->t:F

    .line 139
    iget v0, p0, Lsoftware/simplicial/a/bx;->n:F

    iput v0, p0, Lsoftware/simplicial/a/bx;->w:F

    .line 140
    return-void
.end method

.method public a(Ljava/lang/String;IFLsoftware/simplicial/a/am;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x0

    const v3, 0x3f666666    # 0.9f

    const v2, 0x3dcccccd    # 0.1f

    const/high16 v1, 0x3f000000    # 0.5f

    .line 32
    iput-object p1, p0, Lsoftware/simplicial/a/bx;->b:Ljava/lang/String;

    .line 33
    iput p2, p0, Lsoftware/simplicial/a/bx;->c:I

    .line 35
    iput v0, p0, Lsoftware/simplicial/a/bx;->e:I

    .line 36
    iput v0, p0, Lsoftware/simplicial/a/bx;->d:I

    .line 37
    iput v0, p0, Lsoftware/simplicial/a/bx;->f:I

    .line 39
    packed-switch p2, :pswitch_data_0

    .line 123
    :goto_0
    iget v0, p0, Lsoftware/simplicial/a/bx;->l:F

    iget v1, p0, Lsoftware/simplicial/a/bx;->m:F

    sget v2, Lsoftware/simplicial/a/bx;->a:F

    invoke-super {p0, v0, v1, v2}, Lsoftware/simplicial/a/ao;->a(FFF)V

    .line 124
    return-void

    .line 42
    :pswitch_0
    const v0, -0x333334

    iput v0, p0, Lsoftware/simplicial/a/bx;->g:I

    .line 43
    const/4 v0, -0x1

    iput v0, p0, Lsoftware/simplicial/a/bx;->h:I

    .line 44
    sget-object v0, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-ne p4, v0, :cond_0

    .line 46
    iput p3, p0, Lsoftware/simplicial/a/bx;->l:F

    .line 47
    iput v4, p0, Lsoftware/simplicial/a/bx;->m:F

    goto :goto_0

    .line 51
    :cond_0
    mul-float v0, p3, v1

    iput v0, p0, Lsoftware/simplicial/a/bx;->l:F

    .line 52
    mul-float v0, p3, v1

    iput v0, p0, Lsoftware/simplicial/a/bx;->m:F

    goto :goto_0

    .line 56
    :pswitch_1
    const v0, -0xffff1f

    iput v0, p0, Lsoftware/simplicial/a/bx;->g:I

    .line 57
    const v0, -0xffff01

    iput v0, p0, Lsoftware/simplicial/a/bx;->h:I

    .line 58
    sget-object v0, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    if-ne p4, v0, :cond_1

    .line 60
    mul-float v0, p3, v4

    iput v0, p0, Lsoftware/simplicial/a/bx;->l:F

    .line 61
    mul-float v0, p3, v1

    iput v0, p0, Lsoftware/simplicial/a/bx;->m:F

    goto :goto_0

    .line 63
    :cond_1
    sget-object v0, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-ne p4, v0, :cond_2

    .line 65
    const/high16 v0, 0x3e800000    # 0.25f

    mul-float/2addr v0, p3

    iput v0, p0, Lsoftware/simplicial/a/bx;->l:F

    .line 66
    mul-float v0, p3, v1

    iput v0, p0, Lsoftware/simplicial/a/bx;->m:F

    goto :goto_0

    .line 70
    :cond_2
    mul-float v0, p3, v2

    iput v0, p0, Lsoftware/simplicial/a/bx;->l:F

    .line 71
    mul-float v0, p3, v2

    iput v0, p0, Lsoftware/simplicial/a/bx;->m:F

    goto :goto_0

    .line 75
    :pswitch_2
    const v0, -0xff1f00

    iput v0, p0, Lsoftware/simplicial/a/bx;->g:I

    .line 76
    const v0, -0xff0100

    iput v0, p0, Lsoftware/simplicial/a/bx;->h:I

    .line 77
    sget-object v0, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    if-ne p4, v0, :cond_3

    .line 79
    const/high16 v0, 0x3f800000    # 1.0f

    mul-float/2addr v0, p3

    iput v0, p0, Lsoftware/simplicial/a/bx;->l:F

    .line 80
    mul-float v0, p3, v1

    iput v0, p0, Lsoftware/simplicial/a/bx;->m:F

    goto :goto_0

    .line 82
    :cond_3
    sget-object v0, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-ne p4, v0, :cond_4

    .line 84
    const/high16 v0, 0x3f400000    # 0.75f

    mul-float/2addr v0, p3

    iput v0, p0, Lsoftware/simplicial/a/bx;->l:F

    .line 85
    mul-float v0, p3, v1

    iput v0, p0, Lsoftware/simplicial/a/bx;->m:F

    goto :goto_0

    .line 89
    :cond_4
    mul-float v0, p3, v3

    iput v0, p0, Lsoftware/simplicial/a/bx;->l:F

    .line 90
    mul-float v0, p3, v3

    iput v0, p0, Lsoftware/simplicial/a/bx;->m:F

    goto :goto_0

    .line 94
    :pswitch_3
    const/high16 v0, -0x4f0000

    iput v0, p0, Lsoftware/simplicial/a/bx;->g:I

    .line 95
    const/high16 v0, -0x310000

    iput v0, p0, Lsoftware/simplicial/a/bx;->h:I

    .line 96
    sget-object v0, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-ne p4, v0, :cond_5

    .line 98
    mul-float v0, p3, v1

    iput v0, p0, Lsoftware/simplicial/a/bx;->l:F

    .line 99
    const/high16 v0, 0x3e800000    # 0.25f

    mul-float/2addr v0, p3

    iput v0, p0, Lsoftware/simplicial/a/bx;->m:F

    goto/16 :goto_0

    .line 103
    :cond_5
    mul-float v0, p3, v3

    iput v0, p0, Lsoftware/simplicial/a/bx;->l:F

    .line 104
    mul-float v0, p3, v2

    iput v0, p0, Lsoftware/simplicial/a/bx;->m:F

    goto/16 :goto_0

    .line 108
    :pswitch_4
    const v0, -0xff1e1f

    iput v0, p0, Lsoftware/simplicial/a/bx;->g:I

    .line 109
    const v0, -0xff0001

    iput v0, p0, Lsoftware/simplicial/a/bx;->h:I

    .line 110
    sget-object v0, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-ne p4, v0, :cond_6

    .line 112
    mul-float v0, p3, v1

    iput v0, p0, Lsoftware/simplicial/a/bx;->l:F

    .line 113
    const/high16 v0, 0x3f400000    # 0.75f

    mul-float/2addr v0, p3

    iput v0, p0, Lsoftware/simplicial/a/bx;->m:F

    goto/16 :goto_0

    .line 117
    :cond_6
    mul-float v0, p3, v2

    iput v0, p0, Lsoftware/simplicial/a/bx;->l:F

    .line 118
    mul-float v0, p3, v3

    iput v0, p0, Lsoftware/simplicial/a/bx;->m:F

    goto/16 :goto_0

    .line 39
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 6
    check-cast p1, Lsoftware/simplicial/a/bx;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/a/bx;->a(Lsoftware/simplicial/a/bx;)I

    move-result v0

    return v0
.end method
