.class public final enum Lsoftware/simplicial/a/c/e;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/c/e;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/c/e;

.field public static final enum b:Lsoftware/simplicial/a/c/e;

.field public static final enum c:Lsoftware/simplicial/a/c/e;

.field public static final enum d:Lsoftware/simplicial/a/c/e;

.field public static final enum e:Lsoftware/simplicial/a/c/e;

.field public static final enum f:Lsoftware/simplicial/a/c/e;

.field public static final enum g:Lsoftware/simplicial/a/c/e;

.field public static final enum h:Lsoftware/simplicial/a/c/e;

.field public static final enum i:Lsoftware/simplicial/a/c/e;

.field public static final enum j:Lsoftware/simplicial/a/c/e;

.field public static final enum k:Lsoftware/simplicial/a/c/e;

.field public static final enum l:Lsoftware/simplicial/a/c/e;

.field public static final enum m:Lsoftware/simplicial/a/c/e;

.field public static final enum n:Lsoftware/simplicial/a/c/e;

.field public static final enum o:Lsoftware/simplicial/a/c/e;

.field public static final enum p:Lsoftware/simplicial/a/c/e;

.field public static final enum q:Lsoftware/simplicial/a/c/e;

.field public static final r:[Lsoftware/simplicial/a/c/e;

.field private static final synthetic s:[Lsoftware/simplicial/a/c/e;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 12
    new-instance v0, Lsoftware/simplicial/a/c/e;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/c/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/c/e;->a:Lsoftware/simplicial/a/c/e;

    new-instance v0, Lsoftware/simplicial/a/c/e;

    const-string v1, "FFA_TIME"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/c/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/c/e;->b:Lsoftware/simplicial/a/c/e;

    new-instance v0, Lsoftware/simplicial/a/c/e;

    const-string v1, "TEAMS"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/a/c/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/c/e;->c:Lsoftware/simplicial/a/c/e;

    new-instance v0, Lsoftware/simplicial/a/c/e;

    const-string v1, "TEAMS_TIME"

    invoke-direct {v0, v1, v6}, Lsoftware/simplicial/a/c/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/c/e;->d:Lsoftware/simplicial/a/c/e;

    new-instance v0, Lsoftware/simplicial/a/c/e;

    const-string v1, "CTF"

    invoke-direct {v0, v1, v7}, Lsoftware/simplicial/a/c/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/c/e;->e:Lsoftware/simplicial/a/c/e;

    new-instance v0, Lsoftware/simplicial/a/c/e;

    const-string v1, "SURVIVAL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/c/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/c/e;->f:Lsoftware/simplicial/a/c/e;

    new-instance v0, Lsoftware/simplicial/a/c/e;

    const-string v1, "SOCCER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/c/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/c/e;->g:Lsoftware/simplicial/a/c/e;

    new-instance v0, Lsoftware/simplicial/a/c/e;

    const-string v1, "FFA_CLASSIC"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/c/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/c/e;->h:Lsoftware/simplicial/a/c/e;

    new-instance v0, Lsoftware/simplicial/a/c/e;

    const-string v1, "FFA"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/c/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/c/e;->i:Lsoftware/simplicial/a/c/e;

    new-instance v0, Lsoftware/simplicial/a/c/e;

    const-string v1, "DOMINATION"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/c/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/c/e;->j:Lsoftware/simplicial/a/c/e;

    new-instance v0, Lsoftware/simplicial/a/c/e;

    const-string v1, "ZA"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/c/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/c/e;->k:Lsoftware/simplicial/a/c/e;

    new-instance v0, Lsoftware/simplicial/a/c/e;

    const-string v1, "PAINT"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/c/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/c/e;->l:Lsoftware/simplicial/a/c/e;

    new-instance v0, Lsoftware/simplicial/a/c/e;

    const-string v1, "TEAM_DEATHMATCH"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/c/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/c/e;->m:Lsoftware/simplicial/a/c/e;

    new-instance v0, Lsoftware/simplicial/a/c/e;

    const-string v1, "X"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/c/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/c/e;->n:Lsoftware/simplicial/a/c/e;

    new-instance v0, Lsoftware/simplicial/a/c/e;

    const-string v1, "X2"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/c/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/c/e;->o:Lsoftware/simplicial/a/c/e;

    new-instance v0, Lsoftware/simplicial/a/c/e;

    const-string v1, "X3"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/c/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/c/e;->p:Lsoftware/simplicial/a/c/e;

    new-instance v0, Lsoftware/simplicial/a/c/e;

    const-string v1, "X4"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/c/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/c/e;->q:Lsoftware/simplicial/a/c/e;

    .line 10
    const/16 v0, 0x11

    new-array v0, v0, [Lsoftware/simplicial/a/c/e;

    sget-object v1, Lsoftware/simplicial/a/c/e;->a:Lsoftware/simplicial/a/c/e;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/c/e;->b:Lsoftware/simplicial/a/c/e;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/a/c/e;->c:Lsoftware/simplicial/a/c/e;

    aput-object v1, v0, v5

    sget-object v1, Lsoftware/simplicial/a/c/e;->d:Lsoftware/simplicial/a/c/e;

    aput-object v1, v0, v6

    sget-object v1, Lsoftware/simplicial/a/c/e;->e:Lsoftware/simplicial/a/c/e;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lsoftware/simplicial/a/c/e;->f:Lsoftware/simplicial/a/c/e;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lsoftware/simplicial/a/c/e;->g:Lsoftware/simplicial/a/c/e;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lsoftware/simplicial/a/c/e;->h:Lsoftware/simplicial/a/c/e;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lsoftware/simplicial/a/c/e;->i:Lsoftware/simplicial/a/c/e;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lsoftware/simplicial/a/c/e;->j:Lsoftware/simplicial/a/c/e;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lsoftware/simplicial/a/c/e;->k:Lsoftware/simplicial/a/c/e;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lsoftware/simplicial/a/c/e;->l:Lsoftware/simplicial/a/c/e;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lsoftware/simplicial/a/c/e;->m:Lsoftware/simplicial/a/c/e;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lsoftware/simplicial/a/c/e;->n:Lsoftware/simplicial/a/c/e;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lsoftware/simplicial/a/c/e;->o:Lsoftware/simplicial/a/c/e;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lsoftware/simplicial/a/c/e;->p:Lsoftware/simplicial/a/c/e;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lsoftware/simplicial/a/c/e;->q:Lsoftware/simplicial/a/c/e;

    aput-object v2, v0, v1

    sput-object v0, Lsoftware/simplicial/a/c/e;->s:[Lsoftware/simplicial/a/c/e;

    .line 14
    invoke-static {}, Lsoftware/simplicial/a/c/e;->values()[Lsoftware/simplicial/a/c/e;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/a/c/e;->r:[Lsoftware/simplicial/a/c/e;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/c/e;
    .locals 1

    .prologue
    .line 10
    const-class v0, Lsoftware/simplicial/a/c/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/c/e;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/c/e;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lsoftware/simplicial/a/c/e;->s:[Lsoftware/simplicial/a/c/e;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/c/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/c/e;

    return-object v0
.end method
