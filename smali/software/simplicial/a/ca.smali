.class public Lsoftware/simplicial/a/ca;
.super Lsoftware/simplicial/a/ao;
.source "SourceFile"


# instance fields
.field public A:F

.field public B:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lsoftware/simplicial/a/ao;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public a:I

.field public b:F

.field public c:F

.field public d:F

.field public e:F

.field public f:F

.field public g:F

.field public h:F

.field public i:F

.field public j:F

.field public k:F

.field public z:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lsoftware/simplicial/a/ao;-><init>()V

    .line 33
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/ca;->B:Ljava/util/Map;

    .line 37
    invoke-virtual {p0}, Lsoftware/simplicial/a/ca;->b()V

    .line 38
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lsoftware/simplicial/a/ao;-><init>()V

    .line 33
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/ca;->B:Ljava/util/Map;

    .line 42
    iput p1, p0, Lsoftware/simplicial/a/ca;->a:I

    .line 43
    invoke-virtual {p0}, Lsoftware/simplicial/a/ca;->b()V

    .line 44
    return-void
.end method


# virtual methods
.method public a(FF)F
    .locals 3

    .prologue
    .line 160
    iget v0, p0, Lsoftware/simplicial/a/ca;->i:F

    iget v1, p0, Lsoftware/simplicial/a/ca;->g:F

    sub-float/2addr v0, v1

    mul-float/2addr v0, p1

    iget v1, p0, Lsoftware/simplicial/a/ca;->h:F

    iget v2, p0, Lsoftware/simplicial/a/ca;->f:F

    sub-float/2addr v1, v2

    mul-float/2addr v1, p2

    sub-float/2addr v0, v1

    iget v1, p0, Lsoftware/simplicial/a/ca;->h:F

    iget v2, p0, Lsoftware/simplicial/a/ca;->g:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lsoftware/simplicial/a/ca;->i:F

    iget v2, p0, Lsoftware/simplicial/a/ca;->f:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 161
    const v1, 0x431c8000    # 156.5f

    div-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/ca;->r:F

    .line 162
    iget v0, p0, Lsoftware/simplicial/a/ca;->r:F

    iget v1, p0, Lsoftware/simplicial/a/ca;->r:F

    mul-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/ca;->o:F

    .line 163
    iget v0, p0, Lsoftware/simplicial/a/ca;->r:F

    return v0
.end method

.method public a()V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 177
    iget v0, p0, Lsoftware/simplicial/a/ca;->f:F

    iget v1, p0, Lsoftware/simplicial/a/ca;->b:F

    iget v2, p0, Lsoftware/simplicial/a/ca;->f:F

    sub-float/2addr v1, v2

    div-float/2addr v1, v3

    add-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/ca;->f:F

    .line 178
    iget v0, p0, Lsoftware/simplicial/a/ca;->g:F

    iget v1, p0, Lsoftware/simplicial/a/ca;->c:F

    iget v2, p0, Lsoftware/simplicial/a/ca;->g:F

    sub-float/2addr v1, v2

    div-float/2addr v1, v3

    add-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/ca;->g:F

    .line 179
    iget v0, p0, Lsoftware/simplicial/a/ca;->h:F

    iget v1, p0, Lsoftware/simplicial/a/ca;->d:F

    iget v2, p0, Lsoftware/simplicial/a/ca;->h:F

    sub-float/2addr v1, v2

    div-float/2addr v1, v3

    add-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/ca;->h:F

    .line 180
    iget v0, p0, Lsoftware/simplicial/a/ca;->i:F

    iget v1, p0, Lsoftware/simplicial/a/ca;->e:F

    iget v2, p0, Lsoftware/simplicial/a/ca;->i:F

    sub-float/2addr v1, v2

    div-float/2addr v1, v3

    add-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/ca;->i:F

    .line 182
    iget v0, p0, Lsoftware/simplicial/a/ca;->w:F

    iget v1, p0, Lsoftware/simplicial/a/ca;->n:F

    iget v2, p0, Lsoftware/simplicial/a/ca;->w:F

    sub-float/2addr v1, v2

    div-float/2addr v1, v3

    add-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/ca;->w:F

    .line 193
    return-void
.end method

.method public a(FFFFFF)V
    .locals 3

    .prologue
    const v2, 0x429c8000    # 78.25f

    .line 54
    invoke-super {p0, p1, p2, v2}, Lsoftware/simplicial/a/ao;->a(FFF)V

    .line 56
    float-to-double v0, p3

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    double-to-float v0, v0

    mul-float/2addr v0, v2

    add-float/2addr v0, p1

    iput v0, p0, Lsoftware/simplicial/a/ca;->b:F

    .line 57
    float-to-double v0, p3

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    double-to-float v0, v0

    mul-float/2addr v0, v2

    add-float/2addr v0, p2

    iput v0, p0, Lsoftware/simplicial/a/ca;->c:F

    .line 58
    float-to-double v0, p3

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    double-to-float v0, v0

    mul-float/2addr v0, v2

    sub-float v0, p1, v0

    iput v0, p0, Lsoftware/simplicial/a/ca;->d:F

    .line 59
    float-to-double v0, p3

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    double-to-float v0, v0

    mul-float/2addr v0, v2

    sub-float v0, p2, v0

    iput v0, p0, Lsoftware/simplicial/a/ca;->e:F

    .line 60
    iput p3, p0, Lsoftware/simplicial/a/ca;->j:F

    .line 62
    iput p4, p0, Lsoftware/simplicial/a/ca;->z:F

    .line 63
    iput p5, p0, Lsoftware/simplicial/a/ca;->A:F

    .line 64
    iput p6, p0, Lsoftware/simplicial/a/ca;->k:F

    .line 66
    iget-object v0, p0, Lsoftware/simplicial/a/ca;->B:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 67
    return-void
.end method

.method public a(Lsoftware/simplicial/a/ao;)Z
    .locals 6

    .prologue
    const/high16 v5, 0x7f800000    # Float.POSITIVE_INFINITY

    const/4 v0, 0x0

    .line 81
    iget v1, p0, Lsoftware/simplicial/a/ca;->b:F

    iget v2, p0, Lsoftware/simplicial/a/ca;->d:F

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 82
    iget v2, p0, Lsoftware/simplicial/a/ca;->b:F

    iget v3, p0, Lsoftware/simplicial/a/ca;->d:F

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 83
    iget v3, p1, Lsoftware/simplicial/a/ao;->l:F

    iget v4, p1, Lsoftware/simplicial/a/ao;->n:F

    sub-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_0

    iget v2, p1, Lsoftware/simplicial/a/ao;->l:F

    iget v3, p1, Lsoftware/simplicial/a/ao;->n:F

    add-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    .line 85
    :cond_0
    iput v5, p0, Lsoftware/simplicial/a/ca;->r:F

    iput v5, p0, Lsoftware/simplicial/a/ca;->o:F

    .line 100
    :cond_1
    :goto_0
    return v0

    .line 88
    :cond_2
    iget v1, p0, Lsoftware/simplicial/a/ca;->c:F

    iget v2, p0, Lsoftware/simplicial/a/ca;->e:F

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 89
    iget v2, p0, Lsoftware/simplicial/a/ca;->c:F

    iget v3, p0, Lsoftware/simplicial/a/ca;->e:F

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 90
    iget v3, p1, Lsoftware/simplicial/a/ao;->m:F

    iget v4, p1, Lsoftware/simplicial/a/ao;->n:F

    sub-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_3

    iget v2, p1, Lsoftware/simplicial/a/ao;->m:F

    iget v3, p1, Lsoftware/simplicial/a/ao;->n:F

    add-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_4

    .line 92
    :cond_3
    iput v5, p0, Lsoftware/simplicial/a/ca;->r:F

    iput v5, p0, Lsoftware/simplicial/a/ca;->o:F

    goto :goto_0

    .line 96
    :cond_4
    iget v1, p1, Lsoftware/simplicial/a/ao;->n:F

    .line 97
    iget v2, p1, Lsoftware/simplicial/a/ao;->l:F

    iget v3, p1, Lsoftware/simplicial/a/ao;->m:F

    invoke-virtual {p0, v2, v3}, Lsoftware/simplicial/a/ca;->b(FF)F

    move-result v2

    cmpl-float v1, v2, v1

    if-gtz v1, :cond_1

    .line 100
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Lsoftware/simplicial/a/ao;F)Z
    .locals 6

    .prologue
    const/high16 v5, 0x7f800000    # Float.POSITIVE_INFINITY

    const/4 v0, 0x0

    .line 106
    iget v1, p0, Lsoftware/simplicial/a/ca;->b:F

    iget v2, p0, Lsoftware/simplicial/a/ca;->d:F

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 107
    iget v2, p0, Lsoftware/simplicial/a/ca;->b:F

    iget v3, p0, Lsoftware/simplicial/a/ca;->d:F

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 108
    sub-float/2addr v2, p2

    iget v3, p1, Lsoftware/simplicial/a/ao;->l:F

    iget v4, p1, Lsoftware/simplicial/a/ao;->n:F

    sub-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_0

    add-float/2addr v1, p2

    iget v2, p1, Lsoftware/simplicial/a/ao;->l:F

    iget v3, p1, Lsoftware/simplicial/a/ao;->n:F

    add-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    .line 110
    :cond_0
    iput v5, p0, Lsoftware/simplicial/a/ca;->r:F

    iput v5, p0, Lsoftware/simplicial/a/ca;->o:F

    .line 125
    :cond_1
    :goto_0
    return v0

    .line 113
    :cond_2
    iget v1, p0, Lsoftware/simplicial/a/ca;->c:F

    iget v2, p0, Lsoftware/simplicial/a/ca;->e:F

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 114
    iget v2, p0, Lsoftware/simplicial/a/ca;->c:F

    iget v3, p0, Lsoftware/simplicial/a/ca;->e:F

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 115
    sub-float/2addr v2, p2

    iget v3, p1, Lsoftware/simplicial/a/ao;->m:F

    iget v4, p1, Lsoftware/simplicial/a/ao;->n:F

    sub-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_3

    add-float/2addr v1, p2

    iget v2, p1, Lsoftware/simplicial/a/ao;->m:F

    iget v3, p1, Lsoftware/simplicial/a/ao;->n:F

    add-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_4

    .line 117
    :cond_3
    iput v5, p0, Lsoftware/simplicial/a/ca;->r:F

    iput v5, p0, Lsoftware/simplicial/a/ca;->o:F

    goto :goto_0

    .line 121
    :cond_4
    iget v1, p1, Lsoftware/simplicial/a/ao;->n:F

    sub-float/2addr v1, p2

    .line 122
    iget v2, p1, Lsoftware/simplicial/a/ao;->l:F

    iget v3, p1, Lsoftware/simplicial/a/ao;->m:F

    invoke-virtual {p0, v2, v3}, Lsoftware/simplicial/a/ca;->b(FF)F

    move-result v2

    cmpl-float v1, v2, v1

    if-gtz v1, :cond_1

    .line 125
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(FF)F
    .locals 3

    .prologue
    .line 168
    iget v0, p0, Lsoftware/simplicial/a/ca;->e:F

    iget v1, p0, Lsoftware/simplicial/a/ca;->c:F

    sub-float/2addr v0, v1

    mul-float/2addr v0, p1

    iget v1, p0, Lsoftware/simplicial/a/ca;->d:F

    iget v2, p0, Lsoftware/simplicial/a/ca;->b:F

    sub-float/2addr v1, v2

    mul-float/2addr v1, p2

    sub-float/2addr v0, v1

    iget v1, p0, Lsoftware/simplicial/a/ca;->d:F

    iget v2, p0, Lsoftware/simplicial/a/ca;->c:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lsoftware/simplicial/a/ca;->e:F

    iget v2, p0, Lsoftware/simplicial/a/ca;->b:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 169
    const v1, 0x431c8000    # 156.5f

    div-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/ca;->r:F

    .line 170
    iget v0, p0, Lsoftware/simplicial/a/ca;->r:F

    iget v1, p0, Lsoftware/simplicial/a/ca;->r:F

    mul-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/ca;->o:F

    .line 171
    iget v0, p0, Lsoftware/simplicial/a/ca;->r:F

    return v0
.end method

.method public b()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 48
    move-object v0, p0

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    invoke-virtual/range {v0 .. v6}, Lsoftware/simplicial/a/ca;->a(FFFFFF)V

    .line 49
    return-void
.end method

.method public b(Lsoftware/simplicial/a/ao;)Z
    .locals 6

    .prologue
    const/high16 v5, 0x7f800000    # Float.POSITIVE_INFINITY

    const/4 v0, 0x0

    .line 131
    iget v1, p0, Lsoftware/simplicial/a/ca;->f:F

    iget v2, p0, Lsoftware/simplicial/a/ca;->h:F

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 132
    iget v2, p0, Lsoftware/simplicial/a/ca;->f:F

    iget v3, p0, Lsoftware/simplicial/a/ca;->h:F

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 133
    iget v3, p1, Lsoftware/simplicial/a/ao;->s:F

    iget v4, p1, Lsoftware/simplicial/a/ao;->n:F

    sub-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_0

    iget v2, p1, Lsoftware/simplicial/a/ao;->s:F

    iget v3, p1, Lsoftware/simplicial/a/ao;->n:F

    add-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    .line 135
    :cond_0
    iput v5, p0, Lsoftware/simplicial/a/ca;->r:F

    iput v5, p0, Lsoftware/simplicial/a/ca;->o:F

    .line 149
    :cond_1
    :goto_0
    return v0

    .line 138
    :cond_2
    iget v1, p0, Lsoftware/simplicial/a/ca;->g:F

    iget v2, p0, Lsoftware/simplicial/a/ca;->i:F

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 139
    iget v2, p0, Lsoftware/simplicial/a/ca;->g:F

    iget v3, p0, Lsoftware/simplicial/a/ca;->i:F

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 140
    iget v3, p1, Lsoftware/simplicial/a/ao;->t:F

    iget v4, p1, Lsoftware/simplicial/a/ao;->n:F

    sub-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_3

    iget v2, p1, Lsoftware/simplicial/a/ao;->t:F

    iget v3, p1, Lsoftware/simplicial/a/ao;->n:F

    add-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_4

    .line 142
    :cond_3
    iput v5, p0, Lsoftware/simplicial/a/ca;->r:F

    iput v5, p0, Lsoftware/simplicial/a/ca;->o:F

    goto :goto_0

    .line 146
    :cond_4
    iget v1, p1, Lsoftware/simplicial/a/ao;->n:F

    .line 147
    iget v2, p1, Lsoftware/simplicial/a/ao;->s:F

    iget v3, p1, Lsoftware/simplicial/a/ao;->t:F

    invoke-virtual {p0, v2, v3}, Lsoftware/simplicial/a/ca;->a(FF)F

    move-result v2

    cmpl-float v1, v2, v1

    if-gtz v1, :cond_1

    .line 149
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(Lsoftware/simplicial/a/ao;F)Z
    .locals 2

    .prologue
    .line 155
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not Implemented."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public c(FF)Lsoftware/simplicial/a/cb;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 197
    iget v0, p0, Lsoftware/simplicial/a/ca;->d:F

    iget v1, p0, Lsoftware/simplicial/a/ca;->b:F

    sub-float/2addr v0, v1

    .line 198
    iget v1, p0, Lsoftware/simplicial/a/ca;->e:F

    iget v2, p0, Lsoftware/simplicial/a/ca;->c:F

    sub-float/2addr v1, v2

    .line 199
    iget v2, p0, Lsoftware/simplicial/a/ca;->b:F

    sub-float v2, p1, v2

    .line 200
    iget v3, p0, Lsoftware/simplicial/a/ca;->c:F

    sub-float v3, p2, v3

    .line 202
    mul-float v4, v2, v0

    mul-float v5, v3, v1

    add-float/2addr v4, v5

    cmpg-float v4, v4, v8

    if-gez v4, :cond_0

    .line 203
    sget-object v0, Lsoftware/simplicial/a/cb;->a:Lsoftware/simplicial/a/cb;

    .line 212
    :goto_0
    return-object v0

    .line 205
    :cond_0
    iget v4, p0, Lsoftware/simplicial/a/ca;->b:F

    iget v5, p0, Lsoftware/simplicial/a/ca;->d:F

    sub-float/2addr v4, v5

    .line 206
    iget v5, p0, Lsoftware/simplicial/a/ca;->c:F

    iget v6, p0, Lsoftware/simplicial/a/ca;->e:F

    sub-float/2addr v5, v6

    .line 207
    iget v6, p0, Lsoftware/simplicial/a/ca;->d:F

    sub-float v6, p1, v6

    .line 208
    iget v7, p0, Lsoftware/simplicial/a/ca;->e:F

    sub-float v7, p2, v7

    .line 209
    mul-float/2addr v4, v6

    mul-float/2addr v5, v7

    add-float/2addr v4, v5

    cmpg-float v4, v4, v8

    if-gez v4, :cond_1

    .line 210
    sget-object v0, Lsoftware/simplicial/a/cb;->a:Lsoftware/simplicial/a/cb;

    goto :goto_0

    .line 212
    :cond_1
    mul-float/2addr v1, v2

    mul-float/2addr v0, v3

    sub-float v0, v1, v0

    cmpg-float v0, v0, v8

    if-gez v0, :cond_2

    sget-object v0, Lsoftware/simplicial/a/cb;->b:Lsoftware/simplicial/a/cb;

    goto :goto_0

    :cond_2
    sget-object v0, Lsoftware/simplicial/a/cb;->c:Lsoftware/simplicial/a/cb;

    goto :goto_0
.end method

.method public c()V
    .locals 5

    .prologue
    const v4, 0x429c8000    # 78.25f

    .line 71
    iget v0, p0, Lsoftware/simplicial/a/ca;->e:F

    iget v1, p0, Lsoftware/simplicial/a/ca;->c:F

    sub-float/2addr v0, v1

    float-to-double v0, v0

    iget v2, p0, Lsoftware/simplicial/a/ca;->d:F

    iget v3, p0, Lsoftware/simplicial/a/ca;->b:F

    sub-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lsoftware/simplicial/a/ca;->j:F

    .line 72
    iget v0, p0, Lsoftware/simplicial/a/ca;->l:F

    iget v1, p0, Lsoftware/simplicial/a/ca;->j:F

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    double-to-float v1, v2

    mul-float/2addr v1, v4

    add-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/ca;->b:F

    .line 73
    iget v0, p0, Lsoftware/simplicial/a/ca;->m:F

    iget v1, p0, Lsoftware/simplicial/a/ca;->j:F

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    double-to-float v1, v2

    mul-float/2addr v1, v4

    add-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/ca;->c:F

    .line 74
    iget v0, p0, Lsoftware/simplicial/a/ca;->l:F

    iget v1, p0, Lsoftware/simplicial/a/ca;->j:F

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    double-to-float v1, v2

    mul-float/2addr v1, v4

    sub-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/ca;->d:F

    .line 75
    iget v0, p0, Lsoftware/simplicial/a/ca;->m:F

    iget v1, p0, Lsoftware/simplicial/a/ca;->j:F

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    double-to-float v1, v2

    mul-float/2addr v1, v4

    sub-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/ca;->e:F

    .line 76
    return-void
.end method

.method public d(FF)Lsoftware/simplicial/a/cb;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 217
    iget v0, p0, Lsoftware/simplicial/a/ca;->h:F

    iget v1, p0, Lsoftware/simplicial/a/ca;->f:F

    sub-float/2addr v0, v1

    .line 218
    iget v1, p0, Lsoftware/simplicial/a/ca;->i:F

    iget v2, p0, Lsoftware/simplicial/a/ca;->g:F

    sub-float/2addr v1, v2

    .line 219
    iget v2, p0, Lsoftware/simplicial/a/ca;->f:F

    sub-float v2, p1, v2

    .line 220
    iget v3, p0, Lsoftware/simplicial/a/ca;->g:F

    sub-float v3, p2, v3

    .line 222
    mul-float v4, v2, v0

    mul-float v5, v3, v1

    add-float/2addr v4, v5

    cmpg-float v4, v4, v8

    if-gez v4, :cond_0

    .line 223
    sget-object v0, Lsoftware/simplicial/a/cb;->a:Lsoftware/simplicial/a/cb;

    .line 232
    :goto_0
    return-object v0

    .line 225
    :cond_0
    iget v4, p0, Lsoftware/simplicial/a/ca;->f:F

    iget v5, p0, Lsoftware/simplicial/a/ca;->h:F

    sub-float/2addr v4, v5

    .line 226
    iget v5, p0, Lsoftware/simplicial/a/ca;->g:F

    iget v6, p0, Lsoftware/simplicial/a/ca;->i:F

    sub-float/2addr v5, v6

    .line 227
    iget v6, p0, Lsoftware/simplicial/a/ca;->h:F

    sub-float v6, p1, v6

    .line 228
    iget v7, p0, Lsoftware/simplicial/a/ca;->i:F

    sub-float v7, p2, v7

    .line 229
    mul-float/2addr v4, v6

    mul-float/2addr v5, v7

    add-float/2addr v4, v5

    cmpg-float v4, v4, v8

    if-gez v4, :cond_1

    .line 230
    sget-object v0, Lsoftware/simplicial/a/cb;->a:Lsoftware/simplicial/a/cb;

    goto :goto_0

    .line 232
    :cond_1
    mul-float/2addr v1, v2

    mul-float/2addr v0, v3

    sub-float v0, v1, v0

    cmpg-float v0, v0, v8

    if-gez v0, :cond_2

    sget-object v0, Lsoftware/simplicial/a/cb;->b:Lsoftware/simplicial/a/cb;

    goto :goto_0

    :cond_2
    sget-object v0, Lsoftware/simplicial/a/cb;->c:Lsoftware/simplicial/a/cb;

    goto :goto_0
.end method
