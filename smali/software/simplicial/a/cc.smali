.class public Lsoftware/simplicial/a/cc;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lsoftware/simplicial/a/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/cc;->a:Ljava/util/Collection;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lsoftware/simplicial/a/cc;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/b;

    .line 36
    invoke-interface {v0}, Lsoftware/simplicial/a/b;->p_()V

    goto :goto_0

    .line 38
    :cond_0
    return-void
.end method

.method public a(II)V
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lsoftware/simplicial/a/cc;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/b;

    .line 111
    invoke-interface {v0, p1, p2}, Lsoftware/simplicial/a/b;->a(II)V

    goto :goto_0

    .line 113
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 42
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;ZLjava/lang/String;)V

    .line 43
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;ZLjava/lang/String;)V

    .line 48
    return-void
.end method

.method public a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lsoftware/simplicial/a/cc;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/b;

    .line 54
    invoke-interface {v0, p1, p2, p3}, Lsoftware/simplicial/a/b;->a(Ljava/lang/String;ZLjava/lang/String;)V

    goto :goto_0

    .line 56
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;[BLjava/lang/String;ZLsoftware/simplicial/a/q;ILsoftware/simplicial/a/q;Lsoftware/simplicial/a/q;JJII)V
    .locals 17

    .prologue
    .line 85
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/cc;->a:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lsoftware/simplicial/a/b;

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move/from16 v5, p4

    move-object/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-wide/from16 v10, p11

    move/from16 v12, p13

    move-wide/from16 v13, p9

    move/from16 v15, p14

    .line 87
    invoke-interface/range {v1 .. v15}, Lsoftware/simplicial/a/b;->a(Ljava/lang/String;[BLjava/lang/String;ZLsoftware/simplicial/a/q;ILsoftware/simplicial/a/q;Lsoftware/simplicial/a/q;JIJI)V

    goto :goto_0

    .line 89
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;[BLsoftware/simplicial/a/q;I)V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lsoftware/simplicial/a/cc;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/b;

    .line 70
    invoke-interface {v0, p1, p2, p3, p4}, Lsoftware/simplicial/a/b;->a(Ljava/lang/String;[BLsoftware/simplicial/a/q;I)V

    goto :goto_0

    .line 72
    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lsoftware/simplicial/a/cc;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/b;

    .line 95
    invoke-interface {v0, p1}, Lsoftware/simplicial/a/b;->a(Ljava/util/List;)V

    goto :goto_0

    .line 97
    :cond_0
    return-void
.end method

.method public a(Lsoftware/simplicial/a/b;)V
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lsoftware/simplicial/a/cc;->a:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 17
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Lsoftware/simplicial/a/cc;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/b;

    .line 28
    invoke-interface {v0, p1}, Lsoftware/simplicial/a/b;->a(Z)V

    goto :goto_0

    .line 30
    :cond_0
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lsoftware/simplicial/a/cc;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/b;

    .line 78
    invoke-interface {v0}, Lsoftware/simplicial/a/b;->b()V

    goto :goto_0

    .line 80
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lsoftware/simplicial/a/cc;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/b;

    .line 103
    invoke-interface {v0, p1}, Lsoftware/simplicial/a/b;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 105
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lsoftware/simplicial/a/cc;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/b;

    .line 62
    invoke-interface {v0, p1, p2}, Lsoftware/simplicial/a/b;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 64
    :cond_0
    return-void
.end method

.method public b(Lsoftware/simplicial/a/b;)V
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lsoftware/simplicial/a/cc;->a:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 22
    return-void
.end method
