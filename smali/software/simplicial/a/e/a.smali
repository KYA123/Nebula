.class public Lsoftware/simplicial/a/e/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/a/e/a$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/logging/Logger;

.field private static b:Ljava/util/logging/ConsoleHandler;

.field private static c:Ljava/util/logging/FileHandler;

.field private static d:Z

.field private static e:Z

.field private static f:Ljava/lang/Thread;

.field private static g:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lsoftware/simplicial/a/e/a$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 18
    const-class v0, Lsoftware/simplicial/a/e/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/a/e/a;->a:Ljava/util/logging/Logger;

    .line 20
    sput-object v1, Lsoftware/simplicial/a/e/a;->b:Ljava/util/logging/ConsoleHandler;

    .line 21
    sput-object v1, Lsoftware/simplicial/a/e/a;->c:Ljava/util/logging/FileHandler;

    .line 23
    const/4 v0, 0x1

    sput-boolean v0, Lsoftware/simplicial/a/e/a;->d:Z

    .line 27
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    sput-object v0, Lsoftware/simplicial/a/e/a;->g:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized a()V
    .locals 4

    .prologue
    .line 127
    const-class v1, Lsoftware/simplicial/a/e/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lsoftware/simplicial/a/e/a;->f:Ljava/lang/Thread;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 142
    :goto_0
    monitor-exit v1

    return-void

    .line 130
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    sput-boolean v0, Lsoftware/simplicial/a/e/a;->e:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 133
    :try_start_2
    sget-object v0, Lsoftware/simplicial/a/e/a;->f:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 134
    sget-object v0, Lsoftware/simplicial/a/e/a;->f:Ljava/lang/Thread;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Ljava/lang/Thread;->join(J)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 141
    :goto_1
    const/4 v0, 0x0

    :try_start_3
    sput-object v0, Lsoftware/simplicial/a/e/a;->f:Ljava/lang/Thread;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 127
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 136
    :catch_0
    move-exception v0

    .line 138
    :try_start_4
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method public static a(Ljava/util/logging/Level;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 146
    sget-boolean v0, Lsoftware/simplicial/a/e/a;->d:Z

    if-nez v0, :cond_0

    .line 153
    :goto_0
    return-void

    .line 149
    :cond_0
    sget-object v0, Lsoftware/simplicial/a/e/a;->f:Ljava/lang/Thread;

    if-nez v0, :cond_1

    .line 150
    invoke-static {}, Lsoftware/simplicial/a/e/a;->e()V

    .line 152
    :cond_1
    sget-object v0, Lsoftware/simplicial/a/e/a;->g:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v1, Lsoftware/simplicial/a/e/a$a;

    invoke-direct {v1, p0, p1}, Lsoftware/simplicial/a/e/a$a;-><init>(Ljava/util/logging/Level;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 157
    sget-boolean v0, Lsoftware/simplicial/a/e/a;->d:Z

    if-nez v0, :cond_0

    .line 164
    :goto_0
    return-void

    .line 160
    :cond_0
    sget-object v0, Lsoftware/simplicial/a/e/a;->f:Ljava/lang/Thread;

    if-nez v0, :cond_1

    .line 161
    invoke-static {}, Lsoftware/simplicial/a/e/a;->e()V

    .line 163
    :cond_1
    sget-object v0, Lsoftware/simplicial/a/e/a;->g:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v1, Lsoftware/simplicial/a/e/a$a;

    invoke-direct {v1, p0, p1, p2}, Lsoftware/simplicial/a/e/a$a;-><init>(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static declared-synchronized a(Z)V
    .locals 2

    .prologue
    .line 31
    const-class v0, Lsoftware/simplicial/a/e/a;

    monitor-enter v0

    :try_start_0
    sput-boolean p0, Lsoftware/simplicial/a/e/a;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 32
    monitor-exit v0

    return-void

    .line 31
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method static synthetic b()Z
    .locals 1

    .prologue
    .line 16
    sget-boolean v0, Lsoftware/simplicial/a/e/a;->e:Z

    return v0
.end method

.method static synthetic c()Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lsoftware/simplicial/a/e/a;->g:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method

.method static synthetic d()Ljava/util/logging/Logger;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lsoftware/simplicial/a/e/a;->a:Ljava/util/logging/Logger;

    return-object v0
.end method

.method private static declared-synchronized e()V
    .locals 4

    .prologue
    .line 87
    const-class v1, Lsoftware/simplicial/a/e/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lsoftware/simplicial/a/e/a;->f:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 88
    invoke-static {}, Lsoftware/simplicial/a/e/a;->a()V

    .line 90
    :cond_0
    const/4 v0, 0x0

    sput-boolean v0, Lsoftware/simplicial/a/e/a;->e:Z

    .line 91
    new-instance v0, Ljava/lang/Thread;

    new-instance v2, Lsoftware/simplicial/a/e/a$1;

    invoke-direct {v2}, Lsoftware/simplicial/a/e/a$1;-><init>()V

    const-string v3, "Logger"

    invoke-direct {v0, v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    sput-object v0, Lsoftware/simplicial/a/e/a;->f:Ljava/lang/Thread;

    .line 122
    sget-object v0, Lsoftware/simplicial/a/e/a;->f:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123
    monitor-exit v1

    return-void

    .line 87
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
