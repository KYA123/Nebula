.class public Lsoftware/simplicial/a/f/af;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:Lsoftware/simplicial/a/f/ba;

.field public b:J

.field public c:Lsoftware/simplicial/a/aq;

.field public d:Lsoftware/simplicial/a/am;

.field public e:Lsoftware/simplicial/a/ap;

.field public f:I

.field public g:I

.field public h:I

.field public i:Ljava/lang/String;

.field public j:[B

.field public k:B

.field public l:Z

.field public m:J

.field public n:I

.field public o:I

.field public p:Z

.field public q:Z

.field public r:Z

.field public s:I

.field public t:Z

.field public u:I

.field public v:Z

.field public w:Lsoftware/simplicial/a/c/g;

.field public x:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lsoftware/simplicial/a/f/bh;->P:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 46
    return-void
.end method

.method public static a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/ba;J)[B
    .locals 4

    .prologue
    .line 55
    sget-object v0, Lsoftware/simplicial/a/f/ba;->a:Lsoftware/simplicial/a/f/ba;

    if-ne p1, v0, :cond_0

    .line 56
    new-instance v0, Ljava/security/InvalidParameterException;

    const-string v1, "result"

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :cond_0
    :try_start_0
    sget-object v0, Lsoftware/simplicial/a/f/bh;->P:Lsoftware/simplicial/a/f/bh;

    invoke-static {p0, v0}, Lsoftware/simplicial/a/f/af;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;)V

    .line 61
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/ba;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 62
    invoke-virtual {p0, p2, p3}, Lsoftware/simplicial/a/f/bn;->writeLong(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v0

    :goto_0
    return-object v0

    .line 64
    :catch_0
    move-exception v0

    .line 66
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred writing data. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 67
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/ba;Lsoftware/simplicial/a/aq;Lsoftware/simplicial/a/am;IIILjava/lang/String;[BILsoftware/simplicial/a/ap;ZZZJIIZZIZILsoftware/simplicial/a/c/g;ZJ)[B
    .locals 6

    .prologue
    .line 80
    :try_start_0
    sget-object v2, Lsoftware/simplicial/a/f/bh;->P:Lsoftware/simplicial/a/f/bh;

    invoke-static {p0, v2}, Lsoftware/simplicial/a/f/af;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;)V

    .line 81
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/ba;->ordinal()I

    move-result v2

    invoke-virtual {p0, v2}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 82
    sget-object v2, Lsoftware/simplicial/a/f/ba;->a:Lsoftware/simplicial/a/f/ba;

    if-ne p1, v2, :cond_0

    .line 84
    invoke-virtual {p2}, Lsoftware/simplicial/a/aq;->ordinal()I

    move-result v2

    invoke-virtual {p0, v2}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 85
    invoke-virtual {p3}, Lsoftware/simplicial/a/am;->ordinal()I

    move-result v2

    invoke-virtual {p0, v2}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 86
    invoke-virtual/range {p10 .. p10}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v2

    invoke-virtual {p0, v2}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 87
    invoke-virtual {p0, p4}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 88
    invoke-virtual {p0, p5}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 89
    invoke-virtual {p0, p6}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 90
    invoke-virtual {p0, p7}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 91
    invoke-virtual {p0, p9}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 92
    move/from16 v0, p11

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 93
    move-wide/from16 v0, p14

    invoke-virtual {p0, v0, v1}, Lsoftware/simplicial/a/f/bn;->writeLong(J)V

    .line 94
    move/from16 v0, p16

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 95
    move/from16 v0, p17

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 96
    move/from16 v0, p12

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 97
    move/from16 v0, p18

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 98
    move/from16 v0, p19

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 99
    move/from16 v0, p20

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 100
    move/from16 v0, p21

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 101
    move/from16 v0, p22

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 102
    move/from16 v0, p13

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 103
    invoke-virtual/range {p23 .. p23}, Lsoftware/simplicial/a/c/g;->ordinal()I

    move-result v2

    invoke-virtual {p0, v2}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 104
    array-length v2, p8

    invoke-virtual {p0, v2}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 105
    invoke-virtual {p0, p8}, Lsoftware/simplicial/a/f/bn;->write([B)V

    .line 106
    move/from16 v0, p24

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 108
    :cond_0
    move-wide/from16 v0, p25

    invoke-virtual {p0, v0, v1}, Lsoftware/simplicial/a/f/bn;->writeLong(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v2

    :goto_0
    return-object v2

    .line 110
    :catch_0
    move-exception v2

    .line 112
    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception occurred writing data. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 113
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 4

    .prologue
    .line 123
    :try_start_0
    new-instance v0, Lsoftware/simplicial/a/f/bm;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 125
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    invoke-static {v1}, Lsoftware/simplicial/a/f/ba;->a(B)Lsoftware/simplicial/a/f/ba;

    move-result-object v1

    iput-object v1, p0, Lsoftware/simplicial/a/f/af;->a:Lsoftware/simplicial/a/f/ba;

    .line 126
    iget-object v1, p0, Lsoftware/simplicial/a/f/af;->a:Lsoftware/simplicial/a/f/ba;

    sget-object v2, Lsoftware/simplicial/a/f/ba;->a:Lsoftware/simplicial/a/f/ba;

    if-ne v1, v2, :cond_0

    .line 128
    sget-object v1, Lsoftware/simplicial/a/aq;->c:[Lsoftware/simplicial/a/aq;

    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lsoftware/simplicial/a/f/af;->c:Lsoftware/simplicial/a/aq;

    .line 129
    sget-object v1, Lsoftware/simplicial/a/am;->s:[Lsoftware/simplicial/a/am;

    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lsoftware/simplicial/a/f/af;->d:Lsoftware/simplicial/a/am;

    .line 130
    sget-object v1, Lsoftware/simplicial/a/ap;->e:[Lsoftware/simplicial/a/ap;

    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lsoftware/simplicial/a/f/af;->e:Lsoftware/simplicial/a/ap;

    .line 131
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/af;->f:I

    .line 132
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/af;->g:I

    .line 133
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/af;->h:I

    .line 134
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lsoftware/simplicial/a/f/af;->i:Ljava/lang/String;

    .line 135
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    iput-byte v1, p0, Lsoftware/simplicial/a/f/af;->k:B

    .line 136
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v1

    iput-boolean v1, p0, Lsoftware/simplicial/a/f/af;->l:Z

    .line 137
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lsoftware/simplicial/a/f/af;->m:J

    .line 138
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/af;->n:I

    .line 139
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/af;->o:I

    .line 140
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v1

    iput-boolean v1, p0, Lsoftware/simplicial/a/f/af;->p:Z

    .line 141
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v1

    iput-boolean v1, p0, Lsoftware/simplicial/a/f/af;->q:Z

    .line 142
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v1

    iput-boolean v1, p0, Lsoftware/simplicial/a/f/af;->r:Z

    .line 143
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/af;->s:I

    .line 144
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v1

    iput-boolean v1, p0, Lsoftware/simplicial/a/f/af;->t:Z

    .line 145
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/af;->u:I

    .line 146
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v1

    iput-boolean v1, p0, Lsoftware/simplicial/a/f/af;->v:Z

    .line 147
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    invoke-static {v1}, Lsoftware/simplicial/a/c/g;->a(B)Lsoftware/simplicial/a/c/g;

    move-result-object v1

    iput-object v1, p0, Lsoftware/simplicial/a/f/af;->w:Lsoftware/simplicial/a/c/g;

    .line 148
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    new-array v1, v1, [B

    iput-object v1, p0, Lsoftware/simplicial/a/f/af;->j:[B

    .line 149
    iget-object v1, p0, Lsoftware/simplicial/a/f/af;->j:[B

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/f/bm;->readFully([B)V

    .line 150
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v1

    iput-boolean v1, p0, Lsoftware/simplicial/a/f/af;->x:Z

    .line 152
    :cond_0
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lsoftware/simplicial/a/f/af;->b:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 160
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 154
    :catch_0
    move-exception v0

    .line 156
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred parsing "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/a/f/af;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 157
    const/4 v0, 0x0

    goto :goto_0
.end method
