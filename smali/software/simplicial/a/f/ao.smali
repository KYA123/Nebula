.class public Lsoftware/simplicial/a/f/ao;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:Lsoftware/simplicial/a/d/a;

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lsoftware/simplicial/a/f/bh;->C:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 21
    return-void
.end method


# virtual methods
.method public a()Lsoftware/simplicial/a/f/ba;
    .locals 2

    .prologue
    .line 88
    sget-object v0, Lsoftware/simplicial/a/f/ao$1;->a:[I

    iget-object v1, p0, Lsoftware/simplicial/a/f/ao;->a:Lsoftware/simplicial/a/d/a;

    invoke-virtual {v1}, Lsoftware/simplicial/a/d/a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 98
    sget-object v0, Lsoftware/simplicial/a/f/ba;->g:Lsoftware/simplicial/a/f/ba;

    :goto_0
    return-object v0

    .line 91
    :pswitch_0
    sget-object v0, Lsoftware/simplicial/a/f/ba;->b:Lsoftware/simplicial/a/f/ba;

    goto :goto_0

    .line 94
    :pswitch_1
    sget-object v0, Lsoftware/simplicial/a/f/ba;->c:Lsoftware/simplicial/a/f/ba;

    goto :goto_0

    .line 96
    :pswitch_2
    sget-object v0, Lsoftware/simplicial/a/f/ba;->a:Lsoftware/simplicial/a/f/ba;

    goto :goto_0

    .line 88
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 4

    .prologue
    .line 67
    :try_start_0
    new-instance v1, Lsoftware/simplicial/a/f/bm;

    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v2, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v0, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v0}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 68
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v0

    .line 69
    if-ltz v0, :cond_0

    sget-object v2, Lsoftware/simplicial/a/d/a;->f:[Lsoftware/simplicial/a/d/a;

    array-length v2, v2

    if-lt v0, v2, :cond_1

    .line 70
    :cond_0
    sget-object v0, Lsoftware/simplicial/a/d/a;->e:Lsoftware/simplicial/a/d/a;

    invoke-virtual {v0}, Lsoftware/simplicial/a/d/a;->ordinal()I

    move-result v0

    .line 71
    :cond_1
    sget-object v2, Lsoftware/simplicial/a/d/a;->f:[Lsoftware/simplicial/a/d/a;

    aget-object v0, v2, v0

    iput-object v0, p0, Lsoftware/simplicial/a/f/ao;->a:Lsoftware/simplicial/a/d/a;

    .line 72
    iget-object v0, p0, Lsoftware/simplicial/a/f/ao;->a:Lsoftware/simplicial/a/d/a;

    sget-object v2, Lsoftware/simplicial/a/d/a;->a:Lsoftware/simplicial/a/d/a;

    if-ne v0, v2, :cond_2

    .line 74
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/a/f/ao;->b:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 77
    :catch_0
    move-exception v0

    .line 79
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred parsing "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/a/f/ao;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 80
    const/4 v0, 0x0

    goto :goto_0
.end method
