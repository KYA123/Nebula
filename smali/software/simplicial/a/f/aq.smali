.class public Lsoftware/simplicial/a/f/aq;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:Lsoftware/simplicial/a/f/ba;

.field public b:Lsoftware/simplicial/a/d/c;

.field public c:Lsoftware/simplicial/a/am;

.field public d:I

.field public e:I

.field public f:I

.field public g:Ljava/lang/String;

.field public h:Lsoftware/simplicial/a/ap;

.field public i:Z

.field public j:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lsoftware/simplicial/a/f/bh;->E:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 32
    return-void
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 4

    .prologue
    .line 85
    :try_start_0
    new-instance v0, Lsoftware/simplicial/a/f/bm;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 87
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    int-to-byte v1, v1

    invoke-static {v1}, Lsoftware/simplicial/a/f/ba;->a(B)Lsoftware/simplicial/a/f/ba;

    move-result-object v1

    iput-object v1, p0, Lsoftware/simplicial/a/f/aq;->a:Lsoftware/simplicial/a/f/ba;

    .line 88
    iget-object v1, p0, Lsoftware/simplicial/a/f/aq;->a:Lsoftware/simplicial/a/f/ba;

    sget-object v2, Lsoftware/simplicial/a/f/ba;->a:Lsoftware/simplicial/a/f/ba;

    if-ne v1, v2, :cond_0

    .line 90
    sget-object v1, Lsoftware/simplicial/a/d/c;->j:[Lsoftware/simplicial/a/d/c;

    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lsoftware/simplicial/a/f/aq;->b:Lsoftware/simplicial/a/d/c;

    .line 91
    sget-object v1, Lsoftware/simplicial/a/am;->s:[Lsoftware/simplicial/a/am;

    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lsoftware/simplicial/a/f/aq;->c:Lsoftware/simplicial/a/am;

    .line 92
    sget-object v1, Lsoftware/simplicial/a/ap;->e:[Lsoftware/simplicial/a/ap;

    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lsoftware/simplicial/a/f/aq;->h:Lsoftware/simplicial/a/ap;

    .line 93
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/aq;->d:I

    .line 94
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/aq;->e:I

    .line 95
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/aq;->f:I

    .line 96
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lsoftware/simplicial/a/f/aq;->g:Ljava/lang/String;

    .line 97
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v1

    iput-boolean v1, p0, Lsoftware/simplicial/a/f/aq;->i:Z

    .line 98
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/a/f/aq;->j:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 101
    :catch_0
    move-exception v0

    .line 103
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred parsing "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/a/f/aq;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 104
    const/4 v0, 0x0

    goto :goto_0
.end method
