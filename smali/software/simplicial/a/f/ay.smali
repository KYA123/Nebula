.class public Lsoftware/simplicial/a/f/ay;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:Lsoftware/simplicial/a/au;

.field public b:I

.field public c:I

.field public d:Ljava/lang/String;

.field public e:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 27
    sget-object v0, Lsoftware/simplicial/a/f/bh;->aR:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 20
    iput-object v2, p0, Lsoftware/simplicial/a/f/ay;->a:Lsoftware/simplicial/a/au;

    .line 21
    iput v1, p0, Lsoftware/simplicial/a/f/ay;->b:I

    .line 22
    iput v1, p0, Lsoftware/simplicial/a/f/ay;->c:I

    .line 23
    iput-object v2, p0, Lsoftware/simplicial/a/f/ay;->d:Ljava/lang/String;

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/a/f/ay;->e:Z

    .line 28
    return-void
.end method

.method public static a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/au;I)[B
    .locals 4

    .prologue
    .line 35
    :try_start_0
    sget-object v0, Lsoftware/simplicial/a/f/bh;->aR:Lsoftware/simplicial/a/f/bh;

    invoke-static {p0, v0, p1}, Lsoftware/simplicial/a/f/ay;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;I)V

    .line 37
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 38
    invoke-virtual {p2}, Lsoftware/simplicial/a/au;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 39
    invoke-virtual {p0, p3}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v0

    :goto_0
    return-object v0

    .line 41
    :catch_0
    move-exception v0

    .line 43
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred writing data.\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 44
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 4

    .prologue
    .line 75
    :try_start_0
    iget v0, p1, Lsoftware/simplicial/a/f/bi;->b:I

    iput v0, p0, Lsoftware/simplicial/a/f/ay;->ar:I

    .line 76
    new-instance v0, Lsoftware/simplicial/a/f/bm;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 78
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v1

    .line 79
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v2

    invoke-static {v2}, Lsoftware/simplicial/a/au;->a(B)Lsoftware/simplicial/a/au;

    move-result-object v2

    iput-object v2, p0, Lsoftware/simplicial/a/f/ay;->a:Lsoftware/simplicial/a/au;

    .line 80
    if-eqz v1, :cond_0

    .line 82
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lsoftware/simplicial/a/f/ay;->d:Ljava/lang/String;

    .line 83
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/a/f/ay;->c:I

    .line 96
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 87
    :cond_0
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/a/f/ay;->b:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 90
    :catch_0
    move-exception v0

    .line 92
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred parsing "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/a/f/ay;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 93
    const/4 v0, 0x0

    goto :goto_1
.end method
