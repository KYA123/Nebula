.class public Lsoftware/simplicial/a/f/bd;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:Lsoftware/simplicial/a/f/bj;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lsoftware/simplicial/a/f/bh;->d:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 23
    return-void
.end method

.method public static a(Lsoftware/simplicial/a/f/bn;III)[B
    .locals 4

    .prologue
    .line 29
    :try_start_0
    sget-object v0, Lsoftware/simplicial/a/f/bh;->d:Lsoftware/simplicial/a/f/bh;

    invoke-static {p0, v0, p1}, Lsoftware/simplicial/a/f/bd;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;I)V

    .line 30
    invoke-virtual {p0, p2}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 31
    invoke-virtual {p0, p3}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 39
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v0

    :goto_0
    return-object v0

    .line 33
    :catch_0
    move-exception v0

    .line 35
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred writing data. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 36
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 5

    .prologue
    .line 46
    :try_start_0
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/bj;

    move-object v1, v0

    iput-object v1, p0, Lsoftware/simplicial/a/f/bd;->c:Lsoftware/simplicial/a/f/bj;

    .line 47
    iget v1, p1, Lsoftware/simplicial/a/f/bi;->b:I

    iput v1, p0, Lsoftware/simplicial/a/f/bd;->ar:I

    .line 48
    new-instance v1, Lsoftware/simplicial/a/f/bm;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    iget-object v3, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v2}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 49
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v2

    iput v2, p0, Lsoftware/simplicial/a/f/bd;->a:I

    .line 50
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/bd;->b:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 52
    :catch_0
    move-exception v1

    .line 54
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception occurred parsing "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/a/f/bd;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 55
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "KEEP_ALIVE: gsaddress "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lsoftware/simplicial/a/f/bd;->b:I

    invoke-static {v1}, Lsoftware/simplicial/a/ba;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
