.class public Lsoftware/simplicial/a/f/bn;
.super Ljava/io/DataOutputStream;
.source "SourceFile"


# instance fields
.field private final a:Ljava/io/ByteArrayOutputStream;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-direct {p0, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 17
    iget-object v0, p0, Lsoftware/simplicial/a/f/bn;->out:Ljava/io/OutputStream;

    check-cast v0, Ljava/io/ByteArrayOutputStream;

    iput-object v0, p0, Lsoftware/simplicial/a/f/bn;->a:Ljava/io/ByteArrayOutputStream;

    .line 18
    return-void
.end method


# virtual methods
.method public a(FFF)V
    .locals 2

    .prologue
    .line 23
    const/high16 v0, 0x437f0000    # 255.0f

    sub-float v1, p1, p2

    mul-float/2addr v0, v1

    sub-float v1, p3, p2

    div-float/2addr v0, v1

    float-to-int v0, v0

    .line 24
    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 25
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 47
    ushr-int/lit8 v0, p1, 0x10

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 48
    ushr-int/lit8 v0, p1, 0x8

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 49
    ushr-int/lit8 v0, p1, 0x0

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 50
    return-void
.end method

.method public a()[B
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lsoftware/simplicial/a/f/bn;->a:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lsoftware/simplicial/a/f/bn;->a:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v0

    return v0
.end method

.method public b(FFF)V
    .locals 2

    .prologue
    .line 30
    const v0, 0x477fff00    # 65535.0f

    sub-float v1, p1, p2

    mul-float/2addr v0, v1

    sub-float v1, p3, p2

    div-float/2addr v0, v1

    float-to-int v0, v0

    int-to-short v0, v0

    .line 31
    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    .line 32
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lsoftware/simplicial/a/f/bn;->a:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 65
    return-void
.end method

.method public c(FFF)V
    .locals 2

    .prologue
    .line 36
    cmpg-float v0, p1, p2

    if-gez v0, :cond_1

    move v0, p2

    .line 37
    :goto_0
    cmpl-float v1, v0, p3

    if-lez v1, :cond_0

    move v0, p3

    .line 39
    :cond_0
    const v1, 0x4b7fffff    # 1.6777215E7f

    sub-float/2addr v0, p2

    mul-float/2addr v0, v1

    sub-float v1, p3, p2

    div-float/2addr v0, v1

    float-to-int v0, v0

    .line 40
    ushr-int/lit8 v1, v0, 0x10

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 41
    ushr-int/lit8 v1, v0, 0x8

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 42
    ushr-int/lit8 v0, v0, 0x0

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 43
    return-void

    :cond_1
    move v0, p1

    goto :goto_0
.end method
