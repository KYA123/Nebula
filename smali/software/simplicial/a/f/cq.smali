.class public Lsoftware/simplicial/a/f/cq;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:S

.field public b:B

.field public c:Z

.field public d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lsoftware/simplicial/a/f/bh;->aG:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 21
    return-void
.end method

.method public static a(Lsoftware/simplicial/a/f/bn;ISBZZ)[B
    .locals 4

    .prologue
    .line 27
    :try_start_0
    sget-object v0, Lsoftware/simplicial/a/f/bh;->aG:Lsoftware/simplicial/a/f/bh;

    invoke-static {p0, v0, p1}, Lsoftware/simplicial/a/f/cq;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;I)V

    .line 28
    invoke-virtual {p0, p2}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    .line 29
    invoke-virtual {p0, p3}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 30
    invoke-virtual {p0, p4}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 31
    invoke-virtual {p0, p5}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 39
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v0

    :goto_0
    return-object v0

    .line 33
    :catch_0
    move-exception v0

    .line 35
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred writing data.\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 36
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 46
    :try_start_0
    iget v2, p1, Lsoftware/simplicial/a/f/bi;->b:I

    iput v2, p0, Lsoftware/simplicial/a/f/cq;->ar:I

    .line 47
    new-instance v2, Lsoftware/simplicial/a/f/bm;

    new-instance v3, Ljava/io/ByteArrayInputStream;

    iget-object v4, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v2, v3}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 49
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v3

    iput-short v3, p0, Lsoftware/simplicial/a/f/cq;->a:S

    .line 50
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    iput-byte v3, p0, Lsoftware/simplicial/a/f/cq;->b:B

    .line 51
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->available()I

    move-result v3

    if-lez v3, :cond_1

    .line 53
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v3

    iput-boolean v3, p0, Lsoftware/simplicial/a/f/cq;->c:Z

    .line 54
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->available()I

    move-result v3

    if-lez v3, :cond_0

    .line 55
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v2

    iput-boolean v2, p0, Lsoftware/simplicial/a/f/cq;->d:Z

    .line 68
    :goto_0
    return v0

    .line 57
    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lsoftware/simplicial/a/f/cq;->d:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 62
    :catch_0
    move-exception v0

    .line 64
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception occurred parsing "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/a/f/cq;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    move v0, v1

    .line 65
    goto :goto_0

    .line 60
    :cond_1
    const/4 v2, 0x1

    :try_start_1
    iput-boolean v2, p0, Lsoftware/simplicial/a/f/cq;->c:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
