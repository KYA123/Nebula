.class public Lsoftware/simplicial/a/f/cr;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/i/b;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lsoftware/simplicial/a/i/e;

.field public e:I

.field public f:I

.field public g:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lsoftware/simplicial/a/f/bh;->aH:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/f/cr;->a:Ljava/util/List;

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/f/cr;->b:Ljava/util/List;

    .line 18
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/f/cr;->c:Ljava/util/List;

    .line 27
    return-void
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 65
    :try_start_0
    new-instance v2, Lsoftware/simplicial/a/f/bm;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v3, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v1, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v2, v1}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 67
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    invoke-static {v1}, Lsoftware/simplicial/a/i/e;->a(B)Lsoftware/simplicial/a/i/e;

    move-result-object v1

    iput-object v1, p0, Lsoftware/simplicial/a/f/cr;->d:Lsoftware/simplicial/a/i/e;

    .line 68
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/cr;->e:I

    .line 69
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/cr;->f:I

    .line 70
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->a()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/cr;->g:I

    .line 71
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 72
    iget-object v1, p0, Lsoftware/simplicial/a/f/cr;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    move v1, v0

    .line 73
    :goto_0
    if-ge v1, v3, :cond_0

    .line 74
    iget-object v4, p0, Lsoftware/simplicial/a/f/cr;->b:Ljava/util/List;

    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->a()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 75
    :cond_0
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 76
    iget-object v1, p0, Lsoftware/simplicial/a/f/cr;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    move v1, v0

    .line 77
    :goto_1
    if-ge v1, v3, :cond_1

    .line 78
    iget-object v4, p0, Lsoftware/simplicial/a/f/cr;->c:Ljava/util/List;

    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->a()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 79
    :cond_1
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 80
    iget-object v1, p0, Lsoftware/simplicial/a/f/cr;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    move v1, v0

    .line 81
    :goto_2
    if-ge v1, v3, :cond_2

    .line 83
    new-instance v4, Lsoftware/simplicial/a/i/b;

    invoke-direct {v4}, Lsoftware/simplicial/a/i/b;-><init>()V

    .line 85
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v5

    iput v5, v4, Lsoftware/simplicial/a/i/b;->a:I

    .line 86
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lsoftware/simplicial/a/i/b;->b:Ljava/lang/String;

    .line 88
    iget-object v5, p0, Lsoftware/simplicial/a/f/cr;->a:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 81
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 91
    :catch_0
    move-exception v1

    .line 93
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception occurred parsing "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/a/f/cr;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 97
    :goto_3
    return v0

    :cond_2
    const/4 v0, 0x1

    goto :goto_3
.end method
