.class Lsoftware/simplicial/a/f/cv$2;
.super Ljava/util/TimerTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/a/f/cv;->i()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field a:I

.field b:I

.field c:Lsoftware/simplicial/a/f/bn;

.field final synthetic d:Lsoftware/simplicial/a/f/cv;


# direct methods
.method constructor <init>(Lsoftware/simplicial/a/f/cv;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 375
    iput-object p1, p0, Lsoftware/simplicial/a/f/cv$2;->d:Lsoftware/simplicial/a/f/cv;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 376
    iput v0, p0, Lsoftware/simplicial/a/f/cv$2;->a:I

    .line 377
    iput v0, p0, Lsoftware/simplicial/a/f/cv$2;->b:I

    .line 379
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/f/cv$2;->c:Lsoftware/simplicial/a/f/bn;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 384
    .line 385
    iget-object v3, p0, Lsoftware/simplicial/a/f/cv$2;->d:Lsoftware/simplicial/a/f/cv;

    monitor-enter v3

    .line 389
    :try_start_0
    iget v2, p0, Lsoftware/simplicial/a/f/cv$2;->a:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lsoftware/simplicial/a/f/cv$2;->a:I

    .line 392
    iget-object v2, p0, Lsoftware/simplicial/a/f/cv$2;->d:Lsoftware/simplicial/a/f/cv;

    invoke-static {v2}, Lsoftware/simplicial/a/f/cv;->z(Lsoftware/simplicial/a/f/cv;)I

    move-result v2

    .line 393
    iget-object v4, p0, Lsoftware/simplicial/a/f/cv$2;->c:Lsoftware/simplicial/a/f/bn;

    iget-object v5, p0, Lsoftware/simplicial/a/f/cv$2;->d:Lsoftware/simplicial/a/f/cv;

    invoke-static {v5}, Lsoftware/simplicial/a/f/cv;->A(Lsoftware/simplicial/a/f/cv;)I

    move-result v5

    iget-object v6, p0, Lsoftware/simplicial/a/f/cv$2;->d:Lsoftware/simplicial/a/f/cv;

    invoke-static {v6}, Lsoftware/simplicial/a/f/cv;->B(Lsoftware/simplicial/a/f/cv;)I

    move-result v6

    invoke-static {v4, v5, v6, v2}, Lsoftware/simplicial/a/f/bd;->a(Lsoftware/simplicial/a/f/bn;III)[B

    move-result-object v2

    .line 394
    iget-object v4, p0, Lsoftware/simplicial/a/f/cv$2;->c:Lsoftware/simplicial/a/f/bn;

    invoke-virtual {v4}, Lsoftware/simplicial/a/f/bn;->b()I

    move-result v4

    .line 395
    new-instance v5, Ljava/net/DatagramPacket;

    invoke-direct {v5, v2, v4}, Ljava/net/DatagramPacket;-><init>([BI)V

    .line 396
    iget-object v2, p0, Lsoftware/simplicial/a/f/cv$2;->d:Lsoftware/simplicial/a/f/cv;

    invoke-static {v2}, Lsoftware/simplicial/a/f/cv;->u(Lsoftware/simplicial/a/f/cv;)Ljava/net/DatagramSocket;

    move-result-object v2

    .line 397
    iget v4, p0, Lsoftware/simplicial/a/f/cv$2;->a:I

    rem-int/lit8 v4, v4, 0x2

    if-ne v4, v0, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/net/DatagramSocket;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 399
    invoke-virtual {v2, v5}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V

    move v1, v0

    .line 410
    :cond_0
    iget-object v2, p0, Lsoftware/simplicial/a/f/cv$2;->d:Lsoftware/simplicial/a/f/cv;

    invoke-static {v2}, Lsoftware/simplicial/a/f/cv;->C(Lsoftware/simplicial/a/f/cv;)Ljava/net/DatagramSocket;

    move-result-object v2

    .line 411
    if-nez v2, :cond_1

    .line 412
    iget-object v2, p0, Lsoftware/simplicial/a/f/cv$2;->d:Lsoftware/simplicial/a/f/cv;

    invoke-static {v2}, Lsoftware/simplicial/a/f/cv;->u(Lsoftware/simplicial/a/f/cv;)Ljava/net/DatagramSocket;

    move-result-object v2

    .line 413
    :cond_1
    iget v4, p0, Lsoftware/simplicial/a/f/cv$2;->a:I

    rem-int/lit8 v4, v4, 0x2

    if-nez v4, :cond_6

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/net/DatagramSocket;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 415
    invoke-virtual {v2, v5}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V

    .line 427
    :goto_0
    if-eqz v0, :cond_3

    .line 428
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/a/f/cv$2;->b:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 451
    :goto_1
    :try_start_1
    iget v0, p0, Lsoftware/simplicial/a/f/cv$2;->b:I

    const/16 v1, 0xa

    if-le v0, v1, :cond_2

    .line 453
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv$2;->d:Lsoftware/simplicial/a/f/cv;

    invoke-virtual {v0}, Lsoftware/simplicial/a/f/cv;->f()V

    .line 455
    :cond_2
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 456
    return-void

    .line 430
    :cond_3
    :try_start_2
    iget v0, p0, Lsoftware/simplicial/a/f/cv$2;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsoftware/simplicial/a/f/cv$2;->b:I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 432
    :catch_0
    move-exception v0

    .line 434
    :try_start_3
    iget v1, p0, Lsoftware/simplicial/a/f/cv$2;->b:I

    if-nez v1, :cond_5

    .line 436
    iget-object v1, p0, Lsoftware/simplicial/a/f/cv$2;->d:Lsoftware/simplicial/a/f/cv;

    invoke-static {v1}, Lsoftware/simplicial/a/f/cv;->u(Lsoftware/simplicial/a/f/cv;)Ljava/net/DatagramSocket;

    move-result-object v1

    .line 437
    if-eqz v1, :cond_4

    .line 438
    invoke-virtual {v1}, Ljava/net/DatagramSocket;->close()V

    .line 439
    :cond_4
    iget-object v1, p0, Lsoftware/simplicial/a/f/cv$2;->d:Lsoftware/simplicial/a/f/cv;

    invoke-static {v1}, Lsoftware/simplicial/a/f/cv;->C(Lsoftware/simplicial/a/f/cv;)Ljava/net/DatagramSocket;

    move-result-object v1

    .line 440
    if-eqz v1, :cond_5

    .line 441
    invoke-virtual {v1}, Ljava/net/DatagramSocket;->close()V

    .line 443
    :cond_5
    iget v1, p0, Lsoftware/simplicial/a/f/cv$2;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lsoftware/simplicial/a/f/cv$2;->b:I

    .line 444
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v2, "FAILED TO SEND KEEP ALIVE"

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 455
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 446
    :catch_1
    move-exception v0

    .line 448
    :try_start_4
    iget v0, p0, Lsoftware/simplicial/a/f/cv$2;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsoftware/simplicial/a/f/cv$2;->b:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_0
.end method
