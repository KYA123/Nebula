.class public Lsoftware/simplicial/a/f/e;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/b/g;",
            ">;"
        }
    .end annotation
.end field

.field public b:I

.field public c:Lsoftware/simplicial/a/b/b;

.field public d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lsoftware/simplicial/a/f/bh;->Z:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/f/e;->a:Ljava/util/List;

    .line 27
    return-void
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 64
    :try_start_0
    new-instance v5, Lsoftware/simplicial/a/f/bm;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    iget-object v3, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v5, v2}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 66
    invoke-virtual {v5}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v2

    iput v2, p0, Lsoftware/simplicial/a/f/e;->b:I

    .line 67
    invoke-virtual {v5}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 68
    and-int/lit8 v2, v3, 0x1

    ushr-int/lit8 v2, v2, 0x0

    int-to-byte v2, v2

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_0
    iput-boolean v2, p0, Lsoftware/simplicial/a/f/e;->d:Z

    .line 69
    and-int/lit16 v2, v3, 0xfe

    ushr-int/lit8 v2, v2, 0x1

    int-to-byte v2, v2

    .line 70
    if-ltz v2, :cond_0

    sget-object v3, Lsoftware/simplicial/a/b/b;->f:[Lsoftware/simplicial/a/b/b;

    array-length v3, v3

    if-lt v2, v3, :cond_1

    .line 71
    :cond_0
    sget-object v2, Lsoftware/simplicial/a/b/b;->a:Lsoftware/simplicial/a/b/b;

    invoke-virtual {v2}, Lsoftware/simplicial/a/b/b;->ordinal()I

    move-result v2

    int-to-byte v2, v2

    .line 72
    :cond_1
    sget-object v3, Lsoftware/simplicial/a/b/b;->f:[Lsoftware/simplicial/a/b/b;

    aget-object v2, v3, v2

    iput-object v2, p0, Lsoftware/simplicial/a/f/e;->c:Lsoftware/simplicial/a/b/b;

    .line 74
    invoke-virtual {v5}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v6

    .line 75
    iget-object v2, p0, Lsoftware/simplicial/a/f/e;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    move v4, v1

    .line 76
    :goto_1
    if-ge v4, v6, :cond_6

    .line 78
    new-instance v7, Lsoftware/simplicial/a/b/g;

    invoke-direct {v7}, Lsoftware/simplicial/a/b/g;-><init>()V

    .line 80
    invoke-virtual {v5}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v7, Lsoftware/simplicial/a/b/g;->f:Ljava/lang/String;

    .line 81
    invoke-virtual {v5}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v2

    iput v2, v7, Lsoftware/simplicial/a/b/g;->c:I

    .line 83
    invoke-virtual {v5}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 84
    and-int/lit8 v2, v3, 0xf

    ushr-int/lit8 v2, v2, 0x0

    int-to-byte v2, v2

    iput v2, v7, Lsoftware/simplicial/a/b/g;->a:I

    .line 85
    and-int/lit8 v2, v3, 0x30

    ushr-int/lit8 v2, v2, 0x4

    int-to-byte v2, v2

    .line 86
    and-int/lit8 v3, v3, 0x40

    ushr-int/lit8 v3, v3, 0x6

    int-to-byte v3, v3

    if-ne v3, v0, :cond_5

    move v3, v0

    :goto_2
    iput-boolean v3, v7, Lsoftware/simplicial/a/b/g;->g:Z

    .line 88
    if-ltz v2, :cond_2

    sget-object v3, Lsoftware/simplicial/a/b/d;->d:[Lsoftware/simplicial/a/b/d;

    array-length v3, v3

    if-lt v2, v3, :cond_3

    .line 89
    :cond_2
    sget-object v2, Lsoftware/simplicial/a/b/d;->a:Lsoftware/simplicial/a/b/d;

    invoke-virtual {v2}, Lsoftware/simplicial/a/b/d;->ordinal()I

    move-result v2

    int-to-byte v2, v2

    .line 90
    :cond_3
    sget-object v3, Lsoftware/simplicial/a/b/d;->d:[Lsoftware/simplicial/a/b/d;

    aget-object v2, v3, v2

    iput-object v2, v7, Lsoftware/simplicial/a/b/g;->e:Lsoftware/simplicial/a/b/d;

    .line 92
    iget-object v2, v7, Lsoftware/simplicial/a/b/g;->e:Lsoftware/simplicial/a/b/d;

    invoke-static {v2}, Lsoftware/simplicial/a/b/a;->b(Lsoftware/simplicial/a/b/d;)Lsoftware/simplicial/a/ap;

    move-result-object v2

    iput-object v2, v7, Lsoftware/simplicial/a/b/g;->d:Lsoftware/simplicial/a/ap;

    .line 93
    iget-object v2, v7, Lsoftware/simplicial/a/b/g;->e:Lsoftware/simplicial/a/b/d;

    invoke-static {v2}, Lsoftware/simplicial/a/b/a;->a(Lsoftware/simplicial/a/b/d;)I

    move-result v2

    iput v2, v7, Lsoftware/simplicial/a/b/g;->b:I

    .line 95
    iget-object v2, p0, Lsoftware/simplicial/a/f/e;->a:Ljava/util/List;

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    :cond_4
    move v2, v1

    .line 68
    goto :goto_0

    :cond_5
    move v3, v1

    .line 86
    goto :goto_2

    .line 99
    :catch_0
    move-exception v0

    .line 101
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception occurred parsing "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/a/f/e;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    move v0, v1

    .line 105
    :cond_6
    return v0
.end method
