.class public Lsoftware/simplicial/a/f/f;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:Lsoftware/simplicial/a/b/d;

.field public b:Z

.field public c:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lsoftware/simplicial/a/f/bh;->ag:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 21
    return-void
.end method

.method public static a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/b/d;ZZ)[B
    .locals 4

    .prologue
    .line 27
    :try_start_0
    sget-object v0, Lsoftware/simplicial/a/f/bh;->ag:Lsoftware/simplicial/a/f/bh;

    invoke-static {p0, v0, p1}, Lsoftware/simplicial/a/f/f;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;I)V

    .line 28
    invoke-virtual {p2}, Lsoftware/simplicial/a/b/d;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 29
    invoke-virtual {p0, p3}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 30
    invoke-virtual {p0, p4}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v0

    :goto_0
    return-object v0

    .line 32
    :catch_0
    move-exception v0

    .line 34
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred writing data.\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 35
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 4

    .prologue
    .line 45
    :try_start_0
    iget v0, p1, Lsoftware/simplicial/a/f/bi;->b:I

    iput v0, p0, Lsoftware/simplicial/a/f/f;->ar:I

    .line 46
    new-instance v1, Lsoftware/simplicial/a/f/bm;

    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v2, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v0, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v0}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 48
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v0

    .line 49
    if-ltz v0, :cond_0

    sget-object v2, Lsoftware/simplicial/a/b/d;->d:[Lsoftware/simplicial/a/b/d;

    array-length v2, v2

    if-lt v0, v2, :cond_1

    .line 50
    :cond_0
    sget-object v0, Lsoftware/simplicial/a/b/d;->a:Lsoftware/simplicial/a/b/d;

    invoke-virtual {v0}, Lsoftware/simplicial/a/b/d;->ordinal()I

    move-result v0

    .line 51
    :cond_1
    sget-object v2, Lsoftware/simplicial/a/b/d;->d:[Lsoftware/simplicial/a/b/d;

    aget-object v0, v2, v0

    iput-object v0, p0, Lsoftware/simplicial/a/f/f;->a:Lsoftware/simplicial/a/b/d;

    .line 52
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/a/f/f;->c:Z

    .line 53
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/a/f/f;->b:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 55
    :catch_0
    move-exception v0

    .line 57
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred parsing "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/a/f/f;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 58
    const/4 v0, 0x0

    goto :goto_0
.end method
