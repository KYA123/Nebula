.class public Lsoftware/simplicial/a/f/l;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:Lsoftware/simplicial/a/am;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lsoftware/simplicial/a/f/bh;->aQ:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 19
    return-void
.end method

.method public static a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/am;)[B
    .locals 4

    .prologue
    .line 25
    :try_start_0
    sget-object v0, Lsoftware/simplicial/a/f/bh;->aQ:Lsoftware/simplicial/a/f/bh;

    invoke-static {p0, v0, p1}, Lsoftware/simplicial/a/f/l;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;I)V

    .line 26
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lsoftware/simplicial/a/am;->ordinal()I

    move-result v0

    :goto_0
    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 34
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v0

    :goto_1
    return-object v0

    .line 26
    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    .line 28
    :catch_0
    move-exception v0

    .line 30
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred writing data.\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 31
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 4

    .prologue
    .line 57
    :try_start_0
    iget v0, p1, Lsoftware/simplicial/a/f/bi;->b:I

    iput v0, p0, Lsoftware/simplicial/a/f/l;->ar:I

    .line 58
    new-instance v0, Lsoftware/simplicial/a/f/bm;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 60
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v0

    invoke-static {v0}, Lsoftware/simplicial/a/am;->a(B)Lsoftware/simplicial/a/am;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/a/f/l;->a:Lsoftware/simplicial/a/am;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 62
    :catch_0
    move-exception v0

    .line 64
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred parsing "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/a/f/l;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 65
    const/4 v0, 0x0

    goto :goto_0
.end method
