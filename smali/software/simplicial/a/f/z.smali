.class public final enum Lsoftware/simplicial/a/f/z;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/f/z;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/f/z;

.field public static final enum b:Lsoftware/simplicial/a/f/z;

.field public static final enum c:Lsoftware/simplicial/a/f/z;

.field public static final enum d:Lsoftware/simplicial/a/f/z;

.field public static final e:[Lsoftware/simplicial/a/f/z;

.field private static final synthetic f:[Lsoftware/simplicial/a/f/z;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 8
    new-instance v0, Lsoftware/simplicial/a/f/z;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/f/z;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/f/z;->a:Lsoftware/simplicial/a/f/z;

    new-instance v0, Lsoftware/simplicial/a/f/z;

    const-string v1, "GAME_NOT_FOUND"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/f/z;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/f/z;->b:Lsoftware/simplicial/a/f/z;

    new-instance v0, Lsoftware/simplicial/a/f/z;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/f/z;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/f/z;->c:Lsoftware/simplicial/a/f/z;

    new-instance v0, Lsoftware/simplicial/a/f/z;

    const-string v1, "ACCOUNT_ALREADY_SIGNED_IN"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/a/f/z;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/f/z;->d:Lsoftware/simplicial/a/f/z;

    .line 6
    const/4 v0, 0x4

    new-array v0, v0, [Lsoftware/simplicial/a/f/z;

    sget-object v1, Lsoftware/simplicial/a/f/z;->a:Lsoftware/simplicial/a/f/z;

    aput-object v1, v0, v2

    sget-object v1, Lsoftware/simplicial/a/f/z;->b:Lsoftware/simplicial/a/f/z;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/f/z;->c:Lsoftware/simplicial/a/f/z;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/a/f/z;->d:Lsoftware/simplicial/a/f/z;

    aput-object v1, v0, v5

    sput-object v0, Lsoftware/simplicial/a/f/z;->f:[Lsoftware/simplicial/a/f/z;

    .line 9
    invoke-static {}, Lsoftware/simplicial/a/f/z;->values()[Lsoftware/simplicial/a/f/z;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/a/f/z;->e:[Lsoftware/simplicial/a/f/z;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/f/z;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lsoftware/simplicial/a/f/z;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/f/z;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/f/z;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lsoftware/simplicial/a/f/z;->f:[Lsoftware/simplicial/a/f/z;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/f/z;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/f/z;

    return-object v0
.end method
