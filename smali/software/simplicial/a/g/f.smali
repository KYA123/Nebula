.class public final enum Lsoftware/simplicial/a/g/f;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/g/f;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/g/f;

.field public static final enum b:Lsoftware/simplicial/a/g/f;

.field public static final enum c:Lsoftware/simplicial/a/g/f;

.field private static final synthetic d:[Lsoftware/simplicial/a/g/f;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5
    new-instance v0, Lsoftware/simplicial/a/g/f;

    const-string v1, "DAILY"

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/g/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/g/f;->a:Lsoftware/simplicial/a/g/f;

    new-instance v0, Lsoftware/simplicial/a/g/f;

    const-string v1, "WEEKLY"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/g/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/g/f;->b:Lsoftware/simplicial/a/g/f;

    new-instance v0, Lsoftware/simplicial/a/g/f;

    const-string v1, "ALLTIME"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/g/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/g/f;->c:Lsoftware/simplicial/a/g/f;

    .line 3
    const/4 v0, 0x3

    new-array v0, v0, [Lsoftware/simplicial/a/g/f;

    sget-object v1, Lsoftware/simplicial/a/g/f;->a:Lsoftware/simplicial/a/g/f;

    aput-object v1, v0, v2

    sget-object v1, Lsoftware/simplicial/a/g/f;->b:Lsoftware/simplicial/a/g/f;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/g/f;->c:Lsoftware/simplicial/a/g/f;

    aput-object v1, v0, v4

    sput-object v0, Lsoftware/simplicial/a/g/f;->d:[Lsoftware/simplicial/a/g/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/g/f;
    .locals 1

    .prologue
    .line 3
    const-class v0, Lsoftware/simplicial/a/g/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/g/f;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/g/f;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lsoftware/simplicial/a/g/f;->d:[Lsoftware/simplicial/a/g/f;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/g/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/g/f;

    return-object v0
.end method
