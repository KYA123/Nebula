.class public final enum Lsoftware/simplicial/a/h/f;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/h/f;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/h/f;

.field public static final enum b:Lsoftware/simplicial/a/h/f;

.field public static final c:[Lsoftware/simplicial/a/h/f;

.field private static final synthetic d:[Lsoftware/simplicial/a/h/f;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 8
    new-instance v0, Lsoftware/simplicial/a/h/f;

    const-string v1, "TWO_V_TWO"

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/h/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/h/f;->a:Lsoftware/simplicial/a/h/f;

    new-instance v0, Lsoftware/simplicial/a/h/f;

    const-string v1, "THREE_V_THREE"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/h/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/h/f;->b:Lsoftware/simplicial/a/h/f;

    .line 6
    const/4 v0, 0x2

    new-array v0, v0, [Lsoftware/simplicial/a/h/f;

    sget-object v1, Lsoftware/simplicial/a/h/f;->a:Lsoftware/simplicial/a/h/f;

    aput-object v1, v0, v2

    sget-object v1, Lsoftware/simplicial/a/h/f;->b:Lsoftware/simplicial/a/h/f;

    aput-object v1, v0, v3

    sput-object v0, Lsoftware/simplicial/a/h/f;->d:[Lsoftware/simplicial/a/h/f;

    .line 9
    invoke-static {}, Lsoftware/simplicial/a/h/f;->values()[Lsoftware/simplicial/a/h/f;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/a/h/f;->c:[Lsoftware/simplicial/a/h/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/h/f;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lsoftware/simplicial/a/h/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/h/f;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/h/f;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lsoftware/simplicial/a/h/f;->d:[Lsoftware/simplicial/a/h/f;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/h/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/h/f;

    return-object v0
.end method
