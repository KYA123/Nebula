.class public final enum Lsoftware/simplicial/a/i/c;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/i/c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/i/c;

.field public static final enum b:Lsoftware/simplicial/a/i/c;

.field public static final enum c:Lsoftware/simplicial/a/i/c;

.field public static final enum d:Lsoftware/simplicial/a/i/c;

.field public static final e:[Lsoftware/simplicial/a/i/c;

.field private static final synthetic f:[Lsoftware/simplicial/a/i/c;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 8
    new-instance v0, Lsoftware/simplicial/a/i/c;

    const-string v1, "INVALID"

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/i/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/i/c;->a:Lsoftware/simplicial/a/i/c;

    new-instance v0, Lsoftware/simplicial/a/i/c;

    const-string v1, "REGISTER"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/i/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/i/c;->b:Lsoftware/simplicial/a/i/c;

    new-instance v0, Lsoftware/simplicial/a/i/c;

    const-string v1, "UNREGISTER"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/i/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/i/c;->c:Lsoftware/simplicial/a/i/c;

    new-instance v0, Lsoftware/simplicial/a/i/c;

    const-string v1, "JOIN_MY_MATCH"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/a/i/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/i/c;->d:Lsoftware/simplicial/a/i/c;

    .line 6
    const/4 v0, 0x4

    new-array v0, v0, [Lsoftware/simplicial/a/i/c;

    sget-object v1, Lsoftware/simplicial/a/i/c;->a:Lsoftware/simplicial/a/i/c;

    aput-object v1, v0, v2

    sget-object v1, Lsoftware/simplicial/a/i/c;->b:Lsoftware/simplicial/a/i/c;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/i/c;->c:Lsoftware/simplicial/a/i/c;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/a/i/c;->d:Lsoftware/simplicial/a/i/c;

    aput-object v1, v0, v5

    sput-object v0, Lsoftware/simplicial/a/i/c;->f:[Lsoftware/simplicial/a/i/c;

    .line 10
    invoke-static {}, Lsoftware/simplicial/a/i/c;->values()[Lsoftware/simplicial/a/i/c;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/a/i/c;->e:[Lsoftware/simplicial/a/i/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(B)Lsoftware/simplicial/a/i/c;
    .locals 1

    .prologue
    .line 16
    :try_start_0
    sget-object v0, Lsoftware/simplicial/a/i/c;->e:[Lsoftware/simplicial/a/i/c;

    aget-object v0, v0, p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 20
    :goto_0
    return-object v0

    .line 18
    :catch_0
    move-exception v0

    .line 20
    sget-object v0, Lsoftware/simplicial/a/i/c;->a:Lsoftware/simplicial/a/i/c;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/i/c;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lsoftware/simplicial/a/i/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/i/c;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/i/c;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lsoftware/simplicial/a/i/c;->f:[Lsoftware/simplicial/a/i/c;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/i/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/i/c;

    return-object v0
.end method
