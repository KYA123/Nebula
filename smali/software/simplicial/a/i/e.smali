.class public final enum Lsoftware/simplicial/a/i/e;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/i/e;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/i/e;

.field public static final enum b:Lsoftware/simplicial/a/i/e;

.field public static final enum c:Lsoftware/simplicial/a/i/e;

.field public static final enum d:Lsoftware/simplicial/a/i/e;

.field public static final enum e:Lsoftware/simplicial/a/i/e;

.field public static final f:[Lsoftware/simplicial/a/i/e;

.field private static final synthetic g:[Lsoftware/simplicial/a/i/e;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 8
    new-instance v0, Lsoftware/simplicial/a/i/e;

    const-string v1, "INVALID"

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/i/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/i/e;->a:Lsoftware/simplicial/a/i/e;

    new-instance v0, Lsoftware/simplicial/a/i/e;

    const-string v1, "UNREGISTERED"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/i/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/i/e;->b:Lsoftware/simplicial/a/i/e;

    new-instance v0, Lsoftware/simplicial/a/i/e;

    const-string v1, "REGISTERED"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/i/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/i/e;->c:Lsoftware/simplicial/a/i/e;

    new-instance v0, Lsoftware/simplicial/a/i/e;

    const-string v1, "COMPETING"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/a/i/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/i/e;->d:Lsoftware/simplicial/a/i/e;

    new-instance v0, Lsoftware/simplicial/a/i/e;

    const-string v1, "FORMING"

    invoke-direct {v0, v1, v6}, Lsoftware/simplicial/a/i/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/i/e;->e:Lsoftware/simplicial/a/i/e;

    .line 6
    const/4 v0, 0x5

    new-array v0, v0, [Lsoftware/simplicial/a/i/e;

    sget-object v1, Lsoftware/simplicial/a/i/e;->a:Lsoftware/simplicial/a/i/e;

    aput-object v1, v0, v2

    sget-object v1, Lsoftware/simplicial/a/i/e;->b:Lsoftware/simplicial/a/i/e;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/i/e;->c:Lsoftware/simplicial/a/i/e;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/a/i/e;->d:Lsoftware/simplicial/a/i/e;

    aput-object v1, v0, v5

    sget-object v1, Lsoftware/simplicial/a/i/e;->e:Lsoftware/simplicial/a/i/e;

    aput-object v1, v0, v6

    sput-object v0, Lsoftware/simplicial/a/i/e;->g:[Lsoftware/simplicial/a/i/e;

    .line 10
    invoke-static {}, Lsoftware/simplicial/a/i/e;->values()[Lsoftware/simplicial/a/i/e;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/a/i/e;->f:[Lsoftware/simplicial/a/i/e;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(B)Lsoftware/simplicial/a/i/e;
    .locals 1

    .prologue
    .line 16
    :try_start_0
    sget-object v0, Lsoftware/simplicial/a/i/e;->f:[Lsoftware/simplicial/a/i/e;

    aget-object v0, v0, p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 20
    :goto_0
    return-object v0

    .line 18
    :catch_0
    move-exception v0

    .line 20
    sget-object v0, Lsoftware/simplicial/a/i/e;->a:Lsoftware/simplicial/a/i/e;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/i/e;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lsoftware/simplicial/a/i/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/i/e;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/i/e;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lsoftware/simplicial/a/i/e;->g:[Lsoftware/simplicial/a/i/e;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/i/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/i/e;

    return-object v0
.end method
