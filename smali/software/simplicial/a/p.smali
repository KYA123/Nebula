.class public final enum Lsoftware/simplicial/a/p;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/p;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/p;

.field public static final enum b:Lsoftware/simplicial/a/p;

.field public static final enum c:Lsoftware/simplicial/a/p;

.field public static final enum d:Lsoftware/simplicial/a/p;

.field public static final enum e:Lsoftware/simplicial/a/p;

.field public static final enum f:Lsoftware/simplicial/a/p;

.field public static final enum g:Lsoftware/simplicial/a/p;

.field public static final enum h:Lsoftware/simplicial/a/p;

.field public static i:[Lsoftware/simplicial/a/p;

.field private static final synthetic j:[Lsoftware/simplicial/a/p;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 8
    new-instance v0, Lsoftware/simplicial/a/p;

    const-string v1, "TINY"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/p;->a:Lsoftware/simplicial/a/p;

    new-instance v0, Lsoftware/simplicial/a/p;

    const-string v1, "TINY_MAYHEM"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/p;->b:Lsoftware/simplicial/a/p;

    new-instance v0, Lsoftware/simplicial/a/p;

    const-string v1, "SMALL"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/p;->c:Lsoftware/simplicial/a/p;

    new-instance v0, Lsoftware/simplicial/a/p;

    const-string v1, "SMALL_MAYHEM"

    invoke-direct {v0, v1, v6}, Lsoftware/simplicial/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/p;->d:Lsoftware/simplicial/a/p;

    new-instance v0, Lsoftware/simplicial/a/p;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v7}, Lsoftware/simplicial/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/p;->e:Lsoftware/simplicial/a/p;

    new-instance v0, Lsoftware/simplicial/a/p;

    const-string v1, "NORMAL_MAYHEM"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/p;->f:Lsoftware/simplicial/a/p;

    new-instance v0, Lsoftware/simplicial/a/p;

    const-string v1, "LARGE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/p;->g:Lsoftware/simplicial/a/p;

    new-instance v0, Lsoftware/simplicial/a/p;

    const-string v1, "LARGE_MAYHEM"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/p;->h:Lsoftware/simplicial/a/p;

    .line 6
    const/16 v0, 0x8

    new-array v0, v0, [Lsoftware/simplicial/a/p;

    sget-object v1, Lsoftware/simplicial/a/p;->a:Lsoftware/simplicial/a/p;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/p;->b:Lsoftware/simplicial/a/p;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/a/p;->c:Lsoftware/simplicial/a/p;

    aput-object v1, v0, v5

    sget-object v1, Lsoftware/simplicial/a/p;->d:Lsoftware/simplicial/a/p;

    aput-object v1, v0, v6

    sget-object v1, Lsoftware/simplicial/a/p;->e:Lsoftware/simplicial/a/p;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lsoftware/simplicial/a/p;->f:Lsoftware/simplicial/a/p;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lsoftware/simplicial/a/p;->g:Lsoftware/simplicial/a/p;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lsoftware/simplicial/a/p;->h:Lsoftware/simplicial/a/p;

    aput-object v2, v0, v1

    sput-object v0, Lsoftware/simplicial/a/p;->j:[Lsoftware/simplicial/a/p;

    .line 9
    invoke-static {}, Lsoftware/simplicial/a/p;->values()[Lsoftware/simplicial/a/p;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/a/p;->i:[Lsoftware/simplicial/a/p;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(I)Lsoftware/simplicial/a/p;
    .locals 1

    .prologue
    .line 13
    if-ltz p0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/p;->i:[Lsoftware/simplicial/a/p;

    array-length v0, v0

    if-lt p0, v0, :cond_1

    .line 14
    :cond_0
    const/4 v0, 0x0

    .line 15
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lsoftware/simplicial/a/p;->i:[Lsoftware/simplicial/a/p;

    aget-object v0, v0, p0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/p;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lsoftware/simplicial/a/p;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/p;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/p;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lsoftware/simplicial/a/p;->j:[Lsoftware/simplicial/a/p;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/p;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/p;

    return-object v0
.end method
