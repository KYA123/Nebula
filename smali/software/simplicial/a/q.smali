.class public final enum Lsoftware/simplicial/a/q;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/q;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/q;

.field public static final enum b:Lsoftware/simplicial/a/q;

.field public static final enum c:Lsoftware/simplicial/a/q;

.field public static final enum d:Lsoftware/simplicial/a/q;

.field public static final enum e:Lsoftware/simplicial/a/q;

.field public static final enum f:Lsoftware/simplicial/a/q;

.field public static g:[Lsoftware/simplicial/a/q;

.field private static final synthetic h:[Lsoftware/simplicial/a/q;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 8
    new-instance v0, Lsoftware/simplicial/a/q;

    const-string v1, "INVALID"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    new-instance v0, Lsoftware/simplicial/a/q;

    const-string v1, "MEMBER"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/q;->b:Lsoftware/simplicial/a/q;

    new-instance v0, Lsoftware/simplicial/a/q;

    const-string v1, "ADMIN"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/a/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/q;->c:Lsoftware/simplicial/a/q;

    new-instance v0, Lsoftware/simplicial/a/q;

    const-string v1, "LEADER"

    invoke-direct {v0, v1, v6}, Lsoftware/simplicial/a/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/q;->d:Lsoftware/simplicial/a/q;

    new-instance v0, Lsoftware/simplicial/a/q;

    const-string v1, "ELDER"

    invoke-direct {v0, v1, v7}, Lsoftware/simplicial/a/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/q;->e:Lsoftware/simplicial/a/q;

    new-instance v0, Lsoftware/simplicial/a/q;

    const-string v1, "DIAMOND"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/q;->f:Lsoftware/simplicial/a/q;

    .line 6
    const/4 v0, 0x6

    new-array v0, v0, [Lsoftware/simplicial/a/q;

    sget-object v1, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/q;->b:Lsoftware/simplicial/a/q;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/a/q;->c:Lsoftware/simplicial/a/q;

    aput-object v1, v0, v5

    sget-object v1, Lsoftware/simplicial/a/q;->d:Lsoftware/simplicial/a/q;

    aput-object v1, v0, v6

    sget-object v1, Lsoftware/simplicial/a/q;->e:Lsoftware/simplicial/a/q;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lsoftware/simplicial/a/q;->f:Lsoftware/simplicial/a/q;

    aput-object v2, v0, v1

    sput-object v0, Lsoftware/simplicial/a/q;->h:[Lsoftware/simplicial/a/q;

    .line 10
    invoke-static {}, Lsoftware/simplicial/a/q;->values()[Lsoftware/simplicial/a/q;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/a/q;->g:[Lsoftware/simplicial/a/q;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(B)Lsoftware/simplicial/a/q;
    .locals 1

    .prologue
    .line 74
    if-ltz p0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/q;->g:[Lsoftware/simplicial/a/q;

    array-length v0, v0

    if-ge p0, v0, :cond_0

    .line 75
    sget-object v0, Lsoftware/simplicial/a/q;->g:[Lsoftware/simplicial/a/q;

    aget-object v0, v0, p0

    .line 76
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lsoftware/simplicial/a/q;->b:Lsoftware/simplicial/a/q;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Lsoftware/simplicial/a/q;
    .locals 2

    .prologue
    .line 81
    if-nez p0, :cond_0

    .line 82
    sget-object v0, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    .line 98
    :goto_0
    return-object v0

    .line 83
    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 98
    sget-object v0, Lsoftware/simplicial/a/q;->b:Lsoftware/simplicial/a/q;

    goto :goto_0

    .line 83
    :sswitch_0
    const-string v1, "INVALID"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string v1, "MEMBER"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_2
    const-string v1, "ELDER"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_3
    const-string v1, "ADMIN"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_4
    const-string v1, "DIAMOND"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x4

    goto :goto_1

    :sswitch_5
    const-string v1, "LEADER"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x5

    goto :goto_1

    .line 86
    :pswitch_0
    sget-object v0, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    goto :goto_0

    .line 88
    :pswitch_1
    sget-object v0, Lsoftware/simplicial/a/q;->b:Lsoftware/simplicial/a/q;

    goto :goto_0

    .line 90
    :pswitch_2
    sget-object v0, Lsoftware/simplicial/a/q;->e:Lsoftware/simplicial/a/q;

    goto :goto_0

    .line 92
    :pswitch_3
    sget-object v0, Lsoftware/simplicial/a/q;->c:Lsoftware/simplicial/a/q;

    goto :goto_0

    .line 94
    :pswitch_4
    sget-object v0, Lsoftware/simplicial/a/q;->f:Lsoftware/simplicial/a/q;

    goto :goto_0

    .line 96
    :pswitch_5
    sget-object v0, Lsoftware/simplicial/a/q;->d:Lsoftware/simplicial/a/q;

    goto :goto_0

    .line 83
    nop

    :sswitch_data_0
    .sparse-switch
        -0x7a64c6f7 -> :sswitch_5
        -0x78aa8166 -> :sswitch_1
        -0x728e52cc -> :sswitch_4
        -0x60648229 -> :sswitch_0
        0x3b40b2f -> :sswitch_3
        0x3efe9ea -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/q;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lsoftware/simplicial/a/q;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/q;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/q;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lsoftware/simplicial/a/q;->h:[Lsoftware/simplicial/a/q;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/q;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/q;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 14
    sget-object v1, Lsoftware/simplicial/a/q$1;->a:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/q;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 29
    :goto_0
    :pswitch_0
    return v0

    .line 17
    :pswitch_1
    const/4 v0, 0x5

    goto :goto_0

    .line 19
    :pswitch_2
    const/4 v0, 0x4

    goto :goto_0

    .line 21
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 23
    :pswitch_4
    const/4 v0, 0x2

    goto :goto_0

    .line 25
    :pswitch_5
    const/4 v0, 0x1

    goto :goto_0

    .line 14
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
    .end packed-switch
.end method

.method public b()Lsoftware/simplicial/a/q;
    .locals 2

    .prologue
    .line 34
    sget-object v0, Lsoftware/simplicial/a/q$1;->a:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/q;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 49
    sget-object v0, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    :goto_0
    return-object v0

    .line 37
    :pswitch_0
    sget-object v0, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    goto :goto_0

    .line 39
    :pswitch_1
    sget-object v0, Lsoftware/simplicial/a/q;->e:Lsoftware/simplicial/a/q;

    goto :goto_0

    .line 41
    :pswitch_2
    sget-object v0, Lsoftware/simplicial/a/q;->c:Lsoftware/simplicial/a/q;

    goto :goto_0

    .line 43
    :pswitch_3
    sget-object v0, Lsoftware/simplicial/a/q;->f:Lsoftware/simplicial/a/q;

    goto :goto_0

    .line 45
    :pswitch_4
    sget-object v0, Lsoftware/simplicial/a/q;->d:Lsoftware/simplicial/a/q;

    goto :goto_0

    .line 47
    :pswitch_5
    sget-object v0, Lsoftware/simplicial/a/q;->d:Lsoftware/simplicial/a/q;

    goto :goto_0

    .line 34
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public c()Lsoftware/simplicial/a/q;
    .locals 2

    .prologue
    .line 54
    sget-object v0, Lsoftware/simplicial/a/q$1;->a:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/q;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 69
    sget-object v0, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    :goto_0
    return-object v0

    .line 57
    :pswitch_0
    sget-object v0, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    goto :goto_0

    .line 59
    :pswitch_1
    sget-object v0, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    goto :goto_0

    .line 61
    :pswitch_2
    sget-object v0, Lsoftware/simplicial/a/q;->b:Lsoftware/simplicial/a/q;

    goto :goto_0

    .line 63
    :pswitch_3
    sget-object v0, Lsoftware/simplicial/a/q;->e:Lsoftware/simplicial/a/q;

    goto :goto_0

    .line 65
    :pswitch_4
    sget-object v0, Lsoftware/simplicial/a/q;->c:Lsoftware/simplicial/a/q;

    goto :goto_0

    .line 67
    :pswitch_5
    sget-object v0, Lsoftware/simplicial/a/q;->f:Lsoftware/simplicial/a/q;

    goto :goto_0

    .line 54
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
