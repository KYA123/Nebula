.class public final enum Lsoftware/simplicial/a/t$a;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsoftware/simplicial/a/t;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/t$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/t$a;

.field public static final enum b:Lsoftware/simplicial/a/t$a;

.field public static final enum c:Lsoftware/simplicial/a/t$a;

.field public static final enum d:Lsoftware/simplicial/a/t$a;

.field public static final enum e:Lsoftware/simplicial/a/t$a;

.field public static final enum f:Lsoftware/simplicial/a/t$a;

.field public static final enum g:Lsoftware/simplicial/a/t$a;

.field public static final enum h:Lsoftware/simplicial/a/t$a;

.field public static final enum i:Lsoftware/simplicial/a/t$a;

.field public static final enum j:Lsoftware/simplicial/a/t$a;

.field private static final synthetic k:[Lsoftware/simplicial/a/t$a;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1829
    new-instance v0, Lsoftware/simplicial/a/t$a;

    const-string v1, "DISCONNECTED"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/t$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/t$a;->a:Lsoftware/simplicial/a/t$a;

    new-instance v0, Lsoftware/simplicial/a/t$a;

    const-string v1, "RECONNECTING"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/t$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/t$a;->b:Lsoftware/simplicial/a/t$a;

    new-instance v0, Lsoftware/simplicial/a/t$a;

    const-string v1, "CONNECTING_LOBBIES"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/a/t$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/t$a;->c:Lsoftware/simplicial/a/t$a;

    new-instance v0, Lsoftware/simplicial/a/t$a;

    const-string v1, "CONNECTING_GAME"

    invoke-direct {v0, v1, v6}, Lsoftware/simplicial/a/t$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/t$a;->d:Lsoftware/simplicial/a/t$a;

    new-instance v0, Lsoftware/simplicial/a/t$a;

    const-string v1, "CONNECTED_LOBBIES"

    invoke-direct {v0, v1, v7}, Lsoftware/simplicial/a/t$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/t$a;->e:Lsoftware/simplicial/a/t$a;

    new-instance v0, Lsoftware/simplicial/a/t$a;

    const-string v1, "CONNECTED_GAME"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/t$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/t$a;->f:Lsoftware/simplicial/a/t$a;

    new-instance v0, Lsoftware/simplicial/a/t$a;

    const-string v1, "JOINING_GAME"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/t$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/t$a;->g:Lsoftware/simplicial/a/t$a;

    new-instance v0, Lsoftware/simplicial/a/t$a;

    const-string v1, "JOINED_GAME"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/t$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/t$a;->h:Lsoftware/simplicial/a/t$a;

    new-instance v0, Lsoftware/simplicial/a/t$a;

    const-string v1, "JOINING_LOBBY"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/t$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/t$a;->i:Lsoftware/simplicial/a/t$a;

    new-instance v0, Lsoftware/simplicial/a/t$a;

    const-string v1, "JOINED_LOBBY"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/t$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/t$a;->j:Lsoftware/simplicial/a/t$a;

    .line 1827
    const/16 v0, 0xa

    new-array v0, v0, [Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->a:Lsoftware/simplicial/a/t$a;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/t$a;->b:Lsoftware/simplicial/a/t$a;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/a/t$a;->c:Lsoftware/simplicial/a/t$a;

    aput-object v1, v0, v5

    sget-object v1, Lsoftware/simplicial/a/t$a;->d:Lsoftware/simplicial/a/t$a;

    aput-object v1, v0, v6

    sget-object v1, Lsoftware/simplicial/a/t$a;->e:Lsoftware/simplicial/a/t$a;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lsoftware/simplicial/a/t$a;->f:Lsoftware/simplicial/a/t$a;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lsoftware/simplicial/a/t$a;->g:Lsoftware/simplicial/a/t$a;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lsoftware/simplicial/a/t$a;->h:Lsoftware/simplicial/a/t$a;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lsoftware/simplicial/a/t$a;->i:Lsoftware/simplicial/a/t$a;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lsoftware/simplicial/a/t$a;->j:Lsoftware/simplicial/a/t$a;

    aput-object v2, v0, v1

    sput-object v0, Lsoftware/simplicial/a/t$a;->k:[Lsoftware/simplicial/a/t$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1827
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/t$a;
    .locals 1

    .prologue
    .line 1827
    const-class v0, Lsoftware/simplicial/a/t$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/t$a;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/t$a;
    .locals 1

    .prologue
    .line 1827
    sget-object v0, Lsoftware/simplicial/a/t$a;->k:[Lsoftware/simplicial/a/t$a;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/t$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/t$a;

    return-object v0
.end method
