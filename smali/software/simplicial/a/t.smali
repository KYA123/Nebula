.class public Lsoftware/simplicial/a/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;
.implements Lsoftware/simplicial/a/f/cw;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/a/t$a;
    }
.end annotation


# instance fields
.field public A:I

.field public B:I

.field public C:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lsoftware/simplicial/a/f/bi;",
            ">;"
        }
    .end annotation
.end field

.field public D:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lsoftware/simplicial/a/f/be;",
            ">;"
        }
    .end annotation
.end field

.field private E:Lsoftware/simplicial/a/u;

.field private F:Lsoftware/simplicial/a/al;

.field private G:Lsoftware/simplicial/a/f/cv;

.field private H:Lsoftware/simplicial/a/f/bg;

.field private I:Lsoftware/simplicial/a/t$a;

.field private J:Ljava/util/concurrent/atomic/AtomicLong;

.field private K:Ljava/lang/Thread;

.field private L:Z

.field private M:J

.field private N:Lsoftware/simplicial/a/bj;

.field private O:Z

.field private P:Z

.field private Q:Z

.field private R:J

.field private S:[B

.field private T:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lsoftware/simplicial/a/f/ak;",
            ">;"
        }
    .end annotation
.end field

.field private U:J

.field public final a:Ljava/lang/Object;

.field public b:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lsoftware/simplicial/a/w;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lsoftware/simplicial/a/m;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lsoftware/simplicial/a/c/d;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lsoftware/simplicial/a/b/c;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lsoftware/simplicial/a/j;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lsoftware/simplicial/a/at;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lsoftware/simplicial/a/h/e;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lsoftware/simplicial/a/c;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lsoftware/simplicial/a/an;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lsoftware/simplicial/a/av;",
            ">;"
        }
    .end annotation
.end field

.field public l:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lsoftware/simplicial/a/ar;",
            ">;"
        }
    .end annotation
.end field

.field public m:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lsoftware/simplicial/a/ah;",
            ">;"
        }
    .end annotation
.end field

.field public n:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lsoftware/simplicial/a/i/f;",
            ">;"
        }
    .end annotation
.end field

.field public o:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lsoftware/simplicial/a/o;",
            ">;"
        }
    .end annotation
.end field

.field public p:J

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;

.field public s:I

.field public t:Z

.field public u:F

.field public v:Lsoftware/simplicial/a/c/h;

.field public w:I

.field public x:I

.field public y:Lsoftware/simplicial/a/i/e;

.field public z:Lsoftware/simplicial/a/h/a$a;


# direct methods
.method public constructor <init>(Lsoftware/simplicial/a/al;)V
    .locals 13

    .prologue
    const/4 v4, -0x1

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/t;->a:Ljava/lang/Object;

    .line 121
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/a/t;->b:Ljava/util/Collection;

    .line 122
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/a/t;->c:Ljava/util/Collection;

    .line 123
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/a/t;->d:Ljava/util/Collection;

    .line 124
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/a/t;->e:Ljava/util/Collection;

    .line 125
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/a/t;->f:Ljava/util/Collection;

    .line 126
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/a/t;->g:Ljava/util/Collection;

    .line 127
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/a/t;->h:Ljava/util/Collection;

    .line 128
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/a/t;->i:Ljava/util/Collection;

    .line 129
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/a/t;->j:Ljava/util/Collection;

    .line 130
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/a/t;->k:Ljava/util/Collection;

    .line 131
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/a/t;->l:Ljava/util/Collection;

    .line 132
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/a/t;->m:Ljava/util/Collection;

    .line 133
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/a/t;->n:Ljava/util/Collection;

    .line 134
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/a/t;->o:Ljava/util/Collection;

    .line 135
    iput-wide v2, p0, Lsoftware/simplicial/a/t;->p:J

    .line 136
    const-string v0, ""

    iput-object v0, p0, Lsoftware/simplicial/a/t;->q:Ljava/lang/String;

    .line 137
    const-string v0, ""

    iput-object v0, p0, Lsoftware/simplicial/a/t;->r:Ljava/lang/String;

    .line 138
    iput v4, p0, Lsoftware/simplicial/a/t;->s:I

    .line 139
    iput-boolean v1, p0, Lsoftware/simplicial/a/t;->t:Z

    .line 140
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lsoftware/simplicial/a/t;->u:F

    .line 141
    sget-object v0, Lsoftware/simplicial/a/c/h;->a:Lsoftware/simplicial/a/c/h;

    iput-object v0, p0, Lsoftware/simplicial/a/t;->v:Lsoftware/simplicial/a/c/h;

    .line 142
    iput v1, p0, Lsoftware/simplicial/a/t;->w:I

    .line 143
    iput v1, p0, Lsoftware/simplicial/a/t;->x:I

    .line 144
    sget-object v0, Lsoftware/simplicial/a/i/e;->a:Lsoftware/simplicial/a/i/e;

    iput-object v0, p0, Lsoftware/simplicial/a/t;->y:Lsoftware/simplicial/a/i/e;

    .line 145
    sget-object v0, Lsoftware/simplicial/a/h/a$a;->a:Lsoftware/simplicial/a/h/a$a;

    iput-object v0, p0, Lsoftware/simplicial/a/t;->z:Lsoftware/simplicial/a/h/a$a;

    .line 146
    iput v1, p0, Lsoftware/simplicial/a/t;->A:I

    .line 147
    iput v1, p0, Lsoftware/simplicial/a/t;->B:I

    .line 152
    new-instance v0, Lsoftware/simplicial/a/f/cv;

    invoke-direct {v0, p0}, Lsoftware/simplicial/a/f/cv;-><init>(Lsoftware/simplicial/a/f/cw;)V

    iput-object v0, p0, Lsoftware/simplicial/a/t;->G:Lsoftware/simplicial/a/f/cv;

    .line 153
    iget-object v0, p0, Lsoftware/simplicial/a/t;->G:Lsoftware/simplicial/a/f/cv;

    iput-object v0, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    .line 154
    sget-object v0, Lsoftware/simplicial/a/t$a;->a:Lsoftware/simplicial/a/t$a;

    iput-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    .line 155
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lsoftware/simplicial/a/t;->J:Ljava/util/concurrent/atomic/AtomicLong;

    .line 157
    iput-boolean v1, p0, Lsoftware/simplicial/a/t;->L:Z

    .line 158
    iput-wide v2, p0, Lsoftware/simplicial/a/t;->M:J

    .line 159
    sget-object v0, Lsoftware/simplicial/a/bj;->b:Lsoftware/simplicial/a/bj;

    iput-object v0, p0, Lsoftware/simplicial/a/t;->N:Lsoftware/simplicial/a/bj;

    .line 160
    iput-boolean v1, p0, Lsoftware/simplicial/a/t;->O:Z

    .line 161
    iput-boolean v1, p0, Lsoftware/simplicial/a/t;->P:Z

    .line 162
    iput-boolean v1, p0, Lsoftware/simplicial/a/t;->Q:Z

    .line 163
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lsoftware/simplicial/a/t;->R:J

    .line 164
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/a/t;->S:[B

    .line 166
    iput-wide v2, p0, Lsoftware/simplicial/a/t;->U:J

    .line 170
    iput-object p1, p0, Lsoftware/simplicial/a/t;->F:Lsoftware/simplicial/a/al;

    .line 172
    iget-object v12, p0, Lsoftware/simplicial/a/t;->a:Ljava/lang/Object;

    monitor-enter v12

    .line 174
    :try_start_0
    new-instance v0, Lsoftware/simplicial/a/u;

    iget-object v2, p0, Lsoftware/simplicial/a/t;->G:Lsoftware/simplicial/a/f/cv;

    sget-object v3, Lsoftware/simplicial/a/am;->a:Lsoftware/simplicial/a/am;

    const/4 v4, 0x0

    sget-object v5, Lsoftware/simplicial/a/aq;->a:Lsoftware/simplicial/a/aq;

    sget-object v6, Lsoftware/simplicial/a/ap;->a:Lsoftware/simplicial/a/ap;

    const-string v7, "NULL"

    const/4 v1, 0x0

    new-array v8, v1, [B

    const/4 v9, -0x1

    sget-object v10, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    const/4 v11, 0x0

    move-object v1, p1

    invoke-direct/range {v0 .. v11}, Lsoftware/simplicial/a/u;-><init>(Lsoftware/simplicial/a/al;Lsoftware/simplicial/a/f/bg;Lsoftware/simplicial/a/am;ILsoftware/simplicial/a/aq;Lsoftware/simplicial/a/ap;Ljava/lang/String;[BILsoftware/simplicial/a/bj;Z)V

    iput-object v0, p0, Lsoftware/simplicial/a/t;->E:Lsoftware/simplicial/a/u;

    .line 176
    monitor-exit v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 178
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/t;->C:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 179
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/t;->D:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 180
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/t;->T:Ljava/util/Queue;

    .line 181
    return-void

    .line 176
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private declared-synchronized D()V
    .locals 4

    .prologue
    .line 576
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->K:Ljava/lang/Thread;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 591
    :goto_0
    monitor-exit p0

    return-void

    .line 579
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lsoftware/simplicial/a/t;->L:Z

    .line 580
    iget-object v0, p0, Lsoftware/simplicial/a/t;->K:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 584
    :try_start_2
    iget-object v0, p0, Lsoftware/simplicial/a/t;->K:Ljava/lang/Thread;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Ljava/lang/Thread;->join(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 590
    :goto_1
    const/4 v0, 0x0

    :try_start_3
    iput-object v0, p0, Lsoftware/simplicial/a/t;->K:Ljava/lang/Thread;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 576
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 586
    :catch_0
    move-exception v0

    .line 588
    :try_start_4
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method private declared-synchronized a(Ljava/lang/String;ILsoftware/simplicial/a/bg;)V
    .locals 7

    .prologue
    const/4 v5, -0x1

    .line 957
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->a:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->c:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->d:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->b:Lsoftware/simplicial/a/t$a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_1

    .line 983
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 960
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->u()V

    .line 962
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lsoftware/simplicial/a/t;->U:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 964
    iget-object v0, p0, Lsoftware/simplicial/a/t;->k:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/av;

    .line 965
    sget-object v2, Lsoftware/simplicial/a/f/ba;->o:Lsoftware/simplicial/a/f/ba;

    const-wide/16 v4, 0x0

    const/4 v3, -0x1

    invoke-interface {v0, v2, v4, v5, v3}, Lsoftware/simplicial/a/av;->a(Lsoftware/simplicial/a/f/ba;JI)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 957
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 968
    :cond_2
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lsoftware/simplicial/a/t;->U:J

    .line 970
    iput-object p1, p0, Lsoftware/simplicial/a/t;->q:Ljava/lang/String;

    .line 972
    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->f:Lsoftware/simplicial/a/t$a;

    if-ne v0, v1, :cond_3

    .line 973
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/a/t;->O:Z

    .line 976
    :goto_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/a/t;->Q:Z

    .line 978
    sget-object v0, Lsoftware/simplicial/a/bj;->c:Lsoftware/simplicial/a/bj;

    iput-object v0, p0, Lsoftware/simplicial/a/t;->N:Lsoftware/simplicial/a/bj;

    .line 980
    if-nez p3, :cond_4

    .line 981
    :goto_3
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 982
    iget-object v6, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v1

    iget-object v2, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-interface {v2}, Lsoftware/simplicial/a/f/bg;->b()I

    move-result v2

    move v3, p2

    move-object v4, p1

    invoke-static/range {v0 .. v5}, Lsoftware/simplicial/a/f/ae;->a(Lsoftware/simplicial/a/f/bn;IIILjava/lang/String;I)[B

    move-result-object v0

    invoke-interface {v6, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V

    goto :goto_0

    .line 974
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/a/t;->O:Z

    goto :goto_2

    .line 980
    :cond_4
    iget v5, p3, Lsoftware/simplicial/a/bg;->b:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3
.end method

.method private declared-synchronized a(Ljava/lang/String;ILsoftware/simplicial/a/bg;III)V
    .locals 10

    .prologue
    .line 906
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->a:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->c:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->d:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->b:Lsoftware/simplicial/a/t$a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_1

    .line 933
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 909
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->u()V

    .line 911
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lsoftware/simplicial/a/t;->U:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 913
    iget-object v0, p0, Lsoftware/simplicial/a/t;->k:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/av;

    .line 914
    sget-object v2, Lsoftware/simplicial/a/f/ba;->o:Lsoftware/simplicial/a/f/ba;

    const-wide/16 v4, 0x0

    const/4 v3, -0x1

    invoke-interface {v0, v2, v4, v5, v3}, Lsoftware/simplicial/a/av;->a(Lsoftware/simplicial/a/f/ba;JI)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 906
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 917
    :cond_2
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lsoftware/simplicial/a/t;->U:J

    .line 919
    iput-object p1, p0, Lsoftware/simplicial/a/t;->q:Ljava/lang/String;

    .line 921
    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->f:Lsoftware/simplicial/a/t$a;

    if-ne v0, v1, :cond_3

    .line 922
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/a/t;->O:Z

    .line 925
    :goto_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/a/t;->Q:Z

    .line 927
    sget-object v0, Lsoftware/simplicial/a/bj;->c:Lsoftware/simplicial/a/bj;

    iput-object v0, p0, Lsoftware/simplicial/a/t;->N:Lsoftware/simplicial/a/bj;

    .line 929
    if-nez p3, :cond_4

    const/4 v5, -0x1

    .line 930
    :goto_3
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 931
    iget-object v9, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v1

    iget-object v2, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-interface {v2}, Lsoftware/simplicial/a/f/bg;->b()I

    move-result v2

    move v3, p2

    move-object v4, p1

    move v6, p4

    move v7, p5

    move/from16 v8, p6

    invoke-static/range {v0 .. v8}, Lsoftware/simplicial/a/f/ca;->a(Lsoftware/simplicial/a/f/bn;IIILjava/lang/String;IIII)[B

    move-result-object v0

    invoke-interface {v9, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V

    goto :goto_0

    .line 923
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/a/t;->O:Z

    goto :goto_2

    .line 929
    :cond_4
    iget v5, p3, Lsoftware/simplicial/a/bg;->b:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3
.end method

.method private declared-synchronized a(Ljava/lang/String;[B)V
    .locals 2

    .prologue
    .line 1534
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->j:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/an;

    .line 1535
    invoke-interface {v0, p1, p2}, Lsoftware/simplicial/a/an;->a(Ljava/lang/String;[B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1534
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1536
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/am;ILsoftware/simplicial/a/aq;Lsoftware/simplicial/a/ap;Ljava/lang/String;[BILsoftware/simplicial/a/bj;ZZZZZIZLsoftware/simplicial/a/c/g;Z)V
    .locals 29

    .prologue
    .line 1348
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lsoftware/simplicial/a/t;->Q:Z

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lsoftware/simplicial/a/t;->t:Z

    .line 1349
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lsoftware/simplicial/a/t;->Q:Z

    .line 1352
    const/4 v6, 0x0

    .line 1353
    const/4 v5, 0x0

    .line 1354
    const/4 v4, 0x0

    .line 1359
    sget-object v7, Lsoftware/simplicial/a/aa;->a:Lsoftware/simplicial/a/aa;

    .line 1362
    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/t;->a:Ljava/lang/Object;

    move-object/from16 v20, v0

    monitor-enter v20
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1364
    :try_start_1
    move-object/from16 v0, p0

    iget-object v8, v0, Lsoftware/simplicial/a/t;->E:Lsoftware/simplicial/a/u;

    .line 1367
    iget v7, v8, Lsoftware/simplicial/a/u;->aZ:I

    move/from16 v0, p7

    if-ne v0, v7, :cond_0

    const/4 v7, 0x1

    move/from16 v19, v7

    .line 1368
    :goto_0
    if-eqz v19, :cond_8

    .line 1372
    iget-object v4, v8, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    array-length v4, v4

    new-array v6, v4, [Lsoftware/simplicial/a/bf;

    .line 1373
    iget-object v4, v8, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    const/4 v5, 0x0

    const/4 v7, 0x0

    iget-object v9, v8, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    array-length v9, v9

    invoke-static {v4, v5, v6, v7, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1375
    iget-object v4, v8, Lsoftware/simplicial/a/u;->ah:[Lsoftware/simplicial/a/ae;

    array-length v4, v4

    new-array v5, v4, [Lsoftware/simplicial/a/ae;

    .line 1376
    iget-object v4, v8, Lsoftware/simplicial/a/u;->ah:[Lsoftware/simplicial/a/ae;

    const/4 v7, 0x0

    const/4 v9, 0x0

    iget-object v10, v8, Lsoftware/simplicial/a/u;->ah:[Lsoftware/simplicial/a/ae;

    array-length v10, v10

    invoke-static {v4, v7, v5, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1378
    iget-object v4, v8, Lsoftware/simplicial/a/u;->ag:[Lsoftware/simplicial/a/bt;

    array-length v4, v4

    new-array v4, v4, [Lsoftware/simplicial/a/bt;

    .line 1379
    iget-object v7, v8, Lsoftware/simplicial/a/u;->ag:[Lsoftware/simplicial/a/bt;

    const/4 v9, 0x0

    const/4 v10, 0x0

    iget-object v11, v8, Lsoftware/simplicial/a/u;->ag:[Lsoftware/simplicial/a/bt;

    array-length v11, v11

    invoke-static {v7, v9, v4, v10, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object/from16 v16, v4

    move-object/from16 v17, v5

    move-object/from16 v18, v6

    .line 1381
    :goto_1
    iget-wide v0, v8, Lsoftware/simplicial/a/u;->A:J

    move-wide/from16 v22, v0

    .line 1382
    iget v0, v8, Lsoftware/simplicial/a/u;->B:I

    move/from16 v21, v0

    .line 1383
    iget-wide v0, v8, Lsoftware/simplicial/a/u;->C:J

    move-wide/from16 v24, v0

    .line 1384
    iget-boolean v0, v8, Lsoftware/simplicial/a/u;->D:Z

    move/from16 v26, v0

    .line 1385
    iget-object v0, v8, Lsoftware/simplicial/a/u;->m:Lsoftware/simplicial/a/aa;

    move-object/from16 v27, v0

    .line 1386
    iget-boolean v0, v8, Lsoftware/simplicial/a/u;->o:Z

    move/from16 v28, v0

    .line 1388
    new-instance v4, Lsoftware/simplicial/a/u;

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/t;->F:Lsoftware/simplicial/a/al;

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    move-object/from16 v7, p1

    move/from16 v8, p2

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    move-object/from16 v12, p6

    move/from16 v13, p7

    move-object/from16 v14, p8

    move/from16 v15, p9

    invoke-direct/range {v4 .. v15}, Lsoftware/simplicial/a/u;-><init>(Lsoftware/simplicial/a/al;Lsoftware/simplicial/a/f/bg;Lsoftware/simplicial/a/am;ILsoftware/simplicial/a/aq;Lsoftware/simplicial/a/ap;Ljava/lang/String;[BILsoftware/simplicial/a/bj;Z)V

    .line 1390
    if-eqz v19, :cond_2

    .line 1392
    const/4 v5, 0x0

    iget-object v6, v4, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    const/4 v7, 0x0

    iget-object v8, v4, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    array-length v8, v8

    move-object/from16 v0, v18

    array-length v9, v0

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    move-object/from16 v0, v18

    invoke-static {v0, v5, v6, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1393
    const/4 v5, 0x0

    :goto_2
    iget-object v6, v4, Lsoftware/simplicial/a/u;->ah:[Lsoftware/simplicial/a/ae;

    array-length v6, v6

    move-object/from16 v0, v17

    array-length v7, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    if-ge v5, v6, :cond_1

    .line 1394
    iget-object v6, v4, Lsoftware/simplicial/a/u;->ah:[Lsoftware/simplicial/a/ae;

    aget-object v7, v17, v5

    aput-object v7, v6, v5

    .line 1393
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1367
    :cond_0
    const/4 v7, 0x0

    move/from16 v19, v7

    goto/16 :goto_0

    .line 1395
    :cond_1
    const/4 v5, 0x0

    :goto_3
    iget-object v6, v4, Lsoftware/simplicial/a/u;->ag:[Lsoftware/simplicial/a/bt;

    array-length v6, v6

    move-object/from16 v0, v16

    array-length v7, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    if-ge v5, v6, :cond_2

    .line 1396
    iget-object v6, v4, Lsoftware/simplicial/a/u;->ag:[Lsoftware/simplicial/a/bt;

    aget-object v7, v16, v5

    aput-object v7, v6, v5

    .line 1395
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 1399
    :cond_2
    move/from16 v0, p10

    iput-boolean v0, v4, Lsoftware/simplicial/a/u;->q:Z

    .line 1400
    move/from16 v0, p15

    iput-boolean v0, v4, Lsoftware/simplicial/a/u;->t:Z

    .line 1401
    move/from16 v0, p17

    iput-boolean v0, v4, Lsoftware/simplicial/a/u;->u:Z

    .line 1402
    move/from16 v0, p11

    iput-boolean v0, v4, Lsoftware/simplicial/a/u;->p:Z

    .line 1403
    if-eqz p11, :cond_3

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lsoftware/simplicial/a/t;->t:Z

    if-nez v5, :cond_3

    .line 1404
    sget-object v5, Lsoftware/simplicial/a/c/h;->e:Lsoftware/simplicial/a/c/h;

    move-object/from16 v0, p0

    iput-object v5, v0, Lsoftware/simplicial/a/t;->v:Lsoftware/simplicial/a/c/h;

    .line 1405
    :cond_3
    move-object/from16 v0, p16

    iput-object v0, v4, Lsoftware/simplicial/a/u;->v:Lsoftware/simplicial/a/c/g;

    .line 1406
    move/from16 v0, p12

    iput-boolean v0, v4, Lsoftware/simplicial/a/u;->r:Z

    .line 1407
    if-eqz p12, :cond_4

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lsoftware/simplicial/a/t;->t:Z

    if-nez v5, :cond_4

    .line 1408
    sget-object v5, Lsoftware/simplicial/a/h/a$a;->e:Lsoftware/simplicial/a/h/a$a;

    move-object/from16 v0, p0

    iput-object v5, v0, Lsoftware/simplicial/a/t;->z:Lsoftware/simplicial/a/h/a$a;

    .line 1409
    :cond_4
    if-eqz p15, :cond_5

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lsoftware/simplicial/a/t;->t:Z

    if-nez v5, :cond_5

    .line 1410
    sget-object v5, Lsoftware/simplicial/a/i/e;->d:Lsoftware/simplicial/a/i/e;

    move-object/from16 v0, p0

    iput-object v5, v0, Lsoftware/simplicial/a/t;->y:Lsoftware/simplicial/a/i/e;

    .line 1411
    :cond_5
    move/from16 v0, p13

    iput-boolean v0, v4, Lsoftware/simplicial/a/u;->s:Z

    .line 1412
    move/from16 v0, p14

    iput v0, v4, Lsoftware/simplicial/a/u;->w:I

    .line 1413
    move-wide/from16 v0, v22

    iput-wide v0, v4, Lsoftware/simplicial/a/u;->A:J

    .line 1414
    move/from16 v0, v21

    iput v0, v4, Lsoftware/simplicial/a/u;->B:I

    .line 1415
    move-wide/from16 v0, v24

    iput-wide v0, v4, Lsoftware/simplicial/a/u;->C:J

    .line 1416
    move/from16 v0, v26

    iput-boolean v0, v4, Lsoftware/simplicial/a/u;->D:Z

    .line 1417
    move-object/from16 v0, v27

    iput-object v0, v4, Lsoftware/simplicial/a/u;->m:Lsoftware/simplicial/a/aa;

    .line 1418
    move/from16 v0, v28

    iput-boolean v0, v4, Lsoftware/simplicial/a/u;->o:Z

    .line 1420
    move-object/from16 v0, p0

    iput-object v4, v0, Lsoftware/simplicial/a/t;->E:Lsoftware/simplicial/a/u;

    .line 1422
    monitor-exit v20
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1424
    if-nez p10, :cond_6

    if-eqz p15, :cond_7

    :cond_6
    :try_start_2
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lsoftware/simplicial/a/t;->t:Z

    if-eqz v4, :cond_7

    .line 1425
    const/4 v4, -0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lsoftware/simplicial/a/t;->i(I)V

    .line 1427
    :cond_7
    move-object/from16 v0, p0

    move/from16 v1, p7

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/t;->k(I)V

    .line 1429
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    move-object/from16 v2, p6

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/t;->a(Ljava/lang/String;[B)V

    .line 1430
    sget-object v4, Lsoftware/simplicial/a/t$a;->f:Lsoftware/simplicial/a/t$a;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lsoftware/simplicial/a/t;->b(Lsoftware/simplicial/a/t$a;)V

    .line 1431
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/t$a;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1432
    monitor-exit p0

    return-void

    .line 1422
    :catchall_0
    move-exception v4

    :try_start_3
    monitor-exit v20
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1348
    :catchall_1
    move-exception v4

    monitor-exit p0

    throw v4

    :cond_8
    move-object/from16 v16, v4

    move-object/from16 v17, v5

    move-object/from16 v18, v6

    goto/16 :goto_1
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/ac;)V
    .locals 1

    .prologue
    .line 1204
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->p()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1205
    monitor-exit p0

    return-void

    .line 1204
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/af;)V
    .locals 20

    .prologue
    .line 1326
    monitor-enter p0

    :try_start_0
    sget-object v2, Lsoftware/simplicial/a/bj;->c:Lsoftware/simplicial/a/bj;

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/t;->N:Lsoftware/simplicial/a/bj;

    .line 1328
    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/a/f/af;->a:Lsoftware/simplicial/a/f/ba;

    sget-object v3, Lsoftware/simplicial/a/f/ba;->a:Lsoftware/simplicial/a/f/ba;

    if-ne v2, v3, :cond_1

    .line 1330
    move-object/from16 v0, p1

    iget-object v3, v0, Lsoftware/simplicial/a/f/af;->d:Lsoftware/simplicial/a/am;

    move-object/from16 v0, p1

    iget v4, v0, Lsoftware/simplicial/a/f/af;->f:I

    move-object/from16 v0, p1

    iget-object v5, v0, Lsoftware/simplicial/a/f/af;->c:Lsoftware/simplicial/a/aq;

    move-object/from16 v0, p1

    iget-object v6, v0, Lsoftware/simplicial/a/f/af;->e:Lsoftware/simplicial/a/ap;

    move-object/from16 v0, p1

    iget-object v7, v0, Lsoftware/simplicial/a/f/af;->i:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v8, v0, Lsoftware/simplicial/a/f/af;->j:[B

    move-object/from16 v0, p1

    iget v9, v0, Lsoftware/simplicial/a/f/af;->h:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/t;->N:Lsoftware/simplicial/a/bj;

    move-object/from16 v0, p1

    iget-boolean v11, v0, Lsoftware/simplicial/a/f/af;->r:Z

    move-object/from16 v0, p1

    iget-boolean v12, v0, Lsoftware/simplicial/a/f/af;->p:Z

    move-object/from16 v0, p1

    iget-boolean v13, v0, Lsoftware/simplicial/a/f/af;->l:Z

    move-object/from16 v0, p1

    iget-boolean v14, v0, Lsoftware/simplicial/a/f/af;->q:Z

    move-object/from16 v0, p1

    iget-boolean v15, v0, Lsoftware/simplicial/a/f/af;->t:Z

    move-object/from16 v0, p1

    iget v0, v0, Lsoftware/simplicial/a/f/af;->u:I

    move/from16 v16, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, Lsoftware/simplicial/a/f/af;->v:Z

    move/from16 v17, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lsoftware/simplicial/a/f/af;->w:Lsoftware/simplicial/a/c/g;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, Lsoftware/simplicial/a/f/af;->x:Z

    move/from16 v19, v0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v19}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/am;ILsoftware/simplicial/a/aq;Lsoftware/simplicial/a/ap;Ljava/lang/String;[BILsoftware/simplicial/a/bj;ZZZZZIZLsoftware/simplicial/a/c/g;Z)V

    .line 1340
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/t;->k:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/av;

    .line 1341
    move-object/from16 v0, p1

    iget-object v4, v0, Lsoftware/simplicial/a/f/af;->a:Lsoftware/simplicial/a/f/ba;

    move-object/from16 v0, p1

    iget-wide v6, v0, Lsoftware/simplicial/a/f/af;->b:J

    move-object/from16 v0, p1

    iget v5, v0, Lsoftware/simplicial/a/f/af;->s:I

    invoke-interface {v2, v4, v6, v7, v5}, Lsoftware/simplicial/a/av;->a(Lsoftware/simplicial/a/f/ba;JI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1326
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 1336
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lsoftware/simplicial/a/t;->O:Z

    if-nez v2, :cond_0

    .line 1337
    sget-object v2, Lsoftware/simplicial/a/t$a;->e:Lsoftware/simplicial/a/t$a;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lsoftware/simplicial/a/t;->b(Lsoftware/simplicial/a/t$a;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1342
    :cond_2
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/ag;)V
    .locals 5

    .prologue
    .line 1484
    monitor-enter p0

    :try_start_0
    iget-wide v0, p1, Lsoftware/simplicial/a/f/ag;->d:J

    iget-wide v2, p0, Lsoftware/simplicial/a/t;->R:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 1490
    :cond_0
    monitor-exit p0

    return-void

    .line 1486
    :cond_1
    :try_start_1
    iget-wide v0, p1, Lsoftware/simplicial/a/f/ag;->d:J

    iput-wide v0, p0, Lsoftware/simplicial/a/t;->R:J

    .line 1488
    iget-object v0, p0, Lsoftware/simplicial/a/t;->m:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/ah;

    .line 1489
    iget v2, p1, Lsoftware/simplicial/a/f/ag;->a:I

    iget v3, p1, Lsoftware/simplicial/a/f/ag;->b:I

    iget-object v4, p1, Lsoftware/simplicial/a/f/ag;->c:Ljava/lang/String;

    invoke-interface {v0, v2, v3, v4}, Lsoftware/simplicial/a/ah;->a(IILjava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1484
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/ai;)V
    .locals 8

    .prologue
    .line 1555
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v0

    .line 1556
    iget-wide v2, p1, Lsoftware/simplicial/a/f/ai;->f:J

    iget-wide v4, v0, Lsoftware/simplicial/a/u;->O:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 1562
    :cond_0
    monitor-exit p0

    return-void

    .line 1558
    :cond_1
    :try_start_1
    iget-wide v2, p1, Lsoftware/simplicial/a/f/ai;->f:J

    iput-wide v2, v0, Lsoftware/simplicial/a/u;->O:J

    .line 1560
    iget-object v0, p0, Lsoftware/simplicial/a/t;->c:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/m;

    .line 1561
    iget v1, p1, Lsoftware/simplicial/a/f/ai;->ar:I

    iget-object v2, p1, Lsoftware/simplicial/a/f/ai;->a:Ljava/lang/String;

    iget-object v3, p1, Lsoftware/simplicial/a/f/ai;->b:[B

    iget-object v4, p1, Lsoftware/simplicial/a/f/ai;->c:Ljava/lang/String;

    iget v5, p1, Lsoftware/simplicial/a/f/ai;->d:I

    iget-boolean v6, p1, Lsoftware/simplicial/a/f/ai;->e:Z

    invoke-interface/range {v0 .. v6}, Lsoftware/simplicial/a/m;->a(ILjava/lang/String;[BLjava/lang/String;IZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1555
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/ak;)V
    .locals 3

    .prologue
    .line 522
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v0

    .line 524
    iget v1, p1, Lsoftware/simplicial/a/f/ak;->a:I

    iget v2, v0, Lsoftware/simplicial/a/u;->aZ:I

    if-ne v1, v2, :cond_0

    .line 526
    invoke-virtual {v0, p1}, Lsoftware/simplicial/a/u;->a(Lsoftware/simplicial/a/f/be;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 532
    :goto_0
    monitor-exit p0

    return-void

    .line 530
    :cond_0
    :try_start_1
    iget-object v0, p0, Lsoftware/simplicial/a/t;->T:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 522
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/ao;)V
    .locals 6

    .prologue
    .line 1586
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Lsoftware/simplicial/a/f/ao;->a:Lsoftware/simplicial/a/d/a;

    sget-object v1, Lsoftware/simplicial/a/d/a;->a:Lsoftware/simplicial/a/d/a;

    if-ne v0, v1, :cond_0

    .line 1588
    sget-object v0, Lsoftware/simplicial/a/t$a;->j:Lsoftware/simplicial/a/t$a;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/t;->b(Lsoftware/simplicial/a/t$a;)V

    .line 1595
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->k:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/av;

    .line 1596
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/ao;->a()Lsoftware/simplicial/a/f/ba;

    move-result-object v2

    const-wide/16 v4, 0x0

    iget v3, p1, Lsoftware/simplicial/a/f/ao;->b:I

    invoke-interface {v0, v2, v4, v5, v3}, Lsoftware/simplicial/a/av;->a(Lsoftware/simplicial/a/f/ba;JI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1586
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1592
    :cond_0
    :try_start_1
    sget-object v0, Lsoftware/simplicial/a/t$a;->e:Lsoftware/simplicial/a/t$a;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/t;->b(Lsoftware/simplicial/a/t$a;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1597
    :cond_1
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/aq;)V
    .locals 6

    .prologue
    .line 1601
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Lsoftware/simplicial/a/f/aq;->a:Lsoftware/simplicial/a/f/ba;

    sget-object v1, Lsoftware/simplicial/a/f/ba;->a:Lsoftware/simplicial/a/f/ba;

    if-ne v0, v1, :cond_0

    .line 1603
    sget-object v0, Lsoftware/simplicial/a/c/h;->a:Lsoftware/simplicial/a/c/h;

    iput-object v0, p0, Lsoftware/simplicial/a/t;->v:Lsoftware/simplicial/a/c/h;

    .line 1604
    sget-object v0, Lsoftware/simplicial/a/h/a$a;->a:Lsoftware/simplicial/a/h/a$a;

    iput-object v0, p0, Lsoftware/simplicial/a/t;->z:Lsoftware/simplicial/a/h/a$a;

    .line 1605
    sget-object v0, Lsoftware/simplicial/a/i/e;->a:Lsoftware/simplicial/a/i/e;

    iput-object v0, p0, Lsoftware/simplicial/a/t;->y:Lsoftware/simplicial/a/i/e;

    .line 1606
    sget-object v0, Lsoftware/simplicial/a/t$a;->j:Lsoftware/simplicial/a/t$a;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/t;->b(Lsoftware/simplicial/a/t$a;)V

    .line 1613
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->k:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/av;

    .line 1614
    iget-object v2, p1, Lsoftware/simplicial/a/f/aq;->a:Lsoftware/simplicial/a/f/ba;

    const-wide/16 v4, 0x0

    iget v3, p1, Lsoftware/simplicial/a/f/aq;->j:I

    invoke-interface {v0, v2, v4, v5, v3}, Lsoftware/simplicial/a/av;->a(Lsoftware/simplicial/a/f/ba;JI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1601
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1610
    :cond_0
    :try_start_1
    sget-object v0, Lsoftware/simplicial/a/t$a;->e:Lsoftware/simplicial/a/t$a;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/t;->b(Lsoftware/simplicial/a/t$a;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1615
    :cond_1
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/ar;)V
    .locals 2

    .prologue
    .line 1630
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->j:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->i:Lsoftware/simplicial/a/t$a;

    if-ne v0, v1, :cond_1

    .line 1631
    :cond_0
    sget-object v0, Lsoftware/simplicial/a/t$a;->e:Lsoftware/simplicial/a/t$a;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/t;->b(Lsoftware/simplicial/a/t$a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1632
    :cond_1
    monitor-exit p0

    return-void

    .line 1630
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/at;J)V
    .locals 4

    .prologue
    .line 1547
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->J:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, p2, p3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 1549
    iget-object v0, p0, Lsoftware/simplicial/a/t;->l:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/ar;

    .line 1550
    iget-object v2, p1, Lsoftware/simplicial/a/f/at;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Lsoftware/simplicial/a/ar;->b(Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1547
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1551
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/av;J)V
    .locals 6

    .prologue
    .line 1619
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->J:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, p2, p3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 1621
    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->j:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_0

    .line 1622
    sget-object v0, Lsoftware/simplicial/a/t$a;->j:Lsoftware/simplicial/a/t$a;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/t;->b(Lsoftware/simplicial/a/t$a;)V

    .line 1624
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->l:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/ar;

    .line 1625
    iget-object v2, p1, Lsoftware/simplicial/a/f/av;->b:Ljava/util/List;

    iget-object v3, p1, Lsoftware/simplicial/a/f/av;->c:Ljava/util/List;

    iget-object v4, p1, Lsoftware/simplicial/a/f/av;->d:Ljava/util/List;

    iget v5, p1, Lsoftware/simplicial/a/f/av;->e:F

    invoke-interface {v0, v2, v3, v4, v5}, Lsoftware/simplicial/a/ar;->a(Ljava/util/List;Ljava/util/List;Ljava/util/List;F)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1619
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1626
    :cond_1
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/ay;)V
    .locals 5

    .prologue
    .line 1042
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->g:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/at;

    .line 1043
    iget-object v2, p1, Lsoftware/simplicial/a/f/ay;->a:Lsoftware/simplicial/a/au;

    iget-object v3, p1, Lsoftware/simplicial/a/f/ay;->d:Ljava/lang/String;

    iget v4, p1, Lsoftware/simplicial/a/f/ay;->c:I

    invoke-interface {v0, v2, v3, v4}, Lsoftware/simplicial/a/at;->a(Lsoftware/simplicial/a/au;Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1042
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1044
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/b;)V
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 1447
    monitor-enter p0

    :try_start_0
    iget v0, p1, Lsoftware/simplicial/a/f/b;->a:I

    new-array v2, v0, [Lsoftware/simplicial/a/bg$b;

    .line 1448
    iget v0, p1, Lsoftware/simplicial/a/f/b;->a:I

    new-array v3, v0, [Lsoftware/simplicial/a/aq;

    .line 1449
    iget v0, p1, Lsoftware/simplicial/a/f/b;->a:I

    new-array v4, v0, [Z

    .line 1450
    iget v0, p1, Lsoftware/simplicial/a/f/b;->a:I

    new-array v5, v0, [Lsoftware/simplicial/a/bn;

    .line 1451
    iget v0, p1, Lsoftware/simplicial/a/f/b;->a:I

    new-array v6, v0, [Lsoftware/simplicial/a/ak;

    .line 1452
    iget v0, p1, Lsoftware/simplicial/a/f/b;->a:I

    new-array v7, v0, [S

    move v8, v1

    .line 1454
    :goto_0
    iget v0, p1, Lsoftware/simplicial/a/f/b;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ge v8, v0, :cond_2

    .line 1458
    :try_start_1
    iget-object v0, p1, Lsoftware/simplicial/a/f/b;->c:[B

    aget-byte v0, v0, v8

    and-int/lit16 v0, v0, 0x80

    const/16 v9, 0x80

    if-ne v0, v9, :cond_0

    sget-object v0, Lsoftware/simplicial/a/aq;->b:Lsoftware/simplicial/a/aq;

    :goto_1
    aput-object v0, v3, v8

    .line 1459
    iget-object v0, p1, Lsoftware/simplicial/a/f/b;->c:[B

    aget-byte v0, v0, v8

    and-int/lit8 v0, v0, 0x40

    const/16 v9, 0x40

    if-ne v0, v9, :cond_1

    const/4 v0, 0x1

    :goto_2
    aput-boolean v0, v4, v8

    .line 1460
    sget-object v0, Lsoftware/simplicial/a/bg$b;->B:[Lsoftware/simplicial/a/bg$b;

    iget-object v9, p1, Lsoftware/simplicial/a/f/b;->c:[B

    aget-byte v9, v9, v8

    and-int/lit8 v9, v9, 0x1f

    int-to-byte v9, v9

    aget-object v0, v0, v9

    aput-object v0, v2, v8

    .line 1461
    iget-object v0, p1, Lsoftware/simplicial/a/f/b;->d:[B

    aget-byte v0, v0, v8

    .line 1462
    sget-object v9, Lsoftware/simplicial/a/bn;->j:[Lsoftware/simplicial/a/bn;

    and-int/lit8 v10, v0, 0xf

    ushr-int/lit8 v10, v10, 0x0

    aget-object v9, v9, v10

    aput-object v9, v5, v8

    .line 1463
    sget-object v9, Lsoftware/simplicial/a/ak;->e:[Lsoftware/simplicial/a/ak;

    and-int/lit16 v0, v0, 0xf0

    ushr-int/lit8 v0, v0, 0x4

    aget-object v0, v9, v0

    aput-object v0, v6, v8

    .line 1464
    iget-object v0, p1, Lsoftware/simplicial/a/f/b;->e:[S

    aget-short v0, v0, v8

    aput-short v0, v7, v8

    .line 1454
    :goto_3
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_0

    .line 1458
    :cond_0
    sget-object v0, Lsoftware/simplicial/a/aq;->a:Lsoftware/simplicial/a/aq;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :cond_1
    move v0, v1

    .line 1459
    goto :goto_2

    .line 1466
    :catch_0
    move-exception v0

    .line 1468
    :try_start_2
    sget-object v0, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v9, "Exception occurred handling OnAccountStatusResultMessage"

    invoke-static {v0, v9}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 1469
    sget-object v0, Lsoftware/simplicial/a/aq;->a:Lsoftware/simplicial/a/aq;

    aput-object v0, v3, v8

    .line 1470
    const/4 v0, 0x0

    aput-boolean v0, v4, v8

    .line 1471
    sget-object v0, Lsoftware/simplicial/a/bg$b;->b:Lsoftware/simplicial/a/bg$b;

    aput-object v0, v2, v8

    .line 1472
    sget-object v0, Lsoftware/simplicial/a/bn;->a:Lsoftware/simplicial/a/bn;

    aput-object v0, v5, v8

    .line 1473
    sget-object v0, Lsoftware/simplicial/a/ak;->a:Lsoftware/simplicial/a/ak;

    aput-object v0, v6, v8

    .line 1474
    const/4 v0, -0x1

    aput-short v0, v7, v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    .line 1447
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1478
    :cond_2
    :try_start_3
    iget-object v0, p0, Lsoftware/simplicial/a/t;->i:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/c;

    .line 1479
    iget-object v1, p1, Lsoftware/simplicial/a/f/b;->b:[I

    invoke-interface/range {v0 .. v7}, Lsoftware/simplicial/a/c;->a([I[Lsoftware/simplicial/a/bg$b;[Lsoftware/simplicial/a/aq;[Z[Lsoftware/simplicial/a/bn;[Lsoftware/simplicial/a/ak;[S)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_4

    .line 1480
    :cond_3
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/bb;)V
    .locals 6

    .prologue
    .line 1246
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v0

    .line 1248
    iget v1, p1, Lsoftware/simplicial/a/f/bb;->ar:I

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->q()I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 1250
    invoke-virtual {v0, p1}, Lsoftware/simplicial/a/u;->a(Lsoftware/simplicial/a/f/be;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1272
    :cond_0
    monitor-exit p0

    return-void

    .line 1254
    :cond_1
    :try_start_1
    iget-object v1, p1, Lsoftware/simplicial/a/f/bb;->a:Lsoftware/simplicial/a/f/ba;

    sget-object v2, Lsoftware/simplicial/a/f/ba;->n:Lsoftware/simplicial/a/f/ba;

    if-ne v1, v2, :cond_2

    iget-boolean v1, p0, Lsoftware/simplicial/a/t;->t:Z

    if-nez v1, :cond_2

    .line 1256
    invoke-virtual {v0}, Lsoftware/simplicial/a/u;->i()V

    .line 1257
    const/4 v1, 0x1

    iput-boolean v1, p0, Lsoftware/simplicial/a/t;->t:Z

    .line 1260
    :cond_2
    iget-object v1, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v2, Lsoftware/simplicial/a/t$a;->f:Lsoftware/simplicial/a/t$a;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v2, Lsoftware/simplicial/a/t$a;->g:Lsoftware/simplicial/a/t$a;

    if-ne v1, v2, :cond_5

    .line 1262
    :cond_3
    iget-object v1, p1, Lsoftware/simplicial/a/f/bb;->a:Lsoftware/simplicial/a/f/ba;

    sget-object v2, Lsoftware/simplicial/a/f/ba;->a:Lsoftware/simplicial/a/f/ba;

    if-eq v1, v2, :cond_4

    iget-boolean v1, v0, Lsoftware/simplicial/a/u;->e:Z

    if-eqz v1, :cond_6

    .line 1263
    :cond_4
    sget-object v1, Lsoftware/simplicial/a/t$a;->h:Lsoftware/simplicial/a/t$a;

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->b(Lsoftware/simplicial/a/t$a;)V

    .line 1268
    :cond_5
    :goto_0
    invoke-virtual {v0, p1}, Lsoftware/simplicial/a/u;->a(Lsoftware/simplicial/a/f/be;)V

    .line 1270
    iget-object v0, p0, Lsoftware/simplicial/a/t;->k:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/av;

    .line 1271
    iget-object v2, p1, Lsoftware/simplicial/a/f/bb;->a:Lsoftware/simplicial/a/f/ba;

    iget-wide v4, p1, Lsoftware/simplicial/a/f/bb;->b:J

    iget v3, p1, Lsoftware/simplicial/a/f/bb;->n:I

    invoke-interface {v0, v2, v4, v5, v3}, Lsoftware/simplicial/a/av;->a(Lsoftware/simplicial/a/f/ba;JI)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1246
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1265
    :cond_6
    :try_start_2
    sget-object v1, Lsoftware/simplicial/a/t$a;->f:Lsoftware/simplicial/a/t$a;

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->b(Lsoftware/simplicial/a/t$a;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private a(Lsoftware/simplicial/a/f/be;J)V
    .locals 8

    .prologue
    .line 376
    :try_start_0
    sget-object v1, Lsoftware/simplicial/a/t$1;->a:[I

    iget-object v2, p1, Lsoftware/simplicial/a/f/be;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 484
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "C: Uhandled message "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lsoftware/simplicial/a/f/be;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 496
    :goto_0
    return-void

    .line 379
    :pswitch_0
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/x;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/x;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 488
    :catch_0
    move-exception v1

    .line 490
    :try_start_1
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v3, "Failed to handle message %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p1, Lsoftware/simplicial/a/f/be;->aq:Lsoftware/simplicial/a/f/bh;

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 493
    :catchall_0
    move-exception v1

    throw v1

    .line 382
    :pswitch_1
    :try_start_2
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/co;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/co;)V

    goto :goto_0

    .line 385
    :pswitch_2
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/s;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/s;)V

    goto :goto_0

    .line 388
    :pswitch_3
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/m;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/m;)V

    goto :goto_0

    .line 391
    :pswitch_4
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/l;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/l;)V

    goto :goto_0

    .line 394
    :pswitch_5
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/q;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/q;)V

    goto :goto_0

    .line 397
    :pswitch_6
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/p;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/p;)V

    goto :goto_0

    .line 400
    :pswitch_7
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/ar;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/ar;)V

    goto :goto_0

    .line 403
    :pswitch_8
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/av;

    move-object v1, v0

    invoke-direct {p0, v1, p2, p3}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/av;J)V

    goto :goto_0

    .line 406
    :pswitch_9
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/aq;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/aq;)V

    goto :goto_0

    .line 409
    :pswitch_a
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/ao;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/ao;)V

    goto :goto_0

    .line 412
    :pswitch_b
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/k;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/k;)V

    goto :goto_0

    .line 415
    :pswitch_c
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/bt;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/bt;)V

    goto :goto_0

    .line 418
    :pswitch_d
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/ai;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/ai;)V

    goto/16 :goto_0

    .line 421
    :pswitch_e
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/i;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/i;)V

    goto/16 :goto_0

    .line 424
    :pswitch_f
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/j;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/j;)V

    goto/16 :goto_0

    .line 427
    :pswitch_10
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/ay;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/ay;)V

    goto/16 :goto_0

    .line 430
    :pswitch_11
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/at;

    move-object v1, v0

    invoke-direct {p0, v1, p2, p3}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/at;J)V

    goto/16 :goto_0

    .line 433
    :pswitch_12
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/bw;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/bw;)V

    goto/16 :goto_0

    .line 436
    :pswitch_13
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/bs;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/bs;)V

    goto/16 :goto_0

    .line 439
    :pswitch_14
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/u;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/u;)V

    goto/16 :goto_0

    .line 442
    :pswitch_15
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/ag;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/ag;)V

    goto/16 :goto_0

    .line 445
    :pswitch_16
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/b;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/b;)V

    goto/16 :goto_0

    .line 448
    :pswitch_17
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/af;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/af;)V

    goto/16 :goto_0

    .line 451
    :pswitch_18
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/cm;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/cm;)V

    goto/16 :goto_0

    .line 454
    :pswitch_19
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/ci;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/ci;)V

    goto/16 :goto_0

    .line 457
    :pswitch_1a
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/cn;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/cn;)V

    goto/16 :goto_0

    .line 460
    :pswitch_1b
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/h;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/h;)V

    goto/16 :goto_0

    .line 463
    :pswitch_1c
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/e;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/e;)V

    goto/16 :goto_0

    .line 466
    :pswitch_1d
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/bb;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/bb;)V

    goto/16 :goto_0

    .line 469
    :pswitch_1e
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/ac;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/ac;)V

    goto/16 :goto_0

    .line 472
    :pswitch_1f
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/ak;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/ak;)V

    goto/16 :goto_0

    .line 475
    :pswitch_20
    invoke-virtual {p0, p2, p3}, Lsoftware/simplicial/a/t;->a(J)V

    goto/16 :goto_0

    .line 478
    :pswitch_21
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/cr;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/cr;)V

    goto/16 :goto_0

    .line 481
    :pswitch_22
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/cu;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/cu;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 376
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
    .end packed-switch
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/bs;)V
    .locals 7

    .prologue
    .line 1494
    monitor-enter p0

    :try_start_0
    iget-wide v0, p1, Lsoftware/simplicial/a/f/bs;->e:J

    iget-wide v2, p0, Lsoftware/simplicial/a/t;->R:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 1500
    :cond_0
    monitor-exit p0

    return-void

    .line 1496
    :cond_1
    :try_start_1
    iget-wide v0, p1, Lsoftware/simplicial/a/f/bs;->e:J

    iput-wide v0, p0, Lsoftware/simplicial/a/t;->R:J

    .line 1498
    iget-object v0, p0, Lsoftware/simplicial/a/t;->c:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/m;

    .line 1499
    iget v1, p1, Lsoftware/simplicial/a/f/bs;->ar:I

    iget v2, p1, Lsoftware/simplicial/a/f/bs;->a:I

    iget v3, p1, Lsoftware/simplicial/a/f/bs;->b:I

    iget-object v4, p1, Lsoftware/simplicial/a/f/bs;->c:Ljava/lang/String;

    iget-object v5, p1, Lsoftware/simplicial/a/f/bs;->d:Ljava/lang/String;

    invoke-interface/range {v0 .. v5}, Lsoftware/simplicial/a/m;->a(IIILjava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1494
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/bt;)V
    .locals 9

    .prologue
    .line 1566
    monitor-enter p0

    :try_start_0
    iget-wide v0, p1, Lsoftware/simplicial/a/f/bt;->e:J

    iget-wide v2, p0, Lsoftware/simplicial/a/t;->R:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 1572
    :cond_0
    monitor-exit p0

    return-void

    .line 1568
    :cond_1
    :try_start_1
    iget-wide v0, p1, Lsoftware/simplicial/a/f/bt;->e:J

    iput-wide v0, p0, Lsoftware/simplicial/a/t;->R:J

    .line 1570
    iget-object v0, p0, Lsoftware/simplicial/a/t;->c:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/m;

    .line 1571
    iget v1, p1, Lsoftware/simplicial/a/f/bt;->ar:I

    iget v2, p1, Lsoftware/simplicial/a/f/bt;->d:I

    iget-object v3, p1, Lsoftware/simplicial/a/f/bt;->a:Ljava/lang/String;

    iget-object v4, p1, Lsoftware/simplicial/a/f/bt;->b:[B

    sget-object v5, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    sget-object v6, Lsoftware/simplicial/a/n;->a:Lsoftware/simplicial/a/n;

    iget-object v7, p1, Lsoftware/simplicial/a/f/bt;->c:Ljava/lang/String;

    invoke-interface/range {v0 .. v7}, Lsoftware/simplicial/a/m;->a(IILjava/lang/String;[BLsoftware/simplicial/a/q;Lsoftware/simplicial/a/n;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1566
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/bw;)V
    .locals 3

    .prologue
    .line 1540
    monitor-enter p0

    :try_start_0
    iget v0, p1, Lsoftware/simplicial/a/f/bw;->a:I

    iput v0, p0, Lsoftware/simplicial/a/t;->s:I

    .line 1541
    iget-object v0, p0, Lsoftware/simplicial/a/t;->b:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/w;

    .line 1542
    iget v2, p1, Lsoftware/simplicial/a/f/bw;->a:I

    invoke-interface {v0, v2}, Lsoftware/simplicial/a/w;->b(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1540
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1543
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/ci;)V
    .locals 13

    .prologue
    .line 1313
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->h:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/h/e;

    .line 1314
    iget v1, p1, Lsoftware/simplicial/a/f/ci;->a:I

    iget-object v2, p1, Lsoftware/simplicial/a/f/ci;->b:Ljava/lang/String;

    iget-object v3, p1, Lsoftware/simplicial/a/f/ci;->c:Lsoftware/simplicial/a/h/a$a;

    iget v4, p1, Lsoftware/simplicial/a/f/ci;->e:I

    iget v5, p1, Lsoftware/simplicial/a/f/ci;->f:I

    iget-boolean v6, p1, Lsoftware/simplicial/a/f/ci;->d:Z

    new-instance v7, Ljava/util/ArrayList;

    iget-object v8, p1, Lsoftware/simplicial/a/f/ci;->h:Ljava/util/List;

    invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v8, Ljava/util/ArrayList;

    iget-object v9, p1, Lsoftware/simplicial/a/f/ci;->i:Ljava/util/List;

    invoke-direct {v8, v9}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v9, Ljava/util/ArrayList;

    iget-object v10, p1, Lsoftware/simplicial/a/f/ci;->g:Ljava/util/List;

    invoke-direct {v9, v10}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v10, Ljava/util/ArrayList;

    iget-object v12, p1, Lsoftware/simplicial/a/f/ci;->j:Ljava/util/List;

    invoke-direct {v10, v12}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface/range {v0 .. v10}, Lsoftware/simplicial/a/h/e;->a(ILjava/lang/String;Lsoftware/simplicial/a/h/a$a;IIZLjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1313
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1315
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/cm;)V
    .locals 6

    .prologue
    .line 1320
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->h:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/h/e;

    .line 1321
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p1, Lsoftware/simplicial/a/f/cm;->a:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v3, Ljava/util/ArrayList;

    iget-object v4, p1, Lsoftware/simplicial/a/f/cm;->b:Ljava/util/List;

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v4, Ljava/util/ArrayList;

    iget-object v5, p1, Lsoftware/simplicial/a/f/cm;->c:Ljava/util/List;

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0, v2, v3, v4}, Lsoftware/simplicial/a/h/e;->a(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1320
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1322
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/cn;)V
    .locals 7

    .prologue
    .line 1303
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Lsoftware/simplicial/a/f/cn;->c:Lsoftware/simplicial/a/h/a$a;

    iput-object v0, p0, Lsoftware/simplicial/a/t;->z:Lsoftware/simplicial/a/h/a$a;

    .line 1304
    iget-byte v0, p1, Lsoftware/simplicial/a/f/cn;->d:B

    iput v0, p0, Lsoftware/simplicial/a/t;->A:I

    .line 1305
    iget-byte v0, p1, Lsoftware/simplicial/a/f/cn;->e:B

    iput v0, p0, Lsoftware/simplicial/a/t;->B:I

    .line 1307
    iget-object v0, p0, Lsoftware/simplicial/a/t;->h:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/h/e;

    .line 1308
    iget v1, p1, Lsoftware/simplicial/a/f/cn;->a:I

    iget-object v2, p1, Lsoftware/simplicial/a/f/cn;->b:Ljava/lang/String;

    iget-object v3, p1, Lsoftware/simplicial/a/f/cn;->c:Lsoftware/simplicial/a/h/a$a;

    iget-byte v4, p1, Lsoftware/simplicial/a/f/cn;->d:B

    iget-byte v5, p1, Lsoftware/simplicial/a/f/cn;->e:B

    invoke-interface/range {v0 .. v5}, Lsoftware/simplicial/a/h/e;->a(ILjava/lang/String;Lsoftware/simplicial/a/h/a$a;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1303
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1309
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/co;)V
    .locals 21

    .prologue
    .line 1504
    monitor-enter p0

    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v20

    .line 1506
    move-object/from16 v0, v20

    iget-object v2, v0, Lsoftware/simplicial/a/u;->ba:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v3, v0, Lsoftware/simplicial/a/f/co;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1508
    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/a/f/co;->i:Ljava/lang/String;

    move-object/from16 v0, v20

    iput-object v2, v0, Lsoftware/simplicial/a/u;->ba:Ljava/lang/String;

    .line 1509
    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/a/f/co;->j:[B

    move-object/from16 v0, v20

    iput-object v2, v0, Lsoftware/simplicial/a/u;->bb:[B

    .line 1510
    move-object/from16 v0, v20

    iget-object v2, v0, Lsoftware/simplicial/a/u;->ba:Ljava/lang/String;

    move-object/from16 v0, v20

    iget-object v3, v0, Lsoftware/simplicial/a/u;->bb:[B

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lsoftware/simplicial/a/t;->a(Ljava/lang/String;[B)V

    .line 1514
    :cond_0
    move-object/from16 v0, v20

    iget-object v2, v0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    move-object/from16 v0, p1

    iget-object v3, v0, Lsoftware/simplicial/a/f/co;->r:Lsoftware/simplicial/a/am;

    if-ne v2, v3, :cond_1

    move-object/from16 v0, v20

    iget-object v2, v0, Lsoftware/simplicial/a/u;->aV:Lsoftware/simplicial/a/aq;

    move-object/from16 v0, p1

    iget-object v3, v0, Lsoftware/simplicial/a/f/co;->q:Lsoftware/simplicial/a/aq;

    if-ne v2, v3, :cond_1

    move-object/from16 v0, v20

    iget v2, v0, Lsoftware/simplicial/a/u;->aY:I

    move-object/from16 v0, p1

    iget-byte v3, v0, Lsoftware/simplicial/a/f/co;->d:B

    if-ne v2, v3, :cond_1

    move-object/from16 v0, v20

    iget-object v2, v0, Lsoftware/simplicial/a/u;->aQ:Lsoftware/simplicial/a/ap;

    move-object/from16 v0, p1

    iget-object v3, v0, Lsoftware/simplicial/a/f/co;->p:Lsoftware/simplicial/a/ap;

    if-ne v2, v3, :cond_1

    move-object/from16 v0, v20

    iget-boolean v2, v0, Lsoftware/simplicial/a/u;->aR:Z

    move-object/from16 v0, p1

    iget-boolean v3, v0, Lsoftware/simplicial/a/f/co;->o:Z

    if-eq v2, v3, :cond_4

    .line 1516
    :cond_1
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v3, "OnTopScoresMessage - NEW CG"

    invoke-static {v2, v3}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 1518
    move-object/from16 v0, p1

    iget-object v3, v0, Lsoftware/simplicial/a/f/co;->r:Lsoftware/simplicial/a/am;

    move-object/from16 v0, p1

    iget-byte v4, v0, Lsoftware/simplicial/a/f/co;->d:B

    move-object/from16 v0, p1

    iget-object v5, v0, Lsoftware/simplicial/a/f/co;->q:Lsoftware/simplicial/a/aq;

    move-object/from16 v0, p1

    iget-object v6, v0, Lsoftware/simplicial/a/f/co;->p:Lsoftware/simplicial/a/ap;

    move-object/from16 v0, p1

    iget-object v7, v0, Lsoftware/simplicial/a/f/co;->i:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v8, v0, Lsoftware/simplicial/a/f/co;->j:[B

    move-object/from16 v0, p1

    iget v9, v0, Lsoftware/simplicial/a/f/co;->k:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/t;->N:Lsoftware/simplicial/a/bj;

    move-object/from16 v0, p1

    iget-boolean v11, v0, Lsoftware/simplicial/a/f/co;->o:Z

    move-object/from16 v0, p1

    iget-boolean v12, v0, Lsoftware/simplicial/a/f/co;->m:Z

    move-object/from16 v0, p1

    iget-boolean v13, v0, Lsoftware/simplicial/a/f/co;->l:Z

    move-object/from16 v0, p1

    iget-boolean v14, v0, Lsoftware/simplicial/a/f/co;->n:Z

    move-object/from16 v0, p1

    iget-boolean v15, v0, Lsoftware/simplicial/a/f/co;->s:Z

    move-object/from16 v0, p1

    iget v0, v0, Lsoftware/simplicial/a/f/co;->t:I

    move/from16 v16, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, Lsoftware/simplicial/a/f/co;->u:Z

    move/from16 v17, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lsoftware/simplicial/a/f/co;->w:Lsoftware/simplicial/a/c/g;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, Lsoftware/simplicial/a/f/co;->v:Z

    move/from16 v19, v0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v19}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/am;ILsoftware/simplicial/a/aq;Lsoftware/simplicial/a/ap;Ljava/lang/String;[BILsoftware/simplicial/a/bj;ZZZZZIZLsoftware/simplicial/a/c/g;Z)V

    .line 1521
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lsoftware/simplicial/a/f/co;->l:Z

    if-eqz v2, :cond_5

    sget-object v2, Lsoftware/simplicial/a/c/h;->e:Lsoftware/simplicial/a/c/h;

    :goto_0
    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/t;->v:Lsoftware/simplicial/a/c/h;

    .line 1522
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lsoftware/simplicial/a/f/co;->n:Z

    if-eqz v2, :cond_6

    sget-object v2, Lsoftware/simplicial/a/h/a$a;->e:Lsoftware/simplicial/a/h/a$a;

    :goto_1
    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/t;->z:Lsoftware/simplicial/a/h/a$a;

    .line 1523
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lsoftware/simplicial/a/f/co;->u:Z

    if-nez v2, :cond_2

    move-object/from16 v0, p1

    iget-boolean v2, v0, Lsoftware/simplicial/a/f/co;->v:Z

    if-eqz v2, :cond_3

    .line 1524
    :cond_2
    sget-object v2, Lsoftware/simplicial/a/i/e;->d:Lsoftware/simplicial/a/i/e;

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/t;->y:Lsoftware/simplicial/a/i/e;

    .line 1526
    :cond_3
    move-object/from16 v0, p1

    iget v2, v0, Lsoftware/simplicial/a/f/co;->k:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lsoftware/simplicial/a/t;->k(I)V

    .line 1529
    :cond_4
    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/u;->a(Lsoftware/simplicial/a/f/be;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1530
    monitor-exit p0

    return-void

    .line 1521
    :cond_5
    :try_start_1
    sget-object v2, Lsoftware/simplicial/a/c/h;->a:Lsoftware/simplicial/a/c/h;

    goto :goto_0

    .line 1522
    :cond_6
    sget-object v2, Lsoftware/simplicial/a/h/a$a;->a:Lsoftware/simplicial/a/h/a$a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1504
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private a(Lsoftware/simplicial/a/f/cr;)V
    .locals 9

    .prologue
    .line 508
    iget-object v0, p1, Lsoftware/simplicial/a/f/cr;->d:Lsoftware/simplicial/a/i/e;

    iput-object v0, p0, Lsoftware/simplicial/a/t;->y:Lsoftware/simplicial/a/i/e;

    .line 510
    iget-object v0, p0, Lsoftware/simplicial/a/t;->n:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/i/f;

    .line 511
    iget-object v1, p1, Lsoftware/simplicial/a/f/cr;->a:Ljava/util/List;

    iget-object v2, p1, Lsoftware/simplicial/a/f/cr;->d:Lsoftware/simplicial/a/i/e;

    iget v3, p1, Lsoftware/simplicial/a/f/cr;->e:I

    iget v4, p1, Lsoftware/simplicial/a/f/cr;->f:I

    iget-object v5, p1, Lsoftware/simplicial/a/f/cr;->c:Ljava/util/List;

    iget v6, p1, Lsoftware/simplicial/a/f/cr;->g:I

    iget-object v7, p1, Lsoftware/simplicial/a/f/cr;->b:Ljava/util/List;

    invoke-interface/range {v0 .. v7}, Lsoftware/simplicial/a/i/f;->a(Ljava/util/List;Lsoftware/simplicial/a/i/e;IILjava/util/List;ILjava/util/List;)V

    goto :goto_0

    .line 512
    :cond_0
    return-void
.end method

.method private a(Lsoftware/simplicial/a/f/cu;)V
    .locals 6

    .prologue
    .line 500
    iget-object v0, p1, Lsoftware/simplicial/a/f/cu;->a:Lsoftware/simplicial/a/i/e;

    iput-object v0, p0, Lsoftware/simplicial/a/t;->y:Lsoftware/simplicial/a/i/e;

    .line 502
    iget-object v0, p0, Lsoftware/simplicial/a/t;->n:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/i/f;

    .line 503
    iget-object v2, p1, Lsoftware/simplicial/a/f/cu;->a:Lsoftware/simplicial/a/i/e;

    iget v3, p1, Lsoftware/simplicial/a/f/cu;->b:I

    iget-boolean v4, p1, Lsoftware/simplicial/a/f/cu;->c:Z

    iget-boolean v5, p1, Lsoftware/simplicial/a/f/cu;->d:Z

    invoke-interface {v0, v2, v3, v4, v5}, Lsoftware/simplicial/a/i/f;->a(Lsoftware/simplicial/a/i/e;IZZ)V

    goto :goto_0

    .line 504
    :cond_0
    return-void
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/e;)V
    .locals 6

    .prologue
    .line 1291
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->e:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/b/c;

    .line 1292
    iget-object v2, p1, Lsoftware/simplicial/a/f/e;->a:Ljava/util/List;

    iget v3, p1, Lsoftware/simplicial/a/f/e;->b:I

    iget-object v4, p1, Lsoftware/simplicial/a/f/e;->c:Lsoftware/simplicial/a/b/b;

    iget-boolean v5, p1, Lsoftware/simplicial/a/f/e;->d:Z

    invoke-interface {v0, v2, v3, v4, v5}, Lsoftware/simplicial/a/b/c;->a(Ljava/util/List;ILsoftware/simplicial/a/b/b;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1291
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1293
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/h;)V
    .locals 5

    .prologue
    .line 1297
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->e:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/b/c;

    .line 1298
    iget v2, p1, Lsoftware/simplicial/a/f/h;->a:I

    iget-object v3, p1, Lsoftware/simplicial/a/f/h;->b:Lsoftware/simplicial/a/b/b;

    iget-object v4, p1, Lsoftware/simplicial/a/f/h;->c:Lsoftware/simplicial/a/b/e;

    invoke-interface {v0, v2, v3, v4}, Lsoftware/simplicial/a/b/c;->a(ILsoftware/simplicial/a/b/b;Lsoftware/simplicial/a/b/e;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1297
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1299
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/i;)V
    .locals 5

    .prologue
    .line 1030
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->f:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/j;

    .line 1031
    iget v2, p1, Lsoftware/simplicial/a/f/i;->ar:I

    iget-object v3, p1, Lsoftware/simplicial/a/f/i;->d:Ljava/lang/String;

    iget v4, p1, Lsoftware/simplicial/a/f/i;->b:I

    invoke-interface {v0, v2, v3, v4}, Lsoftware/simplicial/a/j;->a(ILjava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1030
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1032
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/j;)V
    .locals 4

    .prologue
    .line 1048
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->f:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/j;

    .line 1049
    iget-object v2, p1, Lsoftware/simplicial/a/f/j;->a:Lsoftware/simplicial/a/k;

    iget-object v3, p1, Lsoftware/simplicial/a/f/j;->b:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Lsoftware/simplicial/a/j;->a(Lsoftware/simplicial/a/k;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1048
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1050
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/k;)V
    .locals 9

    .prologue
    .line 1576
    monitor-enter p0

    :try_start_0
    iget-wide v0, p1, Lsoftware/simplicial/a/f/k;->e:J

    iget-wide v2, p0, Lsoftware/simplicial/a/t;->R:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 1582
    :cond_0
    monitor-exit p0

    return-void

    .line 1578
    :cond_1
    :try_start_1
    iget-wide v0, p1, Lsoftware/simplicial/a/f/k;->e:J

    iput-wide v0, p0, Lsoftware/simplicial/a/t;->R:J

    .line 1580
    iget-object v0, p0, Lsoftware/simplicial/a/t;->c:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/m;

    .line 1581
    iget v1, p1, Lsoftware/simplicial/a/f/k;->ar:I

    iget v2, p1, Lsoftware/simplicial/a/f/k;->d:I

    iget-object v3, p1, Lsoftware/simplicial/a/f/k;->a:Ljava/lang/String;

    const/4 v4, 0x0

    new-array v4, v4, [B

    iget-object v5, p1, Lsoftware/simplicial/a/f/k;->c:Lsoftware/simplicial/a/q;

    sget-object v6, Lsoftware/simplicial/a/n;->b:Lsoftware/simplicial/a/n;

    iget-object v7, p1, Lsoftware/simplicial/a/f/k;->b:Ljava/lang/String;

    invoke-interface/range {v0 .. v7}, Lsoftware/simplicial/a/m;->a(IILjava/lang/String;[BLsoftware/simplicial/a/q;Lsoftware/simplicial/a/n;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1576
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/l;)V
    .locals 3

    .prologue
    .line 1655
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->o:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/o;

    .line 1656
    iget-object v2, p1, Lsoftware/simplicial/a/f/l;->a:Lsoftware/simplicial/a/am;

    invoke-interface {v0, v2}, Lsoftware/simplicial/a/o;->a(Lsoftware/simplicial/a/am;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1655
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1657
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/m;)V
    .locals 6

    .prologue
    .line 1649
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->d:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/c/d;

    .line 1650
    iget-object v2, p1, Lsoftware/simplicial/a/f/m;->a:Lsoftware/simplicial/a/c/b;

    iget-object v3, p1, Lsoftware/simplicial/a/f/m;->b:Lsoftware/simplicial/a/c/e;

    iget-object v4, p1, Lsoftware/simplicial/a/f/m;->c:Lsoftware/simplicial/a/c/g;

    iget v5, p1, Lsoftware/simplicial/a/f/m;->d:I

    invoke-interface {v0, v2, v3, v4, v5}, Lsoftware/simplicial/a/c/d;->a(Lsoftware/simplicial/a/c/b;Lsoftware/simplicial/a/c/e;Lsoftware/simplicial/a/c/g;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1649
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1651
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/p;)V
    .locals 4

    .prologue
    .line 1636
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->d:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/c/d;

    .line 1637
    iget-object v2, p1, Lsoftware/simplicial/a/f/p;->a:Ljava/util/List;

    iget v3, p1, Lsoftware/simplicial/a/f/p;->b:I

    invoke-interface {v0, v2, v3}, Lsoftware/simplicial/a/c/d;->a(Ljava/util/List;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1636
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1638
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/q;)V
    .locals 3

    .prologue
    .line 1642
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v1

    .line 1643
    iget-object v0, p0, Lsoftware/simplicial/a/t;->d:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/c/d;

    .line 1644
    invoke-interface {v0, v1}, Lsoftware/simplicial/a/c/d;->a(Lsoftware/simplicial/a/ai;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1642
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1645
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/s;)V
    .locals 7

    .prologue
    .line 1661
    monitor-enter p0

    :try_start_0
    iget v0, p1, Lsoftware/simplicial/a/f/s;->d:I

    iget v1, p1, Lsoftware/simplicial/a/f/s;->e:I

    if-ne v0, v1, :cond_1

    .line 1663
    iget-object v0, p1, Lsoftware/simplicial/a/f/s;->a:Lsoftware/simplicial/a/c/h;

    iput-object v0, p0, Lsoftware/simplicial/a/t;->v:Lsoftware/simplicial/a/c/h;

    .line 1664
    iget v0, p1, Lsoftware/simplicial/a/f/s;->b:I

    iput v0, p0, Lsoftware/simplicial/a/t;->w:I

    .line 1665
    iget v0, p1, Lsoftware/simplicial/a/f/s;->c:I

    iput v0, p0, Lsoftware/simplicial/a/t;->x:I

    .line 1672
    :cond_0
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->d:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/c/d;

    .line 1673
    iget-object v1, p1, Lsoftware/simplicial/a/f/s;->a:Lsoftware/simplicial/a/c/h;

    iget v2, p1, Lsoftware/simplicial/a/f/s;->b:I

    iget v3, p1, Lsoftware/simplicial/a/f/s;->c:I

    iget v4, p1, Lsoftware/simplicial/a/f/s;->d:I

    iget v5, p1, Lsoftware/simplicial/a/f/s;->e:I

    invoke-interface/range {v0 .. v5}, Lsoftware/simplicial/a/c/d;->a(Lsoftware/simplicial/a/c/h;IIII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1661
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1667
    :cond_1
    :try_start_1
    iget v0, p1, Lsoftware/simplicial/a/f/s;->e:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1669
    sget-object v0, Lsoftware/simplicial/a/c/h;->a:Lsoftware/simplicial/a/c/h;

    iput-object v0, p0, Lsoftware/simplicial/a/t;->v:Lsoftware/simplicial/a/c/h;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1674
    :cond_2
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/u;)V
    .locals 1

    .prologue
    .line 517
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v0

    invoke-virtual {v0, p1}, Lsoftware/simplicial/a/u;->a(Lsoftware/simplicial/a/f/be;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 518
    monitor-exit p0

    return-void

    .line 517
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/x;)V
    .locals 6

    .prologue
    .line 1212
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Lsoftware/simplicial/a/f/x;->a:Lsoftware/simplicial/a/f/z;

    sget-object v1, Lsoftware/simplicial/a/f/z;->a:Lsoftware/simplicial/a/f/z;

    if-ne v0, v1, :cond_3

    .line 1215
    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->d:Lsoftware/simplicial/a/t$a;

    if-ne v0, v1, :cond_0

    .line 1216
    sget-object v0, Lsoftware/simplicial/a/t$a;->f:Lsoftware/simplicial/a/t$a;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/t;->b(Lsoftware/simplicial/a/t$a;)V

    .line 1217
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->c:Lsoftware/simplicial/a/t$a;

    if-ne v0, v1, :cond_1

    .line 1218
    sget-object v0, Lsoftware/simplicial/a/t$a;->e:Lsoftware/simplicial/a/t$a;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/t;->b(Lsoftware/simplicial/a/t$a;)V

    .line 1220
    :cond_1
    iget v0, p1, Lsoftware/simplicial/a/f/x;->f:F

    iput v0, p0, Lsoftware/simplicial/a/t;->u:F

    .line 1222
    iget v0, p1, Lsoftware/simplicial/a/f/x;->d:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 1224
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v0

    .line 1225
    iget v1, p1, Lsoftware/simplicial/a/f/x;->d:I

    iput v1, v0, Lsoftware/simplicial/a/u;->aZ:I

    .line 1227
    iget v0, p1, Lsoftware/simplicial/a/f/x;->d:I

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/t;->k(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1235
    :cond_2
    monitor-exit p0

    return-void

    .line 1232
    :cond_3
    :try_start_1
    iget-object v0, p0, Lsoftware/simplicial/a/t;->k:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/av;

    .line 1233
    iget-object v2, p1, Lsoftware/simplicial/a/f/x;->a:Lsoftware/simplicial/a/f/z;

    invoke-static {v2}, Lsoftware/simplicial/a/f/ba;->a(Lsoftware/simplicial/a/f/z;)Lsoftware/simplicial/a/f/ba;

    move-result-object v2

    const-wide/16 v4, 0x0

    iget v3, p1, Lsoftware/simplicial/a/f/x;->e:I

    invoke-interface {v0, v2, v4, v5, v3}, Lsoftware/simplicial/a/av;->a(Lsoftware/simplicial/a/f/ba;JI)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1212
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(Lsoftware/simplicial/a/t$a;)V
    .locals 2

    .prologue
    .line 327
    iget-object v0, p0, Lsoftware/simplicial/a/t;->b:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/w;

    .line 328
    invoke-interface {v0, p1}, Lsoftware/simplicial/a/w;->a(Lsoftware/simplicial/a/t$a;)V

    goto :goto_0

    .line 329
    :cond_0
    return-void
.end method

.method public static a(I)Z
    .locals 1

    .prologue
    .line 185
    sget-object v0, Lsoftware/simplicial/a/f/bh;->w:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->u:Lsoftware/simplicial/a/f/bh;

    .line 186
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->ax:Lsoftware/simplicial/a/f/bh;

    .line 187
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->Y:Lsoftware/simplicial/a/f/bh;

    .line 188
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->X:Lsoftware/simplicial/a/f/bh;

    .line 189
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->t:Lsoftware/simplicial/a/f/bh;

    .line 190
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->s:Lsoftware/simplicial/a/f/bh;

    .line 191
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->G:Lsoftware/simplicial/a/f/bh;

    .line 192
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->F:Lsoftware/simplicial/a/f/bh;

    .line 193
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->E:Lsoftware/simplicial/a/f/bh;

    .line 194
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->C:Lsoftware/simplicial/a/f/bh;

    .line 195
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->j:Lsoftware/simplicial/a/f/bh;

    .line 196
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->z:Lsoftware/simplicial/a/f/bh;

    .line 197
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->i:Lsoftware/simplicial/a/f/bh;

    .line 198
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->y:Lsoftware/simplicial/a/f/bh;

    .line 199
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->v:Lsoftware/simplicial/a/f/bh;

    .line 200
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->K:Lsoftware/simplicial/a/f/bh;

    .line 201
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->U:Lsoftware/simplicial/a/f/bh;

    .line 202
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->T:Lsoftware/simplicial/a/f/bh;

    .line 203
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->P:Lsoftware/simplicial/a/f/bh;

    .line 204
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->am:Lsoftware/simplicial/a/f/bh;

    .line 205
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->ap:Lsoftware/simplicial/a/f/bh;

    .line 206
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->as:Lsoftware/simplicial/a/f/bh;

    .line 207
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->ah:Lsoftware/simplicial/a/f/bh;

    .line 208
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->Z:Lsoftware/simplicial/a/f/bh;

    .line 209
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->l:Lsoftware/simplicial/a/f/bh;

    .line 210
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->b:Lsoftware/simplicial/a/f/bh;

    .line 211
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->p:Lsoftware/simplicial/a/f/bh;

    .line 212
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->h:Lsoftware/simplicial/a/f/bh;

    .line 213
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->ay:Lsoftware/simplicial/a/f/bh;

    .line 214
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->az:Lsoftware/simplicial/a/f/bh;

    .line 215
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->aD:Lsoftware/simplicial/a/f/bh;

    .line 216
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->aH:Lsoftware/simplicial/a/f/bh;

    .line 217
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->aL:Lsoftware/simplicial/a/f/bh;

    .line 218
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->aQ:Lsoftware/simplicial/a/f/bh;

    .line 219
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->aR:Lsoftware/simplicial/a/f/bh;

    .line 220
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-ne p0, v0, :cond_1

    .line 222
    :cond_0
    const/4 v0, 0x1

    .line 223
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized b(J)V
    .locals 13

    .prologue
    const-wide/16 v10, 0xa

    const/4 v3, 0x0

    const-wide/32 v8, 0x2bf20

    const-wide/16 v6, 0x3e8

    const-wide/16 v4, 0x0

    .line 272
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 273
    if-nez v0, :cond_1

    .line 323
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 276
    :cond_1
    :try_start_1
    iget-object v1, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v2, Lsoftware/simplicial/a/t$a;->h:Lsoftware/simplicial/a/t$a;

    if-ne v1, v2, :cond_2

    invoke-virtual {v0}, Lsoftware/simplicial/a/u;->d()Lsoftware/simplicial/a/bf;

    move-result-object v1

    iget v1, v1, Lsoftware/simplicial/a/bf;->S:I

    if-nez v1, :cond_2

    iget v1, v0, Lsoftware/simplicial/a/u;->g:F

    cmpl-float v1, v1, v3

    if-nez v1, :cond_2

    .line 277
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->b()V

    .line 279
    :cond_2
    iget-boolean v1, v0, Lsoftware/simplicial/a/u;->e:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lsoftware/simplicial/a/t;->t:Z

    if-eqz v1, :cond_3

    .line 280
    const/4 v1, 0x0

    iput-boolean v1, p0, Lsoftware/simplicial/a/t;->t:Z

    .line 282
    :cond_3
    iget-object v1, p0, Lsoftware/simplicial/a/t;->N:Lsoftware/simplicial/a/bj;

    sget-object v2, Lsoftware/simplicial/a/bj;->c:Lsoftware/simplicial/a/bj;

    if-ne v1, v2, :cond_8

    .line 284
    invoke-virtual {v0}, Lsoftware/simplicial/a/u;->d()Lsoftware/simplicial/a/bf;

    move-result-object v0

    .line 286
    if-eqz v0, :cond_4

    iget v0, v0, Lsoftware/simplicial/a/bf;->O:F

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_4

    .line 287
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->u()V

    .line 290
    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/a/t;->N:Lsoftware/simplicial/a/bj;

    sget-object v1, Lsoftware/simplicial/a/bj;->c:Lsoftware/simplicial/a/bj;

    if-ne v0, v1, :cond_6

    .line 292
    iget-wide v0, p0, Lsoftware/simplicial/a/t;->p:J

    sub-long v0, v8, v0

    const-wide/16 v2, 0x2710

    cmp-long v0, v0, v2

    if-gtz v0, :cond_5

    iget-wide v0, p0, Lsoftware/simplicial/a/t;->p:J

    const-wide/16 v2, 0x3e8

    rem-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-nez v0, :cond_5

    .line 293
    iget-object v0, p0, Lsoftware/simplicial/a/t;->b:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/w;

    .line 294
    iget-wide v2, p0, Lsoftware/simplicial/a/t;->p:J

    sub-long v2, v8, v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-int v2, v2

    invoke-interface {v0, v2}, Lsoftware/simplicial/a/w;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 272
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 289
    :cond_4
    :try_start_2
    iget-wide v0, p0, Lsoftware/simplicial/a/t;->p:J

    add-long/2addr v0, v10

    iput-wide v0, p0, Lsoftware/simplicial/a/t;->p:J

    goto :goto_1

    .line 296
    :cond_5
    iget-wide v0, p0, Lsoftware/simplicial/a/t;->p:J

    cmp-long v0, v0, v8

    if-ltz v0, :cond_6

    .line 298
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->p()V

    .line 299
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lsoftware/simplicial/a/t;->p:J

    .line 303
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/a/t;->J:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    sub-long v0, p1, v0

    const-wide/16 v2, 0x1770

    cmp-long v0, v0, v2

    if-lez v0, :cond_7

    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->a:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_7

    .line 305
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->p()V

    .line 306
    iget-object v0, p0, Lsoftware/simplicial/a/t;->b:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/w;

    .line 307
    invoke-interface {v0}, Lsoftware/simplicial/a/w;->a()V

    goto :goto_3

    .line 310
    :cond_7
    iget-object v0, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-interface {v0}, Lsoftware/simplicial/a/f/bg;->e()Lsoftware/simplicial/a/f/aa;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/a/f/aa;->a:Lsoftware/simplicial/a/f/aa;

    if-ne v0, v1, :cond_8

    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->a:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_8

    .line 311
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->p()V

    .line 315
    :cond_8
    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->g:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_9

    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->i:Lsoftware/simplicial/a/t$a;

    if-ne v0, v1, :cond_b

    .line 317
    :cond_9
    iget-wide v0, p0, Lsoftware/simplicial/a/t;->M:J

    add-long/2addr v0, v10

    iput-wide v0, p0, Lsoftware/simplicial/a/t;->M:J

    .line 318
    iget-wide v0, p0, Lsoftware/simplicial/a/t;->M:J

    cmp-long v0, v0, v6

    if-lez v0, :cond_0

    .line 319
    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->g:Lsoftware/simplicial/a/t$a;

    if-ne v0, v1, :cond_a

    sget-object v0, Lsoftware/simplicial/a/t$a;->f:Lsoftware/simplicial/a/t$a;

    :goto_4
    invoke-direct {p0, v0}, Lsoftware/simplicial/a/t;->b(Lsoftware/simplicial/a/t$a;)V

    goto/16 :goto_0

    :cond_a
    sget-object v0, Lsoftware/simplicial/a/t$a;->e:Lsoftware/simplicial/a/t$a;

    goto :goto_4

    .line 322
    :cond_b
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lsoftware/simplicial/a/t;->M:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method private declared-synchronized b(Lsoftware/simplicial/a/t$a;)V
    .locals 3

    .prologue
    .line 551
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->u()V

    .line 553
    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, p1, :cond_1

    .line 561
    :cond_0
    monitor-exit p0

    return-void

    .line 556
    :cond_1
    :try_start_1
    iget-object v1, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    .line 557
    iput-object p1, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    .line 559
    iget-object v0, p0, Lsoftware/simplicial/a/t;->b:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/w;

    .line 560
    invoke-interface {v0, p1, v1}, Lsoftware/simplicial/a/w;->a(Lsoftware/simplicial/a/t$a;Lsoftware/simplicial/a/t$a;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 551
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static b(I)Z
    .locals 1

    .prologue
    .line 228
    sget-object v0, Lsoftware/simplicial/a/f/bh;->w:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p0, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->i:Lsoftware/simplicial/a/f/bh;

    .line 229
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-ne p0, v0, :cond_1

    .line 230
    :cond_0
    const/4 v0, 0x1

    .line 231
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(J)V
    .locals 5

    .prologue
    .line 345
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->D:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/f/be;

    .line 346
    if-nez v0, :cond_1

    .line 354
    :cond_0
    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/a/t;->C:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/f/bi;

    .line 355
    if-nez v0, :cond_2

    .line 370
    return-void

    .line 349
    :cond_1
    invoke-direct {p0, v0, p1, p2}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/be;J)V

    goto :goto_0

    .line 361
    :cond_2
    :try_start_0
    sget-object v1, Lsoftware/simplicial/a/f/bh;->aS:[Lsoftware/simplicial/a/f/bh;

    iget v2, v0, Lsoftware/simplicial/a/f/bi;->a:I

    aget-object v1, v1, v2

    invoke-static {v1}, Lsoftware/simplicial/a/f/be;->a(Lsoftware/simplicial/a/f/bh;)Lsoftware/simplicial/a/f/be;

    move-result-object v1

    .line 362
    iget-object v2, v1, Lsoftware/simplicial/a/f/be;->aq:Lsoftware/simplicial/a/f/bh;

    sget-object v3, Lsoftware/simplicial/a/f/bh;->w:Lsoftware/simplicial/a/f/bh;

    if-eq v2, v3, :cond_3

    invoke-virtual {v1, v0}, Lsoftware/simplicial/a/f/be;->a(Lsoftware/simplicial/a/f/bi;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 363
    :cond_3
    invoke-direct {p0, v1, p1, p2}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/be;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 365
    :catch_0
    move-exception v1

    .line 367
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error parsing wrapper "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v0, v0, Lsoftware/simplicial/a/f/bi;->a:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private declared-synchronized k(I)V
    .locals 2

    .prologue
    .line 1436
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->T:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/f/ak;

    .line 1437
    :goto_0
    if-eqz v0, :cond_1

    .line 1439
    iget v1, v0, Lsoftware/simplicial/a/f/ak;->a:I

    if-ne v1, p1, :cond_0

    .line 1440
    invoke-direct {p0, v0}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/ak;)V

    .line 1441
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->T:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/f/ak;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1443
    :cond_1
    monitor-exit p0

    return-void

    .line 1436
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public A()Lsoftware/simplicial/a/u;
    .locals 2

    .prologue
    .line 1806
    iget-object v1, p0, Lsoftware/simplicial/a/t;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1808
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->E:Lsoftware/simplicial/a/u;

    monitor-exit v1

    return-object v0

    .line 1809
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public B()Lsoftware/simplicial/a/u;
    .locals 1

    .prologue
    .line 1814
    iget-object v0, p0, Lsoftware/simplicial/a/t;->E:Lsoftware/simplicial/a/u;

    return-object v0
.end method

.method public C()Lsoftware/simplicial/a/bf;
    .locals 1

    .prologue
    .line 1819
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->B()Lsoftware/simplicial/a/u;

    move-result-object v0

    invoke-virtual {v0}, Lsoftware/simplicial/a/u;->e()Lsoftware/simplicial/a/bf;

    move-result-object v0

    return-object v0
.end method

.method public a()Lsoftware/simplicial/a/f/aa;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-interface {v0}, Lsoftware/simplicial/a/f/bg;->e()Lsoftware/simplicial/a/f/aa;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized a(II)V
    .locals 5

    .prologue
    .line 658
    monitor-enter p0

    :try_start_0
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 659
    iget-object v1, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v2

    int-to-short v3, p1

    int-to-byte v4, p2

    invoke-static {v0, v2, v3, v4}, Lsoftware/simplicial/a/f/d;->a(Lsoftware/simplicial/a/f/bn;ISB)[B

    move-result-object v0

    invoke-interface {v1, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 660
    monitor-exit p0

    return-void

    .line 658
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(IILsoftware/simplicial/a/d/c;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/ap;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 11

    .prologue
    .line 644
    monitor-enter p0

    :try_start_0
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 645
    iget-object v10, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v1

    int-to-short v2, p1

    int-to-byte v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-static/range {v0 .. v9}, Lsoftware/simplicial/a/f/as;->a(Lsoftware/simplicial/a/f/bn;ISBLsoftware/simplicial/a/d/c;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/ap;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;)[B

    move-result-object v0

    invoke-interface {v10, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 647
    monitor-exit p0

    return-void

    .line 644
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(IIZ)V
    .locals 5

    .prologue
    .line 652
    monitor-enter p0

    :try_start_0
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 653
    iget-object v1, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v2

    int-to-short v3, p1

    int-to-byte v4, p2

    invoke-static {v0, v2, v3, v4, p3}, Lsoftware/simplicial/a/f/o;->a(Lsoftware/simplicial/a/f/bn;ISBZ)[B

    move-result-object v0

    invoke-interface {v1, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 654
    monitor-exit p0

    return-void

    .line 652
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(IIZZ)V
    .locals 7

    .prologue
    .line 670
    monitor-enter p0

    :try_start_0
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 671
    iget-object v6, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v1

    int-to-short v2, p1

    int-to-byte v3, p2

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lsoftware/simplicial/a/f/cq;->a(Lsoftware/simplicial/a/f/bn;ISBZZ)[B

    move-result-object v0

    invoke-interface {v6, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 672
    monitor-exit p0

    return-void

    .line 670
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(IJLsoftware/simplicial/a/s;J)V
    .locals 3

    .prologue
    .line 1084
    monitor-enter p0

    :try_start_0
    new-instance v0, Lsoftware/simplicial/a/f/cx;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/cx;-><init>()V

    .line 1085
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v1

    iput v1, v0, Lsoftware/simplicial/a/f/cx;->ar:I

    .line 1086
    const/4 v1, 0x0

    iput-boolean v1, v0, Lsoftware/simplicial/a/f/cx;->b:Z

    .line 1087
    const/4 v1, 0x0

    iput-boolean v1, v0, Lsoftware/simplicial/a/f/cx;->g:Z

    .line 1088
    const/4 v1, 0x1

    iput-boolean v1, v0, Lsoftware/simplicial/a/f/cx;->j:Z

    .line 1089
    iput p1, v0, Lsoftware/simplicial/a/f/cx;->k:I

    .line 1090
    iput-wide p2, v0, Lsoftware/simplicial/a/f/cx;->l:J

    .line 1091
    const/4 v1, 0x1

    iput-boolean v1, v0, Lsoftware/simplicial/a/f/cx;->m:Z

    .line 1092
    iput-object p4, v0, Lsoftware/simplicial/a/f/cx;->n:Lsoftware/simplicial/a/s;

    .line 1093
    iput-wide p5, v0, Lsoftware/simplicial/a/f/cx;->o:J

    .line 1094
    const/4 v1, 0x0

    iput-boolean v1, v0, Lsoftware/simplicial/a/f/cx;->p:Z

    .line 1095
    const/4 v1, 0x0

    iput-boolean v1, v0, Lsoftware/simplicial/a/f/cx;->z:Z

    .line 1096
    const/4 v1, 0x0

    iput-boolean v1, v0, Lsoftware/simplicial/a/f/cx;->B:Z

    .line 1098
    iget-object v1, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    new-instance v2, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v2}, Lsoftware/simplicial/a/f/bn;-><init>()V

    invoke-virtual {v0, v2}, Lsoftware/simplicial/a/f/cx;->a(Lsoftware/simplicial/a/f/bn;)[B

    move-result-object v0

    invoke-interface {v1, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1099
    monitor-exit p0

    return-void

    .line 1084
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(ILjava/lang/String;)V
    .locals 3

    .prologue
    const/16 v1, 0xc8

    .line 787
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->u()V

    .line 789
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 790
    const/4 v0, 0x0

    const/16 v1, 0xc8

    invoke-virtual {p2, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p2

    .line 792
    :cond_0
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 793
    iget-object v1, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v2

    invoke-static {v0, v2, p1, p2}, Lsoftware/simplicial/a/f/ag;->a(Lsoftware/simplicial/a/f/bn;IILjava/lang/String;)[B

    move-result-object v0

    invoke-interface {v1, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 794
    monitor-exit p0

    return-void

    .line 787
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(ILsoftware/simplicial/a/d/c;)V
    .locals 3

    .prologue
    .line 1014
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->f:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->e:Lsoftware/simplicial/a/t$a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, v1, :cond_0

    .line 1020
    :goto_0
    monitor-exit p0

    return-void

    .line 1017
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->u()V

    .line 1018
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 1019
    iget-object v1, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v2

    invoke-static {v0, v2, p1, p2}, Lsoftware/simplicial/a/f/aw;->a(Lsoftware/simplicial/a/f/bn;IILsoftware/simplicial/a/d/c;)[B

    move-result-object v0

    invoke-interface {v1, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1014
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(IZ)V
    .locals 2

    .prologue
    .line 1769
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lsoftware/simplicial/a/t;->Q:Z

    .line 1770
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 1771
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v1

    invoke-static {v0, v1, p1, p2}, Lsoftware/simplicial/a/f/cj;->a(Lsoftware/simplicial/a/f/bn;IIZ)[B

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/a/t;->S:[B

    .line 1772
    iget-object v0, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    iget-object v1, p0, Lsoftware/simplicial/a/t;->S:[B

    invoke-interface {v0, v1}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1773
    monitor-exit p0

    return-void

    .line 1769
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(J)V
    .locals 3

    .prologue
    .line 1239
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->J:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 1240
    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->e:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->j:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->i:Lsoftware/simplicial/a/t$a;

    if-ne v0, v1, :cond_1

    .line 1241
    :cond_0
    sget-object v0, Lsoftware/simplicial/a/t$a;->f:Lsoftware/simplicial/a/t$a;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/t;->b(Lsoftware/simplicial/a/t$a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1242
    :cond_1
    monitor-exit p0

    return-void

    .line 1239
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/16 v1, 0x64

    .line 754
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->u()V

    .line 756
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 757
    const/4 v0, 0x0

    const/16 v1, 0x64

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 759
    :cond_0
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 760
    iget-object v1, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v2

    invoke-static {v0, v2, p1}, Lsoftware/simplicial/a/f/k;->a(Lsoftware/simplicial/a/f/bn;ILjava/lang/String;)[B

    move-result-object v0

    invoke-interface {v1, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 761
    monitor-exit p0

    return-void

    .line 754
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;ILsoftware/simplicial/a/am;Lsoftware/simplicial/a/y;IILjava/lang/String;Lsoftware/simplicial/a/x;Ljava/lang/String;ZLjava/lang/String;[BLsoftware/simplicial/a/e;Lsoftware/simplicial/a/af;IBILjava/lang/String;Lsoftware/simplicial/a/as;)V
    .locals 22

    .prologue
    .line 831
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v3, Lsoftware/simplicial/a/t$a;->a:Lsoftware/simplicial/a/t$a;

    if-eq v2, v3, :cond_0

    .line 832
    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/a/t;->p()V

    .line 834
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/a/t;->u()V

    .line 835
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/t;->J:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 837
    invoke-static/range {p3 .. p3}, Lsoftware/simplicial/a/ai;->c(Lsoftware/simplicial/a/am;)Lsoftware/simplicial/a/ap;

    move-result-object v8

    .line 838
    sget-object v2, Lsoftware/simplicial/a/y;->c:Lsoftware/simplicial/a/y;

    move-object/from16 v0, p4

    if-ne v0, v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/a/t;->Q:Z

    .line 839
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/t;->G:Lsoftware/simplicial/a/f/cv;

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    .line 840
    sget-object v2, Lsoftware/simplicial/a/bj;->c:Lsoftware/simplicial/a/bj;

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/t;->N:Lsoftware/simplicial/a/bj;

    .line 841
    sget-object v2, Lsoftware/simplicial/a/c/h;->a:Lsoftware/simplicial/a/c/h;

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/t;->v:Lsoftware/simplicial/a/c/h;

    .line 842
    sget-object v2, Lsoftware/simplicial/a/h/a$a;->a:Lsoftware/simplicial/a/h/a$a;

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/t;->z:Lsoftware/simplicial/a/h/a$a;

    .line 843
    sget-object v2, Lsoftware/simplicial/a/i/e;->a:Lsoftware/simplicial/a/i/e;

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/t;->y:Lsoftware/simplicial/a/i/e;

    .line 844
    move-object/from16 v0, p0

    iget-object v14, v0, Lsoftware/simplicial/a/t;->a:Ljava/lang/Object;

    monitor-enter v14
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 847
    :try_start_1
    new-instance v2, Lsoftware/simplicial/a/u;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/t;->F:Lsoftware/simplicial/a/al;

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    sget-object v7, Lsoftware/simplicial/a/aq;->a:Lsoftware/simplicial/a/aq;

    const/4 v5, 0x0

    new-array v10, v5, [B

    move-object/from16 v0, p0

    iget-object v12, v0, Lsoftware/simplicial/a/t;->N:Lsoftware/simplicial/a/bj;

    move-object/from16 v5, p3

    move/from16 v6, p5

    move-object/from16 v9, p9

    move/from16 v11, p6

    move/from16 v13, p10

    invoke-direct/range {v2 .. v13}, Lsoftware/simplicial/a/u;-><init>(Lsoftware/simplicial/a/al;Lsoftware/simplicial/a/f/bg;Lsoftware/simplicial/a/am;ILsoftware/simplicial/a/aq;Lsoftware/simplicial/a/ap;Ljava/lang/String;[BILsoftware/simplicial/a/bj;Z)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/t;->E:Lsoftware/simplicial/a/u;

    .line 849
    monitor-exit v14
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 850
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/t$a;)V

    .line 851
    const/4 v2, 0x0

    new-array v2, v2, [B

    move-object/from16 v0, p0

    move-object/from16 v1, p9

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/t;->a(Ljava/lang/String;[B)V

    .line 852
    sget-object v2, Lsoftware/simplicial/a/t$1;->c:[I

    invoke-virtual/range {p4 .. p4}, Lsoftware/simplicial/a/y;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 864
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/t;->G:Lsoftware/simplicial/a/f/cv;

    invoke-virtual/range {p3 .. p3}, Lsoftware/simplicial/a/am;->ordinal()I

    move-result v6

    invoke-virtual/range {p4 .. p4}, Lsoftware/simplicial/a/y;->ordinal()I

    move-result v7

    const/16 v11, 0x181

    move-object/from16 v3, p1

    move/from16 v4, p2

    move-object/from16 v5, p0

    move/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move/from16 v12, p10

    move-object/from16 v13, p11

    move-object/from16 v14, p12

    move-object/from16 v15, p13

    move-object/from16 v16, p14

    move/from16 v17, p15

    move/from16 v18, p16

    move/from16 v19, p17

    move-object/from16 v20, p18

    move-object/from16 v21, p19

    invoke-virtual/range {v2 .. v21}, Lsoftware/simplicial/a/f/cv;->a(Ljava/lang/String;ILsoftware/simplicial/a/f/cw;IIILjava/lang/String;Lsoftware/simplicial/a/x;SZLjava/lang/String;[BLsoftware/simplicial/a/e;Lsoftware/simplicial/a/af;IBILjava/lang/String;Lsoftware/simplicial/a/as;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 866
    sget-object v2, Lsoftware/simplicial/a/t$a;->a:Lsoftware/simplicial/a/t$a;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lsoftware/simplicial/a/t;->b(Lsoftware/simplicial/a/t$a;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 867
    :cond_1
    monitor-exit p0

    return-void

    .line 838
    :cond_2
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 849
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v14
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 831
    :catchall_1
    move-exception v2

    monitor-exit p0

    throw v2

    .line 855
    :pswitch_0
    :try_start_5
    sget-object v2, Lsoftware/simplicial/a/t$a;->d:Lsoftware/simplicial/a/t$a;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lsoftware/simplicial/a/t;->b(Lsoftware/simplicial/a/t$a;)V

    goto :goto_1

    .line 858
    :pswitch_1
    sget-object v2, Lsoftware/simplicial/a/t$a;->c:Lsoftware/simplicial/a/t$a;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lsoftware/simplicial/a/t;->b(Lsoftware/simplicial/a/t$a;)V

    goto :goto_1

    .line 861
    :pswitch_2
    sget-object v2, Lsoftware/simplicial/a/t$a;->b:Lsoftware/simplicial/a/t$a;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lsoftware/simplicial/a/t;->b(Lsoftware/simplicial/a/t$a;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1

    .line 852
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public declared-synchronized a(Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 3

    .prologue
    const/16 v1, 0x64

    .line 776
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->u()V

    .line 778
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 779
    const/4 v0, 0x0

    const/16 v1, 0x64

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 781
    :cond_0
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 782
    iget-object v1, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v2

    invoke-static {v0, v2, p1, p2, p3}, Lsoftware/simplicial/a/f/ai;->a(Lsoftware/simplicial/a/f/bn;ILjava/lang/String;Ljava/lang/String;[B)[B

    move-result-object v0

    invoke-interface {v1, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 783
    monitor-exit p0

    return-void

    .line 776
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;)V
    .locals 3

    .prologue
    .line 1169
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->e:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->i:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->j:Lsoftware/simplicial/a/t$a;

    if-ne v0, v1, :cond_1

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x10

    if-gt v0, v1, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->length()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-gez v0, :cond_2

    .line 1179
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 1172
    :cond_2
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lsoftware/simplicial/a/t;->Q:Z

    .line 1174
    iput-object p1, p0, Lsoftware/simplicial/a/t;->q:Ljava/lang/String;

    .line 1175
    iput-object p4, p0, Lsoftware/simplicial/a/t;->r:Ljava/lang/String;

    .line 1176
    sget-object v0, Lsoftware/simplicial/a/t$a;->i:Lsoftware/simplicial/a/t$a;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/t;->b(Lsoftware/simplicial/a/t$a;)V

    .line 1177
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 1178
    iget-object v1, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v2

    invoke-static {v0, v2, p1, p2, p3}, Lsoftware/simplicial/a/f/ap;->a(Lsoftware/simplicial/a/f/bn;ILjava/lang/String;Ljava/lang/String;[B)[B

    move-result-object v0

    invoke-interface {v1, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1169
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;ZIILjava/lang/String;[BLsoftware/simplicial/a/am;Lsoftware/simplicial/a/ac;Lsoftware/simplicial/a/d/c;Ljava/lang/String;Lsoftware/simplicial/a/ap;FSSZ[Z)V
    .locals 20

    .prologue
    .line 1153
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v3, Lsoftware/simplicial/a/t$a;->e:Lsoftware/simplicial/a/t$a;

    if-eq v2, v3, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v3, Lsoftware/simplicial/a/t$a;->i:Lsoftware/simplicial/a/t$a;

    if-eq v2, v3, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v3, Lsoftware/simplicial/a/t$a;->j:Lsoftware/simplicial/a/t$a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v2, v3, :cond_0

    .line 1165
    :goto_0
    monitor-exit p0

    return-void

    .line 1156
    :cond_0
    const/4 v2, 0x0

    :try_start_1
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/a/t;->Q:Z

    .line 1159
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lsoftware/simplicial/a/t;->q:Ljava/lang/String;

    .line 1160
    move-object/from16 v0, p10

    move-object/from16 v1, p0

    iput-object v0, v1, Lsoftware/simplicial/a/t;->r:Ljava/lang/String;

    .line 1161
    sget-object v2, Lsoftware/simplicial/a/t$a;->i:Lsoftware/simplicial/a/t$a;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lsoftware/simplicial/a/t;->b(Lsoftware/simplicial/a/t$a;)V

    .line 1162
    new-instance v2, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v2}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 1163
    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    move-object/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v3

    move-object/from16 v4, p1

    move/from16 v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p11

    move-object/from16 v12, p8

    move-object/from16 v13, p9

    move/from16 v14, p12

    move/from16 v15, p13

    move/from16 v16, p14

    move/from16 v17, p15

    move-object/from16 v18, p16

    invoke-static/range {v2 .. v18}, Lsoftware/simplicial/a/f/an;->a(Lsoftware/simplicial/a/f/bn;ILjava/lang/String;ZIILjava/lang/String;[BLsoftware/simplicial/a/am;Lsoftware/simplicial/a/ap;Lsoftware/simplicial/a/ac;Lsoftware/simplicial/a/d/c;FSSZ[Z)[B

    move-result-object v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1153
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized a(Ljava/lang/String;[BLsoftware/simplicial/a/e;Lsoftware/simplicial/a/af;ILsoftware/simplicial/a/bd;ILjava/lang/String;Lsoftware/simplicial/a/as;)V
    .locals 14

    .prologue
    .line 1103
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v3, Lsoftware/simplicial/a/t$a;->f:Lsoftware/simplicial/a/t$a;

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v3, Lsoftware/simplicial/a/t$a;->g:Lsoftware/simplicial/a/t$a;

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v3, Lsoftware/simplicial/a/t$a;->h:Lsoftware/simplicial/a/t$a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v2, v3, :cond_1

    .line 1127
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1106
    :cond_1
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lsoftware/simplicial/a/t;->U:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    .line 1108
    iget-object v2, p0, Lsoftware/simplicial/a/t;->k:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/av;

    .line 1109
    sget-object v4, Lsoftware/simplicial/a/f/ba;->o:Lsoftware/simplicial/a/f/ba;

    const-wide/16 v6, 0x0

    const/4 v5, -0x1

    invoke-interface {v2, v4, v6, v7, v5}, Lsoftware/simplicial/a/av;->a(Lsoftware/simplicial/a/f/ba;JI)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1103
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 1112
    :cond_2
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lsoftware/simplicial/a/t;->U:J

    .line 1114
    const/4 v2, 0x0

    iput-boolean v2, p0, Lsoftware/simplicial/a/t;->Q:Z

    .line 1116
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v2

    .line 1117
    const/high16 v3, -0x40800000    # -1.0f

    iput v3, v2, Lsoftware/simplicial/a/u;->g:F

    .line 1118
    iget-boolean v3, v2, Lsoftware/simplicial/a/u;->q:Z

    if-nez v3, :cond_3

    iget-boolean v3, v2, Lsoftware/simplicial/a/u;->t:Z

    if-eqz v3, :cond_4

    :cond_3
    iget-boolean v3, p0, Lsoftware/simplicial/a/t;->t:Z

    if-nez v3, :cond_5

    .line 1119
    :cond_4
    const/4 v3, -0x1

    iput v3, v2, Lsoftware/simplicial/a/u;->j:I

    .line 1120
    :cond_5
    new-instance v3, Lsoftware/simplicial/a/f/bc;

    invoke-direct {v3}, Lsoftware/simplicial/a/f/bc;-><init>()V

    .line 1121
    invoke-virtual {v2, v3}, Lsoftware/simplicial/a/u;->a(Lsoftware/simplicial/a/f/be;)V

    .line 1123
    sget-object v2, Lsoftware/simplicial/a/t$a;->g:Lsoftware/simplicial/a/t$a;

    invoke-direct {p0, v2}, Lsoftware/simplicial/a/t;->b(Lsoftware/simplicial/a/t$a;)V

    .line 1124
    new-instance v2, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v2}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 1125
    iget-object v13, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v3

    move-object/from16 v0, p4

    iget-byte v6, v0, Lsoftware/simplicial/a/af;->c:B

    invoke-virtual/range {p3 .. p3}, Lsoftware/simplicial/a/e;->a()I

    move-result v4

    int-to-short v8, v4

    move-object/from16 v0, p6

    iget-byte v9, v0, Lsoftware/simplicial/a/bd;->c:B

    move-object v4, p1

    move-object/from16 v5, p2

    move/from16 v7, p5

    move/from16 v10, p7

    move-object/from16 v11, p8

    move-object/from16 v12, p9

    invoke-static/range {v2 .. v12}, Lsoftware/simplicial/a/f/az;->a(Lsoftware/simplicial/a/f/bn;ILjava/lang/String;[BBISBILjava/lang/String;Lsoftware/simplicial/a/as;)[B

    move-result-object v2

    invoke-interface {v13, v2}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public declared-synchronized a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bg;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 742
    monitor-enter p0

    :try_start_0
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 743
    iget-object v1, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v2

    invoke-static {v0, v2, p1}, Lsoftware/simplicial/a/f/a;->a(Lsoftware/simplicial/a/f/bn;ILjava/util/List;)[B

    move-result-object v0

    invoke-interface {v1, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 744
    monitor-exit p0

    return-void

    .line 742
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lsoftware/simplicial/a/am;)V
    .locals 3

    .prologue
    .line 710
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->u()V

    .line 712
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 713
    iget-object v1, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v2

    invoke-static {v0, v2, p1}, Lsoftware/simplicial/a/f/l;->a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/am;)[B

    move-result-object v0

    invoke-interface {v1, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V

    .line 714
    return-void
.end method

.method public declared-synchronized a(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/f/bg;ILjava/lang/String;Lsoftware/simplicial/a/ap;IZLjava/lang/String;[BLsoftware/simplicial/a/e;Lsoftware/simplicial/a/af;IBILjava/lang/String;Lsoftware/simplicial/a/as;)V
    .locals 24

    .prologue
    .line 1055
    monitor-enter p0

    const/4 v2, 0x0

    :try_start_0
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/a/t;->Q:Z

    .line 1056
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    .line 1057
    sget-object v2, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/t;->N:Lsoftware/simplicial/a/bj;

    .line 1058
    sget-object v2, Lsoftware/simplicial/a/c/h;->a:Lsoftware/simplicial/a/c/h;

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/t;->v:Lsoftware/simplicial/a/c/h;

    .line 1059
    sget-object v2, Lsoftware/simplicial/a/h/a$a;->a:Lsoftware/simplicial/a/h/a$a;

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/t;->z:Lsoftware/simplicial/a/h/a$a;

    .line 1060
    sget-object v2, Lsoftware/simplicial/a/i/e;->a:Lsoftware/simplicial/a/i/e;

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/t;->y:Lsoftware/simplicial/a/i/e;

    .line 1061
    move-object/from16 v0, p0

    iget-object v14, v0, Lsoftware/simplicial/a/t;->a:Ljava/lang/Object;

    monitor-enter v14
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1064
    :try_start_1
    new-instance v2, Lsoftware/simplicial/a/u;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/t;->F:Lsoftware/simplicial/a/al;

    sget-object v7, Lsoftware/simplicial/a/aq;->a:Lsoftware/simplicial/a/aq;

    const/4 v4, 0x0

    new-array v10, v4, [B

    move-object/from16 v0, p0

    iget-object v12, v0, Lsoftware/simplicial/a/t;->N:Lsoftware/simplicial/a/bj;

    move-object/from16 v4, p2

    move-object/from16 v5, p1

    move/from16 v6, p3

    move-object/from16 v8, p5

    move-object/from16 v9, p4

    move/from16 v11, p6

    move/from16 v13, p7

    invoke-direct/range {v2 .. v13}, Lsoftware/simplicial/a/u;-><init>(Lsoftware/simplicial/a/al;Lsoftware/simplicial/a/f/bg;Lsoftware/simplicial/a/am;ILsoftware/simplicial/a/aq;Lsoftware/simplicial/a/ap;Ljava/lang/String;[BILsoftware/simplicial/a/bj;Z)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/t;->E:Lsoftware/simplicial/a/u;

    .line 1066
    monitor-exit v14
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1067
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/t$a;)V

    .line 1068
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lsoftware/simplicial/a/t;->q:Ljava/lang/String;

    .line 1069
    new-instance v2, Lsoftware/simplicial/a/f/w;

    invoke-direct {v2}, Lsoftware/simplicial/a/f/w;-><init>()V

    .line 1070
    const/4 v3, 0x1

    iput v3, v2, Lsoftware/simplicial/a/f/w;->a:I

    .line 1071
    sget-object v3, Lsoftware/simplicial/a/y;->a:Lsoftware/simplicial/a/y;

    invoke-virtual {v3}, Lsoftware/simplicial/a/y;->ordinal()I

    move-result v3

    iput v3, v2, Lsoftware/simplicial/a/f/w;->c:I

    .line 1072
    const/16 v3, 0x181

    iput-short v3, v2, Lsoftware/simplicial/a/f/w;->g:S

    .line 1073
    new-instance v4, Lsoftware/simplicial/a/f/bl;

    invoke-direct {v4}, Lsoftware/simplicial/a/f/bl;-><init>()V

    .line 1074
    const/16 v3, 0x181

    iput-short v3, v4, Lsoftware/simplicial/a/f/bl;->d:S

    .line 1075
    new-instance v3, Lsoftware/simplicial/a/v;

    const/4 v5, -0x1

    const/4 v6, 0x0

    const-string v7, ""

    sget-object v8, Lsoftware/simplicial/a/a;->b:Lsoftware/simplicial/a/a;

    const-string v9, ""

    const/4 v10, 0x0

    new-array v10, v10, [B

    sget-object v11, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    new-instance v12, Lsoftware/simplicial/a/bc;

    invoke-direct {v12}, Lsoftware/simplicial/a/bc;-><init>()V

    const-wide/16 v18, 0x0

    move-object/from16 v13, p8

    move-object/from16 v14, p9

    move-object/from16 v15, p10

    move-object/from16 v16, p11

    move/from16 v17, p12

    move/from16 v20, p13

    move/from16 v21, p14

    move-object/from16 v22, p15

    move-object/from16 v23, p16

    invoke-direct/range {v3 .. v23}, Lsoftware/simplicial/a/v;-><init>(Lsoftware/simplicial/a/f/bl;IILjava/lang/String;Lsoftware/simplicial/a/a;Ljava/lang/String;[BLsoftware/simplicial/a/q;Lsoftware/simplicial/a/bc;Ljava/lang/String;[BLsoftware/simplicial/a/e;Lsoftware/simplicial/a/af;IJBILjava/lang/String;Lsoftware/simplicial/a/as;)V

    iput-object v3, v2, Lsoftware/simplicial/a/f/w;->s:Ljava/lang/Object;

    .line 1077
    new-instance v3, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v3}, Lsoftware/simplicial/a/f/bn;-><init>()V

    invoke-virtual {v2, v3}, Lsoftware/simplicial/a/f/w;->a(Lsoftware/simplicial/a/f/bn;)[B

    move-result-object v2

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Lsoftware/simplicial/a/f/bg;->a([B)V

    .line 1079
    sget-object v2, Lsoftware/simplicial/a/t$a;->d:Lsoftware/simplicial/a/t$a;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lsoftware/simplicial/a/t;->b(Lsoftware/simplicial/a/t$a;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1080
    monitor-exit p0

    return-void

    .line 1066
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v14
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1055
    :catchall_1
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized a(Lsoftware/simplicial/a/au;I)V
    .locals 3

    .prologue
    .line 1036
    monitor-enter p0

    :try_start_0
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 1037
    iget-object v1, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v2

    invoke-static {v0, v2, p1, p2}, Lsoftware/simplicial/a/f/ay;->a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/au;I)[B

    move-result-object v0

    invoke-interface {v1, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1038
    monitor-exit p0

    return-void

    .line 1036
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lsoftware/simplicial/a/b/d;ZZ)V
    .locals 3

    .prologue
    .line 1749
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lsoftware/simplicial/a/t;->Q:Z

    .line 1751
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 1752
    iget-object v1, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v2

    invoke-static {v0, v2, p1, p3, p2}, Lsoftware/simplicial/a/f/f;->a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/b/d;ZZ)[B

    move-result-object v0

    invoke-interface {v1, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1753
    monitor-exit p0

    return-void

    .line 1749
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lsoftware/simplicial/a/bg;)V
    .locals 7

    .prologue
    .line 871
    monitor-enter p0

    :try_start_0
    const-string v1, ""

    const/4 v2, -0x1

    const/4 v4, -0x1

    const/4 v5, 0x0

    const/4 v6, -0x1

    move-object v0, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v6}, Lsoftware/simplicial/a/t;->a(Ljava/lang/String;ILsoftware/simplicial/a/bg;III)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 872
    monitor-exit p0

    return-void

    .line 871
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lsoftware/simplicial/a/bg;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 876
    monitor-enter p0

    const/4 v2, -0x1

    const/4 v4, -0x1

    const/4 v5, 0x0

    const/4 v6, -0x1

    move-object v0, p0

    move-object v1, p2

    move-object v3, p1

    :try_start_0
    invoke-direct/range {v0 .. v6}, Lsoftware/simplicial/a/t;->a(Ljava/lang/String;ILsoftware/simplicial/a/bg;III)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 877
    monitor-exit p0

    return-void

    .line 876
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lsoftware/simplicial/a/c/b;II)V
    .locals 10

    .prologue
    .line 676
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->u()V

    .line 678
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lsoftware/simplicial/a/t;->U:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 680
    iget-object v0, p0, Lsoftware/simplicial/a/t;->k:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/av;

    .line 681
    sget-object v2, Lsoftware/simplicial/a/f/ba;->o:Lsoftware/simplicial/a/f/ba;

    const-wide/16 v4, 0x0

    const/4 v3, -0x1

    invoke-interface {v0, v2, v4, v5, v3}, Lsoftware/simplicial/a/av;->a(Lsoftware/simplicial/a/f/ba;JI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 676
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 684
    :cond_0
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lsoftware/simplicial/a/t;->U:J

    .line 687
    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->f:Lsoftware/simplicial/a/t$a;

    if-ne v0, v1, :cond_2

    .line 688
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/a/t;->O:Z

    .line 692
    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/a/t;->Q:Z

    .line 694
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 695
    iget-object v9, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v1

    sget-object v3, Lsoftware/simplicial/a/c/e;->a:Lsoftware/simplicial/a/c/e;

    sget-object v4, Lsoftware/simplicial/a/c/g;->a:Lsoftware/simplicial/a/c/g;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, p1

    move v5, p2

    move v8, p3

    invoke-static/range {v0 .. v8}, Lsoftware/simplicial/a/f/m;->a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/c/b;Lsoftware/simplicial/a/c/e;Lsoftware/simplicial/a/c/g;IZZI)[B

    move-result-object v0

    invoke-interface {v9, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 697
    :cond_1
    monitor-exit p0

    return-void

    .line 690
    :cond_2
    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, Lsoftware/simplicial/a/t;->O:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public declared-synchronized a(Lsoftware/simplicial/a/c/b;Lsoftware/simplicial/a/c/e;Lsoftware/simplicial/a/c/g;ZZ)V
    .locals 10

    .prologue
    .line 718
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->u()V

    .line 720
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lsoftware/simplicial/a/t;->U:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 722
    iget-object v0, p0, Lsoftware/simplicial/a/t;->k:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/av;

    .line 723
    sget-object v2, Lsoftware/simplicial/a/f/ba;->o:Lsoftware/simplicial/a/f/ba;

    const-wide/16 v4, 0x0

    const/4 v3, -0x1

    invoke-interface {v0, v2, v4, v5, v3}, Lsoftware/simplicial/a/av;->a(Lsoftware/simplicial/a/f/ba;JI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 718
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 726
    :cond_0
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lsoftware/simplicial/a/t;->U:J

    .line 729
    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->f:Lsoftware/simplicial/a/t$a;

    if-ne v0, v1, :cond_2

    .line 730
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/a/t;->O:Z

    .line 733
    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/a/t;->Q:Z

    .line 735
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 736
    iget-object v9, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v1

    const/4 v5, -0x1

    const/4 v8, -0x1

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v6, p4

    move v7, p5

    invoke-static/range {v0 .. v8}, Lsoftware/simplicial/a/f/m;->a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/c/b;Lsoftware/simplicial/a/c/e;Lsoftware/simplicial/a/c/g;IZZI)[B

    move-result-object v0

    invoke-interface {v9, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 738
    :cond_1
    monitor-exit p0

    return-void

    .line 731
    :cond_2
    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, Lsoftware/simplicial/a/t;->O:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public a(Lsoftware/simplicial/a/f/be;)V
    .locals 1

    .prologue
    .line 1801
    iget-object v0, p0, Lsoftware/simplicial/a/t;->D:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 1802
    return-void
.end method

.method public a(Lsoftware/simplicial/a/f/bi;)V
    .locals 1

    .prologue
    .line 1787
    iget v0, p1, Lsoftware/simplicial/a/f/bi;->a:I

    invoke-static {v0}, Lsoftware/simplicial/a/t;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1789
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v0

    invoke-virtual {v0, p1}, Lsoftware/simplicial/a/u;->a(Lsoftware/simplicial/a/f/bi;)V

    .line 1792
    :cond_0
    iget v0, p1, Lsoftware/simplicial/a/f/bi;->a:I

    invoke-static {v0}, Lsoftware/simplicial/a/t;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1794
    iget-object v0, p0, Lsoftware/simplicial/a/t;->C:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 1796
    :cond_1
    return-void
.end method

.method public declared-synchronized a(Lsoftware/simplicial/a/i/c;ZZI)V
    .locals 8

    .prologue
    .line 1738
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->u()V

    .line 1740
    sget-object v0, Lsoftware/simplicial/a/i/c;->d:Lsoftware/simplicial/a/i/c;

    if-ne p1, v0, :cond_0

    .line 1741
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/a/t;->Q:Z

    .line 1743
    :cond_0
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 1744
    iget-object v7, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v1

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->s()I

    move-result v2

    move-object v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-static/range {v0 .. v6}, Lsoftware/simplicial/a/f/cp;->a(Lsoftware/simplicial/a/f/bn;IILsoftware/simplicial/a/i/c;ZZI)[B

    move-result-object v0

    invoke-interface {v7, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1745
    monitor-exit p0

    return-void

    .line 1738
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lsoftware/simplicial/a/l;IZ)V
    .locals 3

    .prologue
    .line 1024
    monitor-enter p0

    :try_start_0
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 1025
    iget-object v1, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v2

    invoke-static {v0, v2, p1, p2, p3}, Lsoftware/simplicial/a/f/i;->a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/l;IZ)[B

    move-result-object v0

    invoke-interface {v1, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1026
    monitor-exit p0

    return-void

    .line 1024
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lsoftware/simplicial/a/p;)V
    .locals 6

    .prologue
    .line 987
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->a:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->c:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->d:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->b:Lsoftware/simplicial/a/t$a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_1

    .line 1010
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 990
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->u()V

    .line 992
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lsoftware/simplicial/a/t;->U:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 994
    iget-object v0, p0, Lsoftware/simplicial/a/t;->k:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/av;

    .line 995
    sget-object v2, Lsoftware/simplicial/a/f/ba;->o:Lsoftware/simplicial/a/f/ba;

    const-wide/16 v4, 0x0

    const/4 v3, -0x1

    invoke-interface {v0, v2, v4, v5, v3}, Lsoftware/simplicial/a/av;->a(Lsoftware/simplicial/a/f/ba;JI)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 987
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 998
    :cond_2
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lsoftware/simplicial/a/t;->U:J

    .line 1000
    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->f:Lsoftware/simplicial/a/t$a;

    if-ne v0, v1, :cond_3

    .line 1001
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/a/t;->O:Z

    .line 1004
    :goto_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/a/t;->Q:Z

    .line 1006
    sget-object v0, Lsoftware/simplicial/a/bj;->c:Lsoftware/simplicial/a/bj;

    iput-object v0, p0, Lsoftware/simplicial/a/t;->N:Lsoftware/simplicial/a/bj;

    .line 1008
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 1009
    iget-object v1, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v2

    iget-object v3, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-interface {v3}, Lsoftware/simplicial/a/f/bg;->b()I

    move-result v3

    invoke-static {v0, v2, v3, p1}, Lsoftware/simplicial/a/f/ae;->a(Lsoftware/simplicial/a/f/bn;IILsoftware/simplicial/a/p;)[B

    move-result-object v0

    invoke-interface {v1, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V

    goto :goto_0

    .line 1002
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/a/t;->O:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public declared-synchronized a(Lsoftware/simplicial/a/x;)V
    .locals 3

    .prologue
    .line 748
    monitor-enter p0

    :try_start_0
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 749
    iget-object v1, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v2

    invoke-static {v0, v2, p1}, Lsoftware/simplicial/a/f/cc;->a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/x;)[B

    move-result-object v0

    invoke-interface {v1, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 750
    monitor-exit p0

    return-void

    .line 748
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 241
    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->h:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->g:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->i:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->j:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_0

    .line 244
    :goto_0
    return-void

    .line 243
    :cond_0
    sget-object v0, Lsoftware/simplicial/a/t$a;->f:Lsoftware/simplicial/a/t$a;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/t;->b(Lsoftware/simplicial/a/t$a;)V

    goto :goto_0
.end method

.method public declared-synchronized b(II)V
    .locals 5

    .prologue
    .line 664
    monitor-enter p0

    :try_start_0
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 665
    iget-object v1, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v2

    int-to-short v3, p1

    int-to-byte v4, p2

    invoke-static {v0, v2, v3, v4}, Lsoftware/simplicial/a/f/ch;->a(Lsoftware/simplicial/a/f/bn;ISB)[B

    move-result-object v0

    invoke-interface {v1, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 666
    monitor-exit p0

    return-void

    .line 664
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(ILjava/lang/String;)V
    .locals 3

    .prologue
    const/16 v1, 0xc8

    .line 798
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->u()V

    .line 800
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 801
    const/4 v0, 0x0

    const/16 v1, 0xc8

    invoke-virtual {p2, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p2

    .line 803
    :cond_0
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 804
    iget-object v1, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v2

    invoke-static {v0, v2, p1, p2}, Lsoftware/simplicial/a/f/bs;->a(Lsoftware/simplicial/a/f/bn;IILjava/lang/String;)[B

    move-result-object v0

    invoke-interface {v1, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 805
    monitor-exit p0

    return-void

    .line 798
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/16 v1, 0x64

    .line 765
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->u()V

    .line 767
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 768
    const/4 v0, 0x0

    const/16 v1, 0x64

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 770
    :cond_0
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 771
    iget-object v1, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v2

    invoke-static {v0, v2, p1}, Lsoftware/simplicial/a/f/bt;->a(Lsoftware/simplicial/a/f/bn;ILjava/lang/String;)[B

    move-result-object v0

    invoke-interface {v1, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 772
    monitor-exit p0

    return-void

    .line 765
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Lsoftware/simplicial/a/bg;)V
    .locals 2

    .prologue
    .line 937
    monitor-enter p0

    :try_start_0
    const-string v0, ""

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1, p1}, Lsoftware/simplicial/a/t;->a(Ljava/lang/String;ILsoftware/simplicial/a/bg;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 938
    monitor-exit p0

    return-void

    .line 937
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Lsoftware/simplicial/a/bg;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 942
    monitor-enter p0

    const/4 v0, -0x1

    :try_start_0
    invoke-direct {p0, p2, v0, p1}, Lsoftware/simplicial/a/t;->a(Ljava/lang/String;ILsoftware/simplicial/a/bg;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 943
    monitor-exit p0

    return-void

    .line 942
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 333
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v0

    invoke-virtual {v0}, Lsoftware/simplicial/a/u;->l()V

    .line 334
    return-void
.end method

.method public declared-synchronized c(I)V
    .locals 7

    .prologue
    .line 886
    monitor-enter p0

    :try_start_0
    const-string v1, ""

    const/4 v3, 0x0

    const/4 v4, -0x1

    const/4 v5, 0x0

    const/4 v6, -0x1

    move-object v0, p0

    move v2, p1

    invoke-direct/range {v0 .. v6}, Lsoftware/simplicial/a/t;->a(Ljava/lang/String;ILsoftware/simplicial/a/bg;III)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 887
    monitor-exit p0

    return-void

    .line 886
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 881
    monitor-enter p0

    const/4 v2, -0x1

    const/4 v3, 0x0

    const/4 v4, -0x1

    const/4 v5, 0x0

    const/4 v6, -0x1

    move-object v0, p0

    move-object v1, p1

    :try_start_0
    invoke-direct/range {v0 .. v6}, Lsoftware/simplicial/a/t;->a(Ljava/lang/String;ILsoftware/simplicial/a/bg;III)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 882
    monitor-exit p0

    return-void

    .line 881
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 338
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v0

    invoke-virtual {v0}, Lsoftware/simplicial/a/u;->m()V

    .line 339
    return-void
.end method

.method public declared-synchronized d(I)V
    .locals 7

    .prologue
    .line 891
    monitor-enter p0

    :try_start_0
    const-string v1, ""

    const/4 v2, -0x1

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, -0x1

    move-object v0, p0

    move v4, p1

    invoke-direct/range {v0 .. v6}, Lsoftware/simplicial/a/t;->a(Ljava/lang/String;ILsoftware/simplicial/a/bg;III)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 892
    monitor-exit p0

    return-void

    .line 891
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 947
    monitor-enter p0

    const/4 v0, -0x1

    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/a/t;->a(Ljava/lang/String;ILsoftware/simplicial/a/bg;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 948
    monitor-exit p0

    return-void

    .line 947
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 537
    new-instance v0, Lsoftware/simplicial/a/f/ac;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/ac;-><init>()V

    .line 538
    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/be;)V

    .line 539
    return-void
.end method

.method public declared-synchronized e(I)V
    .locals 7

    .prologue
    .line 896
    monitor-enter p0

    :try_start_0
    const-string v1, ""

    const/4 v2, -0x1

    const/4 v3, 0x0

    const/4 v4, -0x1

    const/4 v5, 0x0

    move-object v0, p0

    move v6, p1

    invoke-direct/range {v0 .. v6}, Lsoftware/simplicial/a/t;->a(Ljava/lang/String;ILsoftware/simplicial/a/bg;III)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 897
    monitor-exit p0

    return-void

    .line 896
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized e(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1141
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->f:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->g:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->h:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->e:Lsoftware/simplicial/a/t$a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, v1, :cond_1

    .line 1147
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1145
    :cond_1
    :try_start_1
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 1146
    iget-object v1, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v2

    invoke-static {v0, v2, p1}, Lsoftware/simplicial/a/f/ax;->a(Lsoftware/simplicial/a/f/bn;ILjava/lang/String;)[B

    move-result-object v0

    invoke-interface {v1, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1141
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public f()Lsoftware/simplicial/a/t$a;
    .locals 1

    .prologue
    .line 543
    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    return-object v0
.end method

.method public declared-synchronized f(I)V
    .locals 7

    .prologue
    .line 901
    monitor-enter p0

    :try_start_0
    const-string v1, ""

    const/4 v2, -0x1

    const/4 v3, 0x0

    const/4 v4, -0x1

    const/4 v6, -0x1

    move-object v0, p0

    move v5, p1

    invoke-direct/range {v0 .. v6}, Lsoftware/simplicial/a/t;->a(Ljava/lang/String;ILsoftware/simplicial/a/bg;III)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 902
    monitor-exit p0

    return-void

    .line 901
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized g()V
    .locals 4

    .prologue
    .line 565
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->K:Ljava/lang/Thread;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 572
    :goto_0
    monitor-exit p0

    return-void

    .line 568
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lsoftware/simplicial/a/t;->L:Z

    .line 569
    iget-object v0, p0, Lsoftware/simplicial/a/t;->J:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 570
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lsoftware/simplicial/a/t;->K:Ljava/lang/Thread;

    .line 571
    iget-object v0, p0, Lsoftware/simplicial/a/t;->K:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 565
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized g(I)V
    .locals 2

    .prologue
    .line 952
    monitor-enter p0

    :try_start_0
    const-string v0, ""

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lsoftware/simplicial/a/t;->a(Ljava/lang/String;ILsoftware/simplicial/a/bg;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 953
    monitor-exit p0

    return-void

    .line 952
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public h()V
    .locals 2

    .prologue
    .line 598
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->B()Lsoftware/simplicial/a/u;

    move-result-object v0

    iget-object v0, v0, Lsoftware/simplicial/a/u;->x:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 599
    return-void
.end method

.method public declared-synchronized h(I)V
    .locals 3

    .prologue
    .line 1131
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->f:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->g:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->h:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->e:Lsoftware/simplicial/a/t$a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, v1, :cond_1

    .line 1137
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1135
    :cond_1
    :try_start_1
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 1136
    iget-object v1, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v2

    invoke-static {v0, v2, p1}, Lsoftware/simplicial/a/f/ax;->a(Lsoftware/simplicial/a/f/bn;II)[B

    move-result-object v0

    invoke-interface {v1, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1131
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public i()V
    .locals 3

    .prologue
    .line 607
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->B()Lsoftware/simplicial/a/u;

    move-result-object v0

    .line 608
    sget-object v1, Lsoftware/simplicial/a/t$1;->b:[I

    invoke-virtual {v0}, Lsoftware/simplicial/a/u;->e()Lsoftware/simplicial/a/bf;

    move-result-object v2

    iget-object v2, v2, Lsoftware/simplicial/a/bf;->ao:Lsoftware/simplicial/a/s;

    invoke-virtual {v2}, Lsoftware/simplicial/a/s;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 620
    :goto_0
    return-void

    .line 611
    :pswitch_0
    iget-object v0, v0, Lsoftware/simplicial/a/u;->y:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    .line 614
    :pswitch_1
    sget-object v1, Lsoftware/simplicial/a/s;->b:Lsoftware/simplicial/a/s;

    iput-object v1, v0, Lsoftware/simplicial/a/u;->E:Lsoftware/simplicial/a/s;

    goto :goto_0

    .line 617
    :pswitch_2
    sget-object v1, Lsoftware/simplicial/a/s;->c:Lsoftware/simplicial/a/s;

    iput-object v1, v0, Lsoftware/simplicial/a/u;->E:Lsoftware/simplicial/a/s;

    goto :goto_0

    .line 608
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public declared-synchronized i(I)V
    .locals 3

    .prologue
    .line 1691
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v0

    .line 1693
    iget-boolean v1, v0, Lsoftware/simplicial/a/u;->e:Z

    if-nez v1, :cond_2

    .line 1695
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->u()V

    .line 1696
    iget-boolean v1, v0, Lsoftware/simplicial/a/u;->q:Z

    if-nez v1, :cond_0

    iget-boolean v1, v0, Lsoftware/simplicial/a/u;->t:Z

    if-eqz v1, :cond_1

    :cond_0
    iget-boolean v1, p0, Lsoftware/simplicial/a/t;->t:Z

    if-eqz v1, :cond_1

    const/4 v1, -0x1

    if-ne p1, v1, :cond_1

    .line 1697
    const/4 p1, -0x2

    .line 1698
    :cond_1
    iput p1, v0, Lsoftware/simplicial/a/u;->j:I

    .line 1699
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 1700
    iget-object v1, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v2

    invoke-static {v0, v2, p1}, Lsoftware/simplicial/a/f/bz;->a(Lsoftware/simplicial/a/f/bn;II)[B

    move-result-object v0

    invoke-interface {v1, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1702
    :cond_2
    monitor-exit p0

    return-void

    .line 1691
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public j()V
    .locals 2

    .prologue
    .line 628
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->B()Lsoftware/simplicial/a/u;

    move-result-object v0

    .line 629
    iget-object v0, v0, Lsoftware/simplicial/a/u;->y:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 630
    return-void
.end method

.method public j(I)V
    .locals 1

    .prologue
    .line 1824
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->B()Lsoftware/simplicial/a/u;

    move-result-object v0

    iget-object v0, v0, Lsoftware/simplicial/a/u;->F:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 1825
    return-void
.end method

.method public k()V
    .locals 2

    .prologue
    .line 638
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->B()Lsoftware/simplicial/a/u;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/a/s;->a:Lsoftware/simplicial/a/s;

    iput-object v1, v0, Lsoftware/simplicial/a/u;->E:Lsoftware/simplicial/a/s;

    .line 639
    return-void
.end method

.method public l()V
    .locals 4

    .prologue
    .line 702
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->u()V

    .line 704
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 705
    iget-object v1, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v2

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lsoftware/simplicial/a/f/l;->a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/am;)[B

    move-result-object v0

    invoke-interface {v1, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V

    .line 706
    return-void
.end method

.method public declared-synchronized m()V
    .locals 3

    .prologue
    .line 809
    monitor-enter p0

    :try_start_0
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 810
    iget-object v1, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v2

    invoke-static {v0, v2}, Lsoftware/simplicial/a/f/au;->a(Lsoftware/simplicial/a/f/bn;I)[B

    move-result-object v0

    invoke-interface {v1, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 811
    monitor-exit p0

    return-void

    .line 809
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized n()V
    .locals 3

    .prologue
    .line 815
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->i:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/t;->I:Lsoftware/simplicial/a/t$a;

    sget-object v1, Lsoftware/simplicial/a/t$a;->j:Lsoftware/simplicial/a/t$a;

    if-ne v0, v1, :cond_1

    .line 817
    :cond_0
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 818
    iget-object v1, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v2

    invoke-static {v0, v2}, Lsoftware/simplicial/a/f/ar;->a(Lsoftware/simplicial/a/f/bn;I)[B

    move-result-object v0

    invoke-interface {v1, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 820
    :cond_1
    monitor-exit p0

    return-void

    .line 815
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public o()Lsoftware/simplicial/a/bf;
    .locals 1

    .prologue
    .line 824
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v0

    invoke-virtual {v0}, Lsoftware/simplicial/a/u;->d()Lsoftware/simplicial/a/bf;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized p()V
    .locals 3

    .prologue
    .line 1185
    monitor-enter p0

    :try_start_0
    sget-object v0, Lsoftware/simplicial/a/bj;->b:Lsoftware/simplicial/a/bj;

    iput-object v0, p0, Lsoftware/simplicial/a/t;->N:Lsoftware/simplicial/a/bj;

    .line 1186
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/a/t;->t:Z

    .line 1187
    iget-object v0, p0, Lsoftware/simplicial/a/t;->T:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 1189
    sget-object v0, Lsoftware/simplicial/a/t$a;->a:Lsoftware/simplicial/a/t$a;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/t;->b(Lsoftware/simplicial/a/t$a;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1197
    :goto_0
    :try_start_1
    iget-object v0, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-interface {v0}, Lsoftware/simplicial/a/f/bg;->f()V

    .line 1199
    iget-object v0, p0, Lsoftware/simplicial/a/t;->G:Lsoftware/simplicial/a/f/cv;

    iput-object v0, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1200
    monitor-exit p0

    return-void

    .line 1191
    :catch_0
    move-exception v0

    .line 1193
    :try_start_2
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v2, "Exception in Client.Disconnect"

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1185
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public q()I
    .locals 1

    .prologue
    .line 1276
    iget-object v0, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-interface {v0}, Lsoftware/simplicial/a/f/bg;->d()I

    move-result v0

    return v0
.end method

.method public r()I
    .locals 1

    .prologue
    .line 1281
    iget-object v0, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-interface {v0}, Lsoftware/simplicial/a/f/bg;->c()I

    move-result v0

    return v0
.end method

.method public run()V
    .locals 4

    .prologue
    .line 249
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    const-string v1, "C"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 250
    iget-object v0, p0, Lsoftware/simplicial/a/t;->J:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 252
    :goto_0
    iget-boolean v0, p0, Lsoftware/simplicial/a/t;->L:Z

    if-nez v0, :cond_0

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 254
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 256
    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/a/t;->c(J)V

    .line 257
    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/a/t;->b(J)V

    .line 261
    const-wide/16 v0, 0xa

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 263
    :catch_0
    move-exception v0

    goto :goto_0

    .line 268
    :cond_0
    return-void
.end method

.method public s()I
    .locals 1

    .prologue
    .line 1286
    iget-object v0, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-interface {v0}, Lsoftware/simplicial/a/f/bg;->b()I

    move-result v0

    return v0
.end method

.method public t()Lsoftware/simplicial/a/bj;
    .locals 1

    .prologue
    .line 1678
    iget-object v0, p0, Lsoftware/simplicial/a/t;->N:Lsoftware/simplicial/a/bj;

    return-object v0
.end method

.method public declared-synchronized u()V
    .locals 4

    .prologue
    .line 1683
    monitor-enter p0

    const-wide/32 v0, 0x2bf20

    :try_start_0
    iget-wide v2, p0, Lsoftware/simplicial/a/t;->p:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x2710

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 1684
    iget-object v0, p0, Lsoftware/simplicial/a/t;->b:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/w;

    .line 1685
    const/4 v2, -0x1

    invoke-interface {v0, v2}, Lsoftware/simplicial/a/w;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1683
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1686
    :cond_0
    const-wide/16 v0, 0x0

    :try_start_1
    iput-wide v0, p0, Lsoftware/simplicial/a/t;->p:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1687
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized v()V
    .locals 3

    .prologue
    .line 1706
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->w()V

    .line 1708
    iget-object v0, p0, Lsoftware/simplicial/a/t;->b:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 1709
    iget-object v0, p0, Lsoftware/simplicial/a/t;->c:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 1710
    iget-object v0, p0, Lsoftware/simplicial/a/t;->d:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 1711
    iget-object v0, p0, Lsoftware/simplicial/a/t;->i:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 1712
    iget-object v0, p0, Lsoftware/simplicial/a/t;->m:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 1713
    iget-object v0, p0, Lsoftware/simplicial/a/t;->k:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 1714
    iget-object v0, p0, Lsoftware/simplicial/a/t;->j:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 1715
    iget-object v0, p0, Lsoftware/simplicial/a/t;->n:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 1717
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/a/t;->F:Lsoftware/simplicial/a/al;

    .line 1719
    iget-object v1, p0, Lsoftware/simplicial/a/t;->a:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1721
    :try_start_1
    iget-object v0, p0, Lsoftware/simplicial/a/t;->E:Lsoftware/simplicial/a/u;

    const/4 v2, 0x0

    iput-object v2, v0, Lsoftware/simplicial/a/u;->aJ:Lsoftware/simplicial/a/al;

    .line 1722
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/a/t;->E:Lsoftware/simplicial/a/u;

    .line 1723
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1725
    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lsoftware/simplicial/a/t;->P:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1726
    monitor-exit p0

    return-void

    .line 1723
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1706
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized w()V
    .locals 1

    .prologue
    .line 1730
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lsoftware/simplicial/a/t;->P:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1734
    :goto_0
    monitor-exit p0

    return-void

    .line 1732
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->p()V

    .line 1733
    invoke-direct {p0}, Lsoftware/simplicial/a/t;->D()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1730
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized x()V
    .locals 3

    .prologue
    .line 1757
    monitor-enter p0

    :try_start_0
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 1758
    iget-object v1, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v2

    invoke-static {v0, v2}, Lsoftware/simplicial/a/f/c;->a(Lsoftware/simplicial/a/f/bn;I)[B

    move-result-object v0

    invoke-interface {v1, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1759
    monitor-exit p0

    return-void

    .line 1757
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized y()V
    .locals 3

    .prologue
    .line 1763
    monitor-enter p0

    :try_start_0
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 1764
    iget-object v1, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    invoke-virtual {p0}, Lsoftware/simplicial/a/t;->r()I

    move-result v2

    invoke-static {v0, v2}, Lsoftware/simplicial/a/f/cg;->a(Lsoftware/simplicial/a/f/bn;I)[B

    move-result-object v0

    invoke-interface {v1, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1765
    monitor-exit p0

    return-void

    .line 1763
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized z()V
    .locals 2

    .prologue
    .line 1777
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/t;->S:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 1782
    :goto_0
    monitor-exit p0

    return-void

    .line 1780
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lsoftware/simplicial/a/t;->Q:Z

    .line 1781
    iget-object v0, p0, Lsoftware/simplicial/a/t;->H:Lsoftware/simplicial/a/f/bg;

    iget-object v1, p0, Lsoftware/simplicial/a/t;->S:[B

    invoke-interface {v0, v1}, Lsoftware/simplicial/a/f/bg;->a([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1777
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
