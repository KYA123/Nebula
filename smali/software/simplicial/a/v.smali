.class public final Lsoftware/simplicial/a/v;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lsoftware/simplicial/a/f/bl;

.field public final b:I

.field public final c:I

.field public final d:J

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:[B

.field public final h:Lsoftware/simplicial/a/q;

.field public final i:Lsoftware/simplicial/a/bc;

.field public final j:Ljava/lang/String;

.field public final k:[B

.field public final l:Lsoftware/simplicial/a/e;

.field public final m:Lsoftware/simplicial/a/af;

.field public final n:Lsoftware/simplicial/a/as;

.field public final o:I

.field public final p:B

.field public final q:I

.field public final r:Ljava/lang/String;

.field public s:Lsoftware/simplicial/a/a;


# direct methods
.method public constructor <init>(Lsoftware/simplicial/a/f/bl;IILjava/lang/String;Lsoftware/simplicial/a/a;Ljava/lang/String;[BLsoftware/simplicial/a/q;Lsoftware/simplicial/a/bc;Ljava/lang/String;[BLsoftware/simplicial/a/e;Lsoftware/simplicial/a/af;IJBILjava/lang/String;Lsoftware/simplicial/a/as;)V
    .locals 3

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v2, Lsoftware/simplicial/a/f/bl;

    invoke-direct {v2, p1}, Lsoftware/simplicial/a/f/bl;-><init>(Lsoftware/simplicial/a/f/bl;)V

    iput-object v2, p0, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    .line 43
    iput p2, p0, Lsoftware/simplicial/a/v;->b:I

    .line 44
    iput p3, p0, Lsoftware/simplicial/a/v;->c:I

    .line 45
    iput-object p5, p0, Lsoftware/simplicial/a/v;->s:Lsoftware/simplicial/a/a;

    .line 46
    move-wide/from16 v0, p15

    iput-wide v0, p0, Lsoftware/simplicial/a/v;->d:J

    .line 48
    iput-object p4, p0, Lsoftware/simplicial/a/v;->e:Ljava/lang/String;

    .line 49
    iput-object p6, p0, Lsoftware/simplicial/a/v;->f:Ljava/lang/String;

    .line 50
    iput-object p7, p0, Lsoftware/simplicial/a/v;->g:[B

    .line 51
    iput-object p8, p0, Lsoftware/simplicial/a/v;->h:Lsoftware/simplicial/a/q;

    .line 52
    new-instance v2, Lsoftware/simplicial/a/bc;

    invoke-direct {v2, p9}, Lsoftware/simplicial/a/bc;-><init>(Lsoftware/simplicial/a/bc;)V

    iput-object v2, p0, Lsoftware/simplicial/a/v;->i:Lsoftware/simplicial/a/bc;

    .line 54
    iput-object p10, p0, Lsoftware/simplicial/a/v;->j:Ljava/lang/String;

    .line 55
    array-length v2, p11

    invoke-static {p11, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v2

    iput-object v2, p0, Lsoftware/simplicial/a/v;->k:[B

    .line 56
    iput-object p12, p0, Lsoftware/simplicial/a/v;->l:Lsoftware/simplicial/a/e;

    .line 57
    move-object/from16 v0, p13

    iput-object v0, p0, Lsoftware/simplicial/a/v;->m:Lsoftware/simplicial/a/af;

    .line 58
    move-object/from16 v0, p20

    iput-object v0, p0, Lsoftware/simplicial/a/v;->n:Lsoftware/simplicial/a/as;

    .line 59
    move/from16 v0, p17

    iput-byte v0, p0, Lsoftware/simplicial/a/v;->p:B

    .line 60
    move/from16 v0, p14

    iput v0, p0, Lsoftware/simplicial/a/v;->o:I

    .line 61
    move/from16 v0, p18

    iput v0, p0, Lsoftware/simplicial/a/v;->q:I

    .line 62
    move-object/from16 v0, p19

    iput-object v0, p0, Lsoftware/simplicial/a/v;->r:Ljava/lang/String;

    .line 63
    return-void
.end method

.method public static a(Lsoftware/simplicial/a/f/bm;)Lsoftware/simplicial/a/v;
    .locals 24

    .prologue
    .line 96
    sget-object v2, Lsoftware/simplicial/a/e;->a:[Lsoftware/simplicial/a/e;

    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v3

    aget-object v15, v2, v3

    .line 97
    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v13

    .line 98
    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v2

    new-array v14, v2, [B

    .line 99
    const/4 v2, 0x0

    array-length v3, v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v2, v3}, Lsoftware/simplicial/a/f/bm;->readFully([BII)V

    .line 100
    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v2

    invoke-static {v2}, Lsoftware/simplicial/a/af;->a(I)Lsoftware/simplicial/a/af;

    move-result-object v16

    .line 101
    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v17

    .line 102
    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v20

    .line 103
    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v21

    .line 104
    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v22

    .line 105
    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v2

    invoke-static {v2}, Lsoftware/simplicial/a/as;->a(I)Lsoftware/simplicial/a/as;

    move-result-object v23

    .line 107
    new-instance v3, Lsoftware/simplicial/a/v;

    new-instance v4, Lsoftware/simplicial/a/f/bl;

    invoke-direct {v4}, Lsoftware/simplicial/a/f/bl;-><init>()V

    const/4 v5, -0x1

    const/4 v6, 0x0

    const-string v7, ""

    sget-object v8, Lsoftware/simplicial/a/a;->b:Lsoftware/simplicial/a/a;

    const-string v9, ""

    const/4 v2, 0x0

    new-array v10, v2, [B

    sget-object v11, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    new-instance v12, Lsoftware/simplicial/a/bc;

    invoke-direct {v12}, Lsoftware/simplicial/a/bc;-><init>()V

    const-wide/16 v18, 0x0

    invoke-direct/range {v3 .. v23}, Lsoftware/simplicial/a/v;-><init>(Lsoftware/simplicial/a/f/bl;IILjava/lang/String;Lsoftware/simplicial/a/a;Ljava/lang/String;[BLsoftware/simplicial/a/q;Lsoftware/simplicial/a/bc;Ljava/lang/String;[BLsoftware/simplicial/a/e;Lsoftware/simplicial/a/af;IJBILjava/lang/String;Lsoftware/simplicial/a/as;)V

    return-object v3
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bn;)V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lsoftware/simplicial/a/v;->l:Lsoftware/simplicial/a/e;

    invoke-virtual {v0}, Lsoftware/simplicial/a/e;->a()I

    move-result v0

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    .line 143
    iget-object v0, p0, Lsoftware/simplicial/a/v;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lsoftware/simplicial/a/v;->k:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 145
    iget-object v0, p0, Lsoftware/simplicial/a/v;->k:[B

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->write([B)V

    .line 146
    iget-object v0, p0, Lsoftware/simplicial/a/v;->m:Lsoftware/simplicial/a/af;

    iget-byte v0, v0, Lsoftware/simplicial/a/af;->c:B

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 147
    iget v0, p0, Lsoftware/simplicial/a/v;->o:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 148
    iget-byte v0, p0, Lsoftware/simplicial/a/v;->p:B

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 149
    iget v0, p0, Lsoftware/simplicial/a/v;->q:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 150
    iget-object v0, p0, Lsoftware/simplicial/a/v;->r:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Lsoftware/simplicial/a/v;->n:Lsoftware/simplicial/a/as;

    iget-byte v0, v0, Lsoftware/simplicial/a/as;->c:B

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 152
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 157
    const-string v0, ""

    .line 158
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Client: \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 159
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bl;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 160
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/a/v;->s:Lsoftware/simplicial/a/a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 161
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lsoftware/simplicial/a/v;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 162
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/a/v;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 163
    return-object v0
.end method
