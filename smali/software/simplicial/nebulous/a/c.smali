.class public Lsoftware/simplicial/nebulous/a/c;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lsoftware/simplicial/nebulous/f/g;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lsoftware/simplicial/nebulous/application/MainActivity;

.field private final b:Lsoftware/simplicial/nebulous/f/al$o;


# direct methods
.method public constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/nebulous/f/al$o;)V
    .locals 1

    .prologue
    .line 36
    const v0, 0x7f040066

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 38
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 39
    iput-object p2, p0, Lsoftware/simplicial/nebulous/a/c;->b:Lsoftware/simplicial/nebulous/f/al$o;

    .line 40
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/a/c;)Lsoftware/simplicial/nebulous/application/MainActivity;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    return-object v0
.end method

.method private a(I)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 325
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 327
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f080282

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 328
    new-instance v1, Landroid/widget/EditText;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 329
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 330
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 332
    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f0801cf

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lsoftware/simplicial/nebulous/a/c$4;

    invoke-direct {v3, p0, p1, v1}, Lsoftware/simplicial/nebulous/a/c$4;-><init>(Lsoftware/simplicial/nebulous/a/c;ILandroid/widget/EditText;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 351
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f08005d

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 353
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 355
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 356
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 357
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 358
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Window;->clearFlags(I)V

    .line 359
    return-void
.end method

.method private a(IILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 304
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    .line 305
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f08002f

    .line 306
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    .line 307
    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v0

    if-ne v0, p2, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f080178

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f080318

    .line 308
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/a/c$3;

    invoke-direct {v2, p0, p1, p2}, Lsoftware/simplicial/nebulous/a/c$3;-><init>(Lsoftware/simplicial/nebulous/a/c;II)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f0801bd

    .line 320
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 321
    return-void

    .line 307
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f080237

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(ILjava/lang/String;)V
    .locals 4

    .prologue
    .line 262
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    .line 263
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f08002f

    .line 264
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f080231

    .line 265
    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f080318

    .line 266
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/a/c$12;

    invoke-direct {v2, p0, p1}, Lsoftware/simplicial/nebulous/a/c$12;-><init>(Lsoftware/simplicial/nebulous/a/c;I)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f0801bd

    .line 278
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 279
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/a/c;I)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/a/c;->a(I)V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/a/c;IILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3, p4}, Lsoftware/simplicial/nebulous/a/c;->a(IILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/a/c;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lsoftware/simplicial/nebulous/a/c;->a(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/a/c;)Lsoftware/simplicial/nebulous/f/al$o;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/c;->b:Lsoftware/simplicial/nebulous/f/al$o;

    return-object v0
.end method

.method private b(ILjava/lang/String;)V
    .locals 4

    .prologue
    .line 283
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    .line 284
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f08002f

    .line 285
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f080178

    .line 286
    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f080318

    .line 287
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/a/c$2;

    invoke-direct {v2, p0, p1}, Lsoftware/simplicial/nebulous/a/c$2;-><init>(Lsoftware/simplicial/nebulous/a/c;I)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f0801bd

    .line 299
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 300
    return-void
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/a/c;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lsoftware/simplicial/nebulous/a/c;->b(ILjava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 16

    .prologue
    .line 45
    .line 46
    if-nez p2, :cond_0

    .line 47
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f040066

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 49
    :cond_0
    const v1, 0x7f0d009c

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 50
    const v2, 0x7f0d02c4

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 51
    const v3, 0x7f0d02c9

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    .line 52
    const v4, 0x7f0d02ca

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    .line 53
    const v5, 0x7f0d02cb

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    .line 54
    const v6, 0x7f0d02ce

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 55
    const v7, 0x7f0d02d2

    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 56
    const v8, 0x7f0d02d5

    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 57
    const v9, 0x7f0d02d0

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageButton;

    .line 58
    const v10, 0x7f0d02d3

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageButton;

    .line 59
    const v11, 0x7f0d02d7

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageButton;

    .line 60
    const v12, 0x7f0d02d4

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/RelativeLayout;

    .line 62
    invoke-virtual/range {p0 .. p1}, Lsoftware/simplicial/nebulous/a/c;->getItem(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lsoftware/simplicial/nebulous/f/g;

    .line 64
    iget-object v14, v13, Lsoftware/simplicial/nebulous/f/g;->b:Ljava/lang/String;

    invoke-virtual {v1, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    iget-object v1, v13, Lsoftware/simplicial/nebulous/f/g;->d:Lsoftware/simplicial/a/h/f;

    move-object/from16 v0, p0

    iget-object v14, v0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v14}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    invoke-static {v1, v14}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/h/f;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    .line 67
    iget-boolean v14, v13, Lsoftware/simplicial/nebulous/f/g;->c:Z

    if-eqz v14, :cond_1

    .line 68
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v14, " "

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v14, v0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v15, 0x7f080186

    invoke-virtual {v14, v15}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 69
    :cond_1
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    iget-object v1, v13, Lsoftware/simplicial/nebulous/f/g;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_d

    iget-object v1, v13, Lsoftware/simplicial/nebulous/f/g;->f:Lsoftware/simplicial/a/h/h;

    sget-object v2, Lsoftware/simplicial/a/h/h;->d:Lsoftware/simplicial/a/h/h;

    if-eq v1, v2, :cond_2

    iget-object v1, v13, Lsoftware/simplicial/nebulous/f/g;->f:Lsoftware/simplicial/a/h/h;

    sget-object v2, Lsoftware/simplicial/a/h/h;->c:Lsoftware/simplicial/a/h/h;

    if-ne v1, v2, :cond_d

    .line 73
    :cond_2
    iget-object v1, v13, Lsoftware/simplicial/nebulous/f/g;->e:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lsoftware/simplicial/nebulous/f/h;

    .line 74
    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 75
    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/nebulous/a/c;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v14, 0x7f0c00fb

    invoke-virtual {v2, v14}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 76
    iget v2, v1, Lsoftware/simplicial/nebulous/f/h;->a:I

    const/4 v14, -0x1

    if-eq v2, v14, :cond_c

    .line 78
    iget-object v2, v1, Lsoftware/simplicial/nebulous/f/h;->b:Ljava/lang/String;

    .line 79
    iget-object v14, v1, Lsoftware/simplicial/nebulous/f/h;->c:Lsoftware/simplicial/a/h/h;

    sget-object v15, Lsoftware/simplicial/a/h/h;->b:Lsoftware/simplicial/a/h/h;

    if-ne v14, v15, :cond_a

    .line 81
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v14, " ("

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v14, v0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v15, 0x7f080164

    invoke-virtual {v14, v15}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v14, ")"

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 82
    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/nebulous/a/c;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0c001d

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getColor(I)I

    move-result v14

    invoke-virtual {v6, v14}, Landroid/widget/TextView;->setTextColor(I)V

    .line 88
    :goto_0
    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    iget v2, v1, Lsoftware/simplicial/nebulous/f/h;->a:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v6}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v6

    if-eq v2, v6, :cond_3

    iget-object v2, v13, Lsoftware/simplicial/nebulous/f/g;->f:Lsoftware/simplicial/a/h/h;

    sget-object v6, Lsoftware/simplicial/a/h/h;->d:Lsoftware/simplicial/a/h/h;

    if-eq v2, v6, :cond_b

    :cond_3
    const/16 v2, 0x8

    :goto_1
    invoke-virtual {v9, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 90
    new-instance v2, Lsoftware/simplicial/nebulous/a/c$1;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v13, v1}, Lsoftware/simplicial/nebulous/a/c$1;-><init>(Lsoftware/simplicial/nebulous/a/c;Lsoftware/simplicial/nebulous/f/g;Lsoftware/simplicial/nebulous/f/h;)V

    invoke-virtual {v9, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    :goto_2
    iget-object v1, v13, Lsoftware/simplicial/nebulous/f/g;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_11

    iget-object v1, v13, Lsoftware/simplicial/nebulous/f/g;->f:Lsoftware/simplicial/a/h/h;

    sget-object v2, Lsoftware/simplicial/a/h/h;->d:Lsoftware/simplicial/a/h/h;

    if-eq v1, v2, :cond_4

    iget-object v1, v13, Lsoftware/simplicial/nebulous/f/g;->f:Lsoftware/simplicial/a/h/h;

    sget-object v2, Lsoftware/simplicial/a/h/h;->c:Lsoftware/simplicial/a/h/h;

    if-ne v1, v2, :cond_11

    .line 113
    :cond_4
    iget-object v1, v13, Lsoftware/simplicial/nebulous/f/g;->e:Ljava/util/List;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lsoftware/simplicial/nebulous/f/h;

    .line 114
    const/4 v2, 0x0

    invoke-virtual {v7, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 115
    iget-object v2, v1, Lsoftware/simplicial/nebulous/f/h;->c:Lsoftware/simplicial/a/h/h;

    sget-object v6, Lsoftware/simplicial/a/h/h;->b:Lsoftware/simplicial/a/h/h;

    if-ne v2, v6, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f0c001d

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    :goto_3
    invoke-virtual {v7, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 116
    iget v2, v1, Lsoftware/simplicial/nebulous/f/h;->a:I

    const/4 v6, -0x1

    if-eq v2, v6, :cond_10

    .line 118
    iget-object v2, v1, Lsoftware/simplicial/nebulous/f/h;->b:Ljava/lang/String;

    .line 119
    iget-object v6, v1, Lsoftware/simplicial/nebulous/f/h;->c:Lsoftware/simplicial/a/h/h;

    sget-object v9, Lsoftware/simplicial/a/h/h;->b:Lsoftware/simplicial/a/h/h;

    if-ne v6, v9, :cond_5

    .line 120
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " ("

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v9, 0x7f080164

    invoke-virtual {v6, v9}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ")"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 121
    :cond_5
    invoke-virtual {v7, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    iget v2, v1, Lsoftware/simplicial/nebulous/f/h;->a:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v6}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v6

    if-eq v2, v6, :cond_6

    iget-object v2, v13, Lsoftware/simplicial/nebulous/f/g;->f:Lsoftware/simplicial/a/h/h;

    sget-object v6, Lsoftware/simplicial/a/h/h;->d:Lsoftware/simplicial/a/h/h;

    if-eq v2, v6, :cond_f

    :cond_6
    const/16 v2, 0x8

    :goto_4
    invoke-virtual {v10, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 123
    new-instance v2, Lsoftware/simplicial/nebulous/a/c$5;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v13, v1}, Lsoftware/simplicial/nebulous/a/c$5;-><init>(Lsoftware/simplicial/nebulous/a/c;Lsoftware/simplicial/nebulous/f/g;Lsoftware/simplicial/nebulous/f/h;)V

    invoke-virtual {v10, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    :goto_5
    iget-object v1, v13, Lsoftware/simplicial/nebulous/f/g;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_15

    iget-object v1, v13, Lsoftware/simplicial/nebulous/f/g;->f:Lsoftware/simplicial/a/h/h;

    sget-object v2, Lsoftware/simplicial/a/h/h;->d:Lsoftware/simplicial/a/h/h;

    if-eq v1, v2, :cond_7

    iget-object v1, v13, Lsoftware/simplicial/nebulous/f/g;->f:Lsoftware/simplicial/a/h/h;

    sget-object v2, Lsoftware/simplicial/a/h/h;->c:Lsoftware/simplicial/a/h/h;

    if-ne v1, v2, :cond_15

    .line 146
    :cond_7
    iget-object v1, v13, Lsoftware/simplicial/nebulous/f/g;->e:Ljava/util/List;

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lsoftware/simplicial/nebulous/f/h;

    .line 147
    const/4 v2, 0x0

    invoke-virtual {v8, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 148
    iget-object v2, v1, Lsoftware/simplicial/nebulous/f/h;->c:Lsoftware/simplicial/a/h/h;

    sget-object v6, Lsoftware/simplicial/a/h/h;->b:Lsoftware/simplicial/a/h/h;

    if-ne v2, v6, :cond_12

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f0c001d

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    :goto_6
    invoke-virtual {v8, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 149
    iget v2, v1, Lsoftware/simplicial/nebulous/f/h;->a:I

    const/4 v6, -0x1

    if-eq v2, v6, :cond_14

    .line 151
    iget-object v2, v1, Lsoftware/simplicial/nebulous/f/h;->b:Ljava/lang/String;

    .line 152
    iget-object v6, v1, Lsoftware/simplicial/nebulous/f/h;->c:Lsoftware/simplicial/a/h/h;

    sget-object v7, Lsoftware/simplicial/a/h/h;->b:Lsoftware/simplicial/a/h/h;

    if-ne v6, v7, :cond_8

    .line 153
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " ("

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v7, 0x7f080164

    invoke-virtual {v6, v7}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ")"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 154
    :cond_8
    invoke-virtual {v8, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    iget v2, v1, Lsoftware/simplicial/nebulous/f/h;->a:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v6}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v6

    if-eq v2, v6, :cond_9

    iget-object v2, v13, Lsoftware/simplicial/nebulous/f/g;->f:Lsoftware/simplicial/a/h/h;

    sget-object v6, Lsoftware/simplicial/a/h/h;->d:Lsoftware/simplicial/a/h/h;

    if-eq v2, v6, :cond_13

    :cond_9
    const/16 v2, 0x8

    :goto_7
    invoke-virtual {v11, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 156
    new-instance v2, Lsoftware/simplicial/nebulous/a/c$6;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v13, v1}, Lsoftware/simplicial/nebulous/a/c$6;-><init>(Lsoftware/simplicial/nebulous/a/c;Lsoftware/simplicial/nebulous/f/g;Lsoftware/simplicial/nebulous/f/h;)V

    invoke-virtual {v11, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 177
    :goto_8
    const/4 v1, 0x0

    .line 178
    iget-object v2, v13, Lsoftware/simplicial/nebulous/f/g;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v1

    :goto_9
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_16

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lsoftware/simplicial/nebulous/f/h;

    .line 179
    iget v1, v1, Lsoftware/simplicial/nebulous/f/h;->a:I

    const/4 v7, -0x1

    if-eq v1, v7, :cond_1b

    .line 180
    add-int/lit8 v1, v2, 0x1

    :goto_a
    move v2, v1

    .line 179
    goto :goto_9

    .line 86
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/nebulous/a/c;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0c0093

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getColor(I)I

    move-result v14

    invoke-virtual {v6, v14}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    .line 89
    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 101
    :cond_c
    const-string v1, "---"

    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    const/16 v1, 0x8

    invoke-virtual {v9, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_2

    .line 107
    :cond_d
    const/16 v1, 0x8

    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 108
    const/16 v1, 0x8

    invoke-virtual {v9, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_2

    .line 115
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f0c00fb

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    goto/16 :goto_3

    .line 122
    :cond_f
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 134
    :cond_10
    const-string v1, "---"

    invoke-virtual {v7, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    const/16 v1, 0x8

    invoke-virtual {v10, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_5

    .line 140
    :cond_11
    const/16 v1, 0x8

    invoke-virtual {v7, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 141
    const/16 v1, 0x8

    invoke-virtual {v10, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_5

    .line 148
    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/a/c;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f0c00fb

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    goto/16 :goto_6

    .line 155
    :cond_13
    const/4 v2, 0x0

    goto/16 :goto_7

    .line 167
    :cond_14
    const-string v1, "---"

    invoke-virtual {v8, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 168
    const/16 v1, 0x8

    invoke-virtual {v11, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_8

    .line 173
    :cond_15
    const/16 v1, 0x8

    invoke-virtual {v12, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 174
    const/16 v1, 0x8

    invoke-virtual {v11, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_8

    .line 182
    :cond_16
    iget-object v1, v13, Lsoftware/simplicial/nebulous/f/g;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v2, v1, :cond_17

    iget-object v1, v13, Lsoftware/simplicial/nebulous/f/g;->f:Lsoftware/simplicial/a/h/h;

    sget-object v2, Lsoftware/simplicial/a/h/h;->d:Lsoftware/simplicial/a/h/h;

    if-ne v1, v2, :cond_17

    .line 184
    const/4 v1, 0x0

    invoke-virtual {v3, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 185
    new-instance v1, Lsoftware/simplicial/nebulous/a/c$7;

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v13}, Lsoftware/simplicial/nebulous/a/c$7;-><init>(Lsoftware/simplicial/nebulous/a/c;Lsoftware/simplicial/nebulous/f/g;)V

    invoke-virtual {v3, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 199
    :goto_b
    iget-object v1, v13, Lsoftware/simplicial/nebulous/f/g;->f:Lsoftware/simplicial/a/h/h;

    sget-object v2, Lsoftware/simplicial/a/h/h;->b:Lsoftware/simplicial/a/h/h;

    if-ne v1, v2, :cond_18

    .line 201
    const/4 v1, 0x0

    invoke-virtual {v4, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 202
    new-instance v1, Lsoftware/simplicial/nebulous/a/c$8;

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v13}, Lsoftware/simplicial/nebulous/a/c$8;-><init>(Lsoftware/simplicial/nebulous/a/c;Lsoftware/simplicial/nebulous/f/g;)V

    invoke-virtual {v4, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 212
    const/4 v1, 0x0

    invoke-virtual {v5, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 213
    new-instance v1, Lsoftware/simplicial/nebulous/a/c$9;

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v13}, Lsoftware/simplicial/nebulous/a/c$9;-><init>(Lsoftware/simplicial/nebulous/a/c;Lsoftware/simplicial/nebulous/f/g;)V

    invoke-virtual {v5, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 256
    :goto_c
    return-object p2

    .line 196
    :cond_17
    const/16 v1, 0x8

    invoke-virtual {v3, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_b

    .line 222
    :cond_18
    iget-object v1, v13, Lsoftware/simplicial/nebulous/f/g;->f:Lsoftware/simplicial/a/h/h;

    sget-object v2, Lsoftware/simplicial/a/h/h;->c:Lsoftware/simplicial/a/h/h;

    if-ne v1, v2, :cond_19

    .line 224
    const/16 v1, 0x8

    invoke-virtual {v4, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 226
    const/4 v1, 0x0

    invoke-virtual {v5, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 227
    new-instance v1, Lsoftware/simplicial/nebulous/a/c$10;

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v13}, Lsoftware/simplicial/nebulous/a/c$10;-><init>(Lsoftware/simplicial/nebulous/a/c;Lsoftware/simplicial/nebulous/f/g;)V

    invoke-virtual {v5, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_c

    .line 236
    :cond_19
    iget-object v1, v13, Lsoftware/simplicial/nebulous/f/g;->f:Lsoftware/simplicial/a/h/h;

    sget-object v2, Lsoftware/simplicial/a/h/h;->d:Lsoftware/simplicial/a/h/h;

    if-ne v1, v2, :cond_1a

    .line 238
    const/16 v1, 0x8

    invoke-virtual {v4, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 240
    const/4 v1, 0x0

    invoke-virtual {v5, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 241
    new-instance v1, Lsoftware/simplicial/nebulous/a/c$11;

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v13}, Lsoftware/simplicial/nebulous/a/c$11;-><init>(Lsoftware/simplicial/nebulous/a/c;Lsoftware/simplicial/nebulous/f/g;)V

    invoke-virtual {v5, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_c

    .line 252
    :cond_1a
    const/16 v1, 0x8

    invoke-virtual {v4, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 253
    const/16 v1, 0x8

    invoke-virtual {v5, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_c

    :cond_1b
    move v1, v2

    goto/16 :goto_a
.end method
