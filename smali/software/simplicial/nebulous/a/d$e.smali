.class Lsoftware/simplicial/nebulous/a/d$e;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsoftware/simplicial/nebulous/a/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lsoftware/simplicial/nebulous/a/d$f;",
        "Ljava/lang/Void;",
        "Lsoftware/simplicial/nebulous/a/d$f;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/a/d;


# direct methods
.method private constructor <init>(Lsoftware/simplicial/nebulous/a/d;)V
    .locals 0

    .prologue
    .line 577
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/d$e;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lsoftware/simplicial/nebulous/a/d;Lsoftware/simplicial/nebulous/a/d$1;)V
    .locals 0

    .prologue
    .line 577
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/a/d$e;-><init>(Lsoftware/simplicial/nebulous/a/d;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Lsoftware/simplicial/nebulous/a/d$f;)Lsoftware/simplicial/nebulous/a/d$f;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 583
    aget-object v0, p1, v3

    .line 585
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 586
    const/4 v2, 0x1

    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 587
    iput-boolean v3, v1, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 588
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v2, v1, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 589
    iput-boolean v3, v1, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 591
    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/d$e;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/a/d;->t(Lsoftware/simplicial/nebulous/a/d;)Ljava/util/Map;

    move-result-object v2

    iget-object v3, v0, Lsoftware/simplicial/nebulous/a/d$f;->b:Landroid/view/View;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    iget-object v3, v0, Lsoftware/simplicial/nebulous/a/d$f;->a:Lsoftware/simplicial/a/as;

    if-eq v2, v3, :cond_0

    .line 592
    const/4 v0, 0x0

    .line 597
    :goto_0
    return-object v0

    .line 594
    :cond_0
    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/d$e;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/a/d;->b(Lsoftware/simplicial/nebulous/a/d;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/a/d$e;->a:Lsoftware/simplicial/nebulous/a/d;

    .line 595
    invoke-static {v3}, Lsoftware/simplicial/nebulous/a/d;->b(Lsoftware/simplicial/nebulous/a/d;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v3

    invoke-virtual {v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v4, v0, Lsoftware/simplicial/nebulous/a/d$f;->a:Lsoftware/simplicial/a/as;

    invoke-virtual {v4}, Lsoftware/simplicial/a/as;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "drawable"

    iget-object v6, p0, Lsoftware/simplicial/nebulous/a/d$e;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v6}, Lsoftware/simplicial/nebulous/a/d;->b(Lsoftware/simplicial/nebulous/a/d;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v6

    invoke-virtual {v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 594
    invoke-static {v2, v3, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, v0, Lsoftware/simplicial/nebulous/a/d$f;->c:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method protected a(Lsoftware/simplicial/nebulous/a/d$f;)V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x0

    .line 603
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 605
    if-eqz p1, :cond_0

    iget-object v0, p1, Lsoftware/simplicial/nebulous/a/d$f;->c:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 646
    :cond_0
    :goto_0
    return-void

    .line 608
    :cond_1
    iget-object v5, p1, Lsoftware/simplicial/nebulous/a/d$f;->b:Landroid/view/View;

    .line 609
    iget-object v6, p1, Lsoftware/simplicial/nebulous/a/d$f;->a:Lsoftware/simplicial/a/as;

    .line 611
    const v0, 0x7f0d02f8

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 612
    const v1, 0x7f0d02fa

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 613
    const v2, 0x7f0d02fc

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 614
    const v3, 0x7f0d02fb

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 615
    const v4, 0x7f0d02f9

    invoke-virtual {v5, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 617
    iget-object v7, p0, Lsoftware/simplicial/nebulous/a/d$e;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v7}, Lsoftware/simplicial/nebulous/a/d;->t(Lsoftware/simplicial/nebulous/a/d;)Ljava/util/Map;

    move-result-object v7

    invoke-interface {v7, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-ne v5, v6, :cond_0

    .line 620
    iget-object v5, p1, Lsoftware/simplicial/nebulous/a/d$f;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 622
    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/d$e;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v5}, Lsoftware/simplicial/nebulous/a/d;->u(Lsoftware/simplicial/nebulous/a/d;)Ljava/util/Set;

    move-result-object v5

    invoke-static {v6, v5}, Lsoftware/simplicial/a/as;->a(Lsoftware/simplicial/a/as;Ljava/util/Set;)Z

    move-result v5

    .line 623
    if-eqz v5, :cond_2

    .line 624
    invoke-virtual {v0}, Landroid/widget/ImageView;->clearColorFilter()V

    .line 628
    :goto_1
    invoke-virtual {v4, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 629
    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 631
    iget v0, v6, Lsoftware/simplicial/a/as;->d:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_5

    .line 633
    invoke-virtual {v3, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 634
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d$e;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/a/d;->b(Lsoftware/simplicial/nebulous/a/d;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c002f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 635
    iget v0, v6, Lsoftware/simplicial/a/as;->d:I

    if-ltz v0, :cond_3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    iget v1, v6, Lsoftware/simplicial/a/as;->d:I

    int-to-long v6, v1

    invoke-virtual {v0, v6, v7}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 637
    if-eqz v5, :cond_4

    .line 638
    const v0, 0x3f2aaaab

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setAlpha(F)V

    goto/16 :goto_0

    .line 626
    :cond_2
    const/16 v7, 0xdc

    invoke-static {v7, v8, v8, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v7

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setColorFilter(I)V

    goto :goto_1

    .line 635
    :cond_3
    const-string v0, "---"

    goto :goto_2

    .line 640
    :cond_4
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setAlpha(F)V

    goto/16 :goto_0

    .line 644
    :cond_5
    invoke-virtual {v3, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 577
    check-cast p1, [Lsoftware/simplicial/nebulous/a/d$f;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/a/d$e;->a([Lsoftware/simplicial/nebulous/a/d$f;)Lsoftware/simplicial/nebulous/a/d$f;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 577
    check-cast p1, Lsoftware/simplicial/nebulous/a/d$f;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/a/d$e;->a(Lsoftware/simplicial/nebulous/a/d$f;)V

    return-void
.end method
