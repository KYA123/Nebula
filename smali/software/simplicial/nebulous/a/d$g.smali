.class Lsoftware/simplicial/nebulous/a/d$g;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsoftware/simplicial/nebulous/a/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "g"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lsoftware/simplicial/nebulous/a/d$h;",
        "Ljava/lang/Void;",
        "Lsoftware/simplicial/nebulous/a/d$h;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/a/d;


# direct methods
.method private constructor <init>(Lsoftware/simplicial/nebulous/a/d;)V
    .locals 0

    .prologue
    .line 663
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/d$g;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lsoftware/simplicial/nebulous/a/d;Lsoftware/simplicial/nebulous/a/d$1;)V
    .locals 0

    .prologue
    .line 663
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/a/d$g;-><init>(Lsoftware/simplicial/nebulous/a/d;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Lsoftware/simplicial/nebulous/a/d$h;)Lsoftware/simplicial/nebulous/a/d$h;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 669
    aget-object v1, p1, v3

    .line 671
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 672
    const/4 v0, 0x1

    iput v0, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 673
    iput-boolean v3, v2, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 674
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v0, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 675
    iput-boolean v3, v2, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 677
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d$g;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/a/d;->v(Lsoftware/simplicial/nebulous/a/d;)Ljava/util/Map;

    move-result-object v0

    iget-object v3, v1, Lsoftware/simplicial/nebulous/a/d$h;->b:Landroid/view/View;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bd;

    iget-object v3, v1, Lsoftware/simplicial/nebulous/a/d$h;->a:Lsoftware/simplicial/a/bd;

    invoke-virtual {v0, v3}, Lsoftware/simplicial/a/bd;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 678
    const/4 v0, 0x0

    .line 683
    :goto_0
    return-object v0

    .line 680
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d$g;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/a/d;->b(Lsoftware/simplicial/nebulous/a/d;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v3, p0, Lsoftware/simplicial/nebulous/a/d$g;->a:Lsoftware/simplicial/nebulous/a/d;

    .line 681
    invoke-static {v3}, Lsoftware/simplicial/nebulous/a/d;->b(Lsoftware/simplicial/nebulous/a/d;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v3

    invoke-virtual {v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v4, v1, Lsoftware/simplicial/nebulous/a/d$h;->a:Lsoftware/simplicial/a/bd;

    invoke-virtual {v4}, Lsoftware/simplicial/a/bd;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "drawable"

    iget-object v6, p0, Lsoftware/simplicial/nebulous/a/d$g;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v6}, Lsoftware/simplicial/nebulous/a/d;->b(Lsoftware/simplicial/nebulous/a/d;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v6

    invoke-virtual {v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 680
    invoke-static {v0, v3, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, v1, Lsoftware/simplicial/nebulous/a/d$h;->c:Landroid/graphics/Bitmap;

    move-object v0, v1

    .line 683
    goto :goto_0
.end method

.method protected a(Lsoftware/simplicial/nebulous/a/d$h;)V
    .locals 12

    .prologue
    const/16 v11, 0x8

    const/high16 v10, 0x3f800000    # 1.0f

    const v9, 0x3f2aaaab

    const/4 v8, 0x0

    .line 689
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 691
    if-eqz p1, :cond_0

    iget-object v0, p1, Lsoftware/simplicial/nebulous/a/d$h;->c:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 739
    :cond_0
    :goto_0
    return-void

    .line 694
    :cond_1
    iget-object v5, p1, Lsoftware/simplicial/nebulous/a/d$h;->b:Landroid/view/View;

    .line 695
    iget-object v6, p1, Lsoftware/simplicial/nebulous/a/d$h;->a:Lsoftware/simplicial/a/bd;

    .line 697
    const v0, 0x7f0d02f8

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 698
    const v1, 0x7f0d02fa

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 699
    const v2, 0x7f0d02fc

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 700
    const v3, 0x7f0d02fb

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 701
    const v4, 0x7f0d02f9

    invoke-virtual {v5, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 703
    iget-object v7, p0, Lsoftware/simplicial/nebulous/a/d$g;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v7}, Lsoftware/simplicial/nebulous/a/d;->v(Lsoftware/simplicial/nebulous/a/d;)Ljava/util/Map;

    move-result-object v7

    invoke-interface {v7, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lsoftware/simplicial/a/bd;

    invoke-virtual {v5, v6}, Lsoftware/simplicial/a/bd;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 706
    iget-object v5, p1, Lsoftware/simplicial/nebulous/a/d$h;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 708
    iget-byte v5, v6, Lsoftware/simplicial/a/bd;->c:B

    iget-object v7, p0, Lsoftware/simplicial/nebulous/a/d$g;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v7}, Lsoftware/simplicial/nebulous/a/d;->w(Lsoftware/simplicial/nebulous/a/d;)Ljava/util/Map;

    move-result-object v7

    invoke-static {v5, v7}, Lsoftware/simplicial/a/bd;->a(BLjava/util/Map;)Z

    move-result v5

    .line 709
    if-eqz v5, :cond_2

    .line 710
    invoke-virtual {v0}, Landroid/widget/ImageView;->clearColorFilter()V

    .line 714
    :goto_1
    invoke-virtual {v4, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 716
    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 717
    const/16 v0, 0x55

    const/16 v4, 0xaa

    const/16 v7, 0xff

    invoke-static {v0, v4, v7}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 718
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-short v4, v6, Lsoftware/simplicial/a/bd;->e:S

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 719
    if-eqz v5, :cond_3

    .line 720
    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setAlpha(F)V

    .line 724
    :goto_2
    iget v0, v6, Lsoftware/simplicial/a/bd;->d:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_6

    .line 726
    invoke-virtual {v3, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 727
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d$g;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/a/d;->b(Lsoftware/simplicial/nebulous/a/d;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c002f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 728
    iget v0, v6, Lsoftware/simplicial/a/bd;->d:I

    if-ltz v0, :cond_4

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    iget v1, v6, Lsoftware/simplicial/a/bd;->d:I

    int-to-long v6, v1

    invoke-virtual {v0, v6, v7}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 730
    if-eqz v5, :cond_5

    .line 731
    invoke-virtual {v3, v9}, Landroid/widget/LinearLayout;->setAlpha(F)V

    goto/16 :goto_0

    .line 712
    :cond_2
    const/16 v7, 0xdc

    invoke-static {v7, v8, v8, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v7

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setColorFilter(I)V

    goto :goto_1

    .line 722
    :cond_3
    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setAlpha(F)V

    goto :goto_2

    .line 728
    :cond_4
    const-string v0, "---"

    goto :goto_3

    .line 733
    :cond_5
    invoke-virtual {v3, v10}, Landroid/widget/LinearLayout;->setAlpha(F)V

    goto/16 :goto_0

    .line 737
    :cond_6
    invoke-virtual {v3, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 663
    check-cast p1, [Lsoftware/simplicial/nebulous/a/d$h;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/a/d$g;->a([Lsoftware/simplicial/nebulous/a/d$h;)Lsoftware/simplicial/nebulous/a/d$h;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 663
    check-cast p1, Lsoftware/simplicial/nebulous/a/d$h;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/a/d$g;->a(Lsoftware/simplicial/nebulous/a/d$h;)V

    return-void
.end method
