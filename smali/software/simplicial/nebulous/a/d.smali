.class public Lsoftware/simplicial/nebulous/a/d;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/nebulous/a/d$g;,
        Lsoftware/simplicial/nebulous/a/d$h;,
        Lsoftware/simplicial/nebulous/a/d$e;,
        Lsoftware/simplicial/nebulous/a/d$c;,
        Lsoftware/simplicial/nebulous/a/d$f;,
        Lsoftware/simplicial/nebulous/a/d$d;,
        Lsoftware/simplicial/nebulous/a/d$a;,
        Lsoftware/simplicial/nebulous/a/d$b;
    }
.end annotation


# instance fields
.field public a:Lsoftware/simplicial/nebulous/f/i;

.field b:Ljava/util/concurrent/ExecutorService;

.field private final c:Lsoftware/simplicial/nebulous/application/MainActivity;

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private s:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/af;",
            ">;"
        }
    .end annotation
.end field

.field private t:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/as;",
            ">;"
        }
    .end annotation
.end field

.field private u:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Lsoftware/simplicial/a/bd;",
            ">;"
        }
    .end annotation
.end field

.field private v:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "Lsoftware/simplicial/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private w:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "Lsoftware/simplicial/a/af;",
            ">;"
        }
    .end annotation
.end field

.field private x:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "Lsoftware/simplicial/a/as;",
            ">;"
        }
    .end annotation
.end field

.field private y:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "Lsoftware/simplicial/a/bd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 38
    sget-object v0, Lsoftware/simplicial/nebulous/f/i;->a:Lsoftware/simplicial/nebulous/f/i;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->a:Lsoftware/simplicial/nebulous/f/i;

    .line 39
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->b:Ljava/util/concurrent/ExecutorService;

    .line 40
    const/4 v0, 0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/a/d;->d:I

    .line 41
    iput v1, p0, Lsoftware/simplicial/nebulous/a/d;->e:I

    .line 42
    iput v1, p0, Lsoftware/simplicial/nebulous/a/d;->f:I

    .line 43
    iput v1, p0, Lsoftware/simplicial/nebulous/a/d;->g:I

    .line 44
    iput v1, p0, Lsoftware/simplicial/nebulous/a/d;->h:I

    .line 45
    iput v1, p0, Lsoftware/simplicial/nebulous/a/d;->i:I

    .line 46
    iput v1, p0, Lsoftware/simplicial/nebulous/a/d;->j:I

    .line 47
    iput v1, p0, Lsoftware/simplicial/nebulous/a/d;->k:I

    .line 48
    iput v1, p0, Lsoftware/simplicial/nebulous/a/d;->l:I

    .line 49
    iput v1, p0, Lsoftware/simplicial/nebulous/a/d;->m:I

    .line 50
    iput v1, p0, Lsoftware/simplicial/nebulous/a/d;->n:I

    .line 51
    iput v1, p0, Lsoftware/simplicial/nebulous/a/d;->o:I

    .line 52
    iput v1, p0, Lsoftware/simplicial/nebulous/a/d;->p:I

    .line 53
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->q:Ljava/util/Set;

    .line 54
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->r:Ljava/util/Set;

    .line 55
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->s:Ljava/util/Set;

    .line 56
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->t:Ljava/util/Set;

    .line 57
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->u:Ljava/util/Map;

    .line 58
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->v:Ljava/util/Map;

    .line 59
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->w:Ljava/util/Map;

    .line 60
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->x:Ljava/util/Map;

    .line 61
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->y:Ljava/util/Map;

    .line 66
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/d;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 67
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/a/d;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->v:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/a/d;)Lsoftware/simplicial/nebulous/application/MainActivity;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    return-object v0
.end method

.method static synthetic c(Lsoftware/simplicial/nebulous/a/d;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->r:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic d(Lsoftware/simplicial/nebulous/a/d;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->q:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic e(Lsoftware/simplicial/nebulous/a/d;)I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lsoftware/simplicial/nebulous/a/d;->d:I

    return v0
.end method

.method static synthetic f(Lsoftware/simplicial/nebulous/a/d;)I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lsoftware/simplicial/nebulous/a/d;->e:I

    return v0
.end method

.method static synthetic g(Lsoftware/simplicial/nebulous/a/d;)I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lsoftware/simplicial/nebulous/a/d;->f:I

    return v0
.end method

.method static synthetic h(Lsoftware/simplicial/nebulous/a/d;)I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lsoftware/simplicial/nebulous/a/d;->g:I

    return v0
.end method

.method static synthetic i(Lsoftware/simplicial/nebulous/a/d;)I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lsoftware/simplicial/nebulous/a/d;->h:I

    return v0
.end method

.method static synthetic j(Lsoftware/simplicial/nebulous/a/d;)I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lsoftware/simplicial/nebulous/a/d;->i:I

    return v0
.end method

.method static synthetic k(Lsoftware/simplicial/nebulous/a/d;)I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lsoftware/simplicial/nebulous/a/d;->j:I

    return v0
.end method

.method static synthetic l(Lsoftware/simplicial/nebulous/a/d;)I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lsoftware/simplicial/nebulous/a/d;->k:I

    return v0
.end method

.method static synthetic m(Lsoftware/simplicial/nebulous/a/d;)I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lsoftware/simplicial/nebulous/a/d;->l:I

    return v0
.end method

.method static synthetic n(Lsoftware/simplicial/nebulous/a/d;)I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lsoftware/simplicial/nebulous/a/d;->m:I

    return v0
.end method

.method static synthetic o(Lsoftware/simplicial/nebulous/a/d;)I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lsoftware/simplicial/nebulous/a/d;->n:I

    return v0
.end method

.method static synthetic p(Lsoftware/simplicial/nebulous/a/d;)I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lsoftware/simplicial/nebulous/a/d;->o:I

    return v0
.end method

.method static synthetic q(Lsoftware/simplicial/nebulous/a/d;)I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lsoftware/simplicial/nebulous/a/d;->p:I

    return v0
.end method

.method static synthetic r(Lsoftware/simplicial/nebulous/a/d;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->w:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic s(Lsoftware/simplicial/nebulous/a/d;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->s:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic t(Lsoftware/simplicial/nebulous/a/d;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->x:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic u(Lsoftware/simplicial/nebulous/a/d;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->t:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic v(Lsoftware/simplicial/nebulous/a/d;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->y:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic w(Lsoftware/simplicial/nebulous/a/d;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->u:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public a(I)Lsoftware/simplicial/a/e;
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->a:Lsoftware/simplicial/nebulous/f/i;

    sget-object v1, Lsoftware/simplicial/nebulous/f/i;->h:Lsoftware/simplicial/nebulous/f/i;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->a:Lsoftware/simplicial/nebulous/f/i;

    sget-object v1, Lsoftware/simplicial/nebulous/f/i;->j:Lsoftware/simplicial/nebulous/f/i;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->a:Lsoftware/simplicial/nebulous/f/i;

    sget-object v1, Lsoftware/simplicial/nebulous/f/i;->i:Lsoftware/simplicial/nebulous/f/i;

    if-ne v0, v1, :cond_1

    .line 209
    :cond_0
    const/4 v0, 0x0

    .line 210
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->a:Lsoftware/simplicial/nebulous/f/i;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/i;->l:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/e;

    goto :goto_0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 244
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->a:Lsoftware/simplicial/nebulous/f/i;

    sget-object v1, Lsoftware/simplicial/nebulous/f/i;->a:Lsoftware/simplicial/nebulous/f/i;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->a:Lsoftware/simplicial/nebulous/f/i;

    sget-object v1, Lsoftware/simplicial/nebulous/f/i;->g:Lsoftware/simplicial/nebulous/f/i;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->a:Lsoftware/simplicial/nebulous/f/i;

    sget-object v1, Lsoftware/simplicial/nebulous/f/i;->k:Lsoftware/simplicial/nebulous/f/i;

    if-eq v0, v1, :cond_0

    .line 245
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/a/d;->notifyDataSetChanged()V

    .line 246
    :cond_0
    return-void
.end method

.method public a(ILjava/util/Set;IILjava/util/Set;Ljava/util/Set;IIIIIIIIIILjava/util/Map;Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/d;",
            ">;II",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/e;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/af;",
            ">;IIIIIIIIII",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Lsoftware/simplicial/a/bd;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/as;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 184
    iput p1, p0, Lsoftware/simplicial/nebulous/a/d;->d:I

    .line 185
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, p2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lsoftware/simplicial/nebulous/a/d;->q:Ljava/util/Set;

    .line 186
    iput p3, p0, Lsoftware/simplicial/nebulous/a/d;->e:I

    .line 187
    iput p4, p0, Lsoftware/simplicial/nebulous/a/d;->f:I

    .line 188
    iput-object p5, p0, Lsoftware/simplicial/nebulous/a/d;->r:Ljava/util/Set;

    .line 189
    iput-object p6, p0, Lsoftware/simplicial/nebulous/a/d;->s:Ljava/util/Set;

    .line 190
    move-object/from16 v0, p18

    iput-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->t:Ljava/util/Set;

    .line 191
    move-object/from16 v0, p17

    iput-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->u:Ljava/util/Map;

    .line 192
    iput p7, p0, Lsoftware/simplicial/nebulous/a/d;->g:I

    .line 193
    iput p8, p0, Lsoftware/simplicial/nebulous/a/d;->h:I

    .line 194
    iput p9, p0, Lsoftware/simplicial/nebulous/a/d;->i:I

    .line 195
    iput p10, p0, Lsoftware/simplicial/nebulous/a/d;->j:I

    .line 196
    iput p11, p0, Lsoftware/simplicial/nebulous/a/d;->k:I

    .line 197
    iput p12, p0, Lsoftware/simplicial/nebulous/a/d;->l:I

    .line 198
    move/from16 v0, p13

    iput v0, p0, Lsoftware/simplicial/nebulous/a/d;->m:I

    .line 199
    move/from16 v0, p14

    iput v0, p0, Lsoftware/simplicial/nebulous/a/d;->n:I

    .line 200
    move/from16 v0, p15

    iput v0, p0, Lsoftware/simplicial/nebulous/a/d;->o:I

    .line 201
    move/from16 v0, p16

    iput v0, p0, Lsoftware/simplicial/nebulous/a/d;->p:I

    .line 202
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/d;->a:Lsoftware/simplicial/nebulous/f/i;

    sget-object v2, Lsoftware/simplicial/nebulous/f/i;->a:Lsoftware/simplicial/nebulous/f/i;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/d;->a:Lsoftware/simplicial/nebulous/f/i;

    sget-object v2, Lsoftware/simplicial/nebulous/f/i;->g:Lsoftware/simplicial/nebulous/f/i;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/d;->a:Lsoftware/simplicial/nebulous/f/i;

    sget-object v2, Lsoftware/simplicial/nebulous/f/i;->k:Lsoftware/simplicial/nebulous/f/i;

    if-eq v1, v2, :cond_0

    .line 203
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/a/d;->notifyDataSetChanged()V

    .line 204
    :cond_0
    return-void
.end method

.method public a(Lsoftware/simplicial/nebulous/f/i;)V
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->a:Lsoftware/simplicial/nebulous/f/i;

    .line 174
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/d;->a:Lsoftware/simplicial/nebulous/f/i;

    .line 175
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/d;->a:Lsoftware/simplicial/nebulous/f/i;

    if-eq v1, v0, :cond_0

    .line 176
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/a/d;->notifyDataSetChanged()V

    .line 177
    :cond_0
    return-void
.end method

.method public b(I)Lsoftware/simplicial/a/af;
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->a:Lsoftware/simplicial/nebulous/f/i;

    sget-object v1, Lsoftware/simplicial/nebulous/f/i;->h:Lsoftware/simplicial/nebulous/f/i;

    if-eq v0, v1, :cond_0

    .line 216
    const/4 v0, 0x0

    .line 217
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->a:Lsoftware/simplicial/nebulous/f/i;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/i;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/af;

    goto :goto_0
.end method

.method public c(I)Lsoftware/simplicial/a/as;
    .locals 2

    .prologue
    .line 222
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->a:Lsoftware/simplicial/nebulous/f/i;

    sget-object v1, Lsoftware/simplicial/nebulous/f/i;->i:Lsoftware/simplicial/nebulous/f/i;

    if-eq v0, v1, :cond_0

    .line 223
    const/4 v0, 0x0

    .line 224
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->a:Lsoftware/simplicial/nebulous/f/i;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/i;->n:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/as;

    goto :goto_0
.end method

.method public d(I)Lsoftware/simplicial/a/bd;
    .locals 3

    .prologue
    .line 229
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->a:Lsoftware/simplicial/nebulous/f/i;

    sget-object v1, Lsoftware/simplicial/nebulous/f/i;->j:Lsoftware/simplicial/nebulous/f/i;

    if-eq v0, v1, :cond_1

    .line 230
    const/4 v0, 0x0

    .line 239
    :cond_0
    :goto_0
    return-object v0

    .line 232
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->a:Lsoftware/simplicial/nebulous/f/i;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/i;->o:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bd;

    .line 233
    if-eqz v0, :cond_0

    .line 235
    iget-byte v1, v0, Lsoftware/simplicial/a/bd;->c:B

    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/d;->u:Ljava/util/Map;

    invoke-static {v1, v2}, Lsoftware/simplicial/a/bd;->b(BLjava/util/Map;)Lsoftware/simplicial/a/bd;

    move-result-object v1

    .line 236
    if-eqz v1, :cond_0

    sget-object v2, Lsoftware/simplicial/a/bd;->a:Lsoftware/simplicial/a/bd;

    if-eq v1, v2, :cond_0

    move-object v0, v1

    .line 237
    goto :goto_0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->a:Lsoftware/simplicial/nebulous/f/i;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/i;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/d;->a:Lsoftware/simplicial/nebulous/f/i;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/i;->m:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/d;->a:Lsoftware/simplicial/nebulous/f/i;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/i;->o:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/d;->a:Lsoftware/simplicial/nebulous/f/i;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/i;->n:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/a/d;->a(I)Lsoftware/simplicial/a/e;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 81
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    const v9, 0x7f0d02f8

    const v8, 0x108003f

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/16 v5, 0x8

    .line 87
    .line 89
    if-nez p2, :cond_0

    .line 90
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f04007c

    invoke-virtual {v0, v1, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 92
    :cond_0
    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/a/d;->a(I)Lsoftware/simplicial/a/e;

    move-result-object v0

    .line 93
    if-eqz v0, :cond_1

    .line 95
    new-instance v4, Lsoftware/simplicial/nebulous/a/d$b;

    invoke-direct {v4, p0, v0, p2}, Lsoftware/simplicial/nebulous/a/d$b;-><init>(Lsoftware/simplicial/nebulous/a/d;Lsoftware/simplicial/a/e;Landroid/view/View;)V

    .line 96
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/d;->v:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 99
    const v1, 0x7f0d02fa

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 100
    const v2, 0x7f0d02fb

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 101
    const v3, 0x7f0d02f9

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 103
    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 104
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 105
    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 106
    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 108
    new-instance v0, Lsoftware/simplicial/nebulous/a/d$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lsoftware/simplicial/nebulous/a/d$a;-><init>(Lsoftware/simplicial/nebulous/a/d;Lsoftware/simplicial/nebulous/a/d$1;)V

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/d;->b:Ljava/util/concurrent/ExecutorService;

    new-array v2, v7, [Lsoftware/simplicial/nebulous/a/d$b;

    aput-object v4, v2, v6

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/nebulous/a/d$a;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 111
    :cond_1
    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/a/d;->b(I)Lsoftware/simplicial/a/af;

    move-result-object v0

    .line 112
    if-eqz v0, :cond_2

    .line 114
    new-instance v4, Lsoftware/simplicial/nebulous/a/d$d;

    invoke-direct {v4, p0, v0, p2}, Lsoftware/simplicial/nebulous/a/d$d;-><init>(Lsoftware/simplicial/nebulous/a/d;Lsoftware/simplicial/a/af;Landroid/view/View;)V

    .line 115
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/d;->w:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 118
    const v1, 0x7f0d02fa

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 119
    const v2, 0x7f0d02fb

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 120
    const v3, 0x7f0d02f9

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 122
    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 123
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 124
    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 125
    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 127
    new-instance v0, Lsoftware/simplicial/nebulous/a/d$c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lsoftware/simplicial/nebulous/a/d$c;-><init>(Lsoftware/simplicial/nebulous/a/d;Lsoftware/simplicial/nebulous/a/d$1;)V

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/d;->b:Ljava/util/concurrent/ExecutorService;

    new-array v2, v7, [Lsoftware/simplicial/nebulous/a/d$d;

    aput-object v4, v2, v6

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/nebulous/a/d$c;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 130
    :cond_2
    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/a/d;->c(I)Lsoftware/simplicial/a/as;

    move-result-object v0

    .line 131
    if-eqz v0, :cond_3

    .line 133
    new-instance v4, Lsoftware/simplicial/nebulous/a/d$f;

    invoke-direct {v4, p0, v0, p2}, Lsoftware/simplicial/nebulous/a/d$f;-><init>(Lsoftware/simplicial/nebulous/a/d;Lsoftware/simplicial/a/as;Landroid/view/View;)V

    .line 134
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/d;->x:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 137
    const v1, 0x7f0d02fa

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 138
    const v2, 0x7f0d02fb

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 139
    const v3, 0x7f0d02f9

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 141
    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 142
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 143
    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 144
    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 146
    new-instance v0, Lsoftware/simplicial/nebulous/a/d$e;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lsoftware/simplicial/nebulous/a/d$e;-><init>(Lsoftware/simplicial/nebulous/a/d;Lsoftware/simplicial/nebulous/a/d$1;)V

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/d;->b:Ljava/util/concurrent/ExecutorService;

    new-array v2, v7, [Lsoftware/simplicial/nebulous/a/d$f;

    aput-object v4, v2, v6

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/nebulous/a/d$e;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 149
    :cond_3
    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/a/d;->d(I)Lsoftware/simplicial/a/bd;

    move-result-object v0

    .line 150
    if-eqz v0, :cond_4

    .line 152
    new-instance v4, Lsoftware/simplicial/nebulous/a/d$h;

    invoke-direct {v4, p0, v0, p2}, Lsoftware/simplicial/nebulous/a/d$h;-><init>(Lsoftware/simplicial/nebulous/a/d;Lsoftware/simplicial/a/bd;Landroid/view/View;)V

    .line 153
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/d;->y:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 156
    const v1, 0x7f0d02fa

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 157
    const v2, 0x7f0d02fb

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 158
    const v3, 0x7f0d02f9

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 160
    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 161
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 162
    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 163
    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 165
    new-instance v0, Lsoftware/simplicial/nebulous/a/d$g;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lsoftware/simplicial/nebulous/a/d$g;-><init>(Lsoftware/simplicial/nebulous/a/d;Lsoftware/simplicial/nebulous/a/d$1;)V

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/d;->b:Ljava/util/concurrent/ExecutorService;

    new-array v2, v7, [Lsoftware/simplicial/nebulous/a/d$h;

    aput-object v4, v2, v6

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/nebulous/a/d$g;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 168
    :cond_4
    return-object p2
.end method
