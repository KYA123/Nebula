.class public Lsoftware/simplicial/nebulous/a/e;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lsoftware/simplicial/nebulous/f/j;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lsoftware/simplicial/nebulous/application/MainActivity;


# direct methods
.method public constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V
    .locals 1

    .prologue
    .line 25
    const v0, 0x7f040077

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 27
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/e;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 28
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/a/e;)Lsoftware/simplicial/nebulous/application/MainActivity;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/e;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    return-object v0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 33
    .line 34
    if-nez p2, :cond_0

    .line 35
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/e;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f040077

    invoke-virtual {v0, v1, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 37
    :cond_0
    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/a/e;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/f/j;

    .line 39
    const v1, 0x7f0d009c

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 40
    const v2, 0x7f0d02f3

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 41
    const v3, 0x7f0d0085

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 42
    const v4, 0x7f0d02c2

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    .line 44
    iget-object v5, v0, Lsoftware/simplicial/nebulous/f/j;->b:Ljava/lang/String;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 45
    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 46
    iget-boolean v1, v0, Lsoftware/simplicial/nebulous/f/j;->c:Z

    if-eqz v1, :cond_1

    .line 48
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Lsoftware/simplicial/nebulous/f/j;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 49
    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 56
    :goto_0
    new-instance v1, Lsoftware/simplicial/nebulous/a/e$1;

    invoke-direct {v1, p0, v0}, Lsoftware/simplicial/nebulous/a/e$1;-><init>(Lsoftware/simplicial/nebulous/a/e;Lsoftware/simplicial/nebulous/f/j;)V

    invoke-virtual {v4, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    return-object p2

    .line 53
    :cond_1
    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
