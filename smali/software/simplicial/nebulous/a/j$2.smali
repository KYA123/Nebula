.class Lsoftware/simplicial/nebulous/a/j$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/a/j;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/f/m;

.field final synthetic b:Lsoftware/simplicial/nebulous/a/j;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/a/j;Lsoftware/simplicial/nebulous/f/m;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/j$2;->b:Lsoftware/simplicial/nebulous/a/j;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/a/j$2;->a:Lsoftware/simplicial/nebulous/f/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 76
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/j$2;->b:Lsoftware/simplicial/nebulous/a/j;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/a/j;->a(Lsoftware/simplicial/nebulous/a/j;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    .line 77
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/j$2;->b:Lsoftware/simplicial/nebulous/a/j;

    .line 78
    invoke-static {v1}, Lsoftware/simplicial/nebulous/a/j;->a(Lsoftware/simplicial/nebulous/a/j;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    const v2, 0x7f08002f

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/j$2;->b:Lsoftware/simplicial/nebulous/a/j;

    .line 79
    invoke-static {v2}, Lsoftware/simplicial/nebulous/a/j;->a(Lsoftware/simplicial/nebulous/a/j;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    const v3, 0x7f080231

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/j$2;->a:Lsoftware/simplicial/nebulous/f/m;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/m;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/j$2;->b:Lsoftware/simplicial/nebulous/a/j;

    .line 80
    invoke-static {v1}, Lsoftware/simplicial/nebulous/a/j;->a(Lsoftware/simplicial/nebulous/a/j;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    const v2, 0x7f080318

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/a/j$2$1;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/a/j$2$1;-><init>(Lsoftware/simplicial/nebulous/a/j$2;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/j$2;->b:Lsoftware/simplicial/nebulous/a/j;

    .line 93
    invoke-static {v1}, Lsoftware/simplicial/nebulous/a/j;->a(Lsoftware/simplicial/nebulous/a/j;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    const v2, 0x7f0801bd

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 94
    return-void
.end method
