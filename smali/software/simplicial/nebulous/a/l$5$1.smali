.class Lsoftware/simplicial/nebulous/a/l$5$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/a/l$5;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/a/l$5;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/a/l$5;)V
    .locals 0

    .prologue
    .line 240
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/l$5$1;->a:Lsoftware/simplicial/nebulous/a/l$5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 244
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/l$5$1;->a:Lsoftware/simplicial/nebulous/a/l$5;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/a/l$5;->b:Lsoftware/simplicial/nebulous/a/l;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/a/l;->a(Lsoftware/simplicial/nebulous/a/l;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 265
    :goto_0
    return-void

    .line 247
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/l$5$1;->a:Lsoftware/simplicial/nebulous/a/l$5;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/a/l$5;->b:Lsoftware/simplicial/nebulous/a/l;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/a/l;->a(Lsoftware/simplicial/nebulous/a/l;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    .line 248
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/l$5$1;->a:Lsoftware/simplicial/nebulous/a/l$5;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/a/l$5;->b:Lsoftware/simplicial/nebulous/a/l;

    .line 249
    invoke-static {v1}, Lsoftware/simplicial/nebulous/a/l;->a(Lsoftware/simplicial/nebulous/a/l;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    const v2, 0x7f08002f

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/l$5$1;->a:Lsoftware/simplicial/nebulous/a/l$5;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/a/l$5;->b:Lsoftware/simplicial/nebulous/a/l;

    .line 250
    invoke-static {v1}, Lsoftware/simplicial/nebulous/a/l;->a(Lsoftware/simplicial/nebulous/a/l;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    const v2, 0x7f08031b

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/l$5$1;->a:Lsoftware/simplicial/nebulous/a/l$5;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/a/l$5;->b:Lsoftware/simplicial/nebulous/a/l;

    .line 251
    invoke-static {v1}, Lsoftware/simplicial/nebulous/a/l;->a(Lsoftware/simplicial/nebulous/a/l;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    const v2, 0x7f0801cf

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/a/l$5$1$1;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/a/l$5$1$1;-><init>(Lsoftware/simplicial/nebulous/a/l$5$1;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/l$5$1;->a:Lsoftware/simplicial/nebulous/a/l$5;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/a/l$5;->b:Lsoftware/simplicial/nebulous/a/l;

    .line 264
    invoke-static {v1}, Lsoftware/simplicial/nebulous/a/l;->a(Lsoftware/simplicial/nebulous/a/l;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    const v2, 0x7f08005d

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0
.end method
