.class Lsoftware/simplicial/nebulous/a/m$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/a/m;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/a/bg;

.field final synthetic b:Lsoftware/simplicial/nebulous/a/m;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/a/m;Lsoftware/simplicial/a/bg;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/m$1;->b:Lsoftware/simplicial/nebulous/a/m;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/a/m$1;->a:Lsoftware/simplicial/a/bg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 117
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/m$1;->b:Lsoftware/simplicial/nebulous/a/m;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/a/m;->a(Lsoftware/simplicial/nebulous/a/m;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    if-nez v1, :cond_1

    .line 155
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    const v1, 0x7f0d02e5

    if-ne p2, v1, :cond_3

    move v2, v3

    .line 137
    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/m$1;->a:Lsoftware/simplicial/a/bg;

    iget-boolean v0, v0, Lsoftware/simplicial/a/bg;->r:Z

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/m$1;->a:Lsoftware/simplicial/a/bg;

    iget-boolean v0, v0, Lsoftware/simplicial/a/bg;->s:Z

    if-eq v0, v2, :cond_0

    .line 140
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/m$1;->a:Lsoftware/simplicial/a/bg;

    iput-boolean v3, v0, Lsoftware/simplicial/a/bg;->r:Z

    .line 141
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/m$1;->a:Lsoftware/simplicial/a/bg;

    iput-boolean v2, v0, Lsoftware/simplicial/a/bg;->s:Z

    .line 143
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/m$1;->b:Lsoftware/simplicial/nebulous/a/m;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/a/m;->a(Lsoftware/simplicial/nebulous/a/m;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/m$1;->a:Lsoftware/simplicial/a/bg;

    iget v1, v1, Lsoftware/simplicial/a/bg;->b:I

    iget-object v4, p0, Lsoftware/simplicial/nebulous/a/m$1;->a:Lsoftware/simplicial/a/bg;

    iget-boolean v4, v4, Lsoftware/simplicial/a/bg;->t:Z

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/m$1;->a:Lsoftware/simplicial/a/bg;

    iget-boolean v5, v5, Lsoftware/simplicial/a/bg;->u:Z

    invoke-virtual/range {v0 .. v5}, Lsoftware/simplicial/nebulous/f/al;->a(IZZZZ)V

    .line 148
    const-wide/16 v0, 0xfa

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 154
    :goto_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/m$1;->b:Lsoftware/simplicial/nebulous/a/m;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/a/m;->a(Lsoftware/simplicial/nebulous/a/m;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/m$1;->a:Lsoftware/simplicial/a/bg;

    iget v1, v1, Lsoftware/simplicial/a/bg;->b:I

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/t;->h(I)V

    goto :goto_0

    .line 127
    :cond_3
    const v1, 0x7f0d02e4

    if-ne p2, v1, :cond_4

    move v2, v0

    .line 130
    goto :goto_1

    :cond_4
    move v3, v0

    move v2, v0

    .line 134
    goto :goto_1

    .line 150
    :catch_0
    move-exception v0

    .line 152
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_2
.end method
