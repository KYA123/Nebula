.class Lsoftware/simplicial/nebulous/a/s$9$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/a/s$9;->onLongClick(Landroid/view/View;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/a/s$9;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/a/s$9;)V
    .locals 0

    .prologue
    .line 327
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/s$9$1;->a:Lsoftware/simplicial/nebulous/a/s$9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    .line 331
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/s$9$1;->a:Lsoftware/simplicial/nebulous/a/s$9;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/a/s$9;->b:Lsoftware/simplicial/nebulous/a/s;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/a/s;->a(Lsoftware/simplicial/nebulous/a/s;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    if-nez v1, :cond_0

    .line 346
    :goto_0
    return-void

    .line 334
    :cond_0
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/s$9$1;->a:Lsoftware/simplicial/nebulous/a/s$9;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/a/s$9;->b:Lsoftware/simplicial/nebulous/a/s;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/a/s;->a(Lsoftware/simplicial/nebulous/a/s;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->e:Lsoftware/simplicial/nebulous/f/u;

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/f/u;->d(I)V

    .line 335
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/s$9$1;->a:Lsoftware/simplicial/nebulous/a/s$9;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/a/s$9;->b:Lsoftware/simplicial/nebulous/a/s;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/a/s;->a(Lsoftware/simplicial/nebulous/a/s;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v1, v2, v0}, Lsoftware/simplicial/nebulous/f/al;->a(IZ)V

    .line 336
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move v1, v0

    .line 337
    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/s$9$1;->a:Lsoftware/simplicial/nebulous/a/s$9;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/a/s$9;->b:Lsoftware/simplicial/nebulous/a/s;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/s;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 339
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/s$9$1;->a:Lsoftware/simplicial/nebulous/a/s$9;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/a/s$9;->b:Lsoftware/simplicial/nebulous/a/s;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/a/s;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bg;

    .line 340
    iget-object v3, v0, Lsoftware/simplicial/a/bg;->h:Lsoftware/simplicial/a/bg$a;

    sget-object v4, Lsoftware/simplicial/a/bg$a;->b:Lsoftware/simplicial/a/bg$a;

    if-ne v3, v4, :cond_1

    .line 341
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 337
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 343
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bg;

    .line 344
    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/s$9$1;->a:Lsoftware/simplicial/nebulous/a/s$9;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/a/s$9;->b:Lsoftware/simplicial/nebulous/a/s;

    invoke-virtual {v2, v0}, Lsoftware/simplicial/nebulous/a/s;->remove(Ljava/lang/Object;)V

    goto :goto_2

    .line 345
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/s$9$1;->a:Lsoftware/simplicial/nebulous/a/s$9;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/a/s$9;->b:Lsoftware/simplicial/nebulous/a/s;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/s;->notifyDataSetChanged()V

    goto :goto_0
.end method
