.class Lsoftware/simplicial/nebulous/a/s$9;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/a/s;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/a/bg;

.field final synthetic b:Lsoftware/simplicial/nebulous/a/s;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/a/s;Lsoftware/simplicial/a/bg;)V
    .locals 0

    .prologue
    .line 316
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/s$9;->b:Lsoftware/simplicial/nebulous/a/s;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/a/s$9;->a:Lsoftware/simplicial/a/bg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 3

    .prologue
    .line 320
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/s$9;->a:Lsoftware/simplicial/a/bg;

    iget-object v0, v0, Lsoftware/simplicial/a/bg;->h:Lsoftware/simplicial/a/bg$a;

    sget-object v1, Lsoftware/simplicial/a/bg$a;->b:Lsoftware/simplicial/a/bg$a;

    if-ne v0, v1, :cond_0

    .line 322
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/s$9;->b:Lsoftware/simplicial/nebulous/a/s;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/a/s;->a(Lsoftware/simplicial/nebulous/a/s;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    .line 323
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/s$9;->b:Lsoftware/simplicial/nebulous/a/s;

    .line 324
    invoke-static {v1}, Lsoftware/simplicial/nebulous/a/s;->a(Lsoftware/simplicial/nebulous/a/s;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    const v2, 0x7f08002f

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/s$9;->b:Lsoftware/simplicial/nebulous/a/s;

    .line 325
    invoke-static {v1}, Lsoftware/simplicial/nebulous/a/s;->a(Lsoftware/simplicial/nebulous/a/s;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    const v2, 0x7f080212

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/s$9;->b:Lsoftware/simplicial/nebulous/a/s;

    .line 326
    invoke-static {v1}, Lsoftware/simplicial/nebulous/a/s;->a(Lsoftware/simplicial/nebulous/a/s;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    const v2, 0x7f080318

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/a/s$9$1;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/a/s$9$1;-><init>(Lsoftware/simplicial/nebulous/a/s$9;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/s$9;->b:Lsoftware/simplicial/nebulous/a/s;

    .line 348
    invoke-static {v1}, Lsoftware/simplicial/nebulous/a/s;->a(Lsoftware/simplicial/nebulous/a/s;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    const v2, 0x7f0801bd

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 349
    const/4 v0, 0x1

    .line 351
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
