.class public Lsoftware/simplicial/nebulous/a/s;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/nebulous/a/s$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lsoftware/simplicial/a/bg;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lsoftware/simplicial/nebulous/application/MainActivity;

.field private final b:Lsoftware/simplicial/nebulous/a/s$a;

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/nebulous/a/s$a;)V
    .locals 1

    .prologue
    .line 47
    const v0, 0x7f040071

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 49
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 50
    iput-object p2, p0, Lsoftware/simplicial/nebulous/a/s;->b:Lsoftware/simplicial/nebulous/a/s$a;

    .line 51
    return-void
.end method

.method private a(Lsoftware/simplicial/a/aq;Z)Ljava/lang/String;
    .locals 2

    .prologue
    .line 642
    if-eqz p2, :cond_0

    .line 643
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f080149

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 651
    :goto_0
    return-object v0

    .line 644
    :cond_0
    sget-object v0, Lsoftware/simplicial/nebulous/a/s$3;->c:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/aq;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 651
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f0802dc

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 647
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f080204

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 649
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f0801ff

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 644
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/a/s;)Lsoftware/simplicial/nebulous/application/MainActivity;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    return-object v0
.end method

.method private a(Lsoftware/simplicial/a/bg;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/view/View;Landroid/widget/ImageView;)V
    .locals 9

    .prologue
    .line 396
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->e:Lsoftware/simplicial/nebulous/f/u;

    iget v2, p1, Lsoftware/simplicial/a/bg;->b:I

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/f/u;->c(I)Lsoftware/simplicial/nebulous/f/t;

    move-result-object v1

    .line 397
    if-eqz v1, :cond_0

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/t;->c:Z

    if-nez v1, :cond_3

    :cond_0
    const/16 v1, 0x8

    :goto_0
    move-object/from16 v0, p10

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 398
    iget-object v1, p1, Lsoftware/simplicial/a/bg;->h:Lsoftware/simplicial/a/bg$a;

    sget-object v2, Lsoftware/simplicial/a/bg$a;->d:Lsoftware/simplicial/a/bg$a;

    if-eq v1, v2, :cond_1

    iget-object v1, p1, Lsoftware/simplicial/a/bg;->h:Lsoftware/simplicial/a/bg$a;

    sget-object v2, Lsoftware/simplicial/a/bg$a;->c:Lsoftware/simplicial/a/bg$a;

    if-ne v1, v2, :cond_4

    :cond_1
    const/4 v1, 0x0

    :goto_1
    move-object/from16 v0, p11

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 399
    iget-boolean v1, p1, Lsoftware/simplicial/a/bg;->i:Z

    if-eqz v1, :cond_5

    const v1, 0x7f0201a6

    :goto_2
    move-object/from16 v0, p11

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 401
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->k:Lsoftware/simplicial/a/bn;

    iget-object v2, p1, Lsoftware/simplicial/a/bg;->l:Lsoftware/simplicial/a/bn;

    if-ne v1, v2, :cond_6

    sget-object v1, Lsoftware/simplicial/a/ai;->Y:Lsoftware/simplicial/a/ak;

    iget-object v2, p1, Lsoftware/simplicial/a/bg;->m:Lsoftware/simplicial/a/ak;

    if-ne v1, v2, :cond_6

    const/4 v1, 0x1

    .line 403
    :goto_3
    const/16 v2, 0x8

    invoke-virtual {p3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 405
    iget-object v4, p1, Lsoftware/simplicial/a/bg;->h:Lsoftware/simplicial/a/bg$a;

    .line 406
    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/s;->b:Lsoftware/simplicial/nebulous/a/s$a;

    sget-object v3, Lsoftware/simplicial/nebulous/a/s$a;->c:Lsoftware/simplicial/nebulous/a/s$a;

    if-ne v2, v3, :cond_2

    .line 407
    sget-object v2, Lsoftware/simplicial/a/bg$a;->d:Lsoftware/simplicial/a/bg$a;

    iput-object v2, p1, Lsoftware/simplicial/a/bg;->h:Lsoftware/simplicial/a/bg$a;

    .line 408
    :cond_2
    sget-object v2, Lsoftware/simplicial/nebulous/a/s$3;->b:[I

    iget-object v3, p1, Lsoftware/simplicial/a/bg;->h:Lsoftware/simplicial/a/bg$a;

    invoke-virtual {v3}, Lsoftware/simplicial/a/bg$a;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 637
    :goto_4
    iput-object v4, p1, Lsoftware/simplicial/a/bg;->h:Lsoftware/simplicial/a/bg$a;

    .line 638
    return-void

    .line 397
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 398
    :cond_4
    const/4 v1, 0x4

    goto :goto_1

    .line 399
    :cond_5
    const v1, 0x7f0201a7

    goto :goto_2

    .line 401
    :cond_6
    const/4 v1, 0x0

    goto :goto_3

    .line 411
    :pswitch_0
    const/16 v1, 0x8

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 412
    const/16 v1, 0x8

    move-object/from16 v0, p8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 413
    const/16 v1, 0x8

    invoke-virtual {p4, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 414
    const/16 v1, 0x8

    invoke-virtual {p5, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 415
    const/16 v1, 0x8

    invoke-virtual {p6, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 416
    const/16 v1, 0x8

    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 417
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f0802dc

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 418
    const/16 v1, 0xff

    const/16 v2, 0xff

    const/16 v3, 0xff

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_4

    .line 421
    :pswitch_1
    const/4 v1, 0x0

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 422
    const/4 v1, 0x0

    move-object/from16 v0, p8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 423
    const/16 v1, 0x8

    invoke-virtual {p4, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 424
    const/16 v1, 0x8

    invoke-virtual {p5, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 425
    const/16 v1, 0x8

    invoke-virtual {p6, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 426
    const/16 v1, 0x8

    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 427
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f0801b8

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 428
    const/4 v1, 0x0

    const/16 v2, 0xff

    const/16 v3, 0xff

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_4

    .line 431
    :pswitch_2
    const/16 v1, 0x8

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 432
    const/4 v1, 0x0

    move-object/from16 v0, p8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 433
    const/16 v1, 0x8

    invoke-virtual {p4, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 434
    const/16 v1, 0x8

    invoke-virtual {p5, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 435
    const/16 v1, 0x8

    invoke-virtual {p6, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 436
    const/16 v1, 0x8

    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 437
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f080131

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 438
    const/4 v1, 0x0

    const/16 v2, 0xff

    const/16 v3, 0xff

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_4

    .line 441
    :pswitch_3
    const/16 v2, 0x8

    move-object/from16 v0, p7

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 442
    const/4 v2, 0x0

    move-object/from16 v0, p8

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 443
    const/4 v2, 0x0

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 444
    sget-object v2, Lsoftware/simplicial/nebulous/a/s$3;->a:[I

    iget-object v3, p1, Lsoftware/simplicial/a/bg;->j:Lsoftware/simplicial/a/bg$b;

    invoke-virtual {v3}, Lsoftware/simplicial/a/bg$b;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    .line 514
    const/16 v2, 0x8

    invoke-virtual {p4, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 515
    const/16 v2, 0x8

    invoke-virtual {p5, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 516
    const/16 v2, 0x8

    invoke-virtual {p6, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 517
    const/16 v2, 0x8

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 521
    :goto_5
    const/4 v2, 0x0

    const/16 v3, 0xff

    const/4 v5, 0x0

    invoke-static {v2, v3, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 522
    const-string v2, ""

    .line 523
    iget-boolean v3, p1, Lsoftware/simplicial/a/bg;->q:Z

    if-eqz v3, :cond_7

    .line 524
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f080186

    invoke-virtual {v3, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 525
    :cond_7
    const/4 v3, 0x0

    .line 526
    sget-object v5, Lsoftware/simplicial/nebulous/a/s$3;->a:[I

    iget-object v6, p1, Lsoftware/simplicial/a/bg;->j:Lsoftware/simplicial/a/bg$b;

    invoke-virtual {v6}, Lsoftware/simplicial/a/bg$b;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_2

    .line 613
    const/16 v5, 0xff

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/graphics/Color;->rgb(III)I

    move-result v5

    invoke-virtual {p2, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 614
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v6, 0x7f0802dc

    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 617
    :goto_6
    if-eqz v3, :cond_8

    .line 618
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lsoftware/simplicial/a/bg;->k:Lsoftware/simplicial/a/aq;

    iget-boolean v5, p1, Lsoftware/simplicial/a/bg;->o:Z

    invoke-direct {p0, v3, v5}, Lsoftware/simplicial/nebulous/a/s;->a(Lsoftware/simplicial/a/aq;Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 619
    :cond_8
    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 621
    iget-object v2, p1, Lsoftware/simplicial/a/bg;->j:Lsoftware/simplicial/a/bg$b;

    sget-object v3, Lsoftware/simplicial/a/bg$b;->c:Lsoftware/simplicial/a/bg$b;

    if-eq v2, v3, :cond_9

    iget-object v2, p1, Lsoftware/simplicial/a/bg;->j:Lsoftware/simplicial/a/bg$b;

    sget-object v3, Lsoftware/simplicial/a/bg$b;->f:Lsoftware/simplicial/a/bg$b;

    if-eq v2, v3, :cond_9

    iget-object v2, p1, Lsoftware/simplicial/a/bg;->j:Lsoftware/simplicial/a/bg$b;

    sget-object v3, Lsoftware/simplicial/a/bg$b;->a:Lsoftware/simplicial/a/bg$b;

    if-eq v2, v3, :cond_9

    iget-object v2, p1, Lsoftware/simplicial/a/bg;->j:Lsoftware/simplicial/a/bg$b;

    sget-object v3, Lsoftware/simplicial/a/bg$b;->b:Lsoftware/simplicial/a/bg$b;

    if-ne v2, v3, :cond_15

    .line 624
    :cond_9
    const/16 v1, 0x8

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 448
    :pswitch_4
    const/16 v2, 0x8

    invoke-virtual {p4, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 449
    const/16 v2, 0x8

    invoke-virtual {p5, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 450
    const/16 v2, 0x8

    invoke-virtual {p6, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 451
    const/4 v2, 0x0

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_5

    .line 454
    :pswitch_5
    const/16 v2, 0x8

    invoke-virtual {p4, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 455
    const/16 v2, 0x8

    invoke-virtual {p5, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 456
    const/16 v2, 0x8

    invoke-virtual {p6, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 457
    const/4 v2, 0x0

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_5

    .line 460
    :pswitch_6
    if-eqz v1, :cond_a

    const/4 v2, 0x0

    :goto_7
    invoke-virtual {p4, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 461
    const/16 v2, 0x8

    invoke-virtual {p5, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 462
    if-eqz v1, :cond_b

    const/4 v2, 0x0

    :goto_8
    invoke-virtual {p6, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 463
    const/4 v2, 0x0

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_5

    .line 460
    :cond_a
    const/16 v2, 0x8

    goto :goto_7

    .line 462
    :cond_b
    const/16 v2, 0x8

    goto :goto_8

    .line 466
    :pswitch_7
    if-eqz v1, :cond_c

    const/4 v2, 0x0

    :goto_9
    invoke-virtual {p4, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 467
    const/16 v2, 0x8

    invoke-virtual {p5, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 468
    if-eqz v1, :cond_d

    const/4 v2, 0x0

    :goto_a
    invoke-virtual {p6, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 469
    const/4 v2, 0x0

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_5

    .line 466
    :cond_c
    const/16 v2, 0x8

    goto :goto_9

    .line 468
    :cond_d
    const/16 v2, 0x8

    goto :goto_a

    .line 472
    :pswitch_8
    if-eqz v1, :cond_e

    const/4 v2, 0x0

    :goto_b
    invoke-virtual {p4, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 473
    const/16 v2, 0x8

    invoke-virtual {p5, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 474
    if-eqz v1, :cond_f

    const/4 v2, 0x0

    :goto_c
    invoke-virtual {p6, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 475
    const/4 v2, 0x0

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_5

    .line 472
    :cond_e
    const/16 v2, 0x8

    goto :goto_b

    .line 474
    :cond_f
    const/16 v2, 0x8

    goto :goto_c

    .line 478
    :pswitch_9
    if-eqz v1, :cond_10

    const/4 v2, 0x0

    :goto_d
    invoke-virtual {p4, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 479
    const/16 v2, 0x8

    invoke-virtual {p5, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 480
    if-eqz v1, :cond_11

    const/4 v2, 0x0

    :goto_e
    invoke-virtual {p6, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 481
    const/4 v2, 0x0

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_5

    .line 478
    :cond_10
    const/16 v2, 0x8

    goto :goto_d

    .line 480
    :cond_11
    const/16 v2, 0x8

    goto :goto_e

    .line 484
    :pswitch_a
    const/16 v2, 0x8

    invoke-virtual {p4, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 485
    const/16 v2, 0x8

    invoke-virtual {p5, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 486
    const/16 v2, 0x8

    invoke-virtual {p6, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 487
    const/4 v2, 0x0

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_5

    .line 506
    :pswitch_b
    if-eqz v1, :cond_12

    const/4 v2, 0x0

    :goto_f
    invoke-virtual {p4, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 507
    if-eqz v1, :cond_13

    const/4 v2, 0x0

    :goto_10
    invoke-virtual {p5, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 508
    if-eqz v1, :cond_14

    const/4 v2, 0x0

    :goto_11
    invoke-virtual {p6, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 509
    const/4 v2, 0x0

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_5

    .line 506
    :cond_12
    const/16 v2, 0x8

    goto :goto_f

    .line 507
    :cond_13
    const/16 v2, 0x8

    goto :goto_10

    .line 508
    :cond_14
    const/16 v2, 0x8

    goto :goto_11

    .line 529
    :pswitch_c
    const/16 v5, 0xff

    const/16 v6, 0xff

    const/16 v7, 0xff

    invoke-static {v5, v6, v7}, Landroid/graphics/Color;->rgb(III)I

    move-result v5

    invoke-virtual {p2, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 530
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v6, 0x7f08017d

    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_6

    .line 533
    :pswitch_d
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v6, 0x7f0801d5

    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_6

    .line 536
    :pswitch_e
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f080076

    invoke-virtual {v3, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 537
    const/4 v2, 0x1

    move v8, v2

    move-object v2, v3

    move v3, v8

    .line 538
    goto/16 :goto_6

    .line 540
    :pswitch_f
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f0800dc

    invoke-virtual {v3, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 541
    const/4 v2, 0x1

    move v8, v2

    move-object v2, v3

    move v3, v8

    .line 542
    goto/16 :goto_6

    .line 544
    :pswitch_10
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f0801db

    invoke-virtual {v3, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 545
    const/4 v2, 0x1

    move v8, v2

    move-object v2, v3

    move v3, v8

    .line 546
    goto/16 :goto_6

    .line 548
    :pswitch_11
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f08011d

    invoke-virtual {v3, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 549
    const/4 v2, 0x1

    move v8, v2

    move-object v2, v3

    move v3, v8

    .line 550
    goto/16 :goto_6

    .line 552
    :pswitch_12
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f080124

    invoke-virtual {v3, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 553
    const/4 v2, 0x1

    move v8, v2

    move-object v2, v3

    move v3, v8

    .line 554
    goto/16 :goto_6

    .line 556
    :pswitch_13
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f08032d

    invoke-virtual {v3, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 557
    const/4 v2, 0x1

    move v8, v2

    move-object v2, v3

    move v3, v8

    .line 558
    goto/16 :goto_6

    .line 560
    :pswitch_14
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f080122

    invoke-virtual {v3, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 561
    const/4 v2, 0x1

    move v8, v2

    move-object v2, v3

    move v3, v8

    .line 562
    goto/16 :goto_6

    .line 564
    :pswitch_15
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f080252

    invoke-virtual {v3, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 565
    const/4 v2, 0x1

    move v8, v2

    move-object v2, v3

    move v3, v8

    .line 566
    goto/16 :goto_6

    .line 568
    :pswitch_16
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v6, 0x7f0801d5

    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_6

    .line 571
    :pswitch_17
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v6, 0x7f080095

    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_6

    .line 574
    :pswitch_18
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v6, 0x7f080031

    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_6

    .line 577
    :pswitch_19
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v6, 0x7f0802a6

    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_6

    .line 580
    :pswitch_1a
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v6, 0x7f0802c5

    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_6

    .line 584
    :pswitch_1b
    const/16 v5, 0xff

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/graphics/Color;->rgb(III)I

    move-result v5

    invoke-virtual {p2, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 585
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v6, 0x7f0801d2

    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_6

    .line 588
    :pswitch_1c
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f0802a1

    invoke-virtual {v3, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 589
    const/4 v2, 0x1

    move v8, v2

    move-object v2, v3

    move v3, v8

    .line 590
    goto/16 :goto_6

    .line 592
    :pswitch_1d
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f0802a2

    invoke-virtual {v3, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 593
    const/4 v2, 0x1

    move v8, v2

    move-object v2, v3

    move v3, v8

    .line 594
    goto/16 :goto_6

    .line 596
    :pswitch_1e
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f0802a0

    invoke-virtual {v3, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 597
    const/4 v2, 0x1

    move v8, v2

    move-object v2, v3

    move v3, v8

    .line 598
    goto/16 :goto_6

    .line 600
    :pswitch_1f
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f08024e

    invoke-virtual {v3, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 601
    const/4 v2, 0x1

    move v8, v2

    move-object v2, v3

    move v3, v8

    .line 602
    goto/16 :goto_6

    .line 608
    :pswitch_20
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f080103

    invoke-virtual {v3, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 609
    const/4 v2, 0x1

    move v8, v2

    move-object v2, v3

    move v3, v8

    .line 610
    goto/16 :goto_6

    .line 628
    :cond_15
    const/4 v2, 0x0

    invoke-virtual {p3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 629
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p1, Lsoftware/simplicial/a/bg;->m:Lsoftware/simplicial/a/ak;

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v3, v5}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/ak;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-short v3, p1, Lsoftware/simplicial/a/bg;->n:S

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lsoftware/simplicial/a/bg;->l:Lsoftware/simplicial/a/bn;

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 631
    invoke-virtual {v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v3, v5}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/bn;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 632
    invoke-virtual {p3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 633
    if-eqz v1, :cond_16

    const/4 v1, 0x0

    const/16 v2, 0xff

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    :goto_12
    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_4

    :cond_16
    const/16 v1, 0xff

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    goto :goto_12

    .line 408
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 444
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_b
    .end packed-switch

    .line 526
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_1b
        :pswitch_1b
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_d
        :pswitch_1c
        :pswitch_1e
        :pswitch_14
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_15
        :pswitch_1f
        :pswitch_1d
        :pswitch_20
        :pswitch_20
        :pswitch_20
        :pswitch_20
        :pswitch_20
        :pswitch_c
    .end packed-switch
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/a/s;Lsoftware/simplicial/a/bg;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/view/View;Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct/range {p0 .. p11}, Lsoftware/simplicial/nebulous/a/s;->a(Lsoftware/simplicial/a/bg;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/view/View;Landroid/widget/ImageView;)V

    return-void
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/a/s;)I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lsoftware/simplicial/nebulous/a/s;->c:I

    return v0
.end method

.method static synthetic c(Lsoftware/simplicial/nebulous/a/s;)I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lsoftware/simplicial/nebulous/a/s;->d:I

    return v0
.end method


# virtual methods
.method public a(II)V
    .locals 0

    .prologue
    .line 708
    iput p1, p0, Lsoftware/simplicial/nebulous/a/s;->c:I

    .line 709
    iput p2, p0, Lsoftware/simplicial/nebulous/a/s;->d:I

    .line 710
    return-void
.end method

.method public a([I[Lsoftware/simplicial/a/bg$b;[Lsoftware/simplicial/a/aq;[Z[Lsoftware/simplicial/a/bn;[Lsoftware/simplicial/a/ak;[S)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 657
    move v1, v2

    :goto_0
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/a/s;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 660
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/a/s;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bg;

    move v3, v2

    .line 662
    :goto_1
    array-length v4, p1

    if-ge v3, v4, :cond_3

    .line 664
    iget v4, v0, Lsoftware/simplicial/a/bg;->b:I

    aget v5, p1, v3

    if-ne v4, v5, :cond_1

    .line 666
    aget-object v4, p2, v3

    iput-object v4, v0, Lsoftware/simplicial/a/bg;->j:Lsoftware/simplicial/a/bg$b;

    .line 667
    aget-object v4, p3, v3

    iput-object v4, v0, Lsoftware/simplicial/a/bg;->k:Lsoftware/simplicial/a/aq;

    .line 668
    aget-boolean v4, p4, v3

    iput-boolean v4, v0, Lsoftware/simplicial/a/bg;->o:Z

    .line 669
    aget-object v4, p6, v3

    iput-object v4, v0, Lsoftware/simplicial/a/bg;->m:Lsoftware/simplicial/a/ak;

    .line 670
    aget-object v4, p5, v3

    iput-object v4, v0, Lsoftware/simplicial/a/bg;->l:Lsoftware/simplicial/a/bn;

    .line 671
    aget-short v3, p7, v3

    iput-short v3, v0, Lsoftware/simplicial/a/bg;->n:S

    .line 672
    const/4 v3, 0x1

    .line 676
    :goto_2
    if-nez v3, :cond_0

    .line 677
    sget-object v3, Lsoftware/simplicial/a/bg$b;->c:Lsoftware/simplicial/a/bg$b;

    iput-object v3, v0, Lsoftware/simplicial/a/bg;->j:Lsoftware/simplicial/a/bg$b;

    .line 657
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 662
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 681
    :cond_2
    new-instance v0, Lsoftware/simplicial/nebulous/a/s$2;

    invoke-direct {v0, p0}, Lsoftware/simplicial/nebulous/a/s$2;-><init>(Lsoftware/simplicial/nebulous/a/s;)V

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/a/s;->sort(Ljava/util/Comparator;)V

    .line 703
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/a/s;->notifyDataSetChanged()V

    .line 704
    return-void

    :cond_3
    move v3, v2

    goto :goto_2
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 27

    .prologue
    .line 56
    .line 57
    if-nez p2, :cond_0

    .line 58
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    const v3, 0x7f040071

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 60
    :cond_0
    invoke-virtual/range {p0 .. p1}, Lsoftware/simplicial/nebulous/a/s;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsoftware/simplicial/a/bg;

    .line 62
    const v2, 0x7f0d009c

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 63
    const v4, 0x7f0d02db

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    move-object v6, v4

    check-cast v6, Landroid/widget/TextView;

    .line 64
    const v4, 0x7f0d0085

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 65
    const v5, 0x7f0d02dc

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 66
    iget-object v7, v3, Lsoftware/simplicial/a/bg;->c:Ljava/lang/CharSequence;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    iget-object v2, v3, Lsoftware/simplicial/a/bg;->h:Lsoftware/simplicial/a/bg$a;

    sget-object v7, Lsoftware/simplicial/a/bg$a;->b:Lsoftware/simplicial/a/bg$a;

    if-ne v2, v7, :cond_1

    .line 69
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v8, 0x7f080023

    invoke-virtual {v7, v8}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v7, v3, Lsoftware/simplicial/a/bg;->b:I

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    :goto_0
    const v2, 0x7f0d02c9

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageButton;

    .line 83
    const v2, 0x7f0d02df

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageButton;

    .line 84
    const v2, 0x7f0d02c5

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageButton;

    .line 85
    const v2, 0x7f0d02ca

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageButton;

    .line 86
    const v2, 0x7f0d02c2

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageButton;

    .line 87
    const v2, 0x7f0d007a

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageButton;

    .line 88
    const v2, 0x7f0d02eb

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    .line 89
    const v2, 0x7f0d02e1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    move-object/from16 v2, p0

    .line 91
    invoke-direct/range {v2 .. v13}, Lsoftware/simplicial/nebulous/a/s;->a(Lsoftware/simplicial/a/bg;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/view/View;Landroid/widget/ImageView;)V

    .line 93
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/a/s;->b:Lsoftware/simplicial/nebulous/a/s$a;

    sget-object v14, Lsoftware/simplicial/nebulous/a/s$a;->a:Lsoftware/simplicial/nebulous/a/s$a;

    if-ne v2, v14, :cond_3

    .line 95
    new-instance v14, Lsoftware/simplicial/nebulous/a/s$1;

    move-object/from16 v15, p0

    move-object/from16 v16, v3

    move-object/from16 v17, v4

    move-object/from16 v18, v5

    move-object/from16 v19, v6

    move-object/from16 v20, v7

    move-object/from16 v21, v8

    move-object/from16 v22, v9

    move-object/from16 v23, v10

    move-object/from16 v24, v11

    move-object/from16 v25, v12

    move-object/from16 v26, v13

    invoke-direct/range {v14 .. v26}, Lsoftware/simplicial/nebulous/a/s$1;-><init>(Lsoftware/simplicial/nebulous/a/s;Lsoftware/simplicial/a/bg;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    invoke-virtual {v9, v14}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    new-instance v2, Lsoftware/simplicial/nebulous/a/s$4;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lsoftware/simplicial/nebulous/a/s$4;-><init>(Lsoftware/simplicial/nebulous/a/s;Lsoftware/simplicial/a/bg;)V

    invoke-virtual {v11, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    new-instance v2, Lsoftware/simplicial/nebulous/a/s$5;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lsoftware/simplicial/nebulous/a/s$5;-><init>(Lsoftware/simplicial/nebulous/a/s;Lsoftware/simplicial/a/bg;)V

    invoke-virtual {v6, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    new-instance v2, Lsoftware/simplicial/nebulous/a/s$6;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lsoftware/simplicial/nebulous/a/s$6;-><init>(Lsoftware/simplicial/nebulous/a/s;Lsoftware/simplicial/a/bg;)V

    invoke-virtual {v7, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 229
    new-instance v2, Lsoftware/simplicial/nebulous/a/s$7;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lsoftware/simplicial/nebulous/a/s$7;-><init>(Lsoftware/simplicial/nebulous/a/s;Lsoftware/simplicial/a/bg;)V

    invoke-virtual {v8, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 288
    new-instance v2, Lsoftware/simplicial/nebulous/a/s$8;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lsoftware/simplicial/nebulous/a/s$8;-><init>(Lsoftware/simplicial/nebulous/a/s;Lsoftware/simplicial/a/bg;)V

    invoke-virtual {v10, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 315
    new-instance v2, Lsoftware/simplicial/nebulous/a/s$9;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lsoftware/simplicial/nebulous/a/s$9;-><init>(Lsoftware/simplicial/nebulous/a/s;Lsoftware/simplicial/a/bg;)V

    invoke-virtual {v10, v2}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 355
    new-instance v2, Lsoftware/simplicial/nebulous/a/s$10;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lsoftware/simplicial/nebulous/a/s$10;-><init>(Lsoftware/simplicial/nebulous/a/s;Lsoftware/simplicial/a/bg;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 379
    :goto_1
    new-instance v2, Lsoftware/simplicial/nebulous/a/s$11;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3, v13}, Lsoftware/simplicial/nebulous/a/s$11;-><init>(Lsoftware/simplicial/nebulous/a/s;Lsoftware/simplicial/a/bg;Landroid/widget/ImageView;)V

    invoke-virtual {v13, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 390
    return-object p2

    .line 73
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/nebulous/a/s;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v8, 0x7f08017a

    invoke-virtual {v7, v8}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v8, v3, Lsoftware/simplicial/a/bg;->g:J

    invoke-static {v8, v9}, Lsoftware/simplicial/a/aw;->a(J)I

    move-result v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 74
    iget-object v7, v3, Lsoftware/simplicial/a/bg;->p:Ljava/util/Date;

    if-eqz v7, :cond_2

    .line 76
    const/4 v7, 0x3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    invoke-static {v7, v8}, Ljava/text/DateFormat;->getDateInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v7

    .line 77
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v8, " ("

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v8, v3, Lsoftware/simplicial/a/bg;->p:Ljava/util/Date;

    invoke-virtual {v7, v8}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, ")"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 79
    :cond_2
    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 370
    :cond_3
    const/16 v2, 0x8

    invoke-virtual {v11, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 371
    const/16 v2, 0x8

    invoke-virtual {v9, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 372
    const/16 v2, 0x8

    invoke-virtual {v10, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 373
    const/16 v2, 0x8

    invoke-virtual {v7, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 374
    const/16 v2, 0x8

    invoke-virtual {v6, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 375
    const/16 v2, 0x8

    invoke-virtual {v8, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 376
    const/16 v2, 0x8

    invoke-virtual {v12, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1
.end method
