.class Lsoftware/simplicial/nebulous/application/MainActivity$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/MainActivity;->a(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Lsoftware/simplicial/nebulous/application/MainActivity;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;I)V
    .locals 0

    .prologue
    .line 1395
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$3;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iput p2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$3;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const v4, 0x7f08014e

    const/16 v3, 0x8

    .line 1399
    iget v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$3;->a:I

    if-gez v0, :cond_1

    .line 1401
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$3;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->b(Ljava/lang/String;)V

    .line 1428
    :cond_0
    :goto_0
    return-void

    .line 1405
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$3;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$3;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2, v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$3;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->b(Ljava/lang/String;)V

    .line 1406
    iget v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$3;->a:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 1408
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$3;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    .line 1409
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$3;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f0802ee

    .line 1410
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$3;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 1411
    invoke-virtual {v1, v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$3;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f0801cf

    .line 1412
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/MainActivity$3$1;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/MainActivity$3$1;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity$3;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 1421
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 1422
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v3, v3}, Landroid/view/Window;->setFlags(II)V

    .line 1423
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 1424
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$3;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 1425
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Window;->clearFlags(I)V

    goto/16 :goto_0
.end method
