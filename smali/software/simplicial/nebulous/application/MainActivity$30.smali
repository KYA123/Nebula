.class Lsoftware/simplicial/nebulous/application/MainActivity$30;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$l;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/MainActivity;->a(Ljava/lang/String;ILsoftware/simplicial/nebulous/f/al$l;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/app/ProgressDialog;

.field final synthetic b:Lsoftware/simplicial/nebulous/f/al$l;

.field final synthetic c:Lsoftware/simplicial/nebulous/application/MainActivity;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;Landroid/app/ProgressDialog;Lsoftware/simplicial/nebulous/f/al$l;)V
    .locals 0

    .prologue
    .line 3218
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->a:Landroid/app/ProgressDialog;

    iput-object p3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->b:Lsoftware/simplicial/nebulous/f/al$l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ZLjava/lang/String;I)V
    .locals 7

    .prologue
    .line 3222
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 3223
    if-eqz p1, :cond_6

    .line 3225
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/t;->e(Ljava/lang/String;)V

    .line 3226
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f08020b

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 3227
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v1}, Lsoftware/simplicial/a/t;->a()Lsoftware/simplicial/a/f/aa;

    move-result-object v1

    sget-object v2, Lsoftware/simplicial/a/f/aa;->c:Lsoftware/simplicial/a/f/aa;

    if-ne v1, v2, :cond_0

    const-string v1, "CLAN_CONTRIBUTION"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3228
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f080325

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3229
    :cond_0
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f08020b

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v4, 0x7f0801cf

    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v0, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 3231
    const-string v0, "XP_2X_1HR"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 3233
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/application/MainActivity;I)I

    .line 3234
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x36ee80

    add-long/2addr v2, v4

    invoke-static {v0, v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/application/MainActivity;J)J

    .line 3328
    :cond_1
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v0, v1, :cond_2

    .line 3329
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->x(Lsoftware/simplicial/nebulous/application/MainActivity;)I

    move-result v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->y(Lsoftware/simplicial/nebulous/application/MainActivity;)J

    move-result-wide v2

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->ad:Lsoftware/simplicial/a/s;

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-wide v5, v5, Lsoftware/simplicial/nebulous/application/MainActivity;->ae:J

    invoke-virtual/range {v0 .. v6}, Lsoftware/simplicial/a/t;->a(IJLsoftware/simplicial/a/s;J)V

    .line 3331
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->C:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->D:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->ag:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_4

    .line 3332
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->B:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    .line 3334
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->ai:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_5

    .line 3335
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->ah:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    .line 3338
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->am:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_6

    .line 3339
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->x:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    .line 3342
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->b:Lsoftware/simplicial/nebulous/f/al$l;

    if-eqz v0, :cond_7

    .line 3343
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->b:Lsoftware/simplicial/nebulous/f/al$l;

    invoke-interface {v0, p1, p2, p3}, Lsoftware/simplicial/nebulous/f/al$l;->a(ZLjava/lang/String;I)V

    .line 3344
    :cond_7
    return-void

    .line 3236
    :cond_8
    const-string v0, "XP_3X_1HR"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 3238
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/application/MainActivity;I)I

    .line 3239
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x36ee80

    add-long/2addr v2, v4

    invoke-static {v0, v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/application/MainActivity;J)J

    goto/16 :goto_0

    .line 3241
    :cond_9
    const-string v0, "XP_2X_24HR"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 3243
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/application/MainActivity;I)I

    .line 3244
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    add-long/2addr v2, v4

    invoke-static {v0, v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/application/MainActivity;J)J

    goto/16 :goto_0

    .line 3246
    :cond_a
    const-string v0, "XP_2X_PERM"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 3248
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/application/MainActivity;I)I

    .line 3249
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-wide/16 v2, -0x1

    invoke-static {v0, v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/application/MainActivity;J)J

    goto/16 :goto_0

    .line 3251
    :cond_b
    const-string v0, "XP_3X_24HR"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 3253
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/application/MainActivity;I)I

    .line 3254
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    add-long/2addr v2, v4

    invoke-static {v0, v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/application/MainActivity;J)J

    goto/16 :goto_0

    .line 3256
    :cond_c
    const-string v0, "XP_3X_PERM"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 3258
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/application/MainActivity;I)I

    .line 3259
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-wide/16 v2, -0x1

    invoke-static {v0, v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/application/MainActivity;J)J

    goto/16 :goto_0

    .line 3261
    :cond_d
    const-string v0, "AUTO_1HR"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 3263
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/a/s;->b:Lsoftware/simplicial/a/s;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ad:Lsoftware/simplicial/a/s;

    .line 3264
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x36ee80

    add-long/2addr v2, v4

    iput-wide v2, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ae:J

    goto/16 :goto_0

    .line 3266
    :cond_e
    const-string v0, "ULTRA_1HR"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 3268
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/a/s;->c:Lsoftware/simplicial/a/s;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ad:Lsoftware/simplicial/a/s;

    .line 3269
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x36ee80

    add-long/2addr v2, v4

    iput-wide v2, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ae:J

    goto/16 :goto_0

    .line 3271
    :cond_f
    const-string v0, "AUTO_24HR"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 3273
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/a/s;->b:Lsoftware/simplicial/a/s;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ad:Lsoftware/simplicial/a/s;

    .line 3274
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    add-long/2addr v2, v4

    iput-wide v2, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ae:J

    goto/16 :goto_0

    .line 3276
    :cond_10
    const-string v0, "ULTRA_24HR"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 3278
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/a/s;->c:Lsoftware/simplicial/a/s;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ad:Lsoftware/simplicial/a/s;

    .line 3279
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    add-long/2addr v2, v4

    iput-wide v2, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ae:J

    goto/16 :goto_0

    .line 3281
    :cond_11
    const-string v0, "AUTO_PERM"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 3283
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/a/s;->b:Lsoftware/simplicial/a/s;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ad:Lsoftware/simplicial/a/s;

    .line 3284
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-wide/16 v2, -0x1

    iput-wide v2, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ae:J

    goto/16 :goto_0

    .line 3286
    :cond_12
    const-string v0, "ULTRA_PERM"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 3288
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/a/s;->c:Lsoftware/simplicial/a/s;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ad:Lsoftware/simplicial/a/s;

    .line 3289
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-wide/16 v2, -0x1

    iput-wide v2, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ae:J

    goto/16 :goto_0

    .line 3292
    :cond_13
    const-string v0, "AVATAR"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 3294
    if-ltz p3, :cond_1

    sget-object v0, Lsoftware/simplicial/a/e;->a:[Lsoftware/simplicial/a/e;

    array-length v0, v0

    if-ge p3, v0, :cond_1

    .line 3296
    sget-object v0, Lsoftware/simplicial/a/e;->a:[Lsoftware/simplicial/a/e;

    aget-object v0, v0, p3

    .line 3297
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag$a;->t:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 3298
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->f:Lsoftware/simplicial/nebulous/d/d;

    invoke-virtual {v1, v0}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/e;)Z

    goto/16 :goto_0

    .line 3301
    :cond_14
    const-string v0, "EJECT_SKIN"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 3303
    invoke-static {p3}, Lsoftware/simplicial/a/af;->a(I)Lsoftware/simplicial/a/af;

    move-result-object v0

    .line 3304
    sget-object v1, Lsoftware/simplicial/a/af;->a:Lsoftware/simplicial/a/af;

    if-eq v0, v1, :cond_1

    .line 3306
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag$a;->u:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 3307
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->f:Lsoftware/simplicial/nebulous/d/d;

    invoke-virtual {v1, v0}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/af;)Z

    goto/16 :goto_0

    .line 3310
    :cond_15
    const-string v0, "HAT"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 3312
    invoke-static {p3}, Lsoftware/simplicial/a/as;->a(I)Lsoftware/simplicial/a/as;

    move-result-object v0

    .line 3313
    sget-object v1, Lsoftware/simplicial/a/as;->a:Lsoftware/simplicial/a/as;

    if-eq v0, v1, :cond_1

    .line 3315
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag$a;->v:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 3316
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->f:Lsoftware/simplicial/nebulous/d/d;

    invoke-virtual {v1, v0}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/as;)Z

    goto/16 :goto_0

    .line 3319
    :cond_16
    const-string v0, "PET"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3321
    invoke-static {p3}, Lsoftware/simplicial/a/bd;->a(I)Lsoftware/simplicial/a/bd;

    move-result-object v0

    .line 3322
    sget-object v1, Lsoftware/simplicial/a/bd;->a:Lsoftware/simplicial/a/bd;

    if-eq v0, v1, :cond_1

    .line 3324
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag$a;->w:Ljava/util/Map;

    iget-byte v2, v0, Lsoftware/simplicial/a/bd;->c:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3325
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$30;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->f:Lsoftware/simplicial/nebulous/d/d;

    invoke-virtual {v1, v0}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/bd;)Z

    goto/16 :goto_0
.end method
