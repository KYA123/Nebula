.class Lsoftware/simplicial/nebulous/application/MainActivity$47;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/a/i/e;IZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/a/i/e;

.field final synthetic b:I

.field final synthetic c:Lsoftware/simplicial/nebulous/application/MainActivity;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/a/i/e;I)V
    .locals 0

    .prologue
    .line 3871
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$47;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$47;->a:Lsoftware/simplicial/a/i/e;

    iput p3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$47;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 3875
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$47;->a:Lsoftware/simplicial/a/i/e;

    sget-object v2, Lsoftware/simplicial/a/i/e;->a:Lsoftware/simplicial/a/i/e;

    if-ne v0, v2, :cond_1

    iget v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$47;->b:I

    if-lez v0, :cond_1

    .line 3877
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$47;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->I:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$47;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->z(Lsoftware/simplicial/nebulous/application/MainActivity;)Lsoftware/simplicial/nebulous/application/GameChatFragment;

    move-result-object v0

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/GameChatFragment;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3878
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$47;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->n(Lsoftware/simplicial/nebulous/application/MainActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3879
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$47;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->z(Lsoftware/simplicial/nebulous/application/MainActivity;)Lsoftware/simplicial/nebulous/application/GameChatFragment;

    move-result-object v0

    const/4 v2, -0x1

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$47;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v4, 0x7f080243

    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v1, [B

    sget-object v5, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    sget-object v6, Lsoftware/simplicial/a/n;->b:Lsoftware/simplicial/a/n;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lsoftware/simplicial/nebulous/application/MainActivity$47;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v9, 0x7f0801bc

    .line 3880
    invoke-virtual {v8, v9}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lsoftware/simplicial/nebulous/application/MainActivity$47;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v9, 0x7f08028e

    invoke-virtual {v8, v9}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lsoftware/simplicial/nebulous/application/MainActivity$47;->b:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lsoftware/simplicial/nebulous/application/MainActivity$47;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v9, 0x7f08025c

    invoke-virtual {v8, v9}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 3879
    invoke-virtual/range {v0 .. v7}, Lsoftware/simplicial/nebulous/application/GameChatFragment;->a(IILjava/lang/String;[BLsoftware/simplicial/a/q;Lsoftware/simplicial/a/n;Ljava/lang/String;)V

    .line 3882
    :cond_1
    return-void
.end method
