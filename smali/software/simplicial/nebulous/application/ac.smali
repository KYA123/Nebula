.class public Lsoftware/simplicial/nebulous/application/ac;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lsoftware/simplicial/a/ah;


# static fields
.field public static final a:Ljava/lang/String;

.field public static b:Lsoftware/simplicial/nebulous/f/a;


# instance fields
.field c:Landroid/widget/TextView;

.field d:Landroid/widget/ListView;

.field e:Landroid/widget/Button;

.field f:Landroid/widget/ImageButton;

.field g:Landroid/widget/EditText;

.field h:Landroid/widget/ArrayAdapter;

.field private final i:Ljava/lang/Object;

.field private j:Ljava/util/Timer;

.field private k:Lsoftware/simplicial/nebulous/f/t;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lsoftware/simplicial/nebulous/application/ac;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/ac;->a:Ljava/lang/String;

    .line 31
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/ac;->b:Lsoftware/simplicial/nebulous/f/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    .line 32
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->i:Ljava/lang/Object;

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/ac;)Lsoftware/simplicial/nebulous/f/t;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->k:Lsoftware/simplicial/nebulous/f/t;

    return-object v0
.end method

.method private a()V
    .locals 6

    .prologue
    .line 96
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ac;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 98
    :try_start_0
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ac;->b()V

    .line 100
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->j:Ljava/util/Timer;

    .line 101
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->j:Ljava/util/Timer;

    new-instance v2, Lsoftware/simplicial/nebulous/application/ac$1;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/ac$1;-><init>(Lsoftware/simplicial/nebulous/application/ac;)V

    const-wide/16 v4, 0x3e8

    invoke-virtual {v0, v2, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 124
    monitor-exit v1

    .line 125
    return-void

    .line 124
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 194
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->h:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    const/16 v1, 0x64

    if-lt v0, v1, :cond_0

    .line 195
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->h:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ac;->h:Landroid/widget/ArrayAdapter;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->remove(Ljava/lang/Object;)V

    .line 196
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->h:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, p1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 197
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->h:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 198
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->d:Landroid/widget/ListView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ac;->h:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 199
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/ac;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/application/ac;->a(Ljava/lang/String;)V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 152
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ac;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 154
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->j:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->j:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 157
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->j:Ljava/util/Timer;

    .line 159
    :cond_0
    monitor-exit v1

    .line 160
    return-void

    .line 159
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(IILjava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 166
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ac;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 167
    if-nez v1, :cond_1

    .line 189
    :cond_0
    :goto_0
    return v0

    .line 170
    :cond_1
    iget-object v2, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v2

    if-ne p2, v2, :cond_2

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ac;->k:Lsoftware/simplicial/nebulous/f/t;

    iget v2, v2, Lsoftware/simplicial/nebulous/f/t;->a:I

    if-eq p1, v2, :cond_3

    :cond_2
    iget-object v2, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v2

    if-ne p1, v2, :cond_0

    .line 172
    :cond_3
    new-instance v0, Lsoftware/simplicial/nebulous/application/ac$2;

    invoke-direct {v0, p0, p1, p3}, Lsoftware/simplicial/nebulous/application/ac$2;-><init>(Lsoftware/simplicial/nebulous/application/ac;ILjava/lang/String;)V

    invoke-virtual {v1, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 186
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 75
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->e:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 77
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 92
    :cond_0
    :goto_0
    return-void

    .line 79
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->f:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    .line 81
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->g:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 83
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->g:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 84
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ac;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ac;->k:Lsoftware/simplicial/nebulous/f/t;

    iget v2, v2, Lsoftware/simplicial/nebulous/f/t;->a:I

    invoke-virtual {v1, v2, v0}, Lsoftware/simplicial/a/t;->a(ILjava/lang/String;)V

    .line 85
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ac;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->e:Lsoftware/simplicial/nebulous/f/u;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ac;->k:Lsoftware/simplicial/nebulous/f/t;

    iget v2, v2, Lsoftware/simplicial/nebulous/f/t;->a:I

    invoke-virtual {v1, v2, v0}, Lsoftware/simplicial/nebulous/f/u;->b(ILjava/lang/String;)V

    .line 87
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->g:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 89
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->f:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 90
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ac;->a()V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 45
    const v0, 0x7f040041

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 47
    const v0, 0x7f0d016b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->c:Landroid/widget/TextView;

    .line 48
    const v0, 0x7f0d016c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->d:Landroid/widget/ListView;

    .line 49
    const v0, 0x7f0d00b9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->e:Landroid/widget/Button;

    .line 50
    const v0, 0x7f0d016f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->f:Landroid/widget/ImageButton;

    .line 51
    const v0, 0x7f0d016e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->g:Landroid/widget/EditText;

    .line 53
    return-object v1
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 143
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onPause()V

    .line 145
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/application/MainActivity;->a:Lsoftware/simplicial/a/ah;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->y:Lsoftware/simplicial/a/ah;

    .line 147
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ac;->b()V

    .line 148
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 130
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onResume()V

    .line 132
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->h:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 133
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->k:Lsoftware/simplicial/nebulous/f/t;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/t;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 134
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ac;->h:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 135
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->h:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 137
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iput-object p0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->y:Lsoftware/simplicial/a/ah;

    .line 138
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 59
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 61
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->e:Lsoftware/simplicial/nebulous/f/u;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ac;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "friendID"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/f/u;->b(I)Lsoftware/simplicial/nebulous/f/t;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->k:Lsoftware/simplicial/nebulous/f/t;

    .line 62
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->k:Lsoftware/simplicial/nebulous/f/t;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lsoftware/simplicial/nebulous/f/t;->c:Z

    .line 64
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->e:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->f:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->c:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ac;->k:Lsoftware/simplicial/nebulous/f/t;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/t;->b:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ac;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f080060

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ac;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f040068

    invoke-direct {v0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->h:Landroid/widget/ArrayAdapter;

    .line 69
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ac;->d:Landroid/widget/ListView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ac;->h:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 70
    return-void
.end method
