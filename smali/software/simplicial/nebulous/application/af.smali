.class public Lsoftware/simplicial/nebulous/application/af;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lsoftware/simplicial/a/bo;
.implements Lsoftware/simplicial/a/w;
.implements Lsoftware/simplicial/nebulous/f/al$u;


# static fields
.field public static D:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lsoftware/simplicial/a/bn;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static E:I

.field public static final a:Ljava/lang/String;


# instance fields
.field A:Landroid/widget/Button;

.field B:Landroid/widget/Button;

.field C:Landroid/widget/Button;

.field public b:Landroid/widget/ImageView;

.field public c:Landroid/widget/ImageView;

.field public d:Landroid/widget/ImageView;

.field public e:Landroid/widget/ImageView;

.field public f:Landroid/widget/ImageView;

.field public g:Landroid/widget/ImageView;

.field h:Landroid/widget/Spinner;

.field i:Landroid/widget/Spinner;

.field j:Landroid/widget/ImageButton;

.field k:Landroid/widget/ImageButton;

.field l:Landroid/widget/ImageButton;

.field m:Landroid/widget/ImageButton;

.field n:Landroid/widget/ImageButton;

.field o:Landroid/widget/ImageButton;

.field p:Landroid/widget/LinearLayout;

.field q:Landroid/widget/LinearLayout;

.field r:Landroid/widget/RelativeLayout;

.field s:Landroid/widget/RelativeLayout;

.field t:Landroid/widget/ImageButton;

.field u:Landroid/widget/ImageButton;

.field v:Landroid/widget/ImageButton;

.field w:Lcom/facebook/share/widget/LikeView;

.field x:Landroid/widget/ImageButton;

.field y:Landroid/widget/ImageButton;

.field z:Landroid/widget/CheckBox;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 63
    const-class v0, Lsoftware/simplicial/nebulous/application/af;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/af;->a:Ljava/lang/String;

    .line 92
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lsoftware/simplicial/nebulous/application/af;->D:Ljava/util/Map;

    .line 93
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Random;-><init>(J)V

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    sput v0, Lsoftware/simplicial/nebulous/application/af;->E:I

    .line 97
    sget-object v0, Lsoftware/simplicial/nebulous/application/af;->D:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/bn;->a:Lsoftware/simplicial/a/bn;

    const-string v2, "52.7.125.242"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget-object v0, Lsoftware/simplicial/nebulous/application/af;->D:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/bn;->b:Lsoftware/simplicial/a/bn;

    const-string v2, "52.8.54.152"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    sget-object v0, Lsoftware/simplicial/nebulous/application/af;->D:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/bn;->c:Lsoftware/simplicial/a/bn;

    const-string v2, "52.28.71.221"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    sget-object v0, Lsoftware/simplicial/nebulous/application/af;->D:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/bn;->d:Lsoftware/simplicial/a/bn;

    const-string v2, "52.79.53.242"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    sget-object v0, Lsoftware/simplicial/nebulous/application/af;->D:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/bn;->f:Lsoftware/simplicial/a/bn;

    const-string v2, "54.94.231.111"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    sget-object v0, Lsoftware/simplicial/nebulous/application/af;->D:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/bn;->e:Lsoftware/simplicial/a/bn;

    const-string v2, "54.169.49.111"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    sget-object v0, Lsoftware/simplicial/nebulous/application/af;->D:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/bn;->g:Lsoftware/simplicial/a/bn;

    const-string v2, "52.64.81.137"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/af;)I
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/af;->b()I

    move-result v0

    return v0
.end method

.method private a(Lsoftware/simplicial/a/bj;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 591
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/af;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p1, v1}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/bj;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/af;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v1, v2}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/am;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 595
    :goto_0
    return-object v0

    .line 593
    :catch_0
    move-exception v0

    .line 595
    const-string v0, "Unknown"

    goto :goto_0
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/af;Lsoftware/simplicial/a/bj;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/application/af;->a(Lsoftware/simplicial/a/bj;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Lsoftware/simplicial/a/bn;)V
    .locals 2

    .prologue
    .line 601
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->k:Lsoftware/simplicial/a/bn;

    if-ne p1, v0, :cond_0

    .line 609
    :goto_0
    return-void

    .line 607
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->n:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 608
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-object p1, v0, Lsoftware/simplicial/nebulous/f/ag;->k:Lsoftware/simplicial/a/bn;

    goto :goto_0
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/af;Lsoftware/simplicial/a/bn;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/application/af;->a(Lsoftware/simplicial/a/bn;)V

    return-void
.end method

.method private b()I
    .locals 2

    .prologue
    .line 581
    sget v0, Lsoftware/simplicial/nebulous/application/af;->E:I

    add-int/lit16 v0, v0, 0x6cfc

    .line 582
    sget v1, Lsoftware/simplicial/nebulous/application/af;->E:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lsoftware/simplicial/nebulous/application/af;->E:I

    .line 583
    sget v1, Lsoftware/simplicial/nebulous/application/af;->E:I

    rem-int/lit8 v1, v1, 0x2

    sput v1, Lsoftware/simplicial/nebulous/application/af;->E:I

    .line 584
    return v0
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 746
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 752
    return-void
.end method

.method public a(Ljava/util/List;Ljava/util/Date;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bp;",
            ">;",
            "Ljava/util/Date;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 763
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_1

    .line 783
    :cond_0
    return-void

    .line 766
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bp;

    .line 768
    sget-object v2, Lsoftware/simplicial/nebulous/application/af$3;->a:[I

    invoke-virtual {v0}, Lsoftware/simplicial/a/bp;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 771
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 774
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 777
    :pswitch_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 768
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Lsoftware/simplicial/a/t$a;)V
    .locals 0

    .prologue
    .line 740
    return-void
.end method

.method public a(Lsoftware/simplicial/a/t$a;Lsoftware/simplicial/a/t$a;)V
    .locals 2

    .prologue
    .line 678
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 679
    if-nez v0, :cond_0

    .line 734
    :goto_0
    return-void

    .line 682
    :cond_0
    new-instance v1, Lsoftware/simplicial/nebulous/application/af$2;

    invoke-direct {v1, p0, p1}, Lsoftware/simplicial/nebulous/application/af$2;-><init>(Lsoftware/simplicial/nebulous/application/af;Lsoftware/simplicial/a/t$a;)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 788
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 792
    :goto_0
    return-void

    .line 791
    :cond_0
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/af;->e:Landroid/widget/ImageView;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/16 v0, 0x8

    goto :goto_1
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 758
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 21

    .prologue
    .line 297
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v1}, Lsoftware/simplicial/a/t;->f()Lsoftware/simplicial/a/t$a;

    move-result-object v1

    sget-object v2, Lsoftware/simplicial/a/t$a;->a:Lsoftware/simplicial/a/t$a;

    if-ne v1, v2, :cond_0

    .line 299
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->u:Landroid/widget/ImageButton;

    move-object/from16 v0, p1

    if-ne v0, v1, :cond_1

    .line 301
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->m:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    .line 577
    :cond_0
    :goto_0
    return-void

    .line 303
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->t:Landroid/widget/ImageButton;

    move-object/from16 v0, p1

    if-ne v0, v1, :cond_2

    .line 305
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->Z:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_0

    .line 307
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->v:Landroid/widget/ImageButton;

    move-object/from16 v0, p1

    if-ne v0, v1, :cond_3

    .line 309
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->B:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_0

    .line 311
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->A:Landroid/widget/Button;

    move-object/from16 v0, p1

    if-ne v0, v1, :cond_5

    .line 313
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 314
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->m:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_0

    .line 316
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->Q:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_0

    .line 318
    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->C:Landroid/widget/Button;

    move-object/from16 v0, p1

    if-ne v0, v1, :cond_6

    .line 320
    const/4 v1, 0x1

    sput-boolean v1, Lsoftware/simplicial/nebulous/application/ah;->b:Z

    .line 321
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    sget-object v2, Lsoftware/simplicial/nebulous/application/af;->D:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->k:Lsoftware/simplicial/a/bn;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct/range {p0 .. p0}, Lsoftware/simplicial/nebulous/application/af;->b()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    sget-object v5, Lsoftware/simplicial/a/y;->b:Lsoftware/simplicial/a/y;

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    .line 322
    invoke-static {v7}, Lsoftware/simplicial/a/ai;->c(Lsoftware/simplicial/a/am;)Lsoftware/simplicial/a/ap;

    move-result-object v7

    invoke-static {v6, v7}, Lsoftware/simplicial/a/ai;->b(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/ap;)I

    move-result v6

    const/4 v7, -0x1

    move-object/from16 v0, p0

    iget-object v8, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v8, v8, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v8, v8, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/f/ag;->Y:Lsoftware/simplicial/a/x;

    sget-object v10, Lsoftware/simplicial/a/bj;->c:Lsoftware/simplicial/a/bj;

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lsoftware/simplicial/nebulous/application/af;->a(Lsoftware/simplicial/a/bj;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v11, v11, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v11, v11, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    move-object/from16 v0, p0

    iget-object v12, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v12, v12, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v12, v12, Lsoftware/simplicial/nebulous/f/ag;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v13, v13, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    .line 323
    invoke-virtual {v13}, Lsoftware/simplicial/nebulous/f/ag;->a()[B

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v14, v14, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v14, v14, Lsoftware/simplicial/nebulous/f/ag;->m:Lsoftware/simplicial/a/e;

    move-object/from16 v0, p0

    iget-object v15, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v15, v15, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v15, v15, Lsoftware/simplicial/nebulous/f/ag;->o:Lsoftware/simplicial/a/af;

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lsoftware/simplicial/nebulous/application/MainActivity;->z()I

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->s:Lsoftware/simplicial/a/bd;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-byte v0, v0, Lsoftware/simplicial/a/bd;->c:B

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v18, v0

    .line 324
    invoke-virtual/range {v18 .. v18}, Lsoftware/simplicial/nebulous/f/ag;->b()I

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lsoftware/simplicial/nebulous/f/ag;->d()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->q:Lsoftware/simplicial/a/as;

    move-object/from16 v20, v0

    .line 321
    invoke-virtual/range {v1 .. v20}, Lsoftware/simplicial/a/t;->a(Ljava/lang/String;ILsoftware/simplicial/a/am;Lsoftware/simplicial/a/y;IILjava/lang/String;Lsoftware/simplicial/a/x;Ljava/lang/String;ZLjava/lang/String;[BLsoftware/simplicial/a/e;Lsoftware/simplicial/a/af;IBILjava/lang/String;Lsoftware/simplicial/a/as;)V

    goto/16 :goto_0

    .line 326
    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->B:Landroid/widget/Button;

    move-object/from16 v0, p1

    if-ne v0, v1, :cond_7

    .line 328
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v2, Lsoftware/simplicial/a/d/c;->i:Lsoftware/simplicial/a/d/c;

    iput-object v2, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    .line 329
    const/4 v1, 0x0

    sput-boolean v1, Lsoftware/simplicial/nebulous/application/ah;->b:Z

    .line 330
    sget-object v1, Lsoftware/simplicial/nebulous/application/ah$a;->a:Lsoftware/simplicial/nebulous/application/ah$a;

    sput-object v1, Lsoftware/simplicial/nebulous/application/ah;->c:Lsoftware/simplicial/nebulous/application/ah$a;

    .line 331
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    sget-object v2, Lsoftware/simplicial/nebulous/application/af;->D:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->k:Lsoftware/simplicial/a/bn;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct/range {p0 .. p0}, Lsoftware/simplicial/nebulous/application/af;->b()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    sget-object v5, Lsoftware/simplicial/a/y;->b:Lsoftware/simplicial/a/y;

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    .line 332
    invoke-static {v7}, Lsoftware/simplicial/a/ai;->c(Lsoftware/simplicial/a/am;)Lsoftware/simplicial/a/ap;

    move-result-object v7

    invoke-static {v6, v7}, Lsoftware/simplicial/a/ai;->b(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/ap;)I

    move-result v6

    const/4 v7, -0x1

    move-object/from16 v0, p0

    iget-object v8, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v8, v8, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v8, v8, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/f/ag;->Y:Lsoftware/simplicial/a/x;

    sget-object v10, Lsoftware/simplicial/a/bj;->c:Lsoftware/simplicial/a/bj;

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lsoftware/simplicial/nebulous/application/af;->a(Lsoftware/simplicial/a/bj;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v11, v11, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v11, v11, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    move-object/from16 v0, p0

    iget-object v12, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v12, v12, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v12, v12, Lsoftware/simplicial/nebulous/f/ag;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v13, v13, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    .line 333
    invoke-virtual {v13}, Lsoftware/simplicial/nebulous/f/ag;->a()[B

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v14, v14, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v14, v14, Lsoftware/simplicial/nebulous/f/ag;->m:Lsoftware/simplicial/a/e;

    move-object/from16 v0, p0

    iget-object v15, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v15, v15, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v15, v15, Lsoftware/simplicial/nebulous/f/ag;->o:Lsoftware/simplicial/a/af;

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lsoftware/simplicial/nebulous/application/MainActivity;->z()I

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->s:Lsoftware/simplicial/a/bd;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-byte v0, v0, Lsoftware/simplicial/a/bd;->c:B

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v18, v0

    .line 334
    invoke-virtual/range {v18 .. v18}, Lsoftware/simplicial/nebulous/f/ag;->b()I

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lsoftware/simplicial/nebulous/f/ag;->d()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->q:Lsoftware/simplicial/a/as;

    move-object/from16 v20, v0

    .line 331
    invoke-virtual/range {v1 .. v20}, Lsoftware/simplicial/a/t;->a(Ljava/lang/String;ILsoftware/simplicial/a/am;Lsoftware/simplicial/a/y;IILjava/lang/String;Lsoftware/simplicial/a/x;Ljava/lang/String;ZLjava/lang/String;[BLsoftware/simplicial/a/e;Lsoftware/simplicial/a/af;IBILjava/lang/String;Lsoftware/simplicial/a/as;)V

    goto/16 :goto_0

    .line 336
    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->x:Landroid/widget/ImageButton;

    move-object/from16 v0, p1

    if-ne v0, v1, :cond_9

    .line 338
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    const-string v2, "orborous"

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/f/e;->d(Ljava/lang/String;)V

    .line 340
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "software.simplicial.orborous"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 341
    if-eqz v1, :cond_8

    .line 343
    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/af;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 347
    :cond_8
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 348
    const-string v2, "market://details?id=software.simplicial.orborous"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 349
    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/af;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 352
    :cond_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->y:Landroid/widget/ImageButton;

    move-object/from16 v0, p1

    if-ne v0, v1, :cond_b

    .line 354
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    const-string v2, "rebellious"

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/f/e;->d(Ljava/lang/String;)V

    .line 356
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "software.simplicial.rebellious"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 357
    if-eqz v1, :cond_a

    .line 359
    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/af;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 363
    :cond_a
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 364
    const-string v2, "market://details?id=software.simplicial.rebellious"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 365
    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/af;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 368
    :cond_b
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->j:Landroid/widget/ImageButton;

    move-object/from16 v0, p1

    if-ne v0, v1, :cond_e

    .line 370
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->o()V

    .line 371
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    if-ne v1, v2, :cond_c

    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->ai:Z

    if-nez v1, :cond_c

    .line 373
    new-instance v1, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x1080027

    .line 374
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f080121

    .line 375
    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f0801cf

    .line 376
    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lsoftware/simplicial/nebulous/application/af$6;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lsoftware/simplicial/nebulous/application/af$6;-><init>(Lsoftware/simplicial/nebulous/application/af;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f08005d

    .line 392
    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 395
    new-instance v2, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v2, v3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 396
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 398
    new-instance v3, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 399
    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/nebulous/application/af;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c00fb

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 400
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f080336

    invoke-virtual {v4, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 402
    new-instance v4, Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v4, v5}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    .line 403
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v6, 0x7f0800ef

    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 404
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 405
    new-instance v5, Lsoftware/simplicial/nebulous/application/af$7;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lsoftware/simplicial/nebulous/application/af$7;-><init>(Lsoftware/simplicial/nebulous/application/af;)V

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 417
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 418
    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 420
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 422
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 485
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    const-string v2, "multiplayer"

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/f/e;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 426
    :cond_c
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->P:Z

    if-eqz v1, :cond_d

    .line 428
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    sget-object v2, Lsoftware/simplicial/nebulous/application/af;->D:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->k:Lsoftware/simplicial/a/bn;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct/range {p0 .. p0}, Lsoftware/simplicial/nebulous/application/af;->b()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    sget-object v5, Lsoftware/simplicial/a/y;->a:Lsoftware/simplicial/a/y;

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    .line 429
    invoke-static {v7}, Lsoftware/simplicial/a/ai;->c(Lsoftware/simplicial/a/am;)Lsoftware/simplicial/a/ap;

    move-result-object v7

    invoke-static {v6, v7}, Lsoftware/simplicial/a/ai;->b(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/ap;)I

    move-result v6

    const/4 v7, -0x1

    move-object/from16 v0, p0

    iget-object v8, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v8, v8, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v8, v8, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/f/ag;->Y:Lsoftware/simplicial/a/x;

    sget-object v10, Lsoftware/simplicial/a/bj;->c:Lsoftware/simplicial/a/bj;

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lsoftware/simplicial/nebulous/application/af;->a(Lsoftware/simplicial/a/bj;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v11, v11, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v11, v11, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    move-object/from16 v0, p0

    iget-object v12, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v12, v12, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v12, v12, Lsoftware/simplicial/nebulous/f/ag;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v13, v13, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    .line 430
    invoke-virtual {v13}, Lsoftware/simplicial/nebulous/f/ag;->a()[B

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v14, v14, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v14, v14, Lsoftware/simplicial/nebulous/f/ag;->m:Lsoftware/simplicial/a/e;

    move-object/from16 v0, p0

    iget-object v15, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v15, v15, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v15, v15, Lsoftware/simplicial/nebulous/f/ag;->o:Lsoftware/simplicial/a/af;

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lsoftware/simplicial/nebulous/application/MainActivity;->z()I

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->s:Lsoftware/simplicial/a/bd;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-byte v0, v0, Lsoftware/simplicial/a/bd;->c:B

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v18, v0

    .line 431
    invoke-virtual/range {v18 .. v18}, Lsoftware/simplicial/nebulous/f/ag;->b()I

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lsoftware/simplicial/nebulous/f/ag;->d()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->q:Lsoftware/simplicial/a/as;

    move-object/from16 v20, v0

    .line 428
    invoke-virtual/range {v1 .. v20}, Lsoftware/simplicial/a/t;->a(Ljava/lang/String;ILsoftware/simplicial/a/am;Lsoftware/simplicial/a/y;IILjava/lang/String;Lsoftware/simplicial/a/x;Ljava/lang/String;ZLjava/lang/String;[BLsoftware/simplicial/a/e;Lsoftware/simplicial/a/af;IBILjava/lang/String;Lsoftware/simplicial/a/as;)V

    goto/16 :goto_1

    .line 435
    :cond_d
    new-instance v1, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x1080027

    .line 436
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f0802ee

    .line 437
    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f0801cf

    .line 438
    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lsoftware/simplicial/nebulous/application/af$8;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lsoftware/simplicial/nebulous/application/af$8;-><init>(Lsoftware/simplicial/nebulous/application/af;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f08005d

    .line 452
    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 455
    new-instance v2, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v2, v3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 456
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 458
    new-instance v3, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 459
    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/nebulous/application/af;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c00fb

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 460
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f0800cb

    invoke-virtual {v4, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 462
    new-instance v4, Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v4, v5}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    .line 463
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v6, 0x7f0800ef

    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 464
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 465
    new-instance v5, Lsoftware/simplicial/nebulous/application/af$9;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lsoftware/simplicial/nebulous/application/af$9;-><init>(Lsoftware/simplicial/nebulous/application/af;)V

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 477
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 478
    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 480
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 482
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_1

    .line 487
    :cond_e
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->k:Landroid/widget/ImageButton;

    move-object/from16 v0, p1

    if-ne v0, v1, :cond_10

    .line 489
    const/4 v1, 0x0

    sput-boolean v1, Lsoftware/simplicial/nebulous/application/ah;->b:Z

    .line 491
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->P:Z

    if-eqz v1, :cond_f

    .line 493
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    sget-object v2, Lsoftware/simplicial/nebulous/application/af;->D:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->k:Lsoftware/simplicial/a/bn;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct/range {p0 .. p0}, Lsoftware/simplicial/nebulous/application/af;->b()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    sget-object v5, Lsoftware/simplicial/a/y;->b:Lsoftware/simplicial/a/y;

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    .line 494
    invoke-static {v7}, Lsoftware/simplicial/a/ai;->c(Lsoftware/simplicial/a/am;)Lsoftware/simplicial/a/ap;

    move-result-object v7

    invoke-static {v6, v7}, Lsoftware/simplicial/a/ai;->b(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/ap;)I

    move-result v6

    const/4 v7, -0x1

    move-object/from16 v0, p0

    iget-object v8, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v8, v8, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v8, v8, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/f/ag;->Y:Lsoftware/simplicial/a/x;

    sget-object v10, Lsoftware/simplicial/a/bj;->c:Lsoftware/simplicial/a/bj;

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lsoftware/simplicial/nebulous/application/af;->a(Lsoftware/simplicial/a/bj;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v11, v11, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v11, v11, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    move-object/from16 v0, p0

    iget-object v12, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v12, v12, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v12, v12, Lsoftware/simplicial/nebulous/f/ag;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v13, v13, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    .line 495
    invoke-virtual {v13}, Lsoftware/simplicial/nebulous/f/ag;->a()[B

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v14, v14, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v14, v14, Lsoftware/simplicial/nebulous/f/ag;->m:Lsoftware/simplicial/a/e;

    move-object/from16 v0, p0

    iget-object v15, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v15, v15, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v15, v15, Lsoftware/simplicial/nebulous/f/ag;->o:Lsoftware/simplicial/a/af;

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lsoftware/simplicial/nebulous/application/MainActivity;->z()I

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->s:Lsoftware/simplicial/a/bd;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-byte v0, v0, Lsoftware/simplicial/a/bd;->c:B

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v18, v0

    .line 496
    invoke-virtual/range {v18 .. v18}, Lsoftware/simplicial/nebulous/f/ag;->b()I

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lsoftware/simplicial/nebulous/f/ag;->d()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->q:Lsoftware/simplicial/a/as;

    move-object/from16 v20, v0

    .line 493
    invoke-virtual/range {v1 .. v20}, Lsoftware/simplicial/a/t;->a(Ljava/lang/String;ILsoftware/simplicial/a/am;Lsoftware/simplicial/a/y;IILjava/lang/String;Lsoftware/simplicial/a/x;Ljava/lang/String;ZLjava/lang/String;[BLsoftware/simplicial/a/e;Lsoftware/simplicial/a/af;IBILjava/lang/String;Lsoftware/simplicial/a/as;)V

    .line 549
    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    const-string v2, "find_groups"

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/f/e;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 500
    :cond_f
    new-instance v1, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x1080027

    .line 501
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f0802ee

    .line 502
    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f0801cf

    .line 503
    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lsoftware/simplicial/nebulous/application/af$10;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lsoftware/simplicial/nebulous/application/af$10;-><init>(Lsoftware/simplicial/nebulous/application/af;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f08005d

    .line 517
    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 520
    new-instance v2, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v2, v3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 521
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 523
    new-instance v3, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 524
    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/nebulous/application/af;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c00fb

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 525
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f0800c9

    invoke-virtual {v4, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 527
    new-instance v4, Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v4, v5}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    .line 528
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v6, 0x7f0800ef

    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 529
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 530
    new-instance v5, Lsoftware/simplicial/nebulous/application/af$11;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lsoftware/simplicial/nebulous/application/af$11;-><init>(Lsoftware/simplicial/nebulous/application/af;)V

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 542
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 543
    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 545
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 547
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_2

    .line 551
    :cond_10
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->l:Landroid/widget/ImageButton;

    move-object/from16 v0, p1

    if-ne v0, v1, :cond_11

    .line 553
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    const-string v2, "single_player"

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/f/e;->c(Ljava/lang/String;)V

    .line 555
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->n:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 557
    :cond_11
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->m:Landroid/widget/ImageButton;

    move-object/from16 v0, p1

    if-ne v0, v1, :cond_12

    .line 559
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->z:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 561
    :cond_12
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->n:Landroid/widget/ImageButton;

    move-object/from16 v0, p1

    if-ne v0, v1, :cond_13

    .line 563
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    const-string v2, "reconnect"

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/f/e;->c(Ljava/lang/String;)V

    .line 565
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v1}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v11

    .line 566
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    sget-object v2, Lsoftware/simplicial/nebulous/application/af;->D:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->k:Lsoftware/simplicial/a/bn;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/16 v3, 0x6cfc

    iget-object v4, v11, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v5, Lsoftware/simplicial/a/y;->c:Lsoftware/simplicial/a/y;

    iget v6, v11, Lsoftware/simplicial/a/u;->aY:I

    iget v7, v11, Lsoftware/simplicial/a/u;->aZ:I

    move-object/from16 v0, p0

    iget-object v8, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v8, v8, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v8, v8, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/f/ag;->Y:Lsoftware/simplicial/a/x;

    iget-object v10, v11, Lsoftware/simplicial/a/u;->ba:Ljava/lang/String;

    iget-boolean v11, v11, Lsoftware/simplicial/a/u;->aR:Z

    move-object/from16 v0, p0

    iget-object v12, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v12, v12, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v12, v12, Lsoftware/simplicial/nebulous/f/ag;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v13, v13, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    .line 568
    invoke-virtual {v13}, Lsoftware/simplicial/nebulous/f/ag;->a()[B

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v14, v14, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v14, v14, Lsoftware/simplicial/nebulous/f/ag;->m:Lsoftware/simplicial/a/e;

    move-object/from16 v0, p0

    iget-object v15, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v15, v15, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v15, v15, Lsoftware/simplicial/nebulous/f/ag;->o:Lsoftware/simplicial/a/af;

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v16, v0

    .line 569
    invoke-virtual/range {v16 .. v16}, Lsoftware/simplicial/nebulous/application/MainActivity;->z()I

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->s:Lsoftware/simplicial/a/bd;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-byte v0, v0, Lsoftware/simplicial/a/bd;->c:B

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lsoftware/simplicial/nebulous/f/ag;->b()I

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v19, v0

    .line 570
    invoke-virtual/range {v19 .. v19}, Lsoftware/simplicial/nebulous/f/ag;->d()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->q:Lsoftware/simplicial/a/as;

    move-object/from16 v20, v0

    .line 566
    invoke-virtual/range {v1 .. v20}, Lsoftware/simplicial/a/t;->a(Ljava/lang/String;ILsoftware/simplicial/a/am;Lsoftware/simplicial/a/y;IILjava/lang/String;Lsoftware/simplicial/a/x;Ljava/lang/String;ZLjava/lang/String;[BLsoftware/simplicial/a/e;Lsoftware/simplicial/a/af;IBILjava/lang/String;Lsoftware/simplicial/a/as;)V

    goto/16 :goto_0

    .line 572
    :cond_13
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->o:Landroid/widget/ImageButton;

    move-object/from16 v0, p1

    if-ne v0, v1, :cond_0

    .line 574
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->j:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 263
    invoke-super {p0, p1}, Lsoftware/simplicial/nebulous/application/ao;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 265
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 266
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 267
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/af;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v2, :cond_0

    .line 269
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 270
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->p:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/af;->u:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 271
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->p:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/af;->s:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 272
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->p:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/af;->w:Lcom/facebook/share/widget/LikeView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 274
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 275
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->q:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/af;->y:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 276
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->q:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/af;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 277
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->q:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/af;->r:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 291
    :goto_0
    return-void

    .line 281
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 282
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->p:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/af;->w:Lcom/facebook/share/widget/LikeView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 283
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->p:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/af;->s:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 284
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->p:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/af;->u:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 286
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 287
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->q:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/af;->y:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 288
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->q:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/af;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 289
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->q:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/af;->r:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 115
    const v0, 0x7f040045

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 117
    const v0, 0x7f0d01a0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->h:Landroid/widget/Spinner;

    .line 118
    const v0, 0x7f0d012a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->i:Landroid/widget/Spinner;

    .line 119
    const v0, 0x7f0d01a3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->j:Landroid/widget/ImageButton;

    .line 120
    const v0, 0x7f0d01a2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->k:Landroid/widget/ImageButton;

    .line 121
    const v0, 0x7f0d01a1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->l:Landroid/widget/ImageButton;

    .line 122
    const v0, 0x7f0d0192

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->m:Landroid/widget/ImageButton;

    .line 123
    const v0, 0x7f0d0194

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->n:Landroid/widget/ImageButton;

    .line 124
    const v0, 0x7f0d0193

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->o:Landroid/widget/ImageButton;

    .line 125
    const v0, 0x7f0d009a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->p:Landroid/widget/LinearLayout;

    .line 126
    const v0, 0x7f0d0195

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->q:Landroid/widget/LinearLayout;

    .line 127
    const v0, 0x7f0d018f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->r:Landroid/widget/RelativeLayout;

    .line 128
    const v0, 0x7f0d007c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->s:Landroid/widget/RelativeLayout;

    .line 129
    const v0, 0x7f0d0190

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->v:Landroid/widget/ImageButton;

    .line 130
    const v0, 0x7f0d0199

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->A:Landroid/widget/Button;

    .line 131
    const v0, 0x7f0d019d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->C:Landroid/widget/Button;

    .line 132
    const v0, 0x7f0d019e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->B:Landroid/widget/Button;

    .line 133
    const v0, 0x7f0d018e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->u:Landroid/widget/ImageButton;

    .line 134
    const v0, 0x7f0d007d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->t:Landroid/widget/ImageButton;

    .line 135
    const v0, 0x7f0d0198

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/share/widget/LikeView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->w:Lcom/facebook/share/widget/LikeView;

    .line 136
    const v0, 0x7f0d0197

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->x:Landroid/widget/ImageButton;

    .line 137
    const v0, 0x7f0d0196

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->y:Landroid/widget/ImageButton;

    .line 138
    const v0, 0x7f0d00bb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->z:Landroid/widget/CheckBox;

    .line 139
    const v0, 0x7f0d0191

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->b:Landroid/widget/ImageView;

    .line 140
    const v0, 0x7f0d019c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->c:Landroid/widget/ImageView;

    .line 141
    const v0, 0x7f0d01a4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->d:Landroid/widget/ImageView;

    .line 142
    const v0, 0x7f0d007e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->e:Landroid/widget/ImageView;

    .line 143
    const v0, 0x7f0d019a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->f:Landroid/widget/ImageView;

    .line 144
    const v0, 0x7f0d019b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->g:Landroid/widget/ImageView;

    .line 146
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/af;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/af;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 148
    return-object v1
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 670
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onPause()V

    .line 672
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->b:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 673
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x1

    .line 614
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onResume()V

    .line 620
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->h:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 621
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->i:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 622
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->j:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 623
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->m:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 624
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->n:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 625
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->o:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 626
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->u:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 627
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->t:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 628
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->v:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 629
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->A:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 630
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->C:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 631
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->B:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 632
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->w:Lcom/facebook/share/widget/LikeView;

    invoke-virtual {v0, v1}, Lcom/facebook/share/widget/LikeView;->setEnabled(Z)V

    .line 633
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 634
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->y:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 636
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->b:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 638
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 639
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 640
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 641
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 643
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 644
    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 645
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 646
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 647
    const/16 v2, 0xb

    if-ne v0, v2, :cond_0

    const/16 v0, 0x19

    if-ne v1, v0, :cond_0

    .line 649
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->f:Landroid/widget/ImageView;

    const v1, 0x7f020389

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 650
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->g:Landroid/widget/ImageView;

    const v1, 0x7f020388

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 653
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/a/bo;)V

    .line 654
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->f()V

    .line 656
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->f:Z

    if-eqz v0, :cond_1

    .line 658
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->s:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 659
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al$u;)V

    .line 665
    :goto_0
    return-void

    .line 663
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->s:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const v8, 0x7f04009e

    const/4 v2, 0x0

    .line 154
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 156
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 157
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->h:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 158
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->h:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 164
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->h:Landroid/widget/Spinner;

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v4, v8, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 165
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->h:Landroid/widget/Spinner;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->k:Lsoftware/simplicial/a/bn;

    invoke-virtual {v1}, Lsoftware/simplicial/a/bn;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 166
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->h:Landroid/widget/Spinner;

    new-instance v1, Lsoftware/simplicial/nebulous/application/af$1;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/af$1;-><init>(Lsoftware/simplicial/nebulous/application/af;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 185
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v3, v2

    .line 186
    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->i:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v0

    if-ge v3, v0, :cond_1

    .line 188
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->i:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 189
    const/4 v1, 0x1

    if-ne v3, v1, :cond_4

    .line 191
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    move-object v0, v1

    .line 192
    check-cast v0, Landroid/text/SpannableString;

    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    const/16 v6, 0xff

    const/16 v7, 0xa5

    invoke-static {v6, v7, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v6

    invoke-direct {v5, v6}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v6

    const/16 v7, 0x12

    invoke-virtual {v0, v5, v2, v6, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 194
    :goto_2
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 186
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 196
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->i:Landroid/widget/Spinner;

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v3, v8, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 197
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->i:Landroid/widget/Spinner;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/am;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 198
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->i:Landroid/widget/Spinner;

    new-instance v1, Lsoftware/simplicial/nebulous/application/af$4;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/af$4;-><init>(Lsoftware/simplicial/nebulous/application/af;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 217
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->z:Landroid/widget/CheckBox;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 218
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->z:Landroid/widget/CheckBox;

    new-instance v1, Lsoftware/simplicial/nebulous/application/af$5;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/af$5;-><init>(Lsoftware/simplicial/nebulous/application/af;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 230
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v0

    .line 231
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/af;->n:Landroid/widget/ImageButton;

    iget v3, v0, Lsoftware/simplicial/a/u;->aZ:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    iget v0, v0, Lsoftware/simplicial/a/u;->aZ:I

    if-nez v0, :cond_3

    :cond_2
    const/16 v2, 0x8

    :cond_3
    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 233
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->j:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 234
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->k:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 235
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->l:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 236
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->m:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 237
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->n:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 238
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->o:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 239
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->u:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 240
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->t:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 241
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 242
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->y:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 243
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->v:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 244
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->A:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 245
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->C:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 246
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->B:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 250
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->w:Lcom/facebook/share/widget/LikeView;

    const-string v1, "https://www.facebook.com/NebulousApp"

    sget-object v2, Lcom/facebook/share/widget/LikeView$e;->c:Lcom/facebook/share/widget/LikeView$e;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/share/widget/LikeView;->a(Ljava/lang/String;Lcom/facebook/share/widget/LikeView$e;)V

    .line 251
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->w:Lcom/facebook/share/widget/LikeView;

    sget-object v1, Lcom/facebook/share/widget/LikeView$g;->a:Lcom/facebook/share/widget/LikeView$g;

    invoke-virtual {v0, v1}, Lcom/facebook/share/widget/LikeView;->setLikeViewStyle(Lcom/facebook/share/widget/LikeView$g;)V

    .line 252
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/af;->w:Lcom/facebook/share/widget/LikeView;

    sget-object v1, Lcom/facebook/share/widget/LikeView$a;->c:Lcom/facebook/share/widget/LikeView$a;

    invoke-virtual {v0, v1}, Lcom/facebook/share/widget/LikeView;->setAuxiliaryViewPosition(Lcom/facebook/share/widget/LikeView$a;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 258
    :goto_3
    return-void

    .line 254
    :catch_0
    move-exception v0

    .line 256
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_3

    :cond_4
    move-object v1, v0

    goto/16 :goto_2
.end method
