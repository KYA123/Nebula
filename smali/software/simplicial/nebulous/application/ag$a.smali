.class final enum Lsoftware/simplicial/nebulous/application/ag$a;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsoftware/simplicial/nebulous/application/ag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/nebulous/application/ag$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/nebulous/application/ag$a;

.field public static final enum b:Lsoftware/simplicial/nebulous/application/ag$a;

.field public static final enum c:Lsoftware/simplicial/nebulous/application/ag$a;

.field public static final enum d:Lsoftware/simplicial/nebulous/application/ag$a;

.field public static final enum e:Lsoftware/simplicial/nebulous/application/ag$a;

.field public static final enum f:Lsoftware/simplicial/nebulous/application/ag$a;

.field public static final enum g:Lsoftware/simplicial/nebulous/application/ag$a;

.field private static final synthetic h:[Lsoftware/simplicial/nebulous/application/ag$a;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1632
    new-instance v0, Lsoftware/simplicial/nebulous/application/ag$a;

    const-string v1, "HIGHSCORE"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/nebulous/application/ag$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/application/ag$a;->a:Lsoftware/simplicial/nebulous/application/ag$a;

    new-instance v0, Lsoftware/simplicial/nebulous/application/ag$a;

    const-string v1, "PLAYER_XP"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/nebulous/application/ag$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/application/ag$a;->b:Lsoftware/simplicial/nebulous/application/ag$a;

    new-instance v0, Lsoftware/simplicial/nebulous/application/ag$a;

    const-string v1, "CLAN_XP"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/nebulous/application/ag$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/application/ag$a;->c:Lsoftware/simplicial/nebulous/application/ag$a;

    new-instance v0, Lsoftware/simplicial/nebulous/application/ag$a;

    const-string v1, "CLANWAR"

    invoke-direct {v0, v1, v6}, Lsoftware/simplicial/nebulous/application/ag$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/application/ag$a;->d:Lsoftware/simplicial/nebulous/application/ag$a;

    new-instance v0, Lsoftware/simplicial/nebulous/application/ag$a;

    const-string v1, "ARENA"

    invoke-direct {v0, v1, v7}, Lsoftware/simplicial/nebulous/application/ag$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/application/ag$a;->e:Lsoftware/simplicial/nebulous/application/ag$a;

    new-instance v0, Lsoftware/simplicial/nebulous/application/ag$a;

    const-string v1, "TEAM_ARENA"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/application/ag$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/application/ag$a;->f:Lsoftware/simplicial/nebulous/application/ag$a;

    new-instance v0, Lsoftware/simplicial/nebulous/application/ag$a;

    const-string v1, "TOURNEY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/application/ag$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/application/ag$a;->g:Lsoftware/simplicial/nebulous/application/ag$a;

    .line 1630
    const/4 v0, 0x7

    new-array v0, v0, [Lsoftware/simplicial/nebulous/application/ag$a;

    sget-object v1, Lsoftware/simplicial/nebulous/application/ag$a;->a:Lsoftware/simplicial/nebulous/application/ag$a;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/nebulous/application/ag$a;->b:Lsoftware/simplicial/nebulous/application/ag$a;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/nebulous/application/ag$a;->c:Lsoftware/simplicial/nebulous/application/ag$a;

    aput-object v1, v0, v5

    sget-object v1, Lsoftware/simplicial/nebulous/application/ag$a;->d:Lsoftware/simplicial/nebulous/application/ag$a;

    aput-object v1, v0, v6

    sget-object v1, Lsoftware/simplicial/nebulous/application/ag$a;->e:Lsoftware/simplicial/nebulous/application/ag$a;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lsoftware/simplicial/nebulous/application/ag$a;->f:Lsoftware/simplicial/nebulous/application/ag$a;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lsoftware/simplicial/nebulous/application/ag$a;->g:Lsoftware/simplicial/nebulous/application/ag$a;

    aput-object v2, v0, v1

    sput-object v0, Lsoftware/simplicial/nebulous/application/ag$a;->h:[Lsoftware/simplicial/nebulous/application/ag$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1630
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/nebulous/application/ag$a;
    .locals 1

    .prologue
    .line 1630
    const-class v0, Lsoftware/simplicial/nebulous/application/ag$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/application/ag$a;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/nebulous/application/ag$a;
    .locals 1

    .prologue
    .line 1630
    sget-object v0, Lsoftware/simplicial/nebulous/application/ag$a;->h:[Lsoftware/simplicial/nebulous/application/ag$a;

    invoke-virtual {v0}, [Lsoftware/simplicial/nebulous/application/ag$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/nebulous/application/ag$a;

    return-object v0
.end method
