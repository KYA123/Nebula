.class Lsoftware/simplicial/nebulous/application/ah$20;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/ah;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/widget/EditText;

.field final synthetic b:Lsoftware/simplicial/nebulous/application/ah;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/ah;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 551
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/ah$20;->b:Lsoftware/simplicial/nebulous/application/ah;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/application/ah$20;->a:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    .line 555
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah$20;->b:Lsoftware/simplicial/nebulous/application/ah;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 572
    :goto_0
    return-void

    .line 558
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah$20;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 560
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 562
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah$20;->a:Landroid/widget/EditText;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah$20;->b:Lsoftware/simplicial/nebulous/application/ah;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/ah;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0801a2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 566
    :cond_1
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah$20;->b:Lsoftware/simplicial/nebulous/application/ah;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    sget-object v2, Lsoftware/simplicial/a/d/c;->c:Lsoftware/simplicial/a/d/c;

    if-ne v1, v2, :cond_2

    .line 567
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah$20;->b:Lsoftware/simplicial/nebulous/application/ah;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v1, v0}, Lsoftware/simplicial/a/t;->d(Ljava/lang/String;)V

    .line 570
    :goto_1
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    goto :goto_0

    .line 569
    :cond_2
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah$20;->b:Lsoftware/simplicial/nebulous/application/ah;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah$20;->b:Lsoftware/simplicial/nebulous/application/ah;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->e:Ljava/lang/String;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ah$20;->b:Lsoftware/simplicial/nebulous/application/ah;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    invoke-virtual {v3}, Lsoftware/simplicial/nebulous/f/ag;->a()[B

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v1, v0, v2, v3, v4}, Lsoftware/simplicial/a/t;->a(Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;)V

    goto :goto_1
.end method
