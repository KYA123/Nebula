.class Lsoftware/simplicial/nebulous/application/ai$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/ai;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/application/ai;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/ai;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/ai$1;->a:Lsoftware/simplicial/nebulous/application/ai;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 92
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai$1;->a:Lsoftware/simplicial/nebulous/application/ai;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ai;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_1

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai$1;->a:Lsoftware/simplicial/nebulous/application/ai;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/ai;->a(Lsoftware/simplicial/nebulous/application/ai;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p3, v0, :cond_0

    .line 97
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai$1;->a:Lsoftware/simplicial/nebulous/application/ai;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/ai;->a(Lsoftware/simplicial/nebulous/application/ai;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 98
    const/4 v1, -0x2

    if-ne v0, v1, :cond_2

    .line 100
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai$1;->a:Lsoftware/simplicial/nebulous/application/ai;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ai;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ai$1;->a:Lsoftware/simplicial/nebulous/application/ai;

    const v2, 0x7f0802b5

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/ai;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 101
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 103
    :cond_2
    const/4 v1, -0x1

    if-gt v0, v1, :cond_3

    .line 105
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai$1;->a:Lsoftware/simplicial/nebulous/application/ai;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ai;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ai$1;->a:Lsoftware/simplicial/nebulous/application/ai;

    const v2, 0x7f0802b6

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/ai;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 110
    :cond_3
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ai$1;->a:Lsoftware/simplicial/nebulous/application/ai;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/ai;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iput v0, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->C:I

    .line 111
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai$1;->a:Lsoftware/simplicial/nebulous/application/ai;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ai;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v1, ""

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->D:Ljava/lang/CharSequence;

    .line 112
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai$1;->a:Lsoftware/simplicial/nebulous/application/ai;

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/ai;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai$1;->a:Lsoftware/simplicial/nebulous/application/ai;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ai;->i:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, p3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->E:Ljava/lang/CharSequence;

    .line 113
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai$1;->a:Lsoftware/simplicial/nebulous/application/ai;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ai;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->v:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_0
.end method
