.class Lsoftware/simplicial/nebulous/application/ai$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/ai;->a(Ljava/util/List;Ljava/util/List;Ljava/util/List;F)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Ljava/util/List;

.field final synthetic c:Ljava/util/List;

.field final synthetic d:F

.field final synthetic e:Lsoftware/simplicial/nebulous/application/ai;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/ai;Ljava/util/List;Ljava/util/List;Ljava/util/List;F)V
    .locals 0

    .prologue
    .line 177
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/ai$2;->e:Lsoftware/simplicial/nebulous/application/ai;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/application/ai$2;->a:Ljava/util/List;

    iput-object p3, p0, Lsoftware/simplicial/nebulous/application/ai$2;->b:Ljava/util/List;

    iput-object p4, p0, Lsoftware/simplicial/nebulous/application/ai$2;->c:Ljava/util/List;

    iput p5, p0, Lsoftware/simplicial/nebulous/application/ai$2;->d:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 181
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai$2;->e:Lsoftware/simplicial/nebulous/application/ai;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ai;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_1

    .line 197
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai$2;->e:Lsoftware/simplicial/nebulous/application/ai;

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ai$2;->a:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0, v2}, Lsoftware/simplicial/nebulous/application/ai;->a(Lsoftware/simplicial/nebulous/application/ai;Ljava/util/List;)Ljava/util/List;

    .line 186
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai$2;->e:Lsoftware/simplicial/nebulous/application/ai;

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/ai;->g:Landroid/widget/Button;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai$2;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai$2;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ai$2;->e:Lsoftware/simplicial/nebulous/application/ai;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/ai;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->e:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 188
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai$2;->e:Lsoftware/simplicial/nebulous/application/ai;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ai;->i:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    move v3, v1

    .line 189
    :goto_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai$2;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_3

    .line 190
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai$2;->e:Lsoftware/simplicial/nebulous/application/ai;

    iget-object v4, v0, Lsoftware/simplicial/nebulous/application/ai;->i:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai$2;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ai$2;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    invoke-static {v0, v2}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[B)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 189
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_2
    move v0, v1

    .line 186
    goto :goto_1

    .line 191
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai$2;->e:Lsoftware/simplicial/nebulous/application/ai;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ai;->i:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 192
    iget v0, p0, Lsoftware/simplicial/nebulous/application/ai$2;->d:F

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    .line 194
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai$2;->e:Lsoftware/simplicial/nebulous/application/ai;

    const/4 v2, -0x1

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ai$2;->e:Lsoftware/simplicial/nebulous/application/ai;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/ai;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v4, 0x7f080243

    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v1, [B

    sget-object v5, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    sget-object v6, Lsoftware/simplicial/a/n;->b:Lsoftware/simplicial/a/n;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lsoftware/simplicial/nebulous/application/ai$2;->e:Lsoftware/simplicial/nebulous/application/ai;

    iget-object v8, v8, Lsoftware/simplicial/nebulous/application/ai;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v9, 0x7f08016a

    invoke-virtual {v8, v9}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lsoftware/simplicial/nebulous/application/ai$2;->d:F

    float-to-double v8, v8

    .line 195
    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v8, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "..."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 194
    invoke-virtual/range {v0 .. v7}, Lsoftware/simplicial/nebulous/application/ai;->a(IILjava/lang/String;[BLsoftware/simplicial/a/q;Lsoftware/simplicial/a/n;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
