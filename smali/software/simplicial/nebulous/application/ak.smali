.class public Lsoftware/simplicial/nebulous/application/ak;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lsoftware/simplicial/nebulous/f/al$o;


# static fields
.field public static final a:Ljava/lang/String;

.field public static b:Lsoftware/simplicial/nebulous/f/a;


# instance fields
.field c:Landroid/widget/ListView;

.field d:Landroid/widget/TextView;

.field e:Landroid/widget/Button;

.field f:Landroid/widget/Button;

.field g:Lsoftware/simplicial/nebulous/a/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lsoftware/simplicial/nebulous/application/ak;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/ak;->a:Ljava/lang/String;

    .line 25
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/ak;->b:Lsoftware/simplicial/nebulous/f/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lsoftware/simplicial/nebulous/f/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ak;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 83
    :goto_0
    return-void

    .line 72
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ak;->g:Lsoftware/simplicial/nebulous/a/c;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/c;->clear()V

    .line 73
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/f/g;

    .line 75
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ak;->g:Lsoftware/simplicial/nebulous/a/c;

    invoke-virtual {v2, v0}, Lsoftware/simplicial/nebulous/a/c;->add(Ljava/lang/Object;)V

    goto :goto_1

    .line 77
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ak;->g:Lsoftware/simplicial/nebulous/a/c;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/c;->notifyDataSetChanged()V

    .line 79
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 80
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ak;->d:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 82
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ak;->d:Landroid/widget/TextView;

    const v1, 0x7f0801be

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ak;->f:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 91
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ak;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 93
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ak;->e:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 95
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ak;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->K:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    .line 97
    :cond_1
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 35
    const v0, 0x7f04004b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 37
    const v0, 0x7f0d01fd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ak;->c:Landroid/widget/ListView;

    .line 38
    const v0, 0x7f0d0085

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ak;->d:Landroid/widget/TextView;

    .line 39
    const v0, 0x7f0d011c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ak;->e:Landroid/widget/Button;

    .line 40
    const v0, 0x7f0d00b9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ak;->f:Landroid/widget/Button;

    .line 42
    return-object v1
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 60
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onResume()V

    .line 62
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ak;->d:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 63
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ak;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al$o;)V

    .line 64
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 48
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 50
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ak;->e:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ak;->f:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    new-instance v0, Lsoftware/simplicial/nebulous/a/c;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ak;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1, p0}, Lsoftware/simplicial/nebulous/a/c;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/nebulous/f/al$o;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ak;->g:Lsoftware/simplicial/nebulous/a/c;

    .line 54
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ak;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ak;->g:Lsoftware/simplicial/nebulous/a/c;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 55
    return-void
.end method
