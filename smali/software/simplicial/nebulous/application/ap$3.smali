.class Lsoftware/simplicial/nebulous/application/ap$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/ap;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/application/ap;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/ap;)V
    .locals 0

    .prologue
    .line 459
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/ap$3;->a:Lsoftware/simplicial/nebulous/application/ap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 463
    if-nez p3, :cond_0

    .line 488
    :goto_0
    return-void

    .line 467
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap$3;->a:Lsoftware/simplicial/nebulous/application/ap;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/ap;->a(Lsoftware/simplicial/nebulous/application/ap;)[Ljava/lang/String;

    move-result-object v0

    add-int/lit8 v1, p3, -0x1

    aget-object v0, v0, v1

    .line 469
    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 471
    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 472
    new-instance v0, Ljava/util/Locale;

    aget-object v2, v1, v4

    aget-object v3, v1, v5

    invoke-direct {v0, v2, v3}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap$3;->a:Lsoftware/simplicial/nebulous/application/ap;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    aget-object v3, v1, v4

    iput-object v3, v2, Lsoftware/simplicial/nebulous/f/ag;->aq:Ljava/lang/String;

    .line 474
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap$3;->a:Lsoftware/simplicial/nebulous/application/ap;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    aget-object v1, v1, v5

    iput-object v1, v2, Lsoftware/simplicial/nebulous/f/ag;->ar:Ljava/lang/String;

    .line 484
    :goto_1
    invoke-static {v0}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    .line 485
    new-instance v1, Landroid/content/res/Configuration;

    invoke-direct {v1}, Landroid/content/res/Configuration;-><init>()V

    .line 486
    iput-object v0, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 487
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap$3;->a:Lsoftware/simplicial/nebulous/application/ap;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1, v6}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    goto :goto_0

    .line 478
    :cond_1
    new-instance v0, Ljava/util/Locale;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap$3;->a:Lsoftware/simplicial/nebulous/application/ap;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/application/ap;->a(Lsoftware/simplicial/nebulous/application/ap;)[Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v2, p3, -0x1

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    .line 479
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap$3;->a:Lsoftware/simplicial/nebulous/application/ap;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap$3;->a:Lsoftware/simplicial/nebulous/application/ap;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/ap;->a(Lsoftware/simplicial/nebulous/application/ap;)[Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v3, p3, -0x1

    aget-object v2, v2, v3

    iput-object v2, v1, Lsoftware/simplicial/nebulous/f/ag;->aq:Ljava/lang/String;

    .line 480
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap$3;->a:Lsoftware/simplicial/nebulous/application/ap;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-object v6, v1, Lsoftware/simplicial/nebulous/f/ag;->ar:Ljava/lang/String;

    goto :goto_1
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 493
    return-void
.end method
