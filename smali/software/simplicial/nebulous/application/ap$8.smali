.class Lsoftware/simplicial/nebulous/application/ap$8;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/ap;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/application/ap;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/ap;)V
    .locals 0

    .prologue
    .line 265
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/ap$8;->a:Lsoftware/simplicial/nebulous/application/ap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2

    .prologue
    .line 269
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap$8;->a:Lsoftware/simplicial/nebulous/application/ap;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 274
    :goto_0
    return-void

    .line 272
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap$8;->a:Lsoftware/simplicial/nebulous/application/ap;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    iput v1, v0, Lsoftware/simplicial/nebulous/f/ag;->z:I

    .line 273
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap$8;->a:Lsoftware/simplicial/nebulous/application/ap;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->k:Lsoftware/simplicial/nebulous/c/d;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap$8;->a:Lsoftware/simplicial/nebulous/application/ap;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v1, v1, Lsoftware/simplicial/nebulous/f/ag;->z:I

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/c/d;->b(I)V

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 280
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2

    .prologue
    .line 285
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap$8;->a:Lsoftware/simplicial/nebulous/application/ap;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_1

    .line 292
    :cond_0
    :goto_0
    return-void

    .line 288
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap$8;->a:Lsoftware/simplicial/nebulous/application/ap;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    iput v1, v0, Lsoftware/simplicial/nebulous/f/ag;->z:I

    .line 289
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap$8;->a:Lsoftware/simplicial/nebulous/application/ap;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->k:Lsoftware/simplicial/nebulous/c/d;

    .line 290
    if-eqz v0, :cond_0

    .line 291
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap$8;->a:Lsoftware/simplicial/nebulous/application/ap;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v1, v1, Lsoftware/simplicial/nebulous/f/ag;->z:I

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/c/d;->b(I)V

    goto :goto_0
.end method
