.class Lsoftware/simplicial/nebulous/application/ap$9;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/ap;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/application/ap;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/ap;)V
    .locals 0

    .prologue
    .line 297
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/ap$9;->a:Lsoftware/simplicial/nebulous/application/ap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2

    .prologue
    .line 301
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap$9;->a:Lsoftware/simplicial/nebulous/application/ap;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 306
    :goto_0
    return-void

    .line 304
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap$9;->a:Lsoftware/simplicial/nebulous/application/ap;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    iput v1, v0, Lsoftware/simplicial/nebulous/f/ag;->B:I

    .line 305
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap$9;->a:Lsoftware/simplicial/nebulous/application/ap;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->k:Lsoftware/simplicial/nebulous/c/d;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/c/d;->c()V

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 312
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2

    .prologue
    .line 317
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap$9;->a:Lsoftware/simplicial/nebulous/application/ap;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 322
    :goto_0
    return-void

    .line 320
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap$9;->a:Lsoftware/simplicial/nebulous/application/ap;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    iput v1, v0, Lsoftware/simplicial/nebulous/f/ag;->B:I

    .line 321
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap$9;->a:Lsoftware/simplicial/nebulous/application/ap;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->k:Lsoftware/simplicial/nebulous/c/d;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/c/d;->c()V

    goto :goto_0
.end method
