.class public Lsoftware/simplicial/nebulous/application/ar;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lsoftware/simplicial/a/b;
.implements Lsoftware/simplicial/nebulous/f/al$s;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Lsoftware/simplicial/nebulous/f/al$z;


# static fields
.field public static final a:Ljava/lang/String;

.field public static b:Lsoftware/simplicial/nebulous/f/a;

.field private static final activity:Lsoftware/simplicial/nebulous/application/MainActivity;


# instance fields
.field c:Landroid/widget/TextView;

.field d:Landroid/widget/TextView;

.field e:Landroid/widget/TextView;

.field f:Landroid/widget/TextView;

.field g:Landroid/widget/TextView;

.field h:Landroid/widget/ImageView;

.field i:Landroid/widget/ImageView;

.field j:Landroid/widget/Button;

.field k:Landroid/widget/Button;

.field l:Landroid/widget/Button;

.field m:Landroid/widget/Button;

.field n:Landroid/widget/Button;

.field o:Landroid/widget/Button;

.field p:Landroid/widget/Button;

.field q:Landroid/widget/Button;

.field r:Landroid/widget/Button;

.field s:Landroid/widget/Button;

.field t:Landroid/widget/Button;

.field u:Landroid/widget/Button;

.field v:Landroid/widget/Button;

.field w:Landroid/widget/Button;

.field x:Landroid/widget/TextView;

.field inviteBtn:Landroid/widget/Button;

.field saveSkinBtn:Landroid/widget/Button;

.field private y:I

.field private z:Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lsoftware/simplicial/nebulous/application/ar;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/ar;->a:Ljava/lang/String;

    .line 52
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/ar;->b:Lsoftware/simplicial/nebulous/f/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/ar;)I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lsoftware/simplicial/nebulous/application/ar;->y:I

    return v0
.end method

.method public static getCtx()Lsoftware/simplicial/nebulous/application/MainActivity;
.locals 1
.prologue
    sget-object v0, Lsoftware/simplicial/nebulous/application/ar;->activity:Lsoftware/simplicial/nebulous/application/MainActivity;
    
    return-object v0
.end method

# virtual methods
.method public a(II)V
    .locals 0

    .prologue
    .line 503
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 497
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;[BJLsoftware/simplicial/a/q;)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 363
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->aq:Ljava/util/Set;

    .line 364
    invoke-interface {v4, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v5, v5, Lsoftware/simplicial/nebulous/application/MainActivity;->ar:Ljava/util/Set;

    invoke-interface {v5, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    .line 363
    invoke-static {p2, p3, v4, v5}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[BZZ)Landroid/text/SpannableString;

    move-result-object v4

    iput-object v4, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->D:Ljava/lang/CharSequence;

    .line 365
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 366
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->f:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v6, 0x7f08017a

    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {p4, p5}, Lsoftware/simplicial/a/aw;->a(J)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 367
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ar;->g:Landroid/widget/TextView;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ao:Ljava/util/Set;

    iget v5, p0, Lsoftware/simplicial/nebulous/application/ar;->y:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 369
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->z:Ljava/lang/CharSequence;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->z:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 371
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 372
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->d:Landroid/widget/TextView;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/CharSequence;

    const-string v5, "("

    aput-object v5, v4, v1

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/ar;->z:Ljava/lang/CharSequence;

    aput-object v5, v4, v3

    const/4 v5, 0x2

    const-string v6, ")"

    aput-object v6, v4, v5

    invoke-static {v4}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 379
    :goto_1
    if-eqz p2, :cond_4

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 381
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 382
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->e:Landroid/widget/TextView;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->aq:Ljava/util/Set;

    .line 383
    invoke-interface {v4, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v5, v5, Lsoftware/simplicial/nebulous/application/MainActivity;->ar:Ljava/util/Set;

    invoke-interface {v5, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    .line 382
    invoke-static {p2, p3, v4, v5}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[BZZ)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 384
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->h:Landroid/widget/ImageView;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ar;->i:Landroid/widget/ImageView;

    invoke-static {p6, v0, v4}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/q;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    .line 391
    :goto_2
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ar;->s:Landroid/widget/Button;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->D:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_5

    move v0, v2

    :goto_3
    invoke-virtual {v4, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 392
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ar;->n:Landroid/widget/Button;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->D:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->af:Lsoftware/simplicial/a/q;

    sget-object v5, Lsoftware/simplicial/a/q;->d:Lsoftware/simplicial/a/q;

    if-eq v0, v5, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->af:Lsoftware/simplicial/a/q;

    sget-object v5, Lsoftware/simplicial/a/q;->f:Lsoftware/simplicial/a/q;

    if-eq v0, v5, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->af:Lsoftware/simplicial/a/q;

    sget-object v5, Lsoftware/simplicial/a/q;->c:Lsoftware/simplicial/a/q;

    if-eq v0, v5, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->af:Lsoftware/simplicial/a/q;

    sget-object v5, Lsoftware/simplicial/a/q;->e:Lsoftware/simplicial/a/q;

    if-ne v0, v5, :cond_6

    :cond_0
    move v0, v1

    :goto_4
    invoke-virtual {v4, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 397
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ar;->w:Landroid/widget/Button;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->D:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v5, v5, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v5, v5, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->af:Lsoftware/simplicial/a/q;

    sget-object v5, Lsoftware/simplicial/a/q;->d:Lsoftware/simplicial/a/q;

    if-eq v0, v5, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->af:Lsoftware/simplicial/a/q;

    sget-object v5, Lsoftware/simplicial/a/q;->f:Lsoftware/simplicial/a/q;

    if-eq v0, v5, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->af:Lsoftware/simplicial/a/q;

    sget-object v5, Lsoftware/simplicial/a/q;->c:Lsoftware/simplicial/a/q;

    if-ne v0, v5, :cond_7

    :cond_1
    move v0, v1

    :goto_5
    invoke-virtual {v4, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 400
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ar;->l:Landroid/widget/Button;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-nez v0, :cond_8

    move v0, v2

    :goto_6
    invoke-virtual {v4, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 401
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ar;->m:Landroid/widget/Button;

    iget v0, p0, Lsoftware/simplicial/nebulous/application/ar;->y:I

    const/4 v5, -0x1

    if-ne v0, v5, :cond_9

    move v0, v2

    :goto_7
    invoke-virtual {v4, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 402
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ar;->m:Landroid/widget/Button;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->az:Ljava/util/Set;

    iget v5, p0, Lsoftware/simplicial/nebulous/application/ar;->y:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    move v0, v3

    :goto_8
    invoke-virtual {v4, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 403
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->o:Landroid/widget/Button;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-nez v3, :cond_b

    :goto_9
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 404
    return-void

    :cond_2
    move v0, v2

    .line 367
    goto/16 :goto_0

    .line 376
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 388
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    :cond_5
    move v0, v1

    .line 391
    goto/16 :goto_3

    :cond_6
    move v0, v2

    .line 392
    goto/16 :goto_4

    :cond_7
    move v0, v2

    .line 397
    goto :goto_5

    :cond_8
    move v0, v1

    .line 400
    goto :goto_6

    :cond_9
    move v0, v1

    .line 401
    goto :goto_7

    :cond_a
    move v0, v1

    .line 402
    goto :goto_8

    :cond_b
    move v2, v1

    .line 403
    goto :goto_9
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 509
    return-void
.end method

.method public a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 468
    return-void
.end method

.method public a(Ljava/lang/String;[BLjava/lang/String;ZLsoftware/simplicial/a/q;ILsoftware/simplicial/a/q;Lsoftware/simplicial/a/q;JIJI)V
    .locals 0

    .prologue
    .line 485
    return-void
.end method

.method public a(Ljava/lang/String;[BLsoftware/simplicial/a/q;I)V
    .locals 0

    .prologue
    .line 474
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lsoftware/simplicial/a/bg;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 409
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 449
    :goto_0
    return-void

    :cond_0
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    .line 416
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 418
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bg;

    .line 419
    iget v7, v0, Lsoftware/simplicial/a/bg;->b:I

    iget v8, p0, Lsoftware/simplicial/nebulous/application/ar;->y:I

    if-ne v7, v8, :cond_1

    .line 421
    iget-object v7, v0, Lsoftware/simplicial/a/bg;->h:Lsoftware/simplicial/a/bg$a;

    sget-object v8, Lsoftware/simplicial/a/bg$a;->b:Lsoftware/simplicial/a/bg$a;

    if-ne v7, v8, :cond_2

    move v5, v6

    .line 416
    :cond_1
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 423
    :cond_2
    iget-object v7, v0, Lsoftware/simplicial/a/bg;->h:Lsoftware/simplicial/a/bg$a;

    sget-object v8, Lsoftware/simplicial/a/bg$a;->c:Lsoftware/simplicial/a/bg$a;

    if-ne v7, v8, :cond_3

    move v3, v6

    .line 424
    goto :goto_2

    .line 425
    :cond_3
    iget-object v0, v0, Lsoftware/simplicial/a/bg;->h:Lsoftware/simplicial/a/bg$a;

    sget-object v7, Lsoftware/simplicial/a/bg$a;->d:Lsoftware/simplicial/a/bg$a;

    if-ne v0, v7, :cond_1

    move v4, v6

    .line 426
    goto :goto_2

    .line 430
    :cond_4
    if-eqz v5, :cond_5

    .line 432
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->l:Landroid/widget/Button;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f0800c5

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 433
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->l:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 435
    :cond_5
    if-eqz v4, :cond_6

    .line 437
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->l:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 439
    :cond_6
    if-eqz v3, :cond_7

    .line 441
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->l:Landroid/widget/Button;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f080131

    invoke-virtual {v1, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 442
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->l:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 447
    :cond_7
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->l:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 491
    return-void
.end method

.method public a(Lsoftware/simplicial/a/az;Lsoftware/simplicial/a/r;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;IJZLjava/util/Set;ZLjava/util/Set;Ljava/util/Map;Ljava/util/Set;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsoftware/simplicial/a/az;",
            "Lsoftware/simplicial/a/r;",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/e;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IJZ",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/af;",
            ">;Z",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Lsoftware/simplicial/a/bd;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/as;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 347
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 359
    :goto_0
    return-void

    .line 350
    :cond_0
    if-nez p1, :cond_1

    .line 352
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f080102

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 353
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->f:Landroid/widget/TextView;

    const-string v1, "---"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 357
    :cond_1
    iget-object v2, p2, Lsoftware/simplicial/a/r;->a:Ljava/lang/String;

    iget-object v3, p2, Lsoftware/simplicial/a/r;->b:[B

    iget-wide v4, p1, Lsoftware/simplicial/a/az;->b:J

    iget-object v6, p2, Lsoftware/simplicial/a/r;->c:Lsoftware/simplicial/a/q;

    move-object v0, p0

    move-object v1, p5

    invoke-virtual/range {v0 .. v6}, Lsoftware/simplicial/nebulous/application/ar;->a(Ljava/lang/String;Ljava/lang/String;[BJLsoftware/simplicial/a/q;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 456
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 479
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const v6, 0x7f08005d

    const v3, 0x7f08002f

    const v2, 0x1080027

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 181
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->r:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 183
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 340
    :cond_0
    :goto_0
    return-void

    .line 185
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->p:Landroid/widget/Button;

    if-ne p1, v0, :cond_3

    .line 187
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->z:Ljava/lang/CharSequence;

    .line 188
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ar;->z:Ljava/lang/CharSequence;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ar;->z:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-nez v1, :cond_f

    .line 189
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 191
    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v2, "clipboard"

    invoke-virtual {v0, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 192
    const v2, 0x7f080071

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/ar;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v1

    .line 193
    invoke-virtual {v0, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 195
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f0800d0

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/ar;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 197
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->q:Landroid/widget/Button;

    if-ne p1, v0, :cond_4

    .line 199
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v1, "clipboard"

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 200
    const v1, 0x7f080070

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/ar;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lsoftware/simplicial/nebulous/application/ar;->y:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v1

    .line 201
    invoke-virtual {v0, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 203
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f0800d0

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/ar;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 205
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->l:Landroid/widget/Button;

    if-ne p1, v0, :cond_5

    .line 207
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/ar;->y:I

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/f/al;->a(I)V

    .line 208
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->l:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_0

    .line 210
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->m:Landroid/widget/Button;

    if-ne p1, v0, :cond_6

    .line 212
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 213
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 214
    invoke-virtual {v1, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f080042

    .line 215
    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ar;->c:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f080318

    .line 216
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/ar$2;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/ar$2;-><init>(Lsoftware/simplicial/nebulous/application/ar;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f0801bd

    .line 228
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 230
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->j:Landroid/widget/Button;

    if-ne p1, v0, :cond_7

    .line 232
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 233
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f08005f

    .line 234
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f0801cf

    .line 235
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/ar$3;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/ar$3;-><init>(Lsoftware/simplicial/nebulous/application/ar;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 247
    invoke-virtual {v1, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 249
    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 250
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 252
    new-instance v2, Landroid/widget/CheckBox;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v2, v3}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    .line 253
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v4, 0x7f080187

    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 254
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ar;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c006c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setTextColor(I)V

    .line 255
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v3, v3, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 256
    new-instance v3, Lsoftware/simplicial/nebulous/application/ar$4;

    invoke-direct {v3, p0}, Lsoftware/simplicial/nebulous/application/ar$4;-><init>(Lsoftware/simplicial/nebulous/application/ar;)V

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 268
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 270
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 271
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 273
    :cond_7
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->k:Landroid/widget/Button;

    if-ne p1, v0, :cond_8

    .line 275
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/ar;->y:I

    iput v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->aj:I

    .line 276
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ar;->c:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ak:Ljava/lang/String;

    .line 277
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->aa:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 279
    :cond_8
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->v:Landroid/widget/Button;

    if-ne p1, v0, :cond_9

    .line 281
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->L:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 283
    :cond_9
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->n:Landroid/widget/Button;

    if-ne p1, v0, :cond_a

    .line 285
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/ar;->y:I

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/f/al;->e(I)V

    .line 286
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->n:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 287
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/ar;->y:I

    invoke-virtual {v0, v5, v1, p0}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/a/am;ILsoftware/simplicial/a/b;)V

    goto/16 :goto_0

    .line 289
    :cond_a
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->o:Landroid/widget/Button;

    if-ne p1, v0, :cond_b

    .line 291
    sget-object v0, Lsoftware/simplicial/nebulous/application/ay$a;->b:Lsoftware/simplicial/nebulous/application/ay$a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/ay;->b:Lsoftware/simplicial/nebulous/application/ay$a;

    .line 292
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->I:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 294
    :cond_b
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->w:Landroid/widget/Button;

    if-ne p1, v0, :cond_c

    .line 296
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 297
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 298
    invoke-virtual {v1, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f080235

    .line 299
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f08003d

    .line 300
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/ar$6;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/ar$6;-><init>(Lsoftware/simplicial/nebulous/application/ar;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f080213

    .line 313
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/ar$5;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/ar$5;-><init>(Lsoftware/simplicial/nebulous/application/ar;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 326
    invoke-virtual {v1, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 328
    :cond_c
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->u:Landroid/widget/Button;

    if-ne p1, v0, :cond_d

    .line 330
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->t:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 332
    :cond_d
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->t:Landroid/widget/Button;

    if-ne p1, v0, :cond_e

    .line 334
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->s:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 336
    :cond_e
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->s:Landroid/widget/Button;

    if-ne p1, v0, :invite ## invite

    .line 338
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->y:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0
    
    :invite ## invite
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->inviteBtn:Landroid/widget/Button;

    if-ne p1, v0, :saveskin ## saveSkin
    
    # all the functionality for this button happens in this class
    new-instance v0, Landroid/app/AlertDialog$Builder;
    
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;
    
    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V
    
    const-string v1, "Confirm"
    
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
    
    move-result-object v0
    
    const-string v1, "Inviting someone costs 3 plasma."
    
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
    
    move-result-object v0
    
    const-string v1, "Cancel"
    
    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    
    move-result-object v0
    
    const-string v1, "Ok"
    
    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    
    move-result-object v0
    
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;
    
    goto/16 :goto_0
    
    :saveskin ## saveSkin
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->saveSkinBtn:Landroid/widget/Button;

    if-ne p1, v0, :cond_0
    
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;
    
    sput-object v0, Lsoftware/simplicial/nebulous/application/ar;->activity:Lsoftware/simplicial/nebulous/application/MainActivity;
    
    new-instance v0, Lsoftware/simplicial/nebulous/f/al;
    
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;
    
    iget-object v2, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;
    
    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/f/al;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/nebulous/f/e;)V
    
    iget v1, p0, Lsoftware/simplicial/nebulous/application/ar;->y:I
    
    const/4 v2, 0x0
    
    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/nebulous/f/al;->a(ILsoftware/simplicial/nebulous/f/al$ac;)V
    
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;
    
    const-string v1, "Downloading..."
    
    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;
    
    move-result-object v0
    
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    
    goto/16 :goto_0
    
    :cond_f
    move-object v1, v0

    goto/16 :goto_1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 83
    const v0, 0x7f04004f

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 85
    const v0, 0x7f0d0237

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->c:Landroid/widget/TextView;

    .line 86
    const v0, 0x7f0d0238

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->d:Landroid/widget/TextView;

    .line 87
    const v0, 0x7f0d0239

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->e:Landroid/widget/TextView;

    .line 88
    const v0, 0x7f0d009d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->h:Landroid/widget/ImageView;

    .line 89
    const v0, 0x7f0d009f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->i:Landroid/widget/ImageView;

    .line 90
    const v0, 0x7f0d023a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->f:Landroid/widget/TextView;

    .line 91
    const v0, 0x7f0d023b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->g:Landroid/widget/TextView;

    .line 92
    const v0, 0x7f0d023c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->x:Landroid/widget/TextView;

    .line 93
    const v0, 0x7f0d0240

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->l:Landroid/widget/Button;

    .line 94
    const v0, 0x7f0d00d8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->m:Landroid/widget/Button;

    .line 95
    const v0, 0x7f0d023d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->j:Landroid/widget/Button;

    .line 96
    const v0, 0x7f0d023e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->k:Landroid/widget/Button;

    .line 97
    const v0, 0x7f0d023f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->v:Landroid/widget/Button;

    .line 98
    const v0, 0x7f0d0241

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->n:Landroid/widget/Button;

    .line 99
    const v0, 0x7f0d0242

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->o:Landroid/widget/Button;

    .line 100
    const v0, 0x7f0d0210

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->w:Landroid/widget/Button;

    .line 101
    const v0, 0x7f0d0207

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->p:Landroid/widget/Button;

    .line 102
    const v0, 0x7f0d0208

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->q:Landroid/widget/Button;

    .line 103
    const v0, 0x7f0d00b9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->r:Landroid/widget/Button;

    .line 104
    const v0, 0x7f0d00aa

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->s:Landroid/widget/Button;

    .line 105
    const v0, 0x7f0d00ac

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->t:Landroid/widget/Button;

    .line 106
    const v0, 0x7f0d00ad

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->u:Landroid/widget/Button;
    
    const v0, 0x7f0d0322

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->inviteBtn:Landroid/widget/Button;
    
    const v0, 0x7f0d0323

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->saveSkinBtn:Landroid/widget/Button;

    .line 108
    return-object v1
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 124
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onPause()V

    .line 125
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 114
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onResume()V

    .line 116
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/ak;->a:Lsoftware/simplicial/nebulous/f/ak;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->F:Lsoftware/simplicial/nebulous/f/ak;

    .line 118
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ar;->q:Landroid/widget/Button;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ao:Ljava/util/Set;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ao:Ljava/util/Set;

    iget v2, p0, Lsoftware/simplicial/nebulous/application/ar;->y:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 119
    return-void

    .line 118
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const v4, 0x7f08017d

    const/4 v1, 0x0

    const/4 v5, -0x1

    const/16 v2, 0x8

    .line 130
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 132
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->C:I

    iput v0, p0, Lsoftware/simplicial/nebulous/application/ar;->y:I

    .line 133
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->E:Ljava/lang/CharSequence;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->z:Ljava/lang/CharSequence;

    .line 135
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->t:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->inviteBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->saveSkinBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->u:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 137
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->s:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->r:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->p:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 140
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->q:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->l:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->m:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->j:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->k:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->v:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->n:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 147
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->o:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->w:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 150
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->c:Landroid/widget/TextView;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->f:Landroid/widget/TextView;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 154
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->x:Landroid/widget/TextView;

    new-instance v3, Landroid/text/method/ScrollingMovementMethod;

    invoke-direct {v3}, Landroid/text/method/ScrollingMovementMethod;-><init>()V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 156
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ar;->j:Landroid/widget/Button;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget v0, p0, Lsoftware/simplicial/nebulous/application/ar;->y:I

    if-eq v0, v5, :cond_0

    iget v0, p0, Lsoftware/simplicial/nebulous/application/ar;->y:I

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    .line 157
    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v4

    if-eq v0, v4, :cond_0

    move v0, v1

    .line 156
    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 158
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->k:Landroid/widget/Button;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-eqz v3, :cond_1

    iget v3, p0, Lsoftware/simplicial/nebulous/application/ar;->y:I

    if-eq v3, v5, :cond_1

    iget v3, p0, Lsoftware/simplicial/nebulous/application/ar;->y:I

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    .line 159
    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v4

    if-eq v3, v4, :cond_1

    .line 158
    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 160
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->s:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 161
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->n:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 162
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 163
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 164
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 165
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 166
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->w:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 167
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    const/4 v1, 0x0

    iget v2, p0, Lsoftware/simplicial/nebulous/application/ar;->y:I

    invoke-virtual {v0, v1, v2, p0}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/a/am;ILsoftware/simplicial/a/b;)V

    .line 168
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/ar;->y:I

    new-instance v2, Lsoftware/simplicial/nebulous/application/ar$1;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/ar$1;-><init>(Lsoftware/simplicial/nebulous/application/ar;)V

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/nebulous/f/al;->a(ILsoftware/simplicial/nebulous/f/al$y;)V

    .line 176
    return-void

    :cond_0
    move v0, v2

    .line 157
    goto :goto_0

    :cond_1
    move v1, v2

    .line 159
    goto :goto_1
.end method

.method public p_()V
    .locals 0

    .prologue
    .line 462
    return-void
.end method

## this is strictly for the invite mod, for now
.method public onClick(Landroid/content/DialogInterface;I)V
.locals 5
.prologue
    sget v0, Landroid/content/DialogInterface;->BUTTON_POSITIVE:I
    
    if-eq v0, p2, :btn_neg
    
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;
    
    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;
    
    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;
    
    if-eqz v0, :signed_out
    
    new-instance v0, Lsoftware/simplicial/nebulous/f/al;
    
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;
    
    iget-object v2, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;
    
    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/f/al;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/nebulous/f/e;)V
    
    const-string v1, "/api/account/SendMail"
    
    new-instance v2, Ljava/lang/StringBuilder;
    
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
    
    const-string v3, "Game=Nebulous&Ticket="
    
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    
    move-result-object v2
    
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;
    
    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;
    
    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;
    
    const-string v4, "UTF-8"
    
    invoke-static {v3, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;
    
    move-result-object v3
    
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    
    move-result-object v2
    
    const-string v3, "&ToAID="
    
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    
    move-result-object v2
    
    iget v4, Lsoftware/simplicial/nebulous/ar;->y:I
    
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    
    move-result-object v2
    
    const-string v3, "&ToAllClan=false&Subject="
    
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    
    move-result-object v2
    
    const-string v3, "Nebula - Modded Nebulous!"
    
    const-string v4, "UTF-8"
    
    invoke-static {v3, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;
    
    move-result-object v3
    
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    
    move-result-object v2
    
    const-string v3, "&Message="
    
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    
    move-result-object v2
    
    const-string v3, "" ##! insert download message here, IP's are allowed
    
    const-string v4, "UTF-8"
    
    invoke-static {v3, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;
    
    move-result-object v3
    
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    
    move-result-object v2
    
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    
    move-result-object v2
    
    const/4 v3, 0x0
    
    invoke-virtual {v0, v1, v2, v3, p0}, Lsoftware/simplicial/nebulous/f/al;->a(Ljava/lang/String;Ljava/lang/String;ILsoftware/simplicial/nebulous/f/al$z;)V
    
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;
    
    const-string v1, "Invitation sent. Thank you!"
    
    move v2, v3
    
    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;
    
    move-result-object v0
    
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    
    goto :exit
    
    :signed_out
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ar;->U:Lsoftware/simplicial/nebulous/application/MainActivity;
    
    const-string v1, "You\'re signed out. Sign into an account and try again."
    
    move v2, v3
    
    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;
    
    move-result-object v0
    
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    
    goto :exit
    
    :btn_neg
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V
    
    :exit
    return-void
.end method

.method public a(Lorg/json/JSONObject;)V
.locals 0
.prologue
    return-void
.end method