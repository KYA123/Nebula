.class Lsoftware/simplicial/nebulous/application/as$18;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$s;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/as;->g()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/application/as;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/as;)V
    .locals 0

    .prologue
    .line 892
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/as$18;->a:Lsoftware/simplicial/nebulous/application/as;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lsoftware/simplicial/a/bg;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 896
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as$18;->a:Lsoftware/simplicial/nebulous/application/as;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-eqz v0, :cond_0

    sget-object v0, Lsoftware/simplicial/nebulous/application/as;->d:Lsoftware/simplicial/nebulous/application/as$b;

    sget-object v1, Lsoftware/simplicial/nebulous/application/as$b;->b:Lsoftware/simplicial/nebulous/application/as$b;

    if-eq v0, v1, :cond_1

    .line 912
    :cond_0
    :goto_0
    return-void

    .line 899
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as$18;->a:Lsoftware/simplicial/nebulous/application/as;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/as;->C:Lsoftware/simplicial/nebulous/a/s;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/s;->clear()V

    .line 900
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as$18;->a:Lsoftware/simplicial/nebulous/application/as;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/as;->C:Lsoftware/simplicial/nebulous/a/s;

    invoke-virtual {v0, p1}, Lsoftware/simplicial/nebulous/a/s;->addAll(Ljava/util/Collection;)V

    .line 901
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as$18;->a:Lsoftware/simplicial/nebulous/application/as;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/as;->C:Lsoftware/simplicial/nebulous/a/s;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/s;->notifyDataSetChanged()V

    .line 903
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 904
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bg;

    .line 905
    iget-object v3, v0, Lsoftware/simplicial/a/bg;->h:Lsoftware/simplicial/a/bg$a;

    sget-object v4, Lsoftware/simplicial/a/bg$a;->d:Lsoftware/simplicial/a/bg$a;

    if-ne v3, v4, :cond_2

    .line 906
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 908
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as$18;->a:Lsoftware/simplicial/nebulous/application/as;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/as;->t:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 909
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 910
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as$18;->a:Lsoftware/simplicial/nebulous/application/as;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/t;->a(Ljava/util/List;)V

    .line 911
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as$18;->a:Lsoftware/simplicial/nebulous/application/as;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/as;->u:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/as$18;->a:Lsoftware/simplicial/nebulous/application/as;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/as;->b(Lsoftware/simplicial/nebulous/application/as;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/as$18;->a:Lsoftware/simplicial/nebulous/application/as;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/as;->c(Lsoftware/simplicial/nebulous/application/as;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
