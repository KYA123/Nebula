.class public final enum Lsoftware/simplicial/nebulous/application/as$b;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsoftware/simplicial/nebulous/application/as;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/nebulous/application/as$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/nebulous/application/as$b;

.field public static final enum b:Lsoftware/simplicial/nebulous/application/as$b;

.field public static final enum c:Lsoftware/simplicial/nebulous/application/as$b;

.field public static final enum d:Lsoftware/simplicial/nebulous/application/as$b;

.field public static final enum e:Lsoftware/simplicial/nebulous/application/as$b;

.field public static final enum f:Lsoftware/simplicial/nebulous/application/as$b;

.field private static final synthetic g:[Lsoftware/simplicial/nebulous/application/as$b;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1353
    new-instance v0, Lsoftware/simplicial/nebulous/application/as$b;

    const-string v1, "PLAYERS"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/nebulous/application/as$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/application/as$b;->a:Lsoftware/simplicial/nebulous/application/as$b;

    new-instance v0, Lsoftware/simplicial/nebulous/application/as$b;

    const-string v1, "FRIENDS"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/nebulous/application/as$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/application/as$b;->b:Lsoftware/simplicial/nebulous/application/as$b;

    new-instance v0, Lsoftware/simplicial/nebulous/application/as$b;

    const-string v1, "CLAN_INVITES"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/nebulous/application/as$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/application/as$b;->c:Lsoftware/simplicial/nebulous/application/as$b;

    new-instance v0, Lsoftware/simplicial/nebulous/application/as$b;

    const-string v1, "CLAN_MEMBERS"

    invoke-direct {v0, v1, v6}, Lsoftware/simplicial/nebulous/application/as$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/application/as$b;->d:Lsoftware/simplicial/nebulous/application/as$b;

    new-instance v0, Lsoftware/simplicial/nebulous/application/as$b;

    const-string v1, "CLAN_WAR"

    invoke-direct {v0, v1, v7}, Lsoftware/simplicial/nebulous/application/as$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/application/as$b;->e:Lsoftware/simplicial/nebulous/application/as$b;

    new-instance v0, Lsoftware/simplicial/nebulous/application/as$b;

    const-string v1, "ARENA"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/application/as$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/application/as$b;->f:Lsoftware/simplicial/nebulous/application/as$b;

    .line 1351
    const/4 v0, 0x6

    new-array v0, v0, [Lsoftware/simplicial/nebulous/application/as$b;

    sget-object v1, Lsoftware/simplicial/nebulous/application/as$b;->a:Lsoftware/simplicial/nebulous/application/as$b;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/nebulous/application/as$b;->b:Lsoftware/simplicial/nebulous/application/as$b;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/nebulous/application/as$b;->c:Lsoftware/simplicial/nebulous/application/as$b;

    aput-object v1, v0, v5

    sget-object v1, Lsoftware/simplicial/nebulous/application/as$b;->d:Lsoftware/simplicial/nebulous/application/as$b;

    aput-object v1, v0, v6

    sget-object v1, Lsoftware/simplicial/nebulous/application/as$b;->e:Lsoftware/simplicial/nebulous/application/as$b;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lsoftware/simplicial/nebulous/application/as$b;->f:Lsoftware/simplicial/nebulous/application/as$b;

    aput-object v2, v0, v1

    sput-object v0, Lsoftware/simplicial/nebulous/application/as$b;->g:[Lsoftware/simplicial/nebulous/application/as$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1351
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/nebulous/application/as$b;
    .locals 1

    .prologue
    .line 1351
    const-class v0, Lsoftware/simplicial/nebulous/application/as$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/application/as$b;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/nebulous/application/as$b;
    .locals 1

    .prologue
    .line 1351
    sget-object v0, Lsoftware/simplicial/nebulous/application/as$b;->g:[Lsoftware/simplicial/nebulous/application/as$b;

    invoke-virtual {v0}, [Lsoftware/simplicial/nebulous/application/as$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/nebulous/application/as$b;

    return-object v0
.end method
