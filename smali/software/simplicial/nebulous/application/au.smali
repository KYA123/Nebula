.class public Lsoftware/simplicial/nebulous/application/au;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lsoftware/simplicial/a/w;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field b:Landroid/widget/EditText;

.field c:Landroid/widget/Spinner;

.field d:Landroid/widget/Spinner;

.field e:Landroid/widget/Button;

.field f:Landroid/widget/Button;

.field g:Landroid/widget/CheckBox;

.field h:Landroid/widget/CheckBox;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lsoftware/simplicial/nebulous/application/au;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/au;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/au;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/au;->b()V

    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    .line 126
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->d:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 128
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    invoke-static {v0}, Lsoftware/simplicial/a/ai;->a(Lsoftware/simplicial/a/am;)I

    move-result v1

    .line 130
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v0, v0, Lsoftware/simplicial/nebulous/f/ag;->F:I

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput v1, v0, Lsoftware/simplicial/nebulous/f/ag;->F:I

    .line 132
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 133
    const/4 v0, 0x2

    :goto_0
    if-gt v0, v1, :cond_1

    .line 134
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 133
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 136
    :cond_1
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/au;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f04009e

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v0, v1, v3, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 137
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/au;->d:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 138
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->d:Landroid/widget/Spinner;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/au;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v1, v1, Lsoftware/simplicial/nebulous/f/ag;->F:I

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 140
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->d:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 141
    return-void
.end method

.method private c()Z
    .locals 4

    .prologue
    const v3, 0x7f0801ad

    const/4 v0, 0x0

    .line 172
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/au;->b:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 173
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/au;->b:Landroid/widget/EditText;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/au;->b:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lsoftware/simplicial/a/ba;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 174
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/au;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 175
    invoke-static {v1}, Lsoftware/simplicial/a/ba;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 177
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/au;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/au;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 178
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/au;->b:Landroid/widget/EditText;

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/au;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 182
    :goto_0
    return v0

    .line 181
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag;->J:Ljava/lang/String;

    .line 182
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 263
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 269
    return-void
.end method

.method public a(Lsoftware/simplicial/a/t$a;)V
    .locals 0

    .prologue
    .line 257
    return-void
.end method

.method public a(Lsoftware/simplicial/a/t$a;Lsoftware/simplicial/a/t$a;)V
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 228
    if-nez v0, :cond_0

    .line 251
    :goto_0
    return-void

    .line 231
    :cond_0
    new-instance v1, Lsoftware/simplicial/nebulous/application/au$3;

    invoke-direct {v1, p0, p1}, Lsoftware/simplicial/nebulous/application/au$3;-><init>(Lsoftware/simplicial/nebulous/application/au;Lsoftware/simplicial/a/t$a;)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 275
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 18

    .prologue
    .line 146
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/au;->e:Landroid/widget/Button;

    move-object/from16 v0, p1

    if-ne v0, v1, :cond_2

    .line 148
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/au;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    if-ne v1, v2, :cond_1

    .line 150
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/au;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f0802ee

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lsoftware/simplicial/nebulous/application/au;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f080320

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lsoftware/simplicial/nebulous/application/au;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f080120

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lsoftware/simplicial/nebulous/application/au;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0801cf

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lsoftware/simplicial/nebulous/application/au;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 168
    :cond_0
    :goto_0
    return-void

    .line 155
    :cond_1
    invoke-direct/range {p0 .. p0}, Lsoftware/simplicial/nebulous/application/au;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 158
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/au;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    invoke-static {v1}, Lsoftware/simplicial/a/ai;->c(Lsoftware/simplicial/a/am;)Lsoftware/simplicial/a/ap;

    move-result-object v12

    .line 160
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/au;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/au;->b:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/application/au;->g:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/application/au;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v4, v4, Lsoftware/simplicial/nebulous/f/ag;->F:I

    const/16 v5, 0xa

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/nebulous/application/au;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/f/ag;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/nebulous/application/au;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    invoke-virtual {v7}, Lsoftware/simplicial/nebulous/f/ag;->a()[B

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lsoftware/simplicial/nebulous/application/au;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v8, v8, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v8, v8, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/nebulous/application/au;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/f/ag;->C:Lsoftware/simplicial/a/ac;

    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/nebulous/application/au;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v10, v10, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/nebulous/application/au;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v11, v11, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v11, v11, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    const/4 v13, 0x0

    .line 162
    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/nebulous/application/au;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    .line 161
    invoke-static {v11, v13, v14}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/am;ZLandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v11

    const v13, 0x417a6666    # 15.65f

    .line 162
    invoke-static {v12}, Lsoftware/simplicial/a/ai;->c(Lsoftware/simplicial/a/ap;)S

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lsoftware/simplicial/nebulous/application/au;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v15, v15, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-short v15, v15, Lsoftware/simplicial/nebulous/f/ag;->ag:S

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/au;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    move/from16 v16, v0

    const/16 v17, 0x0

    .line 160
    invoke-virtual/range {v1 .. v17}, Lsoftware/simplicial/a/t;->a(Ljava/lang/String;ZIILjava/lang/String;[BLsoftware/simplicial/a/am;Lsoftware/simplicial/a/ac;Lsoftware/simplicial/a/d/c;Ljava/lang/String;Lsoftware/simplicial/a/ap;FSSZ[Z)V

    .line 164
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/au;->f:Landroid/widget/Button;

    move-object/from16 v0, p1

    if-ne v0, v1, :cond_0

    .line 166
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/au;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    goto/16 :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 52
    const v0, 0x7f040053

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 54
    const v0, 0x7f0d0118

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->b:Landroid/widget/EditText;

    .line 55
    const v0, 0x7f0d012a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->c:Landroid/widget/Spinner;

    .line 56
    const v0, 0x7f0d0119

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->d:Landroid/widget/Spinner;

    .line 57
    const v0, 0x7f0d011c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->e:Landroid/widget/Button;

    .line 58
    const v0, 0x7f0d00a5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->f:Landroid/widget/Button;

    .line 59
    const v0, 0x7f0d011b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->g:Landroid/widget/CheckBox;

    .line 60
    const v0, 0x7f0d00bb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->h:Landroid/widget/CheckBox;

    .line 62
    return-object v1
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 188
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->d:Landroid/widget/Spinner;

    if-ne p1, v0, :cond_0

    .line 190
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v0, v0, Lsoftware/simplicial/nebulous/f/ag;->F:I

    add-int/lit8 v1, p3, 0x2

    if-ne v0, v1, :cond_1

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 192
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    add-int/lit8 v1, p3, 0x2

    iput v1, v0, Lsoftware/simplicial/nebulous/f/ag;->F:I

    .line 193
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/au;->b()V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 201
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 218
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onPause()V

    .line 220
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->b:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 221
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 207
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onResume()V

    .line 209
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->e:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 210
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->f:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 212
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->b:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 213
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 68
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 70
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->b:Landroid/widget/EditText;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/au;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->J:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 72
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v2, v3

    .line 73
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->c:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 75
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->c:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 76
    const/4 v1, 0x1

    if-ne v2, v1, :cond_1

    .line 78
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    move-object v0, v1

    .line 79
    check-cast v0, Landroid/text/SpannableString;

    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    const/16 v6, 0xff

    const/16 v7, 0xa5

    invoke-static {v6, v7, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v6

    invoke-direct {v5, v6}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v6

    const/16 v7, 0x12

    invoke-virtual {v0, v5, v3, v6, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 81
    :goto_1
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 83
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->c:Landroid/widget/Spinner;

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/au;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f04009e

    invoke-direct {v1, v2, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 84
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->c:Landroid/widget/Spinner;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/au;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/am;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 85
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->c:Landroid/widget/Spinner;

    new-instance v1, Lsoftware/simplicial/nebulous/application/au$1;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/au$1;-><init>(Lsoftware/simplicial/nebulous/application/au;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 104
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->h:Landroid/widget/CheckBox;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/au;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 105
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->h:Landroid/widget/CheckBox;

    new-instance v1, Lsoftware/simplicial/nebulous/application/au$2;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/au$2;-><init>(Lsoftware/simplicial/nebulous/application/au;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 118
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->e:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/au;->f:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/au;->b()V

    .line 122
    return-void

    :cond_1
    move-object v1, v0

    goto :goto_1
.end method
