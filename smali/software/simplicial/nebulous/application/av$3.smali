.class Lsoftware/simplicial/nebulous/application/av$3;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/av;->d()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lsoftware/simplicial/a/bm;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/application/av;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/av;)V
    .locals 0

    .prologue
    .line 192
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/av$3;->a:Lsoftware/simplicial/nebulous/application/av;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 196
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av$3;->a:Lsoftware/simplicial/nebulous/application/av;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/av;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 198
    if-nez v0, :cond_0

    .line 199
    const/4 v0, 0x0

    .line 201
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->l:Lsoftware/simplicial/nebulous/f/v;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/v;->a()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bm;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 207
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av$3;->a:Lsoftware/simplicial/nebulous/application/av;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/av;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 232
    :goto_0
    return-void

    .line 210
    :cond_0
    if-eqz p1, :cond_1

    .line 212
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av$3;->a:Lsoftware/simplicial/nebulous/application/av;

    invoke-static {v0, v1}, Lsoftware/simplicial/nebulous/application/av;->b(Lsoftware/simplicial/nebulous/application/av;Z)Z

    .line 216
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bm;

    .line 218
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/av$3;->a:Lsoftware/simplicial/nebulous/application/av;

    invoke-static {v2, v0}, Lsoftware/simplicial/nebulous/application/av;->a(Lsoftware/simplicial/nebulous/application/av;Lsoftware/simplicial/a/bm;)Lsoftware/simplicial/a/bm;

    move-result-object v2

    .line 220
    iget-object v3, v0, Lsoftware/simplicial/a/bm;->b:Ljava/lang/String;

    iput-object v3, v2, Lsoftware/simplicial/a/bm;->b:Ljava/lang/String;

    .line 221
    iget-object v3, v0, Lsoftware/simplicial/a/bm;->d:Ljava/lang/String;

    iput-object v3, v2, Lsoftware/simplicial/a/bm;->d:Ljava/lang/String;

    .line 222
    iget-object v3, v0, Lsoftware/simplicial/a/bm;->c:Ljava/lang/String;

    iput-object v3, v2, Lsoftware/simplicial/a/bm;->c:Ljava/lang/String;

    .line 223
    iget v0, v0, Lsoftware/simplicial/a/bm;->g:I

    iput v0, v2, Lsoftware/simplicial/a/bm;->g:I

    goto :goto_1

    .line 228
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av$3;->a:Lsoftware/simplicial/nebulous/application/av;

    invoke-static {v0, v1}, Lsoftware/simplicial/nebulous/application/av;->c(Lsoftware/simplicial/nebulous/application/av;Z)Z

    .line 231
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av$3;->a:Lsoftware/simplicial/nebulous/application/av;

    sget-object v1, Lsoftware/simplicial/nebulous/application/bc$a;->a:Lsoftware/simplicial/nebulous/application/bc$a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/av;->a(Lsoftware/simplicial/nebulous/application/bc$a;)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 192
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/application/av$3;->a([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 192
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/application/av$3;->a(Ljava/util/List;)V

    return-void
.end method
