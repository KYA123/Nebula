.class public Lsoftware/simplicial/nebulous/application/av;
.super Lsoftware/simplicial/nebulous/application/bc;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lsoftware/simplicial/a/b;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/widget/Button;

.field private c:Landroid/widget/ListView;

.field private d:Landroid/widget/ListView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Lsoftware/simplicial/nebulous/a/z;

.field private h:Lsoftware/simplicial/nebulous/a/q;

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bm;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/be;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lsoftware/simplicial/nebulous/application/av;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/av;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bc;-><init>()V

    .line 54
    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/av;->i:Z

    .line 55
    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/av;->j:Z

    .line 56
    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/av;->k:Z

    .line 57
    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/av;->l:Z

    .line 58
    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/av;->m:Z

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->n:Ljava/util/List;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->o:Ljava/util/List;

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/av;)Ljava/util/List;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->o:Ljava/util/List;

    return-object v0
.end method

.method private a(Lsoftware/simplicial/a/bm;)Lsoftware/simplicial/a/bm;
    .locals 4

    .prologue
    .line 161
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bm;

    .line 162
    iget-object v2, v0, Lsoftware/simplicial/a/bm;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lsoftware/simplicial/a/bm;->b:Ljava/lang/String;

    iget-object v3, p1, Lsoftware/simplicial/a/bm;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 166
    :goto_0
    return-object v0

    .line 165
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->n:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, p1

    .line 166
    goto :goto_0
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/av;Lsoftware/simplicial/a/bm;)Lsoftware/simplicial/a/bm;
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/application/av;->a(Lsoftware/simplicial/a/bm;)Lsoftware/simplicial/a/bm;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/av;Z)Z
    .locals 0

    .prologue
    .line 42
    iput-boolean p1, p0, Lsoftware/simplicial/nebulous/application/av;->k:Z

    return p1
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/application/av;)Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/av;->i:Z

    return v0
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/application/av;Z)Z
    .locals 0

    .prologue
    .line 42
    iput-boolean p1, p0, Lsoftware/simplicial/nebulous/application/av;->j:Z

    return p1
.end method

.method private c()V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/al;->f()V

    .line 173
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    new-instance v1, Lsoftware/simplicial/nebulous/application/av$2;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/av$2;-><init>(Lsoftware/simplicial/nebulous/application/av;)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al$m;)V

    .line 186
    return-void
.end method

.method static synthetic c(Lsoftware/simplicial/nebulous/application/av;Z)Z
    .locals 0

    .prologue
    .line 42
    iput-boolean p1, p0, Lsoftware/simplicial/nebulous/application/av;->m:Z

    return p1
.end method

.method private d()V
    .locals 2

    .prologue
    .line 191
    new-instance v0, Lsoftware/simplicial/nebulous/application/av$3;

    invoke-direct {v0, p0}, Lsoftware/simplicial/nebulous/application/av$3;-><init>(Lsoftware/simplicial/nebulous/application/av;)V

    .line 235
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 236
    return-void
.end method

.method static synthetic d(Lsoftware/simplicial/nebulous/application/av;Z)Z
    .locals 0

    .prologue
    .line 42
    iput-boolean p1, p0, Lsoftware/simplicial/nebulous/application/av;->l:Z

    return p1
.end method


# virtual methods
.method public a(II)V
    .locals 0

    .prologue
    .line 365
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 359
    :goto_0
    return-void

    .line 358
    :cond_0
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/av;->c()V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 371
    return-void
.end method

.method public a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 2

    .prologue
    .line 286
    sget-object v0, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-static {v0, p1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 288
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 289
    if-nez v0, :cond_0

    .line 307
    :goto_0
    return-void

    .line 292
    :cond_0
    new-instance v1, Lsoftware/simplicial/nebulous/application/av$4;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/av$4;-><init>(Lsoftware/simplicial/nebulous/application/av;)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;[BLjava/lang/String;ZLsoftware/simplicial/a/q;ILsoftware/simplicial/a/q;Lsoftware/simplicial/a/q;JIJI)V
    .locals 0

    .prologue
    .line 325
    return-void
.end method

.method public a(Ljava/lang/String;[BLsoftware/simplicial/a/q;I)V
    .locals 0

    .prologue
    .line 313
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bm;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 330
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 350
    :goto_0
    return-void

    .line 333
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/av;->l:Z

    .line 337
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bm;

    .line 339
    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/av;->a(Lsoftware/simplicial/a/bm;)Lsoftware/simplicial/a/bm;

    move-result-object v2

    .line 341
    iget-object v3, v0, Lsoftware/simplicial/a/bm;->b:Ljava/lang/String;

    iput-object v3, v2, Lsoftware/simplicial/a/bm;->b:Ljava/lang/String;

    .line 342
    iput-boolean v4, v2, Lsoftware/simplicial/a/bm;->e:Z

    .line 343
    iget-object v3, v0, Lsoftware/simplicial/a/bm;->f:Ljava/util/Date;

    iput-object v3, v2, Lsoftware/simplicial/a/bm;->f:Ljava/util/Date;

    .line 344
    iget v0, v0, Lsoftware/simplicial/a/bm;->g:I

    iput v0, v2, Lsoftware/simplicial/a/bm;->g:I

    goto :goto_1

    .line 347
    :cond_1
    iput-boolean v4, p0, Lsoftware/simplicial/nebulous/application/av;->i:Z

    .line 349
    sget-object v0, Lsoftware/simplicial/nebulous/application/bc$a;->a:Lsoftware/simplicial/nebulous/application/bc$a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/av;->a(Lsoftware/simplicial/nebulous/application/bc$a;)V

    goto :goto_0
.end method

.method public a(Lsoftware/simplicial/a/az;Lsoftware/simplicial/a/r;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;IJZLjava/util/Set;ZLjava/util/Set;Ljava/util/Map;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsoftware/simplicial/a/az;",
            "Lsoftware/simplicial/a/r;",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/e;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IJZ",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/af;",
            ">;Z",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Lsoftware/simplicial/a/bd;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/as;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 275
    return-void
.end method

.method public a(Lsoftware/simplicial/nebulous/application/bc$a;)V
    .locals 3

    .prologue
    const v2, 0x7f08020c

    .line 98
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->g:Lsoftware/simplicial/nebulous/a/z;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/z;->clear()V

    .line 99
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->h:Lsoftware/simplicial/nebulous/a/q;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/q;->clear()V

    .line 100
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->g:Lsoftware/simplicial/nebulous/a/z;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/z;->notifyDataSetChanged()V

    .line 101
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->h:Lsoftware/simplicial/nebulous/a/q;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/q;->notifyDataSetChanged()V

    .line 103
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/av;->l:Z

    if-eqz v0, :cond_1

    .line 105
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->e:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/av;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " (Code 1)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 157
    :cond_0
    :goto_0
    return-void

    .line 108
    :cond_1
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/av;->m:Z

    if-eqz v0, :cond_2

    .line 110
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->e:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/av;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " (Code 2)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 114
    :cond_2
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/av;->i:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/av;->j:Z

    if-eqz v0, :cond_3

    .line 116
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 117
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->e:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 123
    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->e:Landroid/widget/TextView;

    const v1, 0x7f080156

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/av;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->n:Ljava/util/List;

    new-instance v1, Lsoftware/simplicial/nebulous/application/av$1;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/av$1;-><init>(Lsoftware/simplicial/nebulous/application/av;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 147
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->g:Lsoftware/simplicial/nebulous/a/z;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/av;->n:Ljava/util/List;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/a/z;->addAll(Ljava/util/Collection;)V

    .line 148
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->g:Lsoftware/simplicial/nebulous/a/z;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/z;->notifyDataSetChanged()V

    .line 151
    :cond_3
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/av;->k:Z

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->f:Landroid/widget/TextView;

    const v1, 0x7f0801eb

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/av;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 154
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->h:Lsoftware/simplicial/nebulous/a/q;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/av;->o:Ljava/util/List;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/a/q;->addAll(Ljava/util/Collection;)V

    .line 155
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->h:Lsoftware/simplicial/nebulous/a/q;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/q;->notifyDataSetChanged()V

    goto :goto_0

    .line 120
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->e:Landroid/widget/TextView;

    const v1, 0x7f08017c

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/av;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->e:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 267
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 319
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->b:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 259
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 261
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 66
    const v0, 0x7f040054

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 68
    invoke-super {p0, v1, p3}, Lsoftware/simplicial/nebulous/application/bc;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 70
    const v0, 0x7f0d01fe

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->c:Landroid/widget/ListView;

    .line 71
    const v0, 0x7f0d0085

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->e:Landroid/widget/TextView;

    .line 72
    const v0, 0x7f0d00e9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->b:Landroid/widget/Button;

    .line 73
    const v0, 0x7f0d024e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->d:Landroid/widget/ListView;

    .line 74
    const v0, 0x7f0d024d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->f:Landroid/widget/TextView;

    .line 76
    return-object v1
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 249
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/bc;->onPause()V

    .line 251
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/a/cc;->b(Lsoftware/simplicial/a/b;)V

    .line 252
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 241
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/bc;->onResume()V

    .line 243
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/a/cc;->a(Lsoftware/simplicial/a/b;)V

    .line 244
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 82
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/bc;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 84
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->b:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    new-instance v0, Lsoftware/simplicial/nebulous/a/z;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/av;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Lsoftware/simplicial/nebulous/a/z;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->g:Lsoftware/simplicial/nebulous/a/z;

    .line 86
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/av;->g:Lsoftware/simplicial/nebulous/a/z;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 87
    new-instance v0, Lsoftware/simplicial/nebulous/a/q;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/av;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Lsoftware/simplicial/nebulous/a/q;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->h:Lsoftware/simplicial/nebulous/a/q;

    .line 88
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/av;->d:Landroid/widget/ListView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/av;->h:Lsoftware/simplicial/nebulous/a/q;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 90
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/av;->c()V

    .line 91
    invoke-static {}, Lcom/google/android/gms/common/GoogleApiAvailability;->getInstance()Lcom/google/android/gms/common/GoogleApiAvailability;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/av;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/GoogleApiAvailability;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    .line 92
    if-nez v0, :cond_0

    .line 93
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/av;->d()V

    .line 94
    :cond_0
    return-void
.end method

.method public p_()V
    .locals 0

    .prologue
    .line 281
    return-void
.end method
