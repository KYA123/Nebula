.class Lsoftware/simplicial/nebulous/application/ay$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$o;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/ay;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/application/ay;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/ay;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/ay$1;->a:Lsoftware/simplicial/nebulous/application/ay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/util/ArrayList;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lsoftware/simplicial/nebulous/f/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 71
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ay$1;->a:Lsoftware/simplicial/nebulous/application/ay;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ay;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 120
    :goto_0
    return-void

    .line 74
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ay$1;->a:Lsoftware/simplicial/nebulous/application/ay;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ay;->g:Lsoftware/simplicial/nebulous/a/aa;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/aa;->clear()V

    .line 76
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/f/g;

    .line 78
    iget-object v1, v0, Lsoftware/simplicial/nebulous/f/g;->f:Lsoftware/simplicial/a/h/h;

    sget-object v2, Lsoftware/simplicial/a/h/h;->a:Lsoftware/simplicial/a/h/h;

    if-eq v1, v2, :cond_1

    iget-object v1, v0, Lsoftware/simplicial/nebulous/f/g;->f:Lsoftware/simplicial/a/h/h;

    sget-object v2, Lsoftware/simplicial/a/h/h;->b:Lsoftware/simplicial/a/h/h;

    if-eq v1, v2, :cond_1

    .line 84
    iget-object v1, v0, Lsoftware/simplicial/nebulous/f/g;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    .line 85
    iget-object v1, v0, Lsoftware/simplicial/nebulous/f/g;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v2, v3

    move v4, v3

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lsoftware/simplicial/nebulous/f/h;

    .line 87
    iget v9, v1, Lsoftware/simplicial/nebulous/f/h;->a:I

    const/4 v10, -0x1

    if-eq v9, v10, :cond_b

    .line 89
    iget-object v9, v1, Lsoftware/simplicial/nebulous/f/h;->c:Lsoftware/simplicial/a/h/h;

    sget-object v10, Lsoftware/simplicial/a/h/h;->d:Lsoftware/simplicial/a/h/h;

    if-eq v9, v10, :cond_2

    iget-object v9, v1, Lsoftware/simplicial/nebulous/f/h;->c:Lsoftware/simplicial/a/h/h;

    sget-object v10, Lsoftware/simplicial/a/h/h;->c:Lsoftware/simplicial/a/h/h;

    if-ne v9, v10, :cond_3

    .line 91
    :cond_2
    add-int/lit8 v4, v4, 0x1

    .line 92
    add-int/lit8 v2, v2, 0x1

    .line 94
    :cond_3
    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/h;->c:Lsoftware/simplicial/a/h/h;

    sget-object v9, Lsoftware/simplicial/a/h/h;->b:Lsoftware/simplicial/a/h/h;

    if-ne v1, v9, :cond_b

    .line 96
    add-int/lit8 v2, v2, 0x1

    move v1, v2

    move v2, v4

    :goto_3
    move v4, v2

    move v2, v1

    .line 99
    goto :goto_2

    .line 100
    :cond_4
    sget-object v1, Lsoftware/simplicial/nebulous/application/ay;->b:Lsoftware/simplicial/nebulous/application/ay$a;

    sget-object v8, Lsoftware/simplicial/nebulous/application/ay$a;->a:Lsoftware/simplicial/nebulous/application/ay$a;

    if-ne v1, v8, :cond_6

    .line 102
    if-ne v4, v7, :cond_5

    move v1, v5

    .line 109
    :goto_4
    if-eqz v1, :cond_1

    .line 112
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ay$1;->a:Lsoftware/simplicial/nebulous/application/ay;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/ay;->g:Lsoftware/simplicial/nebulous/a/aa;

    invoke-virtual {v1, v0}, Lsoftware/simplicial/nebulous/a/aa;->add(Ljava/lang/Object;)V

    goto :goto_1

    :cond_5
    move v1, v3

    .line 102
    goto :goto_4

    .line 104
    :cond_6
    sget-object v1, Lsoftware/simplicial/nebulous/application/ay;->b:Lsoftware/simplicial/nebulous/application/ay$a;

    sget-object v4, Lsoftware/simplicial/nebulous/application/ay$a;->b:Lsoftware/simplicial/nebulous/application/ay$a;

    if-ne v1, v4, :cond_a

    .line 106
    if-ge v2, v7, :cond_7

    move v1, v5

    goto :goto_4

    :cond_7
    move v1, v3

    goto :goto_4

    .line 114
    :cond_8
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ay$1;->a:Lsoftware/simplicial/nebulous/application/ay;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ay;->g:Lsoftware/simplicial/nebulous/a/aa;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/aa;->notifyDataSetChanged()V

    .line 116
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_9

    .line 117
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ay$1;->a:Lsoftware/simplicial/nebulous/application/ay;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ay;->f:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 119
    :cond_9
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ay$1;->a:Lsoftware/simplicial/nebulous/application/ay;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ay;->f:Landroid/widget/TextView;

    const v1, 0x7f0801be

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    :cond_a
    move v1, v5

    goto :goto_4

    :cond_b
    move v1, v2

    move v2, v4

    goto :goto_3
.end method
