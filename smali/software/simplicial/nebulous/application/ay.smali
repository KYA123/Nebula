.class public Lsoftware/simplicial/nebulous/application/ay;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/nebulous/application/ay$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static b:Lsoftware/simplicial/nebulous/application/ay$a;

.field public static c:Lsoftware/simplicial/nebulous/f/a;


# instance fields
.field d:Landroid/widget/ListView;

.field e:Landroid/widget/Button;

.field f:Landroid/widget/TextView;

.field g:Lsoftware/simplicial/nebulous/a/aa;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lsoftware/simplicial/nebulous/application/ay;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/ay;->a:Ljava/lang/String;

    .line 29
    sget-object v0, Lsoftware/simplicial/nebulous/application/ay$a;->a:Lsoftware/simplicial/nebulous/application/ay$a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/ay;->b:Lsoftware/simplicial/nebulous/application/ay$a;

    .line 30
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/ay;->c:Lsoftware/simplicial/nebulous/f/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ay;->e:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 129
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ay;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 131
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 39
    const v0, 0x7f04005a

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 41
    const v0, 0x7f0d01fd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ay;->d:Landroid/widget/ListView;

    .line 42
    const v0, 0x7f0d0085

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ay;->f:Landroid/widget/TextView;

    .line 43
    const v0, 0x7f0d00b9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ay;->e:Landroid/widget/Button;

    .line 45
    return-object v1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 136
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ay;->g:Lsoftware/simplicial/nebulous/a/aa;

    invoke-virtual {v0, p3}, Lsoftware/simplicial/nebulous/a/aa;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/f/g;

    .line 138
    sget-object v1, Lsoftware/simplicial/nebulous/application/ay;->b:Lsoftware/simplicial/nebulous/application/ay$a;

    sget-object v2, Lsoftware/simplicial/nebulous/application/ay$a;->a:Lsoftware/simplicial/nebulous/application/ay$a;

    if-ne v1, v2, :cond_0

    .line 140
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ay;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget v0, v0, Lsoftware/simplicial/nebulous/f/g;->a:I

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lsoftware/simplicial/a/t;->a(IZ)V

    .line 141
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ay;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 149
    :goto_0
    return-void

    .line 145
    :cond_0
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ay;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget v0, v0, Lsoftware/simplicial/nebulous/f/g;->a:I

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ay;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->C:I

    invoke-virtual {v1, v0, v2}, Lsoftware/simplicial/nebulous/f/al;->b(II)V

    .line 147
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ay;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 63
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onResume()V

    .line 65
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ay;->f:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 66
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ay;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    new-instance v1, Lsoftware/simplicial/nebulous/application/ay$1;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/ay$1;-><init>(Lsoftware/simplicial/nebulous/application/ay;)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al$o;)V

    .line 122
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 51
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 53
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ay;->e:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ay;->d:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 56
    new-instance v0, Lsoftware/simplicial/nebulous/a/aa;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ay;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Lsoftware/simplicial/nebulous/a/aa;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ay;->g:Lsoftware/simplicial/nebulous/a/aa;

    .line 57
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ay;->d:Landroid/widget/ListView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ay;->g:Lsoftware/simplicial/nebulous/a/aa;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 58
    return-void
.end method
