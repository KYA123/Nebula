.class public Lsoftware/simplicial/nebulous/application/ba;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lsoftware/simplicial/a/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/nebulous/application/ba$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static b:Lsoftware/simplicial/nebulous/application/ba$a;

.field public static c:Lsoftware/simplicial/nebulous/f/a;


# instance fields
.field d:Landroid/widget/ListView;

.field e:Landroid/widget/Button;

.field f:Landroid/widget/Button;

.field g:Landroid/widget/ImageButton;

.field h:Landroid/widget/ImageButton;

.field i:Landroid/widget/TextView;

.field j:Lsoftware/simplicial/nebulous/a/s;

.field private k:I

.field private l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lsoftware/simplicial/a/bg;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lsoftware/simplicial/nebulous/application/ba;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/ba;->a:Ljava/lang/String;

    .line 32
    sget-object v0, Lsoftware/simplicial/nebulous/application/ba$a;->a:Lsoftware/simplicial/nebulous/application/ba$a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/ba;->b:Lsoftware/simplicial/nebulous/application/ba$a;

    .line 33
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/ba;->c:Lsoftware/simplicial/nebulous/f/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/nebulous/application/ba;->k:I

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->l:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/ba;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/ba;->l:Ljava/util/ArrayList;

    return-object p1
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 96
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ba;->g:Landroid/widget/ImageButton;

    iget v0, p0, Lsoftware/simplicial/nebulous/application/ba;->k:I

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 97
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->h:Landroid/widget/ImageButton;

    iget v3, p0, Lsoftware/simplicial/nebulous/application/ba;->k:I

    const/16 v4, 0x9

    if-ge v3, v4, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 99
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->j:Lsoftware/simplicial/nebulous/a/s;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/s;->clear()V

    .line 100
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->j:Lsoftware/simplicial/nebulous/a/s;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/s;->notifyDataSetChanged()V

    .line 101
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/ba;->k:I

    mul-int/lit8 v1, v1, 0x64

    const/16 v2, 0x64

    new-instance v3, Lsoftware/simplicial/nebulous/application/ba$1;

    invoke-direct {v3, p0}, Lsoftware/simplicial/nebulous/application/ba$1;-><init>(Lsoftware/simplicial/nebulous/application/ba;)V

    invoke-virtual {v0, v1, v2, v3}, Lsoftware/simplicial/nebulous/f/al;->a(IILsoftware/simplicial/nebulous/f/al$s;)V

    .line 120
    return-void

    :cond_0
    move v0, v2

    .line 96
    goto :goto_0

    :cond_1
    move v1, v2

    .line 97
    goto :goto_1
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/ba;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ba;->b()V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 125
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ba;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/t;->a(Ljava/util/List;)V

    .line 126
    :cond_0
    return-void
.end method


# virtual methods
.method public a([I[Lsoftware/simplicial/a/bg$b;[Lsoftware/simplicial/a/aq;[Z[Lsoftware/simplicial/a/bn;[Lsoftware/simplicial/a/ak;[S)V
    .locals 10

    .prologue
    .line 173
    iget-object v9, p0, Lsoftware/simplicial/nebulous/application/ba;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 174
    if-nez v9, :cond_0

    .line 189
    :goto_0
    return-void

    .line 177
    :cond_0
    new-instance v0, Lsoftware/simplicial/nebulous/application/ba$2;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lsoftware/simplicial/nebulous/application/ba$2;-><init>(Lsoftware/simplicial/nebulous/application/ba;[I[Lsoftware/simplicial/a/bg$b;[Lsoftware/simplicial/a/aq;[Z[Lsoftware/simplicial/a/bn;[Lsoftware/simplicial/a/ak;[S)V

    invoke-virtual {v9, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->e:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 133
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 149
    :cond_0
    :goto_0
    return-void

    .line 135
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->f:Landroid/widget/Button;

    if-ne p1, v0, :cond_2

    .line 137
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ba;->b()V

    goto :goto_0

    .line 139
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->g:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_3

    iget v0, p0, Lsoftware/simplicial/nebulous/application/ba;->k:I

    if-lez v0, :cond_3

    .line 141
    iget v0, p0, Lsoftware/simplicial/nebulous/application/ba;->k:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/application/ba;->k:I

    .line 142
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ba;->a()V

    goto :goto_0

    .line 144
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->h:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    iget v0, p0, Lsoftware/simplicial/nebulous/application/ba;->k:I

    const/16 v1, 0x9

    if-ge v0, v1, :cond_0

    .line 146
    iget v0, p0, Lsoftware/simplicial/nebulous/application/ba;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/application/ba;->k:I

    .line 147
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ba;->a()V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 48
    const v0, 0x7f040058

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 50
    const v0, 0x7f0d025c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->d:Landroid/widget/ListView;

    .line 51
    const v0, 0x7f0d0085

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->i:Landroid/widget/TextView;

    .line 52
    const v0, 0x7f0d00b9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->e:Landroid/widget/Button;

    .line 53
    const v0, 0x7f0d01e8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->f:Landroid/widget/Button;

    .line 54
    const v0, 0x7f0d012d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->g:Landroid/widget/ImageButton;

    .line 55
    const v0, 0x7f0d012e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->h:Landroid/widget/ImageButton;

    .line 57
    return-object v1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 154
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->j:Lsoftware/simplicial/nebulous/a/s;

    invoke-virtual {v0, p3}, Lsoftware/simplicial/nebulous/a/s;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bg;

    .line 156
    sget-object v1, Lsoftware/simplicial/nebulous/application/ba;->b:Lsoftware/simplicial/nebulous/application/ba$a;

    sget-object v2, Lsoftware/simplicial/nebulous/application/ba$a;->a:Lsoftware/simplicial/nebulous/application/ba$a;

    if-ne v1, v2, :cond_1

    .line 158
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ba;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v0, Lsoftware/simplicial/a/bg;->c:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->ak:Ljava/lang/String;

    .line 159
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ba;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget v0, v0, Lsoftware/simplicial/a/bg;->b:I

    iput v0, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->aj:I

    .line 167
    :cond_0
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 168
    return-void

    .line 161
    :cond_1
    sget-object v1, Lsoftware/simplicial/nebulous/application/ba;->b:Lsoftware/simplicial/nebulous/application/ba$a;

    sget-object v2, Lsoftware/simplicial/nebulous/application/ba$a;->b:Lsoftware/simplicial/nebulous/application/ba$a;

    if-ne v1, v2, :cond_0

    .line 163
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ba;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    sget-object v2, Lsoftware/simplicial/a/i/c;->b:Lsoftware/simplicial/a/i/c;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ba;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v3, v3, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    const/4 v4, 0x1

    iget v0, v0, Lsoftware/simplicial/a/bg;->b:I

    invoke-virtual {v1, v2, v3, v4, v0}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/i/c;ZZI)V

    .line 164
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    const-string v1, "TOURNAMENT_REGISTRATION"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/nebulous/f/e;->a(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 89
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onPause()V

    .line 91
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->i:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 92
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 78
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onResume()V

    .line 80
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->i:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 82
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->i:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 83
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ba;->a()V

    .line 84
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 63
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 65
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->e:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->f:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->d:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 68
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    new-instance v0, Lsoftware/simplicial/nebulous/a/s;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ba;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v2, Lsoftware/simplicial/nebulous/a/s$a;->b:Lsoftware/simplicial/nebulous/a/s$a;

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/a/s;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/nebulous/a/s$a;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->j:Lsoftware/simplicial/nebulous/a/s;

    .line 72
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ba;->d:Landroid/widget/ListView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ba;->j:Lsoftware/simplicial/nebulous/a/s;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 73
    return-void
.end method
