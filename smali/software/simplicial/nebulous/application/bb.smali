.class public Lsoftware/simplicial/nebulous/application/bb;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lsoftware/simplicial/a/c;


# static fields
.field public static final a:Ljava/lang/String;

.field public static b:Lsoftware/simplicial/nebulous/f/a;


# instance fields
.field c:Landroid/widget/ListView;

.field d:Landroid/widget/Button;

.field e:Landroid/widget/TextView;

.field f:Lsoftware/simplicial/nebulous/a/s;

.field private g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lsoftware/simplicial/a/bg;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lsoftware/simplicial/nebulous/application/bb;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/bb;->a:Ljava/lang/String;

    .line 31
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/bb;->b:Lsoftware/simplicial/nebulous/f/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bb;->g:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/bb;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/bb;->g:Ljava/util/ArrayList;

    return-object p1
.end method

.method private a()V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bb;->f:Lsoftware/simplicial/nebulous/a/s;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/s;->clear()V

    .line 85
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bb;->f:Lsoftware/simplicial/nebulous/a/s;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/s;->notifyDataSetChanged()V

    .line 86
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bb;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    new-instance v1, Lsoftware/simplicial/nebulous/application/bb$1;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/bb$1;-><init>(Lsoftware/simplicial/nebulous/application/bb;)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al$t;)V

    .line 105
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/bb;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bb;->b()V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bb;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 110
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bb;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bb;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/t;->a(Ljava/util/List;)V

    .line 111
    :cond_0
    return-void
.end method


# virtual methods
.method public a([I[Lsoftware/simplicial/a/bg$b;[Lsoftware/simplicial/a/aq;[Z[Lsoftware/simplicial/a/bn;[Lsoftware/simplicial/a/ak;[S)V
    .locals 10

    .prologue
    .line 136
    iget-object v9, p0, Lsoftware/simplicial/nebulous/application/bb;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 137
    if-nez v9, :cond_0

    .line 152
    :goto_0
    return-void

    .line 140
    :cond_0
    new-instance v0, Lsoftware/simplicial/nebulous/application/bb$2;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lsoftware/simplicial/nebulous/application/bb$2;-><init>(Lsoftware/simplicial/nebulous/application/bb;[I[Lsoftware/simplicial/a/bg$b;[Lsoftware/simplicial/a/aq;[Z[Lsoftware/simplicial/a/bn;[Lsoftware/simplicial/a/ak;[S)V

    invoke-virtual {v9, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bb;->d:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 118
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bb;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 120
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 42
    const v0, 0x7f040059

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 44
    const v0, 0x7f0d025d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bb;->c:Landroid/widget/ListView;

    .line 45
    const v0, 0x7f0d0085

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bb;->e:Landroid/widget/TextView;

    .line 46
    const v0, 0x7f0d00b9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bb;->d:Landroid/widget/Button;

    .line 48
    return-object v1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 125
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bb;->f:Lsoftware/simplicial/nebulous/a/s;

    invoke-virtual {v0, p3}, Lsoftware/simplicial/nebulous/a/s;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bg;

    .line 127
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bb;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v2, Lsoftware/simplicial/nebulous/f/ak;->a:Lsoftware/simplicial/nebulous/f/ak;

    iput-object v2, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->F:Lsoftware/simplicial/nebulous/f/ak;

    .line 128
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bb;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget v0, v0, Lsoftware/simplicial/a/bg;->b:I

    iput v0, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->C:I

    .line 129
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bb;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v1, ""

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->E:Ljava/lang/CharSequence;

    .line 130
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bb;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->v:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    .line 131
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 77
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onPause()V

    .line 79
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bb;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->i:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 80
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 66
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onResume()V

    .line 68
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bb;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->i:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 70
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bb;->e:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 71
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bb;->a()V

    .line 72
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 54
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 56
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bb;->d:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bb;->c:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 59
    new-instance v0, Lsoftware/simplicial/nebulous/a/s;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bb;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v2, Lsoftware/simplicial/nebulous/a/s$a;->c:Lsoftware/simplicial/nebulous/a/s$a;

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/a/s;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/nebulous/a/s$a;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bb;->f:Lsoftware/simplicial/nebulous/a/s;

    .line 60
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bb;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bb;->f:Lsoftware/simplicial/nebulous/a/s;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 61
    return-void
.end method
