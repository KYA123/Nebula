.class Lsoftware/simplicial/nebulous/application/bi$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/bi;->a(Lsoftware/simplicial/nebulous/application/MainActivity;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lsoftware/simplicial/nebulous/application/bi;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/bi;Z)V
    .locals 0

    .prologue
    .line 487
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iput-boolean p2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v3, 0x3

    const/4 v7, 0x0

    const v6, 0x7f08023c

    .line 491
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 601
    :goto_0
    return-void

    .line 494
    :cond_0
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 496
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/bi;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->aa:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v1

    .line 497
    if-eqz v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->Z:Ljava/util/List;

    sget-object v2, Lsoftware/simplicial/a/bp;->b:Lsoftware/simplicial/a/bp;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 499
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v3, v3, v0}, Ljava/text/DateFormat;->getDateTimeInstance(IILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v0

    .line 500
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/bi;->l:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    const v5, 0x7f0800fa

    invoke-virtual {v4, v5}, Lsoftware/simplicial/nebulous/application/bi;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    const v5, 0x7f080114

    invoke-virtual {v4, v5}, Lsoftware/simplicial/nebulous/application/bi;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/bi;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->aa:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 501
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 502
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->l:Landroid/widget/TextView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/bi;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c003f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 509
    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/bi;->e(Lsoftware/simplicial/nebulous/application/bi;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 511
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 506
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 515
    :cond_2
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->a:Z

    if-eqz v0, :cond_4

    .line 516
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/bi;->f(Lsoftware/simplicial/nebulous/application/bi;)D

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lsoftware/simplicial/nebulous/application/bi;->a(Lsoftware/simplicial/nebulous/application/bi;D)D

    .line 519
    :goto_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/bi;->h(Lsoftware/simplicial/nebulous/application/bi;)D

    move-result-wide v2

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/bi;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0, v2, v3, v4}, Lsoftware/simplicial/nebulous/application/bi;->a(Lsoftware/simplicial/nebulous/application/bi;DLandroid/app/Activity;)V

    .line 523
    :try_start_0
    const-string v0, "NONE"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/bi;->e(Lsoftware/simplicial/nebulous/application/bi;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 525
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->h:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    const v3, 0x7f08023c

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/bi;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 526
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->g:Landroid/widget/ImageView;

    const v1, 0x7f020432

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 600
    :cond_3
    :goto_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 518
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/bi;->g(Lsoftware/simplicial/nebulous/application/bi;)D

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lsoftware/simplicial/nebulous/application/bi;->a(Lsoftware/simplicial/nebulous/application/bi;D)D

    goto :goto_2

    .line 528
    :cond_5
    :try_start_1
    const-string v0, "SKIN"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/bi;->e(Lsoftware/simplicial/nebulous/application/bi;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 530
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->h:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    const v3, 0x7f08023c

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/bi;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 531
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/bi;->i(Lsoftware/simplicial/nebulous/application/bi;)I

    move-result v0

    if-ltz v0, :cond_6

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/bi;->i(Lsoftware/simplicial/nebulous/application/bi;)I

    move-result v0

    sget-object v1, Lsoftware/simplicial/a/e;->a:[Lsoftware/simplicial/a/e;

    array-length v1, v1

    if-ge v0, v1, :cond_6

    .line 532
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->g:Landroid/widget/ImageView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/bi;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget-object v2, Lsoftware/simplicial/a/e;->a:[Lsoftware/simplicial/a/e;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v3}, Lsoftware/simplicial/nebulous/application/bi;->i(Lsoftware/simplicial/nebulous/application/bi;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lsoftware/simplicial/a/e;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "drawable"

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/bi;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_3

    .line 594
    :catch_0
    move-exception v0

    .line 596
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 597
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->h:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-virtual {v2, v6}, Lsoftware/simplicial/nebulous/application/bi;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    const v3, 0x7f080102

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/bi;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 598
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->g:Landroid/widget/ImageView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_3

    .line 534
    :cond_6
    :try_start_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->g:Landroid/widget/ImageView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/bi;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget-object v2, Lsoftware/simplicial/a/e;->b:Lsoftware/simplicial/a/e;

    invoke-virtual {v2}, Lsoftware/simplicial/a/e;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "drawable"

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/bi;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_3

    .line 536
    :cond_7
    const-string v0, "EJECT_SKIN"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/bi;->e(Lsoftware/simplicial/nebulous/application/bi;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 538
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->h:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    const v3, 0x7f08023c

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/bi;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 539
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/bi;->i(Lsoftware/simplicial/nebulous/application/bi;)I

    move-result v0

    if-ltz v0, :cond_8

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/bi;->i(Lsoftware/simplicial/nebulous/application/bi;)I

    move-result v0

    sget-object v1, Lsoftware/simplicial/a/af;->b:[Lsoftware/simplicial/a/af;

    array-length v1, v1

    if-ge v0, v1, :cond_8

    .line 540
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->g:Landroid/widget/ImageView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/bi;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/bi;->i(Lsoftware/simplicial/nebulous/application/bi;)I

    move-result v2

    invoke-static {v2}, Lsoftware/simplicial/a/af;->a(I)Lsoftware/simplicial/a/af;

    move-result-object v2

    invoke-virtual {v2}, Lsoftware/simplicial/a/af;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "drawable"

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/bi;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_3

    .line 542
    :cond_8
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->g:Landroid/widget/ImageView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/bi;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget-object v2, Lsoftware/simplicial/a/af;->a:Lsoftware/simplicial/a/af;

    invoke-virtual {v2}, Lsoftware/simplicial/a/af;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "drawable"

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/bi;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_3

    .line 544
    :cond_9
    const-string v0, "HAT"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/bi;->e(Lsoftware/simplicial/nebulous/application/bi;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 546
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->h:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    const v3, 0x7f08023c

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/bi;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 547
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/bi;->i(Lsoftware/simplicial/nebulous/application/bi;)I

    move-result v0

    if-ltz v0, :cond_a

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/bi;->i(Lsoftware/simplicial/nebulous/application/bi;)I

    move-result v0

    sget-object v1, Lsoftware/simplicial/a/as;->b:[Lsoftware/simplicial/a/as;

    array-length v1, v1

    if-ge v0, v1, :cond_a

    .line 548
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->g:Landroid/widget/ImageView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/bi;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/bi;->i(Lsoftware/simplicial/nebulous/application/bi;)I

    move-result v2

    invoke-static {v2}, Lsoftware/simplicial/a/as;->a(I)Lsoftware/simplicial/a/as;

    move-result-object v2

    invoke-virtual {v2}, Lsoftware/simplicial/a/as;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "drawable"

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/bi;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_3

    .line 550
    :cond_a
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->g:Landroid/widget/ImageView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/bi;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget-object v2, Lsoftware/simplicial/a/as;->a:Lsoftware/simplicial/a/as;

    invoke-virtual {v2}, Lsoftware/simplicial/a/as;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "drawable"

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/bi;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_3

    .line 552
    :cond_b
    const-string v0, "PET"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/bi;->e(Lsoftware/simplicial/nebulous/application/bi;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 554
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->h:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    const v3, 0x7f08023c

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/bi;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 555
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/bi;->i(Lsoftware/simplicial/nebulous/application/bi;)I

    move-result v0

    if-ltz v0, :cond_c

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/bi;->i(Lsoftware/simplicial/nebulous/application/bi;)I

    move-result v0

    sget-object v1, Lsoftware/simplicial/a/bd;->b:[Lsoftware/simplicial/a/bd;

    array-length v1, v1

    if-ge v0, v1, :cond_c

    .line 556
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->g:Landroid/widget/ImageView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/bi;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/bi;->i(Lsoftware/simplicial/nebulous/application/bi;)I

    move-result v2

    invoke-static {v2}, Lsoftware/simplicial/a/bd;->a(I)Lsoftware/simplicial/a/bd;

    move-result-object v2

    invoke-virtual {v2}, Lsoftware/simplicial/a/bd;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "drawable"

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/bi;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_3

    .line 558
    :cond_c
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->g:Landroid/widget/ImageView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/bi;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget-object v2, Lsoftware/simplicial/a/bd;->a:Lsoftware/simplicial/a/bd;

    invoke-virtual {v2}, Lsoftware/simplicial/a/bd;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "drawable"

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/bi;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_3

    .line 560
    :cond_d
    const-string v0, "XP_2X"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/bi;->e(Lsoftware/simplicial/nebulous/application/bi;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 562
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->h:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    const v3, 0x7f08023c

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/bi;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    const v3, 0x7f080314

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/bi;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " +"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/bi;->i(Lsoftware/simplicial/nebulous/application/bi;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    const v3, 0x7f08014d

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/bi;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 563
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->g:Landroid/widget/ImageView;

    const v1, 0x7f020433

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_3

    .line 565
    :cond_e
    const-string v0, "XP_3X"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/bi;->e(Lsoftware/simplicial/nebulous/application/bi;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 567
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->h:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    const v3, 0x7f08023c

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/bi;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    const v3, 0x7f080315

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/bi;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " +"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/bi;->i(Lsoftware/simplicial/nebulous/application/bi;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    const v3, 0x7f08014d

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/bi;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 568
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->g:Landroid/widget/ImageView;

    const v1, 0x7f020435

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_3

    .line 570
    :cond_f
    const-string v0, "AUTO"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/bi;->e(Lsoftware/simplicial/nebulous/application/bi;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 572
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->h:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    const v3, 0x7f08023c

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/bi;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    const v3, 0x7f080036

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/bi;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " +"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/bi;->i(Lsoftware/simplicial/nebulous/application/bi;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    const v3, 0x7f08014d

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/bi;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 573
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->g:Landroid/widget/ImageView;

    const v1, 0x7f02016e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_3

    .line 575
    :cond_10
    const-string v0, "ULTRA"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/bi;->e(Lsoftware/simplicial/nebulous/application/bi;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 577
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->h:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    const v3, 0x7f08023c

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/bi;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    const v3, 0x7f0802d7

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/bi;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " +"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/bi;->i(Lsoftware/simplicial/nebulous/application/bi;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    const v3, 0x7f08014d

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/bi;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 578
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->g:Landroid/widget/ImageView;

    const v1, 0x7f020170

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_3

    .line 580
    :cond_11
    const-string v0, "PLASMA"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/bi;->e(Lsoftware/simplicial/nebulous/application/bi;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 582
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/bi;->i(Lsoftware/simplicial/nebulous/application/bi;)I

    move-result v0

    .line 583
    if-eqz v1, :cond_12

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/bi;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->Z:Ljava/util/List;

    sget-object v2, Lsoftware/simplicial/a/bp;->b:Lsoftware/simplicial/a/bp;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 585
    mul-int/lit8 v0, v0, 0x2

    .line 586
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/bi;->h:Landroid/widget/TextView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/bi;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c003f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 590
    :goto_4
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/bi;->h:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    const v4, 0x7f08023c

    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/bi;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 591
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->g:Landroid/widget/ImageView;

    const v1, 0x7f020316

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_3

    .line 589
    :cond_12
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/bi;->h:Landroid/widget/TextView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$3;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/bi;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c00fb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_4
.end method
