.class public Lsoftware/simplicial/nebulous/application/c;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final a:Ljava/lang/String;

.field public static b:Lsoftware/simplicial/nebulous/f/a;


# instance fields
.field c:Landroid/widget/Button;

.field d:Landroid/widget/ListView;

.field e:Landroid/widget/TextView;

.field f:Landroid/widget/Spinner;

.field g:Landroid/widget/CheckBox;

.field private h:Lsoftware/simplicial/nebulous/a/b;

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lsoftware/simplicial/nebulous/application/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/c;->a:Ljava/lang/String;

    .line 31
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/c;->b:Lsoftware/simplicial/nebulous/f/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/nebulous/application/c;->i:I

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/c;I)I
    .locals 0

    .prologue
    .line 28
    iput p1, p0, Lsoftware/simplicial/nebulous/application/c;->i:I

    return p1
.end method

.method private a()V
    .locals 4

    .prologue
    .line 118
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/c;->h:Lsoftware/simplicial/nebulous/a/b;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/b;->clear()V

    .line 119
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/c;->h:Lsoftware/simplicial/nebulous/a/b;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/b;->notifyDataSetChanged()V

    .line 121
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/c;->e:Landroid/widget/TextView;

    const v1, 0x7f08017d

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/c;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/c;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/c;->i:I

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/c;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v2, v2, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    new-instance v3, Lsoftware/simplicial/nebulous/application/c$3;

    invoke-direct {v3, p0}, Lsoftware/simplicial/nebulous/application/c$3;-><init>(Lsoftware/simplicial/nebulous/application/c;)V

    invoke-virtual {v0, v1, v2, v3}, Lsoftware/simplicial/nebulous/f/al;->a(IZLsoftware/simplicial/nebulous/f/al$c;)V

    .line 138
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/c;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/c;->a()V

    return-void
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/application/c;)Lsoftware/simplicial/nebulous/a/b;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/c;->h:Lsoftware/simplicial/nebulous/a/b;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/c;->c:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 145
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/c;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 147
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 45
    const v0, 0x7f040028

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 47
    const v0, 0x7f0d00b9

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/c;->c:Landroid/widget/Button;

    .line 48
    const v0, 0x7f0d00ba

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/c;->d:Landroid/widget/ListView;

    .line 49
    const v0, 0x7f0d0085

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/c;->e:Landroid/widget/TextView;

    .line 50
    const v0, 0x7f0d00bd

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/c;->f:Landroid/widget/Spinner;

    .line 51
    const v0, 0x7f0d00bb

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/c;->g:Landroid/widget/CheckBox;

    .line 53
    invoke-static {}, Lsoftware/simplicial/nebulous/f/aa;->b()I

    move-result v3

    .line 54
    new-array v4, v3, [Ljava/lang/String;

    move v0, v1

    .line 55
    :goto_0
    if-ge v0, v3, :cond_1

    .line 57
    add-int/lit8 v1, v3, -0x1

    if-ne v0, v1, :cond_0

    .line 59
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/c;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f0800d4

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 55
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 63
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/c;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f08025a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    add-int/lit8 v5, v0, 0x1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    goto :goto_1

    .line 66
    :cond_1
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/c;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f040098

    invoke-direct {v0, v1, v5, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 67
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/c;->f:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 68
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/c;->f:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setSelection(I)V

    .line 70
    add-int/lit8 v0, v3, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/application/c;->i:I

    .line 72
    return-object v2
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 78
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 80
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/c;->c:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    new-instance v0, Lsoftware/simplicial/nebulous/a/b;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/c;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Lsoftware/simplicial/nebulous/a/b;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/c;->h:Lsoftware/simplicial/nebulous/a/b;

    .line 82
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/c;->d:Landroid/widget/ListView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/c;->h:Lsoftware/simplicial/nebulous/a/b;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 83
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/c;->g:Landroid/widget/CheckBox;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/c;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 84
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/c;->g:Landroid/widget/CheckBox;

    new-instance v1, Lsoftware/simplicial/nebulous/application/c$1;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/c$1;-><init>(Lsoftware/simplicial/nebulous/application/c;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 97
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/c;->f:Landroid/widget/Spinner;

    new-instance v1, Lsoftware/simplicial/nebulous/application/c$2;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/c$2;-><init>(Lsoftware/simplicial/nebulous/application/c;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 113
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/c;->a()V

    .line 114
    return-void
.end method
