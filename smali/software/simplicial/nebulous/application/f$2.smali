.class Lsoftware/simplicial/nebulous/application/f$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/f;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/application/f;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/f;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/f$2;->a:Lsoftware/simplicial/nebulous/application/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 100
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/f$2;->a:Lsoftware/simplicial/nebulous/application/f;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/f;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 101
    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 102
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/f$2;->a:Lsoftware/simplicial/nebulous/application/f;

    const v2, 0x7f0800c6

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/f;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 103
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/f$2;->a:Lsoftware/simplicial/nebulous/application/f;

    const v3, 0x7f080110

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/f;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/f$2;->a:Lsoftware/simplicial/nebulous/application/f;

    const v3, 0x7f0800d3

    .line 104
    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/f;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-static {v2}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/f$2;->a:Lsoftware/simplicial/nebulous/application/f;

    invoke-static {v3}, Lsoftware/simplicial/nebulous/application/f;->a(Lsoftware/simplicial/nebulous/application/f;)I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/f$2;->a:Lsoftware/simplicial/nebulous/application/f;

    const v3, 0x7f0801e6

    .line 105
    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/f;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 103
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 107
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/f$2;->a:Lsoftware/simplicial/nebulous/application/f;

    const v2, 0x7f0801e0

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/f;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/f$2$1;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/f$2$1;-><init>(Lsoftware/simplicial/nebulous/application/f$2;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 131
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/f$2;->a:Lsoftware/simplicial/nebulous/application/f;

    const v2, 0x7f08005d

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/f;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 132
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 133
    return-void
.end method
