.class Lsoftware/simplicial/nebulous/application/h$2;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/h;->b()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/application/h;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/h;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/h$2;->a:Lsoftware/simplicial/nebulous/application/h;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 131
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 133
    const-string v1, "android.bluetooth.device.action.FOUND"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 135
    const-string v0, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 136
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/h$2;->a:Lsoftware/simplicial/nebulous/application/h;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/application/h;->a(Lsoftware/simplicial/nebulous/application/h;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/h$2;->a:Lsoftware/simplicial/nebulous/application/h;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/application/h;->b(Lsoftware/simplicial/nebulous/application/h;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 138
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/h$2;->a:Lsoftware/simplicial/nebulous/application/h;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/application/h;->b(Lsoftware/simplicial/nebulous/application/h;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 139
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/h$2;->a:Lsoftware/simplicial/nebulous/application/h;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/h;->f:Lsoftware/simplicial/nebulous/a/f;

    invoke-virtual {v1, v0}, Lsoftware/simplicial/nebulous/a/f;->add(Ljava/lang/Object;)V

    .line 140
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/h$2;->a:Lsoftware/simplicial/nebulous/application/h;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/h;->f:Lsoftware/simplicial/nebulous/a/f;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/f;->notifyDataSetChanged()V

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 143
    :cond_1
    const-string v1, "android.bluetooth.adapter.action.DISCOVERY_STARTED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 145
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/h$2;->a:Lsoftware/simplicial/nebulous/application/h;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/h;->d:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 147
    :cond_2
    const-string v1, "android.bluetooth.adapter.action.DISCOVERY_FINISHED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/h$2;->a:Lsoftware/simplicial/nebulous/application/h;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/h;->d:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
