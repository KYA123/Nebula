.class public Lsoftware/simplicial/nebulous/application/h;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field b:Landroid/widget/Button;

.field c:Landroid/widget/Button;

.field d:Landroid/widget/TextView;

.field e:Landroid/widget/ListView;

.field f:Lsoftware/simplicial/nebulous/a/f;

.field private g:Landroid/bluetooth/BluetoothAdapter;

.field private h:Landroid/content/BroadcastReceiver;

.field private i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lsoftware/simplicial/nebulous/application/h;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/h;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    .line 52
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->i:Ljava/util/Set;

    .line 53
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->j:Ljava/util/HashSet;

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/h;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->i:Ljava/util/Set;

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 112
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->g:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->i:Ljava/util/Set;

    .line 113
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->i:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 116
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->i:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 119
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/h;->f:Lsoftware/simplicial/nebulous/a/f;

    invoke-virtual {v2, v0}, Lsoftware/simplicial/nebulous/a/f;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 122
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->f:Lsoftware/simplicial/nebulous/a/f;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/f;->notifyDataSetChanged()V

    .line 123
    return-void
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/application/h;)Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->j:Ljava/util/HashSet;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 127
    new-instance v0, Lsoftware/simplicial/nebulous/application/h$2;

    invoke-direct {v0, p0}, Lsoftware/simplicial/nebulous/application/h$2;-><init>(Lsoftware/simplicial/nebulous/application/h;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->h:Landroid/content/BroadcastReceiver;

    .line 154
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.bluetooth.device.action.FOUND"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 155
    const-string v1, "android.bluetooth.device.action.FOUND"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 156
    const-string v1, "android.bluetooth.adapter.action.DISCOVERY_STARTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 157
    const-string v1, "android.bluetooth.adapter.action.DISCOVERY_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 158
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/h;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 161
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/h;->g:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Landroid/bluetooth/BluetoothAdapter;)V

    .line 162
    return-void
.end method


# virtual methods
.method public a(Landroid/bluetooth/BluetoothDevice;)V
    .locals 5

    .prologue
    .line 227
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->g:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 229
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 236
    :goto_0
    return-void

    .line 233
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->v:Z

    .line 234
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    new-instance v1, Lsoftware/simplicial/nebulous/b/a;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/h;->g:Landroid/bluetooth/BluetoothAdapter;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-direct {v1, v2, p1, v3, v4}, Lsoftware/simplicial/nebulous/b/a;-><init>(Landroid/bluetooth/BluetoothAdapter;Landroid/bluetooth/BluetoothDevice;Lsoftware/simplicial/nebulous/b/b;Lsoftware/simplicial/a/t;)V

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->s:Lsoftware/simplicial/nebulous/b/a;

    .line 235
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->s:Lsoftware/simplicial/nebulous/b/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/aa;->a:Ljava/util/UUID;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/nebulous/b/a;->a(Ljava/util/UUID;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 27

    .prologue
    .line 186
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/h;->b:Landroid/widget/Button;

    move-object/from16 v0, p1

    if-ne v0, v2, :cond_4

    .line 188
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/h;->g:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_1

    .line 190
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 223
    :cond_0
    :goto_0
    return-void

    .line 194
    :cond_1
    new-instance v4, Lsoftware/simplicial/nebulous/f/ah;

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->t:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v4, v2}, Lsoftware/simplicial/nebulous/f/ah;-><init>(Ljava/util/concurrent/atomic/AtomicInteger;)V

    .line 195
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->j()V

    .line 196
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v8, v2, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    .line 197
    sget-object v2, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-eq v8, v2, :cond_2

    sget-object v2, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-eq v8, v2, :cond_2

    sget-object v2, Lsoftware/simplicial/a/am;->p:Lsoftware/simplicial/a/am;

    if-eq v8, v2, :cond_2

    sget-object v2, Lsoftware/simplicial/a/am;->q:Lsoftware/simplicial/a/am;

    if-eq v8, v2, :cond_2

    sget-object v2, Lsoftware/simplicial/a/am;->r:Lsoftware/simplicial/a/am;

    if-ne v8, v2, :cond_3

    .line 198
    :cond_2
    invoke-static {}, Lsoftware/simplicial/a/ai;->n()Lsoftware/simplicial/a/am;

    move-result-object v8

    .line 199
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v26, v0

    new-instance v2, Lsoftware/simplicial/a/bs;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const/4 v5, 0x1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v9, 0x7f08024b

    invoke-virtual {v7, v9}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/nebulous/application/h;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-static {v7, v9}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/am;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    new-array v7, v7, [B

    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v9, v9, Lsoftware/simplicial/nebulous/f/ag;->D:I

    .line 200
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v12, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v12, v12, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v12, v12, Lsoftware/simplicial/nebulous/f/ag;->C:Lsoftware/simplicial/a/ac;

    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v13, v13, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v13, v13, Lsoftware/simplicial/nebulous/f/ag;->D:I

    sget-object v14, Lsoftware/simplicial/a/aq;->a:Lsoftware/simplicial/a/aq;

    sget-object v15, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->v:Lsoftware/simplicial/a/ap;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-short v0, v0, Lsoftware/simplicial/nebulous/f/ag;->ag:S

    move/from16 v17, v0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    move/from16 v22, v0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    invoke-direct/range {v2 .. v25}, Lsoftware/simplicial/a/bs;-><init>(Lsoftware/simplicial/a/al;Lsoftware/simplicial/a/f/bf;ILjava/lang/String;[BLsoftware/simplicial/a/am;IJLsoftware/simplicial/a/ac;ILsoftware/simplicial/a/aq;Lsoftware/simplicial/a/bj;Lsoftware/simplicial/a/ap;SLsoftware/simplicial/a/c/a;Lsoftware/simplicial/a/b/a;Lsoftware/simplicial/a/h/c;Lsoftware/simplicial/a/i/a;Z[ZZZ)V

    move-object/from16 v0, v26

    iput-object v2, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->A:Lsoftware/simplicial/a/bs;

    .line 203
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->A:Lsoftware/simplicial/a/bs;

    invoke-virtual {v2}, Lsoftware/simplicial/a/bs;->k()Z

    .line 204
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->A:Lsoftware/simplicial/a/bs;

    iput-object v2, v4, Lsoftware/simplicial/nebulous/f/ah;->a:Lsoftware/simplicial/a/bs;

    .line 205
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iput-object v2, v4, Lsoftware/simplicial/nebulous/f/ah;->b:Lsoftware/simplicial/a/t;

    .line 207
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.bluetooth.adapter.action.REQUEST_DISCOVERABLE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 208
    const-string v3, "android.bluetooth.adapter.extra.DISCOVERABLE_DURATION"

    const/16 v5, 0x12c

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 209
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lsoftware/simplicial/nebulous/application/h;->startActivity(Landroid/content/Intent;)V

    .line 211
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/application/h;->g:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->b(Landroid/bluetooth/BluetoothAdapter;)Lsoftware/simplicial/nebulous/b/d;

    move-result-object v2

    iput-object v2, v4, Lsoftware/simplicial/nebulous/f/ah;->c:Lsoftware/simplicial/nebulous/b/d;

    .line 213
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v5, v5, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v5, v5, Lsoftware/simplicial/nebulous/f/ag;->D:I

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v8, 0x7f080044

    .line 214
    invoke-virtual {v7, v8}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    const/4 v8, 0x0

    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/nebulous/application/h;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/am;ZLandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/f/ag;->v:Lsoftware/simplicial/a/ap;

    const/4 v8, 0x1

    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v9, v9, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v10, v10, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v10, v10, Lsoftware/simplicial/nebulous/f/ag;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v11, v11, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    .line 215
    invoke-virtual {v11}, Lsoftware/simplicial/nebulous/f/ag;->a()[B

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v12, v12, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v12, v12, Lsoftware/simplicial/nebulous/f/ag;->l:Lsoftware/simplicial/a/e;

    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v13, v13, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v13, v13, Lsoftware/simplicial/nebulous/f/ag;->n:Lsoftware/simplicial/a/af;

    move-object/from16 v0, p0

    iget-object v14, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 216
    invoke-virtual {v14}, Lsoftware/simplicial/nebulous/application/MainActivity;->z()I

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v15, v15, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v15, v15, Lsoftware/simplicial/nebulous/f/ag;->r:Lsoftware/simplicial/a/bd;

    iget-byte v15, v15, Lsoftware/simplicial/a/bd;->c:B

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v16, v0

    .line 217
    invoke-virtual/range {v16 .. v16}, Lsoftware/simplicial/nebulous/f/ag;->b()I

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lsoftware/simplicial/nebulous/f/ag;->c()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->p:Lsoftware/simplicial/a/as;

    move-object/from16 v18, v0

    .line 213
    invoke-virtual/range {v2 .. v18}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/f/bg;ILjava/lang/String;Lsoftware/simplicial/a/ap;IZLjava/lang/String;[BLsoftware/simplicial/a/e;Lsoftware/simplicial/a/af;IBILjava/lang/String;Lsoftware/simplicial/a/as;)V

    .line 219
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/h;->c:Landroid/widget/Button;

    move-object/from16 v0, p1

    if-ne v0, v2, :cond_0

    .line 221
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    goto/16 :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 59
    const v0, 0x7f04002c

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 61
    const v0, 0x7f0d00db

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->b:Landroid/widget/Button;

    .line 62
    const v0, 0x7f0d00a5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->c:Landroid/widget/Button;

    .line 63
    const v0, 0x7f0d00d9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->d:Landroid/widget/TextView;

    .line 64
    const v0, 0x7f0d00da

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->e:Landroid/widget/ListView;

    .line 66
    return-object v1
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 168
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onPause()V

    .line 170
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->g:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    .line 173
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/h;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 179
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->f:Lsoftware/simplicial/nebulous/a/f;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/f;->clear()V

    .line 180
    return-void

    .line 175
    :catch_0
    move-exception v0

    .line 177
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 92
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onResume()V

    .line 94
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->g:Landroid/bluetooth/BluetoothAdapter;

    .line 95
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->g:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->g:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 97
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f080102

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/h;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0802b3

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/h;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0801cf

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/h;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 98
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 108
    :goto_0
    return-void

    .line 102
    :cond_1
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/h;->a()V

    .line 104
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    invoke-static {v0, v1}, Landroid/support/v4/b/b;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    .line 105
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/h;->b()V

    goto :goto_0

    .line 107
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    const v2, 0x7f0801b5

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/h;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 72
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 74
    new-instance v0, Lsoftware/simplicial/nebulous/a/f;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/h;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Lsoftware/simplicial/nebulous/a/f;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->f:Lsoftware/simplicial/nebulous/a/f;

    .line 75
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->e:Landroid/widget/ListView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/h;->f:Lsoftware/simplicial/nebulous/a/f;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 76
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->e:Landroid/widget/ListView;

    new-instance v1, Lsoftware/simplicial/nebulous/application/h$1;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/h$1;-><init>(Lsoftware/simplicial/nebulous/application/h;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 85
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->b:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/h;->c:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    return-void
.end method
