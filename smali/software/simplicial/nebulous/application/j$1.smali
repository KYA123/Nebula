.class Lsoftware/simplicial/nebulous/application/j$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/j;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/application/j;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/j;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/j$1;->a:Lsoftware/simplicial/nebulous/application/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(IILsoftware/simplicial/a/s;I)V
    .locals 5

    .prologue
    const/4 v2, 0x3

    .line 133
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/j$1;->a:Lsoftware/simplicial/nebulous/application/j;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/j;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 162
    :goto_0
    return-void

    .line 136
    :cond_0
    const/4 v0, 0x1

    if-le p1, v0, :cond_2

    .line 138
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 139
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 140
    const/16 v0, 0xd

    invoke-virtual {v1, v0, p2}, Ljava/util/Calendar;->add(II)V

    .line 141
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v2, v2, v0}, Ljava/text/DateFormat;->getDateTimeInstance(IILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v2

    .line 142
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "x "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/j$1;->a:Lsoftware/simplicial/nebulous/application/j;

    const v4, 0x7f080316

    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/j;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 143
    const/4 v3, -0x1

    if-eq p2, v3, :cond_1

    .line 144
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/j$1;->a:Lsoftware/simplicial/nebulous/application/j;

    const v4, 0x7f0803a5

    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/j;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 145
    :cond_1
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/j$1;->a:Lsoftware/simplicial/nebulous/application/j;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/application/j;->a(Lsoftware/simplicial/nebulous/application/j;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/j$1;->a:Lsoftware/simplicial/nebulous/application/j;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/j;->a(Lsoftware/simplicial/nebulous/application/j;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 147
    packed-switch p1, :pswitch_data_0

    .line 156
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/j$1;->a:Lsoftware/simplicial/nebulous/application/j;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/j;->a(Lsoftware/simplicial/nebulous/application/j;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/j$1;->a:Lsoftware/simplicial/nebulous/application/j;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/j;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0055

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 161
    :cond_2
    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/j$1;->a:Lsoftware/simplicial/nebulous/application/j;

    sget-object v1, Lsoftware/simplicial/nebulous/application/bc$a;->b:Lsoftware/simplicial/nebulous/application/bc$a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/j;->a(Lsoftware/simplicial/nebulous/application/bc$a;)V

    goto/16 :goto_0

    .line 150
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/j$1;->a:Lsoftware/simplicial/nebulous/application/j;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/j;->a(Lsoftware/simplicial/nebulous/application/j;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/j$1;->a:Lsoftware/simplicial/nebulous/application/j;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/j;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c005d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 153
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/j$1;->a:Lsoftware/simplicial/nebulous/application/j;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/j;->a(Lsoftware/simplicial/nebulous/application/j;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/j$1;->a:Lsoftware/simplicial/nebulous/application/j;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/j;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c001d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 147
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
