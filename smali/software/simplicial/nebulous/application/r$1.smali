.class Lsoftware/simplicial/nebulous/application/r$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/r;->a(ILandroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/application/r$a;

.field final synthetic b:I

.field final synthetic c:Lsoftware/simplicial/nebulous/application/r;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/r;Lsoftware/simplicial/nebulous/application/r$a;I)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/r$1;->c:Lsoftware/simplicial/nebulous/application/r;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/application/r$1;->a:Lsoftware/simplicial/nebulous/application/r$a;

    iput p3, p0, Lsoftware/simplicial/nebulous/application/r$1;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 89
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/r$1;->a:Lsoftware/simplicial/nebulous/application/r$a;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/application/r$a;->g:Z

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/r$1;->c:Lsoftware/simplicial/nebulous/application/r;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/r;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/r$1;->a:Lsoftware/simplicial/nebulous/application/r$a;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/r$a;->a:Lsoftware/simplicial/a/p;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/p;)V

    .line 117
    :goto_0
    return-void

    .line 95
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/r$1;->c:Lsoftware/simplicial/nebulous/application/r;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/r;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 96
    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 97
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/r$1;->c:Lsoftware/simplicial/nebulous/application/r;

    const v2, 0x7f0800c6

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/r;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 98
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/r$1;->c:Lsoftware/simplicial/nebulous/application/r;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/r;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/r$1;->c:Lsoftware/simplicial/nebulous/application/r;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/r;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/r$1;->c:Lsoftware/simplicial/nebulous/application/r;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/r;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/f/ag;->aa:[B

    invoke-static {v3, v4, v5, v5}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[BZZ)Landroid/text/SpannableString;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/r$1;->a:Lsoftware/simplicial/nebulous/application/r$a;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/r$a;->a:Lsoftware/simplicial/a/p;

    invoke-static {v2, v3, v4}, Lsoftware/simplicial/nebulous/f/aa;->a(Landroid/content/Context;Ljava/lang/CharSequence;Lsoftware/simplicial/a/p;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/r$1;->c:Lsoftware/simplicial/nebulous/application/r;

    const v3, 0x7f0800d3

    .line 100
    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/r;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/r$1;->a:Lsoftware/simplicial/nebulous/application/r$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/r$a;->c:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/r$1;->c:Lsoftware/simplicial/nebulous/application/r;

    const v3, 0x7f08008d

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/r;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/r$1;->c:Lsoftware/simplicial/nebulous/application/r;

    const v3, 0x7f0801e6

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/r;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 98
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 102
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/r$1;->c:Lsoftware/simplicial/nebulous/application/r;

    const v2, 0x7f0801e0

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/r;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/r$1$1;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/r$1$1;-><init>(Lsoftware/simplicial/nebulous/application/r$1;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 114
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/r$1;->c:Lsoftware/simplicial/nebulous/application/r;

    const v2, 0x7f08005d

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/r;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 115
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0
.end method
