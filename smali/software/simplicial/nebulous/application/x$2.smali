.class Lsoftware/simplicial/nebulous/application/x$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$f;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/x;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/application/x;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/x;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/x$2;->a:Lsoftware/simplicial/nebulous/application/x;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/h/f;I)V
    .locals 6

    .prologue
    .line 113
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/x$2;->a:Lsoftware/simplicial/nebulous/application/x;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/x;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 123
    :goto_0
    return-void

    .line 116
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/x$2;->a:Lsoftware/simplicial/nebulous/application/x;

    iput p2, v0, Lsoftware/simplicial/nebulous/application/x;->b:I

    .line 118
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/x$2;->a:Lsoftware/simplicial/nebulous/application/x;

    sget-object v1, Lsoftware/simplicial/nebulous/application/bc$a;->a:Lsoftware/simplicial/nebulous/application/bc$a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/x;->a(Lsoftware/simplicial/nebulous/application/bc$a;)V

    .line 120
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/x$2;->a:Lsoftware/simplicial/nebulous/application/x;

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/x;->e:Landroid/widget/Button;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/x$2;->a:Lsoftware/simplicial/nebulous/application/x;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/x;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->i:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    int-to-long v4, p2

    cmp-long v0, v2, v4

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 122
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/x$2;->a:Lsoftware/simplicial/nebulous/application/x;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/x;->g:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 120
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
