.class Lsoftware/simplicial/nebulous/application/z$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/z;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/application/z;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/z;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/z$2;->a:Lsoftware/simplicial/nebulous/application/z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 197
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z$2;->a:Lsoftware/simplicial/nebulous/application/z;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/z;->b(Lsoftware/simplicial/nebulous/application/z;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 199
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 201
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/z$2;->a:Lsoftware/simplicial/nebulous/application/z;

    iget-wide v2, v2, Lsoftware/simplicial/nebulous/application/z;->B:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/z$2;->a:Lsoftware/simplicial/nebulous/application/z;

    iget-wide v2, v2, Lsoftware/simplicial/nebulous/application/z;->B:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_4

    .line 203
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z$2;->a:Lsoftware/simplicial/nebulous/application/z;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/application/z;->C:Z

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z$2;->a:Lsoftware/simplicial/nebulous/application/z;

    iget v1, v0, Lsoftware/simplicial/nebulous/application/z;->z:F

    add-float/2addr v1, v6

    iput v1, v0, Lsoftware/simplicial/nebulous/application/z;->z:F

    .line 205
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z$2;->a:Lsoftware/simplicial/nebulous/application/z;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/application/z;->D:Z

    if-eqz v0, :cond_1

    .line 206
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z$2;->a:Lsoftware/simplicial/nebulous/application/z;

    iget v1, v0, Lsoftware/simplicial/nebulous/application/z;->z:F

    sub-float/2addr v1, v6

    iput v1, v0, Lsoftware/simplicial/nebulous/application/z;->z:F

    .line 207
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z$2;->a:Lsoftware/simplicial/nebulous/application/z;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/application/z;->F:Z

    if-eqz v0, :cond_2

    .line 208
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z$2;->a:Lsoftware/simplicial/nebulous/application/z;

    iget v1, v0, Lsoftware/simplicial/nebulous/application/z;->y:F

    sub-float/2addr v1, v6

    iput v1, v0, Lsoftware/simplicial/nebulous/application/z;->y:F

    .line 209
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z$2;->a:Lsoftware/simplicial/nebulous/application/z;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/application/z;->E:Z

    if-eqz v0, :cond_3

    .line 210
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z$2;->a:Lsoftware/simplicial/nebulous/application/z;

    iget v1, v0, Lsoftware/simplicial/nebulous/application/z;->y:F

    add-float/2addr v1, v6

    iput v1, v0, Lsoftware/simplicial/nebulous/application/z;->y:F

    .line 212
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z$2;->a:Lsoftware/simplicial/nebulous/application/z;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/z;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 213
    if-eqz v0, :cond_4

    .line 214
    new-instance v1, Lsoftware/simplicial/nebulous/application/z$2$1;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/z$2$1;-><init>(Lsoftware/simplicial/nebulous/application/z$2;)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 226
    :cond_4
    const-wide/16 v0, 0x14

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 228
    :catch_0
    move-exception v0

    goto :goto_0

    .line 233
    :cond_5
    return-void
.end method
