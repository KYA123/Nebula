.class Lsoftware/simplicial/nebulous/application/z$4;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/z;->a(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lsoftware/simplicial/nebulous/application/z;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/z;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 364
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/z$4;->b:Lsoftware/simplicial/nebulous/application/z;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/application/z$4;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    .line 368
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z$4;->b:Lsoftware/simplicial/nebulous/application/z;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/z;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 391
    :goto_0
    return-void

    .line 371
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z$4;->b:Lsoftware/simplicial/nebulous/application/z;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/z;->f:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 372
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z$4;->b:Lsoftware/simplicial/nebulous/application/z;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/z;->g:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 373
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z$4;->b:Lsoftware/simplicial/nebulous/application/z;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/z;->e:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 375
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z$4;->b:Lsoftware/simplicial/nebulous/application/z;

    const/4 v1, 0x0

    invoke-static {v0, v1, v2}, Lsoftware/simplicial/nebulous/application/z;->a(Lsoftware/simplicial/nebulous/application/z;Landroid/graphics/Bitmap;Z)V

    .line 376
    const-string v0, ""

    .line 377
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/z$4;->b:Lsoftware/simplicial/nebulous/application/z;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/application/z;->c(Lsoftware/simplicial/nebulous/application/z;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 379
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z$4;->b:Lsoftware/simplicial/nebulous/application/z;

    const v1, 0x7f080327

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/z;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 381
    :cond_1
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/z$4;->b:Lsoftware/simplicial/nebulous/application/z;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/application/z;->d(Lsoftware/simplicial/nebulous/application/z;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 383
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z$4;->b:Lsoftware/simplicial/nebulous/application/z;

    const v1, 0x7f080328

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/z;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 385
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 386
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/z$4;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 387
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/z$4;->b:Lsoftware/simplicial/nebulous/application/z;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/z;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/z$4;->b:Lsoftware/simplicial/nebulous/application/z;

    const v3, 0x7f080102

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/z;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/z$4;->b:Lsoftware/simplicial/nebulous/application/z;

    const v4, 0x7f0801cf

    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/z;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v0, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 389
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z$4;->b:Lsoftware/simplicial/nebulous/application/z;

    invoke-static {v0, v5}, Lsoftware/simplicial/nebulous/application/z;->a(Lsoftware/simplicial/nebulous/application/z;Z)Z

    .line 390
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z$4;->b:Lsoftware/simplicial/nebulous/application/z;

    invoke-static {v0, v5}, Lsoftware/simplicial/nebulous/application/z;->b(Lsoftware/simplicial/nebulous/application/z;Z)Z

    goto/16 :goto_0
.end method
