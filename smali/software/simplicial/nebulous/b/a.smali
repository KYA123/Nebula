.class public Lsoftware/simplicial/nebulous/b/a;
.super Lsoftware/simplicial/nebulous/b/c;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/a/f/bg;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/nebulous/b/a$a;
    }
.end annotation


# instance fields
.field public a:Lsoftware/simplicial/a/t;

.field private final c:Landroid/bluetooth/BluetoothAdapter;

.field private final d:Landroid/bluetooth/BluetoothDevice;

.field private final e:Lsoftware/simplicial/nebulous/b/b;

.field private f:Lsoftware/simplicial/nebulous/b/a$a;

.field private g:Ljava/io/OutputStream;

.field private h:I

.field private i:I

.field private j:I

.field private k:Lsoftware/simplicial/a/f/aa;

.field private l:Z

.field private m:Lsoftware/simplicial/nebulous/b/e;


# direct methods
.method public constructor <init>(Landroid/bluetooth/BluetoothAdapter;Landroid/bluetooth/BluetoothDevice;Lsoftware/simplicial/nebulous/b/b;Lsoftware/simplicial/a/t;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/b/c;-><init>()V

    .line 45
    iput-object v0, p0, Lsoftware/simplicial/nebulous/b/a;->f:Lsoftware/simplicial/nebulous/b/a$a;

    .line 46
    iput-object v0, p0, Lsoftware/simplicial/nebulous/b/a;->g:Ljava/io/OutputStream;

    .line 47
    iput v1, p0, Lsoftware/simplicial/nebulous/b/a;->h:I

    .line 48
    iput v1, p0, Lsoftware/simplicial/nebulous/b/a;->i:I

    .line 49
    const/4 v0, 0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/b/a;->j:I

    .line 50
    sget-object v0, Lsoftware/simplicial/a/f/aa;->a:Lsoftware/simplicial/a/f/aa;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/b/a;->k:Lsoftware/simplicial/a/f/aa;

    .line 51
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/b/a;->l:Z

    .line 52
    new-instance v0, Lsoftware/simplicial/nebulous/b/e;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/b/e;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/b/a;->m:Lsoftware/simplicial/nebulous/b/e;

    .line 57
    iput-object p1, p0, Lsoftware/simplicial/nebulous/b/a;->c:Landroid/bluetooth/BluetoothAdapter;

    .line 58
    iput-object p2, p0, Lsoftware/simplicial/nebulous/b/a;->d:Landroid/bluetooth/BluetoothDevice;

    .line 59
    iput-object p3, p0, Lsoftware/simplicial/nebulous/b/a;->e:Lsoftware/simplicial/nebulous/b/b;

    .line 60
    iput-object p4, p0, Lsoftware/simplicial/nebulous/b/a;->a:Lsoftware/simplicial/a/t;

    .line 61
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/b/a;Ljava/io/OutputStream;)Ljava/io/OutputStream;
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lsoftware/simplicial/nebulous/b/a;->g:Ljava/io/OutputStream;

    return-object p1
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/b/a;Lsoftware/simplicial/a/f/aa;)Lsoftware/simplicial/a/f/aa;
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lsoftware/simplicial/nebulous/b/a;->k:Lsoftware/simplicial/a/f/aa;

    return-object p1
.end method

.method private a(Lsoftware/simplicial/a/f/ac;)V
    .locals 0

    .prologue
    .line 178
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/b/a;->f()V

    .line 179
    return-void
.end method

.method private a(Lsoftware/simplicial/a/f/br;)V
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a;->e:Lsoftware/simplicial/nebulous/b/b;

    iget-object v1, p1, Lsoftware/simplicial/a/f/br;->b:Lsoftware/simplicial/a/ay;

    invoke-interface {v0, v1}, Lsoftware/simplicial/nebulous/b/b;->a(Lsoftware/simplicial/a/ay;)V

    .line 174
    return-void
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/x;)V
    .locals 5

    .prologue
    .line 194
    monitor-enter p0

    :try_start_0
    iget v0, p1, Lsoftware/simplicial/a/f/x;->ar:I

    iget v1, p0, Lsoftware/simplicial/nebulous/b/a;->j:I

    if-eq v0, v1, :cond_1

    .line 196
    sget-object v0, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v1, "Ignoring %s message for client %d."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, Lsoftware/simplicial/a/f/x;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v4}, Lsoftware/simplicial/a/f/bh;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p1, Lsoftware/simplicial/a/f/x;->ar:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 200
    :cond_1
    :try_start_1
    iget-object v0, p1, Lsoftware/simplicial/a/f/x;->a:Lsoftware/simplicial/a/f/z;

    sget-object v1, Lsoftware/simplicial/a/f/z;->a:Lsoftware/simplicial/a/f/z;

    if-ne v0, v1, :cond_2

    .line 202
    iget v0, p1, Lsoftware/simplicial/a/f/x;->c:I

    iput v0, p0, Lsoftware/simplicial/nebulous/b/a;->h:I

    .line 203
    iget v0, p1, Lsoftware/simplicial/a/f/x;->b:I

    iput v0, p0, Lsoftware/simplicial/nebulous/b/a;->i:I

    .line 206
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a;->a:Lsoftware/simplicial/a/t;

    invoke-virtual {v0, p1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/be;)V

    .line 208
    iget-object v0, p1, Lsoftware/simplicial/a/f/x;->a:Lsoftware/simplicial/a/f/z;

    sget-object v1, Lsoftware/simplicial/a/f/z;->a:Lsoftware/simplicial/a/f/z;

    if-eq v0, v1, :cond_0

    .line 209
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/b/a;->g()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 194
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/y;)V
    .locals 4

    .prologue
    .line 184
    monitor-enter p0

    :try_start_0
    iget v0, p1, Lsoftware/simplicial/a/f/y;->ar:I

    iput v0, p0, Lsoftware/simplicial/nebulous/b/a;->h:I

    .line 185
    iget v0, p1, Lsoftware/simplicial/a/f/y;->a:I

    iput v0, p0, Lsoftware/simplicial/nebulous/b/a;->i:I

    .line 187
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    iget v1, p0, Lsoftware/simplicial/nebulous/b/a;->h:I

    iget v2, p0, Lsoftware/simplicial/nebulous/b/a;->i:I

    iget v3, p0, Lsoftware/simplicial/nebulous/b/a;->j:I

    invoke-static {v0, v1, v2, v3}, Lsoftware/simplicial/a/f/v;->a(Lsoftware/simplicial/a/f/bn;III)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/b/a;->a([B)V

    .line 189
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a;->e:Lsoftware/simplicial/nebulous/b/b;

    invoke-interface {v0}, Lsoftware/simplicial/nebulous/b/b;->w()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 190
    monitor-exit p0

    return-void

    .line 184
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/b/a;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/b/a;->g()V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/b/a;Lsoftware/simplicial/a/f/ac;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/b/a;->a(Lsoftware/simplicial/a/f/ac;)V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/b/a;Lsoftware/simplicial/a/f/br;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/b/a;->a(Lsoftware/simplicial/a/f/br;)V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/b/a;Lsoftware/simplicial/a/f/x;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/b/a;->a(Lsoftware/simplicial/a/f/x;)V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/b/a;Lsoftware/simplicial/a/f/y;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/b/a;->a(Lsoftware/simplicial/a/f/y;)V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/b/a;Z)Z
    .locals 0

    .prologue
    .line 38
    iput-boolean p1, p0, Lsoftware/simplicial/nebulous/b/a;->l:Z

    return p1
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/b/a;)Lsoftware/simplicial/nebulous/b/b;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a;->e:Lsoftware/simplicial/nebulous/b/b;

    return-object v0
.end method

.method static synthetic c(Lsoftware/simplicial/nebulous/b/a;)Lsoftware/simplicial/nebulous/b/e;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a;->m:Lsoftware/simplicial/nebulous/b/e;

    return-object v0
.end method

.method private declared-synchronized g()V
    .locals 4

    .prologue
    .line 148
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/b/a;->b:Z

    .line 150
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a;->f:Lsoftware/simplicial/nebulous/b/a$a;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a;->f:Lsoftware/simplicial/nebulous/b/a$a;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/b/a$a;->a()V

    .line 153
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a;->f:Lsoftware/simplicial/nebulous/b/a$a;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/b/a$a;->interrupt()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    :try_start_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a;->f:Lsoftware/simplicial/nebulous/b/a$a;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Lsoftware/simplicial/nebulous/b/a$a;->join(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 163
    :goto_0
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lsoftware/simplicial/nebulous/b/a;->f:Lsoftware/simplicial/nebulous/b/a$a;

    .line 166
    :cond_0
    sget-object v0, Lsoftware/simplicial/a/f/aa;->a:Lsoftware/simplicial/a/f/aa;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/b/a;->k:Lsoftware/simplicial/a/f/aa;

    .line 168
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a;->e:Lsoftware/simplicial/nebulous/b/b;

    invoke-interface {v0}, Lsoftware/simplicial/nebulous/b/b;->l()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 169
    monitor-exit p0

    return-void

    .line 148
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 158
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 1

    .prologue
    .line 87
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/b/a;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    monitor-exit p0

    return-void

    .line 87
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/util/UUID;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 65
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a;->k:Lsoftware/simplicial/a/f/aa;

    sget-object v1, Lsoftware/simplicial/a/f/aa;->a:Lsoftware/simplicial/a/f/aa;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, v1, :cond_0

    .line 83
    :goto_0
    monitor-exit p0

    return-void

    .line 69
    :cond_0
    :try_start_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a;->c:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    .line 71
    sget-object v0, Lsoftware/simplicial/a/f/aa;->b:Lsoftware/simplicial/a/f/aa;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/b/a;->k:Lsoftware/simplicial/a/f/aa;

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/b/a;->b:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 75
    :try_start_2
    new-instance v0, Lsoftware/simplicial/nebulous/b/a$a;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/b/a;->d:Landroid/bluetooth/BluetoothDevice;

    invoke-direct {v0, p0, v1, p1}, Lsoftware/simplicial/nebulous/b/a$a;-><init>(Lsoftware/simplicial/nebulous/b/a;Landroid/bluetooth/BluetoothDevice;Ljava/util/UUID;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/b/a;->f:Lsoftware/simplicial/nebulous/b/a$a;

    .line 76
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a;->f:Lsoftware/simplicial/nebulous/b/a$a;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/b/a$a;->start()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 78
    :catch_0
    move-exception v0

    .line 80
    :try_start_3
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 81
    const v1, 0x7f080102

    invoke-virtual {p2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f0801cf

    invoke-virtual {p2, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p2, v1, v0, v2}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 65
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a([B)V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a;->g:Ljava/io/OutputStream;

    .line 94
    if-nez v0, :cond_0

    .line 97
    :goto_0
    return-void

    .line 96
    :cond_0
    invoke-virtual {p0, p1, v0}, Lsoftware/simplicial/nebulous/b/a;->a([BLjava/io/OutputStream;)Z

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lsoftware/simplicial/nebulous/b/a;->j:I

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lsoftware/simplicial/nebulous/b/a;->h:I

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lsoftware/simplicial/nebulous/b/a;->i:I

    return v0
.end method

.method public declared-synchronized e()Lsoftware/simplicial/a/f/aa;
    .locals 1

    .prologue
    .line 120
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a;->k:Lsoftware/simplicial/a/f/aa;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized f()V
    .locals 4

    .prologue
    .line 126
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/b/a;->l:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a;->k:Lsoftware/simplicial/a/f/aa;

    sget-object v1, Lsoftware/simplicial/a/f/aa;->a:Lsoftware/simplicial/a/f/aa;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_1

    .line 144
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 129
    :cond_1
    :try_start_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a;->k:Lsoftware/simplicial/a/f/aa;

    sget-object v1, Lsoftware/simplicial/a/f/aa;->c:Lsoftware/simplicial/a/f/aa;

    if-ne v0, v1, :cond_2

    .line 130
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/b/a;->d()I

    move-result v1

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/b/a;->c()I

    move-result v2

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/b/a;->b()I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lsoftware/simplicial/a/f/ac;->a(Lsoftware/simplicial/a/f/bn;III)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/b/a;->a([B)V

    .line 132
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/b/a;->l:Z

    .line 133
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 134
    new-instance v1, Lsoftware/simplicial/nebulous/b/a$1;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/b/a$1;-><init>(Lsoftware/simplicial/nebulous/b/a;)V

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
