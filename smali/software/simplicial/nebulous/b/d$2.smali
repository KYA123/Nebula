.class Lsoftware/simplicial/nebulous/b/d$2;
.super Ljava/util/TimerTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/b/d;->a(Lsoftware/simplicial/a/f/ac;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/a/f/ac;

.field final synthetic b:Lsoftware/simplicial/nebulous/b/d;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/b/d;Lsoftware/simplicial/a/f/ac;)V
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lsoftware/simplicial/nebulous/b/d$2;->b:Lsoftware/simplicial/nebulous/b/d;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/b/d$2;->a:Lsoftware/simplicial/a/f/ac;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 168
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/d$2;->b:Lsoftware/simplicial/nebulous/b/d;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/b/d;->b(Lsoftware/simplicial/nebulous/b/d;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/b/d$2;->a:Lsoftware/simplicial/a/f/ac;

    iget v1, v1, Lsoftware/simplicial/a/f/ac;->ar:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothSocket;

    .line 169
    if-eqz v0, :cond_0

    .line 173
    :try_start_0
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    :goto_0
    iget-object v1, p0, Lsoftware/simplicial/nebulous/b/d$2;->b:Lsoftware/simplicial/nebulous/b/d;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/b/d;->c(Lsoftware/simplicial/nebulous/b/d;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Thread;

    .line 181
    if-eqz v0, :cond_0

    .line 183
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 186
    const-wide/16 v2, 0xfa

    :try_start_1
    invoke-virtual {v0, v2, v3}, Ljava/lang/Thread;->join(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 194
    :cond_0
    :goto_1
    return-void

    .line 175
    :catch_0
    move-exception v1

    .line 177
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 188
    :catch_1
    move-exception v0

    goto :goto_1
.end method
