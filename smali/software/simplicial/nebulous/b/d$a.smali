.class Lsoftware/simplicial/nebulous/b/d$a;
.super Ljava/lang/Thread;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsoftware/simplicial/nebulous/b/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/b/d;

.field private final b:Landroid/bluetooth/BluetoothServerSocket;


# direct methods
.method public constructor <init>(Lsoftware/simplicial/nebulous/b/d;Ljava/util/UUID;)V
    .locals 2

    .prologue
    .line 301
    iput-object p1, p0, Lsoftware/simplicial/nebulous/b/d$a;->a:Lsoftware/simplicial/nebulous/b/d;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 306
    invoke-static {p1}, Lsoftware/simplicial/nebulous/b/d;->d(Lsoftware/simplicial/nebulous/b/d;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    const-string v1, "NEBULOUS"

    invoke-virtual {v0, v1, p2}, Landroid/bluetooth/BluetoothAdapter;->listenUsingRfcommWithServiceRecord(Ljava/lang/String;Ljava/util/UUID;)Landroid/bluetooth/BluetoothServerSocket;

    move-result-object v0

    .line 307
    iput-object v0, p0, Lsoftware/simplicial/nebulous/b/d$a;->b:Landroid/bluetooth/BluetoothServerSocket;

    .line 308
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 344
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/d$a;->b:Landroid/bluetooth/BluetoothServerSocket;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothServerSocket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 349
    :goto_0
    return-void

    .line 346
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public run()V
    .locals 3

    .prologue
    .line 312
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    const-string v1, "BTS Accept"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 314
    iget-object v1, p0, Lsoftware/simplicial/nebulous/b/d$a;->a:Lsoftware/simplicial/nebulous/b/d;

    monitor-enter v1

    .line 316
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/d$a;->a:Lsoftware/simplicial/nebulous/b/d;

    const/4 v2, 0x0

    iput-boolean v2, v0, Lsoftware/simplicial/nebulous/b/d;->b:Z

    .line 317
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 321
    :cond_0
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/d$a;->a:Lsoftware/simplicial/nebulous/b/d;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/b/d;->b:Z

    if-nez v0, :cond_1

    .line 325
    :try_start_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/d$a;->b:Landroid/bluetooth/BluetoothServerSocket;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothServerSocket;->accept()Landroid/bluetooth/BluetoothSocket;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    .line 332
    if-eqz v0, :cond_0

    .line 333
    iget-object v1, p0, Lsoftware/simplicial/nebulous/b/d$a;->a:Lsoftware/simplicial/nebulous/b/d;

    invoke-static {v1, v0}, Lsoftware/simplicial/nebulous/b/d;->a(Lsoftware/simplicial/nebulous/b/d;Landroid/bluetooth/BluetoothSocket;)V

    goto :goto_0

    .line 317
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 327
    :catch_0
    move-exception v0

    .line 335
    :cond_1
    return-void
.end method
