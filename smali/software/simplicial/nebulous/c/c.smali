.class public Lsoftware/simplicial/nebulous/c/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Long;

.field public b:Landroid/graphics/PointF;


# direct methods
.method public constructor <init>(JLandroid/graphics/PointF;)V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/c/c;->a:Ljava/lang/Long;

    .line 16
    iput-object p3, p0, Lsoftware/simplicial/nebulous/c/c;->b:Landroid/graphics/PointF;

    .line 17
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 28
    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    .line 31
    :goto_0
    return v0

    .line 29
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 30
    :cond_2
    check-cast p1, Lsoftware/simplicial/nebulous/c/c;

    .line 31
    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/c;->b:Landroid/graphics/PointF;

    iget-object v1, p1, Lsoftware/simplicial/nebulous/c/c;->b:Landroid/graphics/PointF;

    invoke-virtual {v0, v1}, Landroid/graphics/PointF;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/c;->b:Landroid/graphics/PointF;

    invoke-virtual {v0}, Landroid/graphics/PointF;->hashCode()I

    move-result v0

    return v0
.end method
