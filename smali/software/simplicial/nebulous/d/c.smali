.class public Lsoftware/simplicial/nebulous/d/c;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/content/Context;)J
    .locals 8

    .prologue
    const-wide/16 v0, -0x1

    const/16 v7, 0x27

    const/4 v2, 0x0

    .line 22
    invoke-static {}, Lsoftware/simplicial/nebulous/f/s;->b()Z

    move-result v3

    if-nez v3, :cond_1

    .line 59
    :cond_0
    :goto_0
    return-wide v0

    .line 29
    :cond_1
    :try_start_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    .line 31
    new-instance v4, Ljava/io/File;

    const-string v5, "//Nebulous//"

    invoke-direct {v4, v3, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 32
    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    .line 34
    new-instance v4, Ljava/io/File;

    const-string v5, "//Nebulous//stats.db"

    invoke-direct {v4, v3, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 35
    invoke-virtual {v4}, Ljava/io/File;->exists()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_3

    .line 63
    if-eqz v2, :cond_2

    .line 64
    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/d/d;->close()V

    .line 65
    :cond_2
    if-eqz v2, :cond_0

    .line 66
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto :goto_0

    .line 38
    :cond_3
    :try_start_1
    new-instance v3, Lsoftware/simplicial/nebulous/d/d;

    invoke-direct {v3, p0}, Lsoftware/simplicial/nebulous/d/d;-><init>(Landroid/content/Context;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 39
    :try_start_2
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v5, 0x1

    new-instance v6, Lsoftware/simplicial/nebulous/d/c$1;

    invoke-direct {v6, v4}, Lsoftware/simplicial/nebulous/d/c$1;-><init>(Ljava/io/File;)V

    invoke-static {v0, v1, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;ILandroid/database/DatabaseErrorHandler;)Landroid/database/sqlite/SQLiteDatabase;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v1

    .line 50
    :try_start_3
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->getVersion()I

    move-result v0

    if-ge v0, v7, :cond_4

    .line 52
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 54
    :try_start_4
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v1

    .line 55
    :try_start_5
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->getVersion()I

    move-result v0

    const/16 v2, 0x27

    invoke-virtual {v3, v1, v0, v2}, Lsoftware/simplicial/nebulous/d/d;->onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 56
    const/16 v0, 0x27

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->setVersion(I)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :cond_4
    move-object v2, v1

    .line 58
    const/4 v0, 0x0

    :try_start_6
    invoke-virtual {v3, v0, v2}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/am;Landroid/database/sqlite/SQLiteDatabase;)Lsoftware/simplicial/a/az;

    move-result-object v0

    .line 59
    iget-wide v0, v0, Lsoftware/simplicial/a/az;->b:J
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 63
    if-eqz v3, :cond_5

    .line 64
    invoke-virtual {v3}, Lsoftware/simplicial/nebulous/d/d;->close()V

    .line 65
    :cond_5
    if-eqz v2, :cond_0

    .line 66
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto :goto_0

    .line 63
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_1
    if-eqz v2, :cond_6

    .line 64
    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/d/d;->close()V

    .line 65
    :cond_6
    if-eqz v1, :cond_7

    .line 66
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_7
    throw v0

    .line 63
    :catchall_1
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_1

    :catchall_2
    move-exception v0

    move-object v2, v3

    goto :goto_1

    :catchall_3
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_1
.end method

.method public static a()Z
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 72
    invoke-static {}, Lsoftware/simplicial/nebulous/f/s;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 102
    :goto_0
    return v0

    .line 77
    :cond_0
    :try_start_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    .line 78
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v2

    .line 80
    new-instance v3, Ljava/io/File;

    const-string v4, "//Nebulous//stats.db"

    invoke-direct {v3, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 81
    new-instance v4, Ljava/io/File;

    const-string v5, "//data//software.simplicial.nebulous//databases//stats.db"

    invoke-direct {v4, v2, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 82
    new-instance v5, Ljava/io/File;

    const-string v6, "//data//software.simplicial.nebulous//databases//stats_temp.db"

    invoke-direct {v5, v2, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 84
    new-instance v6, Ljava/io/File;

    const-string v7, "//Nebulous//achievements.db"

    invoke-direct {v6, v1, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 85
    new-instance v1, Ljava/io/File;

    const-string v7, "//data//software.simplicial.nebulous//databases//achievements.db"

    invoke-direct {v1, v2, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 86
    new-instance v7, Ljava/io/File;

    const-string v8, "//data//software.simplicial.nebulous//databases//achievements_temp.db"

    invoke-direct {v7, v2, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 88
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 89
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    .line 91
    invoke-virtual {v4, v5}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 92
    invoke-virtual {v1, v7}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 94
    invoke-static {v3, v4}, Lsoftware/simplicial/nebulous/f/s;->a(Ljava/io/File;Ljava/io/File;)V

    .line 95
    invoke-static {v6, v1}, Lsoftware/simplicial/nebulous/f/s;->a(Ljava/io/File;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    const/4 v0, 0x1

    goto :goto_0

    .line 97
    :catch_0
    move-exception v1

    .line 99
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public static b()Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 107
    invoke-static {}, Lsoftware/simplicial/nebulous/f/s;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 139
    :goto_0
    return v0

    .line 112
    :cond_0
    :try_start_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    .line 113
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v2

    .line 115
    new-instance v3, Ljava/io/File;

    const-string v4, "//Nebulous//stats.db"

    invoke-direct {v3, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 116
    new-instance v4, Ljava/io/File;

    const-string v5, "//data//software.simplicial.nebulous//databases//stats.db"

    invoke-direct {v4, v2, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 118
    new-instance v5, Ljava/io/File;

    const-string v6, "//Nebulous//achievements.db"

    invoke-direct {v5, v1, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 119
    new-instance v6, Ljava/io/File;

    const-string v7, "//data//software.simplicial.nebulous//databases//achievements.db"

    invoke-direct {v6, v2, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 121
    new-instance v2, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, "//Nebulous//"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 123
    invoke-virtual {v2}, Ljava/io/File;->mkdir()Z

    .line 125
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 126
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    .line 127
    invoke-static {v4, v3}, Lsoftware/simplicial/nebulous/f/s;->a(Ljava/io/File;Ljava/io/File;)V

    .line 129
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 130
    invoke-virtual {v5}, Ljava/io/File;->createNewFile()Z

    .line 131
    invoke-static {v6, v5}, Lsoftware/simplicial/nebulous/f/s;->a(Ljava/io/File;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 139
    const/4 v0, 0x1

    goto :goto_0

    .line 133
    :catch_0
    move-exception v1

    .line 135
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method
