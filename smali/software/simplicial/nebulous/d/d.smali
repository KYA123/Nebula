.class public Lsoftware/simplicial/nebulous/d/d;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 109
    const-string v0, "stats.db"

    const/4 v1, 0x0

    const/16 v2, 0x27

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 110
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;[BI)Landroid/content/ContentValues;
    .locals 3

    .prologue
    .line 1237
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1238
    const-string v1, "gameMode"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1239
    const-string v1, "playerName"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1240
    const-string v1, "playerNameColors"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1241
    const-string v1, "score"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1242
    return-object v0
.end method

.method private a(Ljava/lang/String;Lsoftware/simplicial/a/az;)Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 672
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 673
    const-string v1, "gameMode"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 674
    const-string v1, "averageScore"

    iget v2, p2, Lsoftware/simplicial/a/az;->w:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 675
    const-string v1, "bhCollidedCount"

    iget v2, p2, Lsoftware/simplicial/a/az;->H:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 676
    const-string v1, "biggestBlob"

    iget v2, p2, Lsoftware/simplicial/a/az;->u:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 677
    const-string v1, "blobsEaten"

    iget v2, p2, Lsoftware/simplicial/a/az;->q:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 678
    const-string v1, "blobsLost"

    iget v2, p2, Lsoftware/simplicial/a/az;->r:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 679
    const-string v1, "dotsEaten"

    iget v2, p2, Lsoftware/simplicial/a/az;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 680
    const-string v1, "ejectCount"

    iget v2, p2, Lsoftware/simplicial/a/az;->D:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 681
    const-string v1, "massEjected"

    iget-wide v2, p2, Lsoftware/simplicial/a/az;->t:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 682
    const-string v1, "massGained"

    iget-wide v2, p2, Lsoftware/simplicial/a/az;->s:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 683
    const-string v1, "smbhEatenCount"

    iget v2, p2, Lsoftware/simplicial/a/az;->F:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 684
    const-string v1, "smbhCollidedCount"

    iget v2, p2, Lsoftware/simplicial/a/az;->G:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 685
    const-string v1, "longestLife"

    iget-wide v2, p2, Lsoftware/simplicial/a/az;->y:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 686
    const-string v1, "gamesWon"

    iget v2, p2, Lsoftware/simplicial/a/az;->z:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 687
    const-string v1, "XP"

    iget-wide v2, p2, Lsoftware/simplicial/a/az;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 688
    const-string v1, "highestScore"

    iget v2, p2, Lsoftware/simplicial/a/az;->v:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 689
    const-string v1, "timesRestarted"

    iget v2, p2, Lsoftware/simplicial/a/az;->x:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 690
    const-string v1, "splitCount"

    iget v2, p2, Lsoftware/simplicial/a/az;->E:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 691
    const-string v1, "pumpkinsEaten"

    iget v2, p2, Lsoftware/simplicial/a/az;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 692
    const-string v1, "leavesEaten"

    iget v2, p2, Lsoftware/simplicial/a/az;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 693
    const-string v1, "presentsEaten"

    iget v2, p2, Lsoftware/simplicial/a/az;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 694
    const-string v1, "coinsCollected"

    iget v2, p2, Lsoftware/simplicial/a/az;->p:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 695
    const-string v1, "snowflakesEaten"

    iget v2, p2, Lsoftware/simplicial/a/az;->g:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 696
    const-string v1, "beadsEaten"

    iget v2, p2, Lsoftware/simplicial/a/az;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 697
    const-string v1, "eggsEaten"

    iget v2, p2, Lsoftware/simplicial/a/az;->i:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 698
    const-string v1, "tbhCollidedCount"

    iget v2, p2, Lsoftware/simplicial/a/az;->I:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 699
    const-string v1, "timesTeleported"

    iget v2, p2, Lsoftware/simplicial/a/az;->J:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 700
    const-string v1, "powerupsUsed"

    iget v2, p2, Lsoftware/simplicial/a/az;->K:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 701
    const-string v1, "dropsEaten"

    iget v2, p2, Lsoftware/simplicial/a/az;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 702
    const-string v1, "nebulasEaten"

    iget v2, p2, Lsoftware/simplicial/a/az;->k:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 703
    const-string v1, "candiesEaten"

    iget v2, p2, Lsoftware/simplicial/a/az;->l:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 704
    const-string v1, "sunsEaten"

    iget v2, p2, Lsoftware/simplicial/a/az;->m:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 705
    const-string v1, "moonsEaten"

    iget v2, p2, Lsoftware/simplicial/a/az;->n:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 706
    const-string v1, "notesEaten"

    iget v2, p2, Lsoftware/simplicial/a/az;->o:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 707
    return-object v0
.end method

.method private a(Ljava/lang/String;)Lsoftware/simplicial/a/am;
    .locals 2

    .prologue
    .line 994
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 1035
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Failed to convert gameModeString to GameMode"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 994
    :sswitch_0
    const-string v1, "ALL"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v1, "FFA_TIME"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v1, "FFA"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v1, "FFA_ULTRA"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string v1, "ZA"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    :sswitch_5
    const-string v1, "FFA_CLASSIC"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x5

    goto :goto_0

    :sswitch_6
    const-string v1, "TEAMS"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x6

    goto :goto_0

    :sswitch_7
    const-string v1, "TEAMS_TIME"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x7

    goto :goto_0

    :sswitch_8
    const-string v1, "CTF"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x8

    goto :goto_0

    :sswitch_9
    const-string v1, "SURVIVAL"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x9

    goto :goto_0

    :sswitch_a
    const-string v1, "SOCCER"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0xa

    goto :goto_0

    :sswitch_b
    const-string v1, "DOMINATION"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0xb

    goto/16 :goto_0

    :sswitch_c
    const-string v1, "PAINT"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0xc

    goto/16 :goto_0

    :sswitch_d
    const-string v1, "TEAM_DEATHMATCH"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0xd

    goto/16 :goto_0

    :sswitch_e
    const-string v1, "X"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0xe

    goto/16 :goto_0

    :sswitch_f
    const-string v1, "X2"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0xf

    goto/16 :goto_0

    :sswitch_10
    const-string v1, "X3"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x10

    goto/16 :goto_0

    :sswitch_11
    const-string v1, "X4"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x11

    goto/16 :goto_0

    :sswitch_12
    const-string v1, "X5"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x12

    goto/16 :goto_0

    .line 997
    :pswitch_0
    const/4 v0, 0x0

    .line 1033
    :goto_1
    return-object v0

    .line 999
    :pswitch_1
    sget-object v0, Lsoftware/simplicial/a/am;->b:Lsoftware/simplicial/a/am;

    goto :goto_1

    .line 1001
    :pswitch_2
    sget-object v0, Lsoftware/simplicial/a/am;->a:Lsoftware/simplicial/a/am;

    goto :goto_1

    .line 1003
    :pswitch_3
    sget-object v0, Lsoftware/simplicial/a/am;->j:Lsoftware/simplicial/a/am;

    goto :goto_1

    .line 1005
    :pswitch_4
    sget-object v0, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    goto :goto_1

    .line 1007
    :pswitch_5
    sget-object v0, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    goto :goto_1

    .line 1009
    :pswitch_6
    sget-object v0, Lsoftware/simplicial/a/am;->c:Lsoftware/simplicial/a/am;

    goto :goto_1

    .line 1011
    :pswitch_7
    sget-object v0, Lsoftware/simplicial/a/am;->d:Lsoftware/simplicial/a/am;

    goto :goto_1

    .line 1013
    :pswitch_8
    sget-object v0, Lsoftware/simplicial/a/am;->e:Lsoftware/simplicial/a/am;

    goto :goto_1

    .line 1015
    :pswitch_9
    sget-object v0, Lsoftware/simplicial/a/am;->f:Lsoftware/simplicial/a/am;

    goto :goto_1

    .line 1017
    :pswitch_a
    sget-object v0, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    goto :goto_1

    .line 1019
    :pswitch_b
    sget-object v0, Lsoftware/simplicial/a/am;->i:Lsoftware/simplicial/a/am;

    goto :goto_1

    .line 1021
    :pswitch_c
    sget-object v0, Lsoftware/simplicial/a/am;->l:Lsoftware/simplicial/a/am;

    goto :goto_1

    .line 1023
    :pswitch_d
    sget-object v0, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    goto :goto_1

    .line 1025
    :pswitch_e
    sget-object v0, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    goto :goto_1

    .line 1027
    :pswitch_f
    sget-object v0, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    goto :goto_1

    .line 1029
    :pswitch_10
    sget-object v0, Lsoftware/simplicial/a/am;->p:Lsoftware/simplicial/a/am;

    goto :goto_1

    .line 1031
    :pswitch_11
    sget-object v0, Lsoftware/simplicial/a/am;->q:Lsoftware/simplicial/a/am;

    goto :goto_1

    .line 1033
    :pswitch_12
    sget-object v0, Lsoftware/simplicial/a/am;->r:Lsoftware/simplicial/a/am;

    goto :goto_1

    .line 994
    :sswitch_data_0
    .sparse-switch
        -0x7e290a92 -> :sswitch_b
        -0x6de50a97 -> :sswitch_a
        -0x6dc3750d -> :sswitch_d
        -0x30c104c2 -> :sswitch_9
        -0x504802a -> :sswitch_7
        0x58 -> :sswitch_e
        0xada -> :sswitch_f
        0xadb -> :sswitch_10
        0xadc -> :sswitch_11
        0xadd -> :sswitch_12
        0xb27 -> :sswitch_4
        0xfd81 -> :sswitch_0
        0x105f5 -> :sswitch_8
        0x10f81 -> :sswitch_2
        0x486003e -> :sswitch_c
        0x4c01196 -> :sswitch_6
        0x7abad4e -> :sswitch_3
        0x123a69b4 -> :sswitch_5
        0x52d37ccb -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
    .end packed-switch
.end method

.method private a(I)Lsoftware/simplicial/a/d;
    .locals 3

    .prologue
    .line 1093
    sget-object v0, Lsoftware/simplicial/a/d;->bt:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/d;

    .line 1094
    iget-short v2, v0, Lsoftware/simplicial/a/d;->bw:S

    if-ne v2, p1, :cond_0

    .line 1096
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z
    .locals 4

    .prologue
    .line 643
    invoke-direct {p0, p2}, Lsoftware/simplicial/nebulous/d/d;->b(Lsoftware/simplicial/a/am;)Ljava/lang/String;

    move-result-object v0

    .line 645
    invoke-direct {p0, v0, p3}, Lsoftware/simplicial/nebulous/d/d;->a(Ljava/lang/String;Lsoftware/simplicial/a/az;)Landroid/content/ContentValues;

    move-result-object v0

    .line 647
    const-string v1, "single_player_stats"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 649
    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lsoftware/simplicial/a/af;)Landroid/content/ContentValues;
    .locals 3

    .prologue
    .line 719
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 720
    const-string v1, "purchasedEjectSkins"

    iget-byte v2, p1, Lsoftware/simplicial/a/af;->c:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Byte;)V

    .line 721
    return-object v0
.end method

.method private b(Lsoftware/simplicial/a/as;)Landroid/content/ContentValues;
    .locals 3

    .prologue
    .line 726
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 727
    const-string v1, "purchasedHats"

    iget-byte v2, p1, Lsoftware/simplicial/a/as;->c:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Byte;)V

    .line 728
    return-object v0
.end method

.method private b(Lsoftware/simplicial/a/bd;)Landroid/content/ContentValues;
    .locals 3

    .prologue
    .line 733
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 734
    const-string v1, "purchasedPets"

    iget-byte v2, p1, Lsoftware/simplicial/a/bd;->c:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Byte;)V

    .line 735
    return-object v0
.end method

.method private b(Lsoftware/simplicial/a/d;)Landroid/content/ContentValues;
    .locals 3

    .prologue
    .line 1086
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1087
    const-string v1, "AchieveID"

    iget-short v2, p1, Lsoftware/simplicial/a/d;->bw:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    .line 1088
    return-object v0
.end method

.method private b(Lsoftware/simplicial/a/e;)Landroid/content/ContentValues;
    .locals 3

    .prologue
    .line 712
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 713
    const-string v1, "purchasedAvatars"

    invoke-virtual {p1}, Lsoftware/simplicial/a/e;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 714
    return-object v0
.end method

.method private b(Lsoftware/simplicial/a/am;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 946
    if-nez p1, :cond_0

    .line 947
    const-string v0, "ALL"

    .line 985
    :goto_0
    return-object v0

    .line 948
    :cond_0
    sget-object v0, Lsoftware/simplicial/nebulous/d/d$1;->a:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/am;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 987
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Failed to convert gameMode to String"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 951
    :pswitch_0
    const-string v0, "FFA_TIME"

    goto :goto_0

    .line 953
    :pswitch_1
    const-string v0, "FFA"

    goto :goto_0

    .line 955
    :pswitch_2
    const-string v0, "FFA_ULTRA"

    goto :goto_0

    .line 957
    :pswitch_3
    const-string v0, "ZA"

    goto :goto_0

    .line 959
    :pswitch_4
    const-string v0, "FFA CLASSIC"

    goto :goto_0

    .line 961
    :pswitch_5
    const-string v0, "TEAMS"

    goto :goto_0

    .line 963
    :pswitch_6
    const-string v0, "TEAMS_TIME"

    goto :goto_0

    .line 965
    :pswitch_7
    const-string v0, "CTF"

    goto :goto_0

    .line 967
    :pswitch_8
    const-string v0, "SURVIVAL"

    goto :goto_0

    .line 969
    :pswitch_9
    const-string v0, "SOCCER"

    goto :goto_0

    .line 971
    :pswitch_a
    const-string v0, "DOMINATION"

    goto :goto_0

    .line 973
    :pswitch_b
    const-string v0, "PAINT"

    goto :goto_0

    .line 975
    :pswitch_c
    const-string v0, "TEAM_DEATHMATCH"

    goto :goto_0

    .line 977
    :pswitch_d
    const-string v0, "X"

    goto :goto_0

    .line 979
    :pswitch_e
    const-string v0, "X2"

    goto :goto_0

    .line 981
    :pswitch_f
    const-string v0, "X3"

    goto :goto_0

    .line 983
    :pswitch_10
    const-string v0, "X4"

    goto :goto_0

    .line 985
    :pswitch_11
    const-string v0, "X5"

    goto :goto_0

    .line 948
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 792
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 794
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/d/d;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 796
    const-string v0, "SELECT * FROM single_player_purchased_avatars"

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 800
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 806
    :cond_0
    :try_start_1
    sget-object v0, Lsoftware/simplicial/a/e;->a:[Lsoftware/simplicial/a/e;

    const-string v4, "purchasedAvatars"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    aget-object v0, v0, v4

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 813
    :goto_0
    :try_start_2
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 819
    :cond_1
    if-eqz v3, :cond_2

    .line 820
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 821
    :cond_2
    if-eqz v2, :cond_3

    .line 822
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 825
    :cond_3
    return-object v1

    .line 808
    :catch_0
    move-exception v0

    .line 810
    :try_start_3
    sget-object v4, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 819
    :catchall_0
    move-exception v0

    if-eqz v3, :cond_4

    .line 820
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 821
    :cond_4
    if-eqz v2, :cond_5

    .line 822
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_5
    throw v0
.end method

.method public a(Lsoftware/simplicial/a/am;ZII)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsoftware/simplicial/a/am;",
            "ZII)",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/g/c;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v8, 0x0

    .line 1158
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/d/d;->b(Lsoftware/simplicial/a/am;)Ljava/lang/String;

    move-result-object v0

    .line 1159
    if-eqz p2, :cond_0

    .line 1160
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_MAYHEM"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1162
    :cond_0
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1164
    const/4 v1, 0x3

    new-array v2, v1, [Ljava/lang/String;

    const-string v1, "playerName"

    aput-object v1, v2, v5

    const-string v1, "playerNameColors"

    aput-object v1, v2, v4

    const/4 v1, 0x2

    const-string v3, "score"

    aput-object v3, v2, v1

    .line 1170
    const-string v3, "gameMode=?"

    .line 1171
    new-array v4, v4, [Ljava/lang/String;

    aput-object v0, v4, v5

    .line 1177
    :try_start_0
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/d/d;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1178
    :try_start_1
    const-string v1, "single_player_high_scores"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    .line 1188
    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1192
    :cond_1
    new-instance v3, Lsoftware/simplicial/a/g/c;

    invoke-direct {v3}, Lsoftware/simplicial/a/g/c;-><init>()V

    .line 1194
    const-string v1, "playerName"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v4

    .line 1198
    :try_start_3
    const-string v1, "playerNameColors"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getBlob(I)[B
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v1

    .line 1204
    :goto_0
    if-nez v1, :cond_2

    .line 1205
    const/4 v1, 0x0

    :try_start_4
    new-array v1, v1, [B

    .line 1206
    :cond_2
    invoke-static {v4, v1}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[B)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v3, Lsoftware/simplicial/a/g/c;->a:Ljava/lang/CharSequence;

    .line 1207
    const-string v1, "score"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v3, Lsoftware/simplicial/a/g/c;->b:I

    .line 1209
    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1211
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result v1

    if-nez v1, :cond_1

    .line 1216
    :cond_3
    if-eqz v2, :cond_4

    .line 1217
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1218
    :cond_4
    if-eqz v0, :cond_5

    .line 1219
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 1222
    :cond_5
    invoke-static {v9}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 1224
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move v0, p3

    .line 1225
    :goto_1
    add-int v2, p3, p4

    if-ge v0, v2, :cond_9

    .line 1227
    if-ltz v0, :cond_6

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_6

    .line 1228
    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1225
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1200
    :catch_0
    move-exception v1

    .line 1202
    :try_start_5
    sget-object v5, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-object v1, v8

    goto :goto_0

    .line 1216
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_2
    if-eqz v1, :cond_7

    .line 1217
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1218
    :cond_7
    if-eqz v8, :cond_8

    .line 1219
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_8
    throw v0

    .line 1231
    :cond_9
    return-object v1

    .line 1216
    :catchall_1
    move-exception v1

    move-object v10, v1

    move-object v1, v8

    move-object v8, v0

    move-object v0, v10

    goto :goto_2

    :catchall_2
    move-exception v1

    move-object v8, v0

    move-object v0, v1

    move-object v1, v2

    goto :goto_2
.end method

.method public a(Lsoftware/simplicial/a/am;)Lsoftware/simplicial/a/az;
    .locals 4

    .prologue
    .line 477
    const/4 v1, 0x0

    .line 480
    :try_start_0
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/d/d;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 481
    invoke-virtual {p0, p1, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/am;Landroid/database/sqlite/SQLiteDatabase;)Lsoftware/simplicial/a/az;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 494
    if-eqz v1, :cond_0

    .line 495
    :try_start_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 488
    :cond_0
    :goto_0
    return-object v0

    .line 497
    :catch_0
    move-exception v1

    .line 499
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 483
    :catch_1
    move-exception v0

    .line 485
    :try_start_2
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 486
    new-instance v0, Lsoftware/simplicial/a/az;

    invoke-direct {v0}, Lsoftware/simplicial/a/az;-><init>()V

    .line 487
    iput-object p1, v0, Lsoftware/simplicial/a/az;->a:Lsoftware/simplicial/a/am;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 494
    if-eqz v1, :cond_0

    .line 495
    :try_start_3
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 497
    :catch_2
    move-exception v1

    .line 499
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 492
    :catchall_0
    move-exception v0

    .line 494
    if-eqz v1, :cond_1

    .line 495
    :try_start_4
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 500
    :cond_1
    :goto_1
    throw v0

    .line 497
    :catch_3
    move-exception v1

    .line 499
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public a(Lsoftware/simplicial/a/am;Landroid/database/sqlite/SQLiteDatabase;)Lsoftware/simplicial/a/az;
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v8, -0x1

    .line 506
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/d/d;->b(Lsoftware/simplicial/a/am;)Ljava/lang/String;

    move-result-object v0

    .line 510
    const/16 v1, 0x22

    new-array v2, v1, [Ljava/lang/String;

    const-string v1, "gameMode"

    aput-object v1, v2, v6

    const-string v1, "averageScore"

    aput-object v1, v2, v4

    const/4 v1, 0x2

    const-string v3, "bhCollidedCount"

    aput-object v3, v2, v1

    const/4 v1, 0x3

    const-string v3, "biggestBlob"

    aput-object v3, v2, v1

    const/4 v1, 0x4

    const-string v3, "blobsEaten"

    aput-object v3, v2, v1

    const/4 v1, 0x5

    const-string v3, "blobsLost"

    aput-object v3, v2, v1

    const/4 v1, 0x6

    const-string v3, "dotsEaten"

    aput-object v3, v2, v1

    const/4 v1, 0x7

    const-string v3, "ejectCount"

    aput-object v3, v2, v1

    const/16 v1, 0x8

    const-string v3, "massEjected"

    aput-object v3, v2, v1

    const/16 v1, 0x9

    const-string v3, "massGained"

    aput-object v3, v2, v1

    const/16 v1, 0xa

    const-string v3, "smbhEatenCount"

    aput-object v3, v2, v1

    const/16 v1, 0xb

    const-string v3, "smbhCollidedCount"

    aput-object v3, v2, v1

    const/16 v1, 0xc

    const-string v3, "longestLife"

    aput-object v3, v2, v1

    const/16 v1, 0xd

    const-string v3, "gamesWon"

    aput-object v3, v2, v1

    const/16 v1, 0xe

    const-string v3, "XP"

    aput-object v3, v2, v1

    const/16 v1, 0xf

    const-string v3, "highestScore"

    aput-object v3, v2, v1

    const/16 v1, 0x10

    const-string v3, "timesRestarted"

    aput-object v3, v2, v1

    const/16 v1, 0x11

    const-string v3, "splitCount"

    aput-object v3, v2, v1

    const/16 v1, 0x12

    const-string v3, "pumpkinsEaten"

    aput-object v3, v2, v1

    const/16 v1, 0x13

    const-string v3, "leavesEaten"

    aput-object v3, v2, v1

    const/16 v1, 0x14

    const-string v3, "coinsCollected"

    aput-object v3, v2, v1

    const/16 v1, 0x15

    const-string v3, "presentsEaten"

    aput-object v3, v2, v1

    const/16 v1, 0x16

    const-string v3, "snowflakesEaten"

    aput-object v3, v2, v1

    const/16 v1, 0x17

    const-string v3, "beadsEaten"

    aput-object v3, v2, v1

    const/16 v1, 0x18

    const-string v3, "eggsEaten"

    aput-object v3, v2, v1

    const/16 v1, 0x19

    const-string v3, "tbhCollidedCount"

    aput-object v3, v2, v1

    const/16 v1, 0x1a

    const-string v3, "timesTeleported"

    aput-object v3, v2, v1

    const/16 v1, 0x1b

    const-string v3, "powerupsUsed"

    aput-object v3, v2, v1

    const/16 v1, 0x1c

    const-string v3, "dropsEaten"

    aput-object v3, v2, v1

    const/16 v1, 0x1d

    const-string v3, "nebulasEaten"

    aput-object v3, v2, v1

    const/16 v1, 0x1e

    const-string v3, "candiesEaten"

    aput-object v3, v2, v1

    const/16 v1, 0x1f

    const-string v3, "sunsEaten"

    aput-object v3, v2, v1

    const/16 v1, 0x20

    const-string v3, "moonsEaten"

    aput-object v3, v2, v1

    const/16 v1, 0x21

    const-string v3, "notesEaten"

    aput-object v3, v2, v1

    .line 547
    const-string v3, "gameMode=?"

    .line 548
    new-array v4, v4, [Ljava/lang/String;

    aput-object v0, v4, v6

    .line 550
    const-string v1, "single_player_stats"

    move-object v0, p2

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 560
    new-instance v0, Lsoftware/simplicial/a/az;

    invoke-direct {v0}, Lsoftware/simplicial/a/az;-><init>()V

    .line 564
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 566
    const-string v2, "gameMode"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lsoftware/simplicial/nebulous/d/d;->a(Ljava/lang/String;)Lsoftware/simplicial/a/am;

    move-result-object v2

    iput-object v2, v0, Lsoftware/simplicial/a/az;->a:Lsoftware/simplicial/a/am;

    .line 567
    const-string v2, "XP"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v0, Lsoftware/simplicial/a/az;->b:J

    .line 568
    const-string v2, "dotsEaten"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/az;->c:I

    .line 569
    const-string v2, "blobsEaten"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/az;->q:I

    .line 570
    const-string v2, "blobsLost"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/az;->r:I

    .line 571
    const-string v2, "massGained"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v0, Lsoftware/simplicial/a/az;->s:J

    .line 572
    const-string v2, "massEjected"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v0, Lsoftware/simplicial/a/az;->t:J

    .line 573
    const-string v2, "biggestBlob"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/az;->u:I

    .line 574
    const-string v2, "highestScore"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/az;->v:I

    .line 575
    const-string v2, "averageScore"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/az;->w:I

    .line 576
    const-string v2, "timesRestarted"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/az;->x:I

    .line 577
    const-string v2, "longestLife"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v0, Lsoftware/simplicial/a/az;->y:J

    .line 578
    const-string v2, "gamesWon"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/az;->z:I

    .line 579
    const-string v2, "ejectCount"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/az;->D:I

    .line 580
    const-string v2, "splitCount"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/az;->E:I

    .line 581
    const-string v2, "smbhEatenCount"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/az;->F:I

    .line 582
    const-string v2, "smbhCollidedCount"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/az;->G:I

    .line 583
    const-string v2, "bhCollidedCount"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/az;->H:I

    .line 584
    const-string v2, "tbhCollidedCount"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/az;->I:I

    .line 585
    const-string v2, "timesTeleported"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/az;->J:I

    .line 586
    const-string v2, "powerupsUsed"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/az;->K:I

    .line 587
    const-string v2, "pumpkinsEaten"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 588
    if-eq v2, v8, :cond_0

    .line 589
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/az;->d:I

    .line 590
    :cond_0
    const-string v2, "leavesEaten"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 591
    if-eq v2, v8, :cond_1

    .line 592
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/az;->e:I

    .line 593
    :cond_1
    const-string v2, "coinsCollected"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 594
    if-eq v2, v8, :cond_2

    .line 595
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/az;->p:I

    .line 596
    :cond_2
    const-string v2, "presentsEaten"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 597
    if-eq v2, v8, :cond_3

    .line 598
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/az;->f:I

    .line 599
    :cond_3
    const-string v2, "snowflakesEaten"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 600
    if-eq v2, v8, :cond_4

    .line 601
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/az;->g:I

    .line 602
    :cond_4
    const-string v2, "beadsEaten"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 603
    if-eq v2, v8, :cond_5

    .line 604
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/az;->h:I

    .line 605
    :cond_5
    const-string v2, "eggsEaten"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 606
    if-eq v2, v8, :cond_6

    .line 607
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/az;->i:I

    .line 608
    :cond_6
    const-string v2, "dropsEaten"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 609
    if-eq v2, v8, :cond_7

    .line 610
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/az;->j:I

    .line 611
    :cond_7
    const-string v2, "nebulasEaten"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 612
    if-eq v2, v8, :cond_8

    .line 613
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/az;->k:I

    .line 614
    :cond_8
    const-string v2, "candiesEaten"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 615
    if-eq v2, v8, :cond_9

    .line 616
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/az;->l:I

    .line 617
    :cond_9
    const-string v2, "sunsEaten"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 618
    if-eq v2, v8, :cond_a

    .line 619
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/az;->m:I

    .line 620
    :cond_a
    const-string v2, "moonsEaten"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 621
    if-eq v2, v8, :cond_b

    .line 622
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/az;->n:I

    .line 623
    :cond_b
    const-string v2, "notesEaten"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 624
    if-eq v2, v8, :cond_c

    .line 625
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/az;->o:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 634
    :cond_c
    :goto_0
    if-eqz v1, :cond_d

    .line 635
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 638
    :cond_d
    return-object v0

    .line 629
    :cond_e
    :try_start_1
    new-instance v0, Lsoftware/simplicial/a/az;

    invoke-direct {v0}, Lsoftware/simplicial/a/az;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 634
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_f

    .line 635
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_f
    throw v0
.end method

.method public a(Ljava/lang/String;[BILsoftware/simplicial/a/am;Z)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1101
    invoke-direct {p0, p4}, Lsoftware/simplicial/nebulous/d/d;->b(Lsoftware/simplicial/a/am;)Ljava/lang/String;

    move-result-object v0

    .line 1102
    if-eqz p5, :cond_0

    .line 1103
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_MAYHEM"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1110
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/d/d;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1112
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "select count(*) from single_player_high_scores where gameMode=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    .line 1113
    :try_start_2
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1114
    const/4 v4, 0x0

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 1115
    invoke-interface {v3}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1118
    const/16 v3, 0x64

    if-ge v4, v3, :cond_3

    .line 1120
    :try_start_3
    const-string v3, "single_player_high_scores"

    const/4 v4, 0x0

    invoke-direct {p0, v0, p1, p2, p3}, Lsoftware/simplicial/nebulous/d/d;->a(Ljava/lang/String;Ljava/lang/String;[BI)Landroid/content/ContentValues;

    move-result-object v0

    invoke-virtual {v1, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1149
    if-eqz v2, :cond_1

    .line 1150
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1151
    :cond_1
    if-eqz v1, :cond_2

    .line 1152
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 1154
    :cond_2
    :goto_0
    return-void

    .line 1124
    :cond_3
    :try_start_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "select * from single_player_high_scores where gameMode=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\' order by "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "score"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " asc limit 1"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 1129
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1131
    const-string v3, "score"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 1132
    if-le p3, v3, :cond_4

    .line 1134
    const-string v3, "entryID"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 1135
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 1136
    const-string v6, "entryID"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1138
    const-string v3, "single_player_high_scores"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "entryID="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1139
    const-string v3, "single_player_high_scores"

    const/4 v4, 0x0

    invoke-direct {p0, v0, p1, p2, p3}, Lsoftware/simplicial/nebulous/d/d;->a(Ljava/lang/String;Ljava/lang/String;[BI)Landroid/content/ContentValues;

    move-result-object v0

    invoke-virtual {v1, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1149
    :cond_4
    if-eqz v2, :cond_5

    .line 1150
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1151
    :cond_5
    if-eqz v1, :cond_2

    .line 1152
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto/16 :goto_0

    .line 1143
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 1145
    :goto_1
    :try_start_5
    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1149
    if-eqz v2, :cond_6

    .line 1150
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1151
    :cond_6
    if-eqz v1, :cond_2

    .line 1152
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto/16 :goto_0

    .line 1149
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v2, :cond_7

    .line 1150
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1151
    :cond_7
    if-eqz v1, :cond_8

    .line 1152
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_8
    throw v0

    .line 1149
    :catchall_1
    move-exception v0

    goto :goto_2

    :catchall_2
    move-exception v0

    move-object v2, v3

    goto :goto_2

    .line 1143
    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    move-object v2, v3

    goto :goto_1
.end method

.method public a(Lsoftware/simplicial/a/af;)Z
    .locals 5

    .prologue
    .line 753
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/d/d;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 755
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/d/d;->b(Lsoftware/simplicial/a/af;)Landroid/content/ContentValues;

    move-result-object v1

    .line 757
    const-string v2, "single_player_purchased_eject_skins"

    const/4 v3, 0x0

    const/4 v4, 0x5

    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v2

    .line 759
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 761
    const-wide/16 v0, -0x1

    cmp-long v0, v2, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 654
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/d/d;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 656
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/d/d;->b(Lsoftware/simplicial/a/am;)Ljava/lang/String;

    move-result-object v3

    .line 658
    invoke-direct {p0, v3, p2}, Lsoftware/simplicial/nebulous/d/d;->a(Ljava/lang/String;Lsoftware/simplicial/a/az;)Landroid/content/ContentValues;

    move-result-object v4

    .line 660
    const-string v5, "gameMode=?"

    .line 661
    new-array v6, v0, [Ljava/lang/String;

    aput-object v3, v6, v1

    .line 663
    const-string v3, "single_player_stats"

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    int-to-long v4, v3

    .line 665
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 667
    const-wide/16 v2, -0x1

    cmp-long v2, v4, v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public a(Lsoftware/simplicial/a/as;)Z
    .locals 5

    .prologue
    .line 766
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/d/d;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 768
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/d/d;->b(Lsoftware/simplicial/a/as;)Landroid/content/ContentValues;

    move-result-object v1

    .line 770
    const-string v2, "single_player_purchased_hats"

    const/4 v3, 0x0

    const/4 v4, 0x5

    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v2

    .line 772
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 774
    const-wide/16 v0, -0x1

    cmp-long v0, v2, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lsoftware/simplicial/a/bd;)Z
    .locals 5

    .prologue
    .line 779
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/d/d;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 781
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/d/d;->b(Lsoftware/simplicial/a/bd;)Landroid/content/ContentValues;

    move-result-object v1

    .line 783
    const-string v2, "single_player_purchased_pets"

    const/4 v3, 0x0

    const/4 v4, 0x5

    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v2

    .line 785
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 787
    const-wide/16 v0, -0x1

    cmp-long v0, v2, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lsoftware/simplicial/a/d;)Z
    .locals 5

    .prologue
    .line 1073
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/d/d;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1075
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/d/d;->b(Lsoftware/simplicial/a/d;)Landroid/content/ContentValues;

    move-result-object v1

    .line 1077
    const-string v2, "single_player_earned_achievements"

    const/4 v3, 0x0

    const/4 v4, 0x5

    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v2

    .line 1079
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 1081
    const-wide/16 v0, -0x1

    cmp-long v0, v2, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lsoftware/simplicial/a/e;)Z
    .locals 5

    .prologue
    .line 740
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/d/d;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 742
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/d/d;->b(Lsoftware/simplicial/a/e;)Landroid/content/ContentValues;

    move-result-object v1

    .line 744
    const-string v2, "single_player_purchased_avatars"

    const/4 v3, 0x0

    const/4 v4, 0x5

    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v2

    .line 746
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 748
    const-wide/16 v0, -0x1

    cmp-long v0, v2, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/af;",
            ">;"
        }
    .end annotation

    .prologue
    .line 830
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 832
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/d/d;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 834
    const-string v0, "SELECT * FROM single_player_purchased_eject_skins"

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 838
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 844
    :cond_0
    :try_start_1
    const-string v0, "purchasedEjectSkins"

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lsoftware/simplicial/a/af;->a(I)Lsoftware/simplicial/a/af;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 851
    :goto_0
    :try_start_2
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 857
    :cond_1
    if-eqz v3, :cond_2

    .line 858
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 859
    :cond_2
    if-eqz v2, :cond_3

    .line 860
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 863
    :cond_3
    return-object v1

    .line 846
    :catch_0
    move-exception v0

    .line 848
    :try_start_3
    sget-object v4, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 857
    :catchall_0
    move-exception v0

    if-eqz v3, :cond_4

    .line 858
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 859
    :cond_4
    if-eqz v2, :cond_5

    .line 860
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_5
    throw v0
.end method

.method public c()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/as;",
            ">;"
        }
    .end annotation

    .prologue
    .line 868
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 870
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/d/d;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 872
    const-string v0, "SELECT * FROM single_player_purchased_hats"

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 876
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 882
    :cond_0
    :try_start_1
    const-string v0, "purchasedHats"

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lsoftware/simplicial/a/as;->a(I)Lsoftware/simplicial/a/as;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 889
    :goto_0
    :try_start_2
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 895
    :cond_1
    if-eqz v3, :cond_2

    .line 896
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 897
    :cond_2
    if-eqz v2, :cond_3

    .line 898
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 901
    :cond_3
    return-object v1

    .line 884
    :catch_0
    move-exception v0

    .line 886
    :try_start_3
    sget-object v4, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 895
    :catchall_0
    move-exception v0

    if-eqz v3, :cond_4

    .line 896
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 897
    :cond_4
    if-eqz v2, :cond_5

    .line 898
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_5
    throw v0
.end method

.method public d()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Lsoftware/simplicial/a/bd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 906
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 908
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/d/d;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 910
    const-string v0, "SELECT * FROM single_player_purchased_pets"

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 914
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 920
    :cond_0
    :try_start_1
    const-string v0, "purchasedPets"

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lsoftware/simplicial/a/bd;->a(I)Lsoftware/simplicial/a/bd;

    move-result-object v0

    .line 921
    iget-byte v4, v0, Lsoftware/simplicial/a/bd;->c:B

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    invoke-interface {v1, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 928
    :goto_0
    :try_start_2
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 934
    :cond_1
    if-eqz v3, :cond_2

    .line 935
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 936
    :cond_2
    if-eqz v2, :cond_3

    .line 937
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 940
    :cond_3
    return-object v1

    .line 923
    :catch_0
    move-exception v0

    .line 925
    :try_start_3
    sget-object v4, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 934
    :catchall_0
    move-exception v0

    if-eqz v3, :cond_4

    .line 935
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 936
    :cond_4
    if-eqz v2, :cond_5

    .line 937
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_5
    throw v0
.end method

.method public e()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1041
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1043
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/d/d;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1045
    const-string v2, "SELECT * FROM single_player_earned_achievements"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 1049
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1053
    :cond_0
    const-string v3, "AchieveID"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-direct {p0, v3}, Lsoftware/simplicial/nebulous/d/d;->a(I)Lsoftware/simplicial/a/d;

    move-result-object v3

    .line 1054
    if-eqz v3, :cond_1

    .line 1055
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1057
    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    .line 1062
    :cond_2
    if-eqz v2, :cond_3

    .line 1063
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1064
    :cond_3
    if-eqz v1, :cond_4

    .line 1065
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 1068
    :cond_4
    return-object v0

    .line 1062
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_5

    .line 1063
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1064
    :cond_5
    if-eqz v1, :cond_6

    .line 1065
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_6
    throw v0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 114
    const-string v0, "CREATE TABLE single_player_stats (gameMode TEXT PRIMARY KEY,averageScore INT,bhCollidedCount INT,biggestBlob INT,blobsEaten INT,blobsLost INT,dotsEaten INT,ejectCount INT,gamesWon INT,highestScore INT,longestLife BIGINT,massEjected BIGINT,massGained BIGINT,timesRestarted INT,splitCount INT,smbhEatenCount INT,smbhCollidedCount INT,XP BIGINT,pumpkinsEaten INT,leavesEaten INT,coinsCollected BIGINT,presentsEaten INT,snowflakesEaten INT,beadsEaten INT,eggsEaten INT,tbhCollidedCount INT,timesTeleported INT,powerupsUsed INT,dropsEaten INT,nebulasEaten INT,candiesEaten INT,sunsEaten INT,moonsEaten INT,notesEaten INT )"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 115
    const-string v0, "CREATE TABLE single_player_purchased_avatars (purchasedAvatars INT PRIMARY KEY )"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 116
    const-string v0, "CREATE TABLE single_player_earned_achievements (AchieveID INT PRIMARY KEY )"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 117
    const-string v0, "CREATE TABLE single_player_high_scores (entryID BIGINT PRIMARY KEY,gameMode TEXT,playerName TEXT,score INT,playerNameColors BLOB )"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 118
    const/4 v0, 0x0

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 119
    sget-object v0, Lsoftware/simplicial/a/am;->a:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 120
    sget-object v0, Lsoftware/simplicial/a/am;->b:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 121
    sget-object v0, Lsoftware/simplicial/a/am;->c:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 122
    sget-object v0, Lsoftware/simplicial/a/am;->d:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 123
    sget-object v0, Lsoftware/simplicial/a/am;->e:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 124
    sget-object v0, Lsoftware/simplicial/a/am;->f:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 125
    sget-object v0, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 126
    sget-object v0, Lsoftware/simplicial/a/am;->i:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 127
    sget-object v0, Lsoftware/simplicial/a/am;->l:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 128
    sget-object v0, Lsoftware/simplicial/a/am;->j:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 129
    sget-object v0, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 130
    sget-object v0, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 131
    sget-object v0, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 132
    sget-object v0, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 133
    sget-object v0, Lsoftware/simplicial/a/am;->p:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 134
    sget-object v0, Lsoftware/simplicial/a/am;->q:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 135
    sget-object v0, Lsoftware/simplicial/a/am;->r:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 136
    const-string v0, "CREATE TABLE single_player_purchased_eject_skins (purchasedEjectSkins INT PRIMARY KEY )"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 137
    const-string v0, "CREATE TABLE single_player_purchased_pets (purchasedPets INT PRIMARY KEY )"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 138
    const-string v0, "CREATE TABLE single_player_purchased_hats (purchasedHats INT PRIMARY KEY )"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 139
    return-void
.end method

.method public onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    .prologue
    .line 473
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3

    .prologue
    .line 143
    const/4 v0, 0x2

    if-ge p2, v0, :cond_0

    .line 145
    sget-object v0, Lsoftware/simplicial/a/am;->d:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 147
    :cond_0
    const/4 v0, 0x3

    if-ge p2, v0, :cond_1

    .line 149
    sget-object v0, Lsoftware/simplicial/a/am;->f:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 151
    :cond_1
    const/4 v0, 0x4

    if-ge p2, v0, :cond_2

    .line 153
    sget-object v0, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 155
    :cond_2
    const/4 v0, 0x5

    if-ge p2, v0, :cond_3

    .line 157
    sget-object v0, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 160
    :try_start_0
    const-string v0, "ALTER TABLE single_player_stats ADD COLUMN pumpkinsEaten INTEGER DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    :cond_3
    const/4 v0, 0x7

    if-ge p2, v0, :cond_4

    .line 172
    :try_start_1
    const-string v0, "ALTER TABLE single_player_stats ADD COLUMN leavesEaten INTEGER DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_1

    .line 180
    :cond_4
    const/16 v0, 0x8

    if-ge p2, v0, :cond_6

    .line 184
    :try_start_2
    const-string v0, "CREATE TABLE single_player_purchased_avatars (purchasedAvatars INT PRIMARY KEY )"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_2

    .line 194
    :cond_5
    :try_start_3
    const-string v0, "ALTER TABLE single_player_stats ADD COLUMN coinsCollected BIGINT DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_3
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_3} :catch_3

    .line 202
    :cond_6
    const/16 v0, 0x9

    if-ge p2, v0, :cond_7

    .line 206
    :try_start_4
    const-string v0, "CREATE TABLE single_player_earned_achievements (AchieveID INT PRIMARY KEY )"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_4
    .catch Landroid/database/SQLException; {:try_start_4 .. :try_end_4} :catch_4

    .line 214
    :cond_7
    const/16 v0, 0xa

    if-ge p2, v0, :cond_8

    .line 218
    :try_start_5
    const-string v0, "CREATE TABLE single_player_high_scores (entryID BIGINT PRIMARY KEY,gameMode TEXT,playerName TEXT,score INT,playerNameColors BLOB )"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_5
    .catch Landroid/database/SQLException; {:try_start_5 .. :try_end_5} :catch_5

    .line 226
    :cond_8
    const/16 v0, 0xb

    if-ge p2, v0, :cond_9

    .line 230
    :try_start_6
    const-string v0, "ALTER TABLE single_player_stats ADD COLUMN presentsEaten INTEGER DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_6
    .catch Landroid/database/SQLException; {:try_start_6 .. :try_end_6} :catch_6

    .line 238
    :cond_9
    const/16 v0, 0xc

    if-ge p2, v0, :cond_a

    .line 242
    :try_start_7
    const-string v0, "ALTER TABLE single_player_stats ADD COLUMN snowflakesEaten INTEGER DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_7
    .catch Landroid/database/SQLException; {:try_start_7 .. :try_end_7} :catch_7

    .line 250
    :cond_a
    const/16 v0, 0xd

    if-ge p2, v0, :cond_b

    .line 252
    sget-object v0, Lsoftware/simplicial/a/am;->i:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 254
    :cond_b
    const/16 v0, 0xe

    if-ge p2, v0, :cond_c

    .line 258
    :try_start_8
    const-string v0, "ALTER TABLE single_player_stats ADD COLUMN beadsEaten INTEGER DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_8
    .catch Landroid/database/SQLException; {:try_start_8 .. :try_end_8} :catch_8

    .line 270
    :cond_c
    const/16 v0, 0x10

    if-ge p2, v0, :cond_d

    .line 274
    :try_start_9
    const-string v0, "ALTER TABLE single_player_stats ADD COLUMN eggsEaten INTEGER DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_9
    .catch Landroid/database/SQLException; {:try_start_9 .. :try_end_9} :catch_9

    .line 282
    :cond_d
    const/16 v0, 0x11

    if-ge p2, v0, :cond_f

    .line 286
    :try_start_a
    const-string v0, "ALTER TABLE single_player_stats ADD COLUMN tbhCollidedCount INTEGER DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 287
    const-string v0, "ALTER TABLE single_player_stats ADD COLUMN timesTeleported INTEGER DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 288
    const-string v0, "ALTER TABLE single_player_stats ADD COLUMN powerupsUsed INTEGER DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_a
    .catch Landroid/database/SQLException; {:try_start_a .. :try_end_a} :catch_a

    .line 296
    :cond_e
    const/16 v0, 0xf

    if-lt p2, v0, :cond_f

    .line 300
    :try_start_b
    const-string v0, "single_player_stats"

    const-string v1, "gameMode=\'MAYHEM\'"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_15

    .line 308
    :cond_f
    :goto_0
    const/16 v0, 0x12

    if-ge p2, v0, :cond_10

    .line 312
    :try_start_c
    const-string v0, "ALTER TABLE single_player_stats ADD COLUMN dropsEaten INTEGER DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_c
    .catch Landroid/database/SQLException; {:try_start_c .. :try_end_c} :catch_b

    .line 320
    :cond_10
    const/16 v0, 0x13

    if-ge p2, v0, :cond_11

    .line 324
    :try_start_d
    const-string v0, "ALTER TABLE single_player_stats ADD COLUMN nebulasEaten INTEGER DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_d
    .catch Landroid/database/SQLException; {:try_start_d .. :try_end_d} :catch_c

    .line 332
    :cond_11
    const/16 v0, 0x14

    if-ge p2, v0, :cond_12

    .line 336
    :try_start_e
    const-string v0, "ALTER TABLE single_player_stats ADD COLUMN candiesEaten INTEGER DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_e
    .catch Landroid/database/SQLException; {:try_start_e .. :try_end_e} :catch_d

    .line 344
    :cond_12
    const/16 v0, 0x15

    if-ge p2, v0, :cond_13

    .line 348
    :try_start_f
    const-string v0, "ALTER TABLE single_player_stats ADD COLUMN sunsEaten INTEGER DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_f
    .catch Landroid/database/SQLException; {:try_start_f .. :try_end_f} :catch_e

    .line 356
    :cond_13
    const/16 v0, 0x16

    if-ge p2, v0, :cond_14

    .line 360
    :try_start_10
    const-string v0, "ALTER TABLE single_player_stats ADD COLUMN moonsEaten INTEGER DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_10
    .catch Landroid/database/SQLException; {:try_start_10 .. :try_end_10} :catch_f

    .line 368
    :cond_14
    const/16 v0, 0x17

    if-ge p2, v0, :cond_15

    .line 372
    :try_start_11
    const-string v0, "ALTER TABLE single_player_stats ADD COLUMN notesEaten INTEGER DEFAULT 0"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_11
    .catch Landroid/database/SQLException; {:try_start_11 .. :try_end_11} :catch_10

    .line 380
    :cond_15
    const/16 v0, 0x18

    if-ge p2, v0, :cond_16

    .line 384
    :try_start_12
    const-string v0, "CREATE TABLE single_player_purchased_eject_skins (purchasedEjectSkins INT PRIMARY KEY )"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_12
    .catch Landroid/database/SQLException; {:try_start_12 .. :try_end_12} :catch_11

    .line 392
    :cond_16
    const/16 v0, 0x19

    if-ge p2, v0, :cond_17

    .line 394
    sget-object v0, Lsoftware/simplicial/a/am;->j:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 396
    :cond_17
    const/16 v0, 0x1a

    if-ge p2, v0, :cond_18

    .line 398
    sget-object v0, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 400
    :cond_18
    const/16 v0, 0x1b

    if-ge p2, v0, :cond_19

    .line 404
    :try_start_13
    const-string v0, "ALTER TABLE single_player_high_scores ADD COLUMN playerNameColors BLOB"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_13
    .catch Landroid/database/SQLException; {:try_start_13 .. :try_end_13} :catch_12

    .line 412
    :cond_19
    const/16 v0, 0x1c

    if-ge p2, v0, :cond_1a

    .line 414
    sget-object v0, Lsoftware/simplicial/a/am;->l:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 416
    :cond_1a
    const/16 v0, 0x1d

    if-ge p2, v0, :cond_1b

    .line 418
    sget-object v0, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 420
    :cond_1b
    const/16 v0, 0x1e

    if-ge p2, v0, :cond_1c

    .line 422
    sget-object v0, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 424
    :cond_1c
    const/16 v0, 0x1f

    if-ge p2, v0, :cond_1d

    .line 426
    sget-object v0, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 428
    :cond_1d
    const/16 v0, 0x20

    if-ge p2, v0, :cond_1e

    .line 430
    sget-object v0, Lsoftware/simplicial/a/am;->p:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 432
    :cond_1e
    const/16 v0, 0x22

    if-ge p2, v0, :cond_1f

    .line 436
    :try_start_14
    const-string v0, "CREATE TABLE single_player_purchased_pets (purchasedPets INT PRIMARY KEY )"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_14
    .catch Landroid/database/SQLException; {:try_start_14 .. :try_end_14} :catch_13

    .line 444
    :cond_1f
    const/16 v0, 0x23

    if-ge p2, v0, :cond_20

    .line 446
    sget-object v0, Lsoftware/simplicial/a/am;->p:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 448
    :cond_20
    const/16 v0, 0x24

    if-ge p2, v0, :cond_21

    .line 450
    sget-object v0, Lsoftware/simplicial/a/am;->q:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 452
    :cond_21
    const/16 v0, 0x25

    if-ge p2, v0, :cond_22

    .line 454
    sget-object v0, Lsoftware/simplicial/a/am;->r:Lsoftware/simplicial/a/am;

    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Landroid/database/sqlite/SQLiteDatabase;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/az;)Z

    .line 456
    :cond_22
    const/16 v0, 0x27

    if-ge p2, v0, :cond_23

    .line 460
    :try_start_15
    const-string v0, "CREATE TABLE single_player_purchased_hats (purchasedHats INT PRIMARY KEY )"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_15
    .catch Landroid/database/SQLException; {:try_start_15 .. :try_end_15} :catch_14

    .line 468
    :cond_23
    return-void

    .line 162
    :catch_0
    move-exception v0

    .line 164
    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "duplicate column name"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 165
    throw v0

    .line 174
    :catch_1
    move-exception v0

    .line 176
    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "duplicate column name"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 177
    throw v0

    .line 186
    :catch_2
    move-exception v0

    .line 188
    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "already exists"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 189
    throw v0

    .line 196
    :catch_3
    move-exception v0

    .line 198
    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "duplicate column name"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 199
    throw v0

    .line 208
    :catch_4
    move-exception v0

    .line 210
    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "already exists"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 211
    throw v0

    .line 220
    :catch_5
    move-exception v0

    .line 222
    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "already exists"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 223
    throw v0

    .line 232
    :catch_6
    move-exception v0

    .line 234
    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "duplicate column name"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 235
    throw v0

    .line 244
    :catch_7
    move-exception v0

    .line 246
    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "duplicate column name"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 247
    throw v0

    .line 260
    :catch_8
    move-exception v0

    .line 262
    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "duplicate column name"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 263
    throw v0

    .line 276
    :catch_9
    move-exception v0

    .line 278
    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "duplicate column name"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 279
    throw v0

    .line 290
    :catch_a
    move-exception v0

    .line 292
    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "duplicate column name"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 293
    throw v0

    .line 314
    :catch_b
    move-exception v0

    .line 316
    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "duplicate column name"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_10

    .line 317
    throw v0

    .line 326
    :catch_c
    move-exception v0

    .line 328
    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "duplicate column name"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_11

    .line 329
    throw v0

    .line 338
    :catch_d
    move-exception v0

    .line 340
    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "duplicate column name"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_12

    .line 341
    throw v0

    .line 350
    :catch_e
    move-exception v0

    .line 352
    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "duplicate column name"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_13

    .line 353
    throw v0

    .line 362
    :catch_f
    move-exception v0

    .line 364
    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "duplicate column name"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_14

    .line 365
    throw v0

    .line 374
    :catch_10
    move-exception v0

    .line 376
    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "duplicate column name"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_15

    .line 377
    throw v0

    .line 386
    :catch_11
    move-exception v0

    .line 388
    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "already exists"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_16

    .line 389
    throw v0

    .line 406
    :catch_12
    move-exception v0

    .line 408
    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "duplicate column name"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_19

    .line 409
    throw v0

    .line 438
    :catch_13
    move-exception v0

    .line 440
    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "already exists"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1f

    .line 441
    throw v0

    .line 462
    :catch_14
    move-exception v0

    .line 464
    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "already exists"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_23

    .line 465
    throw v0

    .line 302
    :catch_15
    move-exception v0

    goto/16 :goto_0
.end method
