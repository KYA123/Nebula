.class public final enum Lsoftware/simplicial/nebulous/f/af;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/nebulous/f/af;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/nebulous/f/af;

.field public static final enum b:Lsoftware/simplicial/nebulous/f/af;

.field public static final enum c:Lsoftware/simplicial/nebulous/f/af;

.field private static final synthetic d:[Lsoftware/simplicial/nebulous/f/af;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 8
    new-instance v0, Lsoftware/simplicial/nebulous/f/af;

    const-string v1, "ARENA"

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/f/af;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/f/af;->a:Lsoftware/simplicial/nebulous/f/af;

    new-instance v0, Lsoftware/simplicial/nebulous/f/af;

    const-string v1, "TEAM_ARENA"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/nebulous/f/af;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/f/af;->b:Lsoftware/simplicial/nebulous/f/af;

    new-instance v0, Lsoftware/simplicial/nebulous/f/af;

    const-string v1, "CHALLENGE"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/nebulous/f/af;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/f/af;->c:Lsoftware/simplicial/nebulous/f/af;

    .line 6
    const/4 v0, 0x3

    new-array v0, v0, [Lsoftware/simplicial/nebulous/f/af;

    sget-object v1, Lsoftware/simplicial/nebulous/f/af;->a:Lsoftware/simplicial/nebulous/f/af;

    aput-object v1, v0, v2

    sget-object v1, Lsoftware/simplicial/nebulous/f/af;->b:Lsoftware/simplicial/nebulous/f/af;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/nebulous/f/af;->c:Lsoftware/simplicial/nebulous/f/af;

    aput-object v1, v0, v4

    sput-object v0, Lsoftware/simplicial/nebulous/f/af;->d:[Lsoftware/simplicial/nebulous/f/af;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/nebulous/f/af;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lsoftware/simplicial/nebulous/f/af;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/f/af;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/nebulous/f/af;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lsoftware/simplicial/nebulous/f/af;->d:[Lsoftware/simplicial/nebulous/f/af;

    invoke-virtual {v0}, [Lsoftware/simplicial/nebulous/f/af;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/nebulous/f/af;

    return-object v0
.end method
