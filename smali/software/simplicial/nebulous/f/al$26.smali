.class Lsoftware/simplicial/nebulous/f/al$26;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/f/al;->a(ILsoftware/simplicial/nebulous/f/al$ac;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Lsoftware/simplicial/nebulous/f/al$ac;

.field final synthetic c:Lsoftware/simplicial/nebulous/f/al;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/f/al;ILsoftware/simplicial/nebulous/f/al$ac;)V
    .locals 0

    .prologue
    .line 2112
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/al$26;->c:Lsoftware/simplicial/nebulous/f/al;

    iput p2, p0, Lsoftware/simplicial/nebulous/f/al$26;->a:I

    iput-object p3, p0, Lsoftware/simplicial/nebulous/f/al$26;->b:Lsoftware/simplicial/nebulous/f/al$ac;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 2127
    :try_start_0
    new-instance v1, Ljava/net/URL;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lsoftware/simplicial/nebulous/f/al$26;->c:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v3}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v3

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->aw:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lsoftware/simplicial/nebulous/f/al$26;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 2128
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 2129
    const/4 v3, 0x1

    iput v3, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 2130
    const/4 v3, 0x0

    iput-boolean v3, v2, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 2131
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v3, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 2132
    const/4 v3, 0x0

    iput-boolean v3, v2, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 2133
    invoke-virtual {v1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v1

    .line 2134
    invoke-virtual {v1}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 2135
    const/4 v3, 0x0

    invoke-static {v1, v3, v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0
    
    ## Download Skin
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/al$26;->b:Lsoftware/simplicial/nebulous/f/al$ac;
    
    if-eqz v1, :skip # if this field is null then download the skin
    
    :try
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;
    
    const/16 v2, 0x64
    
    new-instance v3, Ljava/io/FileOutputStream;
    
    new-instance v4, Ljava/lang/StringBuilder;
    
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V
    
    iget-object v5, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;
    
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    
    move-result-object v4
    
    iget v5, Lsoftware/simplicial/nebulous/f/al$26;->a:I
    
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    
    move-result-object v4
    
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    
    move-result-object v4
    
    invoke-direct {v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    
    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/FileOutputStream;)Z
    
    :try_end
    .catch Ljava/lang/Exception; {:try .. :try_end} :catch
    
    .catchall {:try .. :try_end} :finally

    .line 2141
    :skip
    :goto_0
    return-object v0

    .line 2138
    :catch
    move-exception v0
    
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    
    throw v0
    
    :finally
    :try_close
    
    move-object v0, v3
    
    if-eqz v0, :catch
    
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    
    goto :goto_0
    
    :end_close
    .catch Ljava/lang/Exception; {:try_close .. :end_close} :catch
    
    :catch_0
    move-exception v1

    .line 2140
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method protected a(Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 2148
    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GotSkin "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lsoftware/simplicial/nebulous/f/al$26;->a:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p1, :cond_0

    const-string v0, " SUCCESS"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 2149
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$26;->b:Lsoftware/simplicial/nebulous/f/al$ac;

    iget v1, p0, Lsoftware/simplicial/nebulous/f/al$26;->a:I

    invoke-interface {v0, v1, p1}, Lsoftware/simplicial/nebulous/f/al$ac;->a(ILandroid/graphics/Bitmap;)V

    .line 2150
    return-void

    .line 2148
    :cond_0
    const-string v0, " FAILED"

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2112
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/f/al$26;->a([Ljava/lang/Void;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2112
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$26;->b:Lsoftware/simplicial/nebulous/f/al$ac;
    
    if-eqz v0, :default
    
    invoke-static {}, Lsoftware/simplicial/nebulous/application/ar;->getCtx()Lsoftware/simplicial/nebulous/application/MainActivity;
    
    move-result-object v0
    
    new-instance v1, Ljava/lang/StringBuilder;
    
    const-string v2, "Downloaded to: "
    
    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V
    
    iget-object v2, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;
    
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    
    move-result-object v1
    
    iget v2, Lsoftware/simplicial/nebulous/f/al$26;->a:I
    
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    
    move-result-object v1
    
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    
    move-result-object v1
    
    const/4 v2, 0x0
    
    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;
    
    move-result-object v0
    
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    
    goto :exit
    
    :default
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/f/al$26;->a(Landroid/graphics/Bitmap;)V
    
    :exit
    return-void
.end method
