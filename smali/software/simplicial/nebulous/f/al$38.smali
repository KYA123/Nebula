.class Lsoftware/simplicial/nebulous/f/al$38;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$z;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/f/al;->a(JLsoftware/simplicial/nebulous/f/al$aa;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/f/al$aa;

.field final synthetic b:Lsoftware/simplicial/nebulous/f/al;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/f/al;Lsoftware/simplicial/nebulous/f/al$aa;)V
    .locals 0

    .prologue
    .line 2684
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/al$38;->b:Lsoftware/simplicial/nebulous/f/al;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/f/al$38;->a:Lsoftware/simplicial/nebulous/f/al$aa;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;)V
    .locals 12

    .prologue
    .line 2690
    if-eqz p1, :cond_0

    .line 2692
    :try_start_0
    const-string v0, "Error"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "Error"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2695
    const-string v0, "Error"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2696
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/al$38;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;Z)V

    .line 2713
    :cond_0
    :goto_0
    return-void

    .line 2700
    :cond_1
    new-instance v1, Lsoftware/simplicial/nebulous/f/y;

    const-string v0, "MsgID"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    const-string v0, "FromAID"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    const-string v0, "FromName"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v0, "ToAID"

    .line 2701
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    const-string v0, "ToName"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v0, "Subject"

    .line 2702
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v0, "IsNew"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    const-string v0, "TimeSent"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lb/a/a/b/a;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v10

    const-string v0, "TimeExpires"

    .line 2703
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lb/a/a/b/a;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v11

    invoke-direct/range {v1 .. v11}, Lsoftware/simplicial/nebulous/f/y;-><init>(JILjava/lang/String;ILjava/lang/String;Ljava/lang/String;ZLjava/util/Date;Ljava/util/Date;)V

    .line 2704
    const-string v0, "Message"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lsoftware/simplicial/a/ba;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lsoftware/simplicial/nebulous/f/y;->j:Ljava/lang/String;

    .line 2705
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$38;->a:Lsoftware/simplicial/nebulous/f/al$aa;

    invoke-interface {v0, v1}, Lsoftware/simplicial/nebulous/f/al$aa;->a(Lsoftware/simplicial/nebulous/f/y;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2709
    :catch_0
    move-exception v0

    .line 2711
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/al$38;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to ReadMail: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
