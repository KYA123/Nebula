.class Lsoftware/simplicial/nebulous/f/al$48;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$z;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/f/al;->a(IILsoftware/simplicial/nebulous/f/al$s;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/f/al$s;

.field final synthetic b:Lsoftware/simplicial/nebulous/f/al;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/f/al;Lsoftware/simplicial/nebulous/f/al$s;)V
    .locals 0

    .prologue
    .line 3214
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/al$48;->b:Lsoftware/simplicial/nebulous/f/al;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/f/al$48;->a:Lsoftware/simplicial/nebulous/f/al$s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;)V
    .locals 8

    .prologue
    .line 3220
    if-eqz p1, :cond_1

    .line 3223
    :try_start_0
    const-string v0, "FriendRequests"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 3225
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 3227
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 3229
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 3230
    new-instance v4, Lsoftware/simplicial/a/bg;

    invoke-direct {v4}, Lsoftware/simplicial/a/bg;-><init>()V

    .line 3231
    const-string v5, "Id"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, v4, Lsoftware/simplicial/a/bg;->b:I

    .line 3232
    const-string v5, "Name"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lsoftware/simplicial/a/bg;->c:Ljava/lang/CharSequence;

    .line 3233
    const-string v5, "ClanName"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lsoftware/simplicial/a/bg;->d:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 3236
    :try_start_1
    const-string v5, "ClanRole"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lsoftware/simplicial/a/q;->a(Ljava/lang/String;)Lsoftware/simplicial/a/q;

    move-result-object v5

    iput-object v5, v4, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 3242
    :goto_1
    :try_start_2
    invoke-static {v3}, Lsoftware/simplicial/nebulous/f/al;->a(Lorg/json/JSONObject;)[B

    move-result-object v5

    iput-object v5, v4, Lsoftware/simplicial/a/bg;->e:[B

    .line 3243
    const-string v5, "XP"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, v4, Lsoftware/simplicial/a/bg;->g:J

    .line 3244
    const-string v5, "LastPlayedUtc"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 3245
    const-string v5, "LastPlayedUtc"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lb/a/a/b/a;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v5

    iput-object v5, v4, Lsoftware/simplicial/a/bg;->p:Ljava/util/Date;

    .line 3248
    :goto_2
    const-string v5, "Relationship"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lsoftware/simplicial/a/bg$a;->valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/bg$a;

    move-result-object v5

    iput-object v5, v4, Lsoftware/simplicial/a/bg;->h:Lsoftware/simplicial/a/bg$a;

    .line 3249
    const-string v5, "BFF"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 3250
    const-string v5, "BFF"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, v4, Lsoftware/simplicial/a/bg;->i:Z

    .line 3251
    :cond_0
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3227
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3238
    :catch_0
    move-exception v5

    .line 3240
    sget-object v5, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    iput-object v5, v4, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 3258
    :catch_1
    move-exception v0

    .line 3260
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$48;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    const-string v1, "Failed to get friend requests."

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;)V

    .line 3262
    :cond_1
    :goto_3
    return-void

    .line 3247
    :cond_2
    const/4 v5, 0x0

    :try_start_3
    iput-object v5, v4, Lsoftware/simplicial/a/bg;->p:Ljava/util/Date;

    goto :goto_2

    .line 3254
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$48;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->e:Lsoftware/simplicial/nebulous/f/u;

    invoke-virtual {v0, v2}, Lsoftware/simplicial/nebulous/f/u;->a(Ljava/util/ArrayList;)V

    .line 3255
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$48;->a:Lsoftware/simplicial/nebulous/f/al$s;

    invoke-interface {v0, v2}, Lsoftware/simplicial/nebulous/f/al$s;->a(Ljava/util/ArrayList;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_3
.end method
