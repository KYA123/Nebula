.class Lsoftware/simplicial/nebulous/f/al$55;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$z;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al$o;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/f/al$o;

.field final synthetic b:Lsoftware/simplicial/nebulous/f/al;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/f/al;Lsoftware/simplicial/nebulous/f/al$o;)V
    .locals 0

    .prologue
    .line 3588
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/al$55;->b:Lsoftware/simplicial/nebulous/f/al;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/f/al$55;->a:Lsoftware/simplicial/nebulous/f/al$o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;)V
    .locals 16

    .prologue
    .line 3594
    if-eqz p1, :cond_2

    .line 3597
    :try_start_0
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 3598
    const-string v1, "Teams"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v11

    .line 3599
    const/4 v1, 0x0

    move v9, v1

    :goto_0
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result v1

    if-ge v9, v1, :cond_1

    .line 3603
    :try_start_1
    invoke-virtual {v11, v9}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    .line 3605
    const-string v2, "Id"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 3606
    const-string v3, "Name"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 3607
    const-string v4, "TeamArenaSize"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lsoftware/simplicial/a/h/f;->valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/h/f;

    move-result-object v5

    .line 3608
    const-string v4, "MyMembership"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lsoftware/simplicial/a/h/h;->valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/h/h;

    move-result-object v7

    .line 3609
    const-string v4, "Mayhem"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 3610
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 3611
    const-string v8, "Members"

    invoke-virtual {v1, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v12

    .line 3612
    const/4 v1, 0x0

    move v8, v1

    :goto_1
    invoke-virtual {v12}, Lorg/json/JSONArray;->length()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    if-ge v8, v1, :cond_0

    .line 3616
    :try_start_2
    invoke-virtual {v12, v8}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    .line 3618
    const-string v13, "Id"

    invoke-virtual {v1, v13}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v13

    .line 3619
    const-string v14, "Name"

    invoke-virtual {v1, v14}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3620
    const-string v15, "Membership"

    invoke-virtual {v1, v15}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lsoftware/simplicial/a/h/h;->valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/h/h;

    move-result-object v1

    .line 3622
    new-instance v15, Lsoftware/simplicial/nebulous/f/h;

    invoke-direct {v15, v13, v14, v1}, Lsoftware/simplicial/nebulous/f/h;-><init>(ILjava/lang/String;Lsoftware/simplicial/a/h/h;)V

    invoke-interface {v6, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 3612
    :goto_2
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto :goto_1

    .line 3624
    :catch_0
    move-exception v1

    .line 3626
    :try_start_3
    sget-object v13, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    .line 3632
    :catch_1
    move-exception v1

    .line 3634
    :try_start_4
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 3599
    :goto_3
    add-int/lit8 v1, v9, 0x1

    move v9, v1

    goto/16 :goto_0

    .line 3630
    :cond_0
    :try_start_5
    new-instance v1, Lsoftware/simplicial/nebulous/f/g;

    invoke-direct/range {v1 .. v7}, Lsoftware/simplicial/nebulous/f/g;-><init>(ILjava/lang/String;ZLsoftware/simplicial/a/h/f;Ljava/util/List;Lsoftware/simplicial/a/h/h;)V

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_3

    .line 3638
    :cond_1
    :try_start_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/f/al$55;->a:Lsoftware/simplicial/nebulous/f/al$o;

    invoke-interface {v1, v10}, Lsoftware/simplicial/nebulous/f/al$o;->a(Ljava/util/ArrayList;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    .line 3645
    :cond_2
    :goto_4
    return-void

    .line 3641
    :catch_2
    move-exception v1

    .line 3643
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/f/al$55;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    const-string v2, "Failed to get team member details."

    invoke-virtual {v1, v2}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;)V

    goto :goto_4
.end method
