.class Lsoftware/simplicial/nebulous/f/al$57;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$z;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al$q;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/f/al$q;

.field final synthetic b:Lsoftware/simplicial/nebulous/f/al;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/f/al;Lsoftware/simplicial/nebulous/f/al$q;)V
    .locals 0

    .prologue
    .line 3667
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/al$57;->b:Lsoftware/simplicial/nebulous/f/al;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/f/al$57;->a:Lsoftware/simplicial/nebulous/f/al$q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;)V
    .locals 6

    .prologue
    .line 3673
    if-eqz p1, :cond_1

    .line 3676
    :try_start_0
    const-string v0, "ClanInvites"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 3678
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 3680
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 3682
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 3683
    new-instance v4, Lsoftware/simplicial/nebulous/f/m;

    invoke-direct {v4}, Lsoftware/simplicial/nebulous/f/m;-><init>()V

    .line 3684
    const-string v5, "Name"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lsoftware/simplicial/nebulous/f/m;->a:Ljava/lang/String;

    .line 3685
    invoke-static {v3}, Lsoftware/simplicial/nebulous/f/al;->a(Lorg/json/JSONObject;)[B

    move-result-object v3

    iput-object v3, v4, Lsoftware/simplicial/nebulous/f/m;->b:[B

    .line 3686
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3680
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3689
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$57;->a:Lsoftware/simplicial/nebulous/f/al$q;

    invoke-interface {v0, v2}, Lsoftware/simplicial/nebulous/f/al$q;->a(Ljava/util/ArrayList;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3696
    :cond_1
    :goto_1
    return-void

    .line 3692
    :catch_0
    move-exception v0

    .line 3694
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$57;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    const-string v1, "Failed to get clan invites."

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;)V

    goto :goto_1
.end method
