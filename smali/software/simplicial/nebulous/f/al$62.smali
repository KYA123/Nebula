.class Lsoftware/simplicial/nebulous/f/al$62;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$z;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/a/am;IILsoftware/simplicial/a/b;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/a/am;

.field final synthetic b:Lsoftware/simplicial/a/b;

.field final synthetic c:Lsoftware/simplicial/nebulous/f/al;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/f/al;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/b;)V
    .locals 0

    .prologue
    .line 4007
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/al$62;->c:Lsoftware/simplicial/nebulous/f/al;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/f/al$62;->a:Lsoftware/simplicial/a/am;

    iput-object p3, p0, Lsoftware/simplicial/nebulous/f/al$62;->b:Lsoftware/simplicial/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;)V
    .locals 24

    .prologue
    .line 4011
    const/16 v18, 0x0

    .line 4012
    if-eqz p1, :cond_2

    .line 4014
    new-instance v4, Lsoftware/simplicial/a/az;

    invoke-direct {v4}, Lsoftware/simplicial/a/az;-><init>()V

    .line 4015
    new-instance v5, Lsoftware/simplicial/a/r;

    invoke-direct {v5}, Lsoftware/simplicial/a/r;-><init>()V

    .line 4016
    const-string v2, ""

    .line 4019
    :try_start_0
    const-string v2, "AccountName"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 4020
    const-string v2, "ClanName"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Lsoftware/simplicial/a/r;->a:Ljava/lang/String;

    .line 4021
    invoke-static/range {p1 .. p1}, Lsoftware/simplicial/nebulous/f/al;->a(Lorg/json/JSONObject;)[B

    move-result-object v2

    iput-object v2, v5, Lsoftware/simplicial/a/r;->b:[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 4024
    :try_start_1
    const-string v2, "ClanRole"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lsoftware/simplicial/a/q;->a(Ljava/lang/String;)Lsoftware/simplicial/a/q;

    move-result-object v2

    iput-object v2, v5, Lsoftware/simplicial/a/r;->c:Lsoftware/simplicial/a/q;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 4032
    :goto_0
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/al$62;->a:Lsoftware/simplicial/a/am;

    iput-object v2, v4, Lsoftware/simplicial/a/az;->a:Lsoftware/simplicial/a/am;

    .line 4034
    const-string v2, "DotsEaten"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->c:I

    .line 4035
    const-string v2, "BlobsEaten"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->q:I

    .line 4036
    const-string v2, "BlobsLost"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->r:I

    .line 4037
    const-string v2, "BiggestBlob"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->u:I

    .line 4038
    const-string v2, "MassGained"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v4, Lsoftware/simplicial/a/az;->s:J

    .line 4039
    const-string v2, "MassEjected"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v4, Lsoftware/simplicial/a/az;->t:J

    .line 4040
    const-string v2, "EjectCount"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->D:I

    .line 4041
    const-string v2, "SplitCount"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->E:I

    .line 4042
    const-string v2, "AverageScore"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->w:I

    .line 4043
    const-string v2, "HighestScore"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->v:I

    .line 4044
    const-string v2, "TimesRestarted"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->x:I

    .line 4045
    const-string v2, "LongestLifeMS"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v4, Lsoftware/simplicial/a/az;->y:J

    .line 4046
    const-string v2, "GamesWon"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->z:I

    .line 4047
    const-string v2, "ArenasWon"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->A:I

    .line 4048
    const-string v2, "CWsWon"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->B:I

    .line 4049
    const-string v2, "MatchesWon"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->C:I

    .line 4050
    const-string v2, "XP"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v4, Lsoftware/simplicial/a/az;->b:J

    .line 4051
    const-string v2, "CoinsCollected"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->p:I

    .line 4052
    const-string v2, "SMBHCollidedCount"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->G:I

    .line 4053
    const-string v2, "SMBHEatenCount"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->F:I

    .line 4054
    const-string v2, "BHCollidedCount"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->H:I

    .line 4055
    const-string v2, "TBHCollidedCount"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->I:I

    .line 4056
    const-string v2, "TimesTeleported"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->J:I

    .line 4057
    const-string v2, "PowerupsUsed"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->K:I

    .line 4058
    const/4 v7, 0x0

    .line 4059
    const-string v2, "Error"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "Error"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 4060
    const-string v2, "Error"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 4062
    :cond_0
    const-string v2, "SpecialObjects"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 4064
    const-string v2, "SpecialObjects"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 4065
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v2, v6, :cond_f

    .line 4067
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 4068
    const-string v9, "Type"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 4069
    const-string v10, "Count"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 4070
    if-eqz v9, :cond_1

    .line 4072
    const-string v10, "Pumpkins"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 4073
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v4, Lsoftware/simplicial/a/az;->d:I

    .line 4065
    :cond_1
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 4026
    :catch_0
    move-exception v2

    .line 4028
    sget-object v3, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    iput-object v3, v5, Lsoftware/simplicial/a/r;->c:Lsoftware/simplicial/a/q;

    .line 4029
    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6, v2}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 4160
    :catch_1
    move-exception v2

    .line 4162
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/f/al$62;->c:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v3}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v3

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to parse stats result."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;)V

    .line 4163
    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    :cond_2
    move/from16 v2, v18

    .line 4167
    :goto_3
    if-nez v2, :cond_3

    .line 4168
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/al$62;->b:Lsoftware/simplicial/a/b;

    const-string v3, "Stats could not be loaded"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface {v2, v3, v4, v5}, Lsoftware/simplicial/a/b;->a(Ljava/lang/String;ZLjava/lang/String;)V

    .line 4169
    :cond_3
    return-void

    .line 4074
    :cond_4
    :try_start_3
    const-string v10, "Leaves"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 4075
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v4, Lsoftware/simplicial/a/az;->e:I

    goto :goto_2

    .line 4076
    :cond_5
    const-string v10, "Presents"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 4077
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v4, Lsoftware/simplicial/a/az;->f:I

    goto :goto_2

    .line 4078
    :cond_6
    const-string v10, "Snowflakes"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 4079
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v4, Lsoftware/simplicial/a/az;->g:I

    goto :goto_2

    .line 4080
    :cond_7
    const-string v10, "Beads"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 4081
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v4, Lsoftware/simplicial/a/az;->h:I

    goto/16 :goto_2

    .line 4082
    :cond_8
    const-string v10, "Eggs"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 4083
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v4, Lsoftware/simplicial/a/az;->i:I

    goto/16 :goto_2

    .line 4084
    :cond_9
    const-string v10, "Drops"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 4085
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v4, Lsoftware/simplicial/a/az;->j:I

    goto/16 :goto_2

    .line 4086
    :cond_a
    const-string v10, "Nebulas"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 4087
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v4, Lsoftware/simplicial/a/az;->k:I

    goto/16 :goto_2

    .line 4088
    :cond_b
    const-string v10, "Candies"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 4089
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v4, Lsoftware/simplicial/a/az;->l:I

    goto/16 :goto_2

    .line 4090
    :cond_c
    const-string v10, "Suns"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_d

    .line 4091
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v4, Lsoftware/simplicial/a/az;->m:I

    goto/16 :goto_2

    .line 4092
    :cond_d
    const-string v10, "Moons"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 4093
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v4, Lsoftware/simplicial/a/az;->n:I

    goto/16 :goto_2

    .line 4094
    :cond_e
    const-string v10, "Notes"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 4095
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v4, Lsoftware/simplicial/a/az;->o:I

    goto/16 :goto_2

    .line 4100
    :cond_f
    const-string v2, "AchievementsEarned"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 4101
    const/4 v2, 0x0

    move v3, v2

    :goto_4
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v3, v2, :cond_11

    .line 4103
    sget-object v2, Lsoftware/simplicial/a/d;->bv:Ljava/util/Map;

    invoke-virtual {v6, v3}, Lorg/json/JSONArray;->getInt(I)I

    move-result v9

    int-to-short v9, v9

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-interface {v2, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/d;

    .line 4104
    if-eqz v2, :cond_10

    .line 4105
    iget-object v9, v4, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    invoke-interface {v9, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 4101
    :cond_10
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_4

    .line 4108
    :cond_11
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 4109
    const-string v2, "PurchasedAvatars"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 4110
    const/4 v2, 0x0

    :goto_5
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v2, v9, :cond_13

    .line 4112
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getInt(I)I

    move-result v9

    .line 4113
    if-ltz v9, :cond_12

    sget-object v10, Lsoftware/simplicial/a/e;->a:[Lsoftware/simplicial/a/e;

    array-length v10, v10

    if-ge v9, v10, :cond_12

    .line 4114
    sget-object v10, Lsoftware/simplicial/a/e;->a:[Lsoftware/simplicial/a/e;

    aget-object v9, v10, v9

    invoke-interface {v6, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 4110
    :cond_12
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 4117
    :cond_13
    new-instance v13, Ljava/util/HashSet;

    invoke-direct {v13}, Ljava/util/HashSet;-><init>()V

    .line 4118
    const-string v2, "PurchasedEjectSkins"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 4119
    const/4 v2, 0x0

    :goto_6
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v2, v9, :cond_14

    .line 4121
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getInt(I)I

    move-result v9

    .line 4122
    invoke-static {v9}, Lsoftware/simplicial/a/af;->a(I)Lsoftware/simplicial/a/af;

    move-result-object v9

    invoke-interface {v13, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 4119
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 4125
    :cond_14
    new-instance v16, Ljava/util/HashMap;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashMap;-><init>()V

    .line 4126
    const-string v2, "PurchasedPets"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 4127
    const/4 v2, 0x0

    :goto_7
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v2, v9, :cond_15

    .line 4129
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v9

    .line 4130
    const-string v10, "ID"

    invoke-virtual {v9, v10}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v10

    int-to-byte v10, v10

    .line 4131
    const-string v11, "XP"

    invoke-virtual {v9, v11}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v14

    .line 4132
    invoke-static {v10, v14, v15}, Lsoftware/simplicial/a/bd;->a(IJ)Lsoftware/simplicial/a/bd;

    move-result-object v9

    .line 4133
    iget-byte v10, v9, Lsoftware/simplicial/a/bd;->c:B

    invoke-static {v10}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v10

    move-object/from16 v0, v16

    invoke-interface {v0, v10, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4127
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 4136
    :cond_15
    new-instance v17, Ljava/util/HashSet;

    invoke-direct/range {v17 .. v17}, Ljava/util/HashSet;-><init>()V

    .line 4137
    const-string v2, "PurchasedHats"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 4138
    const/4 v2, 0x0

    :goto_8
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v2, v9, :cond_16

    .line 4140
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getInt(I)I

    move-result v9

    .line 4141
    invoke-static {v9}, Lsoftware/simplicial/a/as;->a(I)Lsoftware/simplicial/a/as;

    move-result-object v9

    move-object/from16 v0, v17

    invoke-interface {v0, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 4138
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 4144
    :cond_16
    new-instance v15, Ljava/util/HashSet;

    invoke-direct {v15}, Ljava/util/HashSet;-><init>()V

    .line 4145
    const-string v2, "ValidCustomSkinIDs"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 4146
    const/4 v2, 0x0

    :goto_9
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v2, v9, :cond_17

    .line 4147
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getInt(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v15, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 4146
    add-int/lit8 v2, v2, 0x1

    goto :goto_9

    .line 4149
    :cond_17
    const-string v2, "PurchasedSkinMap"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v12

    .line 4151
    const-string v2, "XPMultiplier"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v9

    .line 4152
    const-string v2, "XPMultiplierDurationRemainingS"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 4154
    const/4 v3, 0x1

    iput-boolean v3, v4, Lsoftware/simplicial/a/az;->M:Z

    .line 4156
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/f/al$62;->b:Lsoftware/simplicial/a/b;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    int-to-long v0, v2

    move-wide/from16 v20, v0

    const-wide/16 v22, 0x3e8

    mul-long v20, v20, v22

    add-long v10, v10, v20

    const/4 v14, 0x1

    invoke-interface/range {v3 .. v17}, Lsoftware/simplicial/a/b;->a(Lsoftware/simplicial/a/az;Lsoftware/simplicial/a/r;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;IJZLjava/util/Set;ZLjava/util/Set;Ljava/util/Map;Ljava/util/Set;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 4158
    const/4 v2, 0x1

    goto/16 :goto_3
.end method
