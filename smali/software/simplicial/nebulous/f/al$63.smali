.class Lsoftware/simplicial/nebulous/f/al$63;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$z;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/a/am;Ljava/lang/String;ILsoftware/simplicial/a/b;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/a/am;

.field final synthetic b:Lsoftware/simplicial/a/b;

.field final synthetic c:Lsoftware/simplicial/nebulous/f/al;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/f/al;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/b;)V
    .locals 0

    .prologue
    .line 4188
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/al$63;->c:Lsoftware/simplicial/nebulous/f/al;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/f/al$63;->a:Lsoftware/simplicial/a/am;

    iput-object p3, p0, Lsoftware/simplicial/nebulous/f/al$63;->b:Lsoftware/simplicial/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;)V
    .locals 19

    .prologue
    .line 4192
    const/16 v18, 0x0

    .line 4193
    if-eqz p1, :cond_2

    .line 4195
    new-instance v4, Lsoftware/simplicial/a/az;

    invoke-direct {v4}, Lsoftware/simplicial/a/az;-><init>()V

    .line 4196
    new-instance v5, Lsoftware/simplicial/a/r;

    invoke-direct {v5}, Lsoftware/simplicial/a/r;-><init>()V

    .line 4197
    const-string v2, ""

    .line 4200
    :try_start_0
    const-string v2, "AccountName"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 4201
    const-string v2, "ClanName"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Lsoftware/simplicial/a/r;->a:Ljava/lang/String;

    .line 4202
    invoke-static/range {p1 .. p1}, Lsoftware/simplicial/nebulous/f/al;->a(Lorg/json/JSONObject;)[B

    move-result-object v2

    iput-object v2, v5, Lsoftware/simplicial/a/r;->b:[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 4205
    :try_start_1
    const-string v2, "ClanRole"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lsoftware/simplicial/a/q;->a(Ljava/lang/String;)Lsoftware/simplicial/a/q;

    move-result-object v2

    iput-object v2, v5, Lsoftware/simplicial/a/r;->c:Lsoftware/simplicial/a/q;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 4212
    :goto_0
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/al$63;->a:Lsoftware/simplicial/a/am;

    iput-object v2, v4, Lsoftware/simplicial/a/az;->a:Lsoftware/simplicial/a/am;

    .line 4214
    const-string v2, "DotsEaten"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->c:I

    .line 4215
    const-string v2, "BlobsEaten"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->q:I

    .line 4216
    const-string v2, "BlobsLost"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->r:I

    .line 4217
    const-string v2, "BiggestBlob"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->u:I

    .line 4218
    const-string v2, "MassGained"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v4, Lsoftware/simplicial/a/az;->s:J

    .line 4219
    const-string v2, "MassEjected"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v4, Lsoftware/simplicial/a/az;->t:J

    .line 4220
    const-string v2, "EjectCount"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->D:I

    .line 4221
    const-string v2, "SplitCount"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->E:I

    .line 4222
    const-string v2, "AverageScore"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->w:I

    .line 4223
    const-string v2, "HighestScore"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->v:I

    .line 4224
    const-string v2, "TimesRestarted"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->x:I

    .line 4225
    const-string v2, "LongestLifeMS"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v4, Lsoftware/simplicial/a/az;->y:J

    .line 4226
    const-string v2, "GamesWon"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->z:I

    .line 4227
    const-string v2, "XP"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v4, Lsoftware/simplicial/a/az;->b:J

    .line 4228
    const-string v2, "CoinsCollected"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->p:I

    .line 4229
    const-string v2, "SMBHCollidedCount"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->G:I

    .line 4230
    const-string v2, "SMBHEatenCount"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->F:I

    .line 4231
    const-string v2, "BHCollidedCount"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->H:I

    .line 4232
    const-string v2, "TBHCollidedCount"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->I:I

    .line 4233
    const-string v2, "TimesTeleported"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->J:I

    .line 4234
    const-string v2, "PowerupsUsed"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lsoftware/simplicial/a/az;->K:I

    .line 4235
    const/4 v7, 0x0

    .line 4236
    const-string v2, "Error"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "Error"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 4237
    const-string v2, "Error"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 4239
    :cond_0
    const-string v2, "SpecialObjects"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 4241
    const-string v2, "SpecialObjects"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 4242
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v2, v6, :cond_f

    .line 4244
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 4245
    const-string v9, "Type"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 4246
    const-string v10, "Count"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 4247
    if-eqz v9, :cond_1

    .line 4249
    const-string v10, "Pumpkins"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 4250
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v4, Lsoftware/simplicial/a/az;->d:I

    .line 4242
    :cond_1
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 4207
    :catch_0
    move-exception v2

    .line 4209
    sget-object v3, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    iput-object v3, v5, Lsoftware/simplicial/a/r;->c:Lsoftware/simplicial/a/q;

    .line 4210
    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6, v2}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 4292
    :catch_1
    move-exception v2

    .line 4294
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/f/al$63;->c:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v3}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v3

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to parse stats result."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;)V

    .line 4295
    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    :cond_2
    move/from16 v2, v18

    .line 4299
    :goto_3
    if-nez v2, :cond_3

    .line 4300
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/al$63;->b:Lsoftware/simplicial/a/b;

    const-string v3, "Stats could not be loaded"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface {v2, v3, v4, v5}, Lsoftware/simplicial/a/b;->a(Ljava/lang/String;ZLjava/lang/String;)V

    .line 4301
    :cond_3
    return-void

    .line 4251
    :cond_4
    :try_start_3
    const-string v10, "Leaves"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 4252
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v4, Lsoftware/simplicial/a/az;->e:I

    goto :goto_2

    .line 4253
    :cond_5
    const-string v10, "Presents"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 4254
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v4, Lsoftware/simplicial/a/az;->f:I

    goto :goto_2

    .line 4255
    :cond_6
    const-string v10, "Snowflakes"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 4256
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v4, Lsoftware/simplicial/a/az;->g:I

    goto :goto_2

    .line 4257
    :cond_7
    const-string v10, "Beads"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 4258
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v4, Lsoftware/simplicial/a/az;->h:I

    goto/16 :goto_2

    .line 4259
    :cond_8
    const-string v10, "Eggs"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 4260
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v4, Lsoftware/simplicial/a/az;->i:I

    goto/16 :goto_2

    .line 4261
    :cond_9
    const-string v10, "Drops"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 4262
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v4, Lsoftware/simplicial/a/az;->j:I

    goto/16 :goto_2

    .line 4263
    :cond_a
    const-string v10, "Nebulas"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 4264
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v4, Lsoftware/simplicial/a/az;->k:I

    goto/16 :goto_2

    .line 4265
    :cond_b
    const-string v10, "Candies"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 4266
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v4, Lsoftware/simplicial/a/az;->l:I

    goto/16 :goto_2

    .line 4267
    :cond_c
    const-string v10, "Suns"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_d

    .line 4268
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v4, Lsoftware/simplicial/a/az;->m:I

    goto/16 :goto_2

    .line 4269
    :cond_d
    const-string v10, "Moons"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 4270
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v4, Lsoftware/simplicial/a/az;->n:I

    goto/16 :goto_2

    .line 4271
    :cond_e
    const-string v10, "Notes"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 4272
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v4, Lsoftware/simplicial/a/az;->o:I

    goto/16 :goto_2

    .line 4277
    :cond_f
    const-string v2, "AchievementsEarned"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 4278
    const/4 v2, 0x0

    move v3, v2

    :goto_4
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v3, v2, :cond_11

    .line 4280
    sget-object v2, Lsoftware/simplicial/a/d;->bv:Ljava/util/Map;

    invoke-virtual {v6, v3}, Lorg/json/JSONArray;->getInt(I)I

    move-result v9

    int-to-short v9, v9

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-interface {v2, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/d;

    .line 4281
    if-eqz v2, :cond_10

    .line 4282
    iget-object v9, v4, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    invoke-interface {v9, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 4278
    :cond_10
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_4

    .line 4287
    :cond_11
    const/4 v2, 0x1

    iput-boolean v2, v4, Lsoftware/simplicial/a/az;->M:Z

    .line 4289
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/f/al$63;->b:Lsoftware/simplicial/a/b;

    const/4 v6, 0x0

    const/4 v9, 0x1

    const-wide/16 v10, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x1

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-interface/range {v3 .. v17}, Lsoftware/simplicial/a/b;->a(Lsoftware/simplicial/a/az;Lsoftware/simplicial/a/r;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;IJZLjava/util/Set;ZLjava/util/Set;Ljava/util/Map;Ljava/util/Set;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 4290
    const/4 v2, 0x1

    goto/16 :goto_3
.end method
