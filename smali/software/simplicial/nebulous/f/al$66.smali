.class Lsoftware/simplicial/nebulous/f/al$66;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$z;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/f/al;->a(IZLsoftware/simplicial/a/bn;IIZZLsoftware/simplicial/nebulous/f/w;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/f/w;

.field final synthetic b:Lsoftware/simplicial/nebulous/f/al;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/f/al;Lsoftware/simplicial/nebulous/f/w;)V
    .locals 0

    .prologue
    .line 4450
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/al$66;->b:Lsoftware/simplicial/nebulous/f/al;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/f/al$66;->a:Lsoftware/simplicial/nebulous/f/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 4454
    if-eqz p1, :cond_3

    .line 4457
    const-string v1, ""

    .line 4461
    :try_start_0
    const-string v2, "Error"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "Error"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 4462
    const-string v2, "Error"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 4464
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 4466
    const-string v2, "Names"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 4467
    const-string v2, "Winnings"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 4468
    const-string v2, "StartIndex"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 4469
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v7

    move v2, v0

    .line 4470
    :goto_0
    if-ge v2, v7, :cond_1

    .line 4471
    new-instance v8, Lsoftware/simplicial/a/g/g;

    const-string v9, "ERROR"

    const/4 v10, 0x0

    invoke-direct {v8, v9, v10}, Lsoftware/simplicial/a/g/g;-><init>(Ljava/lang/String;I)V

    invoke-interface {v3, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4470
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v2, v0

    .line 4472
    :goto_1
    if-ge v2, v7, :cond_2

    .line 4474
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/g/g;

    .line 4476
    invoke-virtual {v4, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v0, Lsoftware/simplicial/a/g/g;->a:Ljava/lang/String;

    .line 4477
    invoke-virtual {v5, v2}, Lorg/json/JSONArray;->getInt(I)I

    move-result v8

    iput v8, v0, Lsoftware/simplicial/a/g/g;->b:I

    .line 4478
    add-int v8, v6, v2

    add-int/lit8 v8, v8, 0x1

    iput v8, v0, Lsoftware/simplicial/a/g/g;->c:I

    .line 4472
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 4481
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$66;->a:Lsoftware/simplicial/nebulous/f/w;

    invoke-interface {v0, v3}, Lsoftware/simplicial/nebulous/f/w;->c(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 4489
    :cond_3
    :goto_2
    return-void

    .line 4483
    :catch_0
    move-exception v0

    .line 4485
    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/al$66;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to parse RequestTourneyLB. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;)V

    .line 4486
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_2
.end method
