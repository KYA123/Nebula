.class Lsoftware/simplicial/nebulous/f/al$68;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$z;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/f/al;->b(ILsoftware/simplicial/a/g/f;IILsoftware/simplicial/nebulous/f/w;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/f/w;

.field final synthetic b:Lsoftware/simplicial/nebulous/f/al;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/f/al;Lsoftware/simplicial/nebulous/f/w;)V
    .locals 0

    .prologue
    .line 4510
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/al$68;->b:Lsoftware/simplicial/nebulous/f/al;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/f/al$68;->a:Lsoftware/simplicial/nebulous/f/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;)V
    .locals 14

    .prologue
    const/4 v3, 0x0

    .line 4514
    if-eqz p1, :cond_4

    .line 4517
    const-string v1, ""

    .line 4521
    :try_start_0
    const-string v0, "Error"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Error"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4522
    const-string v0, "Error"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 4524
    :cond_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 4526
    const-string v0, "Names"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 4527
    const-string v0, "NameColors"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 4528
    const-string v0, "XPs"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v8

    .line 4529
    const-string v0, "StartIndex"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v9

    .line 4530
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v10

    move v0, v3

    .line 4531
    :goto_0
    if-ge v0, v10, :cond_1

    .line 4532
    new-instance v2, Lsoftware/simplicial/a/g/h;

    const-string v4, "ERROR"

    const/4 v11, 0x0

    new-array v11, v11, [B

    const-wide/16 v12, 0x0

    invoke-direct {v2, v4, v11, v12, v13}, Lsoftware/simplicial/a/g/h;-><init>(Ljava/lang/String;[BJ)V

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4531
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v4, v3

    .line 4533
    :goto_1
    if-ge v4, v10, :cond_3

    .line 4535
    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/g/h;

    .line 4537
    invoke-virtual {v6, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lsoftware/simplicial/a/g/h;->a:Ljava/lang/String;

    .line 4538
    invoke-virtual {v7, v4}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v11

    .line 4539
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v2

    new-array v12, v2, [B

    move v2, v3

    .line 4540
    :goto_2
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v13

    if-ge v2, v13, :cond_2

    .line 4541
    invoke-virtual {v11, v2}, Lorg/json/JSONArray;->getInt(I)I

    move-result v13

    int-to-byte v13, v13

    aput-byte v13, v12, v2

    .line 4540
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 4542
    :cond_2
    iput-object v12, v0, Lsoftware/simplicial/a/g/h;->b:[B

    .line 4543
    invoke-virtual {v8, v4}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v12

    iput-wide v12, v0, Lsoftware/simplicial/a/g/h;->c:J

    .line 4544
    add-int v2, v9, v4

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lsoftware/simplicial/a/g/h;->d:I

    .line 4533
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 4547
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$68;->a:Lsoftware/simplicial/nebulous/f/w;

    invoke-interface {v0, v5}, Lsoftware/simplicial/nebulous/f/w;->d(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 4555
    :cond_4
    :goto_3
    return-void

    .line 4549
    :catch_0
    move-exception v0

    .line 4551
    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/al$68;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to parse RequestClanXPLB. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;)V

    .line 4552
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_3
.end method
