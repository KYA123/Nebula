.class public Lsoftware/simplicial/nebulous/f/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/mopub/mobileads/MoPubInterstitial$InterstitialAdListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/nebulous/f/b$a;
    }
.end annotation


# instance fields
.field a:Lsoftware/simplicial/nebulous/f/c;

.field b:Lcom/mopub/mobileads/MoPubInterstitial;

.field c:Lcom/mopub/mobileads/MoPubInterstitial;

.field d:Lcom/mopub/mobileads/MoPubInterstitial;

.field e:Lcom/mopub/mobileads/MoPubInterstitial;

.field f:Lcom/mopub/mobileads/MoPubInterstitial;

.field g:Lcom/mopub/mobileads/MoPubInterstitial;

.field h:Lcom/mopub/mobileads/MoPubInterstitial;

.field i:Lcom/mopub/mobileads/MoPubInterstitial;

.field j:Lcom/mopub/mobileads/MoPubInterstitial;

.field k:Lcom/mopub/mobileads/MoPubInterstitial;

.field l:Lcom/mopub/mobileads/MoPubInterstitial;

.field m:Lcom/mopub/mobileads/MoPubInterstitial;

.field n:Lcom/mopub/mobileads/MoPubInterstitial;

.field public o:Z

.field private final p:Lsoftware/simplicial/nebulous/f/e;

.field private q:I

.field private r:J

.field private s:J

.field private t:Lsoftware/simplicial/nebulous/application/MainActivity;

.field private u:Lsoftware/simplicial/nebulous/f/b$a;

.field private v:Z

.field private w:J


# direct methods
.method public constructor <init>(Lsoftware/simplicial/nebulous/f/b$a;Lsoftware/simplicial/nebulous/f/e;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    sget-object v0, Lsoftware/simplicial/nebulous/f/c;->b:Lsoftware/simplicial/nebulous/f/c;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->a:Lsoftware/simplicial/nebulous/f/c;

    .line 55
    iput-object v3, p0, Lsoftware/simplicial/nebulous/f/b;->n:Lcom/mopub/mobileads/MoPubInterstitial;

    .line 56
    iput v2, p0, Lsoftware/simplicial/nebulous/f/b;->q:I

    .line 57
    iput-wide v4, p0, Lsoftware/simplicial/nebulous/f/b;->r:J

    .line 58
    const-wide/32 v0, 0x1d4c0

    iput-wide v0, p0, Lsoftware/simplicial/nebulous/f/b;->s:J

    .line 59
    iput-object v3, p0, Lsoftware/simplicial/nebulous/f/b;->t:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 67
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/f/b;->v:Z

    .line 68
    iput-wide v4, p0, Lsoftware/simplicial/nebulous/f/b;->w:J

    .line 69
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/f/b;->o:Z

    .line 73
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/b;->u:Lsoftware/simplicial/nebulous/f/b$a;

    .line 74
    iput-object p2, p0, Lsoftware/simplicial/nebulous/f/b;->p:Lsoftware/simplicial/nebulous/f/e;

    .line 75
    return-void
.end method


# virtual methods
.method public a()Lsoftware/simplicial/nebulous/f/c;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->a:Lsoftware/simplicial/nebulous/f/c;

    return-object v0
.end method

.method public declared-synchronized a(Landroid/app/Activity;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 161
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/f/b;->v:Z

    if-eqz v0, :cond_0

    .line 163
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lsoftware/simplicial/nebulous/f/b;->w:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 165
    sget-object v0, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v1, "LoadAd ignored due to rapid requests"

    invoke-static {v0, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 246
    :goto_0
    monitor-exit p0

    return-void

    .line 172
    :cond_0
    :try_start_1
    iget v0, p0, Lsoftware/simplicial/nebulous/f/b;->q:I

    if-lez v0, :cond_2

    .line 173
    const-wide/32 v0, 0x13880

    iput-wide v0, p0, Lsoftware/simplicial/nebulous/f/b;->s:J

    .line 176
    :goto_1
    sget-object v0, Lsoftware/simplicial/nebulous/f/c;->a:Lsoftware/simplicial/nebulous/f/c;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->a:Lsoftware/simplicial/nebulous/f/c;

    .line 177
    iget v0, p0, Lsoftware/simplicial/nebulous/f/b;->q:I

    rem-int/lit8 v0, v0, 0x3

    if-nez v0, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->d:Lcom/mopub/mobileads/MoPubInterstitial;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->e:Lcom/mopub/mobileads/MoPubInterstitial;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->j:Lcom/mopub/mobileads/MoPubInterstitial;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->k:Lcom/mopub/mobileads/MoPubInterstitial;

    if-eqz v0, :cond_1

    .line 178
    if-eqz p2, :cond_3

    sget-object v0, Lsoftware/simplicial/nebulous/f/c;->c:Lsoftware/simplicial/nebulous/f/c;

    :goto_2
    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->a:Lsoftware/simplicial/nebulous/f/c;

    .line 180
    :cond_1
    sget-object v0, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v1, "LoadAd %s %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lsoftware/simplicial/nebulous/f/b;->a:Lsoftware/simplicial/nebulous/f/c;

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/f/c;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lsoftware/simplicial/nebulous/f/b;->q:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 182
    sget-object v0, Lsoftware/simplicial/nebulous/f/b$1;->a:[I

    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/b;->a:Lsoftware/simplicial/nebulous/f/c;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 234
    :goto_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->n:Lcom/mopub/mobileads/MoPubInterstitial;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-nez v0, :cond_d

    .line 244
    :try_start_2
    iget v0, p0, Lsoftware/simplicial/nebulous/f/b;->q:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/f/b;->q:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 161
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 174
    :cond_2
    const-wide/32 v0, 0x1d4c0

    :try_start_3
    iput-wide v0, p0, Lsoftware/simplicial/nebulous/f/b;->s:J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    .line 244
    :catchall_1
    move-exception v0

    :try_start_4
    iget v1, p0, Lsoftware/simplicial/nebulous/f/b;->q:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lsoftware/simplicial/nebulous/f/b;->q:I

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 178
    :cond_3
    :try_start_5
    sget-object v0, Lsoftware/simplicial/nebulous/f/c;->b:Lsoftware/simplicial/nebulous/f/c;

    goto :goto_2

    .line 185
    :pswitch_0
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v5, :cond_5

    .line 187
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 188
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->h:Lcom/mopub/mobileads/MoPubInterstitial;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->n:Lcom/mopub/mobileads/MoPubInterstitial;

    goto :goto_3

    .line 190
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->b:Lcom/mopub/mobileads/MoPubInterstitial;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->n:Lcom/mopub/mobileads/MoPubInterstitial;

    goto :goto_3

    .line 194
    :cond_5
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 195
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->i:Lcom/mopub/mobileads/MoPubInterstitial;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->n:Lcom/mopub/mobileads/MoPubInterstitial;

    goto :goto_3

    .line 197
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->c:Lcom/mopub/mobileads/MoPubInterstitial;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->n:Lcom/mopub/mobileads/MoPubInterstitial;

    goto :goto_3

    .line 201
    :pswitch_1
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v5, :cond_8

    .line 203
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 204
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->j:Lcom/mopub/mobileads/MoPubInterstitial;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->n:Lcom/mopub/mobileads/MoPubInterstitial;

    goto :goto_3

    .line 206
    :cond_7
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->d:Lcom/mopub/mobileads/MoPubInterstitial;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->n:Lcom/mopub/mobileads/MoPubInterstitial;

    goto :goto_3

    .line 210
    :cond_8
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 211
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->k:Lcom/mopub/mobileads/MoPubInterstitial;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->n:Lcom/mopub/mobileads/MoPubInterstitial;

    goto/16 :goto_3

    .line 213
    :cond_9
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->e:Lcom/mopub/mobileads/MoPubInterstitial;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->n:Lcom/mopub/mobileads/MoPubInterstitial;

    goto/16 :goto_3

    .line 217
    :pswitch_2
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v5, :cond_b

    .line 219
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 220
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->l:Lcom/mopub/mobileads/MoPubInterstitial;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->n:Lcom/mopub/mobileads/MoPubInterstitial;

    goto/16 :goto_3

    .line 222
    :cond_a
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->f:Lcom/mopub/mobileads/MoPubInterstitial;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->n:Lcom/mopub/mobileads/MoPubInterstitial;

    goto/16 :goto_3

    .line 226
    :cond_b
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 227
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->m:Lcom/mopub/mobileads/MoPubInterstitial;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->n:Lcom/mopub/mobileads/MoPubInterstitial;

    goto/16 :goto_3

    .line 229
    :cond_c
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->g:Lcom/mopub/mobileads/MoPubInterstitial;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->n:Lcom/mopub/mobileads/MoPubInterstitial;

    goto/16 :goto_3

    .line 236
    :cond_d
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/b;->v:Z

    .line 237
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lsoftware/simplicial/nebulous/f/b;->w:J

    .line 238
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->n:Lcom/mopub/mobileads/MoPubInterstitial;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mopub/mobileads/MoPubInterstitial;->setTesting(Z)V

    .line 240
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->n:Lcom/mopub/mobileads/MoPubInterstitial;

    invoke-virtual {v0}, Lcom/mopub/mobileads/MoPubInterstitial;->load()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 244
    :try_start_6
    iget v0, p0, Lsoftware/simplicial/nebulous/f/b;->q:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/f/b;->q:I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 182
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lsoftware/simplicial/nebulous/application/MainActivity;)V
    .locals 3

    .prologue
    .line 98
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/b;->t:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 102
    :try_start_0
    new-instance v0, Lcom/mopub/mobileads/MoPubInterstitial;

    const-string v1, "e508e065672d4a59a6b9484b9c8daecf"

    invoke-direct {v0, p1, v1}, Lcom/mopub/mobileads/MoPubInterstitial;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->b:Lcom/mopub/mobileads/MoPubInterstitial;

    .line 103
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->b:Lcom/mopub/mobileads/MoPubInterstitial;

    invoke-virtual {v0, p0}, Lcom/mopub/mobileads/MoPubInterstitial;->setInterstitialAdListener(Lcom/mopub/mobileads/MoPubInterstitial$InterstitialAdListener;)V

    .line 104
    new-instance v0, Lcom/mopub/mobileads/MoPubInterstitial;

    const-string v1, "b135c195eb6342b185956fe34defe316"

    invoke-direct {v0, p1, v1}, Lcom/mopub/mobileads/MoPubInterstitial;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->c:Lcom/mopub/mobileads/MoPubInterstitial;

    .line 105
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->c:Lcom/mopub/mobileads/MoPubInterstitial;

    invoke-virtual {v0, p0}, Lcom/mopub/mobileads/MoPubInterstitial;->setInterstitialAdListener(Lcom/mopub/mobileads/MoPubInterstitial$InterstitialAdListener;)V

    .line 106
    new-instance v0, Lcom/mopub/mobileads/MoPubInterstitial;

    const-string v1, "ef9bbb4fcb424f0ba62fa87b4688bdf2"

    invoke-direct {v0, p1, v1}, Lcom/mopub/mobileads/MoPubInterstitial;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->d:Lcom/mopub/mobileads/MoPubInterstitial;

    .line 107
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->d:Lcom/mopub/mobileads/MoPubInterstitial;

    invoke-virtual {v0, p0}, Lcom/mopub/mobileads/MoPubInterstitial;->setInterstitialAdListener(Lcom/mopub/mobileads/MoPubInterstitial$InterstitialAdListener;)V

    .line 108
    new-instance v0, Lcom/mopub/mobileads/MoPubInterstitial;

    const-string v1, "13e79a3b6582488f93c207e455d969e1"

    invoke-direct {v0, p1, v1}, Lcom/mopub/mobileads/MoPubInterstitial;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->e:Lcom/mopub/mobileads/MoPubInterstitial;

    .line 109
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->e:Lcom/mopub/mobileads/MoPubInterstitial;

    invoke-virtual {v0, p0}, Lcom/mopub/mobileads/MoPubInterstitial;->setInterstitialAdListener(Lcom/mopub/mobileads/MoPubInterstitial$InterstitialAdListener;)V

    .line 110
    new-instance v0, Lcom/mopub/mobileads/MoPubInterstitial;

    const-string v1, "b9ea2793f7e94bd3ac0bf0c74b81d8fd"

    invoke-direct {v0, p1, v1}, Lcom/mopub/mobileads/MoPubInterstitial;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->f:Lcom/mopub/mobileads/MoPubInterstitial;

    .line 111
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->f:Lcom/mopub/mobileads/MoPubInterstitial;

    invoke-virtual {v0, p0}, Lcom/mopub/mobileads/MoPubInterstitial;->setInterstitialAdListener(Lcom/mopub/mobileads/MoPubInterstitial$InterstitialAdListener;)V

    .line 112
    new-instance v0, Lcom/mopub/mobileads/MoPubInterstitial;

    const-string v1, "6c43edf0843b44a88eb2b3c1de6f519a"

    invoke-direct {v0, p1, v1}, Lcom/mopub/mobileads/MoPubInterstitial;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->g:Lcom/mopub/mobileads/MoPubInterstitial;

    .line 113
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->g:Lcom/mopub/mobileads/MoPubInterstitial;

    invoke-virtual {v0, p0}, Lcom/mopub/mobileads/MoPubInterstitial;->setInterstitialAdListener(Lcom/mopub/mobileads/MoPubInterstitial$InterstitialAdListener;)V

    .line 115
    new-instance v0, Lcom/mopub/mobileads/MoPubInterstitial;

    const-string v1, "2246ff5c79bd4fada2471fa48877a724"

    invoke-direct {v0, p1, v1}, Lcom/mopub/mobileads/MoPubInterstitial;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->h:Lcom/mopub/mobileads/MoPubInterstitial;

    .line 116
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->h:Lcom/mopub/mobileads/MoPubInterstitial;

    invoke-virtual {v0, p0}, Lcom/mopub/mobileads/MoPubInterstitial;->setInterstitialAdListener(Lcom/mopub/mobileads/MoPubInterstitial$InterstitialAdListener;)V

    .line 117
    new-instance v0, Lcom/mopub/mobileads/MoPubInterstitial;

    const-string v1, "37b38caefa054efc87e0df5e6412832d"

    invoke-direct {v0, p1, v1}, Lcom/mopub/mobileads/MoPubInterstitial;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->i:Lcom/mopub/mobileads/MoPubInterstitial;

    .line 118
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->i:Lcom/mopub/mobileads/MoPubInterstitial;

    invoke-virtual {v0, p0}, Lcom/mopub/mobileads/MoPubInterstitial;->setInterstitialAdListener(Lcom/mopub/mobileads/MoPubInterstitial$InterstitialAdListener;)V

    .line 119
    new-instance v0, Lcom/mopub/mobileads/MoPubInterstitial;

    const-string v1, "5d0d8a4123c3471c92bcd4df7184f187"

    invoke-direct {v0, p1, v1}, Lcom/mopub/mobileads/MoPubInterstitial;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->j:Lcom/mopub/mobileads/MoPubInterstitial;

    .line 120
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->j:Lcom/mopub/mobileads/MoPubInterstitial;

    invoke-virtual {v0, p0}, Lcom/mopub/mobileads/MoPubInterstitial;->setInterstitialAdListener(Lcom/mopub/mobileads/MoPubInterstitial$InterstitialAdListener;)V

    .line 121
    new-instance v0, Lcom/mopub/mobileads/MoPubInterstitial;

    const-string v1, "3a6e504398344da9a93edf8f690ace95"

    invoke-direct {v0, p1, v1}, Lcom/mopub/mobileads/MoPubInterstitial;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->k:Lcom/mopub/mobileads/MoPubInterstitial;

    .line 122
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->k:Lcom/mopub/mobileads/MoPubInterstitial;

    invoke-virtual {v0, p0}, Lcom/mopub/mobileads/MoPubInterstitial;->setInterstitialAdListener(Lcom/mopub/mobileads/MoPubInterstitial$InterstitialAdListener;)V

    .line 123
    new-instance v0, Lcom/mopub/mobileads/MoPubInterstitial;

    const-string v1, "15d6ef7f70714a1992b1ea1e1639a972"

    invoke-direct {v0, p1, v1}, Lcom/mopub/mobileads/MoPubInterstitial;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->l:Lcom/mopub/mobileads/MoPubInterstitial;

    .line 124
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->l:Lcom/mopub/mobileads/MoPubInterstitial;

    invoke-virtual {v0, p0}, Lcom/mopub/mobileads/MoPubInterstitial;->setInterstitialAdListener(Lcom/mopub/mobileads/MoPubInterstitial$InterstitialAdListener;)V

    .line 125
    new-instance v0, Lcom/mopub/mobileads/MoPubInterstitial;

    const-string v1, "e7cafe28a089489ebfc569db9f0319d2"

    invoke-direct {v0, p1, v1}, Lcom/mopub/mobileads/MoPubInterstitial;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->m:Lcom/mopub/mobileads/MoPubInterstitial;

    .line 126
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->m:Lcom/mopub/mobileads/MoPubInterstitial;

    invoke-virtual {v0, p0}, Lcom/mopub/mobileads/MoPubInterstitial;->setInterstitialAdListener(Lcom/mopub/mobileads/MoPubInterstitial$InterstitialAdListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lsoftware/simplicial/nebulous/f/b;->r:J

    .line 134
    return-void

    .line 128
    :catch_0
    move-exception v0

    .line 130
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public declared-synchronized a(FII)Z
    .locals 8

    .prologue
    const/high16 v0, 0x40000000    # 2.0f

    const/high16 v1, 0x3f800000    # 1.0f

    .line 250
    monitor-enter p0

    mul-int/lit16 v2, p2, 0x3e8

    :try_start_0
    div-int v3, v2, p3

    .line 251
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lsoftware/simplicial/nebulous/f/b;->r:J

    sub-long/2addr v4, v6

    .line 256
    invoke-static {p1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p1}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_0
    move v2, v1

    .line 258
    :goto_0
    cmpg-float v6, v2, v1

    if-gez v6, :cond_4

    .line 260
    :goto_1
    cmpl-float v2, v1, v0

    if-lez v2, :cond_3

    .line 263
    :goto_2
    long-to-float v1, v4

    iget-wide v4, p0, Lsoftware/simplicial/nebulous/f/b;->s:J

    long-to-float v2, v4

    div-float/2addr v2, v0

    cmpl-float v1, v1, v2

    if-gez v1, :cond_1

    int-to-float v1, v3

    iget-wide v2, p0, Lsoftware/simplicial/nebulous/f/b;->s:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    long-to-float v2, v2

    div-float v0, v2, v0

    cmpl-float v0, v1, v0

    if-ltz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_3
    monitor-exit p0

    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_3

    .line 250
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    move v1, v2

    goto :goto_1

    :cond_5
    move v2, p1

    goto :goto_0
.end method

.method public declared-synchronized b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 146
    monitor-enter p0

    :try_start_0
    sget-object v1, Lsoftware/simplicial/nebulous/f/b$1;->a:[I

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/b;->a:Lsoftware/simplicial/nebulous/f/c;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/f/c;->ordinal()I

    move-result v2

    aget v1, v1, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v1, :pswitch_data_0

    .line 156
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 151
    :pswitch_0
    :try_start_1
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/b;->n:Lcom/mopub/mobileads/MoPubInterstitial;

    if-eqz v1, :cond_0

    .line 153
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->p:Lsoftware/simplicial/nebulous/f/e;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/b;->a:Lsoftware/simplicial/nebulous/f/c;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/f/e;->a(Lsoftware/simplicial/nebulous/f/c;)V

    .line 154
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->n:Lcom/mopub/mobileads/MoPubInterstitial;

    invoke-virtual {v0}, Lcom/mopub/mobileads/MoPubInterstitial;->show()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 146
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public declared-synchronized c()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 270
    monitor-enter p0

    :try_start_0
    sget-object v1, Lsoftware/simplicial/nebulous/f/b$1;->a:[I

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/b;->a:Lsoftware/simplicial/nebulous/f/c;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/f/c;->ordinal()I

    move-result v2

    aget v1, v1, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v1, :pswitch_data_0

    .line 277
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 275
    :pswitch_0
    :try_start_1
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/b;->n:Lcom/mopub/mobileads/MoPubInterstitial;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/b;->n:Lcom/mopub/mobileads/MoPubInterstitial;

    invoke-virtual {v1}, Lcom/mopub/mobileads/MoPubInterstitial;->isReady()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 270
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public declared-synchronized d()V
    .locals 1

    .prologue
    .line 282
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->b:Lcom/mopub/mobileads/MoPubInterstitial;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->b:Lcom/mopub/mobileads/MoPubInterstitial;

    invoke-virtual {v0}, Lcom/mopub/mobileads/MoPubInterstitial;->destroy()V

    .line 283
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->c:Lcom/mopub/mobileads/MoPubInterstitial;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->c:Lcom/mopub/mobileads/MoPubInterstitial;

    invoke-virtual {v0}, Lcom/mopub/mobileads/MoPubInterstitial;->destroy()V

    .line 284
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->h:Lcom/mopub/mobileads/MoPubInterstitial;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->h:Lcom/mopub/mobileads/MoPubInterstitial;

    invoke-virtual {v0}, Lcom/mopub/mobileads/MoPubInterstitial;->destroy()V

    .line 285
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->i:Lcom/mopub/mobileads/MoPubInterstitial;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->i:Lcom/mopub/mobileads/MoPubInterstitial;

    invoke-virtual {v0}, Lcom/mopub/mobileads/MoPubInterstitial;->destroy()V

    .line 286
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->d:Lcom/mopub/mobileads/MoPubInterstitial;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->d:Lcom/mopub/mobileads/MoPubInterstitial;

    invoke-virtual {v0}, Lcom/mopub/mobileads/MoPubInterstitial;->destroy()V

    .line 287
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->e:Lcom/mopub/mobileads/MoPubInterstitial;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->e:Lcom/mopub/mobileads/MoPubInterstitial;

    invoke-virtual {v0}, Lcom/mopub/mobileads/MoPubInterstitial;->destroy()V

    .line 288
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->j:Lcom/mopub/mobileads/MoPubInterstitial;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->j:Lcom/mopub/mobileads/MoPubInterstitial;

    invoke-virtual {v0}, Lcom/mopub/mobileads/MoPubInterstitial;->destroy()V

    .line 289
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->k:Lcom/mopub/mobileads/MoPubInterstitial;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->k:Lcom/mopub/mobileads/MoPubInterstitial;

    invoke-virtual {v0}, Lcom/mopub/mobileads/MoPubInterstitial;->destroy()V

    .line 290
    :cond_7
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->f:Lcom/mopub/mobileads/MoPubInterstitial;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->f:Lcom/mopub/mobileads/MoPubInterstitial;

    invoke-virtual {v0}, Lcom/mopub/mobileads/MoPubInterstitial;->destroy()V

    .line 291
    :cond_8
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->g:Lcom/mopub/mobileads/MoPubInterstitial;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->g:Lcom/mopub/mobileads/MoPubInterstitial;

    invoke-virtual {v0}, Lcom/mopub/mobileads/MoPubInterstitial;->destroy()V

    .line 292
    :cond_9
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->l:Lcom/mopub/mobileads/MoPubInterstitial;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->l:Lcom/mopub/mobileads/MoPubInterstitial;

    invoke-virtual {v0}, Lcom/mopub/mobileads/MoPubInterstitial;->destroy()V

    .line 293
    :cond_a
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->m:Lcom/mopub/mobileads/MoPubInterstitial;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->m:Lcom/mopub/mobileads/MoPubInterstitial;

    invoke-virtual {v0}, Lcom/mopub/mobileads/MoPubInterstitial;->destroy()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 294
    :cond_b
    monitor-exit p0

    return-void

    .line 282
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized e()V
    .locals 2

    .prologue
    .line 333
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/f/b;->o:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/f/b;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 334
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->t:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/b;->t:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->u()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lsoftware/simplicial/nebulous/f/b;->a(Landroid/app/Activity;Z)V

    .line 336
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lsoftware/simplicial/nebulous/f/b;->r:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 337
    monitor-exit p0

    return-void

    .line 333
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized f()V
    .locals 0

    .prologue
    .line 342
    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public declared-synchronized onInterstitialClicked(Lcom/mopub/mobileads/MoPubInterstitial;)V
    .locals 2

    .prologue
    .line 319
    monitor-enter p0

    :try_start_0
    sget-object v0, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v1, "onInterstitialClicked"

    invoke-static {v0, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 320
    monitor-exit p0

    return-void

    .line 319
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onInterstitialDismissed(Lcom/mopub/mobileads/MoPubInterstitial;)V
    .locals 2

    .prologue
    .line 325
    monitor-enter p0

    :try_start_0
    sget-object v0, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v1, "onInterstitialDismissed"

    invoke-static {v0, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 326
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->t:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/b;->t:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->u()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lsoftware/simplicial/nebulous/f/b;->a(Landroid/app/Activity;Z)V

    .line 327
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/b;->u:Lsoftware/simplicial/nebulous/f/b$a;

    invoke-interface {v0}, Lsoftware/simplicial/nebulous/f/b$a;->t()V

    .line 328
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lsoftware/simplicial/nebulous/f/b;->r:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 329
    monitor-exit p0

    return-void

    .line 325
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onInterstitialFailed(Lcom/mopub/mobileads/MoPubInterstitial;Lcom/mopub/mobileads/MoPubErrorCode;)V
    .locals 2

    .prologue
    .line 305
    monitor-enter p0

    :try_start_0
    sget-object v0, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v1, "onInterstitialFailed"

    invoke-static {v0, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 306
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/b;->v:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 307
    monitor-exit p0

    return-void

    .line 305
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onInterstitialLoaded(Lcom/mopub/mobileads/MoPubInterstitial;)V
    .locals 1

    .prologue
    .line 299
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/b;->v:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 300
    monitor-exit p0

    return-void

    .line 299
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onInterstitialShown(Lcom/mopub/mobileads/MoPubInterstitial;)V
    .locals 2

    .prologue
    .line 312
    monitor-enter p0

    :try_start_0
    sget-object v0, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v1, "onInterstitialShown"

    invoke-static {v0, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 313
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lsoftware/simplicial/nebulous/f/b;->r:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 314
    monitor-exit p0

    return-void

    .line 312
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
