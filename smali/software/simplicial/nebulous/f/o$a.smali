.class public final enum Lsoftware/simplicial/nebulous/f/o$a;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsoftware/simplicial/nebulous/f/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/nebulous/f/o$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/nebulous/f/o$a;

.field public static final enum b:Lsoftware/simplicial/nebulous/f/o$a;

.field public static final enum c:Lsoftware/simplicial/nebulous/f/o$a;

.field public static final enum d:Lsoftware/simplicial/nebulous/f/o$a;

.field private static final synthetic e:[Lsoftware/simplicial/nebulous/f/o$a;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 92
    new-instance v0, Lsoftware/simplicial/nebulous/f/o$a;

    const-string v1, "INIT"

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/f/o$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/f/o$a;->a:Lsoftware/simplicial/nebulous/f/o$a;

    new-instance v0, Lsoftware/simplicial/nebulous/f/o$a;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/nebulous/f/o$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/f/o$a;->b:Lsoftware/simplicial/nebulous/f/o$a;

    new-instance v0, Lsoftware/simplicial/nebulous/f/o$a;

    const-string v1, "LOADED"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/nebulous/f/o$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/f/o$a;->c:Lsoftware/simplicial/nebulous/f/o$a;

    new-instance v0, Lsoftware/simplicial/nebulous/f/o$a;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/nebulous/f/o$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/f/o$a;->d:Lsoftware/simplicial/nebulous/f/o$a;

    .line 90
    const/4 v0, 0x4

    new-array v0, v0, [Lsoftware/simplicial/nebulous/f/o$a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/o$a;->a:Lsoftware/simplicial/nebulous/f/o$a;

    aput-object v1, v0, v2

    sget-object v1, Lsoftware/simplicial/nebulous/f/o$a;->b:Lsoftware/simplicial/nebulous/f/o$a;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/nebulous/f/o$a;->c:Lsoftware/simplicial/nebulous/f/o$a;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/nebulous/f/o$a;->d:Lsoftware/simplicial/nebulous/f/o$a;

    aput-object v1, v0, v5

    sput-object v0, Lsoftware/simplicial/nebulous/f/o$a;->e:[Lsoftware/simplicial/nebulous/f/o$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 90
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/nebulous/f/o$a;
    .locals 1

    .prologue
    .line 90
    const-class v0, Lsoftware/simplicial/nebulous/f/o$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/f/o$a;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/nebulous/f/o$a;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lsoftware/simplicial/nebulous/f/o$a;->e:[Lsoftware/simplicial/nebulous/f/o$a;

    invoke-virtual {v0}, [Lsoftware/simplicial/nebulous/f/o$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/nebulous/f/o$a;

    return-object v0
.end method
