.class public Lsoftware/simplicial/nebulous/f/o;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/nebulous/f/o$a;,
        Lsoftware/simplicial/nebulous/f/o$b;
    }
.end annotation


# static fields
.field public static a:I


# instance fields
.field public final b:I

.field public c:Landroid/graphics/Bitmap;

.field public d:Lsoftware/simplicial/nebulous/f/o$a;

.field public e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-string v0, "webp"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 28
    const/16 v0, 0x4000

    sput v0, Lsoftware/simplicial/nebulous/f/o;->a:I

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/o;->c:Landroid/graphics/Bitmap;

    .line 31
    sget-object v0, Lsoftware/simplicial/nebulous/f/o$a;->a:Lsoftware/simplicial/nebulous/f/o$a;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/o;->d:Lsoftware/simplicial/nebulous/f/o$a;

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/nebulous/f/o;->e:I

    .line 36
    iput p1, p0, Lsoftware/simplicial/nebulous/f/o;->b:I

    .line 37
    return-void
.end method

.method public constructor <init>(ILandroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/o;->c:Landroid/graphics/Bitmap;

    .line 31
    sget-object v0, Lsoftware/simplicial/nebulous/f/o$a;->a:Lsoftware/simplicial/nebulous/f/o$a;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/o;->d:Lsoftware/simplicial/nebulous/f/o$a;

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/nebulous/f/o;->e:I

    .line 41
    iput p1, p0, Lsoftware/simplicial/nebulous/f/o;->b:I

    .line 42
    iput-object p2, p0, Lsoftware/simplicial/nebulous/f/o;->c:Landroid/graphics/Bitmap;

    .line 43
    sget-object v0, Lsoftware/simplicial/nebulous/f/o$a;->c:Lsoftware/simplicial/nebulous/f/o$a;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/o;->d:Lsoftware/simplicial/nebulous/f/o$a;

    .line 44
    return-void
.end method

.method public static a(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 68
    invoke-static {p0, v6}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 70
    new-array v1, v2, [I

    .line 71
    new-array v2, v2, [I

    .line 73
    array-length v3, v0

    int-to-long v4, v3

    invoke-static {v0, v4, v5, v2, v1}, Lcom/google/webp/a;->a([BJ[I[I)I

    move-result v3

    if-eqz v3, :cond_0

    .line 75
    array-length v3, v0

    int-to-long v4, v3

    invoke-static {v0, v4, v5, v2, v1}, Lcom/google/webp/a;->b([BJ[I[I)[B

    move-result-object v0

    .line 76
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v3

    .line 77
    array-length v0, v0

    div-int/lit8 v0, v0, 0x4

    new-array v0, v0, [I

    .line 78
    invoke-virtual {v3, v0}, Ljava/nio/IntBuffer;->get([I)Ljava/nio/IntBuffer;

    .line 79
    aget v2, v2, v6

    aget v1, v1, v6

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v2, v1, v3}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 82
    :goto_0
    return-object v0

    :cond_0
    array-length v1, v0

    invoke-static {v0, v6, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;I[I)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 48
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->WEBP:Landroid/graphics/Bitmap$CompressFormat;

    if-ne p1, v0, :cond_0

    .line 50
    invoke-static {p0}, Landroid/support/v4/c/a;->a(Landroid/graphics/Bitmap;)I

    move-result v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 51
    invoke-virtual {p0, v0}, Landroid/graphics/Bitmap;->copyPixelsToBuffer(Ljava/nio/Buffer;)V

    .line 52
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    mul-int/lit8 v3, v3, 0x4

    int-to-float v4, p2

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/webp/a;->a([BIIIF)[B

    move-result-object v0

    .line 53
    array-length v1, v0

    aput v1, p3, v5

    .line 54
    invoke-static {v0, v5}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    .line 62
    :goto_0
    return-object v0

    .line 58
    :cond_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 59
    invoke-virtual {p0, p1, p2, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 60
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 61
    array-length v1, v0

    aput v1, p3, v5

    .line 62
    invoke-static {v0, v5}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
