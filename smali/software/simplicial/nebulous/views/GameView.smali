.class public Lsoftware/simplicial/nebulous/views/GameView;
.super Landroid/opengl/GLSurfaceView;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/nebulous/views/GameView$c;,
        Lsoftware/simplicial/nebulous/views/GameView$b;,
        Lsoftware/simplicial/nebulous/views/GameView$a;
    }
.end annotation


# static fields
.field public static a:Landroid/graphics/Bitmap;

.field public static b:I

.field public static c:I

.field public static d:Z

.field static final p:[F

.field static final q:[F

.field static final r:[F

.field static final s:[F


# instance fields
.field A:Lsoftware/simplicial/nebulous/views/b/n;

.field B:Lsoftware/simplicial/nebulous/views/b/d;

.field C:Lsoftware/simplicial/nebulous/views/b/c;

.field D:Lsoftware/simplicial/nebulous/views/b/a;

.field E:Lsoftware/simplicial/nebulous/views/b/q;

.field F:[Lsoftware/simplicial/nebulous/views/j;

.field G:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/nebulous/views/a;",
            ">;"
        }
    .end annotation
.end field

.field H:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/nebulous/views/c;",
            ">;"
        }
    .end annotation
.end field

.field I:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/nebulous/views/g;",
            ">;"
        }
    .end annotation
.end field

.field private J:Ljava/nio/FloatBuffer;

.field private K:Ljava/nio/FloatBuffer;

.field private L:Ljava/nio/FloatBuffer;

.field private M:[Ljava/nio/FloatBuffer;

.field private N:[Ljava/nio/FloatBuffer;

.field private O:[Ljava/nio/FloatBuffer;

.field private P:Ljava/nio/FloatBuffer;

.field private Q:Ljava/nio/FloatBuffer;

.field private R:Ljava/nio/FloatBuffer;

.field private S:Ljava/nio/FloatBuffer;

.field private T:Ljava/nio/FloatBuffer;

.field private U:Ljava/nio/FloatBuffer;

.field private V:Ljava/nio/FloatBuffer;

.field private W:Ljava/nio/FloatBuffer;

.field private final aA:Lsoftware/simplicial/nebulous/views/b/m;

.field private final aB:Lsoftware/simplicial/nebulous/views/b/m;

.field private final aC:Lsoftware/simplicial/nebulous/views/b/m;

.field private final aD:Lsoftware/simplicial/nebulous/views/b/m;

.field private final aE:Lsoftware/simplicial/nebulous/views/b/m;

.field private final aF:Lsoftware/simplicial/nebulous/views/b/m;

.field private final aG:Lsoftware/simplicial/nebulous/views/b/m;

.field private final aH:Lsoftware/simplicial/nebulous/views/b/m;

.field private final aI:Lsoftware/simplicial/nebulous/views/b/m;

.field private final aJ:Lsoftware/simplicial/nebulous/views/b/m;

.field private final aK:Lsoftware/simplicial/nebulous/views/b/m;

.field private final aL:Lsoftware/simplicial/nebulous/views/b/m;

.field private final aM:Lsoftware/simplicial/nebulous/views/b/m;

.field private final aN:Lsoftware/simplicial/nebulous/views/b/m;

.field private final aO:Lsoftware/simplicial/nebulous/views/b/m;

.field private final aP:Lsoftware/simplicial/nebulous/views/b/m;

.field private final aQ:Lsoftware/simplicial/nebulous/views/b/m;

.field private final aR:Lsoftware/simplicial/nebulous/views/b/m;

.field private final aS:Lsoftware/simplicial/nebulous/views/b/m;

.field private final aT:Lsoftware/simplicial/nebulous/views/b/m;

.field private final aU:Lsoftware/simplicial/nebulous/views/b/m;

.field private final aV:Lsoftware/simplicial/nebulous/views/b/m;

.field private final aW:Lsoftware/simplicial/nebulous/views/b/m;

.field private final aX:Lsoftware/simplicial/nebulous/views/b/m;

.field private final aY:Lsoftware/simplicial/nebulous/views/b/m;

.field private final aZ:Lsoftware/simplicial/nebulous/views/b/m;

.field private aa:Ljava/nio/FloatBuffer;

.field private ab:F

.field private ac:F

.field private ad:F

.field private ae:F

.field private af:F

.field private ag:F

.field private ah:Lsoftware/simplicial/nebulous/views/a/d;

.field private ai:Lsoftware/simplicial/nebulous/views/a/f;

.field private aj:Lsoftware/simplicial/nebulous/views/a/c;

.field private ak:Lsoftware/simplicial/nebulous/views/a/e;

.field private al:Lsoftware/simplicial/nebulous/views/a/a;

.field private am:Lsoftware/simplicial/nebulous/views/a/b;

.field private an:Lsoftware/simplicial/nebulous/views/a/g;

.field private final ao:Lsoftware/simplicial/nebulous/views/b/e;

.field private final ap:Lsoftware/simplicial/nebulous/views/b/e;

.field private final aq:Lsoftware/simplicial/nebulous/views/b/b;

.field private final ar:Lsoftware/simplicial/nebulous/views/b/b;

.field private final as:Lsoftware/simplicial/nebulous/views/b/b;

.field private final at:Lsoftware/simplicial/nebulous/views/b/b;

.field private final au:Lsoftware/simplicial/nebulous/views/b/m;

.field private final av:Lsoftware/simplicial/nebulous/views/b/m;

.field private final aw:Lsoftware/simplicial/nebulous/views/b/m;

.field private final ax:Lsoftware/simplicial/nebulous/views/b/m;

.field private final ay:Lsoftware/simplicial/nebulous/views/b/m;

.field private final az:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bA:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bB:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bC:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bD:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bE:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bF:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bG:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bH:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bI:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bJ:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bK:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bL:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bM:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bN:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bO:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bP:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bQ:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bR:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bS:[Lsoftware/simplicial/nebulous/views/b/m;

.field private final bT:[Lsoftware/simplicial/nebulous/views/b/m;

.field private final bU:[Lsoftware/simplicial/nebulous/views/b/m;

.field private final bV:[Lsoftware/simplicial/nebulous/views/b/g;

.field private final bW:[Lsoftware/simplicial/nebulous/views/b/m;

.field private final bX:[Lsoftware/simplicial/nebulous/views/b/m;

.field private final bY:[Lsoftware/simplicial/nebulous/views/b/l;

.field private final bZ:[Lsoftware/simplicial/nebulous/views/b/l;

.field private final ba:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bb:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bc:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bd:Lsoftware/simplicial/nebulous/views/b/m;

.field private final be:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bf:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bg:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bh:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bi:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bj:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bk:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bl:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bm:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bn:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bo:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bp:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bq:Lsoftware/simplicial/nebulous/views/b/m;

.field private final br:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bs:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bt:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bu:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bv:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bw:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bx:Lsoftware/simplicial/nebulous/views/b/m;

.field private final by:Lsoftware/simplicial/nebulous/views/b/m;

.field private final bz:Lsoftware/simplicial/nebulous/views/b/m;

.field private cA:Z

.field private cB:Z

.field private cC:F

.field private cD:Z

.field private cE:Z

.field private cF:Lsoftware/simplicial/a/bf;

.field private cG:F

.field private cH:Ljava/lang/String;

.field private cI:Lsoftware/simplicial/nebulous/c/b;

.field private cJ:Lsoftware/simplicial/nebulous/f/aj;

.field private cK:F

.field private cL:F

.field private cM:Z

.field private cN:Z

.field private cO:Z

.field private cP:Z

.field private cQ:Z

.field private cR:Z

.field private cS:Z

.field private cT:Z

.field private cU:Z

.field private cV:Z

.field private cW:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private cX:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private cY:Landroid/graphics/Bitmap;

.field private cZ:Lsoftware/simplicial/a/ao;

.field private final ca:[Lsoftware/simplicial/nebulous/views/b/i;

.field private final cb:[Lsoftware/simplicial/nebulous/views/b/h;

.field private cc:Lsoftware/simplicial/a/u;

.field private cd:F

.field private ce:F

.field private cf:F

.field private cg:F

.field private ch:F

.field private ci:[F

.field private cj:Z

.field private ck:J

.field private cl:I

.field private cm:Landroid/util/DisplayMetrics;

.field private cn:Ljava/util/Random;

.field private co:Lsoftware/simplicial/nebulous/c/d;

.field private cp:[I

.field private cq:[I

.field private cr:F

.field private cs:Lsoftware/simplicial/nebulous/views/GameView$a;

.field private ct:J

.field private cu:I

.field private cv:I

.field private cw:[Lsoftware/simplicial/nebulous/views/GameView$b;

.field private cx:Lsoftware/simplicial/nebulous/views/f;

.field private cy:Z

.field private cz:Z

.field private da:Lsoftware/simplicial/a/i/e;

.field private db:Lsoftware/simplicial/a/bj;

.field private dc:I

.field private dd:I

.field private de:I

.field private df:Lsoftware/simplicial/a/h/a$a;

.field private dg:Lsoftware/simplicial/a/c/h;

.field private dh:I

.field private di:I

.field private dj:F

.field private dk:Z

.field private dl:Z

.field private dm:D

.field private dn:I

.field public e:F

.field public f:Z

.field g:[F

.field h:[F

.field i:[F

.field j:[[F

.field k:[[F

.field l:[F

.field m:[F

.field n:[F

.field o:[F

.field t:J

.field u:Lsoftware/simplicial/nebulous/views/b/p;

.field v:Lsoftware/simplicial/nebulous/views/b/r;

.field w:Lsoftware/simplicial/nebulous/views/b/f;

.field x:Lsoftware/simplicial/nebulous/views/b/s;

.field y:Lsoftware/simplicial/nebulous/views/b/k;

.field z:Lsoftware/simplicial/nebulous/views/b/j;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x4

    .line 203
    const/4 v0, 0x0

    sput-object v0, Lsoftware/simplicial/nebulous/views/GameView;->a:Landroid/graphics/Bitmap;

    .line 204
    sput v2, Lsoftware/simplicial/nebulous/views/GameView;->b:I

    .line 205
    sput v2, Lsoftware/simplicial/nebulous/views/GameView;->c:I

    .line 206
    const/4 v0, 0x0

    sput-boolean v0, Lsoftware/simplicial/nebulous/views/GameView;->d:Z

    .line 219
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    sput-object v0, Lsoftware/simplicial/nebulous/views/GameView;->p:[F

    .line 220
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    sput-object v0, Lsoftware/simplicial/nebulous/views/GameView;->q:[F

    .line 221
    new-array v0, v1, [F

    fill-array-data v0, :array_2

    sput-object v0, Lsoftware/simplicial/nebulous/views/GameView;->r:[F

    .line 222
    new-array v0, v1, [F

    fill-array-data v0, :array_3

    sput-object v0, Lsoftware/simplicial/nebulous/views/GameView;->s:[F

    return-void

    .line 219
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    .line 220
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
        0x3e4ccccd    # 0.2f
        0x3f800000    # 1.0f
    .end array-data

    .line 221
    :array_2
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    .line 222
    :array_3
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f000000    # 0.5f
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 9

    .prologue
    const/16 v7, 0x7e

    const/16 v6, 0x1b

    const/4 v8, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 436
    invoke-direct {p0, p1, p2}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 208
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->e:F

    .line 209
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/views/GameView;->f:Z

    .line 210
    iput-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->g:[F

    .line 211
    iput-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->h:[F

    .line 212
    iput-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->i:[F

    move-object v0, v1

    .line 213
    check-cast v0, [[F

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->j:[[F

    move-object v0, v1

    .line 214
    check-cast v0, [[F

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->k:[[F

    .line 215
    iput-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->l:[F

    .line 216
    iput-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->m:[F

    .line 217
    iput-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->n:[F

    .line 218
    iput-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->o:[F

    .line 223
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lsoftware/simplicial/nebulous/views/GameView;->t:J

    .line 225
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/p;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v0, v3}, Lsoftware/simplicial/nebulous/views/b/p;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->u:Lsoftware/simplicial/nebulous/views/b/p;

    .line 226
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/r;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v0, v3}, Lsoftware/simplicial/nebulous/views/b/r;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->v:Lsoftware/simplicial/nebulous/views/b/r;

    .line 227
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/f;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v0, v3}, Lsoftware/simplicial/nebulous/views/b/f;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->w:Lsoftware/simplicial/nebulous/views/b/f;

    .line 228
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/s;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v0, v3}, Lsoftware/simplicial/nebulous/views/b/s;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->x:Lsoftware/simplicial/nebulous/views/b/s;

    .line 229
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/k;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v0, v3}, Lsoftware/simplicial/nebulous/views/b/k;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->y:Lsoftware/simplicial/nebulous/views/b/k;

    .line 230
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/j;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v0, v3}, Lsoftware/simplicial/nebulous/views/b/j;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->z:Lsoftware/simplicial/nebulous/views/b/j;

    .line 231
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/n;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v0, v3}, Lsoftware/simplicial/nebulous/views/b/n;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->A:Lsoftware/simplicial/nebulous/views/b/n;

    .line 232
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/d;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v0, v3}, Lsoftware/simplicial/nebulous/views/b/d;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->B:Lsoftware/simplicial/nebulous/views/b/d;

    .line 233
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/c;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v0, v3}, Lsoftware/simplicial/nebulous/views/b/c;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->C:Lsoftware/simplicial/nebulous/views/b/c;

    .line 234
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/a;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v0, v3}, Lsoftware/simplicial/nebulous/views/b/a;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->D:Lsoftware/simplicial/nebulous/views/b/a;

    .line 235
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/q;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v0, v3}, Lsoftware/simplicial/nebulous/views/b/q;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->E:Lsoftware/simplicial/nebulous/views/b/q;

    .line 260
    iput-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->ah:Lsoftware/simplicial/nebulous/views/a/d;

    .line 261
    iput-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->ai:Lsoftware/simplicial/nebulous/views/a/f;

    .line 262
    iput-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->aj:Lsoftware/simplicial/nebulous/views/a/c;

    .line 263
    iput-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->ak:Lsoftware/simplicial/nebulous/views/a/e;

    .line 264
    iput-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->al:Lsoftware/simplicial/nebulous/views/a/a;

    .line 265
    iput-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->am:Lsoftware/simplicial/nebulous/views/a/b;

    .line 266
    iput-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->an:Lsoftware/simplicial/nebulous/views/a/g;

    .line 268
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/e;

    const/4 v3, -0x1

    invoke-direct {v0, v3}, Lsoftware/simplicial/nebulous/views/b/e;-><init>(I)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ao:Lsoftware/simplicial/nebulous/views/b/e;

    .line 269
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/e;

    invoke-direct {v0, v2}, Lsoftware/simplicial/nebulous/views/b/e;-><init>(I)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ap:Lsoftware/simplicial/nebulous/views/b/e;

    .line 270
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f02014f

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v8}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aq:Lsoftware/simplicial/nebulous/views/b/b;

    .line 271
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f02016e

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ar:Lsoftware/simplicial/nebulous/views/b/b;

    .line 272
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020170

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->as:Lsoftware/simplicial/nebulous/views/b/b;

    .line 273
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020403

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v8}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->at:Lsoftware/simplicial/nebulous/views/b/b;

    .line 274
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020432

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->au:Lsoftware/simplicial/nebulous/views/b/m;

    .line 275
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020071

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v8}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->av:Lsoftware/simplicial/nebulous/views/b/m;

    .line 276
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020146

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v8}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aw:Lsoftware/simplicial/nebulous/views/b/m;

    .line 277
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020190

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v8}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ax:Lsoftware/simplicial/nebulous/views/b/m;

    .line 278
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020072

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ay:Lsoftware/simplicial/nebulous/views/b/m;

    .line 279
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f0202c0

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->az:Lsoftware/simplicial/nebulous/views/b/m;

    .line 280
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f0202fe

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v8}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aA:Lsoftware/simplicial/nebulous/views/b/m;

    .line 281
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f02006b

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aB:Lsoftware/simplicial/nebulous/views/b/m;

    .line 282
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020300

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aC:Lsoftware/simplicial/nebulous/views/b/m;

    .line 283
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f02016f

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aD:Lsoftware/simplicial/nebulous/views/b/m;

    .line 284
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020301

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aE:Lsoftware/simplicial/nebulous/views/b/m;

    .line 285
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f02038d

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aF:Lsoftware/simplicial/nebulous/views/b/m;

    .line 286
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f0201c0

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aG:Lsoftware/simplicial/nebulous/views/b/m;

    .line 287
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020316

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aH:Lsoftware/simplicial/nebulous/views/b/m;

    .line 288
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020070

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aI:Lsoftware/simplicial/nebulous/views/b/m;

    .line 289
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020144

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v8}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aJ:Lsoftware/simplicial/nebulous/views/b/m;

    .line 290
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020057

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aK:Lsoftware/simplicial/nebulous/views/b/m;

    .line 291
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f02005c

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aL:Lsoftware/simplicial/nebulous/views/b/m;

    .line 292
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f0203bf

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aM:Lsoftware/simplicial/nebulous/views/b/m;

    .line 293
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f0203c0

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aN:Lsoftware/simplicial/nebulous/views/b/m;

    .line 294
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f0203c1

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aO:Lsoftware/simplicial/nebulous/views/b/m;

    .line 295
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f02006c

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aP:Lsoftware/simplicial/nebulous/views/b/m;

    .line 296
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f02006d

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aQ:Lsoftware/simplicial/nebulous/views/b/m;

    .line 297
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f02006e

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aR:Lsoftware/simplicial/nebulous/views/b/m;

    .line 298
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f02006f

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aS:Lsoftware/simplicial/nebulous/views/b/m;

    .line 299
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020149

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aT:Lsoftware/simplicial/nebulous/views/b/m;

    .line 300
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f02014a

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aU:Lsoftware/simplicial/nebulous/views/b/m;

    .line 301
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f02014b

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aV:Lsoftware/simplicial/nebulous/views/b/m;

    .line 302
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f02014c

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aW:Lsoftware/simplicial/nebulous/views/b/m;

    .line 303
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f02014d

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aX:Lsoftware/simplicial/nebulous/views/b/m;

    .line 304
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f02014e

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aY:Lsoftware/simplicial/nebulous/views/b/m;

    .line 305
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020390

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aZ:Lsoftware/simplicial/nebulous/views/b/m;

    .line 306
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f0202ec

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ba:Lsoftware/simplicial/nebulous/views/b/m;

    .line 307
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020302

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bb:Lsoftware/simplicial/nebulous/views/b/m;

    .line 308
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020304

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bc:Lsoftware/simplicial/nebulous/views/b/m;

    .line 309
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020303

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bd:Lsoftware/simplicial/nebulous/views/b/m;

    .line 310
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f0202f1

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->be:Lsoftware/simplicial/nebulous/views/b/m;

    .line 311
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f0202f2

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bf:Lsoftware/simplicial/nebulous/views/b/m;

    .line 312
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f0202f3

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bg:Lsoftware/simplicial/nebulous/views/b/m;

    .line 313
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020404

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bh:Lsoftware/simplicial/nebulous/views/b/m;

    .line 314
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020387

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bi:Lsoftware/simplicial/nebulous/views/b/m;

    .line 315
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020388

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bj:Lsoftware/simplicial/nebulous/views/b/m;

    .line 316
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020389

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bk:Lsoftware/simplicial/nebulous/views/b/m;

    .line 317
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f0200a7

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bl:Lsoftware/simplicial/nebulous/views/b/m;

    .line 318
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f02038b

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bm:Lsoftware/simplicial/nebulous/views/b/m;

    .line 319
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f0201bf

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bn:Lsoftware/simplicial/nebulous/views/b/m;

    .line 320
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f0203bb

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bo:Lsoftware/simplicial/nebulous/views/b/m;

    .line 321
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020076

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bp:Lsoftware/simplicial/nebulous/views/b/m;

    .line 322
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020197

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bq:Lsoftware/simplicial/nebulous/views/b/m;

    .line 323
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020384

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->br:Lsoftware/simplicial/nebulous/views/b/m;

    .line 324
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f0201a4

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bs:Lsoftware/simplicial/nebulous/views/b/m;

    .line 325
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020401

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bt:Lsoftware/simplicial/nebulous/views/b/m;

    .line 326
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f0203b9

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bu:Lsoftware/simplicial/nebulous/views/b/m;

    .line 327
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020391

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bv:Lsoftware/simplicial/nebulous/views/b/m;

    .line 328
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020066

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bw:Lsoftware/simplicial/nebulous/views/b/m;

    .line 329
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f0201ab

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bx:Lsoftware/simplicial/nebulous/views/b/m;

    .line 330
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020073

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->by:Lsoftware/simplicial/nebulous/views/b/m;

    .line 331
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020299

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bz:Lsoftware/simplicial/nebulous/views/b/m;

    .line 332
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f0203bc

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bA:Lsoftware/simplicial/nebulous/views/b/m;

    .line 333
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020077

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bB:Lsoftware/simplicial/nebulous/views/b/m;

    .line 334
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020198

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bC:Lsoftware/simplicial/nebulous/views/b/m;

    .line 335
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020385

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bD:Lsoftware/simplicial/nebulous/views/b/m;

    .line 336
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f0201a5

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bE:Lsoftware/simplicial/nebulous/views/b/m;

    .line 337
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020067

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bF:Lsoftware/simplicial/nebulous/views/b/m;

    .line 338
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f0201ac

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bG:Lsoftware/simplicial/nebulous/views/b/m;

    .line 339
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f02009f

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bH:Lsoftware/simplicial/nebulous/views/b/m;

    .line 340
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020396

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bI:Lsoftware/simplicial/nebulous/views/b/m;

    .line 341
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020074

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bJ:Lsoftware/simplicial/nebulous/views/b/m;

    .line 342
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f0203bd

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bK:Lsoftware/simplicial/nebulous/views/b/m;

    .line 343
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020199

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bL:Lsoftware/simplicial/nebulous/views/b/m;

    .line 344
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020078

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bM:Lsoftware/simplicial/nebulous/views/b/m;

    .line 345
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020386

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bN:Lsoftware/simplicial/nebulous/views/b/m;

    .line 346
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f0203ba

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bO:Lsoftware/simplicial/nebulous/views/b/m;

    .line 347
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020391

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bP:Lsoftware/simplicial/nebulous/views/b/m;

    .line 348
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020066

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bQ:Lsoftware/simplicial/nebulous/views/b/m;

    .line 349
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f020074

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bR:Lsoftware/simplicial/nebulous/views/b/m;

    .line 350
    sget-object v0, Lsoftware/simplicial/a/af;->b:[Lsoftware/simplicial/a/af;

    array-length v0, v0

    new-array v0, v0, [Lsoftware/simplicial/nebulous/views/b/m;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bS:[Lsoftware/simplicial/nebulous/views/b/m;

    .line 351
    sget-object v0, Lsoftware/simplicial/a/bd;->b:[Lsoftware/simplicial/a/bd;

    array-length v0, v0

    new-array v0, v0, [Lsoftware/simplicial/nebulous/views/b/m;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bT:[Lsoftware/simplicial/nebulous/views/b/m;

    .line 352
    sget-object v0, Lsoftware/simplicial/a/as;->b:[Lsoftware/simplicial/a/as;

    array-length v0, v0

    new-array v0, v0, [Lsoftware/simplicial/nebulous/views/b/m;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bU:[Lsoftware/simplicial/nebulous/views/b/m;

    .line 353
    new-array v0, v6, [Lsoftware/simplicial/nebulous/views/b/g;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bV:[Lsoftware/simplicial/nebulous/views/b/g;

    .line 354
    const/16 v0, 0x1e

    new-array v0, v0, [Lsoftware/simplicial/nebulous/views/b/m;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bW:[Lsoftware/simplicial/nebulous/views/b/m;

    .line 355
    const/4 v0, 0x4

    new-array v0, v0, [Lsoftware/simplicial/nebulous/views/b/m;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bX:[Lsoftware/simplicial/nebulous/views/b/m;

    .line 356
    new-array v0, v6, [Lsoftware/simplicial/nebulous/views/b/l;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bY:[Lsoftware/simplicial/nebulous/views/b/l;

    .line 357
    new-array v0, v6, [Lsoftware/simplicial/nebulous/views/b/l;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bZ:[Lsoftware/simplicial/nebulous/views/b/l;

    .line 358
    new-array v0, v6, [Lsoftware/simplicial/nebulous/views/b/i;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ca:[Lsoftware/simplicial/nebulous/views/b/i;

    .line 359
    new-array v0, v6, [Lsoftware/simplicial/nebulous/views/b/h;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cb:[Lsoftware/simplicial/nebulous/views/b/h;

    .line 368
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ci:[F

    .line 369
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/views/GameView;->cj:Z

    .line 370
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lsoftware/simplicial/nebulous/views/GameView;->ck:J

    .line 371
    iput v2, p0, Lsoftware/simplicial/nebulous/views/GameView;->cl:I

    .line 373
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Ljava/util/Random;-><init>(J)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cn:Ljava/util/Random;

    .line 375
    const/16 v0, 0x1f8

    new-array v0, v0, [I

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cp:[I

    .line 376
    const/16 v0, 0x40

    new-array v0, v0, [I

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cq:[I

    .line 377
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cr:F

    .line 378
    sget-object v0, Lsoftware/simplicial/nebulous/views/GameView$a;->a:Lsoftware/simplicial/nebulous/views/GameView$a;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cs:Lsoftware/simplicial/nebulous/views/GameView$a;

    .line 379
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lsoftware/simplicial/nebulous/views/GameView;->ct:J

    .line 380
    iput v2, p0, Lsoftware/simplicial/nebulous/views/GameView;->cu:I

    .line 381
    const/4 v0, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cv:I

    .line 382
    new-array v0, v2, [Lsoftware/simplicial/nebulous/views/GameView$b;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cw:[Lsoftware/simplicial/nebulous/views/GameView$b;

    .line 383
    new-array v0, v2, [Lsoftware/simplicial/nebulous/views/j;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->F:[Lsoftware/simplicial/nebulous/views/j;

    .line 384
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->G:Ljava/util/List;

    .line 385
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->H:Ljava/util/List;

    .line 386
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->I:Ljava/util/List;

    .line 388
    new-instance v0, Lsoftware/simplicial/nebulous/views/f;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/views/f;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cx:Lsoftware/simplicial/nebulous/views/f;

    .line 414
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cW:Ljava/util/Set;

    .line 415
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cX:Ljava/util/Set;

    .line 428
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/views/GameView;->dk:Z

    .line 432
    iput v2, p0, Lsoftware/simplicial/nebulous/views/GameView;->dn:I

    .line 438
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/views/GameView;->setEGLContextClientVersion(I)V

    .line 440
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cn:Ljava/util/Random;

    invoke-virtual {v0, v7}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x64

    .line 441
    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/GameView;->cn:Ljava/util/Random;

    invoke-virtual {v3, v7}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x64

    .line 442
    iget-object v4, p0, Lsoftware/simplicial/nebulous/views/GameView;->cn:Ljava/util/Random;

    invoke-virtual {v4, v7}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x64

    .line 443
    const/high16 v5, -0x1000000

    shl-int/lit8 v6, v4, 0x10

    or-int/2addr v5, v6

    shl-int/lit8 v6, v3, 0x8

    or-int/2addr v5, v6

    or-int/2addr v5, v0

    sput v5, Lsoftware/simplicial/nebulous/views/GameView;->b:I

    .line 444
    const/high16 v5, -0x1000000

    add-int/lit8 v4, v4, 0x1e

    shl-int/lit8 v4, v4, 0x10

    or-int/2addr v4, v5

    add-int/lit8 v3, v3, 0x1e

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v3, v4

    add-int/lit8 v0, v0, 0x1e

    or-int/2addr v0, v3

    sput v0, Lsoftware/simplicial/nebulous/views/GameView;->c:I

    move v0, v2

    .line 446
    :goto_0
    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/GameView;->cp:[I

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 448
    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/GameView;->cn:Ljava/util/Random;

    const/16 v4, 0x88

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x64

    .line 449
    iget-object v4, p0, Lsoftware/simplicial/nebulous/views/GameView;->cn:Ljava/util/Random;

    const/16 v5, 0x88

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x64

    .line 450
    iget-object v5, p0, Lsoftware/simplicial/nebulous/views/GameView;->cn:Ljava/util/Random;

    const/16 v6, 0x88

    invoke-virtual {v5, v6}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x64

    .line 451
    iget-object v6, p0, Lsoftware/simplicial/nebulous/views/GameView;->cp:[I

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v6, v0

    .line 446
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v2

    .line 454
    :goto_1
    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/GameView;->cq:[I

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 456
    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/GameView;->cn:Ljava/util/Random;

    const/16 v4, 0x38

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit16 v3, v3, 0xc8

    .line 457
    iget-object v4, p0, Lsoftware/simplicial/nebulous/views/GameView;->cn:Ljava/util/Random;

    const/16 v5, 0x100

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    .line 459
    iget-object v5, p0, Lsoftware/simplicial/nebulous/views/GameView;->cq:[I

    invoke-static {v2, v4, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v5, v0

    .line 454
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v0, v2

    .line 462
    :goto_2
    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/GameView;->cw:[Lsoftware/simplicial/nebulous/views/GameView$b;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 463
    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/GameView;->cw:[Lsoftware/simplicial/nebulous/views/GameView$b;

    new-instance v4, Lsoftware/simplicial/nebulous/views/GameView$b;

    invoke-direct {v4, p0, v1}, Lsoftware/simplicial/nebulous/views/GameView$b;-><init>(Lsoftware/simplicial/nebulous/views/GameView;Lsoftware/simplicial/nebulous/views/GameView$1;)V

    aput-object v4, v3, v0

    .line 462
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    move v0, v2

    .line 465
    :goto_3
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->bS:[Lsoftware/simplicial/nebulous/views/b/m;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 466
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->bS:[Lsoftware/simplicial/nebulous/views/b/m;

    new-instance v3, Lsoftware/simplicial/nebulous/views/b/m;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget-object v5, Lsoftware/simplicial/a/af;->b:[Lsoftware/simplicial/a/af;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Lsoftware/simplicial/a/af;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "drawable"

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {v3, v4, v5, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    aput-object v3, v1, v0

    .line 465
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    move v0, v2

    .line 467
    :goto_4
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->bT:[Lsoftware/simplicial/nebulous/views/b/m;

    array-length v1, v1

    if-ge v0, v1, :cond_4

    .line 468
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->bT:[Lsoftware/simplicial/nebulous/views/b/m;

    new-instance v3, Lsoftware/simplicial/nebulous/views/b/m;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget-object v5, Lsoftware/simplicial/a/bd;->b:[Lsoftware/simplicial/a/bd;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Lsoftware/simplicial/a/bd;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "drawable"

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {v3, v4, v5, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    aput-object v3, v1, v0

    .line 467
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_4
    move v0, v2

    .line 469
    :goto_5
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->bW:[Lsoftware/simplicial/nebulous/views/b/m;

    array-length v1, v1

    if-ge v0, v1, :cond_5

    .line 470
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->bW:[Lsoftware/simplicial/nebulous/views/b/m;

    new-instance v3, Lsoftware/simplicial/nebulous/views/b/m;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "emote_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v6, v0, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "drawable"

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {v3, v4, v5, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    aput-object v3, v1, v0

    .line 469
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_5
    move v0, v2

    .line 471
    :goto_6
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->bU:[Lsoftware/simplicial/nebulous/views/b/m;

    array-length v1, v1

    if-ge v0, v1, :cond_6

    .line 472
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->bU:[Lsoftware/simplicial/nebulous/views/b/m;

    new-instance v3, Lsoftware/simplicial/nebulous/views/b/m;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget-object v5, Lsoftware/simplicial/a/as;->b:[Lsoftware/simplicial/a/as;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Lsoftware/simplicial/a/as;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "drawable"

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {v3, v4, v5, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    aput-object v3, v1, v0

    .line 471
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 473
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bX:[Lsoftware/simplicial/nebulous/views/b/m;

    new-instance v1, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f0202ef

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v1, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    aput-object v1, v0, v2

    .line 474
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bX:[Lsoftware/simplicial/nebulous/views/b/m;

    new-instance v1, Lsoftware/simplicial/nebulous/views/b/m;

    const v3, 0x7f0202ee

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v1, v3, v4, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    aput-object v1, v0, v8

    .line 475
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bX:[Lsoftware/simplicial/nebulous/views/b/m;

    const/4 v1, 0x2

    new-instance v3, Lsoftware/simplicial/nebulous/views/b/m;

    const v4, 0x7f0202ed

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {v3, v4, v5, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    aput-object v3, v0, v1

    .line 476
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bX:[Lsoftware/simplicial/nebulous/views/b/m;

    const/4 v1, 0x3

    new-instance v3, Lsoftware/simplicial/nebulous/views/b/m;

    const v4, 0x7f0202f0

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {v3, v4, v5, v2}, Lsoftware/simplicial/nebulous/views/b/m;-><init>(ILandroid/content/res/Resources;Z)V

    aput-object v3, v0, v1

    move v1, v2

    .line 477
    :goto_7
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bY:[Lsoftware/simplicial/nebulous/views/b/l;

    array-length v0, v0

    if-ge v1, v0, :cond_7

    .line 478
    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/GameView;->bY:[Lsoftware/simplicial/nebulous/views/b/l;

    new-instance v4, Lsoftware/simplicial/nebulous/views/b/l;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->U:Lsoftware/simplicial/nebulous/f/p;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v0, v6, v8}, Lsoftware/simplicial/nebulous/views/b/l;-><init>(Landroid/content/res/Resources;Lsoftware/simplicial/nebulous/f/p;Ljava/lang/String;Z)V

    aput-object v4, v3, v1

    .line 477
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    :cond_7
    move v1, v2

    .line 479
    :goto_8
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bZ:[Lsoftware/simplicial/nebulous/views/b/l;

    array-length v0, v0

    if-ge v1, v0, :cond_8

    .line 480
    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/GameView;->bZ:[Lsoftware/simplicial/nebulous/views/b/l;

    new-instance v4, Lsoftware/simplicial/nebulous/views/b/l;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->U:Lsoftware/simplicial/nebulous/f/p;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v0, v6, v2}, Lsoftware/simplicial/nebulous/views/b/l;-><init>(Landroid/content/res/Resources;Lsoftware/simplicial/nebulous/f/p;Ljava/lang/String;Z)V

    aput-object v4, v3, v1

    .line 479
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    :cond_8
    move v0, v2

    .line 481
    :goto_9
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->ca:[Lsoftware/simplicial/nebulous/views/b/i;

    array-length v1, v1

    if-ge v0, v1, :cond_9

    .line 482
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->ca:[Lsoftware/simplicial/nebulous/views/b/i;

    new-instance v3, Lsoftware/simplicial/nebulous/views/b/i;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4}, Lsoftware/simplicial/nebulous/views/b/i;-><init>(Landroid/content/res/Resources;)V

    aput-object v3, v1, v0

    .line 481
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_9
    move v0, v2

    .line 483
    :goto_a
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cb:[Lsoftware/simplicial/nebulous/views/b/h;

    array-length v1, v1

    if-ge v0, v1, :cond_a

    .line 484
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cb:[Lsoftware/simplicial/nebulous/views/b/h;

    new-instance v3, Lsoftware/simplicial/nebulous/views/b/h;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4}, Lsoftware/simplicial/nebulous/views/b/h;-><init>(Landroid/content/res/Resources;)V

    aput-object v3, v1, v0

    .line 483
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 485
    :cond_a
    :goto_b
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bV:[Lsoftware/simplicial/nebulous/views/b/g;

    array-length v0, v0

    if-ge v2, v0, :cond_b

    .line 486
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bV:[Lsoftware/simplicial/nebulous/views/b/g;

    new-instance v1, Lsoftware/simplicial/nebulous/views/b/g;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v1, v3}, Lsoftware/simplicial/nebulous/views/b/g;-><init>(Landroid/content/res/Resources;)V

    aput-object v1, v0, v2

    .line 485
    add-int/lit8 v2, v2, 0x1

    goto :goto_b

    .line 488
    :cond_b
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lsoftware/simplicial/nebulous/views/b/h;->a(Landroid/content/res/Resources;)V

    .line 490
    new-instance v0, Lsoftware/simplicial/nebulous/views/GameView$c;

    invoke-direct {v0, p0}, Lsoftware/simplicial/nebulous/views/GameView$c;-><init>(Lsoftware/simplicial/nebulous/views/GameView;)V

    .line 491
    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/views/GameView;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    .line 492
    return-void
.end method

.method static synthetic A(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bJ:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic B(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bG:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic C(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bK:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic D(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bM:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic E(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bL:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic F(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bN:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic G(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bO:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic H(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bP:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic I(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bQ:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic J(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bH:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic K(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bI:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic L(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ay:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic M(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aC:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic N(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aD:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic O(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aE:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic P(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aI:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic Q(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aJ:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic R(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aF:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic S(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aK:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic T(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aL:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic U(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aG:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic V(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aP:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic W(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aQ:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic X(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aR:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic Y(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aS:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic Z(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->be:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/views/GameView;D)D
    .locals 1

    .prologue
    .line 115
    iput-wide p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->dm:D

    return-wide p1
.end method

.method static synthetic a(F)F
    .locals 1

    .prologue
    .line 115
    invoke-static {p0}, Lsoftware/simplicial/nebulous/views/GameView;->b(F)F

    move-result v0

    return v0
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/views/GameView;F)F
    .locals 0

    .prologue
    .line 115
    iput p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->ad:F

    return p1
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/views/GameView;I)I
    .locals 0

    .prologue
    .line 115
    iput p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cv:I

    return p1
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/views/GameView;J)J
    .locals 1

    .prologue
    .line 115
    iput-wide p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->ct:J

    return-wide p1
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/views/GameView;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cY:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/views/GameView;Landroid/util/DisplayMetrics;)Landroid/util/DisplayMetrics;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cm:Landroid/util/DisplayMetrics;

    return-object p1
.end method

.method static synthetic a(Landroid/graphics/Bitmap;Ljava/lang/String;)Ljava/io/File;
    .locals 1

    .prologue
    .line 115
    invoke-static {p0, p1}, Lsoftware/simplicial/nebulous/views/GameView;->b(Landroid/graphics/Bitmap;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/views/GameView;Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->J:Ljava/nio/FloatBuffer;

    return-object p1
.end method

.method private a(Lsoftware/simplicial/a/ao;F)Lsoftware/simplicial/a/bh;
    .locals 12

    .prologue
    const/4 v5, 0x0

    .line 713
    const/4 v2, 0x0

    .line 714
    const/high16 v0, 0x7f800000    # Float.POSITIVE_INFINITY

    .line 716
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-object v7, v1, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    array-length v8, v7

    move v6, v5

    :goto_0
    if-ge v6, v8, :cond_2

    aget-object v9, v7, v6

    move v4, v5

    .line 718
    :goto_1
    iget-object v1, v9, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    array-length v1, v1

    if-ge v4, v1, :cond_1

    .line 720
    iget-object v1, v9, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v3, v1, v4

    .line 722
    invoke-virtual {v3}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v1, v2

    .line 718
    :goto_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v2, v1

    goto :goto_1

    .line 725
    :cond_0
    iget v1, v3, Lsoftware/simplicial/a/bh;->l:F

    iget v10, p1, Lsoftware/simplicial/a/ao;->u:F

    sub-float/2addr v1, v10

    .line 726
    iget v10, v3, Lsoftware/simplicial/a/bh;->m:F

    iget v11, p1, Lsoftware/simplicial/a/ao;->v:F

    sub-float/2addr v10, v11

    .line 727
    mul-float/2addr v1, v1

    mul-float/2addr v10, v10

    add-float/2addr v1, v10

    iget v10, v3, Lsoftware/simplicial/a/bh;->w:F

    iget v11, v3, Lsoftware/simplicial/a/bh;->w:F

    mul-float/2addr v10, v11

    sub-float/2addr v1, v10

    iget v10, p1, Lsoftware/simplicial/a/ao;->x:F

    iget v11, p1, Lsoftware/simplicial/a/ao;->x:F

    mul-float/2addr v10, v11

    sub-float/2addr v1, v10

    .line 729
    mul-float v10, p2, p2

    cmpg-float v10, v1, v10

    if-gez v10, :cond_3

    cmpg-float v10, v1, v0

    if-gez v10, :cond_3

    move v0, v1

    move-object v1, v3

    .line 732
    goto :goto_2

    .line 716
    :cond_1
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_0

    .line 737
    :cond_2
    return-object v2

    :cond_3
    move-object v1, v2

    goto :goto_2
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/views/GameView;Lsoftware/simplicial/a/u;)Lsoftware/simplicial/a/u;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    return-object p1
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/views/GameView;Lsoftware/simplicial/nebulous/views/a/a;)Lsoftware/simplicial/nebulous/views/a/a;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->al:Lsoftware/simplicial/nebulous/views/a/a;

    return-object p1
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/views/GameView;Lsoftware/simplicial/nebulous/views/a/b;)Lsoftware/simplicial/nebulous/views/a/b;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->am:Lsoftware/simplicial/nebulous/views/a/b;

    return-object p1
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/views/GameView;Lsoftware/simplicial/nebulous/views/a/c;)Lsoftware/simplicial/nebulous/views/a/c;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->aj:Lsoftware/simplicial/nebulous/views/a/c;

    return-object p1
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/views/GameView;Lsoftware/simplicial/nebulous/views/a/e;)Lsoftware/simplicial/nebulous/views/a/e;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->ak:Lsoftware/simplicial/nebulous/views/a/e;

    return-object p1
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/views/GameView;Lsoftware/simplicial/nebulous/views/a/f;)Lsoftware/simplicial/nebulous/views/a/f;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->ai:Lsoftware/simplicial/nebulous/views/a/f;

    return-object p1
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/views/GameView;Lsoftware/simplicial/nebulous/views/a/g;)Lsoftware/simplicial/nebulous/views/a/g;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->an:Lsoftware/simplicial/nebulous/views/a/g;

    return-object p1
.end method

.method private a(Lsoftware/simplicial/a/s;)Lsoftware/simplicial/nebulous/views/b/b;
    .locals 2

    .prologue
    .line 5397
    sget-object v0, Lsoftware/simplicial/nebulous/views/GameView$1;->h:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/s;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 5406
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aq:Lsoftware/simplicial/nebulous/views/b/b;

    :goto_0
    return-object v0

    .line 5400
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aq:Lsoftware/simplicial/nebulous/views/b/b;

    goto :goto_0

    .line 5402
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ar:Lsoftware/simplicial/nebulous/views/b/b;

    goto :goto_0

    .line 5404
    :pswitch_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->as:Lsoftware/simplicial/nebulous/views/b/b;

    goto :goto_0

    .line 5397
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/views/GameView;Lsoftware/simplicial/a/s;)Lsoftware/simplicial/nebulous/views/b/b;
    .locals 1

    .prologue
    .line 115
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/views/GameView;->a(Lsoftware/simplicial/a/s;)Lsoftware/simplicial/nebulous/views/b/b;

    move-result-object v0

    return-object v0
.end method

.method private a(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/a/t;)V
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/16 v5, 0xff

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 5021
    iget-object v0, p1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->ah:Z

    sput-boolean v0, Lsoftware/simplicial/nebulous/views/GameView;->d:Z

    .line 5022
    iget-object v0, p1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cH:Ljava/lang/String;

    .line 5023
    iget-object v0, p1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->h:Lsoftware/simplicial/nebulous/c/b;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cI:Lsoftware/simplicial/nebulous/c/b;

    .line 5024
    iget-object v0, p1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->w:Lsoftware/simplicial/nebulous/f/aj;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cJ:Lsoftware/simplicial/nebulous/f/aj;

    .line 5025
    iget-object v0, p1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v0, v0, Lsoftware/simplicial/nebulous/f/ag;->ao:F

    iput v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cK:F

    .line 5026
    iget-object v0, p1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v0, v0, Lsoftware/simplicial/nebulous/f/ag;->ap:F

    iput v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cL:F

    .line 5027
    iget-object v0, p1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->U:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-object v0, v0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    if-eq v0, v3, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cz:Z

    .line 5028
    iget-object v0, p1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->g:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-object v0, v0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    if-eq v0, v3, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cy:Z

    .line 5029
    iget-object v0, p1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->f:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-object v0, v0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    if-eq v0, v3, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cM:Z

    .line 5030
    iget-object v0, p1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->ab:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-object v0, v0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    if-eq v0, v3, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cN:Z

    .line 5031
    iget-object v0, p1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->ac:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-object v0, v0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    if-eq v0, v3, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cO:Z

    .line 5032
    iget-object v0, p1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->as:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-object v0, v0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    if-eq v0, v3, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cP:Z

    .line 5033
    iget-object v0, p1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->V:Z

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cQ:Z

    .line 5034
    iget-object v0, p1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->x:Z

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cR:Z

    .line 5035
    iget-object v0, p1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->T:Z

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cS:Z

    .line 5036
    iget-object v0, p1, Lsoftware/simplicial/nebulous/application/MainActivity;->aq:Ljava/util/Set;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cW:Ljava/util/Set;

    .line 5037
    iget-object v0, p1, Lsoftware/simplicial/nebulous/application/MainActivity;->ar:Ljava/util/Set;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cX:Ljava/util/Set;

    .line 5038
    iget-object v0, p1, Lsoftware/simplicial/nebulous/application/MainActivity;->I:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->j:Lsoftware/simplicial/nebulous/c/e;

    sget-object v3, Lsoftware/simplicial/nebulous/c/e;->a:Lsoftware/simplicial/nebulous/c/e;

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cT:Z

    .line 5039
    iget-boolean v0, p1, Lsoftware/simplicial/nebulous/application/MainActivity;->an:Z

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cU:Z

    .line 5040
    iget-object v0, p1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->ad:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->co:Lsoftware/simplicial/nebulous/c/d;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/c/d;->d()Z

    move-result v0

    if-eqz v0, :cond_7

    :goto_7
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cV:Z

    .line 5041
    iget-object v0, p1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->R:Z

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->dl:Z

    .line 5042
    iget-object v0, p1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->S:Z

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cA:Z

    .line 5043
    iget-object v0, p1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->Q:Z

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cB:Z

    .line 5045
    invoke-virtual {p2}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->db:Lsoftware/simplicial/a/bj;

    .line 5046
    iget-object v0, p2, Lsoftware/simplicial/a/t;->v:Lsoftware/simplicial/a/c/h;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->dg:Lsoftware/simplicial/a/c/h;

    .line 5047
    iget v0, p2, Lsoftware/simplicial/a/t;->w:I

    iput v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->dh:I

    .line 5048
    iget v0, p2, Lsoftware/simplicial/a/t;->x:I

    iput v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->di:I

    .line 5049
    iget-object v0, p2, Lsoftware/simplicial/a/t;->z:Lsoftware/simplicial/a/h/a$a;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->df:Lsoftware/simplicial/a/h/a$a;

    .line 5050
    iget v0, p2, Lsoftware/simplicial/a/t;->A:I

    iput v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->de:I

    .line 5051
    iget v0, p2, Lsoftware/simplicial/a/t;->B:I

    iput v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->dd:I

    .line 5052
    iget-object v0, p2, Lsoftware/simplicial/a/t;->y:Lsoftware/simplicial/a/i/e;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->da:Lsoftware/simplicial/a/i/e;

    .line 5053
    iget-boolean v0, p2, Lsoftware/simplicial/a/t;->t:Z

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cE:Z

    .line 5055
    sget-object v0, Lsoftware/simplicial/nebulous/views/GameView$1;->d:[I

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cJ:Lsoftware/simplicial/nebulous/f/aj;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/aj;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 5066
    invoke-static {v5, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->dc:I

    .line 5069
    :goto_8
    sget-object v0, Lsoftware/simplicial/nebulous/views/GameView$1;->d:[I

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cJ:Lsoftware/simplicial/nebulous/f/aj;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/aj;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 5084
    :goto_9
    return-void

    :cond_0
    move v0, v2

    .line 5027
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 5028
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 5029
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 5030
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 5031
    goto/16 :goto_4

    :cond_5
    move v0, v2

    .line 5032
    goto/16 :goto_5

    :cond_6
    move v0, v2

    .line 5038
    goto/16 :goto_6

    :cond_7
    move v1, v2

    .line 5040
    goto :goto_7

    .line 5060
    :pswitch_0
    invoke-static {v5, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->dc:I

    goto :goto_8

    .line 5063
    :pswitch_1
    const/16 v0, 0x5f

    const/16 v1, 0x5f

    const/16 v2, 0x5f

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->dc:I

    goto :goto_8

    .line 5072
    :pswitch_2
    invoke-static {v4, v4, v4, v6}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    goto :goto_9

    .line 5075
    :pswitch_3
    const v0, 0x3f666666    # 0.9f

    const v1, 0x3f666666    # 0.9f

    const v2, 0x3f6147ae    # 0.88f

    invoke-static {v0, v1, v2, v6}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    goto :goto_9

    .line 5078
    :pswitch_4
    invoke-static {v4, v4, v4, v6}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    goto :goto_9

    .line 5081
    :pswitch_5
    invoke-static {v4, v4, v4, v6}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    goto :goto_9

    .line 5055
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 5069
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/views/GameView;Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/a/t;)V
    .locals 0

    .prologue
    .line 115
    invoke-direct {p0, p1, p2}, Lsoftware/simplicial/nebulous/views/GameView;->a(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/a/t;)V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/views/GameView;Lsoftware/simplicial/nebulous/views/a/d;)V
    .locals 0

    .prologue
    .line 115
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/views/GameView;->a(Lsoftware/simplicial/nebulous/views/a/d;)V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/views/GameView;Lsoftware/simplicial/nebulous/views/d;Lsoftware/simplicial/a/bh;)V
    .locals 0

    .prologue
    .line 115
    invoke-direct {p0, p1, p2}, Lsoftware/simplicial/nebulous/views/GameView;->a(Lsoftware/simplicial/nebulous/views/d;Lsoftware/simplicial/a/bh;)V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/views/GameView;Lsoftware/simplicial/nebulous/views/d;Lsoftware/simplicial/a/bh;Lsoftware/simplicial/a/ca;)V
    .locals 0

    .prologue
    .line 115
    invoke-direct {p0, p1, p2, p3}, Lsoftware/simplicial/nebulous/views/GameView;->a(Lsoftware/simplicial/nebulous/views/d;Lsoftware/simplicial/a/bh;Lsoftware/simplicial/a/ca;)V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/views/GameView;Lsoftware/simplicial/nebulous/views/d;Lsoftware/simplicial/a/bh;Lsoftware/simplicial/a/h;F)V
    .locals 0

    .prologue
    .line 115
    invoke-direct {p0, p1, p2, p3, p4}, Lsoftware/simplicial/nebulous/views/GameView;->a(Lsoftware/simplicial/nebulous/views/d;Lsoftware/simplicial/a/bh;Lsoftware/simplicial/a/h;F)V

    return-void
.end method

.method private a(Lsoftware/simplicial/nebulous/views/a/d;)V
    .locals 1

    .prologue
    .line 5412
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ah:Lsoftware/simplicial/nebulous/views/a/d;

    if-ne v0, p1, :cond_0

    .line 5419
    :goto_0
    return-void

    .line 5414
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ah:Lsoftware/simplicial/nebulous/views/a/d;

    if-eqz v0, :cond_1

    .line 5415
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ah:Lsoftware/simplicial/nebulous/views/a/d;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/views/a/d;->b()V

    .line 5416
    :cond_1
    if-eqz p1, :cond_2

    .line 5417
    invoke-virtual {p1}, Lsoftware/simplicial/nebulous/views/a/d;->a()V

    .line 5418
    :cond_2
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->ah:Lsoftware/simplicial/nebulous/views/a/d;

    goto :goto_0
.end method

.method private a(Lsoftware/simplicial/nebulous/views/d;Lsoftware/simplicial/a/bh;)V
    .locals 9

    .prologue
    .line 627
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-object v0, v0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-ne v0, v1, :cond_4

    .line 629
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v0, v0, Lsoftware/simplicial/a/u;->aE:F

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v1, v1, Lsoftware/simplicial/a/u;->G:F

    sub-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v0, v1

    .line 630
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v0, v0, Lsoftware/simplicial/a/u;->G:F

    add-float/2addr v0, v1

    move v7, v0

    move v8, v1

    .line 638
    :goto_0
    iget v0, p2, Lsoftware/simplicial/a/bh;->s:F

    iget v1, p2, Lsoftware/simplicial/a/bh;->w:F

    sub-float/2addr v0, v1

    cmpg-float v0, v0, v8

    if-gez v0, :cond_0

    .line 640
    iget v0, p2, Lsoftware/simplicial/a/bh;->w:F

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float v3, v0, v1

    const/high16 v4, -0x40800000    # -1.0f

    const/4 v5, 0x0

    iget v0, p2, Lsoftware/simplicial/a/bh;->s:F

    sub-float v6, v0, v8

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lsoftware/simplicial/nebulous/views/GameView;->a(Lsoftware/simplicial/nebulous/views/d;Lsoftware/simplicial/a/bh;FFFF)V

    .line 642
    :cond_0
    iget v0, p2, Lsoftware/simplicial/a/bh;->s:F

    iget v1, p2, Lsoftware/simplicial/a/bh;->w:F

    add-float/2addr v0, v1

    cmpl-float v0, v0, v7

    if-lez v0, :cond_1

    .line 644
    iget v0, p2, Lsoftware/simplicial/a/bh;->w:F

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float v3, v0, v1

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    iget v0, p2, Lsoftware/simplicial/a/bh;->s:F

    sub-float v6, v7, v0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lsoftware/simplicial/nebulous/views/GameView;->a(Lsoftware/simplicial/nebulous/views/d;Lsoftware/simplicial/a/bh;FFFF)V

    .line 646
    :cond_1
    iget v0, p2, Lsoftware/simplicial/a/bh;->t:F

    iget v1, p2, Lsoftware/simplicial/a/bh;->w:F

    sub-float/2addr v0, v1

    cmpg-float v0, v0, v8

    if-gez v0, :cond_2

    .line 648
    iget v0, p2, Lsoftware/simplicial/a/bh;->w:F

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float v3, v0, v1

    const/4 v4, 0x0

    const/high16 v5, -0x40800000    # -1.0f

    iget v0, p2, Lsoftware/simplicial/a/bh;->t:F

    sub-float v6, v0, v8

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lsoftware/simplicial/nebulous/views/GameView;->a(Lsoftware/simplicial/nebulous/views/d;Lsoftware/simplicial/a/bh;FFFF)V

    .line 650
    :cond_2
    iget v0, p2, Lsoftware/simplicial/a/bh;->t:F

    iget v1, p2, Lsoftware/simplicial/a/bh;->w:F

    add-float/2addr v0, v1

    cmpl-float v0, v0, v7

    if-lez v0, :cond_3

    .line 652
    iget v0, p2, Lsoftware/simplicial/a/bh;->w:F

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float v3, v0, v1

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    iget v0, p2, Lsoftware/simplicial/a/bh;->t:F

    sub-float v6, v7, v0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lsoftware/simplicial/nebulous/views/GameView;->a(Lsoftware/simplicial/nebulous/views/d;Lsoftware/simplicial/a/bh;FFFF)V

    .line 654
    :cond_3
    return-void

    .line 634
    :cond_4
    const/4 v1, 0x0

    .line 635
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v0, v0, Lsoftware/simplicial/a/u;->aE:F

    add-float/2addr v0, v1

    move v7, v0

    move v8, v1

    goto :goto_0
.end method

.method private a(Lsoftware/simplicial/nebulous/views/d;Lsoftware/simplicial/a/bh;FFFF)V
    .locals 8

    .prologue
    .line 586
    invoke-static {p6}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    mul-float v2, p4, p4

    mul-float v3, p5, p5

    add-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    float-to-double v2, p3

    cmpg-double v0, v0, v2

    if-gez v0, :cond_1

    .line 621
    :cond_0
    return-void

    .line 590
    :cond_1
    const/4 v0, 0x1

    :goto_0
    iget v1, p1, Lsoftware/simplicial/nebulous/views/d;->d:I

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 592
    iget-object v1, p1, Lsoftware/simplicial/nebulous/views/d;->g:[F

    aget v1, v1, v0

    iget v2, p2, Lsoftware/simplicial/a/bh;->w:F

    mul-float v3, v1, v2

    .line 593
    iget-object v1, p1, Lsoftware/simplicial/nebulous/views/d;->h:[F

    aget v1, v1, v0

    iget v2, p2, Lsoftware/simplicial/a/bh;->w:F

    mul-float v4, v1, v2

    .line 594
    mul-float v1, v3, p4

    mul-float v2, v4, p5

    add-float/2addr v1, v2

    cmpg-float v1, v1, p6

    if-gtz v1, :cond_3

    .line 590
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 598
    :cond_3
    const/high16 v2, 0x3f800000    # 1.0f

    .line 599
    neg-float v1, v2

    mul-float/2addr v1, v3

    div-float/2addr v1, v4

    .line 601
    const/4 v5, 0x0

    cmpl-float v5, v4, v5

    if-nez v5, :cond_4

    .line 603
    const/4 v2, 0x0

    .line 604
    const/high16 v1, 0x3f800000    # 1.0f

    .line 607
    :cond_4
    mul-float v5, p6, v1

    mul-float v6, p4, v1

    mul-float v7, p5, v2

    sub-float/2addr v6, v7

    div-float/2addr v5, v6

    .line 608
    neg-float v2, v2

    div-float v1, v2, v1

    mul-float/2addr v1, v5

    .line 611
    iget v2, p2, Lsoftware/simplicial/a/bh;->w:F

    div-float v2, v5, v2

    .line 612
    iget v5, p2, Lsoftware/simplicial/a/bh;->w:F

    div-float/2addr v1, v5

    .line 615
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v5

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v3, v5, v3

    if-ltz v3, :cond_5

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v3

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_2

    .line 617
    :cond_5
    iget-object v3, p1, Lsoftware/simplicial/nebulous/views/d;->g:[F

    aput v2, v3, v0

    .line 618
    iget-object v2, p1, Lsoftware/simplicial/nebulous/views/d;->h:[F

    aput v1, v2, v0

    goto :goto_1
.end method

.method private a(Lsoftware/simplicial/nebulous/views/d;Lsoftware/simplicial/a/bh;Lsoftware/simplicial/a/ca;)V
    .locals 7

    .prologue
    .line 676
    iget v0, p3, Lsoftware/simplicial/a/ca;->h:F

    iget v1, p3, Lsoftware/simplicial/a/ca;->f:F

    sub-float/2addr v0, v1

    .line 677
    iget v1, p3, Lsoftware/simplicial/a/ca;->i:F

    iget v2, p3, Lsoftware/simplicial/a/ca;->g:F

    sub-float v2, v1, v2

    .line 678
    iget v1, p2, Lsoftware/simplicial/a/bh;->s:F

    iget v3, p3, Lsoftware/simplicial/a/ca;->f:F

    sub-float/2addr v1, v3

    .line 679
    iget v3, p2, Lsoftware/simplicial/a/bh;->t:F

    iget v4, p3, Lsoftware/simplicial/a/ca;->g:F

    sub-float/2addr v3, v4

    .line 680
    mul-float/2addr v1, v0

    mul-float/2addr v3, v2

    add-float/2addr v1, v3

    .line 681
    mul-float v3, v0, v0

    mul-float v4, v2, v2

    add-float/2addr v3, v4

    .line 682
    div-float v3, v1, v3

    .line 685
    const/4 v1, 0x0

    cmpg-float v1, v3, v1

    if-gez v1, :cond_0

    .line 687
    iget v1, p3, Lsoftware/simplicial/a/ca;->f:F

    .line 688
    iget v0, p3, Lsoftware/simplicial/a/ca;->g:F

    .line 701
    :goto_0
    iget v2, p2, Lsoftware/simplicial/a/bh;->s:F

    sub-float v4, v1, v2

    .line 702
    iget v1, p2, Lsoftware/simplicial/a/bh;->t:F

    sub-float v5, v0, v1

    .line 706
    mul-float v0, v4, v4

    mul-float v1, v5, v5

    add-float v6, v0, v1

    .line 708
    iget v0, p2, Lsoftware/simplicial/a/bh;->w:F

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float v3, v0, v1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lsoftware/simplicial/nebulous/views/GameView;->a(Lsoftware/simplicial/nebulous/views/d;Lsoftware/simplicial/a/bh;FFFF)V

    .line 709
    return-void

    .line 690
    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, v3, v1

    if-lez v1, :cond_1

    .line 692
    iget v1, p3, Lsoftware/simplicial/a/ca;->h:F

    .line 693
    iget v0, p3, Lsoftware/simplicial/a/ca;->i:F

    goto :goto_0

    .line 697
    :cond_1
    mul-float/2addr v0, v3

    iget v1, p3, Lsoftware/simplicial/a/ca;->f:F

    add-float/2addr v1, v0

    .line 698
    mul-float v0, v3, v2

    iget v2, p3, Lsoftware/simplicial/a/ca;->g:F

    add-float/2addr v0, v2

    goto :goto_0
.end method

.method private a(Lsoftware/simplicial/nebulous/views/d;Lsoftware/simplicial/a/bh;Lsoftware/simplicial/a/h;F)V
    .locals 8

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v7, 0x3f000000    # 0.5f

    .line 560
    iget v0, p3, Lsoftware/simplicial/a/h;->s:F

    iget v1, p2, Lsoftware/simplicial/a/bh;->s:F

    sub-float/2addr v0, v1

    .line 561
    iget v1, p3, Lsoftware/simplicial/a/h;->t:F

    iget v2, p2, Lsoftware/simplicial/a/bh;->t:F

    sub-float/2addr v1, v2

    .line 563
    iget v2, p2, Lsoftware/simplicial/a/bh;->w:F

    iget v3, p2, Lsoftware/simplicial/a/bh;->w:F

    mul-float/2addr v2, v3

    .line 564
    mul-float v3, p4, p4

    .line 567
    mul-float v4, v5, v0

    .line 568
    mul-float/2addr v5, v1

    .line 569
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    .line 570
    sub-float v1, v2, v3

    add-float v6, v1, v0

    .line 573
    iget v1, p2, Lsoftware/simplicial/a/bh;->w:F

    cmpl-float v1, v1, p4

    if-lez v1, :cond_0

    float-to-double v0, v0

    invoke-static {v0, v1}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v0

    iget v2, p2, Lsoftware/simplicial/a/bh;->w:F

    float-to-double v2, v2

    sub-double/2addr v0, v2

    mul-float v2, v7, p4

    float-to-double v2, v2

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    .line 582
    :goto_0
    return-void

    .line 576
    :cond_0
    iget v0, p2, Lsoftware/simplicial/a/bh;->w:F

    .line 577
    sget v1, Lsoftware/simplicial/a/bh;->e:F

    cmpg-float v1, v0, v1

    if-gez v1, :cond_1

    .line 578
    sget v0, Lsoftware/simplicial/a/bh;->e:F

    .line 579
    :cond_1
    mul-float v3, v0, v7

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    .line 581
    invoke-direct/range {v0 .. v6}, Lsoftware/simplicial/nebulous/views/GameView;->a(Lsoftware/simplicial/nebulous/views/d;Lsoftware/simplicial/a/bh;FFFF)V

    goto :goto_0
.end method

.method public static a([FI)V
    .locals 1

    .prologue
    .line 555
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lsoftware/simplicial/nebulous/views/GameView;->a([FII)V

    .line 556
    return-void
.end method

.method public static a([FII)V
    .locals 5

    .prologue
    const/high16 v4, 0x437f0000    # 255.0f

    .line 534
    sget-boolean v0, Lsoftware/simplicial/nebulous/views/GameView;->d:Z

    if-eqz v0, :cond_1

    .line 536
    const v0, -0xff1e00

    if-ne p2, v0, :cond_0

    .line 537
    const p2, -0xff6e00

    .line 538
    :cond_0
    const v0, -0xff0100

    if-ne p2, v0, :cond_1

    .line 539
    const p2, -0xff5100

    .line 542
    :cond_1
    and-int/lit16 v0, p2, 0xff

    .line 543
    shr-int/lit8 v1, p2, 0x8

    and-int/lit16 v1, v1, 0xff

    .line 544
    shr-int/lit8 v2, p2, 0x10

    and-int/lit16 v2, v2, 0xff

    .line 545
    shr-int/lit8 v3, p2, 0x18

    and-int/lit16 v3, v3, 0xff

    .line 547
    int-to-float v0, v0

    div-float/2addr v0, v4

    aput v0, p0, p1

    .line 548
    add-int/lit8 v0, p1, 0x1

    int-to-float v1, v1

    div-float/2addr v1, v4

    aput v1, p0, v0

    .line 549
    add-int/lit8 v0, p1, 0x2

    int-to-float v1, v2

    div-float/2addr v1, v4

    aput v1, p0, v0

    .line 550
    add-int/lit8 v0, p1, 0x3

    int-to-float v1, v3

    div-float/2addr v1, v4

    aput v1, p0, v0

    .line 551
    return-void
.end method

.method public static a([Ljava/lang/Object;I)V
    .locals 3

    .prologue
    .line 496
    const/4 v0, 0x0

    .line 498
    :goto_0
    if-ge v0, p1, :cond_0

    .line 501
    aget-object v1, p0, v0

    .line 502
    aget-object v2, p0, p1

    aput-object v2, p0, v0

    .line 503
    aput-object v1, p0, p1

    .line 506
    add-int/lit8 v0, v0, 0x1

    .line 507
    add-int/lit8 p1, p1, -0x1

    .line 508
    goto :goto_0

    .line 509
    :cond_0
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/views/GameView;Z)Z
    .locals 0

    .prologue
    .line 115
    iput-boolean p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->dk:Z

    return p1
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/views/GameView;)[Ljava/nio/FloatBuffer;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->M:[Ljava/nio/FloatBuffer;

    return-object v0
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/views/GameView;[Ljava/nio/FloatBuffer;)[Ljava/nio/FloatBuffer;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->M:[Ljava/nio/FloatBuffer;

    return-object p1
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/views/GameView;[Lsoftware/simplicial/nebulous/views/GameView$b;)[Lsoftware/simplicial/nebulous/views/GameView$b;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cw:[Lsoftware/simplicial/nebulous/views/GameView$b;

    return-object p1
.end method

.method static synthetic aA(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bn:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic aB(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/b;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->at:Lsoftware/simplicial/nebulous/views/b/b;

    return-object v0
.end method

.method static synthetic aC(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/b;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aq:Lsoftware/simplicial/nebulous/views/b/b;

    return-object v0
.end method

.method static synthetic aD(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/b;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ar:Lsoftware/simplicial/nebulous/views/b/b;

    return-object v0
.end method

.method static synthetic aE(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/b;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->as:Lsoftware/simplicial/nebulous/views/b/b;

    return-object v0
.end method

.method static synthetic aF(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/e;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ao:Lsoftware/simplicial/nebulous/views/b/e;

    return-object v0
.end method

.method static synthetic aG(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/e;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ap:Lsoftware/simplicial/nebulous/views/b/e;

    return-object v0
.end method

.method static synthetic aH(Lsoftware/simplicial/nebulous/views/GameView;)[Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bS:[Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic aI(Lsoftware/simplicial/nebulous/views/GameView;)[Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bT:[Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic aJ(Lsoftware/simplicial/nebulous/views/GameView;)[Lsoftware/simplicial/nebulous/views/b/g;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bV:[Lsoftware/simplicial/nebulous/views/b/g;

    return-object v0
.end method

.method static synthetic aK(Lsoftware/simplicial/nebulous/views/GameView;)[Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bU:[Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic aL(Lsoftware/simplicial/nebulous/views/GameView;)[Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bW:[Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic aM(Lsoftware/simplicial/nebulous/views/GameView;)[Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bX:[Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic aN(Lsoftware/simplicial/nebulous/views/GameView;)[Lsoftware/simplicial/nebulous/views/b/l;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bY:[Lsoftware/simplicial/nebulous/views/b/l;

    return-object v0
.end method

.method static synthetic aO(Lsoftware/simplicial/nebulous/views/GameView;)[Lsoftware/simplicial/nebulous/views/b/l;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bZ:[Lsoftware/simplicial/nebulous/views/b/l;

    return-object v0
.end method

.method static synthetic aP(Lsoftware/simplicial/nebulous/views/GameView;)[Lsoftware/simplicial/nebulous/views/b/i;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ca:[Lsoftware/simplicial/nebulous/views/b/i;

    return-object v0
.end method

.method static synthetic aQ(Lsoftware/simplicial/nebulous/views/GameView;)[Lsoftware/simplicial/nebulous/views/b/h;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cb:[Lsoftware/simplicial/nebulous/views/b/h;

    return-object v0
.end method

.method static synthetic aR(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/a/f;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ai:Lsoftware/simplicial/nebulous/views/a/f;

    return-object v0
.end method

.method static synthetic aS(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/a/u;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    return-object v0
.end method

.method static synthetic aT(Lsoftware/simplicial/nebulous/views/GameView;)F
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->af:F

    return v0
.end method

.method static synthetic aU(Lsoftware/simplicial/nebulous/views/GameView;)F
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ag:F

    return v0
.end method

.method static synthetic aV(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/a/c;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aj:Lsoftware/simplicial/nebulous/views/a/c;

    return-object v0
.end method

.method static synthetic aW(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/a/bf;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cF:Lsoftware/simplicial/a/bf;

    return-object v0
.end method

.method static synthetic aX(Lsoftware/simplicial/nebulous/views/GameView;)F
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cG:F

    return v0
.end method

.method static synthetic aY(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/a/g;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->an:Lsoftware/simplicial/nebulous/views/a/g;

    return-object v0
.end method

.method static synthetic aZ(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/a/b;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->am:Lsoftware/simplicial/nebulous/views/a/b;

    return-object v0
.end method

.method static synthetic aa(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bf:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic ab(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bg:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic ac(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aT:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic ad(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aU:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic ae(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aV:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic af(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aW:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic ag(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aX:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic ah(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aY:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic ai(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aZ:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic aj(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ba:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic ak(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bb:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic al(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bc:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic am(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bd:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic an(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bh:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic ao(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aH:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic ap(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aA:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic aq(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aB:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic ar(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->az:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic as(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aM:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic at(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aN:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic au(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aO:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic av(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bi:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic aw(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bj:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic ax(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bk:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic ay(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bl:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic az(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bm:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method private static b(F)F
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const v0, 0x3d4ccccd    # 0.05f

    .line 4999
    div-float v1, p0, v2

    sub-float v1, v2, v1

    .line 5000
    cmpg-float v2, v1, v0

    if-gez v2, :cond_0

    .line 5002
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/views/GameView;F)F
    .locals 0

    .prologue
    .line 115
    iput p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->ae:F

    return p1
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/views/GameView;I)I
    .locals 0

    .prologue
    .line 115
    iput p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->dn:I

    return p1
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/views/GameView;J)J
    .locals 1

    .prologue
    .line 115
    iput-wide p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->ck:J

    return-wide p1
.end method

.method private static b(Landroid/graphics/Bitmap;Ljava/lang/String;)Ljava/io/File;
    .locals 4

    .prologue
    .line 513
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Screenshots"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 514
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 515
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 516
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 517
    :cond_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 520
    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 521
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x5a

    invoke-virtual {p0, v1, v3, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 522
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->flush()V

    .line 523
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 529
    :goto_0
    return-object v2

    .line 525
    :catch_0
    move-exception v0

    .line 527
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/views/GameView;Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->K:Ljava/nio/FloatBuffer;

    return-object p1
.end method

.method private b()V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 5008
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v0, v0, Lsoftware/simplicial/a/u;->P:F

    cmpl-float v0, v0, v5

    if-lez v0, :cond_0

    .line 5010
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->al:Lsoftware/simplicial/nebulous/views/a/a;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->R:Ljava/nio/FloatBuffer;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/GameView;->S:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/nebulous/views/a/a;->a(Ljava/nio/FloatBuffer;Ljava/nio/FloatBuffer;)V

    .line 5011
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->al:Lsoftware/simplicial/nebulous/views/a/a;

    sget-object v1, Lsoftware/simplicial/nebulous/views/GameView;->r:[F

    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v2, v2, Lsoftware/simplicial/a/u;->P:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/nebulous/views/a/a;->a([FF)V

    .line 5012
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ao:Lsoftware/simplicial/nebulous/views/b/e;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/views/b/e;->a()Z

    .line 5013
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ad:F

    iget v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->ae:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 5014
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->al:Lsoftware/simplicial/nebulous/views/a/a;

    iget v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->af:F

    iget v2, p0, Lsoftware/simplicial/nebulous/views/GameView;->ag:F

    move v4, v3

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lsoftware/simplicial/nebulous/views/a/a;->a(FFFFFF)V

    .line 5015
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->al:Lsoftware/simplicial/nebulous/views/a/a;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/views/a/a;->a(I)V

    .line 5017
    :cond_0
    return-void
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/views/GameView;Z)Z
    .locals 0

    .prologue
    .line 115
    iput-boolean p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cj:Z

    return p1
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/views/GameView;)[Ljava/nio/FloatBuffer;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->N:[Ljava/nio/FloatBuffer;

    return-object v0
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/views/GameView;[Ljava/nio/FloatBuffer;)[Ljava/nio/FloatBuffer;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->N:[Ljava/nio/FloatBuffer;

    return-object p1
.end method

.method static synthetic bA(Lsoftware/simplicial/nebulous/views/GameView;)Ljava/nio/FloatBuffer;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->V:Ljava/nio/FloatBuffer;

    return-object v0
.end method

.method static synthetic bB(Lsoftware/simplicial/nebulous/views/GameView;)Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cT:Z

    return v0
.end method

.method static synthetic bC(Lsoftware/simplicial/nebulous/views/GameView;)I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->dh:I

    return v0
.end method

.method static synthetic bD(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/a/c/h;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->dg:Lsoftware/simplicial/a/c/h;

    return-object v0
.end method

.method static synthetic bE(Lsoftware/simplicial/nebulous/views/GameView;)I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->di:I

    return v0
.end method

.method static synthetic bF(Lsoftware/simplicial/nebulous/views/GameView;)I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->de:I

    return v0
.end method

.method static synthetic bG(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/a/h/a$a;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->df:Lsoftware/simplicial/a/h/a$a;

    return-object v0
.end method

.method static synthetic bH(Lsoftware/simplicial/nebulous/views/GameView;)I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->dd:I

    return v0
.end method

.method static synthetic bI(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/a/i/e;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->da:Lsoftware/simplicial/a/i/e;

    return-object v0
.end method

.method static synthetic bJ(Lsoftware/simplicial/nebulous/views/GameView;)Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cz:Z

    return v0
.end method

.method static synthetic bK(Lsoftware/simplicial/nebulous/views/GameView;)I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cu:I

    return v0
.end method

.method static synthetic bL(Lsoftware/simplicial/nebulous/views/GameView;)J
    .locals 2

    .prologue
    .line 115
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ck:J

    return-wide v0
.end method

.method static synthetic bM(Lsoftware/simplicial/nebulous/views/GameView;)Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cD:Z

    return v0
.end method

.method static synthetic bN(Lsoftware/simplicial/nebulous/views/GameView;)Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cN:Z

    return v0
.end method

.method static synthetic bO(Lsoftware/simplicial/nebulous/views/GameView;)Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cO:Z

    return v0
.end method

.method static synthetic bP(Lsoftware/simplicial/nebulous/views/GameView;)I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cl:I

    return v0
.end method

.method static synthetic bQ(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/f/aj;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cJ:Lsoftware/simplicial/nebulous/f/aj;

    return-object v0
.end method

.method static synthetic bR(Lsoftware/simplicial/nebulous/views/GameView;)F
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cK:F

    return v0
.end method

.method static synthetic bS(Lsoftware/simplicial/nebulous/views/GameView;)Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cV:Z

    return v0
.end method

.method static synthetic bT(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/c/b;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cI:Lsoftware/simplicial/nebulous/c/b;

    return-object v0
.end method

.method static synthetic bU(Lsoftware/simplicial/nebulous/views/GameView;)Ljava/nio/FloatBuffer;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->T:Ljava/nio/FloatBuffer;

    return-object v0
.end method

.method static synthetic bV(Lsoftware/simplicial/nebulous/views/GameView;)Ljava/nio/FloatBuffer;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->U:Ljava/nio/FloatBuffer;

    return-object v0
.end method

.method static synthetic bW(Lsoftware/simplicial/nebulous/views/GameView;)Ljava/nio/FloatBuffer;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->W:Ljava/nio/FloatBuffer;

    return-object v0
.end method

.method static synthetic bX(Lsoftware/simplicial/nebulous/views/GameView;)Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cB:Z

    return v0
.end method

.method static synthetic bY(Lsoftware/simplicial/nebulous/views/GameView;)[F
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ci:[F

    return-object v0
.end method

.method static synthetic bZ(Lsoftware/simplicial/nebulous/views/GameView;)F
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->dj:F

    return v0
.end method

.method static synthetic ba(Lsoftware/simplicial/nebulous/views/GameView;)Ljava/util/Random;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cn:Ljava/util/Random;

    return-object v0
.end method

.method static synthetic bb(Lsoftware/simplicial/nebulous/views/GameView;)F
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ad:F

    return v0
.end method

.method static synthetic bc(Lsoftware/simplicial/nebulous/views/GameView;)F
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ae:F

    return v0
.end method

.method static synthetic bd(Lsoftware/simplicial/nebulous/views/GameView;)Landroid/util/DisplayMetrics;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cm:Landroid/util/DisplayMetrics;

    return-object v0
.end method

.method static synthetic be(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/a/e;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ak:Lsoftware/simplicial/nebulous/views/a/e;

    return-object v0
.end method

.method static synthetic bf(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/c/d;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->co:Lsoftware/simplicial/nebulous/c/d;

    return-object v0
.end method

.method static synthetic bg(Lsoftware/simplicial/nebulous/views/GameView;)I
    .locals 2

    .prologue
    .line 115
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cl:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cl:I

    return v0
.end method

.method static synthetic bh(Lsoftware/simplicial/nebulous/views/GameView;)J
    .locals 2

    .prologue
    .line 115
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ct:J

    return-wide v0
.end method

.method static synthetic bi(Lsoftware/simplicial/nebulous/views/GameView;)I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cv:I

    return v0
.end method

.method static synthetic bj(Lsoftware/simplicial/nebulous/views/GameView;)Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->dk:Z

    return v0
.end method

.method static synthetic bk(Lsoftware/simplicial/nebulous/views/GameView;)I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->dn:I

    return v0
.end method

.method static synthetic bl(Lsoftware/simplicial/nebulous/views/GameView;)[Lsoftware/simplicial/nebulous/views/GameView$b;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cw:[Lsoftware/simplicial/nebulous/views/GameView$b;

    return-object v0
.end method

.method static synthetic bm(Lsoftware/simplicial/nebulous/views/GameView;)V
    .locals 0

    .prologue
    .line 115
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/views/GameView;->c()V

    return-void
.end method

.method static synthetic bn(Lsoftware/simplicial/nebulous/views/GameView;)[I
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cp:[I

    return-object v0
.end method

.method static synthetic bo(Lsoftware/simplicial/nebulous/views/GameView;)V
    .locals 0

    .prologue
    .line 115
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/views/GameView;->b()V

    return-void
.end method

.method static synthetic bp(Lsoftware/simplicial/nebulous/views/GameView;)Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cj:Z

    return v0
.end method

.method static synthetic bq(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/a/ao;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cZ:Lsoftware/simplicial/a/ao;

    return-object v0
.end method

.method static synthetic br(Lsoftware/simplicial/nebulous/views/GameView;)Ljava/nio/FloatBuffer;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->R:Ljava/nio/FloatBuffer;

    return-object v0
.end method

.method static synthetic bs(Lsoftware/simplicial/nebulous/views/GameView;)Ljava/nio/FloatBuffer;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->S:Ljava/nio/FloatBuffer;

    return-object v0
.end method

.method static synthetic bt(Lsoftware/simplicial/nebulous/views/GameView;)I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->dc:I

    return v0
.end method

.method static synthetic bu(Lsoftware/simplicial/nebulous/views/GameView;)F
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cL:F

    return v0
.end method

.method static synthetic bv(Lsoftware/simplicial/nebulous/views/GameView;)Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cU:Z

    return v0
.end method

.method static synthetic bw(Lsoftware/simplicial/nebulous/views/GameView;)Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cM:Z

    return v0
.end method

.method static synthetic bx(Lsoftware/simplicial/nebulous/views/GameView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cH:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic by(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/a/bj;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->db:Lsoftware/simplicial/a/bj;

    return-object v0
.end method

.method static synthetic bz(Lsoftware/simplicial/nebulous/views/GameView;)Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cE:Z

    return v0
.end method

.method static synthetic c(Lsoftware/simplicial/nebulous/views/GameView;F)F
    .locals 0

    .prologue
    .line 115
    iput p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->af:F

    return p1
.end method

.method static synthetic c(Lsoftware/simplicial/nebulous/views/GameView;I)I
    .locals 0

    .prologue
    .line 115
    iput p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cu:I

    return p1
.end method

.method static synthetic c(Lsoftware/simplicial/nebulous/views/GameView;Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->P:Ljava/nio/FloatBuffer;

    return-object p1
.end method

.method private c()V
    .locals 13

    .prologue
    .line 5088
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-object v0, v0, Lsoftware/simplicial/a/u;->L:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bh;

    .line 5090
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/GameView;->F:[Lsoftware/simplicial/nebulous/views/j;

    iget v3, v0, Lsoftware/simplicial/a/bh;->k:I

    aget-object v2, v2, v3

    .line 5091
    iget-object v3, v2, Lsoftware/simplicial/nebulous/views/j;->a:[Lsoftware/simplicial/nebulous/views/d;

    iget v4, v0, Lsoftware/simplicial/a/bh;->c:I

    aget-object v3, v3, v4

    .line 5092
    iget-boolean v2, v2, Lsoftware/simplicial/nebulous/views/j;->h:Z

    if-eqz v2, :cond_1

    iget v0, v0, Lsoftware/simplicial/a/bh;->c:I

    if-eqz v0, :cond_0

    .line 5093
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, v3, Lsoftware/simplicial/nebulous/views/d;->k:Z

    goto :goto_0

    .line 5096
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-object v0, v0, Lsoftware/simplicial/a/u;->J:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_3
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lsoftware/simplicial/a/ae;

    .line 5098
    const/high16 v0, 0x41400000    # 12.0f

    invoke-direct {p0, v5, v0}, Lsoftware/simplicial/nebulous/views/GameView;->a(Lsoftware/simplicial/a/ao;F)Lsoftware/simplicial/a/bh;

    move-result-object v0

    .line 5100
    if-eqz v0, :cond_3

    .line 5102
    iget v1, v0, Lsoftware/simplicial/a/bh;->s:F

    iget v2, v5, Lsoftware/simplicial/a/ae;->u:F

    sub-float/2addr v1, v2

    .line 5103
    iget v0, v0, Lsoftware/simplicial/a/bh;->t:F

    iget v2, v5, Lsoftware/simplicial/a/ae;->v:F

    sub-float v2, v0, v2

    .line 5104
    mul-float v0, v1, v1

    mul-float v3, v2, v2

    add-float/2addr v0, v3

    float-to-double v6, v0

    invoke-static {v6, v7}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v6

    double-to-float v0, v6

    .line 5105
    const v3, 0x3dcccccd    # 0.1f

    cmpg-float v3, v0, v3

    if-gez v3, :cond_4

    .line 5106
    const v0, 0x3dcccccd    # 0.1f

    .line 5107
    :cond_4
    const/high16 v3, 0x41a00000    # 20.0f

    mul-float/2addr v1, v3

    div-float v6, v1, v0

    .line 5108
    const/high16 v1, 0x41a00000    # 20.0f

    mul-float/2addr v1, v2

    div-float v7, v1, v0

    .line 5109
    iget-object v9, p0, Lsoftware/simplicial/nebulous/views/GameView;->G:Ljava/util/List;

    new-instance v0, Lsoftware/simplicial/nebulous/views/a;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cp:[I

    iget-short v2, v5, Lsoftware/simplicial/a/ae;->b:S

    aget v1, v1, v2

    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-wide v2, v2, Lsoftware/simplicial/a/u;->aO:J

    iget v4, v5, Lsoftware/simplicial/a/ae;->u:F

    iget v5, v5, Lsoftware/simplicial/a/ae;->v:F

    invoke-direct/range {v0 .. v7}, Lsoftware/simplicial/nebulous/views/a;-><init>(IJFFFF)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 5113
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-object v0, v0, Lsoftware/simplicial/a/u;->K:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lsoftware/simplicial/a/bt;

    .line 5115
    const/high16 v0, 0x41400000    # 12.0f

    invoke-direct {p0, v10, v0}, Lsoftware/simplicial/nebulous/views/GameView;->a(Lsoftware/simplicial/a/ao;F)Lsoftware/simplicial/a/bh;

    move-result-object v0

    .line 5117
    if-eqz v0, :cond_7

    .line 5119
    iget v1, v0, Lsoftware/simplicial/a/bh;->s:F

    iget v2, v10, Lsoftware/simplicial/a/bt;->u:F

    sub-float/2addr v1, v2

    .line 5120
    iget v0, v0, Lsoftware/simplicial/a/bh;->t:F

    iget v2, v10, Lsoftware/simplicial/a/bt;->v:F

    sub-float v2, v0, v2

    .line 5121
    mul-float v0, v1, v1

    mul-float v3, v2, v2

    add-float/2addr v0, v3

    float-to-double v4, v0

    invoke-static {v4, v5}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v4

    double-to-float v0, v4

    .line 5122
    const v3, 0x3dcccccd    # 0.1f

    cmpg-float v3, v0, v3

    if-gez v3, :cond_6

    .line 5123
    const v0, 0x3dcccccd    # 0.1f

    .line 5124
    :cond_6
    const/high16 v3, 0x41a00000    # 20.0f

    mul-float/2addr v1, v3

    div-float v6, v1, v0

    .line 5125
    const/high16 v1, 0x41a00000    # 20.0f

    mul-float/2addr v1, v2

    div-float v7, v1, v0

    .line 5126
    iget-object v12, p0, Lsoftware/simplicial/nebulous/views/GameView;->H:Ljava/util/List;

    new-instance v0, Lsoftware/simplicial/nebulous/views/c;

    iget-object v1, v10, Lsoftware/simplicial/a/bt;->d:Lsoftware/simplicial/a/bv;

    iget v2, v10, Lsoftware/simplicial/a/bt;->e:I

    iget-byte v3, v10, Lsoftware/simplicial/a/bt;->a:B

    iget v4, v10, Lsoftware/simplicial/a/bt;->u:F

    iget v5, v10, Lsoftware/simplicial/a/bt;->v:F

    iget-object v8, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-wide v8, v8, Lsoftware/simplicial/a/u;->aO:J

    invoke-direct/range {v0 .. v9}, Lsoftware/simplicial/nebulous/views/c;-><init>(Lsoftware/simplicial/a/bv;IIFFFFJ)V

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 5129
    :cond_7
    const/4 v0, 0x0

    iput v0, v10, Lsoftware/simplicial/a/bt;->f:F

    goto :goto_2

    .line 5132
    :cond_8
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cx:Lsoftware/simplicial/nebulous/views/f;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-wide v2, v1, Lsoftware/simplicial/a/u;->aO:J

    invoke-virtual {v0, v2, v3}, Lsoftware/simplicial/nebulous/views/f;->a(J)V

    .line 5133
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-object v0, v0, Lsoftware/simplicial/a/u;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_9
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bq;

    .line 5135
    iget-object v2, v0, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    sget-object v3, Lsoftware/simplicial/a/bq$a;->b:Lsoftware/simplicial/a/bq$a;

    if-ne v2, v3, :cond_9

    iget-object v2, v0, Lsoftware/simplicial/a/bq;->N:Lsoftware/simplicial/a/s;

    sget-object v3, Lsoftware/simplicial/a/s;->a:Lsoftware/simplicial/a/s;

    if-eq v2, v3, :cond_9

    .line 5137
    iget v2, v0, Lsoftware/simplicial/a/bq;->u:F

    iget v3, v0, Lsoftware/simplicial/a/bq;->s:F

    sub-float/2addr v2, v3

    .line 5138
    iget v3, v0, Lsoftware/simplicial/a/bq;->v:F

    iget v4, v0, Lsoftware/simplicial/a/bq;->t:F

    sub-float/2addr v3, v4

    .line 5139
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v4

    if-gtz v2, :cond_a

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_9

    .line 5140
    :cond_a
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/GameView;->cx:Lsoftware/simplicial/nebulous/views/f;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-wide v4, v3, Lsoftware/simplicial/a/u;->aO:J

    invoke-virtual {v2, v4, v5, v0}, Lsoftware/simplicial/nebulous/views/f;->a(JLsoftware/simplicial/a/bq;)V

    goto :goto_3

    .line 5144
    :cond_b
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 5145
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 5147
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/views/g;

    .line 5148
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-wide v2, v2, Lsoftware/simplicial/a/u;->aO:J

    iget-wide v4, v0, Lsoftware/simplicial/nebulous/views/g;->c:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_d

    .line 5150
    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/g;->b:Lsoftware/simplicial/nebulous/views/d;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/views/d;->I:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 5151
    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/g;->a:Lsoftware/simplicial/nebulous/views/j;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/views/j;->k:Lsoftware/simplicial/nebulous/views/g;

    if-ne v2, v0, :cond_c

    .line 5152
    iget-object v0, v0, Lsoftware/simplicial/nebulous/views/g;->a:Lsoftware/simplicial/nebulous/views/j;

    const/4 v2, 0x0

    iput-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->k:Lsoftware/simplicial/nebulous/views/g;

    .line 5153
    :cond_c
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_4

    .line 5156
    :cond_d
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-wide v2, v2, Lsoftware/simplicial/a/u;->aO:J

    invoke-virtual {v0, v2, v3}, Lsoftware/simplicial/nebulous/views/g;->a(J)V

    goto :goto_4

    .line 5159
    :cond_e
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->dl:Z

    if-eqz v0, :cond_10

    .line 5161
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-object v0, v0, Lsoftware/simplicial/a/u;->M:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_f
    :goto_5
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/a/x;

    .line 5163
    iget-byte v1, v0, Lsoftware/simplicial/a/a/x;->c:B

    add-int/lit8 v1, v1, -0x1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/GameView;->bW:[Lsoftware/simplicial/nebulous/views/b/m;

    array-length v2, v2

    if-ge v1, v2, :cond_f

    .line 5165
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->F:[Lsoftware/simplicial/nebulous/views/j;

    iget-byte v2, v0, Lsoftware/simplicial/a/a/x;->a:B

    aget-object v2, v1, v2

    .line 5166
    iget-object v1, v2, Lsoftware/simplicial/nebulous/views/j;->a:[Lsoftware/simplicial/nebulous/views/d;

    iget-byte v3, v0, Lsoftware/simplicial/a/a/x;->b:B

    aget-object v3, v1, v3

    .line 5167
    new-instance v1, Lsoftware/simplicial/nebulous/views/g;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-wide v4, v4, Lsoftware/simplicial/a/u;->aO:J

    iget-object v6, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-wide v6, v6, Lsoftware/simplicial/a/u;->aO:J

    const-wide/16 v10, 0x6d6

    add-long/2addr v6, v10

    iget-byte v8, v0, Lsoftware/simplicial/a/a/x;->c:B

    invoke-direct/range {v1 .. v8}, Lsoftware/simplicial/nebulous/views/g;-><init>(Lsoftware/simplicial/nebulous/views/j;Lsoftware/simplicial/nebulous/views/d;JJB)V

    .line 5168
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->I:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 5169
    iget-object v0, v3, Lsoftware/simplicial/nebulous/views/d;->I:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 5170
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cn:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextBoolean()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 5171
    iput-object v1, v2, Lsoftware/simplicial/nebulous/views/j;->k:Lsoftware/simplicial/nebulous/views/g;

    goto :goto_5

    .line 5175
    :cond_10
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    invoke-virtual {v0}, Lsoftware/simplicial/a/u;->e()Lsoftware/simplicial/a/bf;

    move-result-object v0

    .line 5176
    if-eqz v0, :cond_16

    iget v0, v0, Lsoftware/simplicial/a/bf;->C:I

    .line 5177
    :goto_6
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v1, v1, Lsoftware/simplicial/a/u;->j:I

    if-ltz v1, :cond_17

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v1, v1, Lsoftware/simplicial/a/u;->j:I

    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-object v2, v2, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    array-length v2, v2

    if-ge v1, v2, :cond_17

    .line 5179
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-object v0, v0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v1, v1, Lsoftware/simplicial/a/u;->j:I

    aget-object v0, v0, v1

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cF:Lsoftware/simplicial/a/bf;

    .line 5180
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cD:Z

    .line 5202
    :goto_7
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cD:Z

    if-nez v0, :cond_11

    .line 5204
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cP:Z

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cY:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1b

    .line 5205
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cF:Lsoftware/simplicial/a/bf;

    const/4 v1, -0x1

    iput v1, v0, Lsoftware/simplicial/a/bf;->ad:I

    .line 5210
    :cond_11
    :goto_8
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cF:Lsoftware/simplicial/a/bf;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v1, v1, Lsoftware/simplicial/a/u;->aE:F

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/bf;->a(F)F

    move-result v0
    
    ## FOV mod (multiplier)
    const/high16 v1, 0x3fc00000 # 1.5
    mul-float/2addr v0, v1
    ##
    

    iput v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->dj:F

    .line 5213
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-object v0, v0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    if-ne v0, v1, :cond_1f

    .line 5215
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-short v0, v0, Lsoftware/simplicial/a/u;->aH:S

    const/16 v1, -0x14

    if-gt v0, v1, :cond_1c

    .line 5217
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-object v0, v0, Lsoftware/simplicial/a/u;->al:[Lsoftware/simplicial/a/bx;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cZ:Lsoftware/simplicial/a/ao;

    .line 5218
    const/high16 v0, 0x43480000    # 200.0f

    move v1, v0

    .line 5282
    :goto_9
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cZ:Lsoftware/simplicial/a/ao;

    if-eqz v0, :cond_2b

    .line 5284
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cZ:Lsoftware/simplicial/a/ao;

    instance-of v0, v0, Lsoftware/simplicial/a/bf;

    if-eqz v0, :cond_12

    .line 5286
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cZ:Lsoftware/simplicial/a/ao;

    check-cast v0, Lsoftware/simplicial/a/bf;

    .line 5287
    const/4 v2, 0x1

    iput-boolean v2, v0, Lsoftware/simplicial/a/bf;->bE:Z

    .line 5288
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v2, v2, Lsoftware/simplicial/a/u;->aE:F

    invoke-virtual {v0, v2}, Lsoftware/simplicial/a/bf;->a(F)F

    .line 5291
    :cond_12
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-short v0, v0, Lsoftware/simplicial/a/u;->aH:S

    int-to-float v0, v0

    .line 5292
    const/high16 v2, -0x3e600000    # -20.0f

    cmpg-float v2, v0, v2

    if-gtz v2, :cond_25

    .line 5293
    const/high16 v2, 0x41a00000    # 20.0f

    add-float/2addr v0, v2

    .line 5297
    :cond_13
    :goto_a
    const/high16 v2, -0x3fc00000    # -3.0f

    cmpg-float v2, v0, v2

    if-gtz v2, :cond_26

    .line 5299
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cs:Lsoftware/simplicial/nebulous/views/GameView$a;

    sget-object v2, Lsoftware/simplicial/nebulous/views/GameView$a;->d:Lsoftware/simplicial/nebulous/views/GameView$a;

    if-eq v0, v2, :cond_14

    .line 5301
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cr:F

    .line 5302
    sget-object v0, Lsoftware/simplicial/nebulous/views/GameView$a;->d:Lsoftware/simplicial/nebulous/views/GameView$a;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cs:Lsoftware/simplicial/nebulous/views/GameView$a;

    .line 5306
    :cond_14
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cF:Lsoftware/simplicial/a/bf;

    iget v2, p0, Lsoftware/simplicial/nebulous/views/GameView;->cr:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/GameView;->cZ:Lsoftware/simplicial/a/ao;

    iget v3, v3, Lsoftware/simplicial/a/ao;->s:F

    iget-object v4, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v4, v4, Lsoftware/simplicial/a/u;->aE:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    invoke-static {v2, v3, v4}, Lb/a/a/d/a/a;->a(FFF)F

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/bf;->s:F

    .line 5307
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cF:Lsoftware/simplicial/a/bf;

    iget v2, p0, Lsoftware/simplicial/nebulous/views/GameView;->cr:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/GameView;->cZ:Lsoftware/simplicial/a/ao;

    iget v3, v3, Lsoftware/simplicial/a/ao;->t:F

    iget-object v4, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v4, v4, Lsoftware/simplicial/a/u;->aE:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    invoke-static {v2, v3, v4}, Lb/a/a/d/a/a;->a(FFF)F

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/bf;->t:F

    .line 5308
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cr:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v2, v2, Lsoftware/simplicial/a/u;->aE:F

    invoke-static {v0, v1, v2}, Lb/a/a/d/a/a;->a(FFF)F

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->dj:F

    .line 5309
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cr:F

    float-to-double v0, v0

    iget-wide v2, p0, Lsoftware/simplicial/nebulous/views/GameView;->dm:D

    add-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cr:F

    .line 5310
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cr:F

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_15

    .line 5311
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cr:F

    .line 5357
    :cond_15
    :goto_b
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->dj:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2c

    .line 5358
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cC:F

    iget v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->dj:F

    div-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cG:F

    .line 5362
    :goto_c
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->G:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 5363
    :goto_d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 5365
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/views/a;

    .line 5366
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-wide v2, v2, Lsoftware/simplicial/a/u;->aO:J

    iget-wide v4, v0, Lsoftware/simplicial/nebulous/views/a;->b:J

    sub-long/2addr v2, v4

    long-to-float v2, v2

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    .line 5367
    const v3, 0x3d99999a    # 0.075f

    cmpg-float v3, v2, v3

    if-gez v3, :cond_2d

    .line 5369
    iget v3, v0, Lsoftware/simplicial/nebulous/views/a;->e:F

    mul-float/2addr v3, v2

    iget v4, v0, Lsoftware/simplicial/nebulous/views/a;->c:F

    add-float/2addr v3, v4

    iput v3, v0, Lsoftware/simplicial/nebulous/views/a;->g:F

    .line 5370
    iget v3, v0, Lsoftware/simplicial/nebulous/views/a;->f:F

    mul-float/2addr v2, v3

    iget v3, v0, Lsoftware/simplicial/nebulous/views/a;->d:F

    add-float/2addr v2, v3

    iput v2, v0, Lsoftware/simplicial/nebulous/views/a;->h:F

    goto :goto_d

    .line 5176
    :cond_16
    const/4 v0, -0x1

    goto/16 :goto_6

    .line 5182
    :cond_17
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v1, v1, Lsoftware/simplicial/a/u;->j:I

    const/4 v2, -0x2

    if-ne v1, v2, :cond_19

    .line 5184
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-object v0, v0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    invoke-static {v0}, Lsoftware/simplicial/a/ai;->a([Lsoftware/simplicial/a/bf;)Lsoftware/simplicial/a/bf;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cF:Lsoftware/simplicial/a/bf;

    .line 5185
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cF:Lsoftware/simplicial/a/bf;

    if-nez v0, :cond_18

    .line 5186
    sget-object v0, Lsoftware/simplicial/a/ai;->U:Lsoftware/simplicial/a/bf;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cF:Lsoftware/simplicial/a/bf;

    .line 5187
    :cond_18
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cD:Z

    goto/16 :goto_7

    .line 5189
    :cond_19
    if-ltz v0, :cond_1a

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-object v1, v1, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    array-length v1, v1

    if-ge v0, v1, :cond_1a

    .line 5191
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-object v1, v1, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v0, v1, v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cF:Lsoftware/simplicial/a/bf;

    .line 5192
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cD:Z

    goto/16 :goto_7

    .line 5196
    :cond_1a
    sget-object v0, Lsoftware/simplicial/a/ai;->U:Lsoftware/simplicial/a/bf;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cF:Lsoftware/simplicial/a/bf;

    .line 5197
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cD:Z

    goto/16 :goto_7

    .line 5207
    :cond_1b
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cF:Lsoftware/simplicial/a/bf;

    const/4 v1, 0x0

    iput v1, v0, Lsoftware/simplicial/a/bf;->ad:I

    goto/16 :goto_8

    .line 5220
    :cond_1c
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-short v0, v0, Lsoftware/simplicial/a/u;->aH:S

    const/16 v1, -0xa

    if-gt v0, v1, :cond_1d

    .line 5222
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-object v0, v0, Lsoftware/simplicial/a/u;->al:[Lsoftware/simplicial/a/bx;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cZ:Lsoftware/simplicial/a/ao;

    .line 5223
    const/high16 v0, 0x43480000    # 200.0f

    move v1, v0

    goto/16 :goto_9

    .line 5225
    :cond_1d
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-short v0, v0, Lsoftware/simplicial/a/u;->aH:S

    if-gtz v0, :cond_1e

    .line 5227
    sget-object v0, Lsoftware/simplicial/a/ai;->U:Lsoftware/simplicial/a/bf;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cZ:Lsoftware/simplicial/a/ao;

    .line 5228
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cZ:Lsoftware/simplicial/a/ao;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v1, v1, Lsoftware/simplicial/a/u;->aE:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iput v1, v0, Lsoftware/simplicial/a/ao;->l:F

    .line 5229
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cZ:Lsoftware/simplicial/a/ao;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v1, v1, Lsoftware/simplicial/a/u;->aE:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iput v1, v0, Lsoftware/simplicial/a/ao;->m:F

    .line 5230
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cZ:Lsoftware/simplicial/a/ao;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v1, v1, Lsoftware/simplicial/a/u;->aE:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iput v1, v0, Lsoftware/simplicial/a/ao;->s:F

    .line 5231
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cZ:Lsoftware/simplicial/a/ao;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v1, v1, Lsoftware/simplicial/a/u;->aE:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iput v1, v0, Lsoftware/simplicial/a/ao;->t:F

    .line 5232
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v0, v0, Lsoftware/simplicial/a/u;->aE:F

    move v1, v0

    goto/16 :goto_9

    .line 5236
    :cond_1e
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cZ:Lsoftware/simplicial/a/ao;

    .line 5237
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v0, v0, Lsoftware/simplicial/a/u;->aE:F

    move v1, v0

    goto/16 :goto_9

    .line 5240
    :cond_1f
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-object v0, v0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->e:Lsoftware/simplicial/a/am;

    if-ne v0, v1, :cond_21

    .line 5242
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v0, v0, Lsoftware/simplicial/a/u;->i:I

    if-ltz v0, :cond_20

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v0, v0, Lsoftware/simplicial/a/u;->i:I

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v1, v1, Lsoftware/simplicial/a/u;->aj:I

    if-ge v0, v1, :cond_20

    .line 5244
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-object v0, v0, Lsoftware/simplicial/a/u;->al:[Lsoftware/simplicial/a/bx;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v1, v1, Lsoftware/simplicial/a/u;->i:I

    aget-object v0, v0, v1

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cZ:Lsoftware/simplicial/a/ao;

    .line 5245
    const/high16 v0, 0x43160000    # 150.0f

    move v1, v0

    goto/16 :goto_9

    .line 5249
    :cond_20
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v0, v0, Lsoftware/simplicial/a/u;->aE:F

    .line 5250
    const/4 v1, 0x0

    iput-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cZ:Lsoftware/simplicial/a/ao;

    move v1, v0

    goto/16 :goto_9

    .line 5253
    :cond_21
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v0, v0, Lsoftware/simplicial/a/u;->i:I

    if-ltz v0, :cond_24

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-short v0, v0, Lsoftware/simplicial/a/u;->aH:S

    if-gtz v0, :cond_24

    .line 5255
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v0, v0, Lsoftware/simplicial/a/u;->aj:I

    if-lez v0, :cond_22

    .line 5257
    sget-object v0, Lsoftware/simplicial/a/ai;->U:Lsoftware/simplicial/a/bf;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cZ:Lsoftware/simplicial/a/ao;

    .line 5258
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cZ:Lsoftware/simplicial/a/ao;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v1, v1, Lsoftware/simplicial/a/u;->aE:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iput v1, v0, Lsoftware/simplicial/a/ao;->s:F

    .line 5259
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cZ:Lsoftware/simplicial/a/ao;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v1, v1, Lsoftware/simplicial/a/u;->aE:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iput v1, v0, Lsoftware/simplicial/a/ao;->t:F

    .line 5260
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v0, v0, Lsoftware/simplicial/a/u;->aE:F

    move v1, v0

    goto/16 :goto_9

    .line 5264
    :cond_22
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v0, v0, Lsoftware/simplicial/a/u;->i:I

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-object v1, v1, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    array-length v1, v1

    if-ge v0, v1, :cond_23

    .line 5266
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-object v0, v0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v1, v1, Lsoftware/simplicial/a/u;->i:I

    aget-object v0, v0, v1

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cZ:Lsoftware/simplicial/a/ao;

    .line 5267
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-object v0, v0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v1, v1, Lsoftware/simplicial/a/u;->i:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v1, v1, Lsoftware/simplicial/a/u;->aE:F

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/a/bf;->a(FZ)F

    move-result v0

    move v1, v0

    goto/16 :goto_9

    .line 5271
    :cond_23
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v0, v0, Lsoftware/simplicial/a/u;->aE:F

    .line 5272
    const/4 v1, 0x0

    iput-object v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cZ:Lsoftware/simplicial/a/ao;

    move v1, v0

    goto/16 :goto_9

    .line 5278
    :cond_24
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cZ:Lsoftware/simplicial/a/ao;

    .line 5279
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget v0, v0, Lsoftware/simplicial/a/u;->aE:F

    move v1, v0

    goto/16 :goto_9

    .line 5294
    :cond_25
    const/high16 v2, -0x3ee00000    # -10.0f

    cmpg-float v2, v0, v2

    if-gtz v2, :cond_13

    .line 5295
    const/high16 v2, 0x41200000    # 10.0f

    add-float/2addr v0, v2

    goto/16 :goto_a

    .line 5313
    :cond_26
    const/high16 v2, -0x40800000    # -1.0f

    cmpg-float v2, v0, v2

    if-gtz v2, :cond_28

    .line 5315
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cs:Lsoftware/simplicial/nebulous/views/GameView$a;

    sget-object v2, Lsoftware/simplicial/nebulous/views/GameView$a;->c:Lsoftware/simplicial/nebulous/views/GameView$a;

    if-eq v0, v2, :cond_27

    .line 5317
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cr:F

    .line 5318
    sget-object v0, Lsoftware/simplicial/nebulous/views/GameView$a;->c:Lsoftware/simplicial/nebulous/views/GameView$a;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cs:Lsoftware/simplicial/nebulous/views/GameView$a;

    .line 5322
    :cond_27
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cF:Lsoftware/simplicial/a/bf;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/GameView;->cZ:Lsoftware/simplicial/a/ao;

    iget v2, v2, Lsoftware/simplicial/a/ao;->s:F

    iput v2, v0, Lsoftware/simplicial/a/bf;->s:F

    .line 5323
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cF:Lsoftware/simplicial/a/bf;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/GameView;->cZ:Lsoftware/simplicial/a/ao;

    iget v2, v2, Lsoftware/simplicial/a/ao;->t:F

    iput v2, v0, Lsoftware/simplicial/a/bf;->t:F

    .line 5324
    iput v1, p0, Lsoftware/simplicial/nebulous/views/GameView;->dj:F

    .line 5325
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cr:F

    float-to-double v0, v0

    iget-wide v2, p0, Lsoftware/simplicial/nebulous/views/GameView;->dm:D

    add-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cr:F

    .line 5326
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cr:F

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_15

    .line 5327
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cr:F

    goto/16 :goto_b

    .line 5329
    :cond_28
    const/4 v2, 0x0

    cmpg-float v0, v0, v2

    if-gtz v0, :cond_2a

    .line 5331
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cs:Lsoftware/simplicial/nebulous/views/GameView$a;

    sget-object v2, Lsoftware/simplicial/nebulous/views/GameView$a;->b:Lsoftware/simplicial/nebulous/views/GameView$a;

    if-eq v0, v2, :cond_29

    .line 5333
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cr:F

    .line 5334
    sget-object v0, Lsoftware/simplicial/nebulous/views/GameView$a;->b:Lsoftware/simplicial/nebulous/views/GameView$a;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cs:Lsoftware/simplicial/nebulous/views/GameView$a;

    .line 5338
    :cond_29
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cF:Lsoftware/simplicial/a/bf;

    iget v2, p0, Lsoftware/simplicial/nebulous/views/GameView;->cr:F

    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/GameView;->cF:Lsoftware/simplicial/a/bf;

    iget v3, v3, Lsoftware/simplicial/a/bf;->s:F

    iget-object v4, p0, Lsoftware/simplicial/nebulous/views/GameView;->cZ:Lsoftware/simplicial/a/ao;

    iget v4, v4, Lsoftware/simplicial/a/ao;->s:F

    invoke-static {v2, v3, v4}, Lb/a/a/d/a/a;->a(FFF)F

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/bf;->s:F

    .line 5339
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cF:Lsoftware/simplicial/a/bf;

    iget v2, p0, Lsoftware/simplicial/nebulous/views/GameView;->cr:F

    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/GameView;->cF:Lsoftware/simplicial/a/bf;

    iget v3, v3, Lsoftware/simplicial/a/bf;->t:F

    iget-object v4, p0, Lsoftware/simplicial/nebulous/views/GameView;->cZ:Lsoftware/simplicial/a/ao;

    iget v4, v4, Lsoftware/simplicial/a/ao;->t:F

    invoke-static {v2, v3, v4}, Lb/a/a/d/a/a;->a(FFF)F

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/bf;->t:F

    .line 5340
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cr:F

    iget v2, p0, Lsoftware/simplicial/nebulous/views/GameView;->dj:F

    invoke-static {v0, v2, v1}, Lb/a/a/d/a/a;->a(FFF)F

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->dj:F

    .line 5341
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cr:F

    float-to-double v0, v0

    iget-wide v2, p0, Lsoftware/simplicial/nebulous/views/GameView;->dm:D

    add-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cr:F

    .line 5342
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cr:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_15

    .line 5343
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cr:F

    goto/16 :goto_b

    .line 5347
    :cond_2a
    sget-object v0, Lsoftware/simplicial/nebulous/views/GameView$a;->a:Lsoftware/simplicial/nebulous/views/GameView$a;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cs:Lsoftware/simplicial/nebulous/views/GameView$a;

    .line 5348
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cr:F

    goto/16 :goto_b

    .line 5353
    :cond_2b
    sget-object v0, Lsoftware/simplicial/nebulous/views/GameView$a;->a:Lsoftware/simplicial/nebulous/views/GameView$a;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cs:Lsoftware/simplicial/nebulous/views/GameView$a;

    .line 5354
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cr:F

    goto/16 :goto_b

    .line 5360
    :cond_2c
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cG:F

    goto/16 :goto_c

    .line 5374
    :cond_2d
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto/16 :goto_d

    .line 5378
    :cond_2e
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->H:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 5379
    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_30

    .line 5381
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/views/c;

    .line 5382
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/GameView;->cc:Lsoftware/simplicial/a/u;

    iget-wide v2, v2, Lsoftware/simplicial/a/u;->aO:J

    iget-wide v4, v0, Lsoftware/simplicial/nebulous/views/c;->b:J

    sub-long/2addr v2, v4

    long-to-float v2, v2

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    .line 5383
    const v3, 0x3d99999a    # 0.075f

    cmpg-float v3, v2, v3

    if-gez v3, :cond_2f

    .line 5385
    iget v3, v0, Lsoftware/simplicial/nebulous/views/c;->e:F

    mul-float/2addr v3, v2

    iget v4, v0, Lsoftware/simplicial/nebulous/views/c;->c:F

    add-float/2addr v3, v4

    iput v3, v0, Lsoftware/simplicial/nebulous/views/c;->g:F

    .line 5386
    iget v3, v0, Lsoftware/simplicial/nebulous/views/c;->f:F

    mul-float/2addr v2, v3

    iget v3, v0, Lsoftware/simplicial/nebulous/views/c;->d:F

    add-float/2addr v2, v3

    iput v2, v0, Lsoftware/simplicial/nebulous/views/c;->h:F

    goto :goto_e

    .line 5390
    :cond_2f
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_e

    .line 5393
    :cond_30
    return-void
.end method

.method static synthetic c(Lsoftware/simplicial/nebulous/views/GameView;)[Ljava/nio/FloatBuffer;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->O:[Ljava/nio/FloatBuffer;

    return-object v0
.end method

.method static synthetic c(Lsoftware/simplicial/nebulous/views/GameView;[Ljava/nio/FloatBuffer;)[Ljava/nio/FloatBuffer;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->O:[Ljava/nio/FloatBuffer;

    return-object p1
.end method

.method static synthetic cA(Lsoftware/simplicial/nebulous/views/GameView;)F
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ab:F

    return v0
.end method

.method static synthetic cB(Lsoftware/simplicial/nebulous/views/GameView;)F
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ac:F

    return v0
.end method

.method static synthetic ca(Lsoftware/simplicial/nebulous/views/GameView;)Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cy:Z

    return v0
.end method

.method static synthetic cb(Lsoftware/simplicial/nebulous/views/GameView;)F
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cC:F

    return v0
.end method

.method static synthetic cc(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/f;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cx:Lsoftware/simplicial/nebulous/views/f;

    return-object v0
.end method

.method static synthetic cd(Lsoftware/simplicial/nebulous/views/GameView;)[I
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cq:[I

    return-object v0
.end method

.method static synthetic ce(Lsoftware/simplicial/nebulous/views/GameView;)Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cA:Z

    return v0
.end method

.method static synthetic cf(Lsoftware/simplicial/nebulous/views/GameView;)Ljava/nio/FloatBuffer;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aa:Ljava/nio/FloatBuffer;

    return-object v0
.end method

.method static synthetic cg(Lsoftware/simplicial/nebulous/views/GameView;)F
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cg:F

    return v0
.end method

.method static synthetic ch(Lsoftware/simplicial/nebulous/views/GameView;)F
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cf:F

    return v0
.end method

.method static synthetic ci(Lsoftware/simplicial/nebulous/views/GameView;)F
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ce:F

    return v0
.end method

.method static synthetic cj(Lsoftware/simplicial/nebulous/views/GameView;)F
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cd:F

    return v0
.end method

.method static synthetic ck(Lsoftware/simplicial/nebulous/views/GameView;)Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cR:Z

    return v0
.end method

.method static synthetic cl(Lsoftware/simplicial/nebulous/views/GameView;)Ljava/nio/FloatBuffer;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->L:Ljava/nio/FloatBuffer;

    return-object v0
.end method

.method static synthetic cm(Lsoftware/simplicial/nebulous/views/GameView;)Ljava/nio/FloatBuffer;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->J:Ljava/nio/FloatBuffer;

    return-object v0
.end method

.method static synthetic cn(Lsoftware/simplicial/nebulous/views/GameView;)Ljava/nio/FloatBuffer;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->K:Ljava/nio/FloatBuffer;

    return-object v0
.end method

.method static synthetic co(Lsoftware/simplicial/nebulous/views/GameView;)F
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ch:F

    return v0
.end method

.method static synthetic cp(Lsoftware/simplicial/nebulous/views/GameView;)Ljava/nio/FloatBuffer;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->P:Ljava/nio/FloatBuffer;

    return-object v0
.end method

.method static synthetic cq(Lsoftware/simplicial/nebulous/views/GameView;)Ljava/nio/FloatBuffer;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->Q:Ljava/nio/FloatBuffer;

    return-object v0
.end method

.method static synthetic cr(Lsoftware/simplicial/nebulous/views/GameView;)Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cP:Z

    return v0
.end method

.method static synthetic cs(Lsoftware/simplicial/nebulous/views/GameView;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cY:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic ct(Lsoftware/simplicial/nebulous/views/GameView;)Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cS:Z

    return v0
.end method

.method static synthetic cu(Lsoftware/simplicial/nebulous/views/GameView;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cW:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic cv(Lsoftware/simplicial/nebulous/views/GameView;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cX:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic cw(Lsoftware/simplicial/nebulous/views/GameView;)D
    .locals 2

    .prologue
    .line 115
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->dm:D

    return-wide v0
.end method

.method static synthetic cx(Lsoftware/simplicial/nebulous/views/GameView;)Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cQ:Z

    return v0
.end method

.method static synthetic cy(Lsoftware/simplicial/nebulous/views/GameView;)Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->dl:Z

    return v0
.end method

.method static synthetic cz(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bR:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic d(Lsoftware/simplicial/nebulous/views/GameView;F)F
    .locals 0

    .prologue
    .line 115
    iput p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->ag:F

    return p1
.end method

.method static synthetic d(Lsoftware/simplicial/nebulous/views/GameView;Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->Q:Ljava/nio/FloatBuffer;

    return-object p1
.end method

.method static synthetic d(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/a/a;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->al:Lsoftware/simplicial/nebulous/views/a/a;

    return-object v0
.end method

.method static synthetic e(Lsoftware/simplicial/nebulous/views/GameView;F)F
    .locals 0

    .prologue
    .line 115
    iput p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->ab:F

    return p1
.end method

.method static synthetic e(Lsoftware/simplicial/nebulous/views/GameView;Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->V:Ljava/nio/FloatBuffer;

    return-object p1
.end method

.method static synthetic e(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->au:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic f(Lsoftware/simplicial/nebulous/views/GameView;F)F
    .locals 0

    .prologue
    .line 115
    iput p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->ac:F

    return p1
.end method

.method static synthetic f(Lsoftware/simplicial/nebulous/views/GameView;Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->W:Ljava/nio/FloatBuffer;

    return-object p1
.end method

.method static synthetic f(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->av:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic g(Lsoftware/simplicial/nebulous/views/GameView;F)F
    .locals 0

    .prologue
    .line 115
    iput p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cd:F

    return p1
.end method

.method static synthetic g(Lsoftware/simplicial/nebulous/views/GameView;Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->aa:Ljava/nio/FloatBuffer;

    return-object p1
.end method

.method static synthetic g(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->aw:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic h(Lsoftware/simplicial/nebulous/views/GameView;F)F
    .locals 0

    .prologue
    .line 115
    iput p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->ce:F

    return p1
.end method

.method static synthetic h(Lsoftware/simplicial/nebulous/views/GameView;Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->L:Ljava/nio/FloatBuffer;

    return-object p1
.end method

.method static synthetic h(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->ax:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic i(Lsoftware/simplicial/nebulous/views/GameView;F)F
    .locals 0

    .prologue
    .line 115
    iput p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cf:F

    return p1
.end method

.method static synthetic i(Lsoftware/simplicial/nebulous/views/GameView;Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->R:Ljava/nio/FloatBuffer;

    return-object p1
.end method

.method static synthetic i(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bo:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic j(Lsoftware/simplicial/nebulous/views/GameView;F)F
    .locals 0

    .prologue
    .line 115
    iput p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cg:F

    return p1
.end method

.method static synthetic j(Lsoftware/simplicial/nebulous/views/GameView;Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->S:Ljava/nio/FloatBuffer;

    return-object p1
.end method

.method static synthetic j(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bp:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic k(Lsoftware/simplicial/nebulous/views/GameView;F)F
    .locals 0

    .prologue
    .line 115
    iput p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->ch:F

    return p1
.end method

.method static synthetic k(Lsoftware/simplicial/nebulous/views/GameView;Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->T:Ljava/nio/FloatBuffer;

    return-object p1
.end method

.method static synthetic k(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bq:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic l(Lsoftware/simplicial/nebulous/views/GameView;F)F
    .locals 0

    .prologue
    .line 115
    iput p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->cC:F

    return p1
.end method

.method static synthetic l(Lsoftware/simplicial/nebulous/views/GameView;Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->U:Ljava/nio/FloatBuffer;

    return-object p1
.end method

.method static synthetic l(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->br:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic m(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bs:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic n(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bt:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic o(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bu:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic p(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bv:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic q(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bw:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic r(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->by:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic s(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bx:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic t(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bz:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic u(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bA:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic v(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bB:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic w(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bC:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic x(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bD:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic y(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bE:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method

.method static synthetic z(Lsoftware/simplicial/nebulous/views/GameView;)Lsoftware/simplicial/nebulous/views/b/m;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->bF:Lsoftware/simplicial/nebulous/views/b/m;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 671
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->onPause()V

    .line 672
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    .prologue
    .line 664
    invoke-super {p0, p1, p2, p3, p4}, Landroid/opengl/GLSurfaceView;->onSizeChanged(IIII)V

    .line 666
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/GameView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/GameView;->cm:Landroid/util/DisplayMetrics;

    .line 667
    return-void
.end method

.method public setGameController(Lsoftware/simplicial/nebulous/c/d;)V
    .locals 0

    .prologue
    .line 658
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/GameView;->co:Lsoftware/simplicial/nebulous/c/d;

    .line 659
    return-void
.end method
