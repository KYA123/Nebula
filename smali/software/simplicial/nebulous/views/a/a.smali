.class public Lsoftware/simplicial/nebulous/views/a/a;
.super Lsoftware/simplicial/nebulous/views/a/d;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private h:F

.field private i:F

.field private j:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 62
    const-string v0, "attribute vec2 aPos;attribute vec2 aTex;varying vec2 vTex;uniform float uAspect;uniform vec4 uTranslateScale;uniform vec2 uAngleDepth;void main() {float xScaled = aPos.x * uTranslateScale.z;float yScaled = aPos.y * uTranslateScale.w;float xRot = xScaled * cos(uAngleDepth.x) - yScaled * sin(uAngleDepth.x);float yRot = yScaled * cos(uAngleDepth.x) + xScaled * sin(uAngleDepth.x);gl_Position.x = (xRot + uTranslateScale.x) * uAspect;gl_Position.y = yRot + uTranslateScale.y;gl_Position.z = uAngleDepth.y;gl_Position.w = 1.0;vTex.x = aTex.x;vTex.y = aTex.y;}"

    const-string v1, "precision mediump float;uniform vec4 uColor;varying lowp vec2 vTex;uniform sampler2D uTex;void main() {gl_FragColor = texture2D(uTex, vTex) * uColor;}"

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/nebulous/views/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->b:I

    const-string v1, "aPos"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->a:I

    .line 64
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->b:I

    const-string v1, "aTex"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->c:I

    .line 66
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->b:I

    const-string v1, "uAspect"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->d:I

    .line 67
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->b:I

    const-string v1, "uTranslateScale"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->e:I

    .line 68
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->b:I

    const-string v1, "uAngleDepth"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->f:I

    .line 69
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->b:I

    const-string v1, "uColor"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->g:I

    .line 71
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->b:I

    const-string v1, "uTex"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 72
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Lsoftware/simplicial/nebulous/views/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->b:I

    const-string v1, "aPos"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->a:I

    .line 78
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->b:I

    const-string v1, "aTex"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->c:I

    .line 80
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->b:I

    const-string v1, "uAspect"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->d:I

    .line 81
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->b:I

    const-string v1, "uTranslateScale"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->e:I

    .line 82
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->b:I

    const-string v1, "uAngleDepth"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->f:I

    .line 83
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->b:I

    const-string v1, "uColor"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->g:I

    .line 85
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->b:I

    const-string v1, "uTex"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 86
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 164
    invoke-super {p0}, Lsoftware/simplicial/nebulous/views/a/d;->a()V

    .line 166
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->a:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 167
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->c:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 168
    return-void
.end method

.method public a(FFFF)V
    .locals 5

    .prologue
    .line 102
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->e:I

    iget v1, p0, Lsoftware/simplicial/nebulous/views/a/a;->h:F

    sub-float v1, p1, v1

    iget v2, p0, Lsoftware/simplicial/nebulous/views/a/a;->i:F

    mul-float/2addr v1, v2

    neg-float v2, p2

    iget v3, p0, Lsoftware/simplicial/nebulous/views/a/a;->j:F

    add-float/2addr v2, v3

    iget v3, p0, Lsoftware/simplicial/nebulous/views/a/a;->i:F

    mul-float/2addr v2, v3

    iget v3, p0, Lsoftware/simplicial/nebulous/views/a/a;->i:F

    mul-float/2addr v3, p3

    iget v4, p0, Lsoftware/simplicial/nebulous/views/a/a;->i:F

    mul-float/2addr v4, p3

    invoke-static {v0, v1, v2, v3, v4}, Landroid/opengl/GLES20;->glUniform4f(IFFFF)V

    .line 103
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->f:I

    const/4 v1, 0x0

    invoke-static {v0, v1, p4}, Landroid/opengl/GLES20;->glUniform2f(IFF)V

    .line 104
    return-void
.end method

.method public a(FFFFF)V
    .locals 5

    .prologue
    .line 96
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->e:I

    iget v1, p0, Lsoftware/simplicial/nebulous/views/a/a;->h:F

    sub-float v1, p1, v1

    iget v2, p0, Lsoftware/simplicial/nebulous/views/a/a;->i:F

    mul-float/2addr v1, v2

    neg-float v2, p2

    iget v3, p0, Lsoftware/simplicial/nebulous/views/a/a;->j:F

    add-float/2addr v2, v3

    iget v3, p0, Lsoftware/simplicial/nebulous/views/a/a;->i:F

    mul-float/2addr v2, v3

    iget v3, p0, Lsoftware/simplicial/nebulous/views/a/a;->i:F

    mul-float/2addr v3, p3

    iget v4, p0, Lsoftware/simplicial/nebulous/views/a/a;->i:F

    mul-float/2addr v4, p3

    invoke-static {v0, v1, v2, v3, v4}, Landroid/opengl/GLES20;->glUniform4f(IFFFF)V

    .line 97
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->f:I

    invoke-static {v0, p4, p5}, Landroid/opengl/GLES20;->glUniform2f(IFF)V

    .line 98
    return-void
.end method

.method public a(FFFFFF)V
    .locals 5

    .prologue
    .line 90
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->e:I

    iget v1, p0, Lsoftware/simplicial/nebulous/views/a/a;->h:F

    sub-float v1, p1, v1

    iget v2, p0, Lsoftware/simplicial/nebulous/views/a/a;->i:F

    mul-float/2addr v1, v2

    neg-float v2, p2

    iget v3, p0, Lsoftware/simplicial/nebulous/views/a/a;->j:F

    add-float/2addr v2, v3

    iget v3, p0, Lsoftware/simplicial/nebulous/views/a/a;->i:F

    mul-float/2addr v2, v3

    iget v3, p0, Lsoftware/simplicial/nebulous/views/a/a;->i:F

    mul-float/2addr v3, p3

    iget v4, p0, Lsoftware/simplicial/nebulous/views/a/a;->i:F

    mul-float/2addr v4, p4

    invoke-static {v0, v1, v2, v3, v4}, Landroid/opengl/GLES20;->glUniform4f(IFFFF)V

    .line 91
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->f:I

    invoke-static {v0, p5, p6}, Landroid/opengl/GLES20;->glUniform2f(IFF)V

    .line 92
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 181
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 182
    return-void
.end method

.method public a(IF)V
    .locals 5

    .prologue
    const/high16 v4, 0x437f0000    # 255.0f

    .line 142
    and-int/lit16 v0, p1, 0xff

    int-to-float v0, v0

    div-float/2addr v0, v4

    .line 143
    shr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-float v1, v1

    div-float/2addr v1, v4

    .line 144
    shr-int/lit8 v2, p1, 0x10

    and-int/lit16 v2, v2, 0xff

    int-to-float v2, v2

    div-float/2addr v2, v4

    .line 145
    shr-int/lit8 v3, p1, 0x18

    and-int/lit16 v3, v3, 0xff

    int-to-float v3, v3

    div-float/2addr v3, v4

    mul-float/2addr v3, p2

    .line 147
    iget v4, p0, Lsoftware/simplicial/nebulous/views/a/a;->g:I

    invoke-static {v4, v0, v1, v2, v3}, Landroid/opengl/GLES20;->glUniform4f(IFFFF)V

    .line 148
    return-void
.end method

.method public a(Ljava/nio/FloatBuffer;)V
    .locals 6

    .prologue
    .line 206
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->c:I

    const/4 v1, 0x2

    const/16 v2, 0x1406

    const/4 v3, 0x0

    const/16 v4, 0x8

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 207
    return-void
.end method

.method public a(Ljava/nio/FloatBuffer;Ljava/nio/FloatBuffer;)V
    .locals 6

    .prologue
    const/16 v2, 0x1406

    const/16 v4, 0x8

    const/4 v1, 0x2

    const/4 v3, 0x0

    .line 157
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->a:I

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 158
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->c:I

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 159
    return-void
.end method

.method public a([F)V
    .locals 5

    .prologue
    .line 137
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->g:I

    const/4 v1, 0x0

    aget v1, p1, v1

    const/4 v2, 0x1

    aget v2, p1, v2

    const/4 v3, 0x2

    aget v3, p1, v3

    const/4 v4, 0x3

    aget v4, p1, v4

    invoke-static {v0, v1, v2, v3, v4}, Landroid/opengl/GLES20;->glUniform4f(IFFFF)V

    .line 138
    return-void
.end method

.method public a([FF)V
    .locals 4

    .prologue
    .line 152
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->g:I

    const/4 v1, 0x0

    aget v1, p1, v1

    const/4 v2, 0x1

    aget v2, p1, v2

    const/4 v3, 0x2

    aget v3, p1, v3

    invoke-static {v0, v1, v2, v3, p2}, Landroid/opengl/GLES20;->glUniform4f(IFFFF)V

    .line 153
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 173
    invoke-super {p0}, Lsoftware/simplicial/nebulous/views/a/d;->b()V

    .line 175
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->a:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    .line 176
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->c:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    .line 177
    return-void
.end method

.method public b(FFFF)V
    .locals 1

    .prologue
    .line 124
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->d:I

    invoke-static {v0, p1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 125
    iput p2, p0, Lsoftware/simplicial/nebulous/views/a/a;->h:F

    .line 126
    iput p3, p0, Lsoftware/simplicial/nebulous/views/a/a;->j:F

    .line 127
    iput p4, p0, Lsoftware/simplicial/nebulous/views/a/a;->i:F

    .line 128
    return-void
.end method

.method public b(FFFFF)V
    .locals 5

    .prologue
    .line 108
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->e:I

    iget v1, p0, Lsoftware/simplicial/nebulous/views/a/a;->h:F

    sub-float v1, p1, v1

    iget v2, p0, Lsoftware/simplicial/nebulous/views/a/a;->i:F

    mul-float/2addr v1, v2

    neg-float v2, p2

    iget v3, p0, Lsoftware/simplicial/nebulous/views/a/a;->j:F

    add-float/2addr v2, v3

    iget v3, p0, Lsoftware/simplicial/nebulous/views/a/a;->i:F

    mul-float/2addr v2, v3

    iget v3, p0, Lsoftware/simplicial/nebulous/views/a/a;->i:F

    mul-float/2addr v3, p3

    iget v4, p0, Lsoftware/simplicial/nebulous/views/a/a;->i:F

    mul-float/2addr v4, p4

    invoke-static {v0, v1, v2, v3, v4}, Landroid/opengl/GLES20;->glUniform4f(IFFFF)V

    .line 109
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/a;->f:I

    const/4 v1, 0x0

    invoke-static {v0, v1, p5}, Landroid/opengl/GLES20;->glUniform2f(IFF)V

    .line 110
    return-void
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 186
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 187
    return-void
.end method

.method public c(I)V
    .locals 2

    .prologue
    .line 191
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 192
    return-void
.end method

.method public d(I)V
    .locals 2

    .prologue
    .line 196
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 197
    return-void
.end method

.method public e(I)V
    .locals 2

    .prologue
    .line 201
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 202
    return-void
.end method

.method public f(I)V
    .locals 5

    .prologue
    const/high16 v4, 0x437f0000    # 255.0f

    .line 211
    shr-int/lit8 v0, p1, 0x0

    and-int/lit16 v0, v0, 0xff

    int-to-float v0, v0

    div-float/2addr v0, v4

    .line 212
    shr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-float v1, v1

    div-float/2addr v1, v4

    .line 213
    shr-int/lit8 v2, p1, 0x10

    and-int/lit16 v2, v2, 0xff

    int-to-float v2, v2

    div-float/2addr v2, v4

    .line 214
    shr-int/lit8 v3, p1, 0x18

    and-int/lit16 v3, v3, 0xff

    int-to-float v3, v3

    div-float/2addr v3, v4

    .line 216
    iget v4, p0, Lsoftware/simplicial/nebulous/views/a/a;->g:I

    invoke-static {v4, v0, v1, v2, v3}, Landroid/opengl/GLES20;->glUniform4f(IFFFF)V

    .line 217
    return-void
.end method
