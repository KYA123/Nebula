.class public Lsoftware/simplicial/nebulous/views/a/c;
.super Lsoftware/simplicial/nebulous/views/a/d;
.source "SourceFile"


# instance fields
.field public final a:[I

.field private final c:I

.field private final d:I

.field private final e:I

.field private f:F

.field private g:F

.field private h:F

.field private i:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 59
    const-string v0, "attribute vec2 aPos;uniform vec3 uTranslateScale;uniform float uAspect;void main() {float xScaled = aPos.x * uTranslateScale.z;float yScaled = aPos.y * uTranslateScale.z;gl_Position.x = (xScaled + uTranslateScale.x) * uAspect;gl_Position.y = yScaled + uTranslateScale.y;gl_Position.z = 0.95;gl_Position.w = 1.0;}"

    const-string v1, "precision mediump float;void main() {gl_FragColor = vec4(0.5,0.5,0.5,1.0);}"

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/nebulous/views/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/a/c;->a:[I

    .line 55
    const/16 v0, 0x15

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/c;->i:I

    .line 61
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/c;->b:I

    const-string v1, "aPos"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/c;->c:I

    .line 62
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/c;->b:I

    const-string v1, "uTranslateScale"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/c;->d:I

    .line 63
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/c;->b:I

    const-string v1, "uAspect"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/c;->e:I

    .line 64
    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 82
    invoke-super {p0}, Lsoftware/simplicial/nebulous/views/a/d;->a()V

    .line 84
    const v0, 0x8892

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/a/c;->a:[I

    aget v1, v1, v3

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 85
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/c;->c:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 86
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/c;->c:I

    const/4 v1, 0x2

    const/16 v2, 0x1406

    const/16 v4, 0x8

    move v5, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    .line 87
    return-void
.end method

.method public a(F)V
    .locals 3

    .prologue
    .line 141
    invoke-static {p1}, Landroid/opengl/GLES20;->glLineWidth(F)V

    .line 142
    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p0, Lsoftware/simplicial/nebulous/views/a/c;->i:I

    mul-int/lit8 v2, v2, 0x2

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 143
    return-void
.end method

.method public a(FFF)V
    .locals 4

    .prologue
    .line 68
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/c;->d:I

    iget v1, p0, Lsoftware/simplicial/nebulous/views/a/c;->f:F

    sub-float v1, p1, v1

    iget v2, p0, Lsoftware/simplicial/nebulous/views/a/c;->g:F

    mul-float/2addr v1, v2

    neg-float v2, p2

    iget v3, p0, Lsoftware/simplicial/nebulous/views/a/c;->h:F

    add-float/2addr v2, v3

    iget v3, p0, Lsoftware/simplicial/nebulous/views/a/c;->g:F

    mul-float/2addr v2, v3

    iget v3, p0, Lsoftware/simplicial/nebulous/views/a/c;->g:F

    mul-float/2addr v3, p3

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/GLES20;->glUniform3f(IFFF)V

    .line 69
    return-void
.end method

.method public a(FFFF)V
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/c;->e:I

    invoke-static {v0, p1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 74
    iput p2, p0, Lsoftware/simplicial/nebulous/views/a/c;->f:F

    .line 75
    iput p3, p0, Lsoftware/simplicial/nebulous/views/a/c;->h:F

    .line 76
    iput p4, p0, Lsoftware/simplicial/nebulous/views/a/c;->g:F

    .line 77
    return-void
.end method

.method public a(Lsoftware/simplicial/a/ap;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/high16 v8, -0x40800000    # -1.0f

    const v7, 0x8892

    const/4 v1, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    .line 99
    sget-object v0, Lsoftware/simplicial/nebulous/views/a/c$1;->a:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 114
    :goto_0
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/c;->i:I

    mul-int/lit8 v0, v0, 0x4

    mul-int/lit8 v0, v0, 0x2

    new-array v2, v0, [F

    move v0, v1

    .line 115
    :goto_1
    iget v3, p0, Lsoftware/simplicial/nebulous/views/a/c;->i:I

    if-ge v0, v3, :cond_0

    .line 117
    mul-int/lit8 v3, v0, 0x4

    add-int/lit8 v3, v3, 0x0

    mul-int/lit8 v4, v0, 0x2

    int-to-float v4, v4

    iget v5, p0, Lsoftware/simplicial/nebulous/views/a/c;->i:I

    add-int/lit8 v5, v5, -0x1

    int-to-float v5, v5

    div-float/2addr v4, v5

    sub-float/2addr v4, v6

    aput v4, v2, v3

    .line 118
    mul-int/lit8 v3, v0, 0x4

    add-int/lit8 v3, v3, 0x1

    aput v8, v2, v3

    .line 120
    mul-int/lit8 v3, v0, 0x4

    add-int/lit8 v3, v3, 0x2

    mul-int/lit8 v4, v0, 0x2

    int-to-float v4, v4

    iget v5, p0, Lsoftware/simplicial/nebulous/views/a/c;->i:I

    add-int/lit8 v5, v5, -0x1

    int-to-float v5, v5

    div-float/2addr v4, v5

    sub-float/2addr v4, v6

    aput v4, v2, v3

    .line 121
    mul-int/lit8 v3, v0, 0x4

    add-int/lit8 v3, v3, 0x3

    aput v6, v2, v3

    .line 115
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 102
    :pswitch_0
    const/16 v0, 0xa

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/c;->i:I

    goto :goto_0

    .line 105
    :pswitch_1
    const/16 v0, 0xe

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/c;->i:I

    goto :goto_0

    .line 108
    :pswitch_2
    const/16 v0, 0x15

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/c;->i:I

    goto :goto_0

    .line 111
    :pswitch_3
    const/16 v0, 0x1d

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/c;->i:I

    goto :goto_0

    :cond_0
    move v0, v1

    .line 123
    :goto_2
    iget v3, p0, Lsoftware/simplicial/nebulous/views/a/c;->i:I

    if-ge v0, v3, :cond_1

    .line 125
    iget v3, p0, Lsoftware/simplicial/nebulous/views/a/c;->i:I

    add-int/2addr v3, v0

    mul-int/lit8 v3, v3, 0x4

    add-int/lit8 v3, v3, 0x0

    aput v8, v2, v3

    .line 126
    iget v3, p0, Lsoftware/simplicial/nebulous/views/a/c;->i:I

    add-int/2addr v3, v0

    mul-int/lit8 v3, v3, 0x4

    add-int/lit8 v3, v3, 0x1

    mul-int/lit8 v4, v0, 0x2

    int-to-float v4, v4

    iget v5, p0, Lsoftware/simplicial/nebulous/views/a/c;->i:I

    add-int/lit8 v5, v5, -0x1

    int-to-float v5, v5

    div-float/2addr v4, v5

    sub-float/2addr v4, v6

    aput v4, v2, v3

    .line 128
    iget v3, p0, Lsoftware/simplicial/nebulous/views/a/c;->i:I

    add-int/2addr v3, v0

    mul-int/lit8 v3, v3, 0x4

    add-int/lit8 v3, v3, 0x2

    aput v6, v2, v3

    .line 129
    iget v3, p0, Lsoftware/simplicial/nebulous/views/a/c;->i:I

    add-int/2addr v3, v0

    mul-int/lit8 v3, v3, 0x4

    add-int/lit8 v3, v3, 0x3

    mul-int/lit8 v4, v0, 0x2

    int-to-float v4, v4

    iget v5, p0, Lsoftware/simplicial/nebulous/views/a/c;->i:I

    add-int/lit8 v5, v5, -0x1

    int-to-float v5, v5

    div-float/2addr v4, v5

    sub-float/2addr v4, v6

    aput v4, v2, v3

    .line 123
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 131
    :cond_1
    invoke-static {v2}, Lsoftware/simplicial/nebulous/f/aa;->a([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    .line 132
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/a/c;->a:[I

    invoke-static {v9, v2, v1}, Landroid/opengl/GLES20;->glDeleteBuffers(I[II)V

    .line 133
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/a/c;->a:[I

    invoke-static {v9, v2, v1}, Landroid/opengl/GLES20;->glGenBuffers(I[II)V

    .line 134
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/a/c;->a:[I

    aget v2, v2, v1

    invoke-static {v7, v2}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 135
    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->capacity()I

    move-result v2

    mul-int/lit8 v2, v2, 0x4

    const v3, 0x88e4

    invoke-static {v7, v2, v0, v3}, Landroid/opengl/GLES20;->glBufferData(IILjava/nio/Buffer;I)V

    .line 136
    invoke-static {v7, v1}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 137
    return-void

    .line 99
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public b()V
    .locals 1

    .prologue
    .line 92
    invoke-super {p0}, Lsoftware/simplicial/nebulous/views/a/d;->b()V

    .line 94
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/c;->c:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    .line 95
    return-void
.end method
