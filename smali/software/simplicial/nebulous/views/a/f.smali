.class public Lsoftware/simplicial/nebulous/views/a/f;
.super Lsoftware/simplicial/nebulous/views/a/d;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final c:I

.field private final d:I

.field private final e:[I

.field private f:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 59
    const-string v0, "attribute vec3 aPos;uniform vec3 uTranslateScale;uniform vec4 uAspectDensityPointMinMax;void main() {float starScaleX = uAspectDensityPointMinMax.x * uTranslateScale.z / aPos.z;float starScaleY = uTranslateScale.z / aPos.z;float xScaled = aPos.x * starScaleX - (uTranslateScale.x * starScaleX / uTranslateScale.z);float yScaled = aPos.y * starScaleY - (uTranslateScale.y * starScaleY / uTranslateScale.z);gl_Position.x = xScaled;gl_Position.y = yScaled;gl_Position.z = 0.95;gl_Position.w = 1.0;gl_PointSize = clamp((4.1 - aPos.z) * uAspectDensityPointMinMax.y, uAspectDensityPointMinMax.z, uAspectDensityPointMinMax.w);}"

    const-string v1, "precision mediump float;void main() {gl_FragColor = vec4(0.8,0.8,0.8,1.0);}"

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/nebulous/views/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/a/f;->e:[I

    .line 55
    const/16 v0, 0x12c

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/f;->f:I

    .line 61
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/f;->b:I

    const-string v1, "aPos"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/f;->a:I

    .line 62
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/f;->b:I

    const-string v1, "uTranslateScale"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/f;->c:I

    .line 63
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/f;->b:I

    const-string v1, "uAspectDensityPointMinMax"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/f;->d:I

    .line 64
    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 79
    invoke-super {p0}, Lsoftware/simplicial/nebulous/views/a/d;->a()V

    .line 81
    const v0, 0x8892

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/a/f;->e:[I

    aget v1, v1, v3

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 82
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/f;->a:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 83
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/f;->a:I

    const/4 v1, 0x3

    const/16 v2, 0x1406

    const/16 v4, 0xc

    move v5, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    .line 84
    return-void
.end method

.method public a(FFF)V
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/f;->c:I

    invoke-static {v0, p1, p2, p3}, Landroid/opengl/GLES20;->glUniform3f(IFFF)V

    .line 69
    return-void
.end method

.method public a(FFFF)V
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/f;->d:I

    invoke-static {v0, p1, p2, p3, p4}, Landroid/opengl/GLES20;->glUniform4f(IFFFF)V

    .line 74
    return-void
.end method

.method public a(Lsoftware/simplicial/a/ap;Ljava/util/Random;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/high16 v8, 0x3f800000    # 1.0f

    const v7, 0x8892

    const/high16 v6, 0x40000000    # 2.0f

    const/4 v1, 0x0

    .line 97
    sget-object v0, Lsoftware/simplicial/nebulous/views/a/f$1;->a:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 112
    :goto_0
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/f;->f:I

    mul-int/lit8 v0, v0, 0x3

    new-array v2, v0, [F

    move v0, v1

    .line 113
    :goto_1
    iget v3, p0, Lsoftware/simplicial/nebulous/views/a/f;->f:I

    if-ge v0, v3, :cond_0

    .line 115
    mul-int/lit8 v3, v0, 0x3

    add-int/lit8 v3, v3, 0x0

    invoke-virtual {p2}, Ljava/util/Random;->nextFloat()F

    move-result v4

    mul-float/2addr v4, v6

    sub-float/2addr v4, v8

    aput v4, v2, v3

    .line 116
    mul-int/lit8 v3, v0, 0x3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p2}, Ljava/util/Random;->nextFloat()F

    move-result v4

    mul-float/2addr v4, v6

    sub-float/2addr v4, v8

    aput v4, v2, v3

    .line 117
    mul-int/lit8 v3, v0, 0x3

    add-int/lit8 v3, v3, 0x2

    invoke-virtual {p2}, Ljava/util/Random;->nextFloat()F

    move-result v4

    mul-float/2addr v4, v6

    const v5, 0x40066666    # 2.1f

    add-float/2addr v4, v5

    aput v4, v2, v3

    .line 113
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 100
    :pswitch_0
    const/16 v0, 0x96

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/f;->f:I

    goto :goto_0

    .line 103
    :pswitch_1
    const/16 v0, 0xd4

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/f;->f:I

    goto :goto_0

    .line 106
    :pswitch_2
    const/16 v0, 0x12c

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/f;->f:I

    goto :goto_0

    .line 109
    :pswitch_3
    const/16 v0, 0x1a8

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/f;->f:I

    goto :goto_0

    .line 119
    :cond_0
    invoke-static {v2}, Lsoftware/simplicial/nebulous/f/aa;->a([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    .line 120
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/a/f;->e:[I

    invoke-static {v9, v2, v1}, Landroid/opengl/GLES20;->glDeleteBuffers(I[II)V

    .line 121
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/a/f;->e:[I

    invoke-static {v9, v2, v1}, Landroid/opengl/GLES20;->glGenBuffers(I[II)V

    .line 122
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/a/f;->e:[I

    aget v2, v2, v1

    invoke-static {v7, v2}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 123
    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->capacity()I

    move-result v2

    mul-int/lit8 v2, v2, 0x4

    const v3, 0x88e4

    invoke-static {v7, v2, v0, v3}, Landroid/opengl/GLES20;->glBufferData(IILjava/nio/Buffer;I)V

    .line 124
    invoke-static {v7, v1}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 125
    return-void

    .line 97
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public b()V
    .locals 2

    .prologue
    .line 89
    invoke-super {p0}, Lsoftware/simplicial/nebulous/views/a/d;->b()V

    .line 91
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/f;->a:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    .line 92
    const v0, 0x8892

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 93
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 129
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/f;->f:I

    mul-int/lit8 v0, v0, 0x1

    invoke-static {v1, v1, v0}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 130
    return-void
.end method
