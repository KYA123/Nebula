.class public Lsoftware/simplicial/nebulous/views/b/a;
.super Lsoftware/simplicial/nebulous/views/b/o;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/nebulous/views/b/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lsoftware/simplicial/nebulous/views/b/o",
        "<",
        "Lsoftware/simplicial/nebulous/views/b/a$a;",
        ">;"
    }
.end annotation


# instance fields
.field private b:Lsoftware/simplicial/nebulous/views/b/a$a;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 6

    .prologue
    const/4 v4, -0x1

    .line 38
    const v0, 0x41e66666    # 28.8f

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-direct {p0, p1, v0, v4, v1}, Lsoftware/simplicial/nebulous/views/b/o;-><init>(Landroid/content/res/Resources;FILandroid/graphics/Paint$Align;)V

    .line 34
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/a$a;

    sget-object v2, Lsoftware/simplicial/a/h/a$a;->a:Lsoftware/simplicial/a/h/a$a;

    const/4 v3, 0x0

    move-object v1, p0

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lsoftware/simplicial/nebulous/views/b/a$a;-><init>(Lsoftware/simplicial/nebulous/views/b/a;Lsoftware/simplicial/a/h/a$a;III)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/a;->b:Lsoftware/simplicial/nebulous/views/b/a$a;

    .line 39
    return-void
.end method


# virtual methods
.method bridge synthetic a(Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 16
    check-cast p1, Lsoftware/simplicial/nebulous/views/b/a$a;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/views/b/a;->a(Lsoftware/simplicial/nebulous/views/b/a$a;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method a(Lsoftware/simplicial/nebulous/views/b/a$a;)Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    const/16 v5, 0xff

    const/4 v4, 0x0

    .line 53
    if-nez p1, :cond_0

    .line 54
    const/4 v0, 0x0

    .line 83
    :goto_0
    return-object v0

    .line 56
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/a;->g:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 58
    const/16 v0, 0x100

    const/16 v1, 0x20

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/a;->g:Landroid/graphics/Bitmap;

    .line 59
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/a;->g:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/a;->f:Landroid/graphics/Canvas;

    .line 62
    :cond_1
    const-string v1, "NULL"

    .line 63
    iget v0, p1, Lsoftware/simplicial/nebulous/views/b/a$a;->b:I

    .line 64
    sget-object v2, Lsoftware/simplicial/nebulous/views/b/a$1;->a:[I

    iget-object v3, p1, Lsoftware/simplicial/nebulous/views/b/a$a;->a:Lsoftware/simplicial/a/h/a$a;

    invoke-virtual {v3}, Lsoftware/simplicial/a/h/a$a;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 79
    :goto_1
    :pswitch_0
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/a;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v4}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 80
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/a;->e:Landroid/text/TextPaint;

    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 81
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/a;->f:Landroid/graphics/Canvas;

    const/4 v2, 0x0

    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/b/a;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    const v4, 0x3f4ccccd    # 0.8f

    mul-float/2addr v3, v4

    iget-object v4, p0, Lsoftware/simplicial/nebulous/views/b/a;->e:Landroid/text/TextPaint;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 83
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/a;->g:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 67
    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/a;->c:Landroid/content/res/Resources;

    const v2, 0x7f08012d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lsoftware/simplicial/nebulous/views/b/a$a;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lsoftware/simplicial/nebulous/views/b/a$a;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 68
    invoke-static {v4, v5, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto :goto_1

    .line 71
    :pswitch_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/a;->c:Landroid/content/res/Resources;

    const v2, 0x7f080259

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lsoftware/simplicial/nebulous/views/b/a$a;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lsoftware/simplicial/nebulous/views/b/a$a;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 72
    invoke-static {v5, v5, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto/16 :goto_1

    .line 64
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lsoftware/simplicial/a/h/a$a;III)V
    .locals 6

    .prologue
    .line 43
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/a;->b:Lsoftware/simplicial/nebulous/views/b/a$a;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/views/b/a$a;->a:Lsoftware/simplicial/a/h/a$a;

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/a;->b:Lsoftware/simplicial/nebulous/views/b/a$a;

    iget v0, v0, Lsoftware/simplicial/nebulous/views/b/a$a;->c:I

    if-ne v0, p3, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/a;->b:Lsoftware/simplicial/nebulous/views/b/a$a;

    iget v0, v0, Lsoftware/simplicial/nebulous/views/b/a$a;->d:I

    if-ne v0, p4, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/a;->b:Lsoftware/simplicial/nebulous/views/b/a$a;

    iget v0, v0, Lsoftware/simplicial/nebulous/views/b/a$a;->b:I

    if-ne v0, p2, :cond_0

    .line 48
    :goto_0
    return-void

    .line 46
    :cond_0
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/a$a;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lsoftware/simplicial/nebulous/views/b/a$a;-><init>(Lsoftware/simplicial/nebulous/views/b/a;Lsoftware/simplicial/a/h/a$a;III)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/a;->b:Lsoftware/simplicial/nebulous/views/b/a$a;

    .line 47
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/a;->b:Lsoftware/simplicial/nebulous/views/b/a$a;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lsoftware/simplicial/nebulous/views/b/a;->a(Ljava/lang/Object;Z)V

    goto :goto_0
.end method
