.class public abstract Lsoftware/simplicial/nebulous/views/b/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<RequestData:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final b:Ljava/util/concurrent/Executor;

.field private static c:I

.field private static d:[I


# instance fields
.field protected a:Z

.field private final e:Ljava/util/concurrent/Executor;

.field private f:Z

.field private g:Landroid/graphics/Bitmap;

.field private h:Z

.field private volatile i:Z

.field private j:[I

.field private k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/views/b/b;->b:Ljava/util/concurrent/Executor;

    .line 19
    const/4 v0, -0x1

    sput v0, Lsoftware/simplicial/nebulous/views/b/b;->c:I

    .line 21
    const/4 v0, 0x0

    new-array v0, v0, [I

    sput-object v0, Lsoftware/simplicial/nebulous/views/b/b;->d:[I

    return-void
.end method

.method constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/views/b/b;->f:Z

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/b;->g:Landroid/graphics/Bitmap;

    .line 26
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/views/b/b;->h:Z

    .line 27
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/views/b/b;->i:Z

    .line 28
    new-array v0, v1, [I

    const/4 v1, -0x1

    aput v1, v0, v2

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/b;->j:[I

    .line 29
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/views/b/b;->k:Z

    .line 30
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/views/b/b;->a:Z

    .line 34
    sget-object v0, Lsoftware/simplicial/nebulous/views/b/b;->b:Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/b;->e:Ljava/util/concurrent/Executor;

    .line 35
    return-void
.end method

.method constructor <init>(Ljava/util/concurrent/Executor;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/views/b/b;->f:Z

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/b;->g:Landroid/graphics/Bitmap;

    .line 26
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/views/b/b;->h:Z

    .line 27
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/views/b/b;->i:Z

    .line 28
    new-array v0, v1, [I

    const/4 v1, -0x1

    aput v1, v0, v2

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/b;->j:[I

    .line 29
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/views/b/b;->k:Z

    .line 30
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/views/b/b;->a:Z

    .line 39
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/b/b;->e:Ljava/util/concurrent/Executor;

    .line 40
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/views/b/b;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 14
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/b/b;->g:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/views/b/b;Z)Z
    .locals 0

    .prologue
    .line 14
    iput-boolean p1, p0, Lsoftware/simplicial/nebulous/views/b/b;->i:Z

    return p1
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/views/b/b;Z)Z
    .locals 0

    .prologue
    .line 14
    iput-boolean p1, p0, Lsoftware/simplicial/nebulous/views/b/b;->h:Z

    return p1
.end method

.method private c()V
    .locals 6

    .prologue
    const v5, 0x812f

    const/16 v4, 0x2601

    const/4 v1, 0x1

    const/16 v3, 0xde1

    const/4 v2, 0x0

    .line 98
    new-array v0, v1, [I

    sput-object v0, Lsoftware/simplicial/nebulous/views/b/b;->d:[I

    .line 99
    sget-object v0, Lsoftware/simplicial/nebulous/views/b/b;->d:[I

    invoke-static {v1, v0, v2}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 100
    sget-object v0, Lsoftware/simplicial/nebulous/views/b/b;->d:[I

    aget v0, v0, v2

    invoke-static {v3, v0}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 101
    const/16 v0, 0x2801

    invoke-static {v3, v0, v4}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 102
    const/16 v0, 0x2800

    invoke-static {v3, v0, v4}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 103
    const/16 v0, 0x2802

    invoke-static {v3, v0, v5}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 104
    const/16 v0, 0x2803

    invoke-static {v3, v0, v5}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 105
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v1, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 106
    invoke-static {v2, v2, v2, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 107
    invoke-static {v3, v2, v0, v2}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 108
    invoke-static {v3, v2}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 109
    return-void
.end method


# virtual methods
.method abstract a(Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRequestData;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation
.end method

.method protected a(Ljava/lang/Object;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRequestData;Z)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 116
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/views/b/b;->f:Z

    .line 117
    if-nez p2, :cond_0

    .line 119
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/views/b/b;->i:Z

    .line 120
    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/views/b/b;->a(Ljava/lang/Object;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/b;->g:Landroid/graphics/Bitmap;

    .line 121
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/views/b/b;->i:Z

    .line 122
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/views/b/b;->h:Z

    .line 138
    :goto_0
    return-void

    .line 126
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/b;->e:Ljava/util/concurrent/Executor;

    new-instance v1, Lsoftware/simplicial/nebulous/views/b/b$1;

    invoke-direct {v1, p0, p1}, Lsoftware/simplicial/nebulous/views/b/b$1;-><init>(Lsoftware/simplicial/nebulous/views/b/b;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/views/b/b;->a:Z

    .line 150
    if-eqz p1, :cond_0

    .line 151
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/views/b/b;->h:Z

    .line 152
    :cond_0
    return-void
.end method

.method public a()Z
    .locals 7

    .prologue
    const v6, 0x812f

    const/16 v5, 0x2601

    const/4 v1, 0x1

    const/16 v4, 0xde1

    const/4 v0, 0x0

    .line 44
    sget-object v2, Lsoftware/simplicial/nebulous/views/b/b;->d:[I

    array-length v2, v2

    if-nez v2, :cond_0

    .line 45
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/views/b/b;->c()V

    .line 46
    :cond_0
    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/views/b/b;->f:Z

    if-eqz v2, :cond_1

    .line 47
    const/4 v2, 0x0

    invoke-virtual {p0, v2, v1}, Lsoftware/simplicial/nebulous/views/b/b;->a(Ljava/lang/Object;Z)V

    .line 49
    :cond_1
    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/views/b/b;->h:Z

    if-eqz v2, :cond_4

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/views/b/b;->i:Z

    if-nez v2, :cond_4

    .line 51
    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/views/b/b;->h:Z

    .line 53
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/b;->g:Landroid/graphics/Bitmap;

    .line 54
    if-nez v2, :cond_2

    .line 56
    sget-object v1, Lsoftware/simplicial/nebulous/views/b/b;->d:[I

    aget v1, v1, v0

    invoke-static {v4, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 57
    sget-object v1, Lsoftware/simplicial/nebulous/views/b/b;->d:[I

    aget v1, v1, v0

    sput v1, Lsoftware/simplicial/nebulous/views/b/b;->c:I

    .line 93
    :goto_0
    return v0

    .line 61
    :cond_2
    iget-boolean v3, p0, Lsoftware/simplicial/nebulous/views/b/b;->k:Z

    if-nez v3, :cond_3

    .line 63
    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/b/b;->j:[I

    invoke-static {v1, v3, v0}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 64
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/views/b/b;->k:Z

    .line 65
    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/b/b;->j:[I

    aget v3, v3, v0

    invoke-static {v4, v3}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 66
    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/b/b;->j:[I

    aget v3, v3, v0

    sput v3, Lsoftware/simplicial/nebulous/views/b/b;->c:I

    .line 67
    const/16 v3, 0x2801

    invoke-static {v4, v3, v5}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 68
    const/16 v3, 0x2800

    invoke-static {v4, v3, v5}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 69
    const/16 v3, 0x2802

    invoke-static {v4, v3, v6}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 70
    const/16 v3, 0x2803

    invoke-static {v4, v3, v6}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 77
    :goto_1
    invoke-static {v4, v0, v2, v0}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 78
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/views/b/b;->a:Z

    move v0, v1

    .line 79
    goto :goto_0

    .line 74
    :cond_3
    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/b/b;->j:[I

    aget v3, v3, v0

    invoke-static {v4, v3}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 75
    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/b/b;->j:[I

    aget v3, v3, v0

    sput v3, Lsoftware/simplicial/nebulous/views/b/b;->c:I

    goto :goto_1

    .line 82
    :cond_4
    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/views/b/b;->k:Z

    if-eqz v2, :cond_6

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/views/b/b;->a:Z

    if-eqz v2, :cond_6

    .line 84
    sget v2, Lsoftware/simplicial/nebulous/views/b/b;->c:I

    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/b/b;->j:[I

    aget v3, v3, v0

    if-ne v2, v3, :cond_5

    move v0, v1

    .line 85
    goto :goto_0

    .line 87
    :cond_5
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/b;->j:[I

    aget v2, v2, v0

    invoke-static {v4, v2}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 88
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/b;->j:[I

    aget v0, v2, v0

    sput v0, Lsoftware/simplicial/nebulous/views/b/b;->c:I

    move v0, v1

    .line 89
    goto :goto_0

    .line 91
    :cond_6
    sget-object v1, Lsoftware/simplicial/nebulous/views/b/b;->d:[I

    aget v1, v1, v0

    invoke-static {v4, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 92
    sget-object v1, Lsoftware/simplicial/nebulous/views/b/b;->d:[I

    aget v1, v1, v0

    sput v1, Lsoftware/simplicial/nebulous/views/b/b;->c:I

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 142
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/b;->j:[I

    invoke-static {v2, v0, v1}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 143
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/views/b/b;->k:Z

    .line 144
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/views/b/b;->h:Z

    .line 145
    return-void
.end method
