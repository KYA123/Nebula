.class public Lsoftware/simplicial/nebulous/views/b/d;
.super Lsoftware/simplicial/nebulous/views/b/o;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/nebulous/views/b/d$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lsoftware/simplicial/nebulous/views/b/o",
        "<",
        "Lsoftware/simplicial/nebulous/views/b/d$a;",
        ">;"
    }
.end annotation


# instance fields
.field private b:Lsoftware/simplicial/nebulous/views/b/d$a;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 36
    const v0, 0x41e66666    # 28.8f

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-direct {p0, p1, v0, v3, v1}, Lsoftware/simplicial/nebulous/views/b/o;-><init>(Landroid/content/res/Resources;FILandroid/graphics/Paint$Align;)V

    .line 32
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/d$a;

    sget-object v1, Lsoftware/simplicial/a/aa;->a:Lsoftware/simplicial/a/aa;

    sget-object v2, Lsoftware/simplicial/a/am;->a:Lsoftware/simplicial/a/am;

    invoke-direct {v0, p0, v1, v2, v3}, Lsoftware/simplicial/nebulous/views/b/d$a;-><init>(Lsoftware/simplicial/nebulous/views/b/d;Lsoftware/simplicial/a/aa;Lsoftware/simplicial/a/am;I)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/d;->b:Lsoftware/simplicial/nebulous/views/b/d$a;

    .line 37
    return-void
.end method


# virtual methods
.method bridge synthetic a(Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 16
    check-cast p1, Lsoftware/simplicial/nebulous/views/b/d$a;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/views/b/d;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method a(Lsoftware/simplicial/nebulous/views/b/d$a;)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    .line 51
    if-nez p1, :cond_0

    .line 52
    const/4 v0, 0x0

    .line 130
    :goto_0
    return-object v0

    .line 54
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/d;->g:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 56
    const/16 v0, 0x200

    const/16 v1, 0x20

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/d;->g:Landroid/graphics/Bitmap;

    .line 57
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/d;->g:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/d;->f:Landroid/graphics/Canvas;

    .line 61
    :cond_1
    const v1, -0xdd3301

    .line 62
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/aa;->b:Lsoftware/simplicial/a/aa;

    if-ne v0, v2, :cond_2

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->c(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/am;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/am;->b:Lsoftware/simplicial/a/am;

    if-ne v0, v2, :cond_2

    .line 64
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v0

    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/d;->c:Landroid/content/res/Resources;

    invoke-static {v0, v2}, Lsoftware/simplicial/nebulous/f/r;->a(Lsoftware/simplicial/a/aa;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    .line 126
    :goto_1
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/d;->g:Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 127
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/d;->e:Landroid/text/TextPaint;

    invoke-virtual {v2, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 128
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/d;->f:Landroid/graphics/Canvas;

    const/4 v2, 0x0

    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/b/d;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    const v4, 0x3f4ccccd    # 0.8f

    mul-float/2addr v3, v4

    iget-object v4, p0, Lsoftware/simplicial/nebulous/views/b/d;->e:Landroid/text/TextPaint;

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 130
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/d;->g:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 66
    :cond_2
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/aa;->e:Lsoftware/simplicial/a/aa;

    if-ne v0, v2, :cond_3

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->c(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/am;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/am;->e:Lsoftware/simplicial/a/am;

    if-ne v0, v2, :cond_3

    .line 68
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v0

    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/d;->c:Landroid/content/res/Resources;

    invoke-static {v0, v2}, Lsoftware/simplicial/nebulous/f/r;->a(Lsoftware/simplicial/a/aa;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 70
    :cond_3
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/aa;->f:Lsoftware/simplicial/a/aa;

    if-ne v0, v2, :cond_4

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->c(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/am;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/am;->i:Lsoftware/simplicial/a/am;

    if-ne v0, v2, :cond_4

    .line 72
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v0

    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/d;->c:Landroid/content/res/Resources;

    invoke-static {v0, v2}, Lsoftware/simplicial/nebulous/f/r;->a(Lsoftware/simplicial/a/aa;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 74
    :cond_4
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/aa;->g:Lsoftware/simplicial/a/aa;

    if-ne v0, v2, :cond_5

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->c(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/am;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/am;->l:Lsoftware/simplicial/a/am;

    if-ne v0, v2, :cond_5

    .line 76
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v0

    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/d;->c:Landroid/content/res/Resources;

    invoke-static {v0, v2}, Lsoftware/simplicial/nebulous/f/r;->a(Lsoftware/simplicial/a/aa;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 78
    :cond_5
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/aa;->i:Lsoftware/simplicial/a/aa;

    if-ne v0, v2, :cond_6

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->c(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/am;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    if-ne v0, v2, :cond_6

    .line 80
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v0

    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/d;->c:Landroid/content/res/Resources;

    invoke-static {v0, v2}, Lsoftware/simplicial/nebulous/f/r;->a(Lsoftware/simplicial/a/aa;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 82
    :cond_6
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/aa;->c:Lsoftware/simplicial/a/aa;

    if-ne v0, v2, :cond_7

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->c(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/am;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/am;->f:Lsoftware/simplicial/a/am;

    if-ne v0, v2, :cond_7

    .line 84
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v0

    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/d;->c:Landroid/content/res/Resources;

    invoke-static {v0, v2}, Lsoftware/simplicial/nebulous/f/r;->a(Lsoftware/simplicial/a/aa;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 86
    :cond_7
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/aa;->h:Lsoftware/simplicial/a/aa;

    if-ne v0, v2, :cond_8

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->c(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/am;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/am;->d:Lsoftware/simplicial/a/am;

    if-ne v0, v2, :cond_8

    .line 88
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v0

    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/d;->c:Landroid/content/res/Resources;

    invoke-static {v0, v2}, Lsoftware/simplicial/nebulous/f/r;->a(Lsoftware/simplicial/a/aa;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 90
    :cond_8
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/aa;->m:Lsoftware/simplicial/a/aa;

    if-ne v0, v2, :cond_9

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->c(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/am;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    if-ne v0, v2, :cond_9

    .line 92
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v0

    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/d;->c:Landroid/content/res/Resources;

    invoke-static {v0, v2}, Lsoftware/simplicial/nebulous/f/r;->a(Lsoftware/simplicial/a/aa;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 94
    :cond_9
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/aa;->d:Lsoftware/simplicial/a/aa;

    if-ne v0, v2, :cond_a

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->c(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/am;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-ne v0, v2, :cond_a

    .line 96
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v0

    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/d;->c:Landroid/content/res/Resources;

    invoke-static {v0, v2}, Lsoftware/simplicial/nebulous/f/r;->a(Lsoftware/simplicial/a/aa;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 98
    :cond_a
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/aa;->j:Lsoftware/simplicial/a/aa;

    if-ne v0, v2, :cond_b

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->c(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/am;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/am;->a:Lsoftware/simplicial/a/am;

    if-ne v0, v2, :cond_b

    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FFA Score : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->b(Lsoftware/simplicial/nebulous/views/b/d$a;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v2

    iget v2, v2, Lsoftware/simplicial/a/aa;->s:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 102
    :cond_b
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/aa;->k:Lsoftware/simplicial/a/aa;

    if-ne v0, v2, :cond_c

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->c(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/am;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/am;->j:Lsoftware/simplicial/a/am;

    if-ne v0, v2, :cond_c

    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FFAU Score : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->b(Lsoftware/simplicial/nebulous/views/b/d$a;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v2

    iget v2, v2, Lsoftware/simplicial/a/aa;->s:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 106
    :cond_c
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/aa;->l:Lsoftware/simplicial/a/aa;

    if-ne v0, v2, :cond_d

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->c(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/am;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/am;->c:Lsoftware/simplicial/a/am;

    if-ne v0, v2, :cond_d

    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Team Score : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->b(Lsoftware/simplicial/nebulous/views/b/d$a;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v2

    iget v2, v2, Lsoftware/simplicial/a/aa;->s:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 110
    :cond_d
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/aa;->n:Lsoftware/simplicial/a/aa;

    if-ne v0, v2, :cond_e

    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Dots : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->b(Lsoftware/simplicial/nebulous/views/b/d$a;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v2

    iget v2, v2, Lsoftware/simplicial/a/aa;->s:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 114
    :cond_e
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/aa;->o:Lsoftware/simplicial/a/aa;

    if-ne v0, v2, :cond_f

    .line 116
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Blobs : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->b(Lsoftware/simplicial/nebulous/views/b/d$a;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v2

    iget v2, v2, Lsoftware/simplicial/a/aa;->s:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 118
    :cond_f
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/aa;->p:Lsoftware/simplicial/a/aa;

    if-ne v0, v2, :cond_10

    .line 120
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v0

    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/d;->c:Landroid/content/res/Resources;

    invoke-static {v0, v2}, Lsoftware/simplicial/nebulous/f/r;->a(Lsoftware/simplicial/a/aa;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 124
    :cond_10
    const-string v0, ""

    goto/16 :goto_1
.end method

.method public a(Lsoftware/simplicial/a/aa;Lsoftware/simplicial/a/am;I)V
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/d;->b:Lsoftware/simplicial/nebulous/views/b/d$a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/views/b/d$a;->a(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/aa;

    move-result-object v0

    invoke-virtual {v0, p1}, Lsoftware/simplicial/a/aa;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/d;->b:Lsoftware/simplicial/nebulous/views/b/d$a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/views/b/d$a;->b(Lsoftware/simplicial/nebulous/views/b/d$a;)I

    move-result v0

    if-ne v0, p3, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/d;->b:Lsoftware/simplicial/nebulous/views/b/d$a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/views/b/d$a;->c(Lsoftware/simplicial/nebulous/views/b/d$a;)Lsoftware/simplicial/a/am;

    move-result-object v0

    if-ne v0, p2, :cond_0

    .line 46
    :goto_0
    return-void

    .line 44
    :cond_0
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/d$a;

    invoke-direct {v0, p0, p1, p2, p3}, Lsoftware/simplicial/nebulous/views/b/d$a;-><init>(Lsoftware/simplicial/nebulous/views/b/d;Lsoftware/simplicial/a/aa;Lsoftware/simplicial/a/am;I)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/d;->b:Lsoftware/simplicial/nebulous/views/b/d$a;

    .line 45
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/d;->b:Lsoftware/simplicial/nebulous/views/b/d$a;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lsoftware/simplicial/nebulous/views/b/d;->a(Ljava/lang/Object;Z)V

    goto :goto_0
.end method
