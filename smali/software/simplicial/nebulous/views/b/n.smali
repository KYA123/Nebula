.class public Lsoftware/simplicial/nebulous/views/b/n;
.super Lsoftware/simplicial/nebulous/views/b/o;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/nebulous/views/b/n$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lsoftware/simplicial/nebulous/views/b/o",
        "<",
        "Lsoftware/simplicial/nebulous/views/b/n$a;",
        ">;"
    }
.end annotation


# instance fields
.field private b:Lsoftware/simplicial/nebulous/views/b/n$a;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v3, -0x1

    .line 45
    const v0, 0x42666666    # 57.6f

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-direct {p0, p1, v0, v3, v1}, Lsoftware/simplicial/nebulous/views/b/o;-><init>(Landroid/content/res/Resources;FILandroid/graphics/Paint$Align;)V

    .line 41
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/n$a;

    sget-object v8, Lsoftware/simplicial/a/am;->a:Lsoftware/simplicial/a/am;

    move-object v1, p0

    move v4, v2

    move v5, v3

    move v6, v2

    move v7, v3

    invoke-direct/range {v0 .. v8}, Lsoftware/simplicial/nebulous/views/b/n$a;-><init>(Lsoftware/simplicial/nebulous/views/b/n;IIZIZILsoftware/simplicial/a/am;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/n;->b:Lsoftware/simplicial/nebulous/views/b/n$a;

    .line 46
    return-void
.end method


# virtual methods
.method bridge synthetic a(Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 17
    check-cast p1, Lsoftware/simplicial/nebulous/views/b/n$a;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/views/b/n;->a(Lsoftware/simplicial/nebulous/views/b/n$a;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method a(Lsoftware/simplicial/nebulous/views/b/n$a;)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const v7, 0x7f080142

    const v6, 0x7f080311

    const/16 v5, 0xff

    const/4 v4, 0x0

    .line 61
    if-nez p1, :cond_0

    .line 62
    const/4 v0, 0x0

    .line 160
    :goto_0
    return-object v0

    .line 64
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/n;->g:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 66
    const/16 v0, 0x200

    const/16 v1, 0x40

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/n;->g:Landroid/graphics/Bitmap;

    .line 67
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/n;->g:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/n;->f:Landroid/graphics/Canvas;

    .line 70
    :cond_1
    const-string v1, ""

    .line 71
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/n$a;->g(Lsoftware/simplicial/nebulous/views/b/n$a;)I

    move-result v0

    .line 73
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/n$a;->a(Lsoftware/simplicial/nebulous/views/b/n$a;)I

    move-result v2

    const/16 v3, 0x7fff

    if-eq v2, v3, :cond_2

    .line 75
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/n$a;->a(Lsoftware/simplicial/nebulous/views/b/n$a;)I

    move-result v2

    const/16 v3, -0x28

    if-gt v2, v3, :cond_3

    .line 156
    :cond_2
    :goto_1
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/n;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v4}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 157
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/n;->e:Landroid/text/TextPaint;

    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 158
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/n;->f:Landroid/graphics/Canvas;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/n;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/b/n;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    const v4, 0x3f4ccccd    # 0.8f

    mul-float/2addr v3, v4

    iget-object v4, p0, Lsoftware/simplicial/nebulous/views/b/n;->e:Landroid/text/TextPaint;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 160
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/n;->g:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 79
    :cond_3
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/n$a;->a(Lsoftware/simplicial/nebulous/views/b/n$a;)I

    move-result v2

    const/16 v3, -0x1e

    if-gt v2, v3, :cond_4

    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/n;->c:Landroid/content/res/Resources;

    const v2, 0x7f080185

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/n$a;->a(Lsoftware/simplicial/nebulous/views/b/n$a;)I

    move-result v1

    add-int/lit8 v1, v1, 0x28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 82
    invoke-static {v5, v4, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto :goto_1

    .line 84
    :cond_4
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/n$a;->a(Lsoftware/simplicial/nebulous/views/b/n$a;)I

    move-result v2

    const/16 v3, -0xa

    if-gt v2, v3, :cond_6

    .line 86
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/n;->c:Landroid/content/res/Resources;

    const v1, 0x7f080134

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 87
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/n$a;->a(Lsoftware/simplicial/nebulous/views/b/n$a;)I

    move-result v0

    const/16 v2, -0x14

    if-gt v0, v2, :cond_5

    .line 89
    invoke-static {v5, v4, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto :goto_1

    .line 93
    :cond_5
    invoke-static {v4, v5, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto :goto_1

    .line 96
    :cond_6
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/n$a;->a(Lsoftware/simplicial/nebulous/views/b/n$a;)I

    move-result v2

    if-gtz v2, :cond_b

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/n$a;->b(Lsoftware/simplicial/nebulous/views/b/n$a;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 98
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/n$a;->c(Lsoftware/simplicial/nebulous/views/b/n$a;)I

    move-result v0

    if-lez v0, :cond_a

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/n$a;->e(Lsoftware/simplicial/nebulous/views/b/n$a;)I

    move-result v0

    if-ltz v0, :cond_a

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/n$a;->e(Lsoftware/simplicial/nebulous/views/b/n$a;)I

    move-result v0

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/n$a;->c(Lsoftware/simplicial/nebulous/views/b/n$a;)I

    move-result v1

    if-ge v0, v1, :cond_a

    .line 100
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/n$a;->d(Lsoftware/simplicial/nebulous/views/b/n$a;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 102
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/n;->c:Landroid/content/res/Resources;

    const v1, 0x7f0800e1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 103
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/n$a;->g(Lsoftware/simplicial/nebulous/views/b/n$a;)I

    move-result v0

    goto/16 :goto_1

    .line 107
    :cond_7
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/n$a;->e(Lsoftware/simplicial/nebulous/views/b/n$a;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 126
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/n;->c:Landroid/content/res/Resources;

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/n;->c:Landroid/content/res/Resources;

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 127
    invoke-static {v5, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto/16 :goto_1

    .line 110
    :pswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/n$a;->f(Lsoftware/simplicial/nebulous/views/b/n$a;)Lsoftware/simplicial/a/am;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-ne v0, v2, :cond_8

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/n;->c:Landroid/content/res/Resources;

    const v2, 0x7f08032e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/n;->c:Landroid/content/res/Resources;

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 111
    invoke-static {v5, v4, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto/16 :goto_1

    .line 110
    :cond_8
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/n;->c:Landroid/content/res/Resources;

    const v2, 0x7f08022b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 114
    :pswitch_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/n$a;->f(Lsoftware/simplicial/nebulous/views/b/n$a;)Lsoftware/simplicial/a/am;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-ne v0, v2, :cond_9

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/n;->c:Landroid/content/res/Resources;

    const v2, 0x7f08029d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/n;->c:Landroid/content/res/Resources;

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 115
    invoke-static {v4, v5, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto/16 :goto_1

    .line 114
    :cond_9
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/n;->c:Landroid/content/res/Resources;

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 118
    :pswitch_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/n;->c:Landroid/content/res/Resources;

    const v2, 0x7f080057

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/n;->c:Landroid/content/res/Resources;

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 119
    invoke-static {v4, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto/16 :goto_1

    .line 122
    :pswitch_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/n;->c:Landroid/content/res/Resources;

    const v2, 0x7f080317

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/n;->c:Landroid/content/res/Resources;

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 123
    invoke-static {v5, v5, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto/16 :goto_1

    .line 134
    :cond_a
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/n;->c:Landroid/content/res/Resources;

    const v1, 0x7f0802e9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 135
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/n$a;->g(Lsoftware/simplicial/nebulous/views/b/n$a;)I

    move-result v0

    goto/16 :goto_1

    .line 138
    :cond_b
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/n$a;->a(Lsoftware/simplicial/nebulous/views/b/n$a;)I

    move-result v2

    if-lez v2, :cond_2

    .line 140
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/n$a;->a(Lsoftware/simplicial/nebulous/views/b/n$a;)I

    move-result v0

    const/16 v1, 0x14

    if-gt v0, v1, :cond_c

    .line 142
    invoke-static {v5, v4, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    .line 152
    :goto_4
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/n$a;->a(Lsoftware/simplicial/nebulous/views/b/n$a;)I

    move-result v1

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v2, v1

    invoke-static {v2, v3}, Lsoftware/simplicial/nebulous/f/aa;->a(J)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 144
    :cond_c
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/n$a;->a(Lsoftware/simplicial/nebulous/views/b/n$a;)I

    move-result v0

    const/16 v1, 0x3c

    if-gt v0, v1, :cond_d

    .line 146
    invoke-static {v5, v5, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto :goto_4

    .line 150
    :cond_d
    invoke-static {v4, v5, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto :goto_4

    .line 107
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(IIZIZILsoftware/simplicial/a/am;)V
    .locals 10

    .prologue
    .line 50
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/n;->b:Lsoftware/simplicial/nebulous/views/b/n$a;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/views/b/n$a;->a(Lsoftware/simplicial/nebulous/views/b/n$a;)I

    move-result v1

    if-ne v1, p1, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/n;->b:Lsoftware/simplicial/nebulous/views/b/n$a;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/views/b/n$a;->b(Lsoftware/simplicial/nebulous/views/b/n$a;)Z

    move-result v1

    if-ne v1, p3, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/n;->b:Lsoftware/simplicial/nebulous/views/b/n$a;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/views/b/n$a;->c(Lsoftware/simplicial/nebulous/views/b/n$a;)I

    move-result v1

    if-ne v1, p4, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/n;->b:Lsoftware/simplicial/nebulous/views/b/n$a;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/views/b/n$a;->d(Lsoftware/simplicial/nebulous/views/b/n$a;)Z

    move-result v1

    if-ne v1, p5, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/n;->b:Lsoftware/simplicial/nebulous/views/b/n$a;

    .line 51
    invoke-static {v1}, Lsoftware/simplicial/nebulous/views/b/n$a;->e(Lsoftware/simplicial/nebulous/views/b/n$a;)I

    move-result v1

    move/from16 v0, p6

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/n;->b:Lsoftware/simplicial/nebulous/views/b/n$a;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/views/b/n$a;->f(Lsoftware/simplicial/nebulous/views/b/n$a;)Lsoftware/simplicial/a/am;

    move-result-object v1

    move-object/from16 v0, p7

    if-ne v1, v0, :cond_0

    .line 56
    :goto_0
    return-void

    .line 54
    :cond_0
    new-instance v1, Lsoftware/simplicial/nebulous/views/b/n$a;

    move-object v2, p0

    move v3, p2

    move v4, p1

    move v5, p3

    move v6, p4

    move v7, p5

    move/from16 v8, p6

    move-object/from16 v9, p7

    invoke-direct/range {v1 .. v9}, Lsoftware/simplicial/nebulous/views/b/n$a;-><init>(Lsoftware/simplicial/nebulous/views/b/n;IIZIZILsoftware/simplicial/a/am;)V

    iput-object v1, p0, Lsoftware/simplicial/nebulous/views/b/n;->b:Lsoftware/simplicial/nebulous/views/b/n$a;

    .line 55
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/n;->b:Lsoftware/simplicial/nebulous/views/b/n$a;

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lsoftware/simplicial/nebulous/views/b/n;->a(Ljava/lang/Object;Z)V

    goto :goto_0
.end method
