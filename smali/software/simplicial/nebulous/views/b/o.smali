.class abstract Lsoftware/simplicial/nebulous/views/b/o;
.super Lsoftware/simplicial/nebulous/views/b/b;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lsoftware/simplicial/nebulous/views/b/b",
        "<TT;>;"
    }
.end annotation


# instance fields
.field protected final c:Landroid/content/res/Resources;

.field protected final d:F

.field protected e:Landroid/text/TextPaint;

.field protected f:Landroid/graphics/Canvas;

.field g:Landroid/graphics/Bitmap;


# direct methods
.method constructor <init>(Landroid/content/res/Resources;FILandroid/graphics/Paint$Align;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 23
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/views/b/b;-><init>()V

    .line 18
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/o;->e:Landroid/text/TextPaint;

    .line 19
    iput-object v1, p0, Lsoftware/simplicial/nebulous/views/b/o;->f:Landroid/graphics/Canvas;

    .line 20
    iput-object v1, p0, Lsoftware/simplicial/nebulous/views/b/o;->g:Landroid/graphics/Bitmap;

    .line 24
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/b/o;->c:Landroid/content/res/Resources;

    .line 25
    iput p2, p0, Lsoftware/simplicial/nebulous/views/b/o;->d:F

    .line 26
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/o;->e:Landroid/text/TextPaint;

    invoke-virtual {v0, p3}, Landroid/text/TextPaint;->setColor(I)V

    .line 27
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/o;->e:Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 28
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/o;->e:Landroid/text/TextPaint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 29
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/o;->e:Landroid/text/TextPaint;

    invoke-virtual {v0, p4}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 30
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/o;->e:Landroid/text/TextPaint;

    invoke-virtual {v0, p2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 31
    return-void
.end method
