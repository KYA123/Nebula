.class public Lsoftware/simplicial/nebulous/views/b/r;
.super Lsoftware/simplicial/nebulous/views/b/o;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/nebulous/views/b/r$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lsoftware/simplicial/nebulous/views/b/o",
        "<",
        "Lsoftware/simplicial/nebulous/views/b/r$a;",
        ">;"
    }
.end annotation


# instance fields
.field private b:Lsoftware/simplicial/nebulous/views/b/r$a;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 4

    .prologue
    const/16 v3, 0xff

    .line 33
    const v0, 0x41e66666    # 28.8f

    const/16 v1, 0x4b

    const/16 v2, 0x80

    invoke-static {v1, v2, v3, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-direct {p0, p1, v0, v1, v2}, Lsoftware/simplicial/nebulous/views/b/o;-><init>(Landroid/content/res/Resources;FILandroid/graphics/Paint$Align;)V

    .line 29
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/r$a;

    const/4 v1, -0x1

    sget-object v2, Lsoftware/simplicial/nebulous/views/k;->a:Lsoftware/simplicial/nebulous/views/k;

    invoke-direct {v0, p0, v1, v2}, Lsoftware/simplicial/nebulous/views/b/r$a;-><init>(Lsoftware/simplicial/nebulous/views/b/r;ILsoftware/simplicial/nebulous/views/k;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/r;->b:Lsoftware/simplicial/nebulous/views/b/r$a;

    .line 34
    return-void
.end method


# virtual methods
.method bridge synthetic a(Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 15
    check-cast p1, Lsoftware/simplicial/nebulous/views/b/r$a;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/views/b/r;->a(Lsoftware/simplicial/nebulous/views/b/r$a;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method a(Lsoftware/simplicial/nebulous/views/b/r$a;)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0xff

    .line 48
    if-nez p1, :cond_0

    .line 49
    const/4 v0, 0x0

    .line 79
    :goto_0
    return-object v0

    .line 51
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/r;->g:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 53
    const/16 v0, 0x80

    const/16 v1, 0x20

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/r;->g:Landroid/graphics/Bitmap;

    .line 54
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/r;->g:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/r;->f:Landroid/graphics/Canvas;

    .line 57
    :cond_1
    sget-object v0, Lsoftware/simplicial/nebulous/views/b/r$1;->a:[I

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/r$a;->b(Lsoftware/simplicial/nebulous/views/b/r$a;)Lsoftware/simplicial/nebulous/views/k;

    move-result-object v1

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/views/k;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 71
    :goto_1
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/r$a;->b(Lsoftware/simplicial/nebulous/views/b/r$a;)Lsoftware/simplicial/nebulous/views/k;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/views/k;->a:Lsoftware/simplicial/nebulous/views/k;

    if-eq v0, v1, :cond_2

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/r$a;->b(Lsoftware/simplicial/nebulous/views/b/r$a;)Lsoftware/simplicial/nebulous/views/k;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/views/k;->b:Lsoftware/simplicial/nebulous/views/k;

    if-eq v0, v1, :cond_2

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/r$a;->b(Lsoftware/simplicial/nebulous/views/b/r$a;)Lsoftware/simplicial/nebulous/views/k;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/views/k;->c:Lsoftware/simplicial/nebulous/views/k;

    if-ne v0, v1, :cond_3

    .line 72
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/r$a;->a(Lsoftware/simplicial/nebulous/views/b/r$a;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " XP"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 76
    :goto_2
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/r;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v4}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 77
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/r;->f:Landroid/graphics/Canvas;

    const/4 v2, 0x0

    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/b/r;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    const v4, 0x3f6147ae    # 0.88f

    mul-float/2addr v3, v4

    iget-object v4, p0, Lsoftware/simplicial/nebulous/views/b/r;->e:Landroid/text/TextPaint;

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 79
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/r;->g:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 60
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/r;->e:Landroid/text/TextPaint;

    invoke-static {v3, v4, v3, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    goto :goto_1

    .line 63
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/r;->e:Landroid/text/TextPaint;

    invoke-static {v3, v3, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    goto :goto_1

    .line 66
    :pswitch_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/r;->e:Landroid/text/TextPaint;

    invoke-static {v3, v3, v4, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    goto :goto_1

    .line 74
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/r$a;->a(Lsoftware/simplicial/nebulous/views/b/r$a;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 57
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(ILsoftware/simplicial/nebulous/views/k;)V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/r;->b:Lsoftware/simplicial/nebulous/views/b/r$a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/views/b/r$a;->a(Lsoftware/simplicial/nebulous/views/b/r$a;)I

    move-result v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/r;->b:Lsoftware/simplicial/nebulous/views/b/r$a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/views/b/r$a;->b(Lsoftware/simplicial/nebulous/views/b/r$a;)Lsoftware/simplicial/nebulous/views/k;

    move-result-object v0

    if-ne v0, p2, :cond_0

    .line 43
    :goto_0
    return-void

    .line 41
    :cond_0
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/r$a;

    invoke-direct {v0, p0, p1, p2}, Lsoftware/simplicial/nebulous/views/b/r$a;-><init>(Lsoftware/simplicial/nebulous/views/b/r;ILsoftware/simplicial/nebulous/views/k;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/r;->b:Lsoftware/simplicial/nebulous/views/b/r$a;

    .line 42
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/r;->b:Lsoftware/simplicial/nebulous/views/b/r$a;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lsoftware/simplicial/nebulous/views/b/r;->a(Ljava/lang/Object;Z)V

    goto :goto_0
.end method
