.class public Lsoftware/simplicial/nebulous/views/d;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final J:Ljava/util/Random;


# instance fields
.field A:[F

.field B:[F

.field C:F

.field D:F

.field E:F

.field F:F

.field public G:F

.field public H:F

.field public I:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/nebulous/views/g;",
            ">;"
        }
    .end annotation
.end field

.field final a:F

.field b:Z

.field c:I

.field d:I

.field e:[F

.field f:[F

.field g:[F

.field h:[F

.field i:[F

.field j:[F

.field k:Z

.field l:F

.field m:Z

.field n:[F

.field o:[F

.field p:F

.field q:F

.field r:F

.field s:F

.field t:F

.field u:F

.field v:F

.field w:F

.field x:F

.field y:I

.field z:[F


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 14
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Random;-><init>(J)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/d;->J:Ljava/util/Random;

    return-void
.end method

.method public constructor <init>()V
    .locals 10

    .prologue
    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    const/16 v6, 0x8

    const/4 v5, 0x0

    const/16 v1, 0x5a

    const/4 v4, 0x0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    sget-object v0, Lsoftware/simplicial/nebulous/views/d;->J:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/d;->a:F

    .line 19
    iput-boolean v5, p0, Lsoftware/simplicial/nebulous/views/d;->b:Z

    .line 20
    iput v5, p0, Lsoftware/simplicial/nebulous/views/d;->c:I

    .line 21
    iput v5, p0, Lsoftware/simplicial/nebulous/views/d;->d:I

    .line 22
    new-array v0, v1, [F

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/d;->e:[F

    .line 23
    new-array v0, v1, [F

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/d;->f:[F

    .line 24
    new-array v0, v1, [F

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/d;->g:[F

    .line 25
    new-array v0, v1, [F

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/d;->h:[F

    .line 26
    new-array v0, v1, [F

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/d;->i:[F

    .line 27
    new-array v0, v1, [F

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/d;->j:[F

    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/views/d;->k:Z

    .line 29
    sget-object v0, Lsoftware/simplicial/nebulous/views/d;->J:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v0, v2

    mul-double/2addr v0, v8

    double-to-float v0, v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/d;->l:F

    .line 30
    iput-boolean v5, p0, Lsoftware/simplicial/nebulous/views/d;->m:Z

    .line 31
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/d;->n:[F

    .line 32
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/d;->o:[F

    .line 33
    sget-object v0, Lsoftware/simplicial/nebulous/views/d;->J:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v0, v2

    mul-double/2addr v0, v8

    double-to-float v0, v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/d;->p:F

    .line 37
    iput v4, p0, Lsoftware/simplicial/nebulous/views/d;->t:F

    .line 38
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lsoftware/simplicial/nebulous/views/d;->u:F

    .line 39
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lsoftware/simplicial/nebulous/views/d;->v:F

    .line 40
    iput v4, p0, Lsoftware/simplicial/nebulous/views/d;->w:F

    .line 41
    iput v4, p0, Lsoftware/simplicial/nebulous/views/d;->x:F

    .line 42
    iput v5, p0, Lsoftware/simplicial/nebulous/views/d;->y:I

    .line 43
    new-array v0, v6, [F

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/d;->z:[F

    .line 44
    new-array v0, v6, [F

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/d;->A:[F

    .line 45
    new-array v0, v6, [F

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/d;->B:[F

    .line 46
    iput v4, p0, Lsoftware/simplicial/nebulous/views/d;->C:F

    .line 47
    iput v4, p0, Lsoftware/simplicial/nebulous/views/d;->D:F

    .line 48
    iput v4, p0, Lsoftware/simplicial/nebulous/views/d;->E:F

    .line 49
    iput v4, p0, Lsoftware/simplicial/nebulous/views/d;->F:F

    .line 51
    sget-object v0, Lsoftware/simplicial/nebulous/views/d;->J:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/d;->G:F

    .line 52
    iput v4, p0, Lsoftware/simplicial/nebulous/views/d;->H:F

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/d;->I:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public a(Lsoftware/simplicial/nebulous/views/j;FFFZILsoftware/simplicial/a/e;FFFZZFID)V
    .locals 19

    .prologue
    .line 58
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lsoftware/simplicial/nebulous/views/d;->q:F

    .line 59
    move/from16 v0, p3

    move-object/from16 v1, p0

    iput v0, v1, Lsoftware/simplicial/nebulous/views/d;->r:F

    .line 60
    move/from16 v0, p4

    move-object/from16 v1, p0

    iput v0, v1, Lsoftware/simplicial/nebulous/views/d;->s:F

    .line 61
    const/4 v2, 0x0

    .line 62
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lsoftware/simplicial/nebulous/views/d;->b:Z

    move/from16 v0, p5

    if-eq v3, v0, :cond_1

    .line 64
    if-eqz p5, :cond_0

    .line 65
    const/4 v2, 0x1

    .line 67
    :cond_0
    move/from16 v0, p5

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lsoftware/simplicial/nebulous/views/d;->b:Z

    .line 68
    if-nez p12, :cond_1

    .line 69
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lsoftware/simplicial/nebulous/views/d;->k:Z

    .line 72
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/views/d;->z:[F

    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/nebulous/views/d;->y:I

    aget v3, v3, v4

    sub-float v3, p8, v3

    .line 73
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/d;->A:[F

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/nebulous/views/d;->y:I

    aget v4, v4, v5

    sub-float v4, p9, v4

    .line 74
    const/4 v5, 0x0

    cmpl-float v5, v3, v5

    if-nez v5, :cond_4

    const/4 v5, 0x0

    cmpl-float v5, v4, v5

    if-nez v5, :cond_4

    .line 76
    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/nebulous/views/d;->F:F

    float-to-double v6, v5

    add-double v6, v6, p15

    double-to-float v5, v6

    move-object/from16 v0, p0

    iput v5, v0, Lsoftware/simplicial/nebulous/views/d;->F:F

    .line 77
    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/nebulous/views/d;->F:F

    const/high16 v6, 0x3f800000    # 1.0f

    cmpl-float v5, v5, v6

    if-lez v5, :cond_2

    .line 78
    const/high16 v5, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iput v5, v0, Lsoftware/simplicial/nebulous/views/d;->F:F

    .line 86
    :cond_2
    :goto_0
    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/nebulous/views/d;->y:I

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, p0

    iput v5, v0, Lsoftware/simplicial/nebulous/views/d;->y:I

    .line 87
    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/nebulous/views/d;->y:I

    const/16 v6, 0x8

    if-lt v5, v6, :cond_3

    .line 88
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput v5, v0, Lsoftware/simplicial/nebulous/views/d;->y:I

    .line 89
    :cond_3
    if-eqz v2, :cond_5

    .line 91
    const/4 v2, 0x0

    :goto_1
    const/16 v5, 0x8

    if-ge v2, v5, :cond_6

    .line 93
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/d;->z:[F

    aput p8, v5, v2

    .line 94
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/d;->A:[F

    aput p9, v5, v2

    .line 95
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/d;->B:[F

    aput p10, v5, v2

    .line 91
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 82
    :cond_4
    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/nebulous/views/d;->F:F

    float-to-double v6, v5

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    mul-double v8, v8, p15

    sub-double/2addr v6, v8

    double-to-float v5, v6

    move-object/from16 v0, p0

    iput v5, v0, Lsoftware/simplicial/nebulous/views/d;->F:F

    .line 83
    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/nebulous/views/d;->F:F

    const/4 v6, 0x0

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_2

    .line 84
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput v5, v0, Lsoftware/simplicial/nebulous/views/d;->F:F

    goto :goto_0

    .line 100
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/d;->z:[F

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/nebulous/views/d;->y:I

    aput p8, v2, v5

    .line 101
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/d;->A:[F

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/nebulous/views/d;->y:I

    aput p9, v2, v5

    .line 102
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/d;->B:[F

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/nebulous/views/d;->y:I

    aput p10, v2, v5

    .line 105
    :cond_6
    const/4 v2, 0x0

    .line 106
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const v6, 0x3a83126f    # 0.001f

    cmpl-float v5, v5, v6

    if-gtz v5, :cond_7

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const v6, 0x3a83126f    # 0.001f

    cmpl-float v5, v5, v6

    if-lez v5, :cond_b

    .line 108
    :cond_7
    float-to-double v4, v4

    float-to-double v2, v3

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    neg-double v2, v2

    double-to-float v2, v2

    .line 110
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/nebulous/views/d;->C:F

    sub-float v3, v2, v3

    float-to-double v4, v3

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    cmpl-double v3, v4, v6

    if-lez v3, :cond_8

    .line 111
    float-to-double v2, v2

    const-wide v4, 0x401921fb54442d18L    # 6.283185307179586

    sub-double/2addr v2, v4

    double-to-float v2, v2

    .line 112
    :cond_8
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/nebulous/views/d;->C:F

    sub-float v3, v2, v3

    float-to-double v4, v3

    const-wide v6, -0x3ff6de04abbbd2e8L    # -3.141592653589793

    cmpg-double v3, v4, v6

    if-gez v3, :cond_9

    .line 113
    float-to-double v2, v2

    const-wide v4, 0x401921fb54442d18L    # 6.283185307179586

    add-double/2addr v2, v4

    double-to-float v2, v2

    .line 115
    :cond_9
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/nebulous/views/d;->C:F

    sub-float v3, v2, v3

    const/high16 v4, 0x40800000    # 4.0f

    div-float/2addr v3, v4

    .line 116
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/nebulous/views/d;->C:F

    add-float/2addr v4, v3

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/d;->C:F

    .line 118
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/nebulous/views/d;->C:F

    cmpg-float v4, v4, v2

    if-gez v4, :cond_c

    .line 120
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/nebulous/views/d;->C:F

    cmpl-float v4, v4, v2

    if-lez v4, :cond_a

    .line 121
    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->C:F

    .line 122
    :cond_a
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->C:F

    float-to-double v4, v2

    const-wide v6, 0x401921fb54442d18L    # 6.283185307179586

    cmpl-double v2, v4, v6

    if-lez v2, :cond_e

    .line 123
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->C:F

    float-to-double v4, v2

    const-wide v6, 0x401921fb54442d18L    # 6.283185307179586

    sub-double/2addr v4, v6

    double-to-float v2, v4

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->C:F

    move v2, v3

    .line 134
    :cond_b
    :goto_2
    if-nez p5, :cond_f

    .line 864
    :goto_3
    return-void

    .line 125
    :cond_c
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/nebulous/views/d;->C:F

    cmpl-float v4, v4, v2

    if-lez v4, :cond_e

    .line 127
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/nebulous/views/d;->C:F

    cmpg-float v4, v4, v2

    if-gez v4, :cond_d

    .line 128
    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->C:F

    .line 129
    :cond_d
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->C:F

    const/4 v4, 0x0

    cmpg-float v2, v2, v4

    if-gez v2, :cond_e

    .line 130
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->C:F

    float-to-double v4, v2

    const-wide v6, 0x401921fb54442d18L    # 6.283185307179586

    add-double/2addr v4, v6

    double-to-float v2, v4

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->C:F

    :cond_e
    move v2, v3

    goto :goto_2

    .line 137
    :cond_f
    const/high16 v4, 0x3f800000    # 1.0f

    .line 138
    const/high16 v3, 0x3f800000    # 1.0f

    .line 140
    sget-object v5, Lsoftware/simplicial/nebulous/views/d$1;->a:[I

    move-object/from16 v0, p7

    iget-object v6, v0, Lsoftware/simplicial/a/e;->ll:Lsoftware/simplicial/a/f;

    invoke-virtual {v6}, Lsoftware/simplicial/a/f;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    :cond_10
    :goto_4
    move v2, v3

    move v3, v4

    .line 378
    :goto_5
    sget-object v4, Lsoftware/simplicial/nebulous/views/d$1;->a:[I

    move-object/from16 v0, p7

    iget-object v5, v0, Lsoftware/simplicial/a/e;->ll:Lsoftware/simplicial/a/f;

    invoke-virtual {v5}, Lsoftware/simplicial/a/f;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_1

    .line 581
    :pswitch_0
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    const/high16 v5, 0x42c80000    # 100.0f

    div-float/2addr v3, v5

    add-float/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    .line 582
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/nebulous/views/d;->p:F

    const/high16 v4, 0x42c80000    # 100.0f

    div-float/2addr v2, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->p:F

    .line 587
    :cond_11
    :goto_6
    sget-object v2, Lsoftware/simplicial/nebulous/views/d$1;->a:[I

    move-object/from16 v0, p7

    iget-object v3, v0, Lsoftware/simplicial/a/e;->ll:Lsoftware/simplicial/a/f;

    invoke-virtual {v3}, Lsoftware/simplicial/a/f;->ordinal()I

    move-result v3

    aget v2, v2, v3

    sparse-switch v2, :sswitch_data_0

    .line 641
    :cond_12
    :goto_7
    sget-object v2, Lsoftware/simplicial/a/e;->iC:Lsoftware/simplicial/a/e;

    move-object/from16 v0, p7

    if-ne v0, v2, :cond_14

    .line 643
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    const v3, 0x3ccccccd    # 0.025f

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    .line 644
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_13

    .line 645
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    .line 646
    :cond_13
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    float-to-double v2, v2

    const-wide v4, 0x3fd5555560000000L    # 0.3333333432674408

    const-wide v6, 0x401921fb54442d18L    # 6.283185307179586

    move-object/from16 v0, p0

    iget v8, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    float-to-double v8, v8

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    .line 648
    :cond_14
    sget-object v2, Lsoftware/simplicial/a/e;->kv:Lsoftware/simplicial/a/e;

    move-object/from16 v0, p7

    if-ne v0, v2, :cond_16

    .line 650
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    const v3, 0x3ca3d70a    # 0.02f

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    .line 651
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_15

    .line 652
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    .line 653
    :cond_15
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    float-to-double v2, v2

    const-wide v4, 0x3fe5555560000000L    # 0.6666666865348816

    const-wide v6, 0x401921fb54442d18L    # 6.283185307179586

    move-object/from16 v0, p0

    iget v8, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    float-to-double v8, v8

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    .line 656
    :cond_16
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    float-to-double v2, v2

    const-wide v4, 0x401921fb54442d18L    # 6.283185307179586

    cmpl-double v2, v2, v4

    if-ltz v2, :cond_17

    .line 657
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    float-to-double v2, v2

    const-wide v4, 0x401921fb54442d18L    # 6.283185307179586

    sub-double/2addr v2, v4

    double-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    .line 658
    :cond_17
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gez v2, :cond_18

    .line 659
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    float-to-double v2, v2

    const-wide v4, 0x401921a8f9651827L    # 6.282871147914228

    add-double/2addr v2, v4

    double-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    .line 661
    :cond_18
    move-object/from16 v0, p7

    iget-object v2, v0, Lsoftware/simplicial/a/e;->ll:Lsoftware/simplicial/a/f;

    sget-object v3, Lsoftware/simplicial/a/f;->kt:Lsoftware/simplicial/a/f;

    if-eq v2, v3, :cond_1a

    .line 663
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->p:F

    float-to-double v2, v2

    const-wide v4, 0x401921fb54442d18L    # 6.283185307179586

    cmpl-double v2, v2, v4

    if-ltz v2, :cond_19

    .line 664
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->p:F

    float-to-double v2, v2

    const-wide v4, 0x401921fb54442d18L    # 6.283185307179586

    sub-double/2addr v2, v4

    double-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->p:F

    .line 665
    :cond_19
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->p:F

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1a

    .line 666
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->p:F

    float-to-double v2, v2

    const-wide v4, 0x401921a8f9651827L    # 6.282871147914228

    add-double/2addr v2, v4

    double-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->p:F

    .line 669
    :cond_1a
    sget-object v2, Lsoftware/simplicial/nebulous/views/d$1;->a:[I

    move-object/from16 v0, p7

    iget-object v3, v0, Lsoftware/simplicial/a/e;->ll:Lsoftware/simplicial/a/f;

    invoke-virtual {v3}, Lsoftware/simplicial/a/f;->ordinal()I

    move-result v3

    aget v2, v2, v3

    sparse-switch v2, :sswitch_data_1

    .line 693
    :cond_1b
    :goto_8
    sget-object v2, Lsoftware/simplicial/nebulous/views/d$1;->a:[I

    move-object/from16 v0, p7

    iget-object v3, v0, Lsoftware/simplicial/a/e;->ll:Lsoftware/simplicial/a/f;

    invoke-virtual {v3}, Lsoftware/simplicial/a/f;->ordinal()I

    move-result v3

    aget v2, v2, v3

    sparse-switch v2, :sswitch_data_2

    .line 778
    :cond_1c
    :goto_9
    move/from16 v0, p6

    move-object/from16 v1, p0

    iput v0, v1, Lsoftware/simplicial/nebulous/views/d;->c:I

    .line 779
    add-int/lit8 v2, p6, 0x1

    mul-int/lit8 v8, v2, 0x8

    .line 780
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->d:I

    .line 781
    add-int/lit8 v3, v8, 0x2

    move-object/from16 v0, p0

    iput v3, v0, Lsoftware/simplicial/nebulous/views/d;->d:I

    .line 784
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/nebulous/views/d;->d:I

    if-eq v2, v3, :cond_4d

    if-lez v2, :cond_4d

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lsoftware/simplicial/nebulous/views/d;->k:Z

    if-nez v3, :cond_4d

    .line 786
    const/4 v7, 0x1

    .line 787
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/nebulous/views/d;->d:I

    add-int/lit8 v9, v3, -0x1

    .line 788
    const/4 v3, 0x1

    .line 789
    add-int/lit8 v4, v2, -0x1

    .line 790
    add-int/lit8 v2, v9, 0x1

    new-array v10, v2, [F

    .line 791
    add-int/lit8 v2, v9, 0x1

    new-array v11, v2, [F

    .line 792
    add-int/lit8 v2, v9, 0x1

    new-array v12, v2, [F

    .line 793
    add-int/lit8 v2, v9, 0x1

    new-array v13, v2, [F

    move v6, v7

    .line 795
    :goto_a
    if-gt v6, v9, :cond_4c

    .line 798
    sub-int v2, v6, v7

    int-to-float v2, v2

    sub-int v5, v9, v7

    int-to-float v5, v5

    div-float/2addr v2, v5

    sub-int v5, v4, v3

    int-to-float v5, v5

    mul-float/2addr v2, v5

    int-to-float v5, v7

    add-float v14, v2, v5

    .line 799
    float-to-double v0, v14

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->floor(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-int v5, v0

    .line 800
    add-int/lit8 v2, v5, 0x1

    .line 801
    int-to-float v15, v5

    sub-float/2addr v14, v15

    .line 803
    if-ge v5, v3, :cond_1d

    move v5, v4

    .line 804
    :cond_1d
    if-le v5, v4, :cond_1e

    move v5, v3

    .line 805
    :cond_1e
    if-ge v2, v3, :cond_1f

    move v2, v4

    .line 806
    :cond_1f
    if-le v2, v4, :cond_20

    move v2, v3

    .line 808
    :cond_20
    move-object/from16 v0, p0

    iget-object v15, v0, Lsoftware/simplicial/nebulous/views/d;->e:[F

    aget v15, v15, v5

    const/high16 v16, 0x3f800000    # 1.0f

    sub-float v16, v16, v14

    mul-float v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/views/d;->e:[F

    move-object/from16 v16, v0

    aget v16, v16, v2

    mul-float v16, v16, v14

    add-float v15, v15, v16

    aput v15, v10, v6

    .line 809
    move-object/from16 v0, p0

    iget-object v15, v0, Lsoftware/simplicial/nebulous/views/d;->f:[F

    aget v15, v15, v5

    const/high16 v16, 0x3f800000    # 1.0f

    sub-float v16, v16, v14

    mul-float v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/views/d;->f:[F

    move-object/from16 v16, v0

    aget v16, v16, v2

    mul-float v16, v16, v14

    add-float v15, v15, v16

    aput v15, v11, v6

    .line 810
    move-object/from16 v0, p0

    iget-object v15, v0, Lsoftware/simplicial/nebulous/views/d;->i:[F

    aget v15, v15, v5

    const/high16 v16, 0x3f800000    # 1.0f

    sub-float v16, v16, v14

    mul-float v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/views/d;->i:[F

    move-object/from16 v16, v0

    aget v16, v16, v2

    mul-float v16, v16, v14

    add-float v15, v15, v16

    aput v15, v12, v6

    .line 811
    move-object/from16 v0, p0

    iget-object v15, v0, Lsoftware/simplicial/nebulous/views/d;->j:[F

    aget v5, v15, v5

    const/high16 v15, 0x3f800000    # 1.0f

    sub-float/2addr v15, v14

    mul-float/2addr v5, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lsoftware/simplicial/nebulous/views/d;->j:[F

    aget v2, v15, v2

    mul-float/2addr v2, v14

    add-float/2addr v2, v5

    aput v2, v13, v6

    .line 795
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto/16 :goto_a

    .line 151
    :pswitch_1
    const/high16 v2, 0x40800000    # 4.0f

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    .line 152
    goto/16 :goto_5

    .line 154
    :pswitch_2
    const/high16 v2, 0x40900000    # 4.5f

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    .line 155
    goto/16 :goto_5

    .line 157
    :pswitch_3
    const/high16 v2, 0x41000000    # 8.0f

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    .line 158
    goto/16 :goto_5

    .line 160
    :pswitch_4
    const/high16 v2, 0x41800000    # 16.0f

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    .line 161
    goto/16 :goto_5

    .line 163
    :pswitch_5
    const/high16 v2, 0x41000000    # 8.0f

    move v3, v4

    .line 164
    goto/16 :goto_5

    .line 166
    :pswitch_6
    const/high16 v2, 0x41200000    # 10.0f

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    .line 167
    goto/16 :goto_5

    .line 169
    :pswitch_7
    const/high16 v2, 0x40000000    # 2.0f

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    .line 170
    goto/16 :goto_5

    .line 172
    :pswitch_8
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->a:F

    const/high16 v4, 0x3f000000    # 0.5f

    cmpg-float v2, v2, v4

    if-gez v2, :cond_21

    const/high16 v2, 0x41200000    # 10.0f

    :goto_b
    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    .line 173
    goto/16 :goto_5

    .line 172
    :cond_21
    const/high16 v2, -0x3ee00000    # -10.0f

    goto :goto_b

    .line 175
    :pswitch_9
    const/high16 v2, 0x40a00000    # 5.0f

    const/high16 v4, 0x40a00000    # 5.0f

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/nebulous/views/d;->a:F

    mul-float/2addr v4, v5

    add-float/2addr v2, v4

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    .line 176
    goto/16 :goto_5

    .line 182
    :pswitch_a
    const/high16 v2, 0x40a00000    # 5.0f

    const/high16 v4, 0x41700000    # 15.0f

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/nebulous/views/d;->a:F

    mul-float/2addr v4, v5

    add-float/2addr v2, v4

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    .line 183
    goto/16 :goto_5

    .line 189
    :pswitch_b
    const/high16 v2, -0x3fc00000    # -3.0f

    const/high16 v4, 0x41200000    # 10.0f

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/nebulous/views/d;->a:F

    mul-float/2addr v4, v5

    sub-float/2addr v2, v4

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    .line 190
    goto/16 :goto_5

    .line 192
    :pswitch_c
    const/high16 v2, -0x3fe00000    # -2.5f

    const/high16 v4, 0x40200000    # 2.5f

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/nebulous/views/d;->a:F

    mul-float/2addr v4, v5

    sub-float/2addr v2, v4

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    .line 193
    goto/16 :goto_5

    .line 195
    :pswitch_d
    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v4, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/nebulous/views/d;->a:F

    mul-float/2addr v4, v5

    sub-float/2addr v2, v4

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    .line 196
    goto/16 :goto_5

    .line 200
    :pswitch_e
    const/high16 v2, 0x40a00000    # 5.0f

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    .line 201
    goto/16 :goto_5

    .line 203
    :pswitch_f
    const/high16 v2, -0x3f600000    # -5.0f

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    .line 204
    goto/16 :goto_5

    .line 208
    :pswitch_10
    const/high16 v2, -0x3f400000    # -6.0f

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    .line 209
    goto/16 :goto_5

    .line 212
    :pswitch_11
    const/high16 v2, -0x3e900000    # -15.0f

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    .line 213
    goto/16 :goto_5

    .line 215
    :pswitch_12
    const/high16 v2, 0x3f000000    # 0.5f

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    .line 216
    goto/16 :goto_5

    .line 220
    :pswitch_13
    const/high16 v2, 0x3f000000    # 0.5f

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    .line 221
    goto/16 :goto_5

    .line 224
    :pswitch_14
    const/high16 v2, -0x40400000    # -1.5f

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    .line 225
    goto/16 :goto_5

    .line 229
    :pswitch_15
    const/high16 v2, -0x41000000    # -0.5f

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    .line 230
    goto/16 :goto_5

    .line 232
    :pswitch_16
    const/high16 v2, -0x40400000    # -1.5f

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    .line 233
    goto/16 :goto_5

    .line 235
    :pswitch_17
    const/high16 v2, 0x40e00000    # 7.0f

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    .line 236
    goto/16 :goto_5

    .line 239
    :pswitch_18
    const/high16 v2, -0x3f200000    # -7.0f

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    .line 240
    goto/16 :goto_5

    .line 242
    :pswitch_19
    const/high16 v2, -0x41000000    # -0.5f

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    .line 243
    goto/16 :goto_5

    .line 245
    :pswitch_1a
    const/high16 v2, 0x3fc00000    # 1.5f

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    .line 246
    goto/16 :goto_5

    .line 248
    :pswitch_1b
    const/high16 v3, 0x40400000    # 3.0f

    .line 249
    const/high16 v2, 0x40400000    # 3.0f

    .line 250
    goto/16 :goto_5

    .line 254
    :pswitch_1c
    sget-object v2, Lsoftware/simplicial/nebulous/views/d;->J:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v2

    const v4, 0x3d4ccccd    # 0.05f

    cmpg-float v2, v2, v4

    if-gez v2, :cond_22

    .line 256
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    const/high16 v4, 0x3f000000    # 0.5f

    cmpg-float v2, v2, v4

    if-gez v2, :cond_24

    .line 258
    const/high16 v2, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    .line 272
    :cond_22
    :goto_c
    const/high16 v2, 0x41200000    # 10.0f

    .line 273
    move-object/from16 v0, p7

    iget-object v4, v0, Lsoftware/simplicial/a/e;->ll:Lsoftware/simplicial/a/f;

    sget-object v5, Lsoftware/simplicial/a/f;->kx:Lsoftware/simplicial/a/f;

    if-ne v4, v5, :cond_23

    .line 274
    const/high16 v2, 0x40a00000    # 5.0f

    .line 276
    :cond_23
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    const/high16 v5, 0x3f000000    # 0.5f

    cmpg-float v4, v4, v5

    if-gez v4, :cond_27

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    .line 277
    goto/16 :goto_5

    .line 260
    :cond_24
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    const/high16 v4, 0x3f800000    # 1.0f

    cmpg-float v2, v2, v4

    if-gez v2, :cond_25

    .line 262
    const/high16 v2, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    goto :goto_c

    .line 266
    :cond_25
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    const/high16 v4, 0x40000000    # 2.0f

    cmpl-float v2, v2, v4

    if-nez v2, :cond_26

    .line 267
    sget-object v2, Lsoftware/simplicial/nebulous/views/d;->J:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v2

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float/2addr v2, v4

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    goto :goto_c

    .line 269
    :cond_26
    const/high16 v2, 0x3f000000    # 0.5f

    sget-object v4, Lsoftware/simplicial/nebulous/views/d;->J:Ljava/util/Random;

    invoke-virtual {v4}, Ljava/util/Random;->nextFloat()F

    move-result v4

    const/high16 v5, 0x3f000000    # 0.5f

    mul-float/2addr v4, v5

    add-float/2addr v2, v4

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    goto :goto_c

    .line 278
    :cond_27
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    const/high16 v5, 0x3f800000    # 1.0f

    cmpg-float v4, v4, v5

    if-gez v4, :cond_28

    .line 279
    neg-float v2, v2

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    goto/16 :goto_5

    .line 281
    :cond_28
    const/4 v2, 0x0

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    .line 282
    goto/16 :goto_5

    .line 284
    :pswitch_1d
    move/from16 v0, p14

    rem-int/lit16 v4, v0, 0x1e0

    .line 285
    const/16 v2, 0xf0

    if-ge v4, v2, :cond_29

    .line 287
    move/from16 v0, p14

    rem-int/lit16 v2, v0, 0xf0

    int-to-double v6, v2

    const-wide/high16 v8, 0x406e000000000000L    # 240.0

    div-double/2addr v6, v8

    const-wide/high16 v8, 0x4049000000000000L    # 50.0

    mul-double/2addr v6, v8

    double-to-float v2, v6

    .line 288
    int-to-float v4, v4

    const/high16 v5, 0x43700000    # 240.0f

    div-float/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    .line 296
    :goto_d
    move/from16 v0, p14

    rem-int/lit16 v4, v0, 0x3c0

    const/16 v5, 0x1e0

    if-lt v4, v5, :cond_54

    .line 298
    neg-float v2, v2

    .line 299
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    neg-float v4, v4

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    goto/16 :goto_5

    .line 292
    :cond_29
    move/from16 v0, p14

    rem-int/lit16 v2, v0, 0xf0

    rsub-int v2, v2, 0xf0

    int-to-double v6, v2

    const-wide/high16 v8, 0x406e000000000000L    # 240.0

    div-double/2addr v6, v8

    const-wide/high16 v8, 0x4049000000000000L    # 50.0

    mul-double/2addr v6, v8

    double-to-float v2, v6

    .line 293
    const/high16 v5, 0x40000000    # 2.0f

    int-to-float v4, v4

    const/high16 v6, 0x43700000    # 240.0f

    div-float/2addr v4, v6

    sub-float v4, v5, v4

    neg-float v4, v4

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    goto :goto_d

    .line 314
    :pswitch_1e
    move/from16 v0, p14

    rem-int/lit16 v2, v0, 0x1e0

    const/16 v4, 0xf0

    if-ge v2, v4, :cond_2a

    .line 315
    move/from16 v0, p14

    rem-int/lit16 v2, v0, 0xf0

    int-to-double v4, v2

    const-wide/high16 v6, 0x406e000000000000L    # 240.0

    div-double/2addr v4, v6

    const-wide/high16 v6, 0x4049000000000000L    # 50.0

    mul-double/2addr v4, v6

    double-to-float v2, v4

    .line 318
    :goto_e
    const/high16 v4, 0x42480000    # 50.0f

    div-float v4, v2, v4

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    .line 319
    move-object/from16 v0, p7

    iget-object v4, v0, Lsoftware/simplicial/a/e;->ll:Lsoftware/simplicial/a/f;

    sget-object v5, Lsoftware/simplicial/a/f;->lc:Lsoftware/simplicial/a/f;

    if-ne v4, v5, :cond_54

    .line 320
    neg-float v2, v2

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    goto/16 :goto_5

    .line 317
    :cond_2a
    move/from16 v0, p14

    rem-int/lit16 v2, v0, 0xf0

    rsub-int v2, v2, 0xf0

    int-to-double v4, v2

    const-wide/high16 v6, 0x406e000000000000L    # 240.0

    div-double/2addr v4, v6

    const-wide/high16 v6, 0x4049000000000000L    # 50.0

    mul-double/2addr v4, v6

    double-to-float v2, v4

    goto :goto_e

    .line 326
    :pswitch_1f
    move/from16 v0, p14

    int-to-float v2, v0

    const/high16 v4, 0x43fa0000    # 500.0f

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/nebulous/views/d;->a:F

    mul-float/2addr v4, v5

    add-float/2addr v2, v4

    float-to-int v2, v2

    rem-int/lit16 v2, v2, 0x1f4

    .line 327
    rsub-int v4, v2, 0x1f4

    int-to-double v4, v4

    const-wide v6, 0x407f400000000000L    # 500.0

    div-double/2addr v4, v6

    const-wide/high16 v6, 0x404e000000000000L    # 60.0

    mul-double/2addr v4, v6

    double-to-float v4, v4

    .line 328
    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    const/4 v6, 0x0

    cmpg-float v5, v5, v6

    if-gez v5, :cond_2b

    .line 329
    neg-float v4, v4

    .line 330
    :cond_2b
    if-nez v2, :cond_10

    .line 331
    sget-object v2, Lsoftware/simplicial/nebulous/views/d;->J:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextBoolean()Z

    move-result v2

    if-eqz v2, :cond_2c

    const/high16 v2, 0x3f800000    # 1.0f

    :goto_f
    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    move v2, v3

    move v3, v4

    goto/16 :goto_5

    :cond_2c
    const/high16 v2, -0x40800000    # -1.0f

    goto :goto_f

    .line 335
    :pswitch_20
    move/from16 v0, p14

    rem-int/lit16 v2, v0, 0x1e0

    .line 336
    const/16 v4, 0xf0

    if-ge v2, v4, :cond_2e

    .line 337
    move/from16 v0, p14

    rem-int/lit16 v2, v0, 0xf0

    int-to-double v4, v2

    const-wide/high16 v6, 0x406e000000000000L    # 240.0

    div-double/2addr v4, v6

    const-wide/high16 v6, 0x4049000000000000L    # 50.0

    mul-double/2addr v4, v6

    double-to-float v2, v4

    .line 341
    :goto_10
    move/from16 v0, p14

    rem-int/lit16 v4, v0, 0x3c0

    const/16 v5, 0x1e0

    if-lt v4, v5, :cond_2d

    .line 342
    neg-float v2, v2

    .line 344
    :cond_2d
    sget-object v4, Lsoftware/simplicial/a/e;->iy:Lsoftware/simplicial/a/e;

    move-object/from16 v0, p7

    if-eq v0, v4, :cond_54

    .line 345
    const/high16 v4, 0x42480000    # 50.0f

    div-float v4, v2, v4

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    goto/16 :goto_5

    .line 339
    :cond_2e
    move/from16 v0, p14

    rem-int/lit16 v2, v0, 0xf0

    rsub-int v2, v2, 0xf0

    int-to-double v4, v2

    const-wide/high16 v6, 0x406e000000000000L    # 240.0

    div-double/2addr v4, v6

    const-wide/high16 v6, 0x4049000000000000L    # 50.0

    mul-double/2addr v4, v6

    double-to-float v2, v4

    goto :goto_10

    .line 349
    :pswitch_21
    const/high16 v2, 0x40800000    # 4.0f

    move v3, v4

    .line 350
    goto/16 :goto_5

    .line 352
    :pswitch_22
    const v2, 0x40551eb8    # 3.33f

    .line 353
    const v3, 0x4038f5c3    # 2.89f

    .line 354
    goto/16 :goto_5

    .line 356
    :pswitch_23
    const v2, 0x411fd70a    # 9.99f

    .line 357
    const v3, 0x418ab852    # 17.34f

    .line 358
    goto/16 :goto_5

    .line 366
    :pswitch_24
    const/high16 v2, 0x40a00000    # 5.0f

    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    .line 367
    goto/16 :goto_5

    .line 369
    :pswitch_25
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v5, 0x3d4ccccd    # 0.05f

    cmpl-float v2, v2, v5

    if-lez v2, :cond_2f

    .line 370
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    const/high16 v5, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    sub-float/2addr v5, v6

    const/high16 v6, 0x41200000    # 10.0f

    div-float/2addr v5, v6

    add-float/2addr v2, v5

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    move v2, v3

    move v3, v4

    goto/16 :goto_5

    .line 371
    :cond_2f
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    sub-float/2addr v2, v5

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v5, 0x3a83126f    # 0.001f

    cmpl-float v2, v2, v5

    if-lez v2, :cond_10

    .line 372
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    sub-float/2addr v5, v6

    const/high16 v6, 0x42c80000    # 100.0f

    div-float/2addr v5, v6

    add-float/2addr v2, v5

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    goto/16 :goto_4

    .line 381
    :pswitch_26
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/nebulous/views/d;->p:F

    const/high16 v4, 0x42c80000    # 100.0f

    div-float/2addr v2, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->p:F

    .line 434
    :pswitch_27
    if-eqz p12, :cond_31

    .line 435
    move/from16 v0, p13

    move-object/from16 v1, p0

    iput v0, v1, Lsoftware/simplicial/nebulous/views/d;->l:F

    .line 439
    :goto_11
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    float-to-double v2, v2

    const-wide v4, 0x3ff921fb54442d18L    # 1.5707963267948966

    add-double/2addr v2, v4

    double-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    .line 440
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gez v2, :cond_30

    .line 441
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    float-to-double v2, v2

    const-wide v4, 0x401921fb54442d18L    # 6.283185307179586

    add-double/2addr v2, v4

    double-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    .line 442
    :cond_30
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    float-to-double v2, v2

    const-wide v4, 0x401921fb54442d18L    # 6.283185307179586

    cmpl-double v2, v2, v4

    if-lez v2, :cond_11

    .line 443
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    float-to-double v2, v2

    const-wide v4, 0x401921fb54442d18L    # 6.283185307179586

    sub-double/2addr v2, v4

    double-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    goto/16 :goto_6

    .line 437
    :cond_31
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->C:F

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    goto :goto_11

    .line 446
    :pswitch_28
    if-eqz p12, :cond_34

    .line 447
    move/from16 v0, p13

    move-object/from16 v1, p0

    iput v0, v1, Lsoftware/simplicial/nebulous/views/d;->l:F

    .line 451
    :goto_12
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    float-to-double v2, v2

    const-wide v4, 0x3ff921fb54442d18L    # 1.5707963267948966

    add-double/2addr v2, v4

    double-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    .line 452
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gez v2, :cond_32

    .line 453
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    float-to-double v2, v2

    const-wide v4, 0x401921fb54442d18L    # 6.283185307179586

    add-double/2addr v2, v4

    double-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    .line 454
    :cond_32
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    float-to-double v2, v2

    const-wide v4, 0x401921fb54442d18L    # 6.283185307179586

    cmpl-double v2, v2, v4

    if-lez v2, :cond_33

    .line 455
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    float-to-double v2, v2

    const-wide v4, 0x401921fb54442d18L    # 6.283185307179586

    sub-double/2addr v2, v4

    double-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    .line 457
    :cond_33
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->a:F

    const/high16 v3, 0x42f00000    # 120.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    add-int v2, v2, p14

    rem-int/lit8 v2, v2, 0x78

    .line 458
    const-wide/high16 v4, 0x3fd0000000000000L    # 0.25

    int-to-double v2, v2

    const-wide/high16 v6, 0x405e000000000000L    # 120.0

    div-double/2addr v2, v6

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v2, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    mul-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    add-double/2addr v2, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v6

    add-double/2addr v2, v4

    double-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->p:F

    goto/16 :goto_6

    .line 449
    :cond_34
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->C:F

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    goto :goto_12

    .line 462
    :pswitch_29
    sget-object v2, Lsoftware/simplicial/nebulous/views/d;->J:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v2

    float-to-double v2, v2

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v2, v4

    double-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    goto/16 :goto_6

    .line 465
    :pswitch_2a
    sget-object v2, Lsoftware/simplicial/nebulous/views/d;->J:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v2

    float-to-double v2, v2

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v2, v4

    double-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    .line 466
    sget-object v2, Lsoftware/simplicial/nebulous/views/d;->J:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v2

    float-to-double v2, v2

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v2, v4

    double-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->p:F

    .line 467
    sget-object v2, Lsoftware/simplicial/nebulous/views/d;->J:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v2

    const v3, 0x3d4ccccd    # 0.05f

    mul-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    goto/16 :goto_6

    .line 470
    :pswitch_2b
    rem-int/lit8 v2, p14, 0x78

    int-to-double v2, v2

    const-wide/high16 v4, 0x405e000000000000L    # 120.0

    div-double/2addr v2, v4

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v2, v4

    double-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    goto/16 :goto_6

    .line 473
    :pswitch_2c
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    rem-int/lit8 v4, p14, 0x32

    int-to-double v4, v4

    const-wide/high16 v6, 0x4049000000000000L    # 50.0

    div-double/2addr v4, v6

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    const-wide/high16 v6, 0x4008000000000000L    # 3.0

    div-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->p:F

    goto/16 :goto_6

    .line 477
    :pswitch_2d
    const-wide v2, 0x3fe999999999999aL    # 0.8

    rem-int/lit8 v4, p14, 0x1e

    int-to-double v4, v4

    const-wide/high16 v6, 0x403e000000000000L    # 30.0

    div-double/2addr v4, v6

    const-wide v6, 0x3fb999999999999aL    # 0.1

    rem-int/lit8 v8, p14, 0x6

    int-to-double v8, v8

    const-wide/high16 v10, 0x4018000000000000L    # 6.0

    div-double/2addr v8, v10

    const-wide v10, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    const-wide/high16 v6, 0x4008000000000000L    # 3.0

    div-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->p:F

    goto/16 :goto_6

    .line 480
    :pswitch_2e
    const-wide v4, 0x3fe999999999999aL    # 0.8

    rem-int/lit8 v2, p14, 0x1e

    int-to-double v6, v2

    const-wide/high16 v8, 0x403e000000000000L    # 30.0

    div-double/2addr v6, v8

    const-wide v8, 0x3fb999999999999aL    # 0.1

    rem-int/lit8 v2, p14, 0x6

    int-to-double v10, v2

    const-wide/high16 v12, 0x4018000000000000L    # 6.0

    div-double/2addr v10, v12

    const-wide v12, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    const-wide/high16 v8, 0x4008000000000000L    # 3.0

    div-double/2addr v6, v8

    add-double/2addr v4, v6

    double-to-float v2, v4

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->p:F

    .line 481
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    const/high16 v4, 0x42c80000    # 100.0f

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    goto/16 :goto_6

    .line 488
    :pswitch_2f
    const/high16 v2, 0x3f800000    # 1.0f

    rem-int/lit8 v4, p14, 0xf

    int-to-double v4, v4

    const-wide/high16 v6, 0x4034000000000000L    # 20.0

    div-double/2addr v4, v6

    double-to-float v4, v4

    sub-float/2addr v2, v4

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->p:F

    .line 489
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    const/high16 v4, 0x42c80000    # 100.0f

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    goto/16 :goto_6

    .line 493
    :pswitch_30
    const/high16 v2, 0x3f000000    # 0.5f

    rem-int/lit8 v4, p14, 0xa

    int-to-double v4, v4

    const-wide/high16 v6, 0x4034000000000000L    # 20.0

    div-double/2addr v4, v6

    double-to-float v4, v4

    add-float/2addr v2, v4

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->p:F

    .line 494
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    const/high16 v4, 0x42c80000    # 100.0f

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    goto/16 :goto_6

    .line 498
    :pswitch_31
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->a:F

    const/high16 v3, 0x42d80000    # 108.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    add-int v2, v2, p14

    rem-int/lit8 v2, v2, 0x6c

    add-int/lit8 v2, v2, 0xc

    int-to-float v2, v2

    const/high16 v3, 0x42700000    # 60.0f

    div-float/2addr v2, v3

    .line 499
    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v2, v4}, Ljava/lang/Math;->min(FF)F

    move-result v4

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Lsoftware/simplicial/nebulous/views/d;->p:F

    .line 500
    const v3, 0x3e4ccccd    # 0.2f

    cmpl-float v3, v2, v3

    if-nez v3, :cond_35

    .line 502
    sget-object v3, Lsoftware/simplicial/nebulous/views/d;->J:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextFloat()F

    move-result v3

    float-to-double v4, v3

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    mul-double/2addr v4, v6

    double-to-float v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    .line 503
    move-object/from16 v0, p7

    iget-object v3, v0, Lsoftware/simplicial/a/e;->ll:Lsoftware/simplicial/a/f;

    sget-object v4, Lsoftware/simplicial/a/f;->ga:Lsoftware/simplicial/a/f;

    if-ne v3, v4, :cond_36

    .line 505
    sget-object v3, Lsoftware/simplicial/nebulous/views/d;->J:Ljava/util/Random;

    const/16 v4, 0xce

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x32

    .line 506
    sget-object v4, Lsoftware/simplicial/nebulous/views/d;->J:Ljava/util/Random;

    const/16 v5, 0xce

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x32

    .line 507
    sget-object v5, Lsoftware/simplicial/nebulous/views/d;->J:Ljava/util/Random;

    const/16 v6, 0xce

    invoke-virtual {v5, v6}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x32

    .line 508
    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/nebulous/views/d;->o:[F

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    invoke-static {v6, v3}, Lsoftware/simplicial/nebulous/views/GameView;->a([FI)V

    .line 515
    :cond_35
    :goto_13
    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v3, v2, v3

    if-lez v3, :cond_11

    .line 516
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/4 v4, 0x3

    const/high16 v5, 0x40000000    # 2.0f

    sub-float v2, v5, v2

    aput v2, v3, v4

    goto/16 :goto_6

    .line 512
    :cond_36
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/16 v4, 0xff

    const/16 v5, 0xff

    const/16 v6, 0xff

    invoke-static {v4, v5, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-static {v3, v4}, Lsoftware/simplicial/nebulous/views/GameView;->a([FI)V

    goto :goto_13

    .line 519
    :pswitch_32
    move/from16 v0, p14

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/nebulous/views/d;->a:F

    const/high16 v4, 0x41f00000    # 30.0f

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    const/high16 v3, 0x41f00000    # 30.0f

    rem-float/2addr v2, v3

    const/high16 v3, 0x41700000    # 15.0f

    cmpg-float v2, v2, v3

    if-gez v2, :cond_37

    .line 520
    const v2, -0x411a3706

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    goto/16 :goto_6

    .line 522
    :cond_37
    const v2, 0x3ee5c8fa

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    goto/16 :goto_6

    .line 525
    :pswitch_33
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->C:F

    float-to-double v2, v2

    const-wide v4, 0x3ff921fb54442d18L    # 1.5707963267948966

    cmpl-double v2, v2, v4

    if-lez v2, :cond_38

    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->C:F

    float-to-double v2, v2

    const-wide v4, 0x4012d97c7f3321d2L    # 4.71238898038469

    cmpg-double v2, v2, v4

    if-gez v2, :cond_38

    const-wide v2, -0x4006de04abbbd2e8L    # -1.5707963267948966

    :goto_14
    double-to-float v2, v2

    .line 526
    const/high16 v3, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    sub-float/2addr v3, v4

    mul-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    goto/16 :goto_6

    .line 525
    :cond_38
    const-wide v2, 0x3ff921fb54442d18L    # 1.5707963267948966

    goto :goto_14

    .line 529
    :pswitch_34
    rem-int/lit8 v2, p14, 0x3c

    int-to-float v2, v2

    const/high16 v3, 0x42700000    # 60.0f

    div-float/2addr v2, v3

    float-to-double v2, v2

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v2, v4

    double-to-float v2, v2

    .line 530
    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    double-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    goto/16 :goto_6

    .line 533
    :pswitch_35
    if-eqz p12, :cond_3a

    .line 534
    move/from16 v0, p13

    move-object/from16 v1, p0

    iput v0, v1, Lsoftware/simplicial/nebulous/views/d;->l:F

    .line 538
    :goto_15
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->a:F

    const/high16 v3, 0x42700000    # 60.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    add-int v2, v2, p14

    rem-int/lit8 v2, v2, 0x3c

    int-to-float v2, v2

    const/high16 v3, 0x41f00000    # 30.0f

    div-float/2addr v2, v3

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    .line 539
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gez v2, :cond_3b

    .line 540
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v3, v4

    const/high16 v4, 0x3f800000    # 1.0f

    add-float/2addr v3, v4

    const v4, 0x3f59999a    # 0.85f

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    .line 544
    :goto_16
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    float-to-double v2, v2

    const-wide v4, 0x3ff921fb54442d18L    # 1.5707963267948966

    add-double/2addr v2, v4

    double-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    .line 545
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gez v2, :cond_39

    .line 546
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    float-to-double v2, v2

    const-wide v4, 0x401921fb54442d18L    # 6.283185307179586

    add-double/2addr v2, v4

    double-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    .line 547
    :cond_39
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    float-to-double v2, v2

    const-wide v4, 0x401921fb54442d18L    # 6.283185307179586

    cmpl-double v2, v2, v4

    if-lez v2, :cond_11

    .line 548
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    float-to-double v2, v2

    const-wide v4, 0x401921fb54442d18L    # 6.283185307179586

    sub-double/2addr v2, v4

    double-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    goto/16 :goto_6

    .line 536
    :cond_3a
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->C:F

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    goto/16 :goto_15

    .line 542
    :cond_3b
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    const/high16 v3, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    const/high16 v5, 0x40000000    # 2.0f

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    const v4, 0x3f59999a    # 0.85f

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    goto :goto_16

    .line 552
    :pswitch_36
    rem-int/lit8 v2, p14, 0x28

    int-to-float v2, v2

    const/high16 v3, 0x42200000    # 40.0f

    div-float/2addr v2, v3

    float-to-double v2, v2

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v2, v4

    double-to-float v2, v2

    .line 553
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    float-to-double v4, v2

    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->a:F

    float-to-double v6, v2

    const-wide v8, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v6, v8

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v2, v4

    mul-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    goto/16 :goto_6

    .line 556
    :pswitch_37
    move/from16 v0, p14

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/nebulous/views/d;->a:F

    const/high16 v4, 0x42f00000    # 120.0f

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    const/high16 v3, 0x42f00000    # 120.0f

    rem-float/2addr v2, v3

    const/high16 v3, 0x42700000    # 60.0f

    cmpg-float v2, v2, v3

    if-gez v2, :cond_3d

    .line 557
    rem-int/lit8 v2, p14, 0x6

    const/4 v3, 0x3

    if-ge v2, v3, :cond_3c

    const v2, -0x41e66666    # -0.15f

    :goto_17
    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    goto/16 :goto_6

    :cond_3c
    const v2, 0x3e19999a    # 0.15f

    goto :goto_17

    .line 559
    :cond_3d
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    goto/16 :goto_6

    .line 562
    :pswitch_38
    rem-int/lit8 v2, p14, 0x6

    const/4 v3, 0x3

    if-ge v2, v3, :cond_3e

    const v2, -0x41e66666    # -0.15f

    :goto_18
    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    goto/16 :goto_6

    :cond_3e
    const v2, 0x3e19999a    # 0.15f

    goto :goto_18

    .line 565
    :pswitch_39
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    const/high16 v3, 0x40000000    # 2.0f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_3f

    .line 566
    const/high16 v2, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    .line 568
    :cond_3f
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    const/high16 v3, 0x40000000    # 2.0f

    cmpl-float v2, v2, v3

    if-nez v2, :cond_40

    .line 570
    sget-object v2, Lsoftware/simplicial/nebulous/views/d;->J:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v2

    const v3, 0x3ca3d70a    # 0.02f

    cmpg-float v2, v2, v3

    if-gez v2, :cond_11

    .line 571
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    const v3, 0x3e2aaaab

    sub-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    goto/16 :goto_6

    .line 575
    :cond_40
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    const v3, 0x3e2aaaab

    sub-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    .line 576
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_11

    .line 577
    const/high16 v2, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->l:F

    goto/16 :goto_6

    .line 594
    :sswitch_0
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    const v3, 0x3d4ccccd    # 0.05f

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    .line 595
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_12

    .line 597
    const/high16 v2, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    .line 598
    move-object/from16 v0, p7

    iget-object v2, v0, Lsoftware/simplicial/a/e;->ll:Lsoftware/simplicial/a/f;

    sget-object v3, Lsoftware/simplicial/a/f;->kt:Lsoftware/simplicial/a/f;

    if-ne v2, v3, :cond_12

    .line 599
    sget-object v2, Lsoftware/simplicial/nebulous/views/d;->J:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v2

    const/high16 v3, 0x3f000000    # 0.5f

    sub-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->p:F

    goto/16 :goto_7

    .line 603
    :sswitch_1
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->a:F

    const/high16 v3, 0x42700000    # 60.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    add-int v2, v2, p14

    rem-int/lit8 v2, v2, 0x3c

    .line 604
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    int-to-double v2, v2

    const-wide/high16 v8, 0x404e000000000000L    # 60.0

    div-double/2addr v2, v8

    const-wide v8, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v2, v8

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    mul-double/2addr v2, v8

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v8

    add-double/2addr v2, v6

    mul-double/2addr v2, v4

    double-to-float v2, v2

    .line 605
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    float-to-double v4, v3

    float-to-double v2, v2

    mul-double v2, v2, p15

    add-double/2addr v2, v4

    double-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    .line 606
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_12

    .line 607
    const/high16 v2, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    goto/16 :goto_7

    .line 614
    :sswitch_2
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    const v3, 0x3cf5c28f    # 0.03f

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    .line 615
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_12

    .line 616
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    const/high16 v3, 0x40000000    # 2.0f

    sub-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    goto/16 :goto_7

    .line 619
    :sswitch_3
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    const v3, 0x3ca3d70a    # 0.02f

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    .line 620
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_12

    .line 621
    const/high16 v2, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    goto/16 :goto_7

    .line 627
    :sswitch_4
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    const v3, 0x3c23d70a    # 0.01f

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    .line 628
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_12

    .line 629
    const/high16 v2, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    goto/16 :goto_7

    .line 632
    :sswitch_5
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    const v3, 0x3dcccccd    # 0.1f

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    .line 633
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_12

    .line 634
    const/high16 v2, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    goto/16 :goto_7

    .line 637
    :sswitch_6
    sget-object v2, Lsoftware/simplicial/nebulous/views/d;->J:Ljava/util/Random;

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    goto/16 :goto_7

    .line 673
    :sswitch_7
    sget-object v2, Lsoftware/simplicial/nebulous/views/d;->J:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v2

    const v3, 0x3d888889

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1b

    .line 674
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lsoftware/simplicial/nebulous/views/d;->m:Z

    if-nez v2, :cond_41

    const/4 v2, 0x1

    :goto_19
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/nebulous/views/d;->m:Z

    goto/16 :goto_8

    :cond_41
    const/4 v2, 0x0

    goto :goto_19

    .line 679
    :sswitch_8
    rem-int/lit8 v2, p14, 0x1e

    if-nez v2, :cond_1b

    .line 680
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lsoftware/simplicial/nebulous/views/d;->m:Z

    if-nez v2, :cond_42

    const/4 v2, 0x1

    :goto_1a
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/nebulous/views/d;->m:Z

    goto/16 :goto_8

    :cond_42
    const/4 v2, 0x0

    goto :goto_1a

    .line 683
    :sswitch_9
    rem-int/lit8 v2, p14, 0x3c

    if-nez v2, :cond_1b

    .line 684
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lsoftware/simplicial/nebulous/views/d;->m:Z

    if-nez v2, :cond_43

    const/4 v2, 0x1

    :goto_1b
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/nebulous/views/d;->m:Z

    goto/16 :goto_8

    :cond_43
    const/4 v2, 0x0

    goto :goto_1b

    .line 687
    :sswitch_a
    rem-int/lit8 v2, p14, 0x5

    if-nez v2, :cond_1b

    .line 688
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lsoftware/simplicial/nebulous/views/d;->m:Z

    if-nez v2, :cond_44

    const/4 v2, 0x1

    :goto_1c
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/nebulous/views/d;->m:Z

    goto/16 :goto_8

    :cond_44
    const/4 v2, 0x0

    goto :goto_1c

    .line 735
    :sswitch_b
    sget-object v2, Lsoftware/simplicial/a/e;->dh:Lsoftware/simplicial/a/e;

    move-object/from16 v0, p7

    if-ne v0, v2, :cond_4a

    const v2, 0x3c088889

    .line 736
    :goto_1d
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/views/d;->n:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-nez v3, :cond_45

    .line 737
    const/high16 v2, 0x3f800000    # 1.0f

    .line 739
    :cond_45
    sget-object v3, Lsoftware/simplicial/nebulous/views/d;->J:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextFloat()F

    move-result v3

    cmpg-float v2, v3, v2

    if-gez v2, :cond_46

    .line 741
    sget-object v2, Lsoftware/simplicial/nebulous/views/d;->J:Ljava/util/Random;

    const/16 v3, 0xce

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x32

    .line 742
    sget-object v3, Lsoftware/simplicial/nebulous/views/d;->J:Ljava/util/Random;

    const/16 v4, 0xce

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x32

    .line 743
    sget-object v4, Lsoftware/simplicial/nebulous/views/d;->J:Ljava/util/Random;

    const/16 v5, 0xce

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x32

    .line 744
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/d;->n:[F

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v5, v2}, Lsoftware/simplicial/nebulous/views/GameView;->a([FI)V

    .line 746
    :cond_46
    const/4 v2, 0x0

    :goto_1e
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/views/d;->o:[F

    array-length v3, v3

    if-ge v2, v3, :cond_1c

    .line 748
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/views/d;->o:[F

    aget v3, v3, v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/d;->n:[F

    aget v4, v4, v2

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const v4, 0x3c23d70a    # 0.01f

    cmpl-float v3, v3, v4

    if-lez v3, :cond_47

    .line 749
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/views/d;->o:[F

    aget v4, v3, v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/d;->n:[F

    aget v5, v5, v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/nebulous/views/d;->o:[F

    aget v6, v6, v2

    sub-float/2addr v5, v6

    const v6, 0x3d4ccccd    # 0.05f

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    aput v4, v3, v2

    .line 750
    :cond_47
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/views/d;->o:[F

    aget v3, v3, v2

    const/4 v4, 0x0

    cmpg-float v3, v3, v4

    if-gez v3, :cond_48

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/4 v4, 0x0

    aput v4, v3, v2

    .line 751
    :cond_48
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/views/d;->o:[F

    aget v3, v3, v2

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v3, v3, v4

    if-lez v3, :cond_49

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/high16 v4, 0x3f800000    # 1.0f

    aput v4, v3, v2

    .line 746
    :cond_49
    add-int/lit8 v2, v2, 0x1

    goto :goto_1e

    .line 735
    :cond_4a
    const v2, 0x3c888889

    goto/16 :goto_1d

    .line 755
    :sswitch_c
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/d;->G:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 756
    const/4 v3, 0x0

    .line 757
    const/high16 v4, 0x437f0000    # 255.0f

    mul-float/2addr v2, v4

    float-to-int v2, v2

    .line 758
    const/16 v4, 0xff

    .line 759
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/d;->o:[F

    invoke-static {v3, v2, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v5, v2}, Lsoftware/simplicial/nebulous/views/GameView;->a([FI)V

    goto/16 :goto_9

    .line 762
    :sswitch_d
    sget-object v2, Lsoftware/simplicial/nebulous/views/d;->J:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v2

    const v3, 0x3d888889

    cmpg-float v2, v2, v3

    if-gez v2, :cond_4b

    .line 764
    sget-object v2, Lsoftware/simplicial/nebulous/views/d;->J:Ljava/util/Random;

    const/16 v3, 0x9c

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x64

    .line 765
    sget-object v3, Lsoftware/simplicial/nebulous/views/d;->J:Ljava/util/Random;

    const/16 v4, 0x9c

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x64

    .line 766
    sget-object v4, Lsoftware/simplicial/nebulous/views/d;->J:Ljava/util/Random;

    const/16 v5, 0x9c

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x64

    .line 767
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/d;->n:[F

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-static {v5, v2}, Lsoftware/simplicial/nebulous/views/GameView;->a([FI)V

    .line 768
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/d;->n:[F

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/4 v5, 0x0

    const/4 v6, 0x4

    invoke-static {v2, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto/16 :goto_9

    .line 772
    :cond_4b
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/4 v3, 0x3

    aget v2, v2, v3

    float-to-double v2, v2

    const-wide v4, 0x3fb999999999999aL    # 0.1

    cmpl-double v2, v2, v4

    if-lez v2, :cond_1c

    .line 773
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/4 v3, 0x3

    aget v4, v2, v3

    const v5, 0x3ca3d70b    # 0.020000001f

    sub-float/2addr v4, v5

    aput v4, v2, v3

    goto/16 :goto_9

    .line 813
    :cond_4c
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/d;->e:[F

    sub-int v3, v9, v7

    add-int/lit8 v3, v3, 0x1

    invoke-static {v10, v7, v2, v7, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 814
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/d;->f:[F

    sub-int v3, v9, v7

    add-int/lit8 v3, v3, 0x1

    invoke-static {v11, v7, v2, v7, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 815
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/d;->i:[F

    sub-int v3, v9, v7

    add-int/lit8 v3, v3, 0x1

    invoke-static {v12, v7, v2, v7, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 816
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/d;->j:[F

    sub-int v3, v9, v7

    add-int/lit8 v3, v3, 0x1

    invoke-static {v13, v7, v2, v7, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 820
    :cond_4d
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lsoftware/simplicial/nebulous/views/j;->c:Z

    if-eqz v2, :cond_4f

    if-eqz p11, :cond_4f

    .line 822
    const/4 v2, 0x0

    :goto_1f
    if-gt v2, v8, :cond_4e

    .line 824
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/views/d;->e:[F

    add-int/lit8 v4, v2, 0x1

    const/4 v5, 0x0

    aput v5, v3, v4

    .line 825
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/views/d;->f:[F

    add-int/lit8 v4, v2, 0x1

    const/4 v5, 0x0

    aput v5, v3, v4

    .line 826
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/views/d;->i:[F

    add-int/lit8 v4, v2, 0x1

    const/4 v5, 0x0

    aput v5, v3, v4

    .line 827
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/views/d;->j:[F

    add-int/lit8 v4, v2, 0x1

    const/4 v5, 0x0

    aput v5, v3, v4

    .line 822
    add-int/lit8 v2, v2, 0x1

    goto :goto_1f

    .line 829
    :cond_4e
    const/4 v2, 0x0

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lsoftware/simplicial/nebulous/views/j;->c:Z

    .line 830
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/nebulous/views/d;->k:Z

    .line 834
    :cond_4f
    const/4 v2, 0x0

    :goto_20
    if-gt v2, v8, :cond_50

    .line 836
    const-wide v4, 0x401921fb54442d18L    # 6.283185307179586

    int-to-double v6, v2

    mul-double/2addr v4, v6

    int-to-double v6, v8

    div-double/2addr v4, v6

    .line 838
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/views/d;->g:[F

    add-int/lit8 v6, v2, 0x1

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    const-wide v12, 0x3ff051eb851eb852L    # 1.02

    const-wide v14, 0x3f947ae147ae147bL    # 0.02

    move/from16 v0, p6

    int-to-double v0, v0

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    const-wide/high16 v16, 0x4024000000000000L    # 10.0

    div-double v14, v14, v16

    sub-double/2addr v12, v14

    mul-double/2addr v10, v12

    double-to-float v7, v10

    aput v7, v3, v6

    .line 839
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/views/d;->h:[F

    add-int/lit8 v6, v2, 0x1

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    const-wide v10, 0x3ff051eb851eb852L    # 1.02

    const-wide v12, 0x3f947ae147ae147bL    # 0.02

    move/from16 v0, p6

    int-to-double v14, v0

    mul-double/2addr v12, v14

    const-wide/high16 v14, 0x4024000000000000L    # 10.0

    div-double/2addr v12, v14

    sub-double/2addr v10, v12

    mul-double/2addr v4, v10

    double-to-float v4, v4

    aput v4, v3, v6

    .line 834
    add-int/lit8 v2, v2, 0x1

    goto :goto_20

    .line 843
    :cond_50
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lsoftware/simplicial/nebulous/views/d;->k:Z

    if-nez v2, :cond_51

    if-nez p11, :cond_53

    .line 845
    :cond_51
    const/4 v2, 0x0

    :goto_21
    if-gt v2, v8, :cond_52

    .line 847
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/views/d;->e:[F

    add-int/lit8 v4, v2, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/d;->g:[F

    add-int/lit8 v6, v2, 0x1

    aget v5, v5, v6

    aput v5, v3, v4

    .line 848
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/views/d;->f:[F

    add-int/lit8 v4, v2, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/d;->h:[F

    add-int/lit8 v6, v2, 0x1

    aget v5, v5, v6

    aput v5, v3, v4

    .line 849
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/views/d;->i:[F

    add-int/lit8 v4, v2, 0x1

    const/4 v5, 0x0

    aput v5, v3, v4

    .line 850
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/views/d;->j:[F

    add-int/lit8 v4, v2, 0x1

    const/4 v5, 0x0

    aput v5, v3, v4

    .line 845
    add-int/lit8 v2, v2, 0x1

    goto :goto_21

    .line 852
    :cond_52
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/nebulous/views/d;->k:Z

    .line 856
    :cond_53
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/d;->g:[F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput v4, v2, v3

    .line 857
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/d;->h:[F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput v4, v2, v3

    .line 858
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/d;->g:[F

    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/nebulous/views/d;->d:I

    add-int/lit8 v3, v3, -0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/d;->g:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    aput v4, v2, v3

    .line 859
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/d;->h:[F

    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/nebulous/views/d;->d:I

    add-int/lit8 v3, v3, -0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/d;->h:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    aput v4, v2, v3

    .line 860
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/d;->e:[F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput v4, v2, v3

    .line 861
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/d;->f:[F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput v4, v2, v3

    .line 862
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/d;->e:[F

    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/nebulous/views/d;->d:I

    add-int/lit8 v3, v3, -0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/d;->e:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    aput v4, v2, v3

    .line 863
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/d;->f:[F

    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/nebulous/views/d;->d:I

    add-int/lit8 v3, v3, -0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/d;->f:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    aput v4, v2, v3

    goto/16 :goto_3

    :cond_54
    move/from16 v18, v3

    move v3, v2

    move/from16 v2, v18

    goto/16 :goto_5

    .line 140
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_11
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_13
        :pswitch_13
        :pswitch_14
        :pswitch_14
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1c
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1e
        :pswitch_1e
        :pswitch_1e
        :pswitch_1e
        :pswitch_1e
        :pswitch_1e
        :pswitch_1e
        :pswitch_1e
        :pswitch_1e
        :pswitch_1e
        :pswitch_1e
        :pswitch_1f
        :pswitch_1f
        :pswitch_1f
        :pswitch_1f
        :pswitch_20
        :pswitch_20
        :pswitch_21
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_24
        :pswitch_24
        :pswitch_24
        :pswitch_24
        :pswitch_24
        :pswitch_24
        :pswitch_25
    .end packed-switch

    .line 378
    :pswitch_data_1
    .packed-switch 0xd
        :pswitch_26
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2e
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2f
        :pswitch_2f
        :pswitch_30
        :pswitch_2f
        :pswitch_2f
        :pswitch_30
        :pswitch_2f
        :pswitch_36
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2d
        :pswitch_31
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_37
        :pswitch_38
        :pswitch_39
    .end packed-switch

    .line 587
    :sswitch_data_0
    .sparse-switch
        0x19 -> :sswitch_4
        0x4c -> :sswitch_6
        0x6a -> :sswitch_0
        0x6e -> :sswitch_0
        0x74 -> :sswitch_5
        0x82 -> :sswitch_2
        0x83 -> :sswitch_0
        0x84 -> :sswitch_2
        0x87 -> :sswitch_2
        0x89 -> :sswitch_0
        0x8b -> :sswitch_1
        0x8d -> :sswitch_3
        0x98 -> :sswitch_0
        0x9e -> :sswitch_2
        0x9f -> :sswitch_2
        0xa0 -> :sswitch_4
        0xa1 -> :sswitch_4
        0xa2 -> :sswitch_4
    .end sparse-switch

    .line 669
    :sswitch_data_1
    .sparse-switch
        0x59 -> :sswitch_8
        0x73 -> :sswitch_a
        0x76 -> :sswitch_9
        0x86 -> :sswitch_8
        0xa3 -> :sswitch_7
        0xa4 -> :sswitch_7
        0xa5 -> :sswitch_8
    .end sparse-switch

    .line 693
    :sswitch_data_2
    .sparse-switch
        0x22 -> :sswitch_b
        0x23 -> :sswitch_b
        0x27 -> :sswitch_b
        0x30 -> :sswitch_b
        0x3a -> :sswitch_b
        0x3e -> :sswitch_b
        0x46 -> :sswitch_b
        0x47 -> :sswitch_b
        0x64 -> :sswitch_b
        0x66 -> :sswitch_b
        0x68 -> :sswitch_b
        0x6e -> :sswitch_c
        0x74 -> :sswitch_b
        0x75 -> :sswitch_b
        0x77 -> :sswitch_b
        0x7a -> :sswitch_b
        0x7b -> :sswitch_b
        0x7f -> :sswitch_b
        0x81 -> :sswitch_b
        0x84 -> :sswitch_b
        0x85 -> :sswitch_b
        0x99 -> :sswitch_b
        0x9a -> :sswitch_b
        0x9e -> :sswitch_b
        0x9f -> :sswitch_b
        0xa6 -> :sswitch_b
        0xa7 -> :sswitch_b
        0xa8 -> :sswitch_b
        0xa9 -> :sswitch_b
        0xaa -> :sswitch_b
        0xab -> :sswitch_b
        0xac -> :sswitch_b
        0xad -> :sswitch_b
        0xae -> :sswitch_b
        0xaf -> :sswitch_b
        0xb0 -> :sswitch_b
        0xb1 -> :sswitch_b
        0xb2 -> :sswitch_b
        0xb3 -> :sswitch_b
        0xb4 -> :sswitch_b
        0xb5 -> :sswitch_b
        0xb6 -> :sswitch_d
    .end sparse-switch
.end method
