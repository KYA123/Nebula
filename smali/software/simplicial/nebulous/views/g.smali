.class Lsoftware/simplicial/nebulous/views/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lsoftware/simplicial/nebulous/views/j;

.field b:Lsoftware/simplicial/nebulous/views/d;

.field c:J

.field d:B

.field e:F

.field f:F

.field private g:J


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/views/j;Lsoftware/simplicial/nebulous/views/d;JJB)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/g;->a:Lsoftware/simplicial/nebulous/views/j;

    .line 22
    iput-object p2, p0, Lsoftware/simplicial/nebulous/views/g;->b:Lsoftware/simplicial/nebulous/views/d;

    .line 23
    iput-wide p3, p0, Lsoftware/simplicial/nebulous/views/g;->g:J

    .line 24
    iput-wide p5, p0, Lsoftware/simplicial/nebulous/views/g;->c:J

    .line 25
    iput-byte p7, p0, Lsoftware/simplicial/nebulous/views/g;->d:B

    .line 26
    return-void
.end method


# virtual methods
.method public a(J)V
    .locals 9

    .prologue
    const/high16 v7, 0x41200000    # 10.0f

    const/high16 v6, 0x3f800000    # 1.0f

    .line 30
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/views/g;->g:J

    sub-long v0, p1, v0

    long-to-double v0, v0

    iget-wide v2, p0, Lsoftware/simplicial/nebulous/views/g;->c:J

    iget-wide v4, p0, Lsoftware/simplicial/nebulous/views/g;->g:J

    sub-long/2addr v2, v4

    long-to-double v2, v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    .line 32
    float-to-double v2, v0

    const-wide v4, 0x3fb999999999999aL    # 0.1

    cmpg-double v1, v2, v4

    if-gez v1, :cond_0

    .line 34
    mul-float v1, v0, v7

    iput v1, p0, Lsoftware/simplicial/nebulous/views/g;->f:F

    .line 35
    mul-float/2addr v0, v7

    iput v0, p0, Lsoftware/simplicial/nebulous/views/g;->e:F

    .line 47
    :goto_0
    return-void

    .line 37
    :cond_0
    float-to-double v2, v0

    const-wide/high16 v4, 0x3fe8000000000000L    # 0.75

    cmpg-double v1, v2, v4

    if-gez v1, :cond_1

    .line 39
    iput v6, p0, Lsoftware/simplicial/nebulous/views/g;->f:F

    .line 40
    iput v6, p0, Lsoftware/simplicial/nebulous/views/g;->e:F

    goto :goto_0

    .line 44
    :cond_1
    const/high16 v1, 0x3f400000    # 0.75f

    sub-float/2addr v0, v1

    const/high16 v1, 0x3e800000    # 0.25f

    div-float/2addr v0, v1

    sub-float v0, v6, v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/g;->f:F

    .line 45
    iput v6, p0, Lsoftware/simplicial/nebulous/views/g;->e:F

    goto :goto_0
.end method
