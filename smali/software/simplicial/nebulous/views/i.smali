.class public final enum Lsoftware/simplicial/nebulous/views/i;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/nebulous/views/i;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum A:Lsoftware/simplicial/nebulous/views/i;

.field public static final enum B:Lsoftware/simplicial/nebulous/views/i;

.field private static final synthetic C:[Lsoftware/simplicial/nebulous/views/i;

.field public static final enum a:Lsoftware/simplicial/nebulous/views/i;

.field public static final enum b:Lsoftware/simplicial/nebulous/views/i;

.field public static final enum c:Lsoftware/simplicial/nebulous/views/i;

.field public static final enum d:Lsoftware/simplicial/nebulous/views/i;

.field public static final enum e:Lsoftware/simplicial/nebulous/views/i;

.field public static final enum f:Lsoftware/simplicial/nebulous/views/i;

.field public static final enum g:Lsoftware/simplicial/nebulous/views/i;

.field public static final enum h:Lsoftware/simplicial/nebulous/views/i;

.field public static final enum i:Lsoftware/simplicial/nebulous/views/i;

.field public static final enum j:Lsoftware/simplicial/nebulous/views/i;

.field public static final enum k:Lsoftware/simplicial/nebulous/views/i;

.field public static final enum l:Lsoftware/simplicial/nebulous/views/i;

.field public static final enum m:Lsoftware/simplicial/nebulous/views/i;

.field public static final enum n:Lsoftware/simplicial/nebulous/views/i;

.field public static final enum o:Lsoftware/simplicial/nebulous/views/i;

.field public static final enum p:Lsoftware/simplicial/nebulous/views/i;

.field public static final enum q:Lsoftware/simplicial/nebulous/views/i;

.field public static final enum r:Lsoftware/simplicial/nebulous/views/i;

.field public static final enum s:Lsoftware/simplicial/nebulous/views/i;

.field public static final enum t:Lsoftware/simplicial/nebulous/views/i;

.field public static final enum u:Lsoftware/simplicial/nebulous/views/i;

.field public static final enum v:Lsoftware/simplicial/nebulous/views/i;

.field public static final enum w:Lsoftware/simplicial/nebulous/views/i;

.field public static final enum x:Lsoftware/simplicial/nebulous/views/i;

.field public static final enum y:Lsoftware/simplicial/nebulous/views/i;

.field public static final enum z:Lsoftware/simplicial/nebulous/views/i;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 8
    new-instance v0, Lsoftware/simplicial/nebulous/views/i;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/nebulous/views/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/i;->a:Lsoftware/simplicial/nebulous/views/i;

    new-instance v0, Lsoftware/simplicial/nebulous/views/i;

    const-string v1, "BAT"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/nebulous/views/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/i;->b:Lsoftware/simplicial/nebulous/views/i;

    new-instance v0, Lsoftware/simplicial/nebulous/views/i;

    const-string v1, "LEAF"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/nebulous/views/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/i;->c:Lsoftware/simplicial/nebulous/views/i;

    new-instance v0, Lsoftware/simplicial/nebulous/views/i;

    const-string v1, "LINE"

    invoke-direct {v0, v1, v6}, Lsoftware/simplicial/nebulous/views/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/i;->d:Lsoftware/simplicial/nebulous/views/i;

    new-instance v0, Lsoftware/simplicial/nebulous/views/i;

    const-string v1, "ACHIEVE_ARENA"

    invoke-direct {v0, v1, v7}, Lsoftware/simplicial/nebulous/views/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/i;->e:Lsoftware/simplicial/nebulous/views/i;

    new-instance v0, Lsoftware/simplicial/nebulous/views/i;

    const-string v1, "ACHIEVE_CW"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/views/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/i;->f:Lsoftware/simplicial/nebulous/views/i;

    new-instance v0, Lsoftware/simplicial/nebulous/views/i;

    const-string v1, "SNOWFLAKE1"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/views/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/i;->g:Lsoftware/simplicial/nebulous/views/i;

    new-instance v0, Lsoftware/simplicial/nebulous/views/i;

    const-string v1, "SNOWFLAKE2"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/views/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/i;->h:Lsoftware/simplicial/nebulous/views/i;

    new-instance v0, Lsoftware/simplicial/nebulous/views/i;

    const-string v1, "SNOWFLAKE3"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/views/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/i;->i:Lsoftware/simplicial/nebulous/views/i;

    new-instance v0, Lsoftware/simplicial/nebulous/views/i;

    const-string v1, "PRESENT1"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/views/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/i;->j:Lsoftware/simplicial/nebulous/views/i;

    new-instance v0, Lsoftware/simplicial/nebulous/views/i;

    const-string v1, "PRESENT2"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/views/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/i;->k:Lsoftware/simplicial/nebulous/views/i;

    new-instance v0, Lsoftware/simplicial/nebulous/views/i;

    const-string v1, "PRESENT3"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/views/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/i;->l:Lsoftware/simplicial/nebulous/views/i;

    new-instance v0, Lsoftware/simplicial/nebulous/views/i;

    const-string v1, "COIN"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/views/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/i;->m:Lsoftware/simplicial/nebulous/views/i;

    new-instance v0, Lsoftware/simplicial/nebulous/views/i;

    const-string v1, "BEAD"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/views/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/i;->n:Lsoftware/simplicial/nebulous/views/i;

    new-instance v0, Lsoftware/simplicial/nebulous/views/i;

    const-string v1, "EGG1"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/views/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/i;->o:Lsoftware/simplicial/nebulous/views/i;

    new-instance v0, Lsoftware/simplicial/nebulous/views/i;

    const-string v1, "EGG2"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/views/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/i;->p:Lsoftware/simplicial/nebulous/views/i;

    new-instance v0, Lsoftware/simplicial/nebulous/views/i;

    const-string v1, "EGG3"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/views/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/i;->q:Lsoftware/simplicial/nebulous/views/i;

    new-instance v0, Lsoftware/simplicial/nebulous/views/i;

    const-string v1, "EGG4"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/views/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/i;->r:Lsoftware/simplicial/nebulous/views/i;

    new-instance v0, Lsoftware/simplicial/nebulous/views/i;

    const-string v1, "EGG5"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/views/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/i;->s:Lsoftware/simplicial/nebulous/views/i;

    new-instance v0, Lsoftware/simplicial/nebulous/views/i;

    const-string v1, "EGG6"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/views/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/i;->t:Lsoftware/simplicial/nebulous/views/i;

    .line 9
    new-instance v0, Lsoftware/simplicial/nebulous/views/i;

    const-string v1, "SPRING_LEAF"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/views/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/i;->u:Lsoftware/simplicial/nebulous/views/i;

    new-instance v0, Lsoftware/simplicial/nebulous/views/i;

    const-string v1, "NEBULA"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/views/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/i;->v:Lsoftware/simplicial/nebulous/views/i;

    new-instance v0, Lsoftware/simplicial/nebulous/views/i;

    const-string v1, "CANDY"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/views/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/i;->w:Lsoftware/simplicial/nebulous/views/i;

    new-instance v0, Lsoftware/simplicial/nebulous/views/i;

    const-string v1, "SUN"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/views/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/i;->x:Lsoftware/simplicial/nebulous/views/i;

    new-instance v0, Lsoftware/simplicial/nebulous/views/i;

    const-string v1, "MOON"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/views/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/i;->y:Lsoftware/simplicial/nebulous/views/i;

    new-instance v0, Lsoftware/simplicial/nebulous/views/i;

    const-string v1, "NOTE1"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/views/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/i;->z:Lsoftware/simplicial/nebulous/views/i;

    new-instance v0, Lsoftware/simplicial/nebulous/views/i;

    const-string v1, "NOTE2"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/views/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/i;->A:Lsoftware/simplicial/nebulous/views/i;

    new-instance v0, Lsoftware/simplicial/nebulous/views/i;

    const-string v1, "NOTE3"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/views/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/i;->B:Lsoftware/simplicial/nebulous/views/i;

    .line 6
    const/16 v0, 0x1c

    new-array v0, v0, [Lsoftware/simplicial/nebulous/views/i;

    sget-object v1, Lsoftware/simplicial/nebulous/views/i;->a:Lsoftware/simplicial/nebulous/views/i;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/nebulous/views/i;->b:Lsoftware/simplicial/nebulous/views/i;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/nebulous/views/i;->c:Lsoftware/simplicial/nebulous/views/i;

    aput-object v1, v0, v5

    sget-object v1, Lsoftware/simplicial/nebulous/views/i;->d:Lsoftware/simplicial/nebulous/views/i;

    aput-object v1, v0, v6

    sget-object v1, Lsoftware/simplicial/nebulous/views/i;->e:Lsoftware/simplicial/nebulous/views/i;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->f:Lsoftware/simplicial/nebulous/views/i;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->g:Lsoftware/simplicial/nebulous/views/i;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->h:Lsoftware/simplicial/nebulous/views/i;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->i:Lsoftware/simplicial/nebulous/views/i;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->j:Lsoftware/simplicial/nebulous/views/i;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->k:Lsoftware/simplicial/nebulous/views/i;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->l:Lsoftware/simplicial/nebulous/views/i;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->m:Lsoftware/simplicial/nebulous/views/i;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->n:Lsoftware/simplicial/nebulous/views/i;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->o:Lsoftware/simplicial/nebulous/views/i;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->p:Lsoftware/simplicial/nebulous/views/i;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->q:Lsoftware/simplicial/nebulous/views/i;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->r:Lsoftware/simplicial/nebulous/views/i;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->s:Lsoftware/simplicial/nebulous/views/i;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->t:Lsoftware/simplicial/nebulous/views/i;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->u:Lsoftware/simplicial/nebulous/views/i;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->v:Lsoftware/simplicial/nebulous/views/i;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->w:Lsoftware/simplicial/nebulous/views/i;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->x:Lsoftware/simplicial/nebulous/views/i;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->y:Lsoftware/simplicial/nebulous/views/i;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->z:Lsoftware/simplicial/nebulous/views/i;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->A:Lsoftware/simplicial/nebulous/views/i;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->B:Lsoftware/simplicial/nebulous/views/i;

    aput-object v2, v0, v1

    sput-object v0, Lsoftware/simplicial/nebulous/views/i;->C:[Lsoftware/simplicial/nebulous/views/i;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/nebulous/views/i;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lsoftware/simplicial/nebulous/views/i;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/views/i;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/nebulous/views/i;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lsoftware/simplicial/nebulous/views/i;->C:[Lsoftware/simplicial/nebulous/views/i;

    invoke-virtual {v0}, [Lsoftware/simplicial/nebulous/views/i;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/nebulous/views/i;

    return-object v0
.end method
