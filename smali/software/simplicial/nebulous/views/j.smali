.class public Lsoftware/simplicial/nebulous/views/j;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final r:Ljava/util/Random;

.field private static final w:[[I


# instance fields
.field public final a:[Lsoftware/simplicial/nebulous/views/d;

.field public b:I

.field public c:Z

.field public d:Z

.field e:[Lsoftware/simplicial/nebulous/views/h;

.field public f:Lsoftware/simplicial/a/e;

.field public g:F

.field public h:Z

.field public i:Z

.field public j:Lsoftware/simplicial/a/bk;

.field public k:Lsoftware/simplicial/nebulous/views/g;

.field public final l:Lsoftware/simplicial/nebulous/views/b/i;

.field public final m:Lsoftware/simplicial/nebulous/views/b/h;

.field public final n:Lsoftware/simplicial/nebulous/views/b/g;

.field public final o:Lsoftware/simplicial/nebulous/views/b/l;

.field public final p:Lsoftware/simplicial/nebulous/views/b/l;

.field public q:Z

.field private s:I

.field private t:I

.field private u:I

.field private v:Lsoftware/simplicial/a/e;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 28
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Random;-><init>(J)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    .line 69
    const/16 v0, 0xa

    new-array v0, v0, [[I

    const/4 v1, 0x0

    new-array v2, v4, [I

    fill-array-data v2, :array_0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-array v2, v4, [I

    fill-array-data v2, :array_1

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-array v2, v4, [I

    fill-array-data v2, :array_2

    aput-object v2, v0, v1

    new-array v1, v4, [I

    fill-array-data v1, :array_3

    aput-object v1, v0, v4

    const/4 v1, 0x4

    new-array v2, v4, [I

    fill-array-data v2, :array_4

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v4, [I

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v4, [I

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v4, [I

    fill-array-data v2, :array_7

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v4, [I

    fill-array-data v2, :array_8

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v4, [I

    fill-array-data v2, :array_9

    aput-object v2, v0, v1

    sput-object v0, Lsoftware/simplicial/nebulous/views/j;->w:[[I

    return-void

    :array_0
    .array-data 4
        0xea
        0x20
        0x59
    .end array-data

    :array_1
    .array-data 4
        0xfb
        0xe6
        0x2b
    .end array-data

    :array_2
    .array-data 4
        0x36
        0xae
        0x4c
    .end array-data

    :array_3
    .array-data 4
        0x36
        0xae
        0x4c
    .end array-data

    :array_4
    .array-data 4
        0xfb
        0xe6
        0x2b
    .end array-data

    :array_5
    .array-data 4
        0xea
        0x20
        0x59
    .end array-data

    :array_6
    .array-data 4
        0x65
        0x2a
        0x8e
    .end array-data

    :array_7
    .array-data 4
        0xd
        0x89
        0xcb
    .end array-data

    :array_8
    .array-data 4
        0xd
        0x89
        0xcb
    .end array-data

    :array_9
    .array-data 4
        0x65
        0x2a
        0x8e
    .end array-data
.end method

.method public constructor <init>(Lsoftware/simplicial/nebulous/views/b/i;Lsoftware/simplicial/nebulous/views/b/h;Lsoftware/simplicial/nebulous/views/b/g;Lsoftware/simplicial/nebulous/views/b/l;Lsoftware/simplicial/nebulous/views/b/l;I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput v1, p0, Lsoftware/simplicial/nebulous/views/j;->b:I

    .line 48
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/views/j;->d:Z

    .line 49
    const/16 v0, 0x19

    new-array v0, v0, [Lsoftware/simplicial/nebulous/views/h;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/j;->e:[Lsoftware/simplicial/nebulous/views/h;

    .line 51
    const/high16 v0, 0x42340000    # 45.0f

    iput v0, p0, Lsoftware/simplicial/nebulous/views/j;->g:F

    .line 52
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/views/j;->h:Z

    .line 53
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/views/j;->i:Z

    .line 55
    iput v1, p0, Lsoftware/simplicial/nebulous/views/j;->t:I

    .line 56
    iput v1, p0, Lsoftware/simplicial/nebulous/views/j;->u:I

    .line 57
    sget-object v0, Lsoftware/simplicial/a/e;->b:Lsoftware/simplicial/a/e;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/j;->v:Lsoftware/simplicial/a/e;

    .line 59
    new-instance v0, Lsoftware/simplicial/a/bk;

    invoke-direct {v0}, Lsoftware/simplicial/a/bk;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/j;->k:Lsoftware/simplicial/nebulous/views/g;

    .line 67
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/views/j;->q:Z

    .line 83
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/j;->l:Lsoftware/simplicial/nebulous/views/b/i;

    .line 84
    iput-object p2, p0, Lsoftware/simplicial/nebulous/views/j;->m:Lsoftware/simplicial/nebulous/views/b/h;

    .line 85
    iput-object p3, p0, Lsoftware/simplicial/nebulous/views/j;->n:Lsoftware/simplicial/nebulous/views/b/g;

    .line 86
    iput-object p4, p0, Lsoftware/simplicial/nebulous/views/j;->o:Lsoftware/simplicial/nebulous/views/b/l;

    .line 87
    iput-object p5, p0, Lsoftware/simplicial/nebulous/views/j;->p:Lsoftware/simplicial/nebulous/views/b/l;

    .line 88
    new-array v0, p6, [Lsoftware/simplicial/nebulous/views/d;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/j;->a:[Lsoftware/simplicial/nebulous/views/d;

    move v0, v1

    .line 89
    :goto_0
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/j;->a:[Lsoftware/simplicial/nebulous/views/d;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 90
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/j;->a:[Lsoftware/simplicial/nebulous/views/d;

    new-instance v3, Lsoftware/simplicial/nebulous/views/d;

    invoke-direct {v3}, Lsoftware/simplicial/nebulous/views/d;-><init>()V

    aput-object v3, v2, v0

    .line 89
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 91
    :cond_0
    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/j;->e:[Lsoftware/simplicial/nebulous/views/h;

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 92
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/j;->e:[Lsoftware/simplicial/nebulous/views/h;

    new-instance v2, Lsoftware/simplicial/nebulous/views/h;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/views/h;-><init>()V

    aput-object v2, v0, v1

    .line 91
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 93
    :cond_1
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 1786
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x19

    if-ge v0, v1, :cond_0

    .line 1788
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/j;->e:[Lsoftware/simplicial/nebulous/views/h;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    iput v2, v1, Lsoftware/simplicial/nebulous/views/h;->g:F

    .line 1789
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/j;->e:[Lsoftware/simplicial/nebulous/views/h;

    aget-object v1, v1, v0

    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, v1, Lsoftware/simplicial/nebulous/views/h;->i:F

    .line 1786
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1791
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1817
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x19

    if-ge v0, v1, :cond_1

    .line 1819
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/j;->e:[Lsoftware/simplicial/nebulous/views/h;

    aget-object v1, v1, v0

    iget v1, v1, Lsoftware/simplicial/nebulous/views/h;->g:F

    cmpl-float v1, v1, v6

    if-lez v1, :cond_0

    .line 1821
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/j;->e:[Lsoftware/simplicial/nebulous/views/h;

    aget-object v1, v1, v0

    iget v2, v1, Lsoftware/simplicial/nebulous/views/h;->g:F

    iget v3, p0, Lsoftware/simplicial/nebulous/views/j;->g:F

    iget-object v4, p0, Lsoftware/simplicial/nebulous/views/j;->e:[Lsoftware/simplicial/nebulous/views/h;

    aget-object v4, v4, v0

    iget v4, v4, Lsoftware/simplicial/nebulous/views/h;->i:F

    const/high16 v5, 0x42700000    # 60.0f

    mul-float/2addr v4, v5

    div-float/2addr v3, v4

    sub-float/2addr v2, v3

    iput v2, v1, Lsoftware/simplicial/nebulous/views/h;->g:F

    .line 1822
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/j;->e:[Lsoftware/simplicial/nebulous/views/h;

    aget-object v1, v1, v0

    iget v1, v1, Lsoftware/simplicial/nebulous/views/h;->g:F

    cmpg-float v1, v1, v6

    if-gez v1, :cond_0

    .line 1823
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/j;->e:[Lsoftware/simplicial/nebulous/views/h;

    aget-object v1, v1, v0

    iput v6, v1, Lsoftware/simplicial/nebulous/views/h;->g:F

    .line 1817
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1826
    :cond_1
    return-void
.end method

.method public a(FFFFFFIFFILsoftware/simplicial/nebulous/views/i;)V
    .locals 3

    .prologue
    .line 1795
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/j;->e:[Lsoftware/simplicial/nebulous/views/h;

    iget v1, p0, Lsoftware/simplicial/nebulous/views/j;->s:I

    aget-object v0, v0, v1

    iput p1, v0, Lsoftware/simplicial/nebulous/views/h;->a:F

    .line 1796
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/j;->e:[Lsoftware/simplicial/nebulous/views/h;

    iget v1, p0, Lsoftware/simplicial/nebulous/views/j;->s:I

    aget-object v0, v0, v1

    iput p2, v0, Lsoftware/simplicial/nebulous/views/h;->b:F

    .line 1797
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/j;->e:[Lsoftware/simplicial/nebulous/views/h;

    iget v1, p0, Lsoftware/simplicial/nebulous/views/j;->s:I

    aget-object v0, v0, v1

    iput p3, v0, Lsoftware/simplicial/nebulous/views/h;->c:F

    .line 1798
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/j;->e:[Lsoftware/simplicial/nebulous/views/h;

    iget v1, p0, Lsoftware/simplicial/nebulous/views/j;->s:I

    aget-object v0, v0, v1

    iput p4, v0, Lsoftware/simplicial/nebulous/views/h;->d:F

    .line 1799
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/j;->e:[Lsoftware/simplicial/nebulous/views/h;

    iget v1, p0, Lsoftware/simplicial/nebulous/views/j;->s:I

    aget-object v0, v0, v1

    iput p5, v0, Lsoftware/simplicial/nebulous/views/h;->e:F

    .line 1800
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/j;->e:[Lsoftware/simplicial/nebulous/views/h;

    iget v1, p0, Lsoftware/simplicial/nebulous/views/j;->s:I

    aget-object v0, v0, v1

    iput p6, v0, Lsoftware/simplicial/nebulous/views/h;->f:F

    .line 1801
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/j;->e:[Lsoftware/simplicial/nebulous/views/h;

    iget v1, p0, Lsoftware/simplicial/nebulous/views/j;->s:I

    aget-object v0, v0, v1

    iget v1, p0, Lsoftware/simplicial/nebulous/views/j;->g:F

    mul-float/2addr v1, p9

    const/high16 v2, 0x40c00000    # 6.0f

    div-float/2addr v1, v2

    iput v1, v0, Lsoftware/simplicial/nebulous/views/h;->g:F

    .line 1802
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/j;->e:[Lsoftware/simplicial/nebulous/views/h;

    iget v1, p0, Lsoftware/simplicial/nebulous/views/j;->s:I

    aget-object v0, v0, v1

    iget v0, v0, Lsoftware/simplicial/nebulous/views/h;->g:F

    iget v1, p0, Lsoftware/simplicial/nebulous/views/j;->g:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 1803
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/j;->e:[Lsoftware/simplicial/nebulous/views/h;

    iget v1, p0, Lsoftware/simplicial/nebulous/views/j;->s:I

    aget-object v0, v0, v1

    iget v1, p0, Lsoftware/simplicial/nebulous/views/j;->g:F

    iput v1, v0, Lsoftware/simplicial/nebulous/views/h;->g:F

    .line 1804
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/j;->e:[Lsoftware/simplicial/nebulous/views/h;

    iget v1, p0, Lsoftware/simplicial/nebulous/views/j;->s:I

    aget-object v0, v0, v1

    iput p8, v0, Lsoftware/simplicial/nebulous/views/h;->i:F

    .line 1805
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/j;->e:[Lsoftware/simplicial/nebulous/views/h;

    iget v1, p0, Lsoftware/simplicial/nebulous/views/j;->s:I

    aget-object v0, v0, v1

    iput p7, v0, Lsoftware/simplicial/nebulous/views/h;->h:I

    .line 1806
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/j;->e:[Lsoftware/simplicial/nebulous/views/h;

    iget v1, p0, Lsoftware/simplicial/nebulous/views/j;->s:I

    aget-object v0, v0, v1

    const/4 v1, 0x0

    iput v1, v0, Lsoftware/simplicial/nebulous/views/h;->j:F

    .line 1807
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/j;->e:[Lsoftware/simplicial/nebulous/views/h;

    iget v1, p0, Lsoftware/simplicial/nebulous/views/j;->s:I

    aget-object v0, v0, v1

    iput p10, v0, Lsoftware/simplicial/nebulous/views/h;->k:I

    .line 1808
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/j;->e:[Lsoftware/simplicial/nebulous/views/h;

    iget v1, p0, Lsoftware/simplicial/nebulous/views/j;->s:I

    aget-object v0, v0, v1

    iput-object p11, v0, Lsoftware/simplicial/nebulous/views/h;->l:Lsoftware/simplicial/nebulous/views/i;

    .line 1810
    iget v0, p0, Lsoftware/simplicial/nebulous/views/j;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/views/j;->s:I

    .line 1811
    iget v0, p0, Lsoftware/simplicial/nebulous/views/j;->s:I

    const/16 v1, 0x19

    if-lt v0, v1, :cond_1

    .line 1812
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/j;->s:I

    .line 1813
    :cond_1
    return-void
.end method

.method public a(Lsoftware/simplicial/a/bf;IFZZZZZLandroid/graphics/Bitmap;ZZZLjava/util/Set;Ljava/util/Set;ZD)V
    .locals 28
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsoftware/simplicial/a/bf;",
            "IFZZZZZ",
            "Landroid/graphics/Bitmap;",
            "ZZZ",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;ZD)V"
        }
    .end annotation

    .prologue
    .line 99
    if-eqz p6, :cond_10

    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/a/bf;->U:Lsoftware/simplicial/a/e;

    sget-object v3, Lsoftware/simplicial/a/e;->b:Lsoftware/simplicial/a/e;

    if-eq v2, v3, :cond_0

    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/a/bf;->U:Lsoftware/simplicial/a/e;

    sget-object v3, Lsoftware/simplicial/a/e;->dQ:Lsoftware/simplicial/a/e;

    if-eq v2, v3, :cond_0

    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/a/bf;->U:Lsoftware/simplicial/a/e;

    sget-object v3, Lsoftware/simplicial/a/e;->dP:Lsoftware/simplicial/a/e;

    if-ne v2, v3, :cond_1

    :cond_0
    move-object/from16 v0, p1

    iget v2, v0, Lsoftware/simplicial/a/bf;->ad:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    move-object/from16 v0, p1

    iget v2, v0, Lsoftware/simplicial/a/bf;->ad:I

    if-eqz v2, :cond_10

    if-eqz p12, :cond_10

    :cond_1
    const/4 v2, 0x1

    :goto_0
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/nebulous/views/j;->q:Z

    .line 104
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->l:Lsoftware/simplicial/nebulous/views/b/i;

    move-object/from16 v0, p1

    iget-object v3, v0, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v4, v0, Lsoftware/simplicial/a/bf;->E:[B

    invoke-virtual {v2, v3, v4}, Lsoftware/simplicial/nebulous/views/b/i;->a(Ljava/lang/String;[B)V

    .line 105
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->m:Lsoftware/simplicial/nebulous/views/b/h;

    move-object/from16 v0, p1

    iget-object v5, v0, Lsoftware/simplicial/a/bf;->H:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v6, v0, Lsoftware/simplicial/a/bf;->I:[B

    move-object/from16 v0, p1

    iget v7, v0, Lsoftware/simplicial/a/bf;->bD:I

    move-object/from16 v0, p1

    iget-object v8, v0, Lsoftware/simplicial/a/bf;->J:Lsoftware/simplicial/a/q;

    move/from16 v3, p10

    move/from16 v4, p11

    move-object/from16 v9, p13

    move-object/from16 v10, p14

    invoke-virtual/range {v2 .. v10}, Lsoftware/simplicial/nebulous/views/b/h;->a(ZZLjava/lang/String;[BILsoftware/simplicial/a/q;Ljava/util/Set;Ljava/util/Set;)V

    .line 106
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->n:Lsoftware/simplicial/nebulous/views/b/g;

    move-object/from16 v0, p1

    iget-object v3, v0, Lsoftware/simplicial/a/bf;->ac:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/views/b/g;->a(Ljava/lang/String;)V

    .line 107
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lsoftware/simplicial/nebulous/views/j;->q:Z

    if-eqz v2, :cond_12

    .line 109
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->o:Lsoftware/simplicial/nebulous/views/b/l;

    move-object/from16 v0, p1

    iget v4, v0, Lsoftware/simplicial/a/bf;->ad:I

    move-object/from16 v0, p1

    iget-object v5, v0, Lsoftware/simplicial/a/bf;->U:Lsoftware/simplicial/a/e;

    if-eqz p5, :cond_11

    if-eqz p8, :cond_11

    const/4 v7, 0x1

    :goto_1
    move/from16 v3, p12

    move-object/from16 v6, p9

    invoke-virtual/range {v2 .. v7}, Lsoftware/simplicial/nebulous/views/b/l;->a(ZILsoftware/simplicial/a/e;Landroid/graphics/Bitmap;Z)V

    .line 110
    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/a/bf;->U:Lsoftware/simplicial/a/e;

    sget-object v3, Lsoftware/simplicial/a/e;->as:Lsoftware/simplicial/a/e;

    if-eq v2, v3, :cond_2

    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/a/bf;->U:Lsoftware/simplicial/a/e;

    sget-object v3, Lsoftware/simplicial/a/e;->kJ:Lsoftware/simplicial/a/e;

    if-eq v2, v3, :cond_2

    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/a/bf;->U:Lsoftware/simplicial/a/e;

    sget-object v3, Lsoftware/simplicial/a/e;->gL:Lsoftware/simplicial/a/e;

    if-eq v2, v3, :cond_2

    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/a/bf;->U:Lsoftware/simplicial/a/e;

    sget-object v3, Lsoftware/simplicial/a/e;->iU:Lsoftware/simplicial/a/e;

    if-eq v2, v3, :cond_2

    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/a/bf;->U:Lsoftware/simplicial/a/e;

    sget-object v3, Lsoftware/simplicial/a/e;->ja:Lsoftware/simplicial/a/e;

    if-ne v2, v3, :cond_3

    .line 112
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->p:Lsoftware/simplicial/nebulous/views/b/l;

    move-object/from16 v0, p1

    iget v4, v0, Lsoftware/simplicial/a/bf;->ad:I

    move-object/from16 v0, p1

    iget-object v5, v0, Lsoftware/simplicial/a/bf;->U:Lsoftware/simplicial/a/e;

    const/4 v7, 0x0

    move/from16 v3, p12

    move-object/from16 v6, p9

    invoke-virtual/range {v2 .. v7}, Lsoftware/simplicial/nebulous/views/b/l;->a(ZILsoftware/simplicial/a/e;Landroid/graphics/Bitmap;Z)V

    .line 120
    :cond_3
    :goto_2
    move/from16 v0, p5

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lsoftware/simplicial/nebulous/views/j;->h:Z

    .line 121
    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/a/bf;->U:Lsoftware/simplicial/a/e;

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    .line 122
    const/4 v3, 0x0

    .line 123
    move-object/from16 v0, p1

    iget-byte v2, v0, Lsoftware/simplicial/a/bf;->am:B

    and-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_4

    move-object/from16 v0, p1

    iget-byte v2, v0, Lsoftware/simplicial/a/bf;->am:B

    and-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_4

    move-object/from16 v0, p1

    iget v2, v0, Lsoftware/simplicial/a/bf;->ad:I

    if-nez v2, :cond_4

    if-eqz p6, :cond_13

    if-eqz p7, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget-boolean v2, v2, Lsoftware/simplicial/a/e;->lv:Z

    if-nez v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget-boolean v2, v2, Lsoftware/simplicial/a/e;->lw:Z

    if-nez v2, :cond_4

    if-eqz p5, :cond_13

    if-eqz p8, :cond_13

    if-eqz p9, :cond_13

    :cond_4
    const/4 v2, 0x1

    :goto_3
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/nebulous/views/j;->i:Z

    .line 132
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/j;->u:I

    if-nez v2, :cond_5

    move-object/from16 v0, p1

    iget v2, v0, Lsoftware/simplicial/a/bf;->S:I

    if-gtz v2, :cond_6

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->v:Lsoftware/simplicial/a/e;

    move-object/from16 v0, p1

    iget-object v4, v0, Lsoftware/simplicial/a/bf;->U:Lsoftware/simplicial/a/e;

    if-eq v2, v4, :cond_7

    .line 134
    :cond_6
    const/4 v2, 0x1

    .line 135
    invoke-direct/range {p0 .. p0}, Lsoftware/simplicial/nebulous/views/j;->b()V

    move v3, v2

    .line 137
    :cond_7
    if-eqz p5, :cond_8

    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/j;->u:I

    if-nez v2, :cond_8

    move-object/from16 v0, p1

    iget v2, v0, Lsoftware/simplicial/a/bf;->S:I

    if-lez v2, :cond_8

    .line 138
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/nebulous/views/j;->c:Z

    .line 139
    :cond_8
    move-object/from16 v0, p1

    iget v2, v0, Lsoftware/simplicial/a/bf;->S:I

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/j;->u:I

    .line 140
    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/a/bf;->U:Lsoftware/simplicial/a/e;

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->v:Lsoftware/simplicial/a/e;

    .line 143
    if-eqz p15, :cond_16

    .line 145
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    move-object/from16 v0, p1

    iget-object v4, v0, Lsoftware/simplicial/a/bf;->Y:Lsoftware/simplicial/a/bd;

    iput-object v4, v2, Lsoftware/simplicial/a/bk;->e:Lsoftware/simplicial/a/bd;

    .line 146
    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/a/bf;->Y:Lsoftware/simplicial/a/bd;

    sget-object v4, Lsoftware/simplicial/a/bd;->a:Lsoftware/simplicial/a/bd;

    if-eq v2, v4, :cond_e

    .line 148
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget-object v2, v2, Lsoftware/simplicial/a/bk;->a:Lsoftware/simplicial/a/bh;

    .line 149
    invoke-virtual/range {p1 .. p1}, Lsoftware/simplicial/a/bf;->q()Lsoftware/simplicial/a/bh;

    move-result-object v4

    .line 150
    if-eqz v2, :cond_9

    invoke-virtual {v2}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v5

    if-nez v5, :cond_9

    if-eq v2, v4, :cond_a

    .line 153
    :cond_9
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iput-object v4, v5, Lsoftware/simplicial/a/bk;->a:Lsoftware/simplicial/a/bh;

    .line 154
    if-eqz v4, :cond_a

    .line 156
    if-nez v2, :cond_a

    .line 158
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget v5, v4, Lsoftware/simplicial/a/bh;->l:F

    iput v5, v2, Lsoftware/simplicial/a/bk;->l:F

    .line 159
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget v5, v4, Lsoftware/simplicial/a/bh;->m:F

    iput v5, v2, Lsoftware/simplicial/a/bk;->m:F

    .line 166
    :cond_a
    if-eqz v4, :cond_e

    .line 168
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    iget v5, v4, Lsoftware/simplicial/a/bh;->w:F

    float-to-double v8, v5

    invoke-static {v8, v9}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    double-to-float v5, v6

    iput v5, v2, Lsoftware/simplicial/a/bk;->n:F

    .line 170
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget v5, v4, Lsoftware/simplicial/a/bh;->s:F

    float-to-double v6, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/j;->a:[Lsoftware/simplicial/nebulous/views/d;

    iget v8, v4, Lsoftware/simplicial/a/bh;->c:I

    aget-object v5, v5, v8

    iget v5, v5, Lsoftware/simplicial/nebulous/views/d;->C:F

    float-to-double v8, v5

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    neg-double v8, v8

    iget v5, v4, Lsoftware/simplicial/a/bh;->w:F

    const/high16 v10, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget v11, v11, Lsoftware/simplicial/a/bk;->n:F

    mul-float/2addr v10, v11

    add-float/2addr v5, v10

    float-to-double v10, v5

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    double-to-float v5, v6

    iput v5, v2, Lsoftware/simplicial/a/bk;->b:F

    .line 171
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget v5, v4, Lsoftware/simplicial/a/bh;->t:F

    float-to-double v6, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/j;->a:[Lsoftware/simplicial/nebulous/views/d;

    iget v8, v4, Lsoftware/simplicial/a/bh;->c:I

    aget-object v5, v5, v8

    iget v5, v5, Lsoftware/simplicial/nebulous/views/d;->C:F

    float-to-double v8, v5

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    iget v5, v4, Lsoftware/simplicial/a/bh;->w:F

    const/high16 v10, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget v11, v11, Lsoftware/simplicial/a/bk;->n:F

    mul-float/2addr v10, v11

    add-float/2addr v5, v10

    float-to-double v10, v5

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    double-to-float v5, v6

    iput v5, v2, Lsoftware/simplicial/a/bk;->c:F

    .line 173
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget v2, v2, Lsoftware/simplicial/a/bk;->b:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget v5, v5, Lsoftware/simplicial/a/bk;->l:F

    sub-float/2addr v2, v5

    .line 174
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget v5, v5, Lsoftware/simplicial/a/bk;->c:F

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget v6, v6, Lsoftware/simplicial/a/bk;->m:F

    sub-float/2addr v5, v6

    .line 175
    mul-float v6, v2, v2

    mul-float v7, v5, v5

    add-float/2addr v6, v7

    .line 177
    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget v7, v7, Lsoftware/simplicial/a/bk;->f:F

    const v8, -0x41555555

    cmpl-float v7, v7, v8

    if-lez v7, :cond_b

    .line 178
    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget v8, v7, Lsoftware/simplicial/a/bk;->f:F

    float-to-double v8, v8

    sub-double v8, v8, p16

    double-to-float v8, v8

    iput v8, v7, Lsoftware/simplicial/a/bk;->f:F

    .line 180
    :cond_b
    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget v7, v7, Lsoftware/simplicial/a/bk;->n:F

    const/high16 v8, 0x40800000    # 4.0f

    mul-float/2addr v7, v8

    cmpl-float v7, v6, v7

    if-lez v7, :cond_14

    .line 182
    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    float-to-double v6, v6

    invoke-static {v6, v7}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v6

    mul-double/2addr v6, v8

    double-to-float v4, v6

    .line 183
    float-to-double v6, v5

    float-to-double v8, v2

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v6

    double-to-float v2, v6

    .line 185
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget v6, v5, Lsoftware/simplicial/a/bk;->l:F

    float-to-double v6, v6

    float-to-double v8, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    float-to-double v10, v4

    mul-double/2addr v8, v10

    mul-double v8, v8, p16

    add-double/2addr v6, v8

    double-to-float v6, v6

    iput v6, v5, Lsoftware/simplicial/a/bk;->l:F

    .line 186
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget v6, v5, Lsoftware/simplicial/a/bk;->m:F

    float-to-double v6, v6

    float-to-double v8, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    float-to-double v10, v4

    mul-double/2addr v8, v10

    mul-double v8, v8, p16

    add-double/2addr v6, v8

    double-to-float v4, v6

    iput v4, v5, Lsoftware/simplicial/a/bk;->m:F

    .line 187
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget v4, v4, Lsoftware/simplicial/a/bk;->f:F

    const/4 v5, 0x0

    cmpg-float v4, v4, v5

    if-gtz v4, :cond_c

    .line 188
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iput v2, v4, Lsoftware/simplicial/a/bk;->d:F

    .line 189
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    const/4 v4, 0x0

    iput v4, v2, Lsoftware/simplicial/a/bk;->f:F

    .line 208
    :cond_d
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget v4, v4, Lsoftware/simplicial/a/bk;->s:F

    iput v4, v2, Lsoftware/simplicial/a/bk;->u:F

    .line 209
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget v4, v4, Lsoftware/simplicial/a/bk;->t:F

    iput v4, v2, Lsoftware/simplicial/a/bk;->v:F

    .line 210
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget v4, v4, Lsoftware/simplicial/a/bk;->l:F

    iput v4, v2, Lsoftware/simplicial/a/bk;->s:F

    .line 211
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget v4, v4, Lsoftware/simplicial/a/bk;->m:F

    iput v4, v2, Lsoftware/simplicial/a/bk;->t:F

    .line 212
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget v4, v2, Lsoftware/simplicial/a/bk;->w:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget v5, v5, Lsoftware/simplicial/a/bk;->n:F

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget v6, v6, Lsoftware/simplicial/a/bk;->w:F

    sub-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    add-float/2addr v4, v5

    iput v4, v2, Lsoftware/simplicial/a/bk;->w:F

    .line 223
    :cond_e
    :goto_5
    if-nez p4, :cond_17

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget-boolean v2, v2, Lsoftware/simplicial/a/e;->lx:Z

    if-nez v2, :cond_17

    if-nez v3, :cond_17

    .line 1782
    :cond_f
    :goto_6
    return-void

    .line 99
    :cond_10
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 109
    :cond_11
    const/4 v7, 0x0

    goto/16 :goto_1

    .line 116
    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->o:Lsoftware/simplicial/nebulous/views/b/l;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/views/b/l;->a(Z)V

    .line 117
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->p:Lsoftware/simplicial/nebulous/views/b/l;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/views/b/l;->a(Z)V

    goto/16 :goto_2

    .line 123
    :cond_13
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 191
    :cond_14
    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget v6, v6, Lsoftware/simplicial/a/bk;->f:F

    const/4 v7, 0x0

    cmpl-float v6, v6, v7

    if-lez v6, :cond_15

    .line 193
    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->b()F

    move-result v2

    const/high16 v4, 0x41000000    # 8.0f

    div-float/2addr v2, v4

    .line 195
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget v5, v4, Lsoftware/simplicial/a/bk;->l:F

    float-to-double v6, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget v5, v5, Lsoftware/simplicial/a/bk;->g:F

    float-to-double v8, v5

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    float-to-double v10, v2

    mul-double/2addr v8, v10

    mul-double v8, v8, p16

    add-double/2addr v6, v8

    double-to-float v5, v6

    iput v5, v4, Lsoftware/simplicial/a/bk;->l:F

    .line 196
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget v5, v4, Lsoftware/simplicial/a/bk;->m:F

    float-to-double v6, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget v5, v5, Lsoftware/simplicial/a/bk;->g:F

    float-to-double v8, v5

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    float-to-double v10, v2

    mul-double/2addr v8, v10

    mul-double v8, v8, p16

    add-double/2addr v6, v8

    double-to-float v2, v6

    iput v2, v4, Lsoftware/simplicial/a/bk;->m:F

    .line 197
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget v4, v4, Lsoftware/simplicial/a/bk;->g:F

    iput v4, v2, Lsoftware/simplicial/a/bk;->d:F

    goto/16 :goto_4

    .line 199
    :cond_15
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    iget v4, v4, Lsoftware/simplicial/a/bk;->f:F

    const v6, -0x41555555

    cmpg-float v4, v4, v6

    if-gtz v4, :cond_d

    .line 201
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    const/high16 v6, 0x3f000000    # 0.5f

    sget-object v7, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v7}, Ljava/util/Random;->nextFloat()F

    move-result v7

    const/high16 v8, 0x3f800000    # 1.0f

    mul-float/2addr v7, v8

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    add-float/2addr v6, v7

    iput v6, v4, Lsoftware/simplicial/a/bk;->f:F

    .line 202
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    float-to-double v6, v5

    float-to-double v8, v2

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v6

    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v2

    const/high16 v5, 0x40000000    # 2.0f

    mul-float/2addr v2, v5

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float/2addr v2, v5

    float-to-double v8, v2

    const-wide v10, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v8, v10

    const-wide/high16 v10, 0x4010000000000000L    # 4.0

    div-double/2addr v8, v10

    add-double/2addr v6, v8

    double-to-float v2, v6

    iput v2, v4, Lsoftware/simplicial/a/bk;->g:F

    goto/16 :goto_4

    .line 220
    :cond_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->j:Lsoftware/simplicial/a/bk;

    const/4 v4, 0x0

    iput-object v4, v2, Lsoftware/simplicial/a/bk;->a:Lsoftware/simplicial/a/bh;

    goto/16 :goto_5

    .line 226
    :cond_17
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/nebulous/views/j;->d:Z

    .line 227
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget-object v2, v2, Lsoftware/simplicial/a/e;->ll:Lsoftware/simplicial/a/f;

    sget-object v4, Lsoftware/simplicial/a/f;->ks:Lsoftware/simplicial/a/f;

    if-eq v2, v4, :cond_1a

    move-object/from16 v0, p1

    iget v2, v0, Lsoftware/simplicial/a/bf;->bD:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget v4, v4, Lsoftware/simplicial/a/e;->lm:I

    if-lt v2, v4, :cond_18

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget v2, v2, Lsoftware/simplicial/a/e;->lm:I

    const/16 v4, 0x32

    if-ge v2, v4, :cond_19

    :cond_18
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget v2, v2, Lsoftware/simplicial/a/e;->ln:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_19

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget v2, v2, Lsoftware/simplicial/a/e;->lo:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_19

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget v2, v2, Lsoftware/simplicial/a/e;->lp:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_19

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget v2, v2, Lsoftware/simplicial/a/e;->lq:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_19

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget v2, v2, Lsoftware/simplicial/a/e;->lr:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_19

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget v2, v2, Lsoftware/simplicial/a/e;->ls:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_19

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget v2, v2, Lsoftware/simplicial/a/e;->ly:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_19

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget v2, v2, Lsoftware/simplicial/a/e;->lz:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_19

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget v2, v2, Lsoftware/simplicial/a/e;->lA:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_19

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget v2, v2, Lsoftware/simplicial/a/e;->lB:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_19

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget v2, v2, Lsoftware/simplicial/a/e;->lC:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_19

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget v2, v2, Lsoftware/simplicial/a/e;->lD:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_19

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget-boolean v2, v2, Lsoftware/simplicial/a/e;->lx:Z

    if-eqz v2, :cond_1a

    .line 231
    :cond_19
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/nebulous/views/j;->d:Z

    .line 233
    :cond_1a
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lsoftware/simplicial/nebulous/views/j;->d:Z

    if-nez v2, :cond_1b

    if-eqz v3, :cond_f

    .line 236
    :cond_1b
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->a:Lsoftware/simplicial/nebulous/views/i;

    .line 237
    const/high16 v4, 0x42340000    # 45.0f

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->g:F

    .line 238
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget v4, v4, Lsoftware/simplicial/a/e;->lm:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_1e

    .line 240
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide v6, 0x4005bf0a8b145769L    # Math.E

    const-wide/high16 v8, -0x4020000000000000L    # -0.5

    move-object/from16 v0, p1

    iget v10, v0, Lsoftware/simplicial/a/bf;->bD:I

    int-to-double v10, v10

    const-wide/high16 v12, 0x4049000000000000L    # 50.0

    sub-double/2addr v10, v12

    const-wide/high16 v12, 0x403e000000000000L    # 30.0

    div-double/2addr v10, v12

    mul-double/2addr v8, v10

    invoke-static {v6, v7, v8, v9}, Lsoftware/simplicial/a/bb;->a(DD)D

    move-result-wide v6

    sub-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    const-wide/high16 v6, 0x4039000000000000L    # 25.0

    mul-double/2addr v4, v6

    double-to-int v4, v4

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    .line 241
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v5, Lsoftware/simplicial/a/e;->dQ:Lsoftware/simplicial/a/e;

    if-ne v4, v5, :cond_1d

    .line 242
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->d:Lsoftware/simplicial/nebulous/views/i;

    move-object v13, v2

    .line 634
    :goto_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v4, Lsoftware/simplicial/a/e;->fg:Lsoftware/simplicial/a/e;

    if-eq v2, v4, :cond_1c

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v4, Lsoftware/simplicial/a/e;->hU:Lsoftware/simplicial/a/e;

    if-eq v2, v4, :cond_1c

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v4, Lsoftware/simplicial/a/e;->if:Lsoftware/simplicial/a/e;

    if-eq v2, v4, :cond_1c

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v4, Lsoftware/simplicial/a/e;->ih:Lsoftware/simplicial/a/e;

    if-eq v2, v4, :cond_1c

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v4, Lsoftware/simplicial/a/e;->ks:Lsoftware/simplicial/a/e;

    if-eq v2, v4, :cond_1c

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v4, Lsoftware/simplicial/a/e;->kY:Lsoftware/simplicial/a/e;

    if-ne v2, v4, :cond_38

    .line 637
    :cond_1c
    if-eqz v3, :cond_38

    .line 639
    const/4 v2, 0x0

    :goto_8
    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/bf;->bF:I

    if-ge v2, v3, :cond_38

    .line 641
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextFloat()F

    move-result v3

    float-to-double v4, v3

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    mul-double/2addr v4, v6

    double-to-float v3, v4

    .line 642
    const/high16 v4, 0x3e800000    # 0.25f

    sget-object v5, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v5}, Ljava/util/Random;->nextFloat()F

    move-result v5

    const v6, 0x3dcccccd    # 0.1f

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    .line 643
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/j;->e:[Lsoftware/simplicial/nebulous/views/h;

    aget-object v5, v5, v2

    float-to-double v6, v3

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    float-to-double v8, v4

    mul-double/2addr v6, v8

    double-to-float v6, v6

    iput v6, v5, Lsoftware/simplicial/nebulous/views/h;->c:F

    .line 644
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/j;->e:[Lsoftware/simplicial/nebulous/views/h;

    aget-object v5, v5, v2

    float-to-double v6, v3

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    float-to-double v8, v4

    mul-double/2addr v6, v8

    double-to-float v3, v6

    iput v3, v5, Lsoftware/simplicial/nebulous/views/h;->d:F

    .line 645
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextFloat()F

    move-result v3

    float-to-double v4, v3

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    mul-double/2addr v4, v6

    double-to-float v3, v4

    .line 646
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v4}, Ljava/util/Random;->nextFloat()F

    move-result v4

    const v5, 0x3d99999a    # 0.075f

    mul-float/2addr v4, v5

    .line 647
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/j;->e:[Lsoftware/simplicial/nebulous/views/h;

    aget-object v5, v5, v2

    float-to-double v6, v3

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    float-to-double v8, v4

    mul-double/2addr v6, v8

    double-to-float v6, v6

    iput v6, v5, Lsoftware/simplicial/nebulous/views/h;->a:F

    .line 648
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/j;->e:[Lsoftware/simplicial/nebulous/views/h;

    aget-object v5, v5, v2

    float-to-double v6, v3

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    float-to-double v8, v4

    mul-double/2addr v6, v8

    double-to-float v3, v6

    iput v3, v5, Lsoftware/simplicial/nebulous/views/h;->b:F

    .line 639
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_8

    .line 243
    :cond_1d
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v5, Lsoftware/simplicial/a/e;->dP:Lsoftware/simplicial/a/e;

    if-ne v4, v5, :cond_37

    .line 244
    const/high16 v4, 0x41b40000    # 22.5f

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->g:F

    move-object v13, v2

    goto/16 :goto_7

    .line 246
    :cond_1e
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget v4, v4, Lsoftware/simplicial/a/e;->lD:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_1f

    .line 248
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 260
    :goto_9
    const/high16 v4, 0x41a00000    # 20.0f

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->g:F

    .line 261
    sget-object v4, Lsoftware/simplicial/nebulous/views/j$1;->a:[I

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget-object v5, v5, Lsoftware/simplicial/a/e;->ll:Lsoftware/simplicial/a/f;

    invoke-virtual {v5}, Lsoftware/simplicial/a/f;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_1

    :goto_a
    move-object v13, v2

    .line 277
    goto/16 :goto_7

    .line 251
    :pswitch_0
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->z:Lsoftware/simplicial/nebulous/views/i;

    goto :goto_9

    .line 254
    :pswitch_1
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->A:Lsoftware/simplicial/nebulous/views/i;

    goto :goto_9

    .line 257
    :pswitch_2
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->B:Lsoftware/simplicial/nebulous/views/i;

    goto :goto_9

    .line 264
    :pswitch_3
    const/4 v4, 0x5

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_a

    .line 267
    :pswitch_4
    const/16 v4, 0x8

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_a

    .line 270
    :pswitch_5
    const/16 v4, 0xb

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_a

    .line 273
    :pswitch_6
    const/16 v4, 0xe

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_a

    .line 276
    :pswitch_7
    const/16 v4, 0x11

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_a

    .line 280
    :cond_1f
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget v4, v4, Lsoftware/simplicial/a/e;->lC:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_20

    .line 282
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->y:Lsoftware/simplicial/nebulous/views/i;

    .line 283
    const/high16 v4, 0x41b00000    # 22.0f

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->g:F

    .line 284
    sget-object v4, Lsoftware/simplicial/nebulous/views/j$1;->a:[I

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget-object v5, v5, Lsoftware/simplicial/a/e;->ll:Lsoftware/simplicial/a/f;

    invoke-virtual {v5}, Lsoftware/simplicial/a/f;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_2

    :goto_b
    move-object v13, v2

    .line 300
    goto/16 :goto_7

    .line 287
    :pswitch_8
    const/4 v4, 0x5

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_b

    .line 290
    :pswitch_9
    const/16 v4, 0x8

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_b

    .line 293
    :pswitch_a
    const/16 v4, 0xb

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_b

    .line 296
    :pswitch_b
    const/16 v4, 0xe

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_b

    .line 299
    :pswitch_c
    const/16 v4, 0x11

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_b

    .line 303
    :cond_20
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget v4, v4, Lsoftware/simplicial/a/e;->lB:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_21

    .line 305
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->x:Lsoftware/simplicial/nebulous/views/i;

    .line 306
    const/high16 v4, 0x41a00000    # 20.0f

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->g:F

    .line 307
    sget-object v4, Lsoftware/simplicial/nebulous/views/j$1;->a:[I

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget-object v5, v5, Lsoftware/simplicial/a/e;->ll:Lsoftware/simplicial/a/f;

    invoke-virtual {v5}, Lsoftware/simplicial/a/f;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_3

    :goto_c
    move-object v13, v2

    .line 323
    goto/16 :goto_7

    .line 310
    :pswitch_d
    const/4 v4, 0x5

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_c

    .line 313
    :pswitch_e
    const/16 v4, 0x8

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_c

    .line 316
    :pswitch_f
    const/16 v4, 0xb

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_c

    .line 319
    :pswitch_10
    const/16 v4, 0xe

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_c

    .line 322
    :pswitch_11
    const/16 v4, 0x11

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_c

    .line 326
    :cond_21
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget v4, v4, Lsoftware/simplicial/a/e;->lA:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_22

    .line 328
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->w:Lsoftware/simplicial/nebulous/views/i;

    .line 329
    const/high16 v4, 0x41800000    # 16.0f

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->g:F

    .line 330
    sget-object v4, Lsoftware/simplicial/nebulous/views/j$1;->a:[I

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget-object v5, v5, Lsoftware/simplicial/a/e;->ll:Lsoftware/simplicial/a/f;

    invoke-virtual {v5}, Lsoftware/simplicial/a/f;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_4

    :goto_d
    move-object v13, v2

    .line 346
    goto/16 :goto_7

    .line 333
    :pswitch_12
    const/4 v4, 0x5

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_d

    .line 336
    :pswitch_13
    const/16 v4, 0x8

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_d

    .line 339
    :pswitch_14
    const/16 v4, 0xb

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_d

    .line 342
    :pswitch_15
    const/16 v4, 0xe

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_d

    .line 345
    :pswitch_16
    const/16 v4, 0x11

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_d

    .line 349
    :cond_22
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v5, Lsoftware/simplicial/a/e;->eL:Lsoftware/simplicial/a/e;

    if-eq v4, v5, :cond_23

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v5, Lsoftware/simplicial/a/e;->eM:Lsoftware/simplicial/a/e;

    if-eq v4, v5, :cond_23

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v5, Lsoftware/simplicial/a/e;->eJ:Lsoftware/simplicial/a/e;

    if-ne v4, v5, :cond_24

    .line 351
    :cond_23
    const/high16 v4, 0x41b00000    # 22.0f

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->g:F

    .line 352
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    packed-switch v4, :pswitch_data_5

    .line 364
    :goto_e
    sget-object v4, Lsoftware/simplicial/nebulous/views/j$1;->a:[I

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget-object v5, v5, Lsoftware/simplicial/a/e;->ll:Lsoftware/simplicial/a/f;

    invoke-virtual {v5}, Lsoftware/simplicial/a/f;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_6

    :goto_f
    move-object v13, v2

    .line 374
    goto/16 :goto_7

    .line 355
    :pswitch_17
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->g:Lsoftware/simplicial/nebulous/views/i;

    goto :goto_e

    .line 358
    :pswitch_18
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->h:Lsoftware/simplicial/nebulous/views/i;

    goto :goto_e

    .line 361
    :pswitch_19
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->i:Lsoftware/simplicial/nebulous/views/i;

    goto :goto_e

    .line 367
    :pswitch_1a
    const/4 v4, 0x5

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_f

    .line 370
    :pswitch_1b
    const/16 v4, 0xb

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_f

    .line 373
    :pswitch_1c
    const/16 v4, 0x11

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_f

    .line 377
    :cond_24
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v5, Lsoftware/simplicial/a/e;->eI:Lsoftware/simplicial/a/e;

    if-eq v4, v5, :cond_25

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v5, Lsoftware/simplicial/a/e;->eK:Lsoftware/simplicial/a/e;

    if-eq v4, v5, :cond_25

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v5, Lsoftware/simplicial/a/e;->eN:Lsoftware/simplicial/a/e;

    if-ne v4, v5, :cond_26

    .line 379
    :cond_25
    const/high16 v4, 0x41900000    # 18.0f

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->g:F

    .line 380
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    packed-switch v4, :pswitch_data_7

    .line 392
    :goto_10
    sget-object v4, Lsoftware/simplicial/nebulous/views/j$1;->a:[I

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget-object v5, v5, Lsoftware/simplicial/a/e;->ll:Lsoftware/simplicial/a/f;

    invoke-virtual {v5}, Lsoftware/simplicial/a/f;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_8

    :goto_11
    move-object v13, v2

    .line 402
    goto/16 :goto_7

    .line 383
    :pswitch_1d
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->j:Lsoftware/simplicial/nebulous/views/i;

    goto :goto_10

    .line 386
    :pswitch_1e
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->k:Lsoftware/simplicial/nebulous/views/i;

    goto :goto_10

    .line 389
    :pswitch_1f
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->l:Lsoftware/simplicial/nebulous/views/i;

    goto :goto_10

    .line 395
    :pswitch_20
    const/16 v4, 0x8

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_11

    .line 398
    :pswitch_21
    const/16 v4, 0xe

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_11

    .line 401
    :pswitch_22
    const/16 v4, 0x11

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_11

    .line 405
    :cond_26
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget v4, v4, Lsoftware/simplicial/a/e;->ln:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_27

    .line 407
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->b:Lsoftware/simplicial/nebulous/views/i;

    .line 408
    const/high16 v4, 0x41d00000    # 26.0f

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->g:F

    .line 409
    sget-object v4, Lsoftware/simplicial/nebulous/views/j$1;->a:[I

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget-object v5, v5, Lsoftware/simplicial/a/e;->ll:Lsoftware/simplicial/a/f;

    invoke-virtual {v5}, Lsoftware/simplicial/a/f;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_9

    :goto_12
    move-object v13, v2

    .line 425
    goto/16 :goto_7

    .line 412
    :pswitch_23
    const/4 v4, 0x5

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_12

    .line 415
    :pswitch_24
    const/16 v4, 0x8

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_12

    .line 418
    :pswitch_25
    const/16 v4, 0xb

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_12

    .line 421
    :pswitch_26
    const/16 v4, 0xe

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_12

    .line 424
    :pswitch_27
    const/16 v4, 0x11

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_12

    .line 428
    :cond_27
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget v4, v4, Lsoftware/simplicial/a/e;->lo:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_28

    .line 430
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->c:Lsoftware/simplicial/nebulous/views/i;

    .line 431
    const/high16 v4, 0x41b00000    # 22.0f

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->g:F

    .line 432
    sget-object v4, Lsoftware/simplicial/nebulous/views/j$1;->a:[I

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget-object v5, v5, Lsoftware/simplicial/a/e;->ll:Lsoftware/simplicial/a/f;

    invoke-virtual {v5}, Lsoftware/simplicial/a/f;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_a

    :goto_13
    move-object v13, v2

    .line 448
    goto/16 :goto_7

    .line 435
    :pswitch_28
    const/4 v4, 0x5

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_13

    .line 438
    :pswitch_29
    const/16 v4, 0x8

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_13

    .line 441
    :pswitch_2a
    const/16 v4, 0xb

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_13

    .line 444
    :pswitch_2b
    const/16 v4, 0xe

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_13

    .line 447
    :pswitch_2c
    const/16 v4, 0x11

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_13

    .line 451
    :cond_28
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget v4, v4, Lsoftware/simplicial/a/e;->lq:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_29

    .line 453
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    packed-switch v4, :pswitch_data_b

    .line 465
    :goto_14
    const/high16 v4, 0x41b00000    # 22.0f

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->g:F

    .line 466
    sget-object v4, Lsoftware/simplicial/nebulous/views/j$1;->a:[I

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget-object v5, v5, Lsoftware/simplicial/a/e;->ll:Lsoftware/simplicial/a/f;

    invoke-virtual {v5}, Lsoftware/simplicial/a/f;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_c

    :goto_15
    move-object v13, v2

    .line 488
    goto/16 :goto_7

    .line 456
    :pswitch_2d
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->g:Lsoftware/simplicial/nebulous/views/i;

    goto :goto_14

    .line 459
    :pswitch_2e
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->h:Lsoftware/simplicial/nebulous/views/i;

    goto :goto_14

    .line 462
    :pswitch_2f
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->i:Lsoftware/simplicial/nebulous/views/i;

    goto :goto_14

    .line 469
    :pswitch_30
    const/4 v4, 0x5

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_15

    .line 472
    :pswitch_31
    const/4 v4, 0x7

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_15

    .line 475
    :pswitch_32
    const/16 v4, 0x9

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_15

    .line 478
    :pswitch_33
    const/16 v4, 0xb

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_15

    .line 481
    :pswitch_34
    const/16 v4, 0xd

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_15

    .line 484
    :pswitch_35
    const/16 v4, 0xf

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_15

    .line 487
    :pswitch_36
    const/16 v4, 0x11

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_15

    .line 491
    :cond_29
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget v4, v4, Lsoftware/simplicial/a/e;->lr:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_2a

    .line 493
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->n:Lsoftware/simplicial/nebulous/views/i;

    .line 494
    const/high16 v4, 0x41700000    # 15.0f

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->g:F

    .line 495
    sget-object v4, Lsoftware/simplicial/nebulous/views/j$1;->a:[I

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget-object v5, v5, Lsoftware/simplicial/a/e;->ll:Lsoftware/simplicial/a/f;

    invoke-virtual {v5}, Lsoftware/simplicial/a/f;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_d

    :goto_16
    move-object v13, v2

    .line 511
    goto/16 :goto_7

    .line 498
    :pswitch_37
    const/4 v4, 0x5

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_16

    .line 501
    :pswitch_38
    const/16 v4, 0x8

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_16

    .line 504
    :pswitch_39
    const/16 v4, 0xb

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_16

    .line 507
    :pswitch_3a
    const/16 v4, 0xe

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_16

    .line 510
    :pswitch_3b
    const/16 v4, 0x11

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_16

    .line 514
    :cond_2a
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget v4, v4, Lsoftware/simplicial/a/e;->ls:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_2b

    .line 516
    const/high16 v4, 0x41800000    # 16.0f

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->g:F

    .line 517
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/4 v5, 0x6

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    packed-switch v4, :pswitch_data_e

    .line 538
    :goto_17
    sget-object v4, Lsoftware/simplicial/nebulous/views/j$1;->a:[I

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget-object v5, v5, Lsoftware/simplicial/a/e;->ll:Lsoftware/simplicial/a/f;

    invoke-virtual {v5}, Lsoftware/simplicial/a/f;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_f

    :goto_18
    move-object v13, v2

    .line 554
    goto/16 :goto_7

    .line 520
    :pswitch_3c
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->o:Lsoftware/simplicial/nebulous/views/i;

    goto :goto_17

    .line 523
    :pswitch_3d
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->p:Lsoftware/simplicial/nebulous/views/i;

    goto :goto_17

    .line 526
    :pswitch_3e
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->q:Lsoftware/simplicial/nebulous/views/i;

    goto :goto_17

    .line 529
    :pswitch_3f
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->r:Lsoftware/simplicial/nebulous/views/i;

    goto :goto_17

    .line 532
    :pswitch_40
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->s:Lsoftware/simplicial/nebulous/views/i;

    goto :goto_17

    .line 535
    :pswitch_41
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->t:Lsoftware/simplicial/nebulous/views/i;

    goto :goto_17

    .line 541
    :pswitch_42
    const/4 v4, 0x5

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_18

    .line 544
    :pswitch_43
    const/16 v4, 0x8

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_18

    .line 547
    :pswitch_44
    const/16 v4, 0xb

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_18

    .line 550
    :pswitch_45
    const/16 v4, 0xe

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_18

    .line 553
    :pswitch_46
    const/16 v4, 0x11

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_18

    .line 557
    :cond_2b
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget v4, v4, Lsoftware/simplicial/a/e;->ly:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_2c

    .line 559
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->u:Lsoftware/simplicial/nebulous/views/i;

    .line 560
    const/high16 v4, 0x41b00000    # 22.0f

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->g:F

    .line 561
    sget-object v4, Lsoftware/simplicial/nebulous/views/j$1;->a:[I

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget-object v5, v5, Lsoftware/simplicial/a/e;->ll:Lsoftware/simplicial/a/f;

    invoke-virtual {v5}, Lsoftware/simplicial/a/f;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_10

    :goto_19
    move-object v13, v2

    .line 577
    goto/16 :goto_7

    .line 564
    :pswitch_47
    const/4 v4, 0x5

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_19

    .line 567
    :pswitch_48
    const/16 v4, 0x8

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_19

    .line 570
    :pswitch_49
    const/16 v4, 0xb

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_19

    .line 573
    :pswitch_4a
    const/16 v4, 0xe

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_19

    .line 576
    :pswitch_4b
    const/16 v4, 0x11

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_19

    .line 580
    :cond_2c
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget v4, v4, Lsoftware/simplicial/a/e;->lz:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_2d

    .line 582
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->v:Lsoftware/simplicial/nebulous/views/i;

    .line 583
    const/high16 v4, 0x41800000    # 16.0f

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->g:F

    .line 584
    sget-object v4, Lsoftware/simplicial/nebulous/views/j$1;->a:[I

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget-object v5, v5, Lsoftware/simplicial/a/e;->ll:Lsoftware/simplicial/a/f;

    invoke-virtual {v5}, Lsoftware/simplicial/a/f;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_11

    :goto_1a
    move-object v13, v2

    .line 600
    goto/16 :goto_7

    .line 587
    :pswitch_4c
    const/4 v4, 0x5

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_1a

    .line 590
    :pswitch_4d
    const/16 v4, 0x8

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_1a

    .line 593
    :pswitch_4e
    const/16 v4, 0xb

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_1a

    .line 596
    :pswitch_4f
    const/16 v4, 0xe

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_1a

    .line 599
    :pswitch_50
    const/16 v4, 0x11

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    goto :goto_1a

    .line 603
    :cond_2d
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v5, Lsoftware/simplicial/a/e;->eb:Lsoftware/simplicial/a/e;

    if-ne v4, v5, :cond_2e

    .line 605
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->e:Lsoftware/simplicial/nebulous/views/i;

    .line 606
    const/16 v4, 0xa

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    move-object v13, v2

    goto/16 :goto_7

    .line 608
    :cond_2e
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v5, Lsoftware/simplicial/a/e;->ed:Lsoftware/simplicial/a/e;

    if-ne v4, v5, :cond_2f

    .line 610
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->f:Lsoftware/simplicial/nebulous/views/i;

    .line 611
    const/16 v4, 0xa

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    move-object v13, v2

    goto/16 :goto_7

    .line 613
    :cond_2f
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v5, Lsoftware/simplicial/a/e;->eH:Lsoftware/simplicial/a/e;

    if-ne v4, v5, :cond_30

    .line 615
    sget-object v2, Lsoftware/simplicial/nebulous/views/i;->m:Lsoftware/simplicial/nebulous/views/i;

    .line 616
    const/high16 v4, 0x41900000    # 18.0f

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->g:F

    .line 617
    const/16 v4, 0x8

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    move-object v13, v2

    goto/16 :goto_7

    .line 619
    :cond_30
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v5, Lsoftware/simplicial/a/e;->ev:Lsoftware/simplicial/a/e;

    if-eq v4, v5, :cond_31

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v5, Lsoftware/simplicial/a/e;->ew:Lsoftware/simplicial/a/e;

    if-eq v4, v5, :cond_31

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v5, Lsoftware/simplicial/a/e;->eT:Lsoftware/simplicial/a/e;

    if-eq v4, v5, :cond_31

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v5, Lsoftware/simplicial/a/e;->gb:Lsoftware/simplicial/a/e;

    if-eq v4, v5, :cond_31

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v5, Lsoftware/simplicial/a/e;->gx:Lsoftware/simplicial/a/e;

    if-eq v4, v5, :cond_31

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v5, Lsoftware/simplicial/a/e;->gA:Lsoftware/simplicial/a/e;

    if-eq v4, v5, :cond_31

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v5, Lsoftware/simplicial/a/e;->gK:Lsoftware/simplicial/a/e;

    if-ne v4, v5, :cond_32

    .line 622
    :cond_31
    const/16 v4, 0xa

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    move-object v13, v2

    goto/16 :goto_7

    .line 624
    :cond_32
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v5, Lsoftware/simplicial/a/e;->gk:Lsoftware/simplicial/a/e;

    if-eq v4, v5, :cond_33

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v5, Lsoftware/simplicial/a/e;->go:Lsoftware/simplicial/a/e;

    if-eq v4, v5, :cond_33

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v5, Lsoftware/simplicial/a/e;->gB:Lsoftware/simplicial/a/e;

    if-eq v4, v5, :cond_33

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v5, Lsoftware/simplicial/a/e;->iB:Lsoftware/simplicial/a/e;

    if-eq v4, v5, :cond_33

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v5, Lsoftware/simplicial/a/e;->ko:Lsoftware/simplicial/a/e;

    if-ne v4, v5, :cond_34

    .line 626
    :cond_33
    const/16 v4, 0xf

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    move-object v13, v2

    goto/16 :goto_7

    .line 628
    :cond_34
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v5, Lsoftware/simplicial/a/e;->gn:Lsoftware/simplicial/a/e;

    if-eq v4, v5, :cond_35

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    sget-object v5, Lsoftware/simplicial/a/e;->go:Lsoftware/simplicial/a/e;

    if-ne v4, v5, :cond_36

    .line 630
    :cond_35
    const/16 v4, 0xc

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    move-object v13, v2

    goto/16 :goto_7

    .line 632
    :cond_36
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    :cond_37
    move-object v13, v2

    goto/16 :goto_7

    .line 653
    :cond_38
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    if-gtz v2, :cond_39

    .line 655
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/nebulous/views/j;->d:Z

    goto/16 :goto_6

    .line 659
    :cond_39
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/j;->t:I

    move/from16 v0, p2

    if-lt v0, v2, :cond_f

    move-object/from16 v0, p1

    iget v2, v0, Lsoftware/simplicial/a/bf;->S:I

    if-lez v2, :cond_f

    .line 661
    const/4 v4, 0x0

    .line 662
    const/4 v3, 0x0

    .line 663
    const/4 v2, 0x0

    :goto_1b
    move-object/from16 v0, p1

    iget-object v5, v0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    array-length v5, v5

    if-ge v2, v5, :cond_3c

    .line 665
    move-object/from16 v0, p1

    iget-object v5, v0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v5, v5, v2

    .line 667
    invoke-virtual {v5}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v6

    if-eqz v6, :cond_3b

    .line 663
    :cond_3a
    :goto_1c
    add-int/lit8 v2, v2, 0x1

    goto :goto_1b

    .line 670
    :cond_3b
    iget v6, v5, Lsoftware/simplicial/a/bh;->w:F

    add-float/2addr v4, v6

    .line 671
    iget v6, v5, Lsoftware/simplicial/a/bh;->w:F

    cmpl-float v6, v6, v3

    if-lez v6, :cond_3a

    .line 672
    iget v3, v5, Lsoftware/simplicial/a/bh;->w:F

    goto :goto_1c

    .line 674
    :cond_3c
    sget v2, Lsoftware/simplicial/a/bh;->e:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v2, v5

    cmpg-float v2, v4, v2

    if-ltz v2, :cond_f

    .line 676
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v2

    mul-float v7, v2, v4

    .line 677
    const/4 v6, 0x0

    .line 678
    const/4 v4, 0x0

    .line 679
    const/4 v2, 0x0

    :goto_1d
    move-object/from16 v0, p1

    iget-object v5, v0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    array-length v5, v5

    if-ge v2, v5, :cond_50

    .line 681
    move-object/from16 v0, p1

    iget-object v5, v0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v5, v5, v2

    .line 683
    invoke-virtual {v5}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v8

    if-eqz v8, :cond_3e

    .line 679
    :cond_3d
    add-int/lit8 v2, v2, 0x1

    goto :goto_1d

    .line 686
    :cond_3e
    iget v8, v5, Lsoftware/simplicial/a/bh;->w:F

    add-float/2addr v4, v8

    .line 687
    cmpg-float v8, v7, v4

    if-gez v8, :cond_3d

    move-object v12, v5

    .line 693
    :goto_1e
    if-eqz v12, :cond_f

    .line 695
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/j;->a:[Lsoftware/simplicial/nebulous/views/d;

    iget v4, v12, Lsoftware/simplicial/a/bh;->c:I

    aget-object v14, v2, v4

    .line 698
    sget-object v2, Lsoftware/simplicial/nebulous/views/j$1;->b:[I

    invoke-virtual {v13}, Lsoftware/simplicial/nebulous/views/i;->ordinal()I

    move-result v4

    aget v2, v2, v4

    packed-switch v2, :pswitch_data_12

    .line 717
    const/4 v7, 0x0

    .line 720
    :goto_1f
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v2

    float-to-double v4, v2

    const-wide v8, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v4, v8

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    mul-double/2addr v4, v8

    double-to-float v15, v4

    .line 721
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v2

    iget v4, v12, Lsoftware/simplicial/a/bh;->w:F

    mul-float v16, v2, v4

    .line 723
    const/high16 v2, 0x42700000    # 60.0f

    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v5, 0x41700000    # 15.0f

    div-float/2addr v3, v5

    add-float/2addr v3, v4

    mul-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    float-to-int v2, v2

    add-int v2, v2, p2

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/j;->t:I

    .line 724
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/j;->b:I

    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/nebulous/views/j;->t:I

    sub-int v3, v3, p2

    mul-int/2addr v2, v3

    int-to-float v2, v2

    const/high16 v3, 0x42700000    # 60.0f

    div-float v11, v2, v3

    .line 726
    const/16 v4, 0xff

    .line 727
    const/16 v3, 0xff

    .line 728
    const/16 v2, 0xff

    .line 730
    iget v5, v12, Lsoftware/simplicial/a/bh;->s:F

    float-to-double v8, v5

    move/from16 v0, v16

    float-to-double v0, v0

    move-wide/from16 v18, v0

    float-to-double v0, v15

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->cos(D)D

    move-result-wide v20

    mul-double v18, v18, v20

    add-double v8, v8, v18

    double-to-float v10, v8

    .line 731
    iget v5, v12, Lsoftware/simplicial/a/bh;->t:F

    float-to-double v8, v5

    move/from16 v0, v16

    float-to-double v0, v0

    move-wide/from16 v18, v0

    float-to-double v0, v15

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->sin(D)D

    move-result-wide v20

    mul-double v18, v18, v20

    add-double v8, v8, v18

    double-to-float v9, v8

    .line 733
    const/4 v5, 0x0

    .line 734
    const/4 v6, 0x0

    .line 735
    const/4 v8, 0x0

    .line 741
    sget-object v17, Lsoftware/simplicial/nebulous/views/j$1;->a:[I

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lsoftware/simplicial/a/e;->ll:Lsoftware/simplicial/a/f;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lsoftware/simplicial/a/f;->ordinal()I

    move-result v18

    aget v17, v17, v18

    packed-switch v17, :pswitch_data_13

    :pswitch_51
    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1777
    :goto_20
    invoke-static {v11, v9, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v9

    .line 1779
    iget v12, v12, Lsoftware/simplicial/a/bh;->c:I

    move-object/from16 v2, p0

    move/from16 v11, p3

    invoke-virtual/range {v2 .. v13}, Lsoftware/simplicial/nebulous/views/j;->a(FFFFFFIFFILsoftware/simplicial/nebulous/views/i;)V

    goto/16 :goto_6

    .line 705
    :pswitch_52
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v2

    float-to-double v4, v2

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    mul-double/2addr v4, v6

    double-to-float v7, v4

    .line 706
    goto/16 :goto_1f

    .line 708
    :pswitch_53
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v2

    float-to-double v4, v2

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double/2addr v4, v6

    const-wide v6, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v4, v6

    double-to-float v7, v4

    .line 709
    goto/16 :goto_1f

    .line 711
    :pswitch_54
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v2

    float-to-double v4, v2

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x4020000000000000L    # 8.0

    div-double/2addr v4, v6

    const-wide v6, 0x3fc921fb54442d18L    # 0.19634954084936207

    sub-double/2addr v4, v6

    double-to-float v7, v4

    .line 712
    goto/16 :goto_1f

    .line 714
    :pswitch_55
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v2

    float-to-double v4, v2

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double/2addr v4, v6

    const-wide v6, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v4, v6

    double-to-float v7, v4

    .line 715
    goto/16 :goto_1f

    .line 772
    :pswitch_56
    iget-object v2, v14, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    const/high16 v3, 0x437f0000    # 255.0f

    mul-float/2addr v2, v3

    float-to-int v4, v2

    .line 773
    iget-object v2, v14, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    const/high16 v3, 0x437f0000    # 255.0f

    mul-float/2addr v2, v3

    float-to-int v3, v2

    .line 774
    iget-object v2, v14, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/4 v14, 0x0

    aget v2, v2, v14

    const/high16 v14, 0x437f0000    # 255.0f

    mul-float/2addr v2, v14

    float-to-int v2, v2

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 775
    goto/16 :goto_20

    .line 787
    :pswitch_57
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x88

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v4, v2, 0x78

    .line 788
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x88

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v3, v2, 0x78

    .line 789
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x88

    invoke-virtual {v2, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x78

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 790
    goto/16 :goto_20

    .line 792
    :pswitch_58
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x1e

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v4, v2, 0x4

    .line 793
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x1e

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v3, v2, 0x5a

    .line 794
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x1e

    invoke-virtual {v2, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0xab

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 795
    goto/16 :goto_20

    .line 797
    :pswitch_59
    const/16 v4, 0xff

    .line 798
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x28

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    rsub-int v3, v2, 0x9c

    .line 799
    const/4 v2, 0x0

    .line 800
    const/high16 v5, 0x42480000    # 50.0f

    iget v6, v12, Lsoftware/simplicial/a/bh;->s:F

    sub-float v6, v10, v6

    mul-float/2addr v5, v6

    div-float v5, v5, v16

    .line 801
    const/high16 v6, 0x42480000    # 50.0f

    iget v14, v12, Lsoftware/simplicial/a/bh;->t:F

    sub-float v14, v9, v14

    mul-float/2addr v6, v14

    div-float v6, v6, v16

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 802
    goto/16 :goto_20

    .line 806
    :pswitch_5a
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x88

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x46

    .line 807
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0x88

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x46

    .line 808
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v5, 0x88

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x78

    .line 809
    iget v5, v14, Lsoftware/simplicial/nebulous/views/d;->G:F

    const/high16 v6, 0x3f800000    # 1.0f

    cmpg-float v5, v5, v6

    if-gez v5, :cond_3f

    .line 811
    const/high16 v5, 0x42480000    # 50.0f

    iget v6, v12, Lsoftware/simplicial/a/bh;->s:F

    sub-float v6, v10, v6

    mul-float/2addr v5, v6

    div-float v5, v5, v16

    .line 812
    const/high16 v6, 0x42480000    # 50.0f

    iget v14, v12, Lsoftware/simplicial/a/bh;->t:F

    sub-float v14, v9, v14

    mul-float/2addr v6, v14

    div-float v6, v6, v16

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    goto/16 :goto_20

    .line 816
    :cond_3f
    const/4 v5, 0x0

    .line 817
    const/4 v6, 0x0

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 819
    goto/16 :goto_20

    .line 821
    :pswitch_5b
    iget-object v2, v14, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    const/high16 v3, 0x437f0000    # 255.0f

    mul-float/2addr v2, v3

    float-to-int v4, v2

    .line 822
    iget-object v2, v14, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    const/high16 v3, 0x437f0000    # 255.0f

    mul-float/2addr v2, v3

    float-to-int v3, v2

    .line 823
    iget-object v2, v14, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/4 v5, 0x0

    aget v2, v2, v5

    const/high16 v5, 0x437f0000    # 255.0f

    mul-float/2addr v2, v5

    float-to-int v2, v2

    .line 824
    iget v5, v14, Lsoftware/simplicial/nebulous/views/d;->G:F

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x42c80000    # 100.0f

    iget v15, v12, Lsoftware/simplicial/a/bh;->s:F

    sub-float v15, v10, v15

    mul-float/2addr v6, v15

    div-float v6, v6, v16

    mul-float/2addr v5, v6

    .line 825
    iget v6, v14, Lsoftware/simplicial/nebulous/views/d;->G:F

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    const/high16 v14, 0x42c80000    # 100.0f

    iget v15, v12, Lsoftware/simplicial/a/bh;->t:F

    sub-float v15, v9, v15

    mul-float/2addr v14, v15

    div-float v14, v14, v16

    mul-float/2addr v6, v14

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 826
    goto/16 :goto_20

    .line 828
    :pswitch_5c
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x8c

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    .line 830
    add-int/lit8 v3, v4, 0x64

    .line 831
    const/16 v2, 0xff

    .line 832
    const/high16 v5, 0x42480000    # 50.0f

    iget v6, v12, Lsoftware/simplicial/a/bh;->s:F

    sub-float v6, v10, v6

    mul-float/2addr v5, v6

    div-float v5, v5, v16

    .line 833
    const/high16 v6, 0x42480000    # 50.0f

    iget v14, v12, Lsoftware/simplicial/a/bh;->t:F

    sub-float v14, v9, v14

    mul-float/2addr v6, v14

    div-float v6, v6, v16

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 834
    goto/16 :goto_20

    .line 836
    :pswitch_5d
    const/16 v4, 0x9c

    .line 837
    const/16 v3, 0xd0

    .line 838
    const/16 v2, 0xf0

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 839
    goto/16 :goto_20

    .line 841
    :pswitch_5e
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x28

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    .line 842
    add-int/lit16 v4, v2, 0xc8

    .line 843
    add-int/lit8 v3, v2, 0x5a

    .line 844
    div-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, 0xa

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 845
    goto/16 :goto_20

    .line 847
    :pswitch_5f
    const/16 v4, 0x1f

    .line 848
    const/16 v3, 0xb6

    .line 849
    const/16 v2, 0x31

    .line 850
    const/high16 v5, 0x42480000    # 50.0f

    iget v6, v12, Lsoftware/simplicial/a/bh;->s:F

    sub-float v6, v10, v6

    mul-float/2addr v5, v6

    div-float v5, v5, v16

    .line 851
    const/high16 v6, 0x42480000    # 50.0f

    iget v14, v12, Lsoftware/simplicial/a/bh;->t:F

    sub-float v14, v9, v14

    mul-float/2addr v6, v14

    div-float v6, v6, v16

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 852
    goto/16 :goto_20

    .line 854
    :pswitch_60
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x28

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    .line 855
    const/16 v4, 0xfa

    .line 856
    add-int/lit8 v3, v2, 0x5a

    .line 857
    add-int/lit8 v2, v2, 0xa

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 858
    goto/16 :goto_20

    .line 860
    :pswitch_61
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x88

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v4, v2, 0x78

    .line 861
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x88

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v3, v2, 0x78

    .line 862
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v5, 0x88

    invoke-virtual {v2, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x78

    .line 863
    sget-object v5, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v5}, Ljava/util/Random;->nextFloat()F

    move-result v5

    float-to-double v14, v5

    const-wide v16, 0x400921fb54442d18L    # Math.PI

    mul-double v14, v14, v16

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    mul-double v14, v14, v16

    double-to-float v6, v14

    .line 864
    const-wide/high16 v14, 0x4044000000000000L    # 40.0

    float-to-double v0, v6

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->cos(D)D

    move-result-wide v16

    mul-double v14, v14, v16

    double-to-float v5, v14

    .line 865
    const-wide/high16 v14, 0x4044000000000000L    # 40.0

    float-to-double v0, v6

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sin(D)D

    move-result-wide v16

    mul-double v14, v14, v16

    double-to-float v6, v14

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 866
    goto/16 :goto_20

    .line 868
    :pswitch_62
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x88

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v4, v2, 0x78

    .line 869
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x88

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v3, v2, 0x78

    .line 870
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v5, 0x88

    invoke-virtual {v2, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x78

    .line 871
    sget-object v5, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v5}, Ljava/util/Random;->nextFloat()F

    move-result v5

    float-to-double v14, v5

    const-wide v16, 0x400921fb54442d18L    # Math.PI

    mul-double v14, v14, v16

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    mul-double v14, v14, v16

    double-to-float v6, v14

    .line 872
    const-wide/high16 v14, 0x403e000000000000L    # 30.0

    float-to-double v0, v6

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->cos(D)D

    move-result-wide v16

    mul-double v14, v14, v16

    double-to-float v5, v14

    .line 873
    const-wide/high16 v14, 0x403e000000000000L    # 30.0

    float-to-double v0, v6

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sin(D)D

    move-result-wide v16

    mul-double v14, v14, v16

    double-to-float v6, v14

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 874
    goto/16 :goto_20

    .line 881
    :pswitch_63
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0xec

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v4, v2, 0x14

    .line 882
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0xec

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v3, v2, 0x14

    .line 883
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0xec

    invoke-virtual {v2, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x14

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 884
    goto/16 :goto_20

    .line 886
    :pswitch_64
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x24

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v4, v2, 0xdc

    .line 887
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x24

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v3, v2, 0xdc

    .line 888
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v8, 0x24

    invoke-virtual {v2, v8}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0xdc

    .line 889
    const/high16 v8, 0x40000000    # 2.0f

    const/high16 v14, 0x40800000    # 4.0f

    sget-object v15, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v15}, Ljava/util/Random;->nextFloat()F

    move-result v15

    mul-float/2addr v14, v15

    sub-float/2addr v8, v14

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 890
    goto/16 :goto_20

    .line 892
    :pswitch_65
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x24

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    .line 893
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0x24

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x69

    .line 894
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x24

    invoke-virtual {v4, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit16 v4, v4, 0xab

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 895
    goto/16 :goto_20

    .line 897
    :pswitch_66
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x24

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v4, v2, 0x4b

    .line 898
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x24

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v3, v2, 0xdc

    .line 899
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x24

    invoke-virtual {v2, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x32

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 900
    goto/16 :goto_20

    .line 902
    :pswitch_67
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x24

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v4, v2, 0x4b

    .line 903
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x24

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v3, v2, 0xdc

    .line 904
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x24

    invoke-virtual {v2, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x32

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 905
    goto/16 :goto_20

    .line 908
    :pswitch_68
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x24

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0xaa

    .line 909
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0x24

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit16 v3, v3, 0xaa

    .line 910
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x24

    invoke-virtual {v4, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit16 v4, v4, 0xdc

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 911
    goto/16 :goto_20

    .line 913
    :pswitch_69
    iget v2, v14, Lsoftware/simplicial/nebulous/views/d;->l:F

    const/high16 v3, 0x40000000    # 2.0f

    cmpl-float v2, v2, v3

    if-nez v2, :cond_40

    .line 915
    const/16 v2, 0x32

    .line 916
    const/16 v3, 0x32

    .line 917
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x38

    invoke-virtual {v4, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit16 v4, v4, 0xc8

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    goto/16 :goto_20

    .line 921
    :cond_40
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x38

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0xc8

    .line 922
    const/16 v3, 0x32

    .line 923
    const/16 v4, 0x32

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 925
    goto/16 :goto_20

    .line 927
    :pswitch_6a
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x24

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x52

    .line 928
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0x24

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit16 v3, v3, 0xdc

    .line 929
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x24

    invoke-virtual {v4, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit16 v4, v4, 0xc4

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 930
    goto/16 :goto_20

    .line 932
    :pswitch_6b
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x14

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0xec

    .line 933
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0x38

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit16 v3, v3, 0xc8

    .line 934
    const/4 v4, 0x0

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 935
    goto/16 :goto_20

    .line 937
    :pswitch_6c
    const/16 v2, 0xff

    .line 938
    const/16 v3, 0x83

    .line 939
    const/16 v4, 0xba

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 940
    goto/16 :goto_20

    .line 942
    :pswitch_6d
    iget-object v2, v14, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    const/high16 v3, 0x437f0000    # 255.0f

    mul-float/2addr v2, v3

    float-to-int v4, v2

    .line 943
    iget-object v2, v14, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    const/high16 v3, 0x437f0000    # 255.0f

    mul-float/2addr v2, v3

    float-to-int v3, v2

    .line 944
    iget-object v2, v14, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/4 v6, 0x0

    aget v2, v2, v6

    const/high16 v6, 0x437f0000    # 255.0f

    mul-float/2addr v2, v6

    float-to-int v2, v2

    .line 945
    const/high16 v6, 0x40f00000    # 7.5f

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 946
    goto/16 :goto_20

    .line 948
    :pswitch_6e
    iget-object v2, v14, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    const/high16 v3, 0x437f0000    # 255.0f

    mul-float/2addr v2, v3

    float-to-int v4, v2

    .line 949
    iget-object v2, v14, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    const/high16 v3, 0x437f0000    # 255.0f

    mul-float/2addr v2, v3

    float-to-int v3, v2

    .line 950
    iget-object v2, v14, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/4 v6, 0x0

    aget v2, v2, v6

    const/high16 v6, 0x437f0000    # 255.0f

    mul-float/2addr v2, v6

    float-to-int v2, v2

    .line 951
    const/high16 v6, -0x3f100000    # -7.5f

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 952
    goto/16 :goto_20

    .line 954
    :pswitch_6f
    iget-object v2, v14, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    const/high16 v3, 0x437f0000    # 255.0f

    mul-float/2addr v2, v3

    float-to-int v4, v2

    .line 955
    iget-object v2, v14, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    const/high16 v3, 0x437f0000    # 255.0f

    mul-float/2addr v2, v3

    float-to-int v3, v2

    .line 956
    iget-object v2, v14, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/4 v5, 0x0

    aget v2, v2, v5

    const/high16 v5, 0x437f0000    # 255.0f

    mul-float/2addr v2, v5

    float-to-int v2, v2

    .line 957
    const/high16 v5, 0x40f00000    # 7.5f

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 958
    goto/16 :goto_20

    .line 960
    :pswitch_70
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x14

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0xa9

    .line 961
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0x14

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit16 v3, v3, 0xef

    .line 962
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v5, 0x14

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit16 v4, v4, 0xa5

    .line 963
    const/high16 v5, 0x40b00000    # 5.5f

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 964
    goto/16 :goto_20

    .line 966
    :pswitch_71
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x1e

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    .line 967
    const/16 v4, 0xfa

    .line 968
    add-int/lit16 v3, v2, 0xdb

    .line 969
    add-int/lit8 v2, v2, 0x7a

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 970
    goto/16 :goto_20

    .line 974
    :pswitch_72
    const-wide v2, 0x4012d97c7f3321d2L    # 4.71238898038469

    iget v4, v14, Lsoftware/simplicial/nebulous/views/d;->l:F

    float-to-double v4, v4

    sub-double/2addr v2, v4

    double-to-float v4, v2

    .line 975
    float-to-double v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    const-wide/high16 v14, 0x403e000000000000L    # 30.0

    mul-double/2addr v2, v14

    double-to-float v3, v2

    .line 976
    float-to-double v14, v4

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    const-wide/high16 v16, 0x403e000000000000L    # 30.0

    mul-double v14, v14, v16

    double-to-float v2, v14

    .line 977
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/views/j;->f:Lsoftware/simplicial/a/e;

    iget-object v5, v5, Lsoftware/simplicial/a/e;->ll:Lsoftware/simplicial/a/f;

    sget-object v6, Lsoftware/simplicial/a/f;->kt:Lsoftware/simplicial/a/f;

    if-ne v5, v6, :cond_41

    .line 979
    neg-float v3, v3

    .line 980
    neg-float v2, v2

    .line 983
    :cond_41
    iget v6, v12, Lsoftware/simplicial/a/bh;->w:F

    .line 984
    iget v5, v12, Lsoftware/simplicial/a/bh;->s:F

    float-to-double v14, v5

    float-to-double v0, v6

    move-wide/from16 v16, v0

    float-to-double v0, v4

    move-wide/from16 v18, v0

    const-wide v20, 0x3ff921fb54442d18L    # 1.5707963267948966

    add-double v18, v18, v20

    sget-object v5, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v5}, Ljava/util/Random;->nextFloat()F

    move-result v5

    float-to-double v0, v5

    move-wide/from16 v20, v0

    const-wide v22, 0x400921fb54442d18L    # Math.PI

    mul-double v20, v20, v22

    add-double v18, v18, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->cos(D)D

    move-result-wide v18

    mul-double v16, v16, v18

    add-double v14, v14, v16

    double-to-float v5, v14

    .line 985
    iget v9, v12, Lsoftware/simplicial/a/bh;->t:F

    float-to-double v14, v9

    float-to-double v0, v6

    move-wide/from16 v16, v0

    float-to-double v0, v4

    move-wide/from16 v18, v0

    const-wide v20, 0x3ff921fb54442d18L    # 1.5707963267948966

    add-double v18, v18, v20

    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v4}, Ljava/util/Random;->nextFloat()F

    move-result v4

    float-to-double v0, v4

    move-wide/from16 v20, v0

    const-wide v22, 0x400921fb54442d18L    # Math.PI

    mul-double v20, v20, v22

    add-double v18, v18, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->sin(D)D

    move-result-wide v18

    mul-double v16, v16, v18

    add-double v14, v14, v16

    double-to-float v4, v14

    .line 987
    sget-object v6, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v9, 0x38

    invoke-virtual {v6, v9}, Ljava/util/Random;->nextInt(I)I

    move-result v6

    add-int/lit16 v6, v6, 0xc8

    .line 988
    sget-object v9, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v10, 0xc8

    invoke-virtual {v9, v10}, Ljava/util/Random;->nextInt(I)I

    move-result v9

    add-int/lit8 v9, v9, 0x19

    .line 989
    const/4 v10, 0x0

    move/from16 v24, v2

    move v2, v6

    move/from16 v6, v24

    move/from16 v25, v5

    move v5, v3

    move/from16 v3, v25

    move/from16 v26, v10

    move v10, v11

    move/from16 v11, v26

    .line 990
    goto/16 :goto_20

    .line 992
    :pswitch_73
    const-wide v2, 0x3ff921fb54442d18L    # 1.5707963267948966

    iget v4, v14, Lsoftware/simplicial/nebulous/views/d;->l:F

    float-to-double v4, v4

    sub-double/2addr v2, v4

    double-to-float v2, v2

    .line 993
    float-to-double v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    iget v3, v14, Lsoftware/simplicial/nebulous/views/d;->G:F

    float-to-double v0, v3

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->cos(D)D

    move-result-wide v16

    mul-double v4, v4, v16

    const-wide/high16 v16, 0x402e000000000000L    # 15.0

    mul-double v4, v4, v16

    double-to-float v5, v4

    .line 994
    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    iget v4, v14, Lsoftware/simplicial/nebulous/views/d;->G:F

    float-to-double v14, v4

    invoke-static {v14, v15}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    mul-double/2addr v2, v14

    const-wide/high16 v14, 0x402e000000000000L    # 15.0

    mul-double/2addr v2, v14

    double-to-float v6, v2

    .line 996
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x100

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    .line 998
    rsub-int v3, v2, 0xff

    .line 999
    const/16 v4, 0xc8

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1000
    goto/16 :goto_20

    .line 1002
    :pswitch_74
    const-wide v2, 0x3ff921fb54442d18L    # 1.5707963267948966

    iget v4, v14, Lsoftware/simplicial/nebulous/views/d;->l:F

    float-to-double v4, v4

    sub-double/2addr v2, v4

    double-to-float v2, v2

    .line 1003
    float-to-double v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    const-wide/high16 v14, 0x402e000000000000L    # 15.0

    mul-double/2addr v4, v14

    double-to-float v5, v4

    .line 1004
    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    const-wide/high16 v14, 0x402e000000000000L    # 15.0

    mul-double/2addr v2, v14

    double-to-float v6, v2

    .line 1006
    const/4 v2, 0x0

    .line 1007
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0x9c

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x64

    .line 1008
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x38

    invoke-virtual {v4, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit16 v4, v4, 0xc8

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1009
    goto/16 :goto_20

    .line 1011
    :pswitch_75
    const-wide v2, 0x3ff921fb54442d18L    # 1.5707963267948966

    iget v4, v14, Lsoftware/simplicial/nebulous/views/d;->l:F

    float-to-double v4, v4

    sub-double/2addr v2, v4

    double-to-float v2, v2

    .line 1012
    float-to-double v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    const-wide/high16 v14, 0x402e000000000000L    # 15.0

    mul-double/2addr v4, v14

    double-to-float v5, v4

    .line 1013
    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    const-wide/high16 v14, 0x402e000000000000L    # 15.0

    mul-double/2addr v2, v14

    double-to-float v6, v2

    .line 1015
    const/16 v2, 0xff

    .line 1016
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0x100

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x0

    .line 1017
    const/16 v4, 0xff

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1018
    goto/16 :goto_20

    .line 1021
    :pswitch_76
    iget-boolean v2, v14, Lsoftware/simplicial/nebulous/views/d;->m:Z

    if-eqz v2, :cond_42

    .line 1023
    const/16 v4, 0x80

    .line 1024
    const/16 v3, 0xff

    .line 1025
    const/16 v2, 0xff

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    goto/16 :goto_20

    .line 1029
    :cond_42
    const/16 v4, 0x80

    .line 1030
    const/16 v3, 0x80

    .line 1031
    const/16 v2, 0x80

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1033
    goto/16 :goto_20

    .line 1035
    :pswitch_77
    iget-boolean v2, v14, Lsoftware/simplicial/nebulous/views/d;->m:Z

    if-eqz v2, :cond_43

    .line 1037
    const/16 v4, 0x84

    .line 1038
    const/16 v3, 0x24

    .line 1039
    const/16 v2, 0xff

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    goto/16 :goto_20

    .line 1043
    :cond_43
    const/16 v4, 0xfd

    .line 1044
    const/16 v3, 0xca

    .line 1045
    const/16 v2, 0xaf

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1047
    goto/16 :goto_20

    .line 1050
    :pswitch_78
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x78

    .line 1051
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0x10

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x78

    .line 1052
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x10

    invoke-virtual {v4, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x78

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1053
    goto/16 :goto_20

    .line 1055
    :pswitch_79
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x28

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0x96

    .line 1056
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0x28

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit16 v3, v3, 0x96

    .line 1057
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x28

    invoke-virtual {v4, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit16 v4, v4, 0x96

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1058
    goto/16 :goto_20

    .line 1061
    :pswitch_7a
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x24

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x7b

    .line 1062
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0x24

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x6a

    .line 1063
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/4 v14, 0x2

    invoke-virtual {v4, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit16 v4, v4, 0xfe

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1064
    goto/16 :goto_20

    .line 1066
    :pswitch_7b
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x24

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0xdc

    .line 1067
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x3d

    .line 1068
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0xa

    invoke-virtual {v4, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x42

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1069
    goto/16 :goto_20

    .line 1071
    :pswitch_7c
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x100

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    .line 1074
    const/16 v4, 0xff

    move v3, v10

    move v10, v11

    move v11, v4

    move v4, v9

    move v9, v2

    .line 1075
    goto/16 :goto_20

    .line 1077
    :pswitch_7d
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    .line 1078
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0x32

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit16 v3, v3, 0x9c

    .line 1079
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x32

    invoke-virtual {v4, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x64

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1080
    goto/16 :goto_20

    .line 1082
    :pswitch_7e
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextBoolean()Z

    move-result v2

    if-eqz v2, :cond_44

    .line 1084
    const/16 v2, 0xd3

    .line 1085
    const/16 v3, 0x47

    .line 1086
    const/16 v4, 0xd6

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    goto/16 :goto_20

    .line 1090
    :cond_44
    const/16 v2, 0xf6

    .line 1091
    const/16 v3, 0xfc

    .line 1092
    const/16 v4, 0x4a

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1094
    goto/16 :goto_20

    .line 1098
    :pswitch_7f
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x24

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0xdc

    .line 1099
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0x24

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit16 v3, v3, 0xdc

    .line 1100
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x24

    invoke-virtual {v4, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit16 v4, v4, 0xdc

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1101
    goto/16 :goto_20

    .line 1103
    :pswitch_80
    const-wide v2, 0x3ff921fb54442d18L    # 1.5707963267948966

    iget v4, v14, Lsoftware/simplicial/nebulous/views/d;->l:F

    float-to-double v4, v4

    sub-double/2addr v2, v4

    double-to-float v2, v2

    .line 1104
    const v3, 0x3e99999a    # 0.3f

    add-float/2addr v3, v2

    const v4, 0x3f19999a    # 0.6f

    sget-object v5, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v5}, Ljava/util/Random;->nextFloat()F

    move-result v5

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    .line 1105
    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    sget-object v6, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v6}, Ljava/util/Random;->nextFloat()F

    move-result v6

    const/high16 v9, 0x42480000    # 50.0f

    mul-float/2addr v6, v9

    const/high16 v9, 0x42960000    # 75.0f

    add-float/2addr v6, v9

    float-to-double v0, v6

    move-wide/from16 v16, v0

    mul-double v4, v4, v16

    double-to-float v5, v4

    .line 1106
    float-to-double v0, v3

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sin(D)D

    move-result-wide v16

    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextFloat()F

    move-result v3

    const/high16 v4, 0x42480000    # 50.0f

    mul-float/2addr v3, v4

    const/high16 v4, 0x42960000    # 75.0f

    add-float/2addr v3, v4

    float-to-double v0, v3

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-float v6, v0

    .line 1108
    iget v3, v12, Lsoftware/simplicial/a/bh;->s:F

    float-to-double v0, v2

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->cos(D)D

    move-result-wide v16

    iget v4, v12, Lsoftware/simplicial/a/bh;->w:F

    float-to-double v0, v4

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-float v4, v0

    add-float/2addr v3, v4

    .line 1109
    iget v4, v12, Lsoftware/simplicial/a/bh;->t:F

    float-to-double v0, v2

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sin(D)D

    move-result-wide v16

    iget v2, v12, Lsoftware/simplicial/a/bh;->w:F

    float-to-double v0, v2

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-float v2, v0

    add-float/2addr v4, v2

    .line 1111
    iget-object v2, v14, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/4 v9, 0x2

    aget v2, v2, v9

    const/high16 v9, 0x437f0000    # 255.0f

    mul-float/2addr v2, v9

    float-to-int v10, v2

    .line 1112
    iget-object v2, v14, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/4 v9, 0x1

    aget v2, v2, v9

    const/high16 v9, 0x437f0000    # 255.0f

    mul-float/2addr v2, v9

    float-to-int v9, v2

    .line 1113
    iget-object v2, v14, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/4 v14, 0x0

    aget v2, v2, v14

    const/high16 v14, 0x437f0000    # 255.0f

    mul-float/2addr v2, v14

    float-to-int v2, v2

    move/from16 v24, v10

    move v10, v11

    move/from16 v11, v24

    .line 1114
    goto/16 :goto_20

    .line 1116
    :pswitch_81
    const-wide v2, 0x3ff921fb54442d18L    # 1.5707963267948966

    iget v4, v14, Lsoftware/simplicial/nebulous/views/d;->l:F

    float-to-double v4, v4

    sub-double/2addr v2, v4

    double-to-float v2, v2

    .line 1117
    const/4 v5, 0x0

    .line 1118
    const/4 v6, 0x0

    .line 1119
    iget v3, v12, Lsoftware/simplicial/a/bh;->s:F

    float-to-double v14, v2

    invoke-static {v14, v15}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    iget v4, v12, Lsoftware/simplicial/a/bh;->w:F

    float-to-double v0, v4

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    double-to-float v4, v14

    add-float/2addr v3, v4

    .line 1120
    iget v4, v12, Lsoftware/simplicial/a/bh;->t:F

    float-to-double v14, v2

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    iget v2, v12, Lsoftware/simplicial/a/bh;->w:F

    float-to-double v0, v2

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    double-to-float v2, v14

    add-float/2addr v4, v2

    .line 1122
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v9, 0x64

    invoke-virtual {v2, v9}, Ljava/util/Random;->nextInt(I)I

    move-result v9

    .line 1123
    add-int/lit16 v2, v9, 0x9c

    .line 1124
    mul-int/lit8 v9, v9, 0x2

    add-int/lit8 v9, v9, 0x14

    .line 1125
    const/4 v10, 0x0

    move/from16 v24, v10

    move v10, v11

    move/from16 v11, v24

    .line 1126
    goto/16 :goto_20

    .line 1130
    :pswitch_82
    const/high16 v2, 0x3f000000    # 0.5f

    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextFloat()F

    move-result v3

    sub-float v9, v2, v3

    .line 1131
    iget v2, v14, Lsoftware/simplicial/nebulous/views/d;->l:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_45

    iget v2, v14, Lsoftware/simplicial/nebulous/views/d;->l:F

    float-to-double v2, v2

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    cmpg-double v2, v2, v4

    if-gez v2, :cond_45

    .line 1133
    const-wide/16 v2, 0x0

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    const/high16 v6, 0x3e800000    # 0.25f

    const v10, 0x3dcccccd    # 0.1f

    mul-float/2addr v10, v9

    add-float/2addr v6, v10

    float-to-double v14, v6

    mul-double/2addr v4, v14

    sub-double/2addr v2, v4

    double-to-float v3, v2

    .line 1134
    const/4 v2, 0x0

    .line 1141
    :goto_21
    const/high16 v4, 0x42960000    # 75.0f

    const/high16 v5, 0x42480000    # 50.0f

    sget-object v6, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v6}, Ljava/util/Random;->nextFloat()F

    move-result v6

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    .line 1142
    float-to-double v14, v3

    invoke-static {v14, v15}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    float-to-double v0, v4

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    double-to-float v5, v14

    .line 1143
    float-to-double v14, v3

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    float-to-double v0, v4

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    double-to-float v6, v14

    .line 1145
    iget v3, v12, Lsoftware/simplicial/a/bh;->s:F

    float-to-double v14, v2

    invoke-static {v14, v15}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    iget v4, v12, Lsoftware/simplicial/a/bh;->w:F

    float-to-double v0, v4

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    double-to-float v4, v14

    add-float/2addr v3, v4

    .line 1146
    iget v4, v12, Lsoftware/simplicial/a/bh;->t:F

    float-to-double v14, v2

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    iget v2, v12, Lsoftware/simplicial/a/bh;->w:F

    float-to-double v0, v2

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    double-to-float v2, v14

    add-float/2addr v4, v2

    .line 1148
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/4 v10, 0x5

    invoke-virtual {v2, v10}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0xc8

    .line 1149
    sget-object v10, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/high16 v14, 0x43fa0000    # 500.0f

    const/high16 v15, 0x3f000000    # 0.5f

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    sub-float v9, v15, v9

    mul-float/2addr v9, v14

    float-to-int v9, v9

    add-int/lit8 v9, v9, 0x1

    invoke-virtual {v10, v9}, Ljava/util/Random;->nextInt(I)I

    move-result v9

    add-int/lit8 v9, v9, 0x0

    .line 1150
    const/4 v10, 0x0

    move/from16 v24, v10

    move v10, v11

    move/from16 v11, v24

    .line 1151
    goto/16 :goto_20

    .line 1138
    :cond_45
    const-wide v2, 0x400921fb54442d18L    # Math.PI

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    const/high16 v6, 0x3e800000    # 0.25f

    const v10, 0x3dcccccd    # 0.1f

    mul-float/2addr v10, v9

    add-float/2addr v6, v10

    float-to-double v14, v6

    mul-double/2addr v4, v14

    add-double/2addr v2, v4

    double-to-float v3, v2

    .line 1139
    const v2, 0x40490fdb    # (float)Math.PI

    goto/16 :goto_21

    .line 1153
    :pswitch_83
    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v11, v2

    .line 1154
    const-wide v2, 0x3ff921fb54442d18L    # 1.5707963267948966

    iget v4, v14, Lsoftware/simplicial/nebulous/views/d;->l:F

    float-to-double v4, v4

    sub-double/2addr v2, v4

    double-to-float v2, v2

    .line 1155
    const v3, 0x3e99999a    # 0.3f

    add-float/2addr v3, v2

    const v4, 0x3f19999a    # 0.6f

    sget-object v5, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v5}, Ljava/util/Random;->nextFloat()F

    move-result v5

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    .line 1157
    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    sget-object v6, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v6}, Ljava/util/Random;->nextFloat()F

    move-result v6

    const/high16 v9, 0x41c80000    # 25.0f

    mul-float/2addr v6, v9

    const/high16 v9, 0x41c80000    # 25.0f

    add-float/2addr v6, v9

    float-to-double v0, v6

    move-wide/from16 v16, v0

    mul-double v4, v4, v16

    double-to-float v4, v4

    neg-float v5, v4

    .line 1158
    float-to-double v0, v3

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sin(D)D

    move-result-wide v16

    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextFloat()F

    move-result v3

    const/high16 v4, 0x41c80000    # 25.0f

    mul-float/2addr v3, v4

    const/high16 v4, 0x41c80000    # 25.0f

    add-float/2addr v3, v4

    float-to-double v0, v3

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-float v3, v0

    neg-float v6, v3

    .line 1160
    iget v3, v12, Lsoftware/simplicial/a/bh;->s:F

    float-to-double v0, v2

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->cos(D)D

    move-result-wide v16

    iget v4, v12, Lsoftware/simplicial/a/bh;->w:F

    float-to-double v0, v4

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-float v4, v0

    add-float/2addr v3, v4

    const v4, 0x3e99999a    # 0.3f

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    .line 1161
    iget v4, v12, Lsoftware/simplicial/a/bh;->t:F

    float-to-double v0, v2

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sin(D)D

    move-result-wide v16

    iget v2, v12, Lsoftware/simplicial/a/bh;->w:F

    float-to-double v0, v2

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-float v2, v0

    add-float/2addr v2, v4

    const v4, 0x3e99999a    # 0.3f

    mul-float/2addr v4, v6

    sub-float v4, v2, v4

    .line 1163
    iget-object v2, v14, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/4 v9, 0x2

    aget v2, v2, v9

    const/high16 v9, 0x437f0000    # 255.0f

    mul-float/2addr v2, v9

    float-to-int v10, v2

    .line 1164
    iget-object v2, v14, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/4 v9, 0x1

    aget v2, v2, v9

    const/high16 v9, 0x437f0000    # 255.0f

    mul-float/2addr v2, v9

    float-to-int v9, v2

    .line 1165
    iget-object v2, v14, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/4 v14, 0x0

    aget v2, v2, v14

    const/high16 v14, 0x437f0000    # 255.0f

    mul-float/2addr v2, v14

    float-to-int v2, v2

    move/from16 v24, v10

    move v10, v11

    move/from16 v11, v24

    .line 1166
    goto/16 :goto_20

    .line 1168
    :pswitch_84
    const-wide v2, 0x3ff921fb54442d18L    # 1.5707963267948966

    iget v4, v14, Lsoftware/simplicial/nebulous/views/d;->l:F

    float-to-double v4, v4

    sub-double/2addr v2, v4

    double-to-float v2, v2

    .line 1169
    float-to-double v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    const-wide v14, 0x4052c00000000000L    # 75.0

    mul-double/2addr v4, v14

    neg-double v4, v4

    double-to-float v5, v4

    .line 1170
    float-to-double v14, v2

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    const-wide v16, 0x4052c00000000000L    # 75.0

    mul-double v14, v14, v16

    neg-double v14, v14

    double-to-float v6, v14

    .line 1172
    iget v4, v12, Lsoftware/simplicial/a/bh;->w:F

    .line 1173
    iget v3, v12, Lsoftware/simplicial/a/bh;->s:F

    float-to-double v14, v3

    float-to-double v0, v4

    move-wide/from16 v16, v0

    float-to-double v0, v2

    move-wide/from16 v18, v0

    const-wide v20, 0x3ff921fb54442d18L    # 1.5707963267948966

    sub-double v18, v18, v20

    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextFloat()F

    move-result v3

    float-to-double v0, v3

    move-wide/from16 v20, v0

    const-wide v22, 0x400921fb54442d18L    # Math.PI

    mul-double v20, v20, v22

    add-double v18, v18, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->cos(D)D

    move-result-wide v18

    mul-double v16, v16, v18

    add-double v14, v14, v16

    double-to-float v3, v14

    .line 1174
    iget v9, v12, Lsoftware/simplicial/a/bh;->t:F

    float-to-double v14, v9

    float-to-double v0, v4

    move-wide/from16 v16, v0

    float-to-double v0, v2

    move-wide/from16 v18, v0

    const-wide v20, 0x3ff921fb54442d18L    # 1.5707963267948966

    sub-double v18, v18, v20

    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v2

    float-to-double v0, v2

    move-wide/from16 v20, v0

    const-wide v22, 0x400921fb54442d18L    # Math.PI

    mul-double v20, v20, v22

    add-double v18, v18, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->sin(D)D

    move-result-wide v18

    mul-double v16, v16, v18

    add-double v14, v14, v16

    double-to-float v4, v14

    .line 1176
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v9, 0x64

    invoke-virtual {v2, v9}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0x96

    .line 1177
    const/4 v9, 0x0

    .line 1178
    const/4 v10, 0x0

    move/from16 v24, v10

    move v10, v11

    move/from16 v11, v24

    .line 1179
    goto/16 :goto_20

    .line 1182
    :pswitch_85
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x1e

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0xdc

    .line 1183
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0x1e

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit16 v3, v3, 0xdc

    .line 1184
    const/4 v4, 0x0

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1185
    goto/16 :goto_20

    .line 1187
    :pswitch_86
    const-wide v2, 0x3ff921fb54442d18L    # 1.5707963267948966

    iget v4, v14, Lsoftware/simplicial/nebulous/views/d;->l:F

    float-to-double v4, v4

    sub-double/2addr v2, v4

    double-to-float v2, v2

    .line 1188
    float-to-double v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    const-wide v14, 0x4052c00000000000L    # 75.0

    mul-double/2addr v4, v14

    double-to-float v5, v4

    .line 1189
    float-to-double v14, v2

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    const-wide v16, 0x4052c00000000000L    # 75.0

    mul-double v14, v14, v16

    double-to-float v6, v14

    .line 1191
    iget v4, v12, Lsoftware/simplicial/a/bh;->w:F

    .line 1192
    iget v3, v12, Lsoftware/simplicial/a/bh;->s:F

    float-to-double v14, v3

    float-to-double v0, v4

    move-wide/from16 v16, v0

    float-to-double v0, v2

    move-wide/from16 v18, v0

    const-wide v20, 0x3ff921fb54442d18L    # 1.5707963267948966

    sub-double v18, v18, v20

    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextFloat()F

    move-result v3

    float-to-double v0, v3

    move-wide/from16 v20, v0

    const-wide v22, 0x400921fb54442d18L    # Math.PI

    mul-double v20, v20, v22

    add-double v18, v18, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->cos(D)D

    move-result-wide v18

    mul-double v16, v16, v18

    add-double v14, v14, v16

    double-to-float v3, v14

    .line 1193
    iget v9, v12, Lsoftware/simplicial/a/bh;->t:F

    float-to-double v14, v9

    float-to-double v0, v4

    move-wide/from16 v16, v0

    float-to-double v0, v2

    move-wide/from16 v18, v0

    const-wide v20, 0x3ff921fb54442d18L    # 1.5707963267948966

    sub-double v18, v18, v20

    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v2

    float-to-double v0, v2

    move-wide/from16 v20, v0

    const-wide v22, 0x400921fb54442d18L    # Math.PI

    mul-double v20, v20, v22

    add-double v18, v18, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->sin(D)D

    move-result-wide v18

    mul-double v16, v16, v18

    add-double v14, v14, v16

    double-to-float v4, v14

    .line 1195
    const/16 v2, 0xbe

    .line 1196
    const/16 v9, 0xfc

    .line 1197
    const/16 v10, 0xfe

    move/from16 v24, v10

    move v10, v11

    move/from16 v11, v24

    .line 1198
    goto/16 :goto_20

    .line 1200
    :pswitch_87
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x1e

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0xab

    .line 1201
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0x1e

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit16 v3, v3, 0xb8

    .line 1202
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x1e

    invoke-virtual {v4, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x70

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1203
    goto/16 :goto_20

    .line 1205
    :pswitch_88
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x46

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0xba

    .line 1206
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0x1e

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1e

    .line 1207
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x1e

    invoke-virtual {v4, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x0

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1208
    goto/16 :goto_20

    .line 1210
    :pswitch_89
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x1e

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0xc3

    .line 1211
    const/16 v3, 0xff

    .line 1212
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/4 v14, 0x3

    invoke-virtual {v4, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit16 v4, v4, 0xfd

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1213
    goto/16 :goto_20

    .line 1215
    :pswitch_8a
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0xf3

    .line 1216
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit16 v3, v3, 0xb9

    .line 1217
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0xa

    invoke-virtual {v4, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x55

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1218
    goto/16 :goto_20

    .line 1220
    :pswitch_8b
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0xce

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v4, v2, 0x32

    .line 1221
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0xce

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v3, v2, 0x32

    .line 1222
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0xce

    invoke-virtual {v2, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x32

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1223
    goto/16 :goto_20

    .line 1225
    :pswitch_8c
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0xf5

    .line 1226
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0x28

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x7f

    .line 1227
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v5, 0x14

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x43

    .line 1228
    const/high16 v5, 0x41c80000    # 25.0f

    iget v6, v12, Lsoftware/simplicial/a/bh;->s:F

    sub-float v6, v10, v6

    mul-float/2addr v5, v6

    div-float v5, v5, v16

    .line 1229
    const/high16 v6, 0x41c80000    # 25.0f

    iget v14, v12, Lsoftware/simplicial/a/bh;->t:F

    sub-float v14, v9, v14

    mul-float/2addr v6, v14

    div-float v6, v6, v16

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1230
    goto/16 :goto_20

    .line 1232
    :pswitch_8d
    iget-object v2, v14, Lsoftware/simplicial/nebulous/views/d;->n:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    const/high16 v3, 0x437f0000    # 255.0f

    mul-float/2addr v2, v3

    float-to-int v4, v2

    .line 1233
    iget-object v2, v14, Lsoftware/simplicial/nebulous/views/d;->n:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    const/high16 v3, 0x437f0000    # 255.0f

    mul-float/2addr v2, v3

    float-to-int v3, v2

    .line 1234
    iget-object v2, v14, Lsoftware/simplicial/nebulous/views/d;->n:[F

    const/4 v14, 0x0

    aget v2, v2, v14

    const/high16 v14, 0x437f0000    # 255.0f

    mul-float/2addr v2, v14

    float-to-int v2, v2

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1235
    goto/16 :goto_20

    .line 1237
    :pswitch_8e
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x88

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v4, v2, 0x78

    .line 1238
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x88

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v3, v2, 0x78

    .line 1239
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x88

    invoke-virtual {v2, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x78

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1240
    goto/16 :goto_20

    .line 1242
    :pswitch_8f
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x88

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v4, v2, 0x78

    .line 1243
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x88

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v3, v2, 0x78

    .line 1244
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x88

    invoke-virtual {v2, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x78

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1245
    goto/16 :goto_20

    .line 1247
    :pswitch_90
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x38

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v4, v2, 0xc8

    .line 1248
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x2d

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v3, v2, 0x0

    .line 1249
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x32

    invoke-virtual {v2, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x46

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1250
    goto/16 :goto_20

    .line 1252
    :pswitch_91
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x38

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0xc8

    .line 1253
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0xc8

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x19

    .line 1254
    const/4 v4, 0x0

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1255
    goto/16 :goto_20

    .line 1257
    :pswitch_92
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x14

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v4, v2, 0x14

    .line 1258
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x14

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v3, v2, 0x14

    .line 1259
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x96

    invoke-virtual {v2, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x64

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1260
    goto/16 :goto_20

    .line 1262
    :pswitch_93
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x1e

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v4, v2, 0x96

    .line 1263
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x1e

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v3, v2, 0xc8

    .line 1264
    const/4 v2, 0x0

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1265
    goto/16 :goto_20

    .line 1267
    :pswitch_94
    const/16 v4, 0x64

    .line 1268
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x28

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v3, v2, 0x96

    .line 1269
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x28

    invoke-virtual {v2, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0x96

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1270
    goto/16 :goto_20

    .line 1272
    :pswitch_95
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x100

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    .line 1275
    const/16 v2, 0xff

    move v4, v9

    move v9, v3

    move/from16 v24, v10

    move v10, v11

    move v11, v3

    move/from16 v3, v24

    .line 1276
    goto/16 :goto_20

    .line 1278
    :pswitch_96
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextBoolean()Z

    move-result v2

    if-eqz v2, :cond_46

    .line 1280
    const/16 v2, 0x32

    .line 1281
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0x38

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit16 v3, v3, 0xc8

    .line 1282
    const/16 v4, 0x32

    .line 1290
    :goto_22
    iget v5, v14, Lsoftware/simplicial/nebulous/views/d;->G:F

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x42c80000    # 100.0f

    iget v15, v12, Lsoftware/simplicial/a/bh;->s:F

    sub-float v15, v10, v15

    mul-float/2addr v6, v15

    div-float v6, v6, v16

    mul-float/2addr v5, v6

    .line 1291
    iget v6, v14, Lsoftware/simplicial/nebulous/views/d;->G:F

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    const/high16 v14, 0x42c80000    # 100.0f

    iget v15, v12, Lsoftware/simplicial/a/bh;->t:F

    sub-float v15, v9, v15

    mul-float/2addr v14, v15

    div-float v14, v14, v16

    mul-float/2addr v6, v14

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1292
    goto/16 :goto_20

    .line 1286
    :cond_46
    const/16 v2, 0x32

    .line 1287
    const/16 v3, 0x32

    .line 1288
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v5, 0x38

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit16 v4, v4, 0xc8

    goto :goto_22

    .line 1294
    :pswitch_97
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v4, v2, 0x9c

    .line 1295
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x1e

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v3, v2, 0x0

    .line 1296
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x1e

    invoke-virtual {v2, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x0

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1297
    goto/16 :goto_20

    .line 1299
    :pswitch_98
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v4, v2, 0x64

    .line 1300
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v3, v2, 0x64

    .line 1301
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x1e

    invoke-virtual {v2, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0xc8

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1302
    goto/16 :goto_20

    .line 1304
    :pswitch_99
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v4, v2, 0x14

    .line 1305
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x50

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v3, v2, 0x0

    .line 1306
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x1e

    invoke-virtual {v2, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0xc8

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1307
    goto/16 :goto_20

    .line 1309
    :pswitch_9a
    const/4 v4, 0x0

    .line 1310
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x9c

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v3, v2, 0x64

    .line 1311
    const/4 v2, 0x0

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1312
    goto/16 :goto_20

    .line 1318
    :pswitch_9b
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x38

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0xc8

    .line 1319
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0x100

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x0

    .line 1320
    const/4 v4, 0x0

    .line 1322
    const/4 v5, 0x0

    .line 1323
    const/high16 v6, -0x3f600000    # -5.0f

    .line 1324
    const/high16 v8, 0x40000000    # 2.0f

    const/high16 v14, 0x40800000    # 4.0f

    sget-object v15, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v15}, Ljava/util/Random;->nextFloat()F

    move-result v15

    mul-float/2addr v14, v15

    sub-float/2addr v8, v14

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1325
    goto/16 :goto_20

    .line 1329
    :pswitch_9c
    const/16 v2, 0xff

    .line 1330
    const/16 v3, 0xff

    .line 1331
    const/16 v4, 0xff

    .line 1333
    const/4 v5, 0x0

    .line 1334
    const/high16 v6, -0x3f600000    # -5.0f

    .line 1335
    const/high16 v8, 0x40a00000    # 5.0f

    const/high16 v14, 0x41200000    # 10.0f

    sget-object v15, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v15}, Ljava/util/Random;->nextFloat()F

    move-result v15

    mul-float/2addr v14, v15

    sub-float/2addr v8, v14

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1336
    goto/16 :goto_20

    .line 1341
    :pswitch_9d
    const/16 v2, 0xff

    .line 1342
    const/16 v3, 0xff

    .line 1343
    const/16 v4, 0xff

    .line 1345
    const/4 v5, 0x0

    .line 1346
    const/high16 v6, -0x3f600000    # -5.0f

    .line 1347
    const/high16 v8, 0x40a00000    # 5.0f

    const/high16 v14, 0x41200000    # 10.0f

    sget-object v15, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v15}, Ljava/util/Random;->nextFloat()F

    move-result v15

    mul-float/2addr v14, v15

    sub-float/2addr v8, v14

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1348
    goto/16 :goto_20

    .line 1350
    :pswitch_9e
    const/16 v2, 0xff

    .line 1351
    const/16 v3, 0xff

    .line 1352
    const/16 v4, 0xff

    .line 1354
    const/high16 v5, 0x42480000    # 50.0f

    iget v6, v12, Lsoftware/simplicial/a/bh;->s:F

    sub-float v6, v10, v6

    mul-float/2addr v5, v6

    div-float v5, v5, v16

    .line 1355
    const/high16 v6, 0x42480000    # 50.0f

    iget v14, v12, Lsoftware/simplicial/a/bh;->t:F

    sub-float v14, v9, v14

    mul-float/2addr v6, v14

    div-float v6, v6, v16

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1356
    goto/16 :goto_20

    .line 1362
    :pswitch_9f
    const/16 v2, 0xff

    .line 1363
    const/16 v3, 0xff

    .line 1364
    const/16 v4, 0xff

    .line 1366
    const-wide/high16 v16, 0x4024000000000000L    # 10.0

    float-to-double v0, v15

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->cos(D)D

    move-result-wide v18

    mul-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-float v5, v0

    .line 1367
    const-wide/high16 v16, 0x4024000000000000L    # 10.0

    float-to-double v14, v15

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    mul-double v14, v14, v16

    double-to-float v6, v14

    .line 1368
    const/high16 v8, 0x40a00000    # 5.0f

    const/high16 v14, 0x41200000    # 10.0f

    sget-object v15, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v15}, Ljava/util/Random;->nextFloat()F

    move-result v15

    mul-float/2addr v14, v15

    sub-float/2addr v8, v14

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1369
    goto/16 :goto_20

    .line 1375
    :pswitch_a0
    const/16 v2, 0xff

    .line 1376
    const/16 v3, 0xff

    .line 1377
    const/16 v4, 0xff

    .line 1379
    const-wide/high16 v16, 0x4008000000000000L    # 3.0

    float-to-double v0, v15

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->cos(D)D

    move-result-wide v18

    mul-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-float v5, v0

    .line 1380
    const-wide/high16 v16, 0x4008000000000000L    # 3.0

    float-to-double v14, v15

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    mul-double v14, v14, v16

    double-to-float v6, v14

    .line 1381
    const/high16 v8, 0x40000000    # 2.0f

    const/high16 v14, 0x40800000    # 4.0f

    sget-object v15, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v15}, Ljava/util/Random;->nextFloat()F

    move-result v15

    mul-float/2addr v14, v15

    sub-float/2addr v8, v14

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1382
    goto/16 :goto_20

    .line 1393
    :pswitch_a1
    const/16 v2, 0xff

    .line 1394
    const/16 v3, 0xff

    .line 1395
    const/16 v4, 0xff

    .line 1396
    const/4 v5, 0x0

    .line 1397
    const/high16 v6, -0x40000000    # -2.0f

    .line 1398
    const/high16 v8, 0x40000000    # 2.0f

    const/high16 v14, 0x40800000    # 4.0f

    sget-object v15, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v15}, Ljava/util/Random;->nextFloat()F

    move-result v15

    mul-float/2addr v14, v15

    sub-float/2addr v8, v14

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1399
    goto/16 :goto_20

    .line 1405
    :pswitch_a2
    const/16 v2, 0xff

    .line 1406
    const/16 v3, 0xff

    .line 1407
    const/16 v4, 0xff

    .line 1408
    const/4 v5, 0x0

    .line 1409
    const/high16 v6, -0x3fa00000    # -3.5f

    .line 1410
    const/high16 v8, 0x40000000    # 2.0f

    const/high16 v14, 0x40800000    # 4.0f

    sget-object v15, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v15}, Ljava/util/Random;->nextFloat()F

    move-result v15

    mul-float/2addr v14, v15

    sub-float/2addr v8, v14

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1411
    goto/16 :goto_20

    .line 1417
    :pswitch_a3
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x38

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    .line 1418
    rsub-int v2, v4, 0xff

    .line 1419
    const/16 v3, 0xff

    .line 1420
    rsub-int v4, v4, 0xff

    .line 1422
    const/4 v5, 0x0

    .line 1423
    const/high16 v6, -0x3f600000    # -5.0f

    .line 1424
    const/high16 v8, 0x40000000    # 2.0f

    const/high16 v14, 0x40800000    # 4.0f

    sget-object v15, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v15}, Ljava/util/Random;->nextFloat()F

    move-result v15

    mul-float/2addr v14, v15

    sub-float/2addr v8, v14

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1425
    goto/16 :goto_20

    .line 1431
    :pswitch_a4
    const-wide/high16 v14, 0x4018000000000000L    # 6.0

    float-to-double v0, v7

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sin(D)D

    move-result-wide v16

    mul-double v14, v14, v16

    neg-double v14, v14

    double-to-float v5, v14

    .line 1432
    const-wide/high16 v14, 0x4018000000000000L    # 6.0

    float-to-double v0, v7

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->cos(D)D

    move-result-wide v16

    mul-double v14, v14, v16

    double-to-float v6, v14

    .line 1433
    const/4 v8, 0x0

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1434
    goto/16 :goto_20

    .line 1440
    :pswitch_a5
    const/high16 v6, 0x40400000    # 3.0f

    .line 1441
    const/4 v8, 0x0

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1442
    goto/16 :goto_20

    .line 1444
    :pswitch_a6
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x38

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v4, v2, 0xc8

    .line 1445
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x2d

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v3, v2, 0x0

    .line 1446
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x32

    invoke-virtual {v2, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x64

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1447
    goto/16 :goto_20

    .line 1449
    :pswitch_a7
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x80

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v4, v2, 0x80

    .line 1450
    const/16 v3, 0xff

    .line 1451
    const/4 v2, 0x0

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1452
    goto/16 :goto_20

    .line 1454
    :pswitch_a8
    const/16 v4, 0xff

    .line 1455
    const/high16 v2, 0x437f0000    # 255.0f

    iget v3, v14, Lsoftware/simplicial/nebulous/views/d;->G:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    mul-float/2addr v2, v3

    float-to-int v3, v2

    .line 1456
    const/high16 v2, 0x437f0000    # 255.0f

    const/high16 v15, 0x3f800000    # 1.0f

    iget v14, v14, Lsoftware/simplicial/nebulous/views/d;->G:F

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v14

    sub-float v14, v15, v14

    mul-float/2addr v2, v14

    float-to-int v2, v2

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1457
    goto/16 :goto_20

    .line 1459
    :pswitch_a9
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x46

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    .line 1460
    add-int/lit8 v4, v2, 0x7d

    .line 1461
    add-int/lit8 v3, v2, 0x16

    .line 1462
    add-int/lit16 v2, v2, 0x83

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1463
    goto/16 :goto_20

    .line 1465
    :pswitch_aa
    const/4 v4, 0x0

    .line 1466
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x38

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v3, v2, 0xc8

    .line 1467
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x6a

    invoke-virtual {v2, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0x96

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1468
    goto/16 :goto_20

    .line 1470
    :pswitch_ab
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x9b

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x64

    .line 1471
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0x9b

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x64

    .line 1472
    const/4 v4, 0x0

    .line 1473
    const/high16 v5, 0x42480000    # 50.0f

    iget v6, v12, Lsoftware/simplicial/a/bh;->s:F

    sub-float v6, v10, v6

    mul-float/2addr v5, v6

    div-float v5, v5, v16

    .line 1474
    const/high16 v6, 0x42480000    # 50.0f

    iget v14, v12, Lsoftware/simplicial/a/bh;->t:F

    sub-float v14, v9, v14

    mul-float/2addr v6, v14

    div-float v6, v6, v16

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1475
    goto/16 :goto_20

    .line 1480
    :pswitch_ac
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x38

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0xc8

    .line 1481
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0xc8

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x19

    .line 1482
    const/4 v4, 0x0

    .line 1483
    const/high16 v5, 0x42480000    # 50.0f

    iget v6, v12, Lsoftware/simplicial/a/bh;->s:F

    sub-float v6, v10, v6

    mul-float/2addr v5, v6

    div-float v5, v5, v16

    .line 1484
    const/high16 v6, 0x42480000    # 50.0f

    iget v14, v12, Lsoftware/simplicial/a/bh;->t:F

    sub-float v14, v9, v14

    mul-float/2addr v6, v14

    div-float v6, v6, v16

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1485
    goto/16 :goto_20

    .line 1488
    :pswitch_ad
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x38

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0xc8

    .line 1489
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0xc8

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x19

    .line 1490
    const/4 v4, 0x0

    .line 1491
    iget v5, v14, Lsoftware/simplicial/nebulous/views/d;->G:F

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x42c80000    # 100.0f

    iget v15, v12, Lsoftware/simplicial/a/bh;->s:F

    sub-float v15, v10, v15

    mul-float/2addr v6, v15

    div-float v6, v6, v16

    mul-float/2addr v5, v6

    .line 1492
    iget v6, v14, Lsoftware/simplicial/nebulous/views/d;->G:F

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    const/high16 v14, 0x42c80000    # 100.0f

    iget v15, v12, Lsoftware/simplicial/a/bh;->t:F

    sub-float v15, v9, v15

    mul-float/2addr v14, v15

    div-float v14, v14, v16

    mul-float/2addr v6, v14

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1493
    goto/16 :goto_20

    .line 1495
    :pswitch_ae
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x88

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v4, v2, 0x78

    .line 1496
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x88

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v3, v2, 0x78

    .line 1497
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v5, 0x88

    invoke-virtual {v2, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x78

    .line 1499
    iget v5, v14, Lsoftware/simplicial/nebulous/views/d;->G:F

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x42c80000    # 100.0f

    iget v15, v12, Lsoftware/simplicial/a/bh;->s:F

    sub-float v15, v10, v15

    mul-float/2addr v6, v15

    div-float v6, v6, v16

    mul-float/2addr v5, v6

    .line 1500
    iget v6, v14, Lsoftware/simplicial/nebulous/views/d;->G:F

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    const/high16 v14, 0x42c80000    # 100.0f

    iget v15, v12, Lsoftware/simplicial/a/bh;->t:F

    sub-float v15, v9, v15

    mul-float/2addr v14, v15

    div-float v14, v14, v16

    mul-float/2addr v6, v14

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1501
    goto/16 :goto_20

    .line 1503
    :pswitch_af
    const-wide v2, 0x3ff921fb54442d18L    # 1.5707963267948966

    iget v4, v14, Lsoftware/simplicial/nebulous/views/d;->l:F

    float-to-double v4, v4

    sub-double/2addr v2, v4

    double-to-float v2, v2

    .line 1504
    float-to-double v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    const-wide v14, 0x4052c00000000000L    # 75.0

    mul-double/2addr v4, v14

    double-to-float v5, v4

    .line 1505
    float-to-double v14, v2

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    const-wide v16, 0x4052c00000000000L    # 75.0

    mul-double v14, v14, v16

    double-to-float v6, v14

    .line 1507
    iget v4, v12, Lsoftware/simplicial/a/bh;->w:F

    .line 1508
    iget v3, v12, Lsoftware/simplicial/a/bh;->s:F

    float-to-double v14, v3

    float-to-double v0, v4

    move-wide/from16 v16, v0

    float-to-double v0, v2

    move-wide/from16 v18, v0

    const-wide v20, 0x3ff921fb54442d18L    # 1.5707963267948966

    sub-double v18, v18, v20

    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextFloat()F

    move-result v3

    float-to-double v0, v3

    move-wide/from16 v20, v0

    const-wide v22, 0x400921fb54442d18L    # Math.PI

    mul-double v20, v20, v22

    add-double v18, v18, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->cos(D)D

    move-result-wide v18

    mul-double v16, v16, v18

    add-double v14, v14, v16

    double-to-float v3, v14

    .line 1509
    iget v9, v12, Lsoftware/simplicial/a/bh;->t:F

    float-to-double v14, v9

    float-to-double v0, v4

    move-wide/from16 v16, v0

    float-to-double v0, v2

    move-wide/from16 v18, v0

    const-wide v20, 0x3ff921fb54442d18L    # 1.5707963267948966

    sub-double v18, v18, v20

    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v2

    float-to-double v0, v2

    move-wide/from16 v20, v0

    const-wide v22, 0x400921fb54442d18L    # Math.PI

    mul-double v20, v20, v22

    add-double v18, v18, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->sin(D)D

    move-result-wide v18

    mul-double v16, v16, v18

    add-double v14, v14, v16

    double-to-float v4, v14

    .line 1512
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v9, 0x38

    invoke-virtual {v2, v9}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0xc8

    .line 1513
    sget-object v9, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v10, 0x64

    invoke-virtual {v9, v10}, Ljava/util/Random;->nextInt(I)I

    move-result v9

    add-int/lit8 v9, v9, 0x19

    .line 1514
    const/4 v10, 0x0

    move/from16 v24, v10

    move v10, v11

    move/from16 v11, v24

    .line 1515
    goto/16 :goto_20

    .line 1517
    :pswitch_b0
    const/16 v2, 0xff

    .line 1518
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0x64

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x2b

    .line 1519
    const/4 v4, 0x7

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1520
    goto/16 :goto_20

    .line 1522
    :pswitch_b1
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x14

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0xaf

    .line 1523
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0x14

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit16 v3, v3, 0xaf

    .line 1524
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0xa

    invoke-virtual {v4, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit16 v4, v4, 0xa5

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1525
    goto/16 :goto_20

    .line 1527
    :pswitch_b2
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextBoolean()Z

    move-result v2

    if-eqz v2, :cond_47

    .line 1529
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x14

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0x9c

    .line 1530
    const/4 v3, 0x0

    .line 1531
    const/4 v4, 0x0

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    goto/16 :goto_20

    .line 1535
    :cond_47
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0xc8

    .line 1536
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit16 v3, v3, 0xc8

    .line 1537
    const/4 v4, 0x0

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1539
    goto/16 :goto_20

    .line 1541
    :pswitch_b3
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v2

    const/high16 v3, 0x3e800000    # 0.25f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_48

    .line 1543
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x14

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x7c

    .line 1544
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0x14

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x3e

    .line 1545
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x14

    invoke-virtual {v4, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x36

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    goto/16 :goto_20

    .line 1549
    :cond_48
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x14

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x5d

    .line 1550
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0x14

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x7e

    .line 1551
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x14

    invoke-virtual {v4, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x34

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1553
    goto/16 :goto_20

    .line 1555
    :pswitch_b4
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v2

    const/high16 v3, 0x3e800000    # 0.25f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_49

    .line 1557
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x14

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0xeb

    .line 1558
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0x14

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1b

    .line 1559
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x14

    invoke-virtual {v4, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x24

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    goto/16 :goto_20

    .line 1563
    :cond_49
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x14

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0x8c

    .line 1564
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0x14

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit16 v3, v3, 0xc6

    .line 1565
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x14

    invoke-virtual {v4, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x3f

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1567
    goto/16 :goto_20

    .line 1569
    :pswitch_b5
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x38

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    .line 1570
    mul-int/lit8 v2, v4, 0x3

    add-int/lit8 v2, v2, 0x14

    .line 1571
    mul-int/lit8 v3, v4, 0x3

    add-int/lit8 v3, v3, 0x14

    .line 1572
    add-int/lit16 v4, v4, 0xc8

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1573
    goto/16 :goto_20

    .line 1575
    :pswitch_b6
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x38

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    .line 1576
    mul-int/lit8 v2, v4, 0x3

    add-int/lit8 v2, v2, 0x14

    .line 1577
    mul-int/lit8 v3, v4, 0x3

    add-int/lit8 v3, v3, 0x14

    .line 1578
    add-int/lit16 v4, v4, 0xc8

    .line 1579
    const/high16 v5, -0x3db80000    # -50.0f

    iget v6, v12, Lsoftware/simplicial/a/bh;->s:F

    sub-float v6, v10, v6

    mul-float/2addr v5, v6

    div-float v5, v5, v16

    .line 1580
    const/high16 v6, -0x3db80000    # -50.0f

    iget v14, v12, Lsoftware/simplicial/a/bh;->t:F

    sub-float v14, v9, v14

    mul-float/2addr v6, v14

    div-float v6, v6, v16

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1581
    goto/16 :goto_20

    .line 1584
    :pswitch_b7
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x38

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    .line 1585
    add-int/lit16 v2, v3, 0xc8

    .line 1586
    mul-int/lit8 v3, v3, 0x4

    add-int/lit8 v3, v3, 0x14

    .line 1587
    const/4 v4, 0x0

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1588
    goto/16 :goto_20

    .line 1590
    :pswitch_b8
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x38

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    .line 1591
    const/4 v2, 0x0

    .line 1592
    mul-int/lit8 v3, v4, 0x4

    add-int/lit8 v3, v3, 0x14

    .line 1593
    add-int/lit16 v4, v4, 0xc8

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1594
    goto/16 :goto_20

    .line 1599
    :pswitch_b9
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    .line 1600
    add-int/lit16 v2, v3, 0x9c

    .line 1601
    mul-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, 0x14

    .line 1602
    const/4 v4, 0x0

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1603
    goto/16 :goto_20

    .line 1605
    :pswitch_ba
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    .line 1606
    add-int/lit16 v2, v3, 0x9c

    .line 1607
    add-int/lit16 v3, v3, 0x9c

    .line 1608
    const/16 v4, 0xff

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1609
    goto/16 :goto_20

    .line 1611
    :pswitch_bb
    const/16 v2, 0xe6

    .line 1612
    const/16 v3, 0xe6

    .line 1613
    const/16 v4, 0xe6

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1614
    goto/16 :goto_20

    .line 1616
    :pswitch_bc
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x14

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0x83

    .line 1617
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0x14

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit16 v3, v3, 0xea

    .line 1618
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x14

    invoke-virtual {v4, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x68

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1619
    goto/16 :goto_20

    .line 1621
    :pswitch_bd
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v4

    .line 1622
    const/16 v2, 0xff

    .line 1623
    const/high16 v3, 0x437f0000    # 255.0f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1624
    const/high16 v14, 0x437f0000    # 255.0f

    const/high16 v15, 0x3f800000    # 1.0f

    sub-float v4, v15, v4

    mul-float/2addr v4, v14

    float-to-int v4, v4

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1625
    goto/16 :goto_20

    .line 1627
    :pswitch_be
    iget v2, v14, Lsoftware/simplicial/nebulous/views/d;->G:F

    float-to-int v2, v2

    .line 1628
    if-ltz v2, :cond_4a

    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->w:[[I

    array-length v3, v3

    if-lt v2, v3, :cond_4b

    .line 1629
    :cond_4a
    const/4 v2, 0x0

    .line 1630
    :cond_4b
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->w:[[I

    aget-object v3, v3, v2

    const/4 v4, 0x0

    aget v9, v3, v4

    .line 1631
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->w:[[I

    aget-object v3, v3, v2

    const/4 v4, 0x1

    aget v10, v3, v4

    .line 1632
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->w:[[I

    aget-object v2, v3, v2

    const/4 v3, 0x2

    aget v2, v2, v3

    .line 1634
    iget v3, v14, Lsoftware/simplicial/nebulous/views/d;->G:F

    const/high16 v4, 0x41200000    # 10.0f

    div-float/2addr v3, v4

    float-to-double v4, v3

    const-wide v16, 0x400921fb54442d18L    # Math.PI

    mul-double v4, v4, v16

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    mul-double v4, v4, v16

    const-wide v16, 0x3ff921fb54442d18L    # 1.5707963267948966

    sub-double v4, v4, v16

    iget v3, v14, Lsoftware/simplicial/nebulous/views/d;->l:F

    float-to-double v14, v3

    sub-double/2addr v4, v14

    double-to-float v4, v4

    .line 1635
    float-to-double v14, v4

    invoke-static {v14, v15}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    const-wide/high16 v16, 0x4049000000000000L    # 50.0

    mul-double v14, v14, v16

    double-to-float v5, v14

    .line 1636
    float-to-double v14, v4

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    const-wide/high16 v16, 0x4049000000000000L    # 50.0

    mul-double v14, v14, v16

    double-to-float v6, v14

    .line 1638
    iget v14, v12, Lsoftware/simplicial/a/bh;->w:F

    .line 1639
    iget v3, v12, Lsoftware/simplicial/a/bh;->s:F

    float-to-double v0, v3

    move-wide/from16 v16, v0

    float-to-double v0, v14

    move-wide/from16 v18, v0

    float-to-double v0, v4

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->cos(D)D

    move-result-wide v20

    mul-double v18, v18, v20

    add-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-float v3, v0

    .line 1640
    iget v15, v12, Lsoftware/simplicial/a/bh;->t:F

    float-to-double v0, v15

    move-wide/from16 v16, v0

    float-to-double v14, v14

    float-to-double v0, v4

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->sin(D)D

    move-result-wide v18

    mul-double v14, v14, v18

    add-double v14, v14, v16

    double-to-float v4, v14

    move/from16 v24, v9

    move v9, v10

    move v10, v11

    move v11, v2

    move/from16 v2, v24

    .line 1641
    goto/16 :goto_20

    .line 1644
    :pswitch_bf
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextBoolean()Z

    move-result v2

    if-eqz v2, :cond_4c

    .line 1646
    const/16 v2, 0x32

    .line 1647
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0x38

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit16 v3, v3, 0xc8

    .line 1648
    const/16 v4, 0x32

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    goto/16 :goto_20

    .line 1652
    :cond_4c
    const/16 v2, 0x32

    .line 1653
    const/16 v3, 0x32

    .line 1654
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x38

    invoke-virtual {v4, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit16 v4, v4, 0xc8

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1656
    goto/16 :goto_20

    .line 1660
    :pswitch_c0
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextBoolean()Z

    move-result v2

    if-eqz v2, :cond_4d

    .line 1662
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x38

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0xc8

    .line 1663
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0x38

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit16 v3, v3, 0xb9

    .line 1664
    const/16 v4, 0x32

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    goto/16 :goto_20

    .line 1668
    :cond_4d
    const/16 v2, 0x32

    .line 1669
    const/16 v3, 0x32

    .line 1670
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x38

    invoke-virtual {v4, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit16 v4, v4, 0xc8

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1672
    goto/16 :goto_20

    .line 1674
    :pswitch_c1
    sget-object v5, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/4 v6, 0x3

    invoke-virtual {v5, v6}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    packed-switch v5, :pswitch_data_14

    .line 1692
    :goto_23
    const/4 v5, 0x0

    .line 1693
    const/high16 v6, -0x3f600000    # -5.0f

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1694
    goto/16 :goto_20

    .line 1677
    :pswitch_c2
    const/16 v2, 0xfb

    .line 1678
    const/16 v3, 0xf7

    .line 1679
    const/16 v4, 0x2f

    .line 1680
    goto :goto_23

    .line 1682
    :pswitch_c3
    const/16 v2, 0x7a

    .line 1683
    const/16 v3, 0x31

    .line 1684
    const/16 v4, 0x8e

    .line 1685
    goto :goto_23

    .line 1687
    :pswitch_c4
    const/4 v2, 0x5

    .line 1688
    const/16 v3, 0xa1

    .line 1689
    const/16 v4, 0x63

    goto :goto_23

    .line 1696
    :pswitch_c5
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextBoolean()Z

    move-result v2

    if-eqz v2, :cond_4e

    .line 1698
    const/16 v2, 0xf4

    .line 1699
    const/16 v3, 0xa2

    .line 1700
    const/16 v4, 0x4a

    .line 1708
    :goto_24
    const/4 v5, 0x0

    .line 1709
    const/high16 v6, -0x3f600000    # -5.0f

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1710
    goto/16 :goto_20

    .line 1704
    :cond_4e
    const/16 v2, 0x55

    .line 1705
    const/16 v3, 0xa7

    .line 1706
    const/16 v4, 0x55

    goto :goto_24

    .line 1712
    :pswitch_c6
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextBoolean()Z

    move-result v2

    if-eqz v2, :cond_4f

    .line 1714
    const/16 v2, 0xed

    .line 1715
    const/16 v3, 0x1d

    .line 1716
    const/16 v4, 0x25

    .line 1724
    :goto_25
    const/4 v5, 0x0

    .line 1725
    const/high16 v6, -0x3f600000    # -5.0f

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1726
    goto/16 :goto_20

    .line 1720
    :cond_4f
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x78

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x14

    .line 1721
    const/16 v3, 0xaa

    .line 1722
    const/16 v4, 0x4c

    goto :goto_25

    .line 1728
    :pswitch_c7
    sget-object v5, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    packed-switch v5, :pswitch_data_15

    .line 1741
    :goto_26
    const/4 v5, 0x0

    .line 1742
    const/high16 v6, -0x3f600000    # -5.0f

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1743
    goto/16 :goto_20

    .line 1731
    :pswitch_c8
    const/16 v2, 0xc

    .line 1732
    const/16 v3, 0xe7

    .line 1733
    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v5, 0x96

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x49

    .line 1734
    goto :goto_26

    .line 1736
    :pswitch_c9
    const/16 v2, 0xee

    .line 1737
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v4, 0x1e

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit16 v3, v3, 0xe1

    .line 1738
    const/16 v4, 0x22

    goto :goto_26

    .line 1745
    :pswitch_ca
    iget-object v2, v14, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    const/high16 v3, 0x437f0000    # 255.0f

    mul-float/2addr v2, v3

    float-to-int v4, v2

    .line 1746
    iget-object v2, v14, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    const/high16 v3, 0x437f0000    # 255.0f

    mul-float/2addr v2, v3

    float-to-int v3, v2

    .line 1747
    iget-object v2, v14, Lsoftware/simplicial/nebulous/views/d;->o:[F

    const/4 v5, 0x0

    aget v2, v2, v5

    const/high16 v5, 0x437f0000    # 255.0f

    mul-float/2addr v2, v5

    float-to-int v2, v2

    .line 1748
    const/4 v5, 0x0

    .line 1749
    const/high16 v6, -0x3f600000    # -5.0f

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1750
    goto/16 :goto_20

    .line 1752
    :pswitch_cb
    const/4 v4, 0x0

    .line 1753
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x32

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v3, v2, 0xa5

    .line 1754
    const/16 v2, 0xc8

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1755
    goto/16 :goto_20

    .line 1757
    :pswitch_cc
    const/16 v4, 0xff

    .line 1758
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v3, 0x96

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    rsub-int v3, v2, 0xd4

    .line 1759
    const/16 v2, 0x14

    move/from16 v24, v9

    move v9, v3

    move v3, v10

    move v10, v11

    move v11, v4

    move/from16 v4, v24

    .line 1760
    goto/16 :goto_20

    .line 1764
    :pswitch_cd
    iget v2, v12, Lsoftware/simplicial/a/bh;->w:F

    const/high16 v3, 0x3f800000    # 1.0f

    sget-object v4, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v4}, Ljava/util/Random;->nextFloat()F

    move-result v4

    const/high16 v5, 0x3f800000    # 1.0f

    mul-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    add-float/2addr v3, v4

    mul-float/2addr v2, v3

    .line 1765
    sget-object v3, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextFloat()F

    move-result v3

    float-to-double v4, v3

    const-wide v14, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v4, v14

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    mul-double/2addr v4, v14

    double-to-float v6, v4

    .line 1766
    iget v3, v12, Lsoftware/simplicial/a/bh;->s:F

    float-to-double v4, v3

    float-to-double v14, v2

    float-to-double v0, v6

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->cos(D)D

    move-result-wide v16

    mul-double v14, v14, v16

    add-double/2addr v4, v14

    double-to-float v3, v4

    .line 1767
    iget v4, v12, Lsoftware/simplicial/a/bh;->t:F

    float-to-double v4, v4

    float-to-double v14, v2

    float-to-double v0, v6

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sin(D)D

    move-result-wide v16

    mul-double v14, v14, v16

    add-double/2addr v4, v14

    double-to-float v4, v4

    .line 1768
    float-to-double v14, v6

    invoke-static {v14, v15}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    neg-double v14, v14

    double-to-float v2, v14

    const/high16 v5, 0x41c80000    # 25.0f

    mul-float/2addr v5, v2

    .line 1769
    float-to-double v14, v6

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    neg-double v14, v14

    double-to-float v2, v14

    const/high16 v6, 0x41c80000    # 25.0f

    mul-float/2addr v6, v2

    .line 1771
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v9, 0x100

    invoke-virtual {v2, v9}, Ljava/util/Random;->nextInt(I)I

    move-result v10

    .line 1772
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v9, 0x100

    invoke-virtual {v2, v9}, Ljava/util/Random;->nextInt(I)I

    move-result v9

    .line 1773
    sget-object v2, Lsoftware/simplicial/nebulous/views/j;->r:Ljava/util/Random;

    const/16 v14, 0x100

    invoke-virtual {v2, v14}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    move/from16 v24, v10

    move v10, v11

    move/from16 v11, v24

    goto/16 :goto_20

    :cond_50
    move-object v12, v6

    goto/16 :goto_1e

    .line 248
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 261
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 284
    :pswitch_data_2
    .packed-switch 0x6
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch

    .line 307
    :pswitch_data_3
    .packed-switch 0xb
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch

    .line 330
    :pswitch_data_4
    .packed-switch 0x10
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
    .end packed-switch

    .line 352
    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_17
        :pswitch_18
        :pswitch_19
    .end packed-switch

    .line 364
    :pswitch_data_6
    .packed-switch 0x15
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
    .end packed-switch

    .line 380
    :pswitch_data_7
    .packed-switch 0x0
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
    .end packed-switch

    .line 392
    :pswitch_data_8
    .packed-switch 0x18
        :pswitch_20
        :pswitch_21
        :pswitch_22
    .end packed-switch

    .line 409
    :pswitch_data_9
    .packed-switch 0x1b
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
    .end packed-switch

    .line 432
    :pswitch_data_a
    .packed-switch 0x20
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
    .end packed-switch

    .line 453
    :pswitch_data_b
    .packed-switch 0x0
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
    .end packed-switch

    .line 466
    :pswitch_data_c
    .packed-switch 0x25
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
    .end packed-switch

    .line 495
    :pswitch_data_d
    .packed-switch 0x2c
        :pswitch_37
        :pswitch_38
        :pswitch_39
        :pswitch_3a
        :pswitch_3b
    .end packed-switch

    .line 517
    :pswitch_data_e
    .packed-switch 0x0
        :pswitch_3c
        :pswitch_3d
        :pswitch_3e
        :pswitch_3f
        :pswitch_40
        :pswitch_41
    .end packed-switch

    .line 538
    :pswitch_data_f
    .packed-switch 0x31
        :pswitch_42
        :pswitch_43
        :pswitch_44
        :pswitch_45
        :pswitch_46
    .end packed-switch

    .line 561
    :pswitch_data_10
    .packed-switch 0x36
        :pswitch_47
        :pswitch_48
        :pswitch_49
        :pswitch_4a
        :pswitch_4b
    .end packed-switch

    .line 584
    :pswitch_data_11
    .packed-switch 0x3b
        :pswitch_4c
        :pswitch_4d
        :pswitch_4e
        :pswitch_4f
        :pswitch_50
    .end packed-switch

    .line 698
    :pswitch_data_12
    .packed-switch 0x1
        :pswitch_52
        :pswitch_52
        :pswitch_52
        :pswitch_52
        :pswitch_52
        :pswitch_53
        :pswitch_54
        :pswitch_55
    .end packed-switch

    .line 741
    :pswitch_data_13
    .packed-switch 0x1
        :pswitch_a5
        :pswitch_a5
        :pswitch_a5
        :pswitch_a5
        :pswitch_a5
        :pswitch_a0
        :pswitch_a0
        :pswitch_a0
        :pswitch_a0
        :pswitch_a0
        :pswitch_9f
        :pswitch_9f
        :pswitch_9f
        :pswitch_9f
        :pswitch_9f
        :pswitch_9d
        :pswitch_9d
        :pswitch_9d
        :pswitch_9d
        :pswitch_9e
        :pswitch_9c
        :pswitch_9c
        :pswitch_9c
        :pswitch_a1
        :pswitch_a1
        :pswitch_a1
        :pswitch_a4
        :pswitch_a4
        :pswitch_a4
        :pswitch_a4
        :pswitch_a4
        :pswitch_9b
        :pswitch_9b
        :pswitch_9b
        :pswitch_9b
        :pswitch_9b
        :pswitch_a1
        :pswitch_a1
        :pswitch_a1
        :pswitch_a1
        :pswitch_a1
        :pswitch_a1
        :pswitch_a1
        :pswitch_c1
        :pswitch_c5
        :pswitch_c6
        :pswitch_c7
        :pswitch_ca
        :pswitch_a2
        :pswitch_a2
        :pswitch_a2
        :pswitch_a2
        :pswitch_a2
        :pswitch_a3
        :pswitch_a3
        :pswitch_a3
        :pswitch_a3
        :pswitch_a3
        :pswitch_51
        :pswitch_51
        :pswitch_51
        :pswitch_51
        :pswitch_51
        :pswitch_56
        :pswitch_56
        :pswitch_56
        :pswitch_56
        :pswitch_56
        :pswitch_56
        :pswitch_56
        :pswitch_56
        :pswitch_56
        :pswitch_56
        :pswitch_56
        :pswitch_56
        :pswitch_56
        :pswitch_56
        :pswitch_56
        :pswitch_56
        :pswitch_56
        :pswitch_56
        :pswitch_56
        :pswitch_56
        :pswitch_56
        :pswitch_56
        :pswitch_56
        :pswitch_56
        :pswitch_56
        :pswitch_56
        :pswitch_56
        :pswitch_56
        :pswitch_56
        :pswitch_57
        :pswitch_57
        :pswitch_57
        :pswitch_57
        :pswitch_57
        :pswitch_57
        :pswitch_57
        :pswitch_57
        :pswitch_57
        :pswitch_57
        :pswitch_57
        :pswitch_58
        :pswitch_59
        :pswitch_5a
        :pswitch_5a
        :pswitch_5a
        :pswitch_5b
        :pswitch_5c
        :pswitch_5d
        :pswitch_5e
        :pswitch_5f
        :pswitch_60
        :pswitch_61
        :pswitch_62
        :pswitch_63
        :pswitch_63
        :pswitch_63
        :pswitch_63
        :pswitch_63
        :pswitch_63
        :pswitch_64
        :pswitch_65
        :pswitch_66
        :pswitch_67
        :pswitch_68
        :pswitch_68
        :pswitch_69
        :pswitch_6a
        :pswitch_6b
        :pswitch_6c
        :pswitch_6d
        :pswitch_6e
        :pswitch_6f
        :pswitch_70
        :pswitch_71
        :pswitch_72
        :pswitch_72
        :pswitch_72
        :pswitch_73
        :pswitch_74
        :pswitch_75
        :pswitch_76
        :pswitch_76
        :pswitch_77
        :pswitch_78
        :pswitch_78
        :pswitch_79
        :pswitch_7a
        :pswitch_7a
        :pswitch_7b
        :pswitch_7c
        :pswitch_7d
        :pswitch_7e
        :pswitch_7f
        :pswitch_7f
        :pswitch_7f
        :pswitch_80
        :pswitch_81
        :pswitch_82
        :pswitch_83
        :pswitch_84
        :pswitch_85
        :pswitch_85
        :pswitch_86
        :pswitch_87
        :pswitch_88
        :pswitch_89
        :pswitch_8a
        :pswitch_8b
        :pswitch_8c
        :pswitch_8d
        :pswitch_8e
        :pswitch_8f
        :pswitch_90
        :pswitch_91
        :pswitch_92
        :pswitch_93
        :pswitch_94
        :pswitch_95
        :pswitch_96
        :pswitch_97
        :pswitch_98
        :pswitch_99
        :pswitch_9a
        :pswitch_a6
        :pswitch_a7
        :pswitch_a8
        :pswitch_a9
        :pswitch_aa
        :pswitch_ab
        :pswitch_ac
        :pswitch_ac
        :pswitch_ac
        :pswitch_ac
        :pswitch_ad
        :pswitch_ad
        :pswitch_ae
        :pswitch_af
        :pswitch_b0
        :pswitch_b1
        :pswitch_b2
        :pswitch_b3
        :pswitch_b4
        :pswitch_b5
        :pswitch_b6
        :pswitch_b7
        :pswitch_b7
        :pswitch_b8
        :pswitch_b9
        :pswitch_b9
        :pswitch_b9
        :pswitch_b9
        :pswitch_ba
        :pswitch_bb
        :pswitch_bc
        :pswitch_bd
        :pswitch_be
        :pswitch_bf
        :pswitch_bf
        :pswitch_c0
        :pswitch_c0
        :pswitch_c0
        :pswitch_cb
        :pswitch_cc
        :pswitch_cd
        :pswitch_cd
        :pswitch_cd
    .end packed-switch

    .line 1674
    :pswitch_data_14
    .packed-switch 0x0
        :pswitch_c2
        :pswitch_c3
        :pswitch_c4
    .end packed-switch

    .line 1728
    :pswitch_data_15
    .packed-switch 0x0
        :pswitch_c8
        :pswitch_c9
    .end packed-switch
.end method
