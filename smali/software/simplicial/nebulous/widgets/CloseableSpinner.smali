.class public Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;
.super Landroid/widget/Spinner;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/nebulous/widgets/CloseableSpinner$a;
    }
.end annotation


# instance fields
.field private a:Lsoftware/simplicial/nebulous/widgets/CloseableSpinner$a;

.field private b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;->b:Z

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;I)V

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;->b:Z

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;->b:Z

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;->b:Z

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;->b:Z

    .line 37
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;->b:Z

    .line 67
    iget-object v0, p0, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;->a:Lsoftware/simplicial/nebulous/widgets/CloseableSpinner$a;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;->a:Lsoftware/simplicial/nebulous/widgets/CloseableSpinner$a;

    invoke-interface {v0}, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner$a;->b()V

    .line 71
    :cond_0
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;->b:Z

    return v0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 86
    invoke-super {p0, p1}, Landroid/widget/Spinner;->onWindowFocusChanged(Z)V

    .line 87
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 88
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;->a()V

    .line 89
    :cond_0
    return-void
.end method

.method public performClick()Z
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;->b:Z

    .line 46
    iget-object v0, p0, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;->a:Lsoftware/simplicial/nebulous/widgets/CloseableSpinner$a;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;->a:Lsoftware/simplicial/nebulous/widgets/CloseableSpinner$a;

    invoke-interface {v0}, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner$a;->a()V

    .line 50
    :cond_0
    invoke-super {p0}, Landroid/widget/Spinner;->performClick()Z

    move-result v0

    return v0
.end method

.method public setSpinnerEventsListener(Lsoftware/simplicial/nebulous/widgets/CloseableSpinner$a;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;->a:Lsoftware/simplicial/nebulous/widgets/CloseableSpinner$a;

    .line 59
    return-void
.end method
